﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Data.Entity;
using Vconnect.Common;
using System.Configuration;
using FiftyOne.Foundation.Mobile.Detection;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;
namespace Vconnect
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            Database.SetInitializer<Vconnect.Models.VconnectDBContext29>(null);

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            //BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
            // DeviceConfig.EvaluateDisplayMode(); //Evaluate incoming request and update Display Mode table
            //System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
        }
        protected void Session_Start()
        {
            switch ((HttpContext.Current.Request.Url.Host).ToString())
            {
                case "successdigestlocalsearch.vconnect.com":
                    Context.Session.Add("webconfigWebsiteRootPath", "http://successdigestlocalsearch.vconnect.com/");
                    break;
                case "thisdaylocalsearch.vconnect.com":
                    Context.Session.Add("webconfigWebsiteRootPath", "http://thisdaylocalsearch.vconnect.com/");
                    break;
                case "vanguardlocalsearch.vconnect.com":
                    Context.Session.Add("webconfigWebsiteRootPath", "http://vanguardlocalsearch.vconnect.com/");
                    break;
                case "smetoolkit.vconnect.com":
                    Context.Session.Add("webconfigWebsiteRootPath", "http://smetoolkit.vconnect.com/");
                    break;
                case "businessdaylocalsearch.vconnect.com":
                    Context.Session.Add("webconfigWebsiteRootPath", "http://businessdaylocalsearch.vconnect.com/");
                    break;
                default:
                    Context.Session.Add("webconfigWebsiteRootPath", "http://www.vconnect.com/");
                    break;
            }
            MyGlobalVariables._checkLoggedin = 0;
            MyGlobalVariables.GetIpAddress = "0";
            MyGlobalVariables.GetDomain = "0";
            MyGlobalVariables.GetReverseDns = "0";
            //  MyGlobalVariables.GetIpAddress = Request.UserHostAddress;
            //try
            //{
            //    MyGlobalVariables.GetDomain = HttpContext.Current.Request.Url.Host.ToString();
            //    MyGlobalVariables.GetReverseDns = System.Net.Dns.GetHostEntry(HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]).HostName.ToString();
            //}
            //string visitorIPAddress = "0";
            //visitorIPAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            //if (string.IsNullOrEmpty(visitorIPAddress))
            //{
            //    MyGlobalVariables.GetIpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            //    visitorIPAddress = MyGlobalVariables.GetIpAddress;
            //}

            //if (string.IsNullOrEmpty(visitorIPAddress))
            //{
            //    MyGlobalVariables.GetIpAddress = HttpContext.Current.Request.UserHostAddress;
            //    visitorIPAddress = MyGlobalVariables.GetIpAddress;
            //}

            //if (string.IsNullOrEmpty(visitorIPAddress))
            //{
            //    // GetLan = true;
            //    MyGlobalVariables.GetIpAddress = "0";

            //}
            //try
            //{
            //    MyGlobalVariables.GetDomain = HttpContext.Current.Request.Url.Host.ToString();
            //}

            //catch
            //{
            //    MyGlobalVariables.GetDomain = "0";

            //}
            ////MyGlobalVariables.GetReverseDns = System.Net.Dns.GetHostEntry(HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]).HostName.ToString();
            // try
            //{
            //   MyGlobalVariables.GetReverseDns = System.Net.Dns.GetHostEntry(visitorIPAddress).HostName.ToString();
            //}    


            //catch
            //{

            //    MyGlobalVariables.GetReverseDns = "0";
            //}
        }
        protected void Application_Error(object sender, EventArgs e)
        {
           // Response.Filter = null;
            //Response.Clear();
            Server.ClearError();
            var routeData = new RouteData();
            //routeData.Values["controller"] = "Static";
            //routeData.Values["action"] = "PageNotFound";
            routeData.Values.Add("controller", "Static");


            if ((Context.Server.GetLastError() is HttpException) && ((Context.Server.GetLastError() as HttpException).GetHttpCode() != 404))
            {
                //routeData.Values["action"] = "PageNotFound";
                //routeData.Values.Add("action", "PageNotFound");
                routeData.Values.Add("action", "PageNotFound");
            }
            else
            {
                // Handle 404 error and response code
                //Response.Clear();
                Response.StatusCode = 404;
                //routeData.Values["action"] = "PageNotFound";
                //routeData.Values.Add("action", "PageNotFound");
                routeData.Values.Add("action", "PageNotFound");
            }
           
            Response.TrySkipIisCustomErrors = true; // If you are using IIS7, have this line
            IController errorsController = new Vconnect.Controllers.StaticController();
            //HttpContextWrapper wrapper = new HttpContextWrapper(Context);
            //var rc = new System.Web.Routing.RequestContext(wrapper, routeData);
            //errorsController.Execute(rc);
            errorsController.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
            Response.End();
        }

        protected void Application_BeginRequest()
        {
            if (Context.Request.IsSecureConnection && HttpContext.Current.Request.Url.Scheme == "https")
            {
                Response.Redirect(Context.Request.Url.ToString().Replace("https:", "http:"));
            }
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
            UserSession objUS = new UserSession();
            //objUS.arrCookies.Add(ConfigurationManager.AppSettings["VCUserUrl"], "VCUserUrl");
            //objUS.arrCookies.Add(ConfigurationManager.AppSettings["VCLoginType"], "VCLoginType");
            objUS.arrCookies.Add(ConfigurationManager.AppSettings["VCLoginID"], "VCLoginID");
            objUS.arrCookies.Add(ConfigurationManager.AppSettings["VCUserName"], "VCUserName");
            objUS.arrCookies.Add(ConfigurationManager.AppSettings["VCUserContentID"], "VCUserContentID");
            objUS.arrCookies.Add(ConfigurationManager.AppSettings["VCEncriptedContentid"], "VCUserEnc_Id");
            objUS.arrCookies.Add(ConfigurationManager.AppSettings["VCUserType"], "VCUserType");
            //objUS.arrCookies.Add(ConfigurationManager.AppSettings["VCUserPhoto"], "VCUserPhoto");
            objUS.arrCookies.Add(ConfigurationManager.AppSettings["VCUserEmail"], "VCUserEmail");
            //objUS.arrCookies.Add(ConfigurationManager.AppSettings["VCUserPhone"], "VCUserPhone");
            
            if (objUS.IsSessionAlive())
                objUS.ValidateCookieTamperingAll();
            // commented on 9-oct-2014

            //string lowercaseURL = (Request.Url.Scheme + "://" +
            //HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.Url.AbsolutePath);
            //if (Regex.IsMatch(lowercaseURL, @"[A-Z]"))
            //{
            //    lowercaseURL = lowercaseURL.ToLower() + HttpContext.Current.Request.Url.Query;
            //    Response.Clear();
            //    Response.Status = "301 Moved Permanently";
            //    Response.AddHeader("Location", lowercaseURL);
            //    Response.End();
            //}
            MyGlobalVariables.IsWapWeb = "WEBSITE";
            //Asked by vikas for SMS connect 6th Apr 14
            if (HttpContext.Current.Request.Url.ToString().ToLower().IndexOf("www.vconnect.com/pages/static/send-free-sms.aspx‎") != -1)
            {
                Response.Redirect("http://m.vconnect.com/login?rturl=quicksms");
            }
            //Askd by chatarpal 
            if (HttpContext.Current.Request.Url.ToString().ToLower().IndexOf("aspxerrorpath‎") != -1)
            {
                Response.Clear();
                Response.StatusCode = 404;
                Response.Redirect("~/404");
            }
            if (HttpContext.Current.Request.Url.ToString().ToLower().IndexOf("genericerrorpage‎") != -1)
            {
                Response.Clear(); Response.Status = "301 Moved Permanently";
                Response.Redirect("/");
            }
            //if ((HttpContext.Current.Request.Url.ToString().ToLower().IndexOf("stanbic.vconnect.com") != -1))
            //{
            //    string websiteRawPath = ConfigurationManager.AppSettings["WebsiteRawPath"].ToString();
            //    string newUrl = HttpContext.Current.Request.RawUrl.ToLower();
            //    newUrl = "http://stanbic.vconnect.com" + "/advertisewithus";                
            //    Response.Redirect(newUrl);
            //}
            //else
            //{
            //redirectwap();

            //Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();                
            //}
            //if ((HttpContext.Current.Request.UrlReferrer != null
            //  && HttpContext.Current.Request.UrlReferrer.ToString().ToLower().IndexOf("m.vconnect.com") != -1
            //  && HttpContext.Current.Request.Url.AbsoluteUri.ToLower().IndexOf("vconnect.com") != -1) || MyGlobalVariables.noRedirect == true)
            //{

            //    MyGlobalVariables.noRedirect = true;
            //}
            //else
            //{
            //    redirectwap();
            //}
            //
            //string capability = Request.Browser["IsTablet"];            
            if ((HttpContext.Current.Request.Url.ToString().ToLower().IndexOf("www.stanbic.vconnect.com") != -1
                || HttpContext.Current.Request.Url.ToString().ToLower().IndexOf("stanbic.vconnect.com") != -1))
            {
                string capability =(Request !=null && Request.Browser !=null)? Request.Browser["IsTablet"] : "False";
               // if (Request.Browser["IsMobile"] == "True" && capability != "True")
                if ((Request != null && Request.Browser != null && Request.Browser["IsMobileDevice"] == "True" && capability != "True"))
                {
                    string newUrl = "http://stanbic.vconnect.co/advertisewithus";
                    Response.RedirectPermanent(newUrl);                   
                }
            }
            else
            {
                redirectwap();
            }
            //else if ((HttpContext.Current.Request.Url.ToString().ToLower().IndexOf("http://localhost:60140/widget") != -1
            //    || HttpContext.Current.Request.Url.ToString().ToLower().IndexOf("www.vconnect.com/widget") != -1
            //    || HttpContext.Current.Request.Url.ToString().ToLower().IndexOf("vconnect.com/widget") != -1)  ||
            //    (HttpContext.Current.Request.UrlReferrer != null && (HttpContext.Current.Request.UrlReferrer.ToString().ToLower().IndexOf("http://localhost:60140/widget") != -1
            //    || HttpContext.Current.Request.UrlReferrer.ToString().ToLower().IndexOf("www.vconnect.com/widget") != -1
            //    || HttpContext.Current.Request.UrlReferrer.ToString().ToLower().IndexOf("vconnect.com/widget") != -1)))
            //{
            //    string capability = Request.Browser["IsTablet"];
            //    if ((Request.Browser["IsMobile"] == "True" || Request.Browser["IsMobileDevice"] == "True") && capability != "True")
            //    {
                    
            //       // string newUrl = HttpContext.Current.Request.Url.ToString().ToLower();
            //        //this.Context.Server.Transfer(newUrl, true);
            //        //Response.Clear(); Response.Status = "301 Moved Permanently";
            //       // Response.RedirectPermanent(newUrl); Response.End();
            //       //Response.AddHeader("Location", newUrl.Trim()); Response.End();
            //    }                
            //}           
            
            //if ((HttpContext.Current.Request.Url.ToString().ToLower().IndexOf("stanbic.vconnect.com") != -1) 
            //{ }
            //else
            //{
            //redirectwap();}

        }
        
        

        public void redirectwap()
        {
            //string wapRawPath = ConfigurationManager.AppSettings["WAPRawPath"].ToString();
            //string wapRootPath = ConfigurationManager.AppSettings["WAPRootPath"].ToString();
            //string websiteRawPath = ConfigurationManager.AppSettings["WebsiteRawPath"].ToString();
            //string websiteRootPath = ConfigurationManager.AppSettings["WebsiteRootPath"].ToString();
            //string capability = Request.Browser["IsTablet"];
            //#region isMobileBrowser
            //if (Request.Browser["IsMobile"] == "True" && capability != "True")
            //{
            string wapRawPath = ConfigurationManager.AppSettings["WAPRawPath"].ToString();
            string wapRootPath = ConfigurationManager.AppSettings["WAPRootPath"].ToString();
            string websiteRawPath = ConfigurationManager.AppSettings["WebsiteRawPath"].ToString();
            string websiteRootPath = ConfigurationManager.AppSettings["WebsiteRootPath"].ToString();
            string capability = Request != null && Request.Browser != null ? Request.Browser["IsTablet"] : "false";
            string isSmallScreen = Request != null && Request.Browser != null ? Request.Browser["IsSmallScreen"] : "false";
            string Browser =Request != null && Request.Browser != null ? Request.Browser["Browser"] : "false";
            var isMobile = Request != null && Request.Browser != null  ? Request.Browser.IsMobileDevice : false;
            var DeviceType = Request != null && Request.Browser != null ? Request.Browser["DeviceType"] : "false";
            //var t="False";
            //if(Browser.Contains("Opera"))
            //{
            //t= isMobileBrowser().ToString();
            //}
            #region isMobileBrowser
            if ((Request != null && Request.Browser != null && Request.Browser.IsMobileDevice == true && capability != "True"))
            {
                //user directly opens vconnect.com
                //if ( (HttpContext.Current.Request.Url.ToString().ToLower().IndexOf("www.stanbic.vconnect.com") != -1
                //    || HttpContext.Current.Request.Url.ToString().ToLower().IndexOf("stanbic.vconnect.com") != -1))
                //{

                //    string newUrl  = "http://stanbic.vconnect.com/advertisewithus";
                //    //Response.Clear(); Response.Status = "301 Moved Permanently";
                //    //Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();
                //    //Response.AppendHeader("Refresh", "0; url=" + newUrl.Trim());
                //    Response.Redirect(newUrl);// Response.End();
                //}
                
            
                //When clicking web link from wap site
                if (HttpContext.Current.Request.UrlReferrer != null)
                {
                    if (HttpContext.Current.Request.UrlReferrer.ToString().ToLower().IndexOf("m.vconnect.com") != -1)
                    {
                        Response.AppendHeader("Refresh", "0; url=" + websiteRawPath);
                        Response.Redirect(ConfigurationManager.AppSettings["websiteRawPath"].ToString().ToLower().ToLower());
                    }
                }
                //http://www.vconnect.com/UserDashboardWEB/getUserDetails?userid= userid;
                //http://m.vconnect.com/UserDashboard/userprofile?id= userid
                //to divert user profile page when directly called from email 
                if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("userdashboard") != -1)
                {
                    string url = Request.RawUrl;
                    var newUrl = wapRawPath + url.ToLower().Replace("userdashboardweb", "userdashboard").Replace("getuserdetails", "userprofile").Replace("userid", "id");
                    Response.Redirect(newUrl.Trim().ToLower());
                }
                if (HttpContext.Current.Request.UrlReferrer == null
                    && (HttpContext.Current.Request.Url.AbsoluteUri.ToLower().IndexOf("www.vconnect.com") != -1
                    || HttpContext.Current.Request.Url.AbsoluteUri.ToLower().IndexOf("vconnect.com") != -1))
                {
                    //http://www.vconnect.com/UserDashboardWEB/getUserDetails?userid= userid;
                    //http://m.vconnect.com/UserDashboard/userprofile?id= userid
                    //to divert user profile page when directly called from email 
                    if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("userdashboard") != -1)
                    {
                        string url = Request.RawUrl;
                        var newUrl = wapRawPath + url.ToLower().Replace("userdashboardweb", "userdashboard").Replace("getuserdetails", "userprofile").Replace("userid", "id");
                        Response.Redirect(newUrl.Trim().ToLower());
                    }
                    if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("advertise") == -1
                 || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("privacypolicy") == -1
                 || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("vc-mobile") == -1
                     || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("vc-agent") == -1
                     || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("careers") == -1)
                    {
                        string url = Request.RawUrl;
                        if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("-page") != -1)
                        {
                            int spagestart = url.ToString().ToLower().IndexOf("-page");
                            url = url.Substring(0, spagestart);
                        }
                        var newUrl = wapRawPath + url.Replace(" ", "-").Replace("---", "-").Replace("--", "-");
                        Response.Redirect(newUrl.Trim().ToLower());
                    }
                    else
                    {
                        Response.Redirect(ConfigurationManager.AppSettings["WAPRootPath"].ToString().ToLower());
                    }
                }
                //user surfing vconnect.com
                else if (HttpContext.Current.Request.UrlReferrer != null
                    && HttpContext.Current.Request.UrlReferrer.ToString().ToLower().IndexOf("www.vconnect.com") != -1
                    && HttpContext.Current.Request.Url.AbsoluteUri.ToLower().IndexOf("www.vconnect.com") != -1)
                {
                    //http://www.vconnect.com/UserDashboardWEB/getUserDetails?userid= userid;
                    //http://m.vconnect.com/UserDashboard/userprofile?id= userid
                    //to divert user profile page when directly called from email 
                    if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("userdashboard") != -1)
                    {
                        string url = Request.RawUrl;
                        var newUrl = wapRawPath + url.ToLower().Replace("userdashboardweb", "userdashboard").Replace("getuserdetails", "userprofile").Replace("userid", "id");
                        Response.Redirect(newUrl.Trim().ToLower());
                    }
                    if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("advertise") == -1
                 || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("privacypolicy") == -1
                 || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("vc-mobile") == -1
                     || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("vc-agent") == -1
                     || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("careers") == -1)
                    {
                        string url = Request.RawUrl;
                        if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("-page") != -1)
                        {
                            int spagestart = url.ToString().ToLower().IndexOf("-page");
                            url = url.Substring(0, spagestart);
                        }
                        var newUrl = wapRawPath + url.Replace(" ", "-").Replace("---", "-").Replace("--", "-");
                        Response.Redirect(newUrl.Trim().ToLower());
                    }
                    else
                    {
                        Response.Redirect(ConfigurationManager.AppSettings["WAPRootPath"].ToString().ToLower());
                    }

                }
                else
                {
                    //http://www.vconnect.com/UserDashboardWEB/getUserDetails?userid= userid;
                    //http://m.vconnect.com/UserDashboard/userprofile?id= userid
                    //to divert user profile page when directly called from email 
                    if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("userdashboard") != -1)
                    {
                        string url = Request.RawUrl;
                        var newUrl = wapRawPath + url.ToLower().Replace("userdashboardweb", "userdashboard").Replace("getuserdetails", "userprofile").Replace("userid", "id");
                        Response.Redirect(newUrl.Trim().ToLower());
                    }
                    if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("advertise") == -1
                || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("privacypolicy") == -1
                || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("vc-mobile") == -1
                    || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("vc-agent") == -1
                        || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("careers") == -1)
                    {
                        string url = Request.RawUrl;
                        if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("-page") != -1)
                        {
                            int spagestart = url.ToString().ToLower().IndexOf("-page");
                            url = url.Substring(0, spagestart);
                        }
                        var newUrl = wapRawPath + url.Replace(" ", "-").Replace("---", "-").Replace("--", "-");
                        Response.Redirect(newUrl.Trim().ToLower());
                    }
                    else
                    {
                        Response.Redirect(ConfigurationManager.AppSettings["WAPRootPath"].ToString().ToLower());
                    }
                }
            }
            #endregion
            //*************Mobile URL ends *****************
            // ---  ,  --

            if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("?") == -1 && HttpContext.Current.Request.RawUrl.ToLower().IndexOf("=") != -1)
            {                                                    
                string newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower();
                newUrl = newUrl.Replace("=", "-");
                Response.Clear(); Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", newUrl.Trim()); Response.End();                
            }

            if (HttpContext.Current.Request.Url.AbsoluteUri.ToLower().IndexOf(websiteRootPath) != -1
                && (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("---") != -1
                || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("--") != -1))
            {
                string URL = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower().Replace("---", "-").Replace("--", "-").Replace("/code/", "");
                // /nigeria/list-of--qsearch
                if (URL.ToLower().IndexOf("list-of-qsearch") != -1)
                {
                    string newUrl = websiteRawPath;
                    Response.Clear(); Response.Status = "301 Moved Permanently";
                    Response.AddHeader("Location", newUrl.Trim()); Response.End();
                }
                else
                {
                    Response.Clear(); Response.Status = "301 Moved Permanently";
                    Response.AddHeader("Location", URL); Response.End();
                }
            }



            if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("---") != -1
                || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("--") != -1)
            {
                string newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower();
                newUrl = newUrl.Replace("---", "-").Replace("--", "-");
                Response.Clear(); Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", newUrl.Trim()); Response.End();
            }
            if (HttpContext.Current.Request.Url.AbsoluteUri.ToLower().StartsWith("http://vconnect.com"))
            {
                string newUrl = "http://www.vconnect.com" + HttpContext.Current.Request.RawUrl.ToLower();
                newUrl = newUrl.Replace("---", "-").Replace("--", "-");
                Response.Clear(); Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", newUrl.Trim()); Response.End();
            }
            else if (HttpContext.Current.Request.Url.AbsoluteUri.ToLower().IndexOf("404.aspx?404") != -1 ||
                     HttpContext.Current.Request.Url.AbsoluteUri.ToLower().IndexOf("404.aspx-404") != -1)
            {
                string newUrl = websiteRootPath + "404.aspx";
                Response.Clear(); Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();
            }
            //http://www.vconnect.com/juhel-nigeria-limited--_b130195
            //http://www.vconnect.com/juhel-nigeria-limited-_b130195
            else if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("-_b") != -1)
            {
                string newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower().Replace("-_b", "_b");
                Response.Clear(); Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();
            }
          

            else if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("list-of-") != -1
                     && HttpContext.Current.Request.RawUrl.ToLower().IndexOf("-&-") != -1)
            {
                string newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower().Replace("-&-", "-");
                Response.Clear(); Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();
            }
            //// http://www.vconnect.com/genericerrorpage.aspx?aspxerrorpath=/abuja-lugbe/brands-from-q
            else if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("genericerrorpage.aspx?aspxerrorpath=") != -1)
            {
                string newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower().Replace("genericerrorpage.aspx?aspxerrorpath=/", "").Replace("genericerrorpage.aspx?aspxerrorpath=-", "");
                Response.Clear(); Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();
            }
            //// http://www.vconnect.com/lagos/list-of-fairly-used-phones?sortby=reviews-qsearch
            else if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("?sortby=reviews-") != -1)
            {
                string newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower().Replace("?sortby=reviews-", "-");
                Response.Clear(); Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();
            }
            //http://www.vconnect.com/m.vconnect.com/abuja/list-of-life-theological-seminary-qsearch
            else if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("http://www.vconnect.com/m.vconnect.com/") != -1)
            {
                string newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower().Replace("m.vconnect.com/", "");
                Response.Clear(); Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();
            }//http://www.vconnect.com/abuja/list-of-haulage-iframe=true_c5797
            else if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("-iframe=true") != -1)
            {
                string newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower().Replace("-iframe=true", "");
                Response.Clear(); Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();

            }
            //http://www.vconnect.com/pages/static/vc-mobile.aspx
            else if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("pages/") != -1)
            {
                string newUrl = HttpContext.Current.Request.RawUrl.ToLower();

                if (newUrl.IndexOf("vc-mobile.aspx") != -1 || newUrl.IndexOf("vc_downloads.aspx") != -1)
                {
                    newUrl = websiteRawPath + "/apps";
                }
                else if (newUrl.IndexOf("vconnecter.aspx") != -1 || newUrl.IndexOf("partnership.aspx") != -1
                   || newUrl.IndexOf("advertise_display.aspx") != -1 || newUrl.IndexOf("bulk_mailer.aspx") != -1
                   || newUrl.IndexOf("send-free-sms.aspx") != -1 || newUrl.IndexOf("vconnect-works.aspx") != -1)
                {
                    newUrl = websiteRawPath + "/advertise";
                }
                else if (newUrl.IndexOf("findus.aspx") != -1 || newUrl.IndexOf("follow-twitter.aspx") != -1)
                {
                    newUrl = websiteRawPath + "/reachus";
                }
                else if (newUrl.IndexOf("getquotes.aspx") != -1 || newUrl.IndexOf("postjob.aspx") != -1)
                {
                    newUrl = websiteRawPath + "/careers";
                }
                else if (newUrl.IndexOf("howclaimbiz.aspx") != -1)
                {
                    newUrl = websiteRawPath + "/claimyourbusiness";
                }
                else if (newUrl.IndexOf("media-center.aspx") != -1)
                {
                    newUrl = websiteRawPath + "/pressrelease";
                }
                else if (newUrl.IndexOf("businessfree.aspx") != -1 || newUrl.IndexOf("business-free.aspx") != -1
                  || newUrl.IndexOf("business%20visible.aspx") != -1 || newUrl.IndexOf("businessowners.aspx") != -1
                  || newUrl.IndexOf("businesssubscription.aspx") != -1 || newUrl.IndexOf("how_to_writereviews.aspx") != -1
                  || newUrl.IndexOf("howsearchworks.aspx") != -1 || newUrl.IndexOf("like-facebook.aspx") != -1
                  || newUrl.IndexOf("local-reseller.aspx") != -1 || newUrl.IndexOf("missing-business.aspx") != -1
                  || newUrl.IndexOf("partners.aspx") != -1 || newUrl.IndexOf("howchangecity.aspx") != -1
                  || newUrl.IndexOf("howchatworks.aspx") != -1)
                {
                    newUrl = websiteRawPath + "/businesssolutions";
                }
                else
                {
                    newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower().Replace("pages/static/", "").Replace("pages/", "").Replace(".aspx", "");
                }
                // if (newUrl.IndexOf("vc-mobile") != -1 || newUrl.IndexOf("vc_downloads") != -1)
                Response.Clear(); Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();

            }

         //http://www.vconnect.com/content/advertise
            else if ((HttpContext.Current.Request.RawUrl.ToLower().IndexOf("content/") != -1)
                && (HttpContext.Current.Request.RawUrl.ToLower().IndexOf(".css") == -1)
                && (HttpContext.Current.Request.RawUrl.ToLower().IndexOf(".js") == -1))
            {
                string newUrl = HttpContext.Current.Request.RawUrl.ToLower();

                if (newUrl.IndexOf("services") != -1)
                {
                    newUrl = websiteRawPath + "/advertise";
                }
                else
                {
                    newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower().Replace("content/", "").Replace(".aspx", "");
                }
                Response.Clear(); Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();

            }
            //http://www.vconnect.com/claimyourbusiness_XX
            else if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("claimyourbusiness_xx") != -1)
            {
                string newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower().Replace("claimyourbusiness_xx", "claimyourbusiness");
                Response.Clear(); Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();

            }
            //http://www.vconnect.com/vconnect_successstories
            else if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("vconnect_successstories") != -1)
            {
                string newUrl = websiteRawPath + "/about_vconnect";
                Response.Clear(); Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();

            }
            //http://www.vconnect.com/qsearch?sq=fire-brigade&amp;sl=lagos
            else if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("amp;sl=") != -1 || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("amp;sid=") != -1)
            {
                string newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower().Replace("amp;sl=", "sl=").Replace("amp;sid=", "sid=");
                Response.Clear(); Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();

            }


        }

        public static bool isMobileBrowser()
        {
            //GETS THE CURRENT USER CONTEXT
            HttpContext context = HttpContext.Current;

            if (context.Request.UserAgent != null && context.Request.UserAgent.IndexOf("tablet") != -1)
            {
                return false;
            }

            //FIRST TRY BUILT IN ASP.NT CHECK
            if (context.Request.Browser.IsMobileDevice)
            {
                return true;
            }
            //THEN TRY CHECKING FOR THE HTTP_X_WAP_PROFILE HEADER
            if (context.Request.ServerVariables["HTTP_X_WAP_PROFILE"] != null)
            {
                return true;
            }
            //THEN TRY CHECKING THAT HTTP_ACCEPT EXISTS AND CONTAINS WAP
            if (context.Request.ServerVariables["HTTP_ACCEPT"] != null &&
                context.Request.ServerVariables["HTTP_ACCEPT"].ToLower().Contains("wap"))
            {
                return true;
            }
            //AND FINALLY CHECK THE HTTP_USER_AGENT 
            //HEADER VARIABLE FOR ANY ONE OF THE FOLLOWING
            if (context.Request.ServerVariables["HTTP_USER_AGENT"] != null)
            {
                //Create a list of all mobile types
                string[] mobiles =
                    new[]
                {
                     "midp", "j2me", "avant", "docomo", 
                    "novarra", "palmos", "palmsource", 
                    "240x320", "opwv", "chtml",
                    "pda", "windows ce", "mmp/", 
                    "blackberry", "mib/", "symbian", 
                    "wireless", "nokia", "hand", "mobi",
                    "phone", "cdm", "up.b", "audio", 
                    "SIE-", "SEC-", "samsung", "HTC", 
                    "mot-", "mitsu", "sagem", "sony"
                    , "alcatel", "lg", "eric", "vx", 
                    "NEC", "philips", "mmm", "xx", 
                    "panasonic", "sharp", "wap", "sch",
                    "rover", "pocket", "benq", "java", 
                    "pt", "vox", "amoi", 
                    "bird", "compal", "kg", "voda",
                    "sany", "kdd", "dbt", "sendo", 
                    "sgh", "gradi", "jb", "dddi", 
                    "moto", "iphone", "Android","Googlebot-Mobile",
                    "iOS","Garnet OS","Palm OS","MeeGo","Maemo",
                    "LiMo 4","DangerOS","Verdict","Nucleus RTOS",
                    "Tizen","webOS","Windows Phone","BlackBerry PlayBook OS",
                    "BlackBerry 10","BlackBerry OS","Brew","Mer project",
                    "GridOS","Sailfish OS","Series 40","Palm OS","Nokia Asha",
                    "Aliyun OS","Bada","Firefox OS","Windows Mobile","Windows RT","SHR","Series40","Playstation Vita","MOT",
                    "Playstation 3","Google TV","Nintendo 3DS","Nintendo Wii",
                    "Xbox","UNIX","HPUX","OpenBSD","Vodafone"
                };

                //Loop through each item in the list created above 
                //and check if the header contains that text
                foreach (string s in mobiles)
                {
                    if (context.Request.ServerVariables["HTTP_USER_AGENT"].ToLower().Contains(s.ToLower()))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        public static string GetVisitorIPAddress(bool GetLan = false)
        {
            string visitorIPAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrEmpty(visitorIPAddress))
                visitorIPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

            if (string.IsNullOrEmpty(visitorIPAddress))
                visitorIPAddress = HttpContext.Current.Request.UserHostAddress;

            if (string.IsNullOrEmpty(visitorIPAddress) || visitorIPAddress.Trim() == "::1")
            {
                GetLan = true;
                visitorIPAddress = string.Empty;
            }

            if (GetLan)
            {
                if (string.IsNullOrEmpty(visitorIPAddress))
                {
                    //This is for Local(LAN) Connected ID Address
                    string stringHostName = Dns.GetHostName();
                    //Get Ip Host Entry
                    IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
                    //Get Ip Address From The Ip Host Entry Address List
                    IPAddress[] arrIpAddress = ipHostEntries.AddressList;

                    try
                    {
                        //visitorIPAddress = arrIpAddress[arrIpAddress.Length - 2].ToString();
                        foreach (var address in arrIpAddress)
                        {

                            if (address.AddressFamily == AddressFamily.InterNetwork)
                                visitorIPAddress = address.ToString();
                        }
                    }
                    catch
                    {
                        try
                        {
                            visitorIPAddress = arrIpAddress[0].ToString();
                        }
                        catch
                        {
                            try
                            {
                                arrIpAddress = Dns.GetHostAddresses(stringHostName);
                                visitorIPAddress = arrIpAddress[0].ToString();
                            }
                            catch
                            {
                                visitorIPAddress = "127.0.0.1";
                            }
                        }
                    }
                }
            }
            MyGlobalVariables.GetIpAddress = visitorIPAddress;
            return visitorIPAddress;
        }
        public static string GetReverseDns(string VisitorIPAddress)
        {
            try
            {
                MyGlobalVariables.GetReverseDns = System.Net.Dns.GetHostEntry(VisitorIPAddress).HostName.ToString();
            }
            catch
            {
                MyGlobalVariables.GetReverseDns = "0";
            }
            return MyGlobalVariables.GetReverseDns;

        }
    }
}

/////live commented
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////stage uncommented
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Http;
//using System.Web.Mvc;
//using System.Web.Optimization;
//using System.Web.Routing;
//using System.Data.Entity;
//using Vconnect.Common;
//using System.Configuration;
//using FiftyOne.Foundation.Mobile.Detection;
//using System.Net;
////using System.Net;
//namespace Vconnect
//{
//    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
//    // visit http://go.microsoft.com/?LinkId=9394801

//    public class MvcApplication : System.Web.HttpApplication
//    {
//        protected void Application_Start()
//        {
//            AreaRegistration.RegisterAllAreas();
//            Database.SetInitializer<Vconnect.Models.VconnectDBContext29>(null);
//            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
//            RouteConfig.RegisterRoutes(RouteTable.Routes);
//            BundleConfig.RegisterBundles(BundleTable.Bundles);
//            AuthConfig.RegisterAuth();
//            // DeviceConfig.EvaluateDisplayMode(); //Evaluate incoming request and update Display Mode table
//            //System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
//        }
//        protected void Session_Start()
//        {
//            switch ((HttpContext.Current.Request.Url.Host).ToString())
//            {
//                case "successdigestlocalsearch.vconnect.com":
//                    Context.Session.Add("webconfigWebsiteRootPath", "http://successdigestlocalsearch.vconnect.com/");
//                    break;
//                case "thisdaylocalsearch.vconnect.com":
//                    Context.Session.Add("webconfigWebsiteRootPath", "http://thisdaylocalsearch.vconnect.com/");
//                    break;
//                case "vanguardlocalsearch.vconnect.com":
//                    Context.Session.Add("webconfigWebsiteRootPath", "http://vanguardlocalsearch.vconnect.com/");
//                    break;
//                case "smetoolkit.vconnect.com":
//                    Context.Session.Add("webconfigWebsiteRootPath", "http://smetoolkit.vconnect.com/");
//                    break;
//                case "businessdaylocalsearch.vconnect.com":
//                    Context.Session.Add("webconfigWebsiteRootPath", "http://businessdaylocalsearch.vconnect.com/");
//                    break;
//                default:
//                    Context.Session.Add("webconfigWebsiteRootPath", "http://www.stage.vconnect.com/");
//                    break;
//            }
//            MyGlobalVariables._checkLoggedin = 0;
//            MyGlobalVariables.GetIpAddress = "0";
//            MyGlobalVariables.GetDomain = "0";
//            MyGlobalVariables.GetReverseDns = "0";
//            ////////if (Session["IPADDRESS"] == null)
//            ////////    Session["IPADDRESS"] = GetVisitorIPAddress();

//            ////////if (Session["REVERSEDNS"] == null || Session["REVERSEDNS"] == "")
//            ////////    Session["REVERSEDNS"] = GetReverseDns(Session["IPADDRESS"].ToString());

//            ////////MyGlobalVariables.GetIpAddress = Session["IPADDRESS"].ToString();
//            ////////MyGlobalVariables.GetReverseDns = Session["REVERSEDNS"].ToString();
//            ////////MyGlobalVariables.GetIpAddress = Request.UserHostAddress;
//            ////////try
//            ////////{
//            ////////    MyGlobalVariables.GetDomain = HttpContext.Current.Request.Url.Host.ToString();
//            ////////    MyGlobalVariables.GetReverseDns = System.Net.Dns.GetHostEntry(HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]).HostName.ToString();
//            ////////}
//            ////////catch
//            ////////{
//            ////////    MyGlobalVariables.GetDomain = "0";
//            ////////    MyGlobalVariables.GetReverseDns = "0";
//            ////////}
//            //if (Session["IPADDRESS"] == null) 
//            //    Session["IPADDRESS"] = GetVisitorIPAddress();

//            //if (Session["REVERSEDNS"] == null || Session["REVERSEDNS"] == "")
//            //    Session["REVERSEDNS"] = GetReverseDns(Session["IPADDRESS"].ToString());

//            //MyGlobalVariables.GetIpAddress = Session["IPADDRESS"].ToString();
//            //MyGlobalVariables.GetReverseDns = Session["REVERSEDNS"].ToString();
//            //MyGlobalVariables.GetIpAddress = Request.UserHostAddress;
//            //try
//            //{
//            //    MyGlobalVariables.GetDomain = HttpContext.Current.Request.Url.Host.ToString();
//            //    MyGlobalVariables.GetReverseDns = System.Net.Dns.GetHostEntry(HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]).HostName.ToString();
//            //}
//            //catch
//            //{
//            //    MyGlobalVariables.GetDomain = "0";
//            //    MyGlobalVariables.GetReverseDns = "0";
//            //}
//            //string visitorIPAddress = "0";
//            //visitorIPAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

//            //if (string.IsNullOrEmpty(visitorIPAddress))
//            //{
//            //    MyGlobalVariables.GetIpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
//            //    visitorIPAddress = MyGlobalVariables.GetIpAddress;
//            //}

//            //if (string.IsNullOrEmpty(visitorIPAddress))
//            //{
//            //    MyGlobalVariables.GetIpAddress = HttpContext.Current.Request.UserHostAddress;
//            //    visitorIPAddress = MyGlobalVariables.GetIpAddress;
//            //}

//            //if (string.IsNullOrEmpty(visitorIPAddress))
//            //{
//            //    // GetLan = true;
//            //    MyGlobalVariables.GetIpAddress = "0";

//            //}
//            //try
//            //{
//            //    MyGlobalVariables.GetDomain = HttpContext.Current.Request.Url.Host.ToString();
//            //}

//            //catch
//            //{
//            //    MyGlobalVariables.GetDomain = "0";

//            //}
//            ////MyGlobalVariables.GetReverseDns = System.Net.Dns.GetHostEntry(HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]).HostName.ToString();
//            //try
//            //{
//            //    MyGlobalVariables.GetReverseDns = System.Net.Dns.GetHostEntry(visitorIPAddress).HostName.ToString();
//            //}


//            //catch
//            //{

//            //    MyGlobalVariables.GetReverseDns = "0";
//            //}
//        }
//        protected void Application_BeginRequest()
//        {

//            var ipAddress = Context.Request.UserHostAddress;
//            MyGlobalVariables.IsWapWeb = "WEBSITE";

//            //Asked by vikas for SMS connect 6th Apr 14
//            if (HttpContext.Current.Request.Url.ToString().ToLower().IndexOf("www.stage.vconnect.com/pages/static/send-free-sms.aspx‎") != -1)
//            {
//                Response.Redirect("http://m.vconnect.com/login?rturl=quicksms");
//            }
//            //Askd by chatarpal 
//            if (HttpContext.Current.Request.Url.ToString().ToLower().IndexOf("aspxerrorpath‎") != -1)
//            {
//                Response.Redirect("~/404");
//            }
//            if (HttpContext.Current.Request.Url.ToString().ToLower().IndexOf("genericerrorpage‎") != -1)
//            {
//                Response.Clear(); Response.Status = "301 Moved Permanently";
//                Response.Redirect("/");
//            }

//            redirectwap();

//        }

//        //protected void Application_Error(object sender, EventArgs e)
//        //{
//        //    Exception ex = Server.GetLastError();
//        //    if (ex is HttpException && ((HttpException)ex).GetHttpCode() == 404)
//        //    {

//        //        Response.Redirect("~/404");
//        //    }
//        //    else
//        //    {
//        //        // your global error handling here!
//        //    }
//        //}

//        protected void Application_Error(object sender, EventArgs e)
//        {
//            Server.ClearError();
//            var routeData = new RouteData();
//            //routeData.Values["controller"] = "Static";
//            //routeData.Values["action"] = "PageNotFound";
//            routeData.Values.Add("controller", "Static");


//            if ((Context.Server.GetLastError() is HttpException) && ((Context.Server.GetLastError() as HttpException).GetHttpCode() != 404))
//            {
//                //routeData.Values["action"] = "PageNotFound";
//                //routeData.Values.Add("action", "PageNotFound");
//                routeData.Values.Add("action", "PageNotFound");
//            }
//            else
//            {
//                // Handle 404 error and response code
//                Response.StatusCode = 404;
//                //routeData.Values["action"] = "PageNotFound";
//                //routeData.Values.Add("action", "PageNotFound");
//                routeData.Values.Add("action", "PageNotFound");
//            }
//            Response.TrySkipIisCustomErrors = true; // If you are using IIS7, have this line
//            IController errorsController = new Vconnect.Controllers.StaticController();
//            //HttpContextWrapper wrapper = new HttpContextWrapper(Context);
//            //var rc = new System.Web.Routing.RequestContext(wrapper, routeData);
//            //errorsController.Execute(rc);
//            errorsController.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
//            Response.End();
//        }


//        //protected void Application_Error(object sender, EventArgs e)
//        //{
//        //    Exception exception = Server.GetLastError();
//        //    Server.ClearError();

//        //    RouteData routeData = new RouteData();
//        //    routeData.Values.Add("controller", "Static");
//        //    routeData.Values.Add("action", "PageNotFound");
//        //   // routeData.Values.Add("exception", exception);

//        //    if (exception.GetType() == typeof(HttpException))
//        //    {
//        //        routeData.Values.Add("statusCode", ((HttpException)exception).GetHttpCode());
//        //    }
//        //    else
//        //    {
//        //        routeData.Values.Add("statusCode", 500);
//        //    }

//        //    IController controller = new Vconnect.Controllers.StaticController();
//        //    controller.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
//        //    Response.End();
//        //}
//        public void redirectwap()
//        {
//            string wapRawPath = ConfigurationManager.AppSettings["WAPRawPath"].ToString();
//            string wapRootPath = ConfigurationManager.AppSettings["WAPRootPath"].ToString();
//            string websiteRawPath = ConfigurationManager.AppSettings["WebsiteRawPath"].ToString();
//            string websiteRootPath = ConfigurationManager.AppSettings["WebsiteRootPath"].ToString();//
//            string capability = Request.Browser["IsTablet"];

//            #region isMobileBrowser

//            //if (isMobileBrowser() == true)
//            if (Request.Browser["IsMobile"] == "True" && capability != "True")
//            {
//                //When clicking web link from wap site
//                if (HttpContext.Current.Request.UrlReferrer != null)
//                {
//                    if (HttpContext.Current.Request.UrlReferrer.ToString().ToLower().IndexOf("m.vconnect.com") != -1)
//                    {
//                        Response.AppendHeader("Refresh", "0; url=" + websiteRawPath);
//                        Response.Redirect(ConfigurationManager.AppSettings["websiteRawPath"].ToString());
//                    }
//                }
//                //user directly opens vconnect.com
//                if (HttpContext.Current.Request.UrlReferrer == null
//                    && (HttpContext.Current.Request.Url.AbsoluteUri.ToLower().IndexOf("www.stage.vconnect.com") != -1
//                    || HttpContext.Current.Request.Url.AbsoluteUri.ToLower().IndexOf("vconnect.com") != -1))
//                {
//                    //http://www.vconnect.com/UserDashboardWEB/getUserDetails?userid= userid;
//                    //http://m.vconnect.com/UserDashboard/userprofile?id= userid
//                    //to divert user profile page when directly called from email 
//                    if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("userdashboard") != -1)
//                    {
//                        string url = Request.RawUrl;
//                        var newUrl = wapRawPath + url.ToLower().Replace("userdashboardweb", "userdashboard").Replace("getuserdetails", "userprofile").Replace("userid", "id");
//                        Response.Redirect(newUrl.Trim());
//                    }

//                    if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("advertise") == -1
//                 || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("privacypolicy") == -1
//                 || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("vc-mobile") == -1
//                     || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("vc-agent") == -1
//                     || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("careers") == -1)
//                    {
//                        string url = Request.RawUrl;
//                        if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("-page") != -1)
//                        {
//                            int spagestart = url.ToString().ToLower().IndexOf("-page");
//                            url = url.Substring(0, spagestart);
//                        }
//                        var newUrl = wapRawPath + url.Replace(" ", "-").Replace("---", "-").Replace("--", "-");
//                        Response.Redirect(newUrl.Trim());
//                    }
//                    else
//                    {
//                        Response.Redirect(ConfigurationManager.AppSettings["WAPRootPath"].ToString());
//                    }

//                }
//                //user surfing vconnect.com
//                else if (HttpContext.Current.Request.UrlReferrer != null
//                    && HttpContext.Current.Request.UrlReferrer.ToString().ToLower().IndexOf("www.stage.vconnect.com") != -1
//                    && HttpContext.Current.Request.Url.AbsoluteUri.ToLower().IndexOf("www.stage.vconnect.com") != -1)
//                {
//                    if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("userdashboard") != -1)
//                    {
//                        string url = Request.RawUrl;
//                        var newUrl = wapRawPath + url.ToLower().Replace("userdashboardweb", "userdashboard").Replace("getuserdetails", "userprofile").Replace("userid", "id");
//                        Response.Redirect(newUrl.Trim());
//                    }

//                    if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("advertise") == -1
//                 || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("privacypolicy") == -1
//                 || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("vc-mobile") == -1
//                     || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("vc-agent") == -1
//                     || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("careers") == -1)
//                    {
//                        string url = Request.RawUrl;
//                        if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("-page") != -1)
//                        {
//                            int spagestart = url.ToString().ToLower().IndexOf("-page");
//                            url = url.Substring(0, spagestart);
//                        }
//                        var newUrl = wapRawPath + url.Replace(" ", "-").Replace("---", "-").Replace("--", "-");
//                        Response.Redirect(newUrl.Trim());
//                    }
//                    else
//                    {
//                        Response.Redirect(ConfigurationManager.AppSettings["WAPRootPath"].ToString());
//                    }

//                }
//                else
//                {
//                    if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("userdashboard") != -1)
//                    {
//                        string url = Request.RawUrl;
//                        var newUrl = wapRawPath + url.ToLower().Replace("userdashboardweb", "userdashboard").Replace("getuserdetails", "userprofile").Replace("userid", "id");
//                        Response.Redirect(newUrl.Trim());
//                    }
//                    if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("advertise") == -1
//                || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("privacypolicy") == -1
//                || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("vc-mobile") == -1
//                    || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("vc-agent") == -1
//                        || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("careers") == -1)
//                    {
//                        string url = Request.RawUrl;
//                        if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("-page") != -1)
//                        {
//                            int spagestart = url.ToString().ToLower().IndexOf("-page");
//                            url = url.Substring(0, spagestart);
//                        }
//                        var newUrl = wapRawPath + url.Replace(" ", "-").Replace("---", "-").Replace("--", "-");
//                        Response.Redirect(newUrl.Trim());
//                    }
//                    else
//                    {
//                        Response.Redirect(ConfigurationManager.AppSettings["WAPRootPath"].ToString());
//                    }
//                }
//            }
//            #endregion
//            //*************Mobile URL ends *****************
//            // ---  ,  --
//            if (HttpContext.Current.Request.Url.AbsoluteUri.ToLower().IndexOf(websiteRootPath) != -1
//                && (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("---") != -1
//                || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("--") != -1))
//            {
//                string URL = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower().Replace("---", "-").Replace("--", "-").Replace("/code/", "");
//                // /nigeria/list-of--qsearch
//                if (URL.ToLower().IndexOf("list-of-qsearch") != -1)
//                {
//                    string newUrl = websiteRawPath;
//                    Response.Clear(); Response.Status = "301 Moved Permanently";
//                    Response.AddHeader("Location", newUrl.Trim()); Response.End();
//                }
//                else
//                {
//                    Response.Clear(); Response.Status = "301 Moved Permanently";
//                    Response.AddHeader("Location", URL); Response.End();
//                }
//            }


//            if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("---") != -1
//                || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("--") != -1)
//            {
//                string newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower();
//                newUrl = newUrl.Replace("---", "-").Replace("--", "-");
//                Response.Clear(); Response.Status = "301 Moved Permanently";
//                Response.AddHeader("Location", newUrl.Trim()); Response.End();
//            }
//            if (HttpContext.Current.Request.Url.AbsoluteUri.ToLower().StartsWith("http://www.stage.vconnect.com"))
//            {
//                string newUrl = "http://www.stage.vconnect.com" + HttpContext.Current.Request.RawUrl.ToLower();
//                newUrl = newUrl.Replace("---", "-").Replace("--", "-");
//                Response.Clear(); Response.Status = "301 Moved Permanently";
//                Response.AddHeader("Location", newUrl.Trim()); Response.End();
//            }
//            else if (HttpContext.Current.Request.Url.AbsoluteUri.ToLower().IndexOf("404.aspx?404") != -1 ||
//                     HttpContext.Current.Request.Url.AbsoluteUri.ToLower().IndexOf("404.aspx-404") != -1)
//            {
//                string newUrl = websiteRootPath + "404.aspx";
//                Response.Clear(); Response.Status = "301 Moved Permanently";
//                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();
//            }
//            //http://www.stage.vconnect.com/juhel-nigeria-limited--_b130195
//            //http://www.stage.vconnect.com/juhel-nigeria-limited-_b130195
//            else if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("-_b") != -1)
//            {
//                string newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower().Replace("-_b", "_b");
//                Response.Clear(); Response.Status = "301 Moved Permanently";
//                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();
//            }
//            else if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("list-of-") != -1
//                     && HttpContext.Current.Request.RawUrl.ToLower().IndexOf("-&-") != -1)
//            {
//                string newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower().Replace("-&-", "-");
//                Response.Clear(); Response.Status = "301 Moved Permanently";
//                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();
//            }
//            //// http://www.stage.vconnect.com/genericerrorpage.aspx?aspxerrorpath=/abuja-lugbe/brands-from-q
//            else if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("genericerrorpage.aspx?aspxerrorpath=") != -1)
//            {
//                string newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower().Replace("genericerrorpage.aspx?aspxerrorpath=/", "").Replace("genericerrorpage.aspx?aspxerrorpath=-", "");
//                Response.Clear(); Response.Status = "301 Moved Permanently";
//                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();
//            }
//            //// http://www.stage.vconnect.com/lagos/list-of-fairly-used-phones?sortby=reviews-qsearch
//            else if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("?sortby=reviews-") != -1)
//            {
//                string newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower().Replace("?sortby=reviews-", "-");
//                Response.Clear(); Response.Status = "301 Moved Permanently";
//                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();
//            }
//            //http://www.stage.vconnect.com/m.vconnect.com/abuja/list-of-life-theological-seminary-qsearch
//            else if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("http://www.stage.vconnect.com/m.vconnect.com/") != -1)
//            {
//                string newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower().Replace("m.vconnect.com/", "");
//                Response.Clear(); Response.Status = "301 Moved Permanently";
//                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();
//            }//http://www.stage.vconnect.com/abuja/list-of-haulage-iframe=true_c5797
//            else if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("-iframe=true") != -1)
//            {
//                string newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower().Replace("-iframe=true", "");
//                Response.Clear(); Response.Status = "301 Moved Permanently";
//                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();

//            }
//            //http://www.stage.vconnect.com/pages/static/vc-mobile.aspx
//            else if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("pages/") != -1)
//            {
//                string newUrl = HttpContext.Current.Request.RawUrl.ToLower();

//                if (newUrl.IndexOf("vc-mobile.aspx") != -1 || newUrl.IndexOf("vc_downloads.aspx") != -1)
//                {
//                    newUrl = websiteRawPath + "/apps";
//                }
//                else if (newUrl.IndexOf("vconnecter.aspx") != -1 || newUrl.IndexOf("partnership.aspx") != -1
//                   || newUrl.IndexOf("advertise_display.aspx") != -1 || newUrl.IndexOf("bulk_mailer.aspx") != -1
//                   || newUrl.IndexOf("send-free-sms.aspx") != -1 || newUrl.IndexOf("vconnect-works.aspx") != -1)
//                {
//                    newUrl = websiteRawPath + "/advertise";
//                }
//                else if (newUrl.IndexOf("findus.aspx") != -1 || newUrl.IndexOf("follow-twitter.aspx") != -1)
//                {
//                    newUrl = websiteRawPath + "/reachus";
//                }
//                else if (newUrl.IndexOf("getquotes.aspx") != -1 || newUrl.IndexOf("postjob.aspx") != -1)
//                {
//                    newUrl = websiteRawPath + "/careers";
//                }
//                else if (newUrl.IndexOf("howclaimbiz.aspx") != -1)
//                {
//                    newUrl = websiteRawPath + "/claimyourbusiness";
//                }
//                else if (newUrl.IndexOf("media-center.aspx") != -1)
//                {
//                    newUrl = websiteRawPath + "/pressrelease";
//                }
//                else if (newUrl.IndexOf("businessfree.aspx") != -1 || newUrl.IndexOf("business-free.aspx") != -1
//                  || newUrl.IndexOf("business%20visible.aspx") != -1 || newUrl.IndexOf("businessowners.aspx") != -1
//                  || newUrl.IndexOf("businesssubscription.aspx") != -1 || newUrl.IndexOf("how_to_writereviews.aspx") != -1
//                  || newUrl.IndexOf("howsearchworks.aspx") != -1 || newUrl.IndexOf("like-facebook.aspx") != -1
//                  || newUrl.IndexOf("local-reseller.aspx") != -1 || newUrl.IndexOf("missing-business.aspx") != -1
//                  || newUrl.IndexOf("partners.aspx") != -1 || newUrl.IndexOf("howchangecity.aspx") != -1
//                  || newUrl.IndexOf("howchatworks.aspx") != -1)
//                {
//                    newUrl = websiteRawPath + "/businesssolutions";
//                }
//                else
//                {
//                    newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower().Replace("pages/static/", "").Replace("pages/", "").Replace(".aspx", "");
//                }
//                // if (newUrl.IndexOf("vc-mobile") != -1 || newUrl.IndexOf("vc_downloads") != -1)
//                Response.Clear(); Response.Status = "301 Moved Permanently";
//                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();

//            }

//         //http://www.stage.vconnect.com/content/advertise
//            else if ((HttpContext.Current.Request.RawUrl.ToLower().IndexOf("content/") != -1)
//                && (HttpContext.Current.Request.RawUrl.ToLower().IndexOf(".css") == -1)
//                && (HttpContext.Current.Request.RawUrl.ToLower().IndexOf(".js") == -1))
//            {
//                string newUrl = HttpContext.Current.Request.RawUrl.ToLower();

//                if (newUrl.IndexOf("services") != -1)
//                {
//                    newUrl = websiteRawPath + "/advertise";
//                }
//                else
//                {
//                    newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower().Replace("content/", "").Replace(".aspx", "");
//                }
//                Response.Clear(); Response.Status = "301 Moved Permanently";
//                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();

//            }
//            //http://www.stage.vconnect.com/claimyourbusiness_XX
//            else if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("claimyourbusiness_xx") != -1)
//            {
//                string newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower().Replace("claimyourbusiness_xx", "claimyourbusiness");
//                Response.Clear(); Response.Status = "301 Moved Permanently";
//                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();

//            }
//            //http://www.stage.vconnect.com/vconnect_successstories
//            else if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("vconnect_successstories") != -1)
//            {
//                string newUrl = websiteRawPath + "/about_vconnect";
//                Response.Clear(); Response.Status = "301 Moved Permanently";
//                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();

//            }
//            //http://www.stage.vconnect.com/qsearch?sq=fire-brigade&amp;sl=lagos
//            else if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("amp;sl=") != -1 || HttpContext.Current.Request.RawUrl.ToLower().IndexOf("amp;sid=") != -1)
//            {
//                string newUrl = websiteRawPath + HttpContext.Current.Request.RawUrl.ToLower().Replace("amp;sl=", "sl=").Replace("amp;sid=", "sid=");
//                Response.Clear(); Response.Status = "301 Moved Permanently";
//                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();

//            }


//        }


//        public static bool isMobileBrowser()
//        {
//            //GETS THE CURRENT USER CONTEXT
//            HttpContext context = HttpContext.Current;

//            if (context.Request.UserAgent.IndexOf("tablet") != -1)
//            {
//                return false;
//            }

//            //FIRST TRY BUILT IN ASP.NT CHECK
//            if (context.Request.Browser.IsMobileDevice)
//            {
//                return true;
//            }
//            //THEN TRY CHECKING FOR THE HTTP_X_WAP_PROFILE HEADER
//            if (context.Request.ServerVariables["HTTP_X_WAP_PROFILE"] != null)
//            {
//                return true;
//            }
//            //THEN TRY CHECKING THAT HTTP_ACCEPT EXISTS AND CONTAINS WAP
//            if (context.Request.ServerVariables["HTTP_ACCEPT"] != null &&
//                context.Request.ServerVariables["HTTP_ACCEPT"].ToLower().Contains("wap"))
//            {
//                return true;
//            }
//            //AND FINALLY CHECK THE HTTP_USER_AGENT 
//            //HEADER VARIABLE FOR ANY ONE OF THE FOLLOWING
//            if (context.Request.ServerVariables["HTTP_USER_AGENT"] != null)
//            {
//                //Create a list of all mobile types
//                string[] mobiles =
//                    new[]
//                {
//                     "midp", "j2me", "avant", "docomo", 
//                    "novarra", "palmos", "palmsource", 
//                    "240x320", "opwv", "chtml",
//                    "pda", "windows ce", "mmp/", 
//                    "blackberry", "mib/", "symbian", 
//                    "wireless", "nokia", "hand", "mobi",
//                    "phone", "cdm", "up.b", "audio", 
//                    "SIE-", "SEC-", "samsung", "HTC", 
//                    "mot-", "mitsu", "sagem", "sony"
//                    , "alcatel", "lg", "eric", "vx", 
//                    "NEC", "philips", "mmm", "xx", 
//                    "panasonic", "sharp", "wap", "sch",
//                    "rover", "pocket", "benq", "java", 
//                    "pt", "vox", "amoi", 
//                    "bird", "compal", "kg", "voda",
//                    "sany", "kdd", "dbt", "sendo", 
//                    "sgh", "gradi", "jb", "dddi", 
//                    "moto", "iphone", "Android","Googlebot-Mobile",
//                    "iOS","Garnet OS","Palm OS","MeeGo","Maemo",
//                    "LiMo 4","DangerOS","Verdict","Nucleus RTOS",
//                    "Tizen","webOS","Windows Phone","BlackBerry PlayBook OS",
//                    "BlackBerry 10","BlackBerry OS","Brew","Mer project",
//                    "GridOS","Sailfish OS","Series 40","Palm OS","Nokia Asha",
//                    "Aliyun OS","Bada","Firefox OS","Windows Mobile","Windows RT","SHR","Series40","Playstation Vita","MOT",
//                    "Playstation 3","Google TV","Nintendo 3DS","Nintendo Wii",
//                    "Xbox","UNIX","HPUX","OpenBSD","Vodafone"
//                };

//                //Loop through each item in the list created above 
//                //and check if the header contains that text
//                foreach (string s in mobiles)
//                {
//                    if (context.Request.ServerVariables["HTTP_USER_AGENT"].ToLower().Contains(s.ToLower()))
//                    {
//                        return true;
//                    }
//                }
//            }

//            return false;
//        }


//        /// <summary>
//        /// method to get Client ip address
//        /// </summary>
//        /// <param name="GetLan"> set to true if want to get local(LAN) Connected ip address</param>
//        /// <returns></returns>
//        //public static string GetVisitorIPAddress(bool GetLan = false)
//        //{
//        //    string visitorIPAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

//        //    if (string.IsNullOrEmpty(visitorIPAddress))
//        //        visitorIPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

//        //    if (string.IsNullOrEmpty(visitorIPAddress))
//        //        visitorIPAddress = HttpContext.Current.Request.UserHostAddress;

//        //    if (string.IsNullOrEmpty(visitorIPAddress) || visitorIPAddress.Trim() == "::1")
//        //    {
//        //        GetLan = true;
//        //        visitorIPAddress = string.Empty;
//        //    }

//        //    if (GetLan)
//        //    {
//        //        if (string.IsNullOrEmpty(visitorIPAddress))
//        //        {
//        //            //This is for Local(LAN) Connected ID Address
//        //            string stringHostName = Dns.GetHostName();
//        //            //Get Ip Host Entry
//        //            IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
//        //            //Get Ip Address From The Ip Host Entry Address List
//        //            IPAddress[] arrIpAddress = ipHostEntries.AddressList;

//        //            try
//        //            {
//        //                visitorIPAddress = arrIpAddress[arrIpAddress.Length - 2].ToString();
//        //            }
//        //            catch
//        //            {
//        //                try
//        //                {
//        //                    visitorIPAddress = arrIpAddress[0].ToString();
//        //                }
//        //                catch
//        //                {
//        //                    try
//        //                    {
//        //                        arrIpAddress = Dns.GetHostAddresses(stringHostName);
//        //                        visitorIPAddress = arrIpAddress[0].ToString();
//        //                    }
//        //                    catch
//        //                    {
//        //                        visitorIPAddress = "127.0.0.1";
//        //                    }
//        //                }
//        //            }
//        //        }
//        //    }
//        //    return visitorIPAddress;
//        //}


//        //public static string GetReverseDns(string VisitorIPAddress)
//        //{
//        //    return System.Net.Dns.GetHostEntry(VisitorIPAddress).HostName.ToString();
//        //}

//        public static string GetVisitorIPAddress(bool GetLan = false)
//        {
//            string visitorIPAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

//            if (string.IsNullOrEmpty(visitorIPAddress))
//                visitorIPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

//            if (string.IsNullOrEmpty(visitorIPAddress))
//                visitorIPAddress = HttpContext.Current.Request.UserHostAddress;

//            if (string.IsNullOrEmpty(visitorIPAddress) || visitorIPAddress.Trim() == "::1")
//            {
//                GetLan = true;
//                visitorIPAddress = string.Empty;
//            }

//            if (GetLan)
//            {
//                if (string.IsNullOrEmpty(visitorIPAddress))
//                {
//                    //This is for Local(LAN) Connected ID Address
//                    string stringHostName = Dns.GetHostName();
//                    //Get Ip Host Entry
//                    IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
//                    //Get Ip Address From The Ip Host Entry Address List
//                    IPAddress[] arrIpAddress = ipHostEntries.AddressList;

//                    try
//                    {
//                        visitorIPAddress = arrIpAddress[arrIpAddress.Length - 2].ToString();
//                    }
//                    catch
//                    {
//                        try
//                        {
//                            visitorIPAddress = arrIpAddress[0].ToString();
//                        }
//                        catch
//                        {
//                            try
//                            {
//                                arrIpAddress = Dns.GetHostAddresses(stringHostName);
//                                visitorIPAddress = arrIpAddress[0].ToString();
//                            }
//                            catch
//                            {
//                                visitorIPAddress = "127.0.0.1";
//                            }
//                        }
//                    }
//                }
//            }
//            MyGlobalVariables.GetIpAddress = visitorIPAddress;
//            return visitorIPAddress;
//        }
//        public static string GetReverseDns(string VisitorIPAddress)
//        {
//            try
//            {
//                MyGlobalVariables.GetReverseDns = System.Net.Dns.GetHostEntry(VisitorIPAddress).HostName.ToString();
//            }
//            catch
//            {
//                MyGlobalVariables.GetReverseDns = "0";
//            }
//            return MyGlobalVariables.GetReverseDns;

//        }

//    }
//}


