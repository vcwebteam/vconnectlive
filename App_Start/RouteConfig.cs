﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//using System.Web.Routing;
////test
//namespace Vconnect
//{
//    public static class RoutingUtility
//    {
//        #region Home
//        public static string GetHomeURL(string location)
//        {
//            if (string.IsNullOrEmpty(location)) return string.Empty;

//            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location.ToLower() }, };

//            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "Default2", parameters);

//            return entity.VirtualPath;
//        }
//        #endregion
//        #region Search Urls
//        public static string GetSearchURLNew(string location, string tags)
//        {
//            if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;

//            RouteValueDictionary parameters = new RouteValueDictionary { { "sq", tags.ToLower() }, { "sl", location.ToLower().Replace("_", "-") } };

//            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "SearchNew", parameters);

//            return entity.VirtualPath;
//        }
//        public static string GetSearchURLNew(string location, string tags, string page)
//        {
//            //Regex regex = new Regex("^[0-9]+$");

//            //if (!regex.IsMatch(page)) return string.Empty;
//            if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;
//            RouteValueDictionary parameters = new RouteValueDictionary { { "sq", tags }, { "sl", location.ToLower().Replace("_", "-") }, { "pg", page } };

//            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "SearchNew", parameters);

//            return entity.VirtualPath;
//        }
//        public static string GetSearchURLold(string location, string tags)
//        {
//            if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;
//            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location.ToLower() }, { "tagname", tags.ToLower() } };

//            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "Searchold", parameters);

//            return entity.VirtualPath;
//        }
//        public static string GetSearchURLold(string location, string tags, string page)
//        {
//            if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;
//            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location }, { "tagname", tags }, { "pg", page } };

//            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "Searchpageold", parameters);

//            return entity.VirtualPath;
//        }
//        #endregion

//        #region Search webUrls
//        public static string GetSearchWebURLNew(string location, string tags)
//        {
//            if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;

//            RouteValueDictionary parameters = new RouteValueDictionary { { "sq", tags.ToLower() }, { "sl", location.ToLower() } };

//            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "WebSearchNew", parameters);

//            return entity.VirtualPath;
//        }
//        public static string GetSearchWebURLNew(string location, string tags, int Sort)
//        {
//            if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;

//            RouteValueDictionary parameters = new RouteValueDictionary { { "sq", tags.ToLower() }, { "sl", location.ToLower() } };

//            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "WebSearchNew", parameters);

//            return entity.VirtualPath;
//        }


//        public static string GetSearchLocWebURLNew(string location, string tags, int locid)
//        {
//            if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;

//            RouteValueDictionary parameters = new RouteValueDictionary { { "sq", tags.ToLower() }, { "sl", location.ToLower() }, { "sid", locid }, };

//            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "WebSearchNew", parameters);

//            return entity.VirtualPath;
//        }
//        public static string GetSearchWebURLNew(string location, string tags, string sct)
//        {
//            if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;

//            RouteValueDictionary parameters = new RouteValueDictionary { { "sq", tags.ToLower() }, { "sl", location.ToLower() }, { "sct", sct.ToLower() }, };

//            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "WebSearchNew", parameters);

//            return entity.VirtualPath;
//        }
//        //public static string GetSearchWebURLNew(string location, string tags, string sct, string locid)
//        //{
//        //    if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;

//        //    RouteValueDictionary parameters = new RouteValueDictionary { { "sq", tags.ToLower() }, { "sl", location.ToLower()}, { "sct", sct.ToLower() }, { "locid", locid.ToLower() }, };

//        //    VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "WebSearchNew", parameters);

//        //    return entity.VirtualPath;
//        //}

//        //public static string GetSearchLocWebURLNew(string location, string tags, int locid)
//        //{
//        //    if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;

//        //    RouteValueDictionary parameters = new RouteValueDictionary { { "sq", tags.ToLower() }, { "sl", location.ToLower() }, { "sid", locid }, };

//        //    VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "WebSearchNew", parameters);

//        //    return entity.VirtualPath;
//        //}
//        public static string GetWebSearchURLold(string location, string tags)
//        {
//            if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;
//            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location.ToLower() }, { "tagname", tags.ToLower() } };

//            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "WebSearchold", parameters);

//            return entity.VirtualPath;
//        }
//        public static string GetWebSearchURLold(string location, string tags, string page)
//        {
//            if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;
//            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location }, { "tagname", tags }, { "pg", page } };

//            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "WebSearchpageold", parameters);

//            return entity.VirtualPath;
//        }
//        #endregion
//        #region BrowseByCatBiz
//        public static string GetCatSearch(string location, string tags)
//        {
//            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location }, { "tagname", tags } };
//            //add parameters in any order, GetVirtualPath method arrange it in order as per it define for route name "Search"
//            //in global.asax
//            //get the URL as per the rules in the RoutingTable
//            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "browsebycat", parameters);

//            return entity.VirtualPath;
//        }
//        public static string GetCatSearch(string location, string tags, int page)
//        {
//            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location }, { "tagname", tags }, { "page", page } };
//            //add parameters in any order, GetVirtualPath method arrange it in order as per it define for route name "Search"
//            //in global.asax
//            //get the URL as per the rules in the RoutingTable
//            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "browsebycatpg", parameters);

//            return entity.VirtualPath;
//        }
//        public static string GetBizSearch(string location, string tags)
//        {
//            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location }, { "tagname", tags } };
//            //add parameters in any order, GetVirtualPath method arrange it in order as per it define for route name "Search"
//            //in global.asax
//            //get the URL as per the rules in the RoutingTable
//            VirtualPathData entity = null;

//            if (HttpContext.Current.Request.RawUrl.Contains("services"))
//                entity = RouteTable.Routes.GetVirtualPath(null, "browsebyservice", parameters);
//            else if (HttpContext.Current.Request.RawUrl.Contains("products"))
//                entity = RouteTable.Routes.GetVirtualPath(null, "browsebyproduct", parameters);
//            else if (HttpContext.Current.Request.RawUrl.Contains("brands"))
//                entity = RouteTable.Routes.GetVirtualPath(null, "browsebybrand", parameters);
//            else if (HttpContext.Current.Request.RawUrl.Contains("category"))
//                entity = RouteTable.Routes.GetVirtualPath(null, "browsebycat", parameters);
//            else //if (HttpContext.Current.Request.RawUrl.Contains("businesses"))
//                entity = RouteTable.Routes.GetVirtualPath(null, "browsebybiz", parameters);


//            return entity.VirtualPath;
//        }
//        public static string GetBizSearch(string location, string tags, int page)
//        {
//            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location }, { "tagname", tags }, { "page", page } };
//            //add parameters in any order, GetVirtualPath method arrange it in order as per it define for route name "Search"
//            //in global.asax
//            //get the URL as per the rules in the RoutingTable
//            VirtualPathData entity = null;
//            if (HttpContext.Current.Request.RawUrl.Contains("services"))
//                entity = RouteTable.Routes.GetVirtualPath(null, "browsebyservicepg", parameters);
//            else if (HttpContext.Current.Request.RawUrl.Contains("products"))
//                entity = RouteTable.Routes.GetVirtualPath(null, "browsebyproductpg", parameters);
//            else if (HttpContext.Current.Request.RawUrl.Contains("brands"))
//                entity = RouteTable.Routes.GetVirtualPath(null, "browsebybrandpg", parameters);
//            else if (HttpContext.Current.Request.RawUrl.Contains("category"))
//                entity = RouteTable.Routes.GetVirtualPath(null, "browsebycatpg", parameters);
//            else //if (HttpContext.Current.Request.RawUrl.Contains("businesses"))
//                entity = RouteTable.Routes.GetVirtualPath(null, "browsebybizpg", parameters);
//            return entity.VirtualPath;
//        }
//        //public static string GetBizSearch(string location, string tags)
//        //{
//        //    RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location }, { "tagname", tags } };
//        //    //add parameters in any order, GetVirtualPath method arrange it in order as per it define for route name "Search"
//        //    //in global.asax
//        //    //get the URL as per the rules in the RoutingTable
//        //    VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "browsebybiz", parameters);

//        //    return entity.VirtualPath;
//        //}
//        //public static string GetBizSearch(string location, string tags, int page)
//        //{
//        //    RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location }, { "tagname", tags }, { "page", page } };
//        //    //add parameters in any order, GetVirtualPath method arrange it in order as per it define for route name "Search"
//        //    //in global.asax
//        //    //get the URL as per the rules in the RoutingTable
//        //    VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "browsebybizpg", parameters);

//        //    return entity.VirtualPath;
//        //}
//        public static string GetCatSearchlist(string location, string tags, int catid)
//        {
//            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location }, { "tagname", tags }, { "catid", catid } };
//            //add parameters in any order, GetVirtualPath method arrange it in order as per it define for route name "Search"
//            //in global.asax
//            //get the URL as per the rules in the RoutingTable
//            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "searchbycat", parameters);

//            return entity.VirtualPath;
//        }
//        public static string GetCatSearchlist(string location, string tags, int catid, int page)
//        {
//            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location }, { "tagname", tags }, { "catid", catid }, { "page", page } };
//            //add parameters in any order, GetVirtualPath method arrange it in order as per it define for route name "Search"
//            //in global.asax
//            //get the URL as per the rules in the RoutingTable
//            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "searchbycatpg", parameters);

//            return entity.VirtualPath;
//        }
//        #endregion
//        //public static string GetSearchWebURLNew(string location, string tags, string sct, string locid)
//        //{
//        //    if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;

//        //    RouteValueDictionary parameters = new RouteValueDictionary { { "sq", tags.ToLower() }, { "sl", location.ToLower().Replace("_", "-") }, { "sct", sct.ToLower() }, { "locid", locid.ToLower() }, };

//        //    VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "WebSearchNew", parameters);

//        //    return entity.VirtualPath;
//        //}
//        public static string GetQuickcategory(string location, int id, string keyword)
//        {
//            if (string.IsNullOrEmpty(id.ToString()) || string.IsNullOrEmpty(keyword) || string.IsNullOrEmpty(location)) return string.Empty;

//            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location.ToLower() }, { "id", id }, { "Keyword", keyword.ToLower() }, };
//            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "Quickcategory", parameters);
//            return entity.VirtualPath;
//        }

//        public static string GetClientAddURL(string issaved)
//        {
//            if (string.IsNullOrEmpty(issaved)) return string.Empty;
//            RouteValueDictionary parameters = new RouteValueDictionary { { "x", issaved } };

//            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "clientadds", parameters);

//            return entity.VirtualPath;
//        }
//    }
//    public class RouteConfig
//    {
//        public static void RegisterRoutes(RouteCollection routes)
//        {
//            routes.MapRoute(
//           name: "wblogin",
//           url: "wblogin",
//           defaults: new { controller = "Account", action = "WebLogin" }
//       );
//            routes.MapRoute(
//           name: "verification",
//           url: "verification",
//           defaults: new { controller = "Account", action = "generateloginidverification" }
//       );
//            routes.MapRoute(
//         name: "verify",
//         url: "verify",
//         defaults: new { controller = "Account", action = "verify" }
//     );
//            routes.MapRoute(
//          name: "NewPassword",
//          url: "newpassword",
//          defaults: new { controller = "Account", action = "NewPassword" }
//      );

//            routes.MapRoute(
//            name: "ForgotPassword",
//            url: "forgotpassword",
//            defaults: new { controller = "Account", action = "ForgotPassword" }
//        );

//            routes.MapRoute(
//            name: "generatenewemaillink",
//            url: "generatenewemaillink",
//            defaults: new { controller = "Account", action = "generatenewemaillink" }
//        );

//            routes.MapRoute(
//            name: "sendverification",
//            url: "sendverification",
//            defaults: new { controller = "Account", action = "sendverification" }
//            );
//            routes.MapRoute(
//            name: "externalloginconfirmation",
//            url: "externalloginconfirmation",
//            defaults: new { controller = "Account", action = "ExternalLoginConfirmation" }
//        );
//            routes.MapRoute(
//         name: "updateuseremail",
//         url: "updateuseremail",
//         defaults: new { controller = "Account", action = "UpdateUserEmail" }
//     );

           
//            routes.MapRoute("Default2", "{loc}.html", new { controller = "HomeWEB", action = "HomeWEB" });
//            #region TargetSms
//            routes.MapRoute(
//              name: "TargetedSms",
//              url: "targetedsms",
//              defaults: new { controller = "BotargetSms", action = "TargetedSms" }
//          );
//            routes.MapRoute(
//             name: "TargetedSmsBo",
//             url: "targetedsmsbo",
//             defaults: new { controller = "BotargetSms", action = "TargetedSmsBo" }
//         );
//            routes.MapRoute(
//             name: "TargetedSmsBoCategory",
//             url: "targetedsmsbocategory",
//             defaults: new { controller = "BotargetSms", action = "TargetedSmsBoCategory" }
//         );
//            routes.MapRoute(
//             name: "TargetedSmsBoLocation",
//             url: "targetedsmsbolocation",
//             defaults: new { controller = "BotargetSms", action = "TargetedSmsBoLocation" }
//         );
//            routes.MapRoute(
//             name: "TargetedSmsMyContact",
//             url: "targetedsmsmycontact",
//             defaults: new { controller = "BotargetSms", action = "TargetedSmsMyContact" }
//         );
//            routes.MapRoute(
//             name: "TargetedSmsVCuserAgegroup",
//             url: "targetedSmsvcuseragegroup",
//             defaults: new { controller = "BotargetSms", action = "TargetedSmsVCuserAgegroup" }
//         );
//            routes.MapRoute(
//            name: "TargetedSmsVCuserLocation",
//            url: "targetedsmsvcuserlocation",
//            defaults: new { controller = "BotargetSms", action = "TargetedSmsVCuserLocation" }
//        );
//            routes.MapRoute(
//            name: "TargetedSmsVCuserProfession",
//            url: "targetedsmsvcuserprofession",
//            defaults: new { controller = "BotargetSms", action = "TargetedSmsVCuserProfession" }
//        );
//            routes.MapRoute(
//            name: "TargetedSmsVCusers",
//            url: "targetedsmsvcusers",
//            defaults: new { controller = "BotargetSms", action = "TargetedSmsVCusers" }
//        );


//            #endregion
//            #region Login-Userreg

//            routes.MapRoute(
//               name: "Login",
//               url: "login",
//               defaults: new { controller = "Account", action = "Login" }
//           );

//            routes.MapRoute(
//             name: "userregistration",
//             url: "userregistration",
//             defaults: new { controller = "UserReg", action = "UserReg" }
//         );

//            routes.MapRoute(
//              name: "businessregistration",
//              url: "businessregistration",
//              defaults: new { controller = "bizReg", action = "bizReg" }
//          );
//            routes.MapRoute(
//            name: "Logout",
//            url: "logout",
//            defaults: new { controller = "Account", action = "LogOff" }
//        );
//            #endregion

//            #region stanbic IBTC
//            routes.MapRoute(
//            name: "Advertwithusibtc",
//            url: "advertisewithus",
//            defaults: new { controller = "Static", action = "Advertwithusibtc" }
//        );
//            routes.MapRoute(
//           name: "ibtcbank",
//           url: "register",
//           defaults: new { controller = "BizReg", action = "ibtcbank" }
//       );
//            routes.MapRoute(
//            name: "stanbicibtcbank",
//            url: "stanbic",
//            defaults: new { controller = "BizReg", action = "ibtcbank" }
//        );
//            #endregion

//            #region NIIT Scholarships

//            routes.MapRoute(
//          name: "niitthanks",
//          url: "thankyou",
//          defaults: new { controller = "BizReg", action = "niitscholarship" }
//      );
//            routes.MapRoute(
//           name: "niitscholarshipweb",
//          url: "niitscholarship",
//          defaults: new { controller = "BizReg", action = "niitscholarship", id = UrlParameter.Optional }
//      );
//            #endregion
//            #region Common

//            routes.MapRoute(
//           name: "Career",
//           url: "careers",
//           defaults: new { controller = "Static", action = "Career" }
//       );
//            routes.MapRoute(
//            name: "Sme_Event",
//            url: "sme_event",
//            defaults: new { controller = "Common", action = "sme_event" }
//        );
//            routes.MapRoute(
//            name: "smeeventII",
//            url: "smeeventii",
//            defaults: new { controller = "Common", action = "smeeventII" }
//        );
//            routes.MapRoute(
//            name: "ThankYou",
//            url: "thankyou",
//            defaults: new { controller = "Common", action = "thankyou" }
//        );
//            #endregion
//            routes.MapRoute(
//               name: "clientadds",
//              url: "naijafoodfiesta",
//              defaults: new { controller = "ClientAdds", action = "naijafoodfiesta", id = UrlParameter.Optional }
//          );
//            #region static
//            //routes.MapRoute(

//            //   name: "defaultaspx",

//            //   url: "default.aspx",

//            //   defaults: new { controller = "HomeWEB", action = "HomeWEB" }

//            //);

//            //routes.MapRoute(

//            // name: "defaultstaticaspx",

//            // url: "pages/static/{name}.aspx",

//            //defaults: new { controller = "HomeWEB", action = "HomeWEB" }

//            //  );



//            //routes.MapRoute(

//            // name: "defaultcontentaspx",

//            // url: "content/{name}.aspx",

//            //defaults: new { controller = "HomeWEB", action = "HomeWEB" }

//            //  );



//            //routes.MapRoute(

//            // name: "contentfaq",

//            // url: "content/faq",

//            //defaults: new { controller = "Static", action = "FAQs" }

//            //  );



//            //routes.MapRoute(

//            //   name: "contentreachus",

//            //   url: "content/reachus",

//            //  defaults: new { controller = "Static", action = "Contactus" }

//            //    );



//            //routes.MapRoute(

//            //   name: "contentadvertise",

//            //   url: "content/advertise",

//            // defaults: new { controller = "Static", action = "Advertize" }

//            //    );



//            routes.MapRoute(

//  name: "VconnectApp",

//  url: "apps",

//  defaults: new { controller = "Static", action = "Product", id = UrlParameter.Optional }

//);
//            routes.MapRoute(
//            name: "PageNotFound",
//            url: "404",
//            defaults: new { controller = "Static", action = "PageNotFound", id = UrlParameter.Optional }
//   );

//            //browseby
//            routes.MapRoute(
//          name: "browsebybussiness",
//          url: "browsebybussiness",
//          defaults: new { controller = "BrowseByCatBiz", action = "index", tagname = "a", loc = "lagos" }
//      );
//            routes.MapRoute(
//          name: "browsebyservices",
//          url: "browsebyservices",
//          defaults: new { controller = "BrowseByCatBiz", action = "index", tagname = "a", loc = "lagos" }
//      );
//            routes.MapRoute(
//          name: "browsebycategories",
//          url: "browsebycategories",
//          defaults: new { controller = "BrowseByCatBiz", action = "index", tagname = "a", loc = "lagos" }
//      );
//            routes.MapRoute(
//          name: "browsebyproducts",
//          url: "browsebyproducts",
//          defaults: new { controller = "BrowseByCatBiz", action = "index", tagname = "a", loc = "lagos" }
//      );
//            //advertwithus
//            routes.MapRoute(
//            name: "AdvertWithUs",
//            url: "advertwithus",
//            defaults: new { controller = "Static", action = "Advertwithus" }
//        );

//            //advertise
//            routes.MapRoute(
//            name: "Advertise",
//            url: "advertise",
//            defaults: new { controller = "Static", action = "Advertize" }
//        );

//            //PressRelease
//            routes.MapRoute(
//            name: "PressRelease",
//            url: "pressrelease",
//            defaults: new { controller = "Static", action = "PressRelease" }
//        );

//            //advertise_two
//            routes.MapRoute(
//            name: "Promoteyourbusiness",
//            url: "promoteyourbusiness",
//            defaults: new { controller = "Static", action = "Promoteyourbusiness" }
//        );
//            //contact
//            routes.MapRoute(
//            name: "Contact",
//            url: "contact",
//            defaults: new { controller = "Static", action = "Contact" }
//        );
//            //index
//            routes.MapRoute(
//            name: "Index",
//            url: "index",
//            defaults: new { controller = "Static", action = "Index" }
//        );
//            //learn_claim
//            routes.MapRoute(
//            name: "Learn_Claim",
//            url: "learn_claim",
//            defaults: new { controller = "Static", action = "Learn_Claim" }
//        );
//            //privacy
//            routes.MapRoute(
//            name: "Privacy",
//            url: "privacy",
//            defaults: new { controller = "Static", action = "Privacy" }
//        );

//            //DGO
//            routes.MapRoute(
//              name: "DGO",
//              url: "vc-agent",
//              defaults: new { controller = "Static", action = "DGO", id = UrlParameter.Optional }
//     );
//            routes.MapRoute(
//            name: "BusinessSolutions",
//            url: "businesssolutions",
//            defaults: new { controller = "Static", action = "BusinessSolutions" }
//        );
//            routes.MapRoute(
//            name: "Feedback",
//            url: "feedback",
//            defaults: new { controller = "Static", action = "Feedback", id = UrlParameter.Optional }
//   );
//            routes.MapRoute(
//          name: "linkredirect",
//          url: "linkredirect",
//          defaults: new { controller = "Static", action = "bannerclick" }
//      );

//            //            routes.MapRoute(
//            //        name: "VconnectOnYourMobile",
//            //        url: "vc-mobile",
//            //        defaults: new { controller = "Static", action = "Product", id = UrlParameter.Optional }
//            //);

//            routes.MapRoute(
//            name: "ClaimYourBusiness",
//            url: "claimyourbusiness",
//            defaults: new { controller = "Static", action = "LearnClaim" }
//        );

//            routes.MapRoute(
//                name: "searchnotfound",
//                url: "searchnotfound",
//                defaults: new { controller = "SearchListWEB", action = "SearchNotFound", id = UrlParameter.Optional }
//       );
         
//            routes.MapRoute(
//               name: "Faq",
//               url: "faq",
//               defaults: new { controller = "Static", action = "FAQs", id = UrlParameter.Optional }
//      );
//            routes.MapRoute(
//              name: "Contactus",
//              url: "reachus",
//              defaults: new { controller = "Static", action = "Contactus", id = UrlParameter.Optional }
//     );

//            routes.MapRoute(
//            name: "Sales",
//            url: "sales",
//            defaults: new { controller = "Static", action = "Contactus", id = UrlParameter.Optional }
//   );
//            routes.MapRoute(
//              name: "SmeEvent",
//              url: "smeevent",
//              defaults: new { controller = "Static", action = "smeevent", id = UrlParameter.Optional }
//     );
//            routes.MapRoute(
//              name: "AboutVconnect",
//              url: "about_vconnect",
//              defaults: new { controller = "Static", action = "About", id = UrlParameter.Optional }
//     );
//            routes.MapRoute(
//                       name: "Terms",
//                       url: "terms",
//                       defaults: new { controller = "UserReg", action = "TermsCondition" }
//                   );
//            routes.MapRoute(
//           name: "PrivacyPolicy",
//           url: "privacypolicy",
//           defaults: new { controller = "Static", action = "privacypolicy" }
//       );
//            #endregion
//            #region CYB
//            routes.MapRoute(
//             name: "CYBWEB",
//             url: "claimbusiness",
//             defaults: new { controller = "ClaimBusiness", action = "SearchCYBDetails" }
//         );

//            routes.MapRoute(
//            name: "CYBSendCodeWEB",
//            url: "claimbusinesssencode",
//            defaults: new { controller = "ClaimBusiness", action = "SendCYBCodeNew" }
//        );
//            routes.MapRoute(
//           name: "CYBReportIncorrect",
//           url: "CYBRepotIncorrect",
//           defaults: new { controller = "ClaimBusiness", action = "CYBReportIncorrectComments" }
//       );
//            #endregion

//            #region BO DASHboard WAP
//            routes.MapRoute(
//          name: "BODashboardWAP",
//          url: "mybusinesss",
//          defaults: new { controller = "BoDashboardWap", action = "GetClimedBusiness" }
//      );

//            routes.MapRoute(
//         name: "BODashboardWEB",
//         url: "mybusiness",
//         defaults: new { controller = "BoDashboardWEB", action = "Index" }
//     );
//            #endregion
//            #region Quickcategory
//            // For WAP Index page setting
//            routes.MapRoute(
//                name: "Quickcategory",
//             url: "{loc}/{Keyword}_cl{id}",
//               constraints: new { id = @"^[0-9]*$" },
//                defaults: new { controller = "Categorylist", action = "Itemlistaction" }
//            );
//            #endregion
//            #region Catserlisting


//            routes.MapRoute(
//           name: "searchbycatpg",
//           url: "{loc}/list-of-{tagname}_c{catid}-page{page}",
//           constraints: new { page = @"^[0-9]*$" },
//           defaults: new { controller = "SearchListWEB", action = "SearchWEB", Sort = UrlParameter.Optional }
//          );
//            routes.MapRoute(
//              name: "searchbycat",
//              url: "{loc}/list-of-{tagname}_c{catid}/{Sort}",
//              defaults: new { controller = "SearchListWEB", action = "SearchWEB", Sort = UrlParameter.Optional }
//             );
//            routes.MapRoute(
//            name: "searchbysevpg",
//            url: "{loc}/list-of-{tagname}-vendors-search_s{catid}-page{page}",
//            constraints: new { page = @"^[0-9]*$" },
//            defaults: new { controller = "SearchListWEB", action = "SearchWEB", Sort = UrlParameter.Optional }
//            );
//            routes.MapRoute(
//            name: "searchbysev",
//            url: "{loc}/list-of-{tagname}-vendors-search_s{catid}/{Sort}",
//            defaults: new { controller = "SearchListWEB", action = "SearchWEB", Sort = UrlParameter.Optional }
//            );
//            routes.MapRoute(
//            name: "searchbyprdpg",
//            url: "{loc}/list-of-{tagname}-vendors-search_p{catid}-page{page}",
//            constraints: new { page = @"^[0-9]*$" },
//            defaults: new { controller = "SearchListWEB", action = "SearchWEB", Sort = UrlParameter.Optional }
//            );
//            routes.MapRoute(
//              name: "searchbyprd",
//              url: "{loc}/list-of-{tagname}-vendors-search_p{catid}/{Sort}",
//              defaults: new { controller = "SearchListWEB", action = "SearchWEB", Sort = UrlParameter.Optional }
//             );

//            routes.MapRoute(
//        name: "searchbybrpg",
//        url: "{loc}/list-of-{tagname}-search_br{catid}-page{page}",
//        constraints: new { page = @"^[0-9]*$" },
//        defaults: new { controller = "SearchListWEB", action = "SearchWEB", Sort = UrlParameter.Optional }
//       );
//            routes.MapRoute(
//              name: "searchbybr",
//              url: "{loc}/list-of-{tagname}-search_br{catid}/{Sort}",
//              defaults: new { controller = "SearchListWEB", action = "SearchWEB", Sort = UrlParameter.Optional }
//             );
//            #endregion
//            #region BrowseByCatBiz
//            routes.MapRoute(
//             name: "browsebycatpg",
//             url: "{loc}/categories-from-{tagname}-page{page}",
//                //constraints: new { page = @"^[0-9]*$" },
//             defaults: new { controller = "BrowseByCatBiz", action = "Index" }
//            );
//            routes.MapRoute(
//               name: "browsebycat",
//               url: "{loc}/categories-from-{tagname}",
//               defaults: new { controller = "BrowseByCatBiz", action = "Index" }
//              );


//            routes.MapRoute(
//             name: "browsebybizpg",
//             url: "{loc}/businesses-from-{tagname}-page{page}",
//                //constraints: new { page = @"^[0-9]*$" },
//             defaults: new { controller = "BrowseByCatBiz", action = "Index" }
//            );
//            routes.MapRoute(
//            name: "browsebybiz",
//            url: "{loc}/businesses-from-{tagname}",
//            defaults: new { controller = "BrowseByCatBiz", action = "Index" }
//             );
//            routes.MapRoute(
//              name: "bizdetailrv3",
//              url: "{businessname}/{bizID}/{id}",
//              constraints: new { bizID = @"^[0-9]*$" },
//              defaults: new { controller = "BizDetail", action = "BizDetailrv", id = UrlParameter.Optional }
//               );
//            routes.MapRoute(
//           name: "bizdetail",
//           url: "{businessname}_b{bizID}",
//           constraints: new { bizID = @"^[0-9]*$" },
//           defaults: new { controller = "BizDetail", action = "BizDetail" }
//            );
//            routes.MapRoute(
//          name: "bizdetailstate",
//          url: "{state}/{businessname}_b{bizID}",
//          constraints: new { bizID = @"^[0-9]*$" },
//          defaults: new { controller = "Static", action = "PageNotFound" }
//           );
//       //     routes.MapRoute(
//       //name: "bizdetail2",
//       //url: "{businessname}_pg{bizID}",
//       //constraints: new { bizID = @"^[0-9]*$" },
//       //defaults: new { controller = "BizDetail", action = "BizDetail" }
//       // );
//            routes.MapRoute(
//            name: "searchbybiz",
//            url: "{businessname}-{state}_b{businessid}",
//            defaults: new { controller = "BrowseByCatBiz", action = "Index", id = UrlParameter.Optional }
//             );

//            routes.MapRoute(
//     name: "browsebybrandpg",
//     url: "{loc}/brands-from-{tagname}-page{page}",
//                //constraints: new { page = @"^[0-9]*$" },
//     defaults: new { controller = "BrowseByCatBiz", action = "Index" }
//    );
//            routes.MapRoute(
//               name: "browsebybrand",
//               url: "{loc}/brands-from-{tagname}",
//               defaults: new { controller = "BrowseByCatBiz", action = "Index" }
//              );
//            routes.MapRoute(
//        name: "browsebybrands",
//        url: "browsebybrands",
//        defaults: new { controller = "BrowseByCatBiz", action = "index", tagname = "a", loc = "lagos" }
//    );
//            routes.MapRoute(
//             name: "browsebyproductpg",
//             url: "{loc}/products-from-{tagname}-page{page}",
//                //constraints: new { page = @"^[0-9]*$" },
//             defaults: new { controller = "BrowseByCatBiz", action = "Index" }
//            );
//            routes.MapRoute(
//               name: "browsebyproduct",
//               url: "{loc}/products-from-{tagname}",
//               defaults: new { controller = "BrowseByCatBiz", action = "Index" }
//              );

//            routes.MapRoute(
//             name: "browsebyservicepg",
//             url: "{loc}/services-from-{tagname}-page{page}",
//                //constraints: new { page = @"^[0-9]*$" },
//             defaults: new { controller = "BrowseByCatBiz", action = "Index" }
//            );
//            routes.MapRoute(
//               name: "browsebyservice",
//               url: "{loc}/services-from-{tagname}",
//               defaults: new { controller = "BrowseByCatBiz", action = "Index" }
//              );


//            #endregion
//            #region Search For WapWeb

//            routes.MapRoute(
//                name: "SearchNew",
//                url: "qsearch",
//                defaults: new { controller = "SearchListWEB", action = "SearchWEB", id = UrlParameter.Optional }
//            );

//            routes.MapRoute(
//               name: "Searchold",
//               url: "{loc}/list-of-{tagname}-qsearch",
//               defaults: new { controller = "SearchListWEB", action = "SearchWEB", id = UrlParameter.Optional }
//           );
//            routes.MapRoute(
//               name: "Searchpageold",
//               url: "{loc}/list-of-{tagname}-qsearch-page{page}",
//              constraints: new { page = @"^[0-9]*$" },
//               defaults: new { controller = "SearchListWEB", action = "SearchWEB", id = UrlParameter.Optional }
//           );


//            #endregion

//            #region business detail
//            routes.MapRoute(
//         name: "bizdetailpgphoto",
//         url: "photogallery_pg{bizID}",
//         constraints: new { bizID = @"^[0-9]*$" },
//         defaults: new { controller = "BizDetail", action = "GetBizPhotosAttachments", module = "photo" }
//          );
//            routes.MapRoute(
//           name: "bizdetailpgattachment",
//           url: "attachmentgallery_pg{bizID}",
//           constraints: new { bizID = @"^[0-9]*$" },
//           defaults: new { controller = "BizDetail", action = "GetBizPhotosAttachments", module = "attachment" }
//            );
//            routes.MapRoute(
//          name: "bizdetailpgvideo",
//          url: "videogallery_pg{bizID}",
//          constraints: new { bizID = @"^[0-9]*$" },
//          defaults: new { controller = "BizDetail", action = "BizDetail" }
//           );

//            routes.MapRoute(
//                 name: "GetBizPhotosAttachments",
//                url: "displayphotosbiz",

//                defaults: new { controller = "BizDetail", action = "GetBizPhotosAttachments", id = UrlParameter.Optional }
//            );

//            routes.MapRoute(
//                      name: "GetDirection",
//                     url: "getdirection",

//                     defaults: new { controller = "BizDetail", action = "GetDirection", id = UrlParameter.Optional }
//                 );
//            #endregion
//            routes.MapRoute(
//                name: "WebSearchNew",
//               url: "qsearch",

//               defaults: new { controller = "SearchListWEB", action = "SearchWEB", id = UrlParameter.Optional }
//           );

//            routes.MapRoute(
//               name: "WebSearchold",
//               url: "{loc}/list-of-{tagname}-qsearch",
//               defaults: new { controller = "SearchListWEB", action = "SearchWEB", id = UrlParameter.Optional }
//           );
//            routes.MapRoute(
//               name: "WebSearchpageold",
//               url: "{loc}/list-of-{tagname}-qsearch-page{page}",
//              constraints: new { page = @"^[0-9]*$" },
//               defaults: new { controller = "SearchListWEB", action = "SearchWEB", id = UrlParameter.Optional }
//           );
//            //region userdashbaord
//         //   routes.MapRoute(
//         //name: "userDashbaordWeb",
//         //url: "users/myaccount",
//         //defaults: new { controller = "UserDashboardWEB", action = "UserDetailsWeb" }
//         // );
//            routes.MapRoute(
//      name: "userDashbaordWeb1",
//      url: "users/myaccount",
//      defaults: new { controller = "UserDashboardWEB", action = "UserDetailsWeb" }
//       );
//            routes.MapRoute(
//      name: "notifications",
//      url: "users/notifications",
//      defaults: new { controller = "UserDashboardWEB", action = "notifications" }
//       );
//            routes.MapRoute(
//      name: "socialactivity",
//      url: "users/socialactivity",
//      defaults: new { controller = "UserDashboardWEB", action = "socialactivity" }
//       );
//            routes.MapRoute(
//        name: "userDashbaord",
//        url: "users/myaccountwap",
//        defaults: new { controller = "UserDashboardWap", action = "userDbWap" }
//         );

//            routes.MapRoute(
//      name: "username",
//      url: "{username}",
//      defaults: new { controller = "UserDashboardWEB", action = "usersinfo" }
//       );
         

//            //For WEB Index page setting
           

//            #region QuickSms

//            routes.MapRoute(
//             name: "QuickSms",
//             url: "QuickSms",
//             defaults: new { controller = "SMSConnect", action = "Quicksms" }
//         );


//            #endregion
           
        
//            routes.MapRoute(
//              name: "Default1",
//              url: "{controller}/{action}/{id}",
//              defaults: new { id = UrlParameter.Optional }
//          );

//            routes.MapRoute(
//                      name: "Default",
//                      url: "",
//                      defaults: new { controller = "HomeWEB", action = "HomeWEB" }
//                  );
          
//        }


//    }
//}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
//test
namespace Vconnect
{
    public static class RoutingUtility
    {
        #region Home
        public static string GetHomeURL(string location)
        {
            if (string.IsNullOrEmpty(location)) return string.Empty;

            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location.ToLower() }, };

            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "Default2", parameters);

            return entity.VirtualPath;
        }
        #endregion
        #region Search Urls
        public static string GetSearchURLNew(string location, string tags)
        {
            if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;

            RouteValueDictionary parameters = new RouteValueDictionary { { "sq", tags.ToLower() }, { "sl", location.ToLower().Replace("_", "-") } };

            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "SearchNew", parameters);

            return entity.VirtualPath;
        }
        public static string GetSearchURLNew(string location, string tags, string page)
        {
            //Regex regex = new Regex("^[0-9]+$");

            //if (!regex.IsMatch(page)) return string.Empty;
            if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;
            RouteValueDictionary parameters = new RouteValueDictionary { { "sq", tags }, { "sl", location.ToLower().Replace("_", "-") }, { "pg", page } };

            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "SearchNew", parameters);

            return entity.VirtualPath;
        }
        public static string GetSearchURLold(string location, string tags)
        {
            if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;
            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location.ToLower() }, { "tagname", tags.ToLower() } };

            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "Searchold", parameters);

            return entity.VirtualPath;
        }
        public static string GetSearchURLold(string location, string tags, string page)
        {
            if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;
            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location }, { "tagname", tags }, { "pg", page } };

            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "Searchpageold", parameters);

            return entity.VirtualPath;
        }
        #endregion

        #region Search webUrls
        public static string GetSearchWebURLNew(string location, string tags)
        {
            if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;

            RouteValueDictionary parameters = new RouteValueDictionary { { "sq", tags.ToLower() }, { "sl", location.ToLower() } };

            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "WebSearchNew", parameters);

            return entity.VirtualPath;
        }
        public static string GetSearchWebURLNew(string location, string tags, int Sort)
        {
            if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;

            RouteValueDictionary parameters = new RouteValueDictionary { { "sq", tags.ToLower() }, { "sl", location.ToLower() } };

            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "WebSearchNew", parameters);

            return entity.VirtualPath;
        }


        public static string GetSearchLocWebURLNew(string location, string tags, int locid)
        {
            if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;

            RouteValueDictionary parameters = new RouteValueDictionary { { "sq", tags.ToLower() }, { "sl", location.ToLower() }, { "sid", locid }, };

            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "WebSearchNew", parameters);

            return entity.VirtualPath;
        }
        public static string GetSearchWebURLNew(string location, string tags, string sct)
        {
            if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;

            RouteValueDictionary parameters = new RouteValueDictionary { { "sq", tags.ToLower() }, { "sl", location.ToLower() }, { "sct", sct.ToLower() }, };

            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "WebSearchNew", parameters);

            return entity.VirtualPath;
        }
        //public static string GetSearchWebURLNew(string location, string tags, string sct, string locid)
        //{
        //    if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;

        //    RouteValueDictionary parameters = new RouteValueDictionary { { "sq", tags.ToLower() }, { "sl", location.ToLower()}, { "sct", sct.ToLower() }, { "locid", locid.ToLower() }, };

        //    VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "WebSearchNew", parameters);

        //    return entity.VirtualPath;
        //}

        //public static string GetSearchLocWebURLNew(string location, string tags, int locid)
        //{
        //    if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;

        //    RouteValueDictionary parameters = new RouteValueDictionary { { "sq", tags.ToLower() }, { "sl", location.ToLower() }, { "sid", locid }, };

        //    VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "WebSearchNew", parameters);

        //    return entity.VirtualPath;
        //}
        public static string GetWebSearchURLold(string location, string tags)
        {
            if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;
            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location.ToLower() }, { "tagname", tags.ToLower() } };

            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "WebSearchold", parameters);

            return entity.VirtualPath;
        }
        public static string GetWebSearchURLold(string location, string tags, string page)
        {
            if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;
            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location }, { "tagname", tags }, { "pg", page } };

            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "WebSearchpageold", parameters);

            return entity.VirtualPath;
        }
        #endregion
        #region BrowseByCatBiz
        public static string GetCatSearch(string location, string tags)
        {
            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location }, { "tagname", tags } };
            //add parameters in any order, GetVirtualPath method arrange it in order as per it define for route name "Search"
            //in global.asax
            //get the URL as per the rules in the RoutingTable
            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "browsebycat", parameters);

            return entity.VirtualPath;
        }
        public static string GetCatSearch(string location, string tags, int page)
        {
            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location }, { "tagname", tags }, { "page", page } };
            //add parameters in any order, GetVirtualPath method arrange it in order as per it define for route name "Search"
            //in global.asax
            //get the URL as per the rules in the RoutingTable
            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "browsebycatpg", parameters);

            return entity.VirtualPath;
        }
        public static string GetBizSearch(string location, string tags)
        {
            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location }, { "tagname", tags } };
            //add parameters in any order, GetVirtualPath method arrange it in order as per it define for route name "Search"
            //in global.asax
            //get the URL as per the rules in the RoutingTable
            VirtualPathData entity = null;

            if (HttpContext.Current.Request.RawUrl.Contains("services"))
                entity = RouteTable.Routes.GetVirtualPath(null, "browsebyservice", parameters);
            else if (HttpContext.Current.Request.RawUrl.Contains("products"))
                entity = RouteTable.Routes.GetVirtualPath(null, "browsebyproduct", parameters);
            else if (HttpContext.Current.Request.RawUrl.Contains("brands"))
                entity = RouteTable.Routes.GetVirtualPath(null, "browsebybrand", parameters);
            else if (HttpContext.Current.Request.RawUrl.Contains("category"))
                entity = RouteTable.Routes.GetVirtualPath(null, "browsebycat", parameters);
            else //if (HttpContext.Current.Request.RawUrl.Contains("businesses"))
                entity = RouteTable.Routes.GetVirtualPath(null, "browsebybiz", parameters);


            return entity.VirtualPath;
        }
        public static string GetBizSearch(string location, string tags, int page)
        {
            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location }, { "tagname", tags }, { "page", page } };
            //add parameters in any order, GetVirtualPath method arrange it in order as per it define for route name "Search"
            //in global.asax
            //get the URL as per the rules in the RoutingTable
            VirtualPathData entity = null;
            if (HttpContext.Current.Request.RawUrl.Contains("services"))
                entity = RouteTable.Routes.GetVirtualPath(null, "browsebyservicepg", parameters);
            else if (HttpContext.Current.Request.RawUrl.Contains("products"))
                entity = RouteTable.Routes.GetVirtualPath(null, "browsebyproductpg", parameters);
            else if (HttpContext.Current.Request.RawUrl.Contains("brands"))
                entity = RouteTable.Routes.GetVirtualPath(null, "browsebybrandpg", parameters);
            else if (HttpContext.Current.Request.RawUrl.Contains("category"))
                entity = RouteTable.Routes.GetVirtualPath(null, "browsebycatpg", parameters);
            else //if (HttpContext.Current.Request.RawUrl.Contains("businesses"))
                entity = RouteTable.Routes.GetVirtualPath(null, "browsebybizpg", parameters);
            return entity.VirtualPath;
        }
        //public static string GetBizSearch(string location, string tags)
        //{
        //    RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location }, { "tagname", tags } };
        //    //add parameters in any order, GetVirtualPath method arrange it in order as per it define for route name "Search"
        //    //in global.asax
        //    //get the URL as per the rules in the RoutingTable
        //    VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "browsebybiz", parameters);

        //    return entity.VirtualPath;
        //}
        //public static string GetBizSearch(string location, string tags, int page)
        //{
        //    RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location }, { "tagname", tags }, { "page", page } };
        //    //add parameters in any order, GetVirtualPath method arrange it in order as per it define for route name "Search"
        //    //in global.asax
        //    //get the URL as per the rules in the RoutingTable
        //    VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "browsebybizpg", parameters);

        //    return entity.VirtualPath;
        //}
        public static string GetCatSearchlist(string location, string tags, int catid)
        {
            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location }, { "tagname", tags }, { "catid", catid } };
            //add parameters in any order, GetVirtualPath method arrange it in order as per it define for route name "Search"
            //in global.asax
            //get the URL as per the rules in the RoutingTable
            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "searchbycat", parameters);

            return entity.VirtualPath;
        }
        public static string GetCatSearchlist(string location, string tags, int catid, int page)
        {
            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location }, { "tagname", tags }, { "catid", catid }, { "page", page } };
            //add parameters in any order, GetVirtualPath method arrange it in order as per it define for route name "Search"
            //in global.asax
            //get the URL as per the rules in the RoutingTable
            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "searchbycatpg", parameters);

            return entity.VirtualPath;
        }
        #endregion
        //public static string GetSearchWebURLNew(string location, string tags, string sct, string locid)
        //{
        //    if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(tags)) return string.Empty;

        //    RouteValueDictionary parameters = new RouteValueDictionary { { "sq", tags.ToLower() }, { "sl", location.ToLower().Replace("_", "-") }, { "sct", sct.ToLower() }, { "locid", locid.ToLower() }, };

        //    VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "WebSearchNew", parameters);

        //    return entity.VirtualPath;
        //}
        public static string GetQuickcategory(string location, int id, string keyword)
        {
            if (string.IsNullOrEmpty(id.ToString()) || string.IsNullOrEmpty(keyword) || string.IsNullOrEmpty(location)) return string.Empty;

            RouteValueDictionary parameters = new RouteValueDictionary { { "loc", location.ToLower() }, { "id", id }, { "Keyword", keyword.ToLower() }, };
            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "Quickcategory", parameters);
            return entity.VirtualPath;
        }

        public static string GetClientAddURL(string issaved)
        {
            if (string.IsNullOrEmpty(issaved)) return string.Empty;
            RouteValueDictionary parameters = new RouteValueDictionary { { "x", issaved } };

            VirtualPathData entity = RouteTable.Routes.GetVirtualPath(null, "clientadds", parameters);

            return entity.VirtualPath;
        }
    }
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {

            routes.MapRoute(
       name: "usersettingsnotifications",
       url: "usersettingsnotifications",
       defaults: new { controller = "UserDashboardWEB", action = "EditProfile", user = UrlParameter.Optional }
        );

            routes.MapRoute(
                name: "userprofileurl",
                //url: "profile/{userid}",
                url: "user/{userid}",
                //constraints: new { profileid = @"^[a-zA-Z0-9.]*$" },
                defaults: new { controller = "UserDashboardWEB", action = "getUserDetails", userid = UrlParameter.Optional }
            );
             //Added by AB on 15/04/2015 for SKU Details page
            routes.MapRoute(
                  name: "SKUDetails",
                  url: "SKUDetails",
                defaults: new { controller = "SKUDetails", action = "SKUDetails", widgeturl = UrlParameter.Optional }
              );
            #region Widget
            routes.MapRoute(
                 name: "widgeturl",
                 url: "widget/{widgeturl}",
                            //constraints: new { profileid = @"^[a-zA-Z0-9.]*$" },
                 defaults: new { controller = "BizDetail", action = "catWidget", widgeturl = UrlParameter.Optional }
             );
            #endregion
            #region TargetSms
            routes.MapRoute(
              name: "TargetedSms",
              url: "targetedsms",
              defaults: new { controller = "BotargetSms", action = "TargetedSms" }
          );
            routes.MapRoute(
             name: "TargetedSmsBo",
             url: "targetedsmsbo",
             defaults: new { controller = "BotargetSms", action = "TargetedSmsBo" }
         );
            routes.MapRoute(
             name: "TargetedSmsBoCategory",
             url: "targetedsmsbocategory",
             defaults: new { controller = "BotargetSms", action = "TargetedSmsBoCategory" }
         );
            routes.MapRoute(
             name: "TargetedSmsBoLocation",
             url: "targetedsmsbolocation",
             defaults: new { controller = "BotargetSms", action = "TargetedSmsBoLocation" }
         );
            routes.MapRoute(
             name: "TargetedSmsMyContact",
             url: "targetedsmsmycontact",
             defaults: new { controller = "BotargetSms", action = "TargetedSmsMyContact" }
         );
            routes.MapRoute(
             name: "TargetedSmsVCuserAgegroup",
             url: "targetedSmsvcuseragegroup",
             defaults: new { controller = "BotargetSms", action = "TargetedSmsVCuserAgegroup" }
         );
            routes.MapRoute(
            name: "TargetedSmsVCuserLocation",
            url: "targetedsmsvcuserlocation",
            defaults: new { controller = "BotargetSms", action = "TargetedSmsVCuserLocation" }
        );
            routes.MapRoute(
            name: "TargetedSmsVCuserProfession",
            url: "targetedsmsvcuserprofession",
            defaults: new { controller = "BotargetSms", action = "TargetedSmsVCuserProfession" }
        );
            routes.MapRoute(
            name: "TargetedSmsVCusers",
            url: "targetedsmsvcusers",
            defaults: new { controller = "BotargetSms", action = "TargetedSmsVCusers" }
        );


            #endregion
            #region Login-Userreg

            routes.MapRoute(
               name: "Login",
               url: "login",
               defaults: new { controller = "Account", action = "Login" }
           );

            routes.MapRoute(
             name: "userregistration",
             url: "userregistration",
             defaults: new { controller = "UserReg", action = "UserReg" }
         );

            routes.MapRoute(
              name: "businessregistration",
              url: "businessregistration",
              defaults: new { controller = "bizReg", action = "bizReg" }
          );
           
            #endregion

            #region stanbic IBTC
            routes.MapRoute(
            name: "Advertwithusibtc",
            url: "advertisewithus",
            defaults: new { controller = "Static", action = "Advertwithusibtc" }
        );
            routes.MapRoute(
           name: "ibtcbank",
           url: "register",
           defaults: new { controller = "BizReg", action = "ibtcbank" }
       );
            routes.MapRoute(
            name: "stanbicibtcbank",
            url: "stanbic",
            defaults: new { controller = "BizReg", action = "ibtcbank" }
        );
            #endregion

            #region NIIT Scholarships

            routes.MapRoute(
          name: "niitthanks",
          url: "thankyou",
          defaults: new { controller = "BizReg", action = "niitscholarship" }
      );
            routes.MapRoute(
           name: "niitscholarshipweb",
          url: "niitscholarship",
          defaults: new { controller = "BizReg", action = "niitscholarship", id = UrlParameter.Optional }
      );
            #endregion
            #region Common

            routes.MapRoute(
           name: "Career",
           url: "careers",
           defaults: new { controller = "Static", action = "Career" }
       );
            routes.MapRoute(
            name: "Sme_Event",
            url: "sme_event",
            defaults: new { controller = "Common", action = "sme_event" }
        );
            routes.MapRoute(
            name: "smeeventII",
            url: "smeeventii",
            defaults: new { controller = "Common", action = "smeeventII" }
        );
            routes.MapRoute(
            name: "ThankYou",
            url: "thankyou",
            defaults: new { controller = "Common", action = "thankyou" }
        );
            #endregion
            routes.MapRoute(
               name: "clientadds",
              url: "naijafoodfiesta",
              defaults: new { controller = "ClientAdds", action = "naijafoodfiesta", id = UrlParameter.Optional }
          );
            #region static
            //routes.MapRoute(

            //   name: "defaultaspx",

            //   url: "default.aspx",

            //   defaults: new { controller = "HomeWEB", action = "HomeWEB" }

            //);

            //routes.MapRoute(

            // name: "defaultstaticaspx",

            // url: "pages/static/{name}.aspx",

            //defaults: new { controller = "HomeWEB", action = "HomeWEB" }

            //  );



            //routes.MapRoute(

            // name: "defaultcontentaspx",

            // url: "content/{name}.aspx",

            //defaults: new { controller = "HomeWEB", action = "HomeWEB" }

            //  );



            //routes.MapRoute(

            // name: "contentfaq",

            // url: "content/faq",

            //defaults: new { controller = "Static", action = "FAQs" }

            //  );



            //routes.MapRoute(

            //   name: "contentreachus",

            //   url: "content/reachus",

            //  defaults: new { controller = "Static", action = "Contactus" }

            //    );



            //routes.MapRoute(

            //   name: "contentadvertise",

            //   url: "content/advertise",

            // defaults: new { controller = "Static", action = "Advertize" }

            //    );



            routes.MapRoute(

  name: "VconnectApp",

  url: "apps",

  defaults: new { controller = "Static", action = "Product", id = UrlParameter.Optional }

);

            //browseby
            routes.MapRoute(
          name: "browsebybussiness",
          url: "browsebybussiness",
          defaults: new { controller = "BrowseByCatBiz", action = "index", tagname = "a", loc = "lagos" }
      );
            routes.MapRoute(
          name: "browsebyservices",
          url: "browsebyservices",
          defaults: new { controller = "BrowseByCatBiz", action = "index", tagname = "a", loc = "lagos" }
      );
            routes.MapRoute(
          name: "browsebycategories",
          url: "browsebycategories",
          defaults: new { controller = "BrowseByCatBiz", action = "index", tagname = "a", loc = "lagos" }
      );
            routes.MapRoute(
          name: "browsebyproducts",
          url: "browsebyproducts",
          defaults: new { controller = "BrowseByCatBiz", action = "index", tagname = "a", loc = "lagos" }
      );
            //advertwithus
            routes.MapRoute(
            name: "AdvertWithUs",
            url: "advertwithus",
            defaults: new { controller = "Static", action = "Advertwithus" }
        );

            //advertise
            routes.MapRoute(
            name: "Advertise",
            url: "advertise",
            defaults: new { controller = "Static", action = "Advertize" }
        );

            //PressRelease
            routes.MapRoute(
            name: "PressRelease",
            url: "pressrelease",
            defaults: new { controller = "Static", action = "PressRelease" }
        );

            //advertise_two
            routes.MapRoute(
            name: "Promoteyourbusiness",
            url: "promoteyourbusiness",
            defaults: new { controller = "Static", action = "Promoteyourbusiness" }
        );
            //contact
            routes.MapRoute(
            name: "Contact",
            url: "contact",
            defaults: new { controller = "Static", action = "Contact" }
        );
            //index
            routes.MapRoute(
            name: "Index",
            url: "index",
            defaults: new { controller = "Static", action = "Index" }
        );
            //learn_claim
            routes.MapRoute(
            name: "Learn_Claim",
            url: "learn_claim",
            defaults: new { controller = "Static", action = "Learn_Claim" }
        );
            //privacy
            routes.MapRoute(
            name: "Privacy",
            url: "privacy",
            defaults: new { controller = "Static", action = "Privacy" }
        );

            //DGO
            routes.MapRoute(
              name: "DGO",
              url: "vc-agent",
              defaults: new { controller = "Static", action = "DGO", id = UrlParameter.Optional }
     );
            routes.MapRoute(
            name: "BusinessSolutions",
            url: "businesssolutions",
            defaults: new { controller = "Static", action = "BusinessSolutions" }
        );
            routes.MapRoute(
            name: "Feedback",
            url: "feedback",
            defaults: new { controller = "Static", action = "Feedback", id = UrlParameter.Optional }
   );
            routes.MapRoute(
          name: "linkredirect",
          url: "linkredirect",
          defaults: new { controller = "Static", action = "bannerclick" }
      );

            routes.MapRoute(
       name: "websitelink",
       url: "websitelink",
       defaults: new { controller = "Detail", action = "bizWebsite", bizid = UrlParameter.Optional }
   );

            //            routes.MapRoute(
            //        name: "VconnectOnYourMobile",
            //        url: "vc-mobile",
            //        defaults: new { controller = "Static", action = "Product", id = UrlParameter.Optional }
            //);

            routes.MapRoute(
            name: "ClaimYourBusiness",
            url: "claimyourbusiness",
            defaults: new { controller = "Static", action = "LearnClaim" }
        );

            routes.MapRoute(
                name: "searchnotfound",
                url: "searchnotfound",
                defaults: new { controller = "SearchListWEB", action = "SearchNotFound", id = UrlParameter.Optional }
       );
            routes.MapRoute(
               name: "PageNotFound",
               url: "404",
               defaults: new { controller = "Static", action = "PageNotFound", id = UrlParameter.Optional }
      );

            routes.MapRoute(
               name: "Faq",
               url: "faq",
               defaults: new { controller = "Static", action = "FAQs", id = UrlParameter.Optional }
      );
            routes.MapRoute(
              name: "Contactus",
              url: "reachus",
              defaults: new { controller = "Static", action = "Contactus", id = UrlParameter.Optional }
     );

            routes.MapRoute(
            name: "Sales",
            url: "sales",
            defaults: new { controller = "Static", action = "Contactus", id = UrlParameter.Optional }
   );
            routes.MapRoute(
              name: "SmeEvent",
              url: "smeevent",
              defaults: new { controller = "Static", action = "smeevent", id = UrlParameter.Optional }
     );
            routes.MapRoute(
              name: "AboutVconnect",
              url: "about_vconnect",
              defaults: new { controller = "Static", action = "About", id = UrlParameter.Optional }
     );
            routes.MapRoute(
                       name: "Terms",
                       url: "terms",
                       defaults: new { controller = "UserReg", action = "TermsCondition" }
                   );
            routes.MapRoute(
           name: "PrivacyPolicy",
           url: "privacypolicy",
           defaults: new { controller = "Static", action = "privacypolicy" }
       );
            #endregion
            #region CYB
            routes.MapRoute(
             name: "CYBWEB",
             url: "claimbusiness",
             defaults: new { controller = "ClaimBusiness", action = "SearchCYBDetails" }
         );

            routes.MapRoute(
            name: "CYBSendCodeWEB",
            url: "claimbusinesssencode",
            defaults: new { controller = "ClaimBusiness", action = "SendCYBCodeNew" }
        );
            routes.MapRoute(
           name: "CYBReportIncorrect",
           url: "CYBRepotIncorrect",
           defaults: new { controller = "ClaimBusiness", action = "CYBReportIncorrectComments" }
       );
            #endregion

            #region BO DASHboard WAP
            routes.MapRoute(
          name: "BODashboardWAP",
          url: "mybusinesss",
          defaults: new { controller = "BoDashboardWap", action = "GetClimedBusiness" }
      );

            routes.MapRoute(
         name: "BODashboardWEB",
         url: "mybusiness",
         defaults: new { controller = "BoDashboardWEB", action = "Index" }
     );
            #endregion
            #region Quickcategory
            // For WAP Index page setting
            routes.MapRoute(
                name: "Quickcategory",
             url: "{loc}/{Keyword}_cl{id}",
               constraints: new { id = @"^[0-9]*$" },
                defaults: new { controller = "Categorylist", action = "Itemlistaction" }
            );
            #endregion
            #region Catserlisting


            routes.MapRoute(
           name: "searchbycatpg",
           url: "{loc}/list-of-{tagname}_c{catid}-page{page}",
           constraints: new { page = @"^[0-9]*$" },
           defaults: new { controller = "SearchListWEB", action = "SearchWEB", Sort = UrlParameter.Optional }
          );
            routes.MapRoute(
              name: "searchbycat",
              url: "{loc}/list-of-{tagname}_c{catid}/{Sort}",
              defaults: new { controller = "SearchListWEB", action = "SearchWEB", Sort = UrlParameter.Optional }
             );
            routes.MapRoute(
            name: "searchbysevpg",
            url: "{loc}/list-of-{tagname}-vendors-search_s{catid}-page{page}",
            constraints: new { page = @"^[0-9]*$" },
            defaults: new { controller = "SearchListWEB", action = "SearchWEB", Sort = UrlParameter.Optional }
            );
            routes.MapRoute(
            name: "searchbysev",
            url: "{loc}/list-of-{tagname}-vendors-search_s{catid}/{Sort}",
            defaults: new { controller = "SearchListWEB", action = "SearchWEB", Sort = UrlParameter.Optional }
            );
            routes.MapRoute(
            name: "searchbyprdpg",
            url: "{loc}/list-of-{tagname}-vendors-search_p{catid}-page{page}",
            constraints: new { page = @"^[0-9]*$" },
            defaults: new { controller = "SearchListWEB", action = "SearchWEB", Sort = UrlParameter.Optional }
            );
            routes.MapRoute(
              name: "searchbyprd",
              url: "{loc}/list-of-{tagname}-vendors-search_p{catid}/{Sort}",
              defaults: new { controller = "SearchListWEB", action = "SearchWEB", Sort = UrlParameter.Optional }
             );

            routes.MapRoute(
        name: "searchbybrpg",
        url: "{loc}/list-of-{tagname}-search_br{catid}-page{page}",
        constraints: new { page = @"^[0-9]*$" },
        defaults: new { controller = "SearchListWEB", action = "SearchWEB", Sort = UrlParameter.Optional }
       );
            routes.MapRoute(
              name: "searchbybr",
              url: "{loc}/list-of-{tagname}-search_br{catid}/{Sort}",
              defaults: new { controller = "SearchListWEB", action = "SearchWEB", Sort = UrlParameter.Optional }
             );
            #endregion


            //routes.MapRoute(
            //  name: "bizattachment",
            //  url: "{viewattachments.aspx}/{BID}/{id}",
            //  constraints: new { BID = @"^[0-9]*$" },
            //  defaults: new { controller = "Detail", action = "Detailattachment", id = UrlParameter.Optional }
            //   );
            #region BrowseByCatBiz
            routes.MapRoute(
              name: "bizdetailrv3",
              url: "{businessname}/{bizID}/{id}",
              constraints: new { bizID = @"^[0-9]*$" },
              defaults: new { controller = "Detail", action = "Detailrv", id = UrlParameter.Optional }
               );
           // routes.MapRoute(
           //name: "bizdetail",
           //url: "{businessname}_b{bizID}",
           //constraints: new { bizID = @"^[0-9]*$" },
           //defaults: new { controller = "BizDetail", action = "BizDetail" }
           // );

            routes.MapRoute(
              name: "bizdetail",
              url: "{businessname}_b{bizID}",
              constraints: new { bizID = @"^[0-9]*$" },
              defaults: new { controller = "Detail", action = "Detail" }
               );
            routes.MapRoute(
          name: "bizdetailstate",
          url: "{state}/{businessname}_b{bizID}",
          constraints: new { bizID = @"^[0-9]*$" },
          defaults: new { controller = "Static", action = "PageNotFound" }
           );
            routes.MapRoute(
             name: "browsebycatpg",
             url: "{loc}/categories-from-{tagname}-page{page}",
                //constraints: new { page = @"^[0-9]*$" },
             defaults: new { controller = "BrowseByCatBiz", action = "Index" }
            );
            routes.MapRoute(
               name: "browsebycat",
               url: "{loc}/categories-from-{tagname}",
               defaults: new { controller = "BrowseByCatBiz", action = "Index" }
              );


            routes.MapRoute(
             name: "browsebybizpg",
             url: "{loc}/businesses-from-{tagname}-page{page}",
                //constraints: new { page = @"^[0-9]*$" },
             defaults: new { controller = "BrowseByCatBiz", action = "Index" }
            );
            routes.MapRoute(
            name: "browsebybiz",
            url: "{loc}/businesses-from-{tagname}",
            defaults: new { controller = "BrowseByCatBiz", action = "Index" }
             );
            
            //     routes.MapRoute(
            //name: "bizdetail2",
            //url: "{businessname}_pg{bizID}",
            //constraints: new { bizID = @"^[0-9]*$" },
            //defaults: new { controller = "BizDetail", action = "BizDetail" }
            // );
            routes.MapRoute(
            name: "searchbybiz",
            url: "{businessname}-{state}_b{businessid}",
            defaults: new { controller = "BrowseByCatBiz", action = "Index", id = UrlParameter.Optional }
             );

    //        routes.MapRoute(
    // name: "browsebybrandpg",
    // url: "{loc}/brands-from-{tagname}-page{page}",
    //            //constraints: new { page = @"^[0-9]*$" },
    // defaults: new { controller = "BrowseByCatBiz", action = "Index" }
    //);
    //        routes.MapRoute(
    //           name: "browsebybrand",
    //           url: "{loc}/brands-from-{tagname}",
    //           defaults: new { controller = "BrowseByCatBiz", action = "Index" }
    //          );
    //        routes.MapRoute(
    //    name: "browsebybrands",
    //    url: "browsebybrands",
    //    defaults: new { controller = "BrowseByCatBiz", action = "index", tagname = "a", loc = "lagos" }
    //);
            routes.MapRoute(
             name: "browsebyproductpg",
             url: "{loc}/products-from-{tagname}-page{page}",
                //constraints: new { page = @"^[0-9]*$" },
             defaults: new { controller = "BrowseByCatBiz", action = "Index" }
            );
            routes.MapRoute(
               name: "browsebyproduct",
               url: "{loc}/products-from-{tagname}",
               defaults: new { controller = "BrowseByCatBiz", action = "Index" }
              );

            routes.MapRoute(
             name: "browsebyservicepg",
             url: "{loc}/services-from-{tagname}-page{page}",
                //constraints: new { page = @"^[0-9]*$" },
             defaults: new { controller = "BrowseByCatBiz", action = "Index" }
            );
            routes.MapRoute(
               name: "browsebyservice",
               url: "{loc}/services-from-{tagname}",
               defaults: new { controller = "BrowseByCatBiz", action = "Index" }
              );


            #endregion
            #region Search For WapWeb

            routes.MapRoute(
                name: "SearchNew",
                url: "qsearch",
                defaults: new { controller = "SearchListWEB", action = "SearchWEB", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "Searchold",
               url: "{loc}/list-of-{tagname}-qsearch",
               defaults: new { controller = "SearchListWEB", action = "SearchWEB", id = UrlParameter.Optional }
           );
            routes.MapRoute(
               name: "Searchpageold",
               url: "{loc}/list-of-{tagname}-qsearch-page{page}",
              constraints: new { page = @"^[0-9]*$" },
               defaults: new { controller = "SearchListWEB", action = "SearchWEB", id = UrlParameter.Optional }
           );


            #endregion

            #region business detail
            routes.MapRoute(
         name: "bizdetailpgphoto",
         url: "photogallery_pg{bizID}",
         constraints: new { bizID = @"^[0-9]*$" },
         defaults: new { controller = "BizDetail", action = "GetBizPhotosAttachments", module = "photo" }
          );
         //   routes.MapRoute(
         //  name: "bizdetailpgattachment",
         //  url: "attachmentgallery_pg{bizID}",
         //  constraints: new { bizID = @"^[0-9]*$" },
         //  defaults: new { controller = "BizDetail", action = "GetBizPhotosAttachments", module = "attachment" }
         //   );
         //   routes.MapRoute(
         // name: "bizdetailpgvideo",
         // url: "videogallery_pg{bizID}",
         // constraints: new { bizID = @"^[0-9]*$" },
         // defaults: new { controller = "BizDetail", action = "BizDetail" }
         //  );

            //routes.MapRoute(
            //     name: "GetBizPhotosAttachments",
            //    url: "displayphotosbiz",

            //    defaults: new { controller = "BizDetail", action = "GetBizPhotosAttachments", id = UrlParameter.Optional }
            //);

            //routes.MapRoute(
            //          name: "GetDirection",
            //         url: "getdirection",

            //         defaults: new { controller = "BizDetail", action = "GetDirection", id = UrlParameter.Optional }
            //     );
            routes.MapRoute(
                     name: "GetDirection",
                    url: "getdirection",

                    defaults: new { controller = "Detail", action = "GetDirection", id = UrlParameter.Optional }
                );
            #endregion
            routes.MapRoute(
                name: "WebSearchNew",
               url: "qsearch",

               defaults: new { controller = "SearchListWEB", action = "SearchWEB", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "WebSearchold",
               url: "{loc}/list-of-{tagname}-qsearch",
               defaults: new { controller = "SearchListWEB", action = "SearchWEB", id = UrlParameter.Optional }
           );
            routes.MapRoute(
               name: "WebSearchpageold",
               url: "{loc}/list-of-{tagname}-qsearch-page{page}",
              constraints: new { page = @"^[0-9]*$" },
               defaults: new { controller = "SearchListWEB", action = "SearchWEB", id = UrlParameter.Optional }
           );
            //region userdashbaord
            //   routes.MapRoute(
            //name: "userDashbaordWeb",
            //url: "users/myaccount",
            //defaults: new { controller = "UserDashboardWEB", action = "UserDetailsWeb" }
            // );
//            routes.MapRoute(
//      name: "userDashbaordWeb1",
//      url: "users/myaccount",
//      defaults: new { controller = "UserDashboardWEB", action = "UserDetailsWeb" }
//       );
//            routes.MapRoute(
//name: "userprofileurlid",
//url: "profile/{profileid}",
//   constraints: new { profileid = @"^[0-9]*$" },
//defaults: new { controller = "UserDashboardWEB", action = "getUserDetailsID" }
//);


//            routes.MapRoute(
//       name: "userprofileurl",
//       url: "profile/{userid}",
//                // constraints: new { profileurl = @"[0-9]*$" },
//       defaults: new { controller = "UserDashboardWEB", action = "getUserDetails" }
//        );
               routes.MapRoute(
                     name: "userDashbaordWeb",
                     url: "users/myaccount",
                     defaults: new { controller = "UserDashboardWEB", action = "UserDetailsWeb" }
                      );
                        routes.MapRoute(
                  name: "userDashbaordWeb1",
                  url: "users/myaccount",
                  defaults: new { controller = "UserDashboardWEB", action = "UserDetailsWeb" }
                   );

            routes.MapRoute(
      name: "notifications",
      url: "users/notifications",
      defaults: new { controller = "UserDashboardWEB", action = "notifications" }
       );
            routes.MapRoute(
      name: "socialactivity",
      url: "users/socialactivity",
      defaults: new { controller = "UserDashboardWEB", action = "socialactivity" }
       );
            routes.MapRoute(
        name: "userDashbaord",
        url: "users/myaccountwap",
        defaults: new { controller = "UserDashboardWap", action = "userDbWap" }
         );
            routes.MapRoute(
         name: "logout",
         url: "logout",
         defaults: new { controller = "Account", action = "LogOff" }
     );


            //For WEB Index page setting
            routes.MapRoute(
                name: "Default1",
                url: "{controller}/{action}/{id}",
                defaults: new { id = UrlParameter.Optional }
            );

            routes.MapRoute(
                      name: "Default",
                      url: "",
                      defaults: new { controller = "HomeWEB", action = "HomeWEB" }
                  );
            routes.MapRoute("Default2", "{loc}.html", new { controller = "HomeWEB", action = "HomeWEB" });

            #region QuickSms

            routes.MapRoute(
             name: "QuickSms",
             url: "QuickSms",
             defaults: new { controller = "SMSConnect", action = "Quicksms" }
         );


            #endregion
            routes.MapRoute(
           name: "newpassword",
           url: "newpassword",
           defaults: new { controller = "Account", action = "NewPassword" }
       );

            routes.MapRoute(
            name: "forgotpassword",
            url: "forgotpassword",
            defaults: new { controller = "Account", action = "ForgotPassword" }
        );

            routes.MapRoute(
            name: "generatenewemaillink",
            url: "generatenewemaillink",
            defaults: new { controller = "Account", action = "generatenewemaillink" }
        );

            routes.MapRoute(
            name: "sendverification",
            url: "sendverification",
            defaults: new { controller = "Account", action = "sendverification" }
            );
            routes.MapRoute(
            name: "externalloginconfirmation",
            url: "externalloginconfirmation",
            defaults: new { controller = "Account", action = "ExternalLoginConfirmation" }
        );
            routes.MapRoute(
         name: "updateuseremail",
         url: "updateuseremail",
         defaults: new { controller = "Account", action = "UpdateUserEmail" }
     );

            routes.MapRoute(
            name: "wblogin",
            url: "wblogin",
            defaults: new { controller = "Account", action = "wblogin" }
        );
            routes.MapRoute(
                 name: "verification",
                 url: "verification",
                 defaults: new { controller = "Account", action = "generateloginidverification" }
             );
            routes.MapRoute(
         name: "verify",
         url: "verify",
         defaults: new { controller = "Account", action = "verify" }
     );
        }


    }
}