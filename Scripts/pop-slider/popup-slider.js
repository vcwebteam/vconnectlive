﻿var Slider = function() { this.initialize.apply(this, arguments) }
Slider.prototype = {

    initialize: function(slider) {
        this.ul = slider.children[0]
        this.li = this.ul.children

        // make <ul> as large as all <li>’s
        this.ul.style.width = (10000000) + 'px'

        this.currentIndex = 0
    },

    goTo: function(index,ulid) {
       //
        // filter invalid indices
      // if (index < 0 || index > ulid.li.length - 1)
           // return
        // move <ul> left
        document.getElementById(ulid).style.left = '-' + (100 * index) + '%'
        this.currentIndex = index
    },

    goToPrev: function(index) {
        this.goTo(index)
    },

    goToNext: function(index) {
        this.goTo(index)
    },
    goReset: function() {
        this.goTo(0)
    }
}