/**
	new details page - component_description

	Usage: 'How to use component'
	Options:
		Option_name - Option_description
			Values: Possible_option_values
	Example:
		Option_example
*/

;(function($, window, document, undefined){
	window.app.comps.ndetails = {
		config: {
			ndetails: {
				photos:{}
			}
		},
		init:function(){
			// Contains the initialization code
			// ...
			var self = this,
					cf = this.config,
					pup = cf.ndetails.photos;

			pup.uploadButton = $('#addPhotoModal .upload-btn');
			pup.uploadActions = $('#addPhotoModal .upload-actions');
			pup.footerNote = $('#addPhotoModal .footer-note');
			pup.uploadSection = $('#addPhotoModal .photo-upload-box');
			pup.editSection = $('#addPhotoModal .edit-section');
			pup.uploadHeader = $('#addPhotoModal .upload-header');
			pup.editHeader = $('#addPhotoModal .edit-header');
			pup.editActions = $('#addPhotoModal .edit-actions');
			pup.saveButton = $('#addPhotoModal .save-btn');

			this.events();
			this.photoUpload();
		},
		events:function(){
			// Contains the event bindings and subscriptions
			var self = this,
					cf = this.config,
					str = cf.ndetails.store,
					pup = cf.ndetails.photos;

		},
		photoUpload:function(){
			// Photo upload...
			var self = this,
					cf = this.config,
					pup = cf.ndetails.photos;

			pup.uploadActions.hide();
			pup.editActions.hide();

			this.require('dropzone', function(){
				return (typeof Dropzone !== 'undefined' && cf.$addPhotoModal.length > 0);
			}, function(){
				var pTemplate = '<div class="dz-preview dz-file-preview">'
				pTemplate += '<div class="dz-details">'
				pTemplate += '<div class="dz-filename"><span data-dz-name></span></div>'
				pTemplate += '<div class="dz-size" data-dz-size></div>'
				pTemplate += '<img data-dz-thumbnail />'
				pTemplate += '</div>'
				pTemplate += '<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>'
				pTemplate += '<div class="dz-success-mark"><span class="icon-ok"></span></div>'
				pTemplate += '<div class="dz-error-mark"><span class="icon-cancel"></span></div>'
				pTemplate += '<div class="dz-error-message"><span data-dz-errormessage></span></div>'
				pTemplate += '</div>';

				var getEditPreview = function(options){
					temp = '<div class="edit-preview row">'
					temp += '<div class="edit-thumb-container columns medium-3">'
					temp += '<img class="edit-thumb" src="' + options.fileThumbUrl + '" alt="thumb">'
					temp += '</div>'
					temp += '<div class="edit-details-container columns medium-9">'
					temp += '<input type="text" id="title" name="' + options.fileNewName + '" data-original="' + options.fileName + '" placeholder="Image Caption">'
					temp += '<textarea name="' + options.fileNewName + '" id="description" cols="30" rows="10" data-original="' + options.fileName + '" placeholder="Write image description here..."></textarea>'
					temp += '</div>'
					temp += '</div>';

					return temp;
				}
				var anySuccess = false;
				var fileTemp = []; //Stores files temporarily
				var fileThumb = []; //Stores file thumbnails

				var photoDropzone = new Dropzone('#add-nphoto', {
					acceptedFiles: 'image/*',
					previewTemplate: pTemplate,
					maxFilesize: 4,
					autoProcessQueue: false,
					addRemoveLinks: true,
					init: function () {
						this.on("complete", function (file) {
							// console.log(addPhotoDrop.files);
							console.log(fileThumb);
							// console.log(file);
							if(file.xhr.statusText == "OK"){
								anySuccess = true;
								for(var i = 0; i < fileTemp.length; i++){
									if(fileTemp[i].fileName == file.name){
										fileThumb.push({
											fileName: fileTemp[i].fileName,
											fileThumbUrl: fileTemp[i].fileThumbUrl,
											fileNewName: JSON.parse(file.xhr.responseText)
											// fileNewName: 'ddd'
										});
									}
								}
								// fileThumb.push();
							}
							$(file._removeLink).hide();
							// console.log('Removed link.');
							if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
								// $('#addPhotoModal').foundation('reveal','close');
								if(anySuccess){

									// Initialize the edit section
									fileThumb.forEach(function(elem, index){
										// getEditPreview(elem).appendTo('');
										console.log(elem);
										pup.editSection.append(getEditPreview(elem));
									});
									pup.uploadHeader.hide();
									pup.uploadSection.hide();
									pup.uploadActions.hide();
									pup.editHeader.show();
									pup.editSection.fadeIn();
									pup.editActions.fadeIn();
								}
								else{
									//Dev:
									// $('#addPhotoModal .btn-box').show();
									// $('#addPhotoModal .upload-btn-box').hide();
									//Prod:
									self.notifyMe({
										message: 'Sorry there was an issue uploading files. Please try again later.',
										type:'error'
									});
								}
							}
						});
						this.on("thumbnail", function(file, dataUrl){
							fileTemp.push({
								fileName: file.name,
								fileThumbUrl: dataUrl,
								fileNewName: ''
							});
						});
						this.on("addedfile", function(file){
							pup.footerNote.hide();
							pup.uploadActions.show();
						});
						this.on("processing", function(file){
							// console.log(addPhotoDrop.files);
							this.options.autoProcessQueue = true;
						});
						this.on("removedfile",function(file){
							if(this.getAcceptedFiles().length == 0){
								pup.uploadActions.hide();
								pup.editActions.hide();
							}
						});
					}
				});

				pup.uploadButton.on('click', function(e){
					e.preventDefault();
					photoDropzone.processQueue();
				});

				$('#addPhotoModal').on('closed',function(){
					// alert('closed!');
					photoDropzone.removeAllFiles();
					anySuccess = false;
					fileThumb = [];
					fileTemp = [];
					pup.footerNote.show();
					pup.uploadSection.show();
					pup.editSection.hide();
					pup.editActions.hide();
					pup.uploadActions.hide();
				});
			});
		}
	};
}(jQuery, window, document));
