/**
	store - controls behavior in the desktop store section

	Usage: 'How to use component'
	Options:
		Option_name - Option_description
			Values: Possible_option_values
	Example:
		Option_example
*/

;(function($, window, document, undefined){
	window.app.comps.store = {
		init:function(){
			var self = this,
					cf = this.config;

			cf.container = $('.st-section');

			if(cf.container.length<=0)return false;

			cf.listViewButton = $('.st-list-btn');
			cf.gridViewButton = $('.st-grid-btn');
			cf.list = $('.st-items-list');
			cf.isLoaded = false;
			cf.isLoadingMore = false;
			cf.loadBottomThreshold = 100;
			cf.$topbar = cf.container.find('.st-topbar');
			cf.$contentWrapper = cf.container.find('.st-content-wrapper');
			cf.$noResultContainer = cf.container.find('.no-result');
			cf.$loadMoreWrapper = cf.container.find('.load-more');
			cf.$filterContainer = cf.container.find('.filters-wrapper');
			cf.$storeItemsWrapper = cf.container.find('.st-items-wrapper');

			cf.storeDataUrl = cf.storeDataUrl || '/Detail/GetStore';

			cf.itemsPerPage = 20;
			cf.totalCount = 100;

			cf.filterOptions = {};
			cf.filterOptions.supplierid = cf.container.data('bizid');
			cf.filterOptions.pageNum = 1;
			cf.filterOptions.productid = '';
			cf.filterOptions.brandid = '';
			cf.filterOptions.attributeid = '';

			cf.$topbar.hide();
			cf.$contentWrapper.hide();

			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
			var self = this,
					cf = this.config;

			cf.listViewButton.off('click.vc.store').on('click.vc.store', function(e){
				e.preventDefault();
				cf.list.removeClass('grid-view').addClass('list-view');
				$(this).addClass('active');
				cf.gridViewButton.removeClass('active');
			});
			cf.gridViewButton.off('click.vc.store').on('click.vc.store', function(e){
				e.preventDefault();
				cf.list.removeClass('list-view').addClass('grid-view');
				$(this).addClass('active');
				cf.listViewButton.removeClass('active');
			});

			// $('[href="#st-section"]').off('click.vc.store').on('click.vc.store', function(e){

			// });
			self.subscribe('vc:hashChange', function(e, data){
				if(data.hash.indexOf('st-section') > -1){
					if(!cf.isLoaded)self.fetch();
				}
			});
			self.subscribe('vc:store/filter', function(e, filterData){
				var filterArr = filterData.filters;
				var ref = '';
				var tmpArr = [];

				// console.log('Filter data array');
				// console.log(filterArr);

				for(var i = 0; i < filterArr.length; i++){
					tmpArr = [];
					ref = '';
					data = filterArr[i];
					// Loop through the objects in the array

					// Set filter option to update
					switch(data.type){
						case 'brands':
							ref = 'brandid';
							break;
						case 'products':
							cf.isLoaded = false;
							ref = 'productid';
							cf.filterOptions.pageNum = 1;
							cf.filterOptions.productid = '';
							cf.filterOptions.brandid = '';
							cf.filterOptions.attributeid = '';
							break;
						case 'sort':
							ref = 'sort';
							cf.filterOptions[ref] = '';
							break;
						case 'minprice':
							ref = 'minprice';
							cf.filterOptions[ref] = '';
							break;
						case 'maxprice':
							ref = 'maxprice';
							cf.filterOptions[ref] = '';
							break;
						default:
							ref = 'attributeid';
					}

					// Prevent it from adding empty array element at the beginning of the array
					if(cf.filterOptions[ref])tmpArr = cf.filterOptions[ref].split(',');

					if(data.checked){ //add
						tmpArr.push(data.val);
					}
					else{ //remove
						var arrIndex = $.inArray(data.val, tmpArr);
						if(arrIndex > -1){
							tmpArr.splice(arrIndex, 1);
						}
					}
					// console.log('tmpArr');
					// console.log(data);
					// console.log(tmpArr);
					cf.filterOptions[ref] = tmpArr.join();
				}

				console.log(cf.filterOptions);
				// if(filterOptions.type)


				cf.filterOptions.pageNum = 1; //Reset the pageNum when filtering
				self.fetch();
			});

			self.quoteEvents();
		},
		quoteEvents:function(){
			var self = this,
					cf = this.config;

			cf.quoteButton = $('[' + self.attr_name('quote') + ']');
			cf.quoteModal = $('#quoteModal');
			cf.quoteForm = $('.getquote-form');

			cf.quoteButton.off('click').on('click', function(e){
				e.preventDefault();
				var stItem = $(this).parents('.st-item');
				var stItemId = stItem.data('id');
				var stItemImgSrc = stItem.find('.item-image img').attr('src');
				var stItemTitle = stItem.find('.item-name').html();
				var stItemPrice = stItem.find('.item-price').html();
				var stItemRating = stItem.find('.item-rating').html();

				cf.quoteModal.find('.message').val('');
				cf.quoteModal.find('.itemid').val(stItemId);
				cf.quoteModal.find('.item-image img').attr('src', stItemImgSrc);
				cf.quoteModal.find('.item-name').html(stItemTitle || '');
				cf.quoteModal.find('.item-price').html(stItemPrice || '');
				cf.quoteModal.find('.item-rating').html(stItemRating || '');
				cf.quoteModal.foundation('reveal', 'open');
			});

			cf.quoteForm.off('valid').on('valid', function(){
				var $form = $(this);
				var url = $(this).attr('action');
				var formValues = {
					itemid: $form.find('.itemid').val(),
					name: $form.find('.username').val(),
					phone: $form.find('.phone').val(),
					whenToBuy: $form.find('input:radio[name=whenToBuy]:checked').val(),
					message: $form.find('.message').val()
				};
				console.log(formValues);
				self.loading(cf.quoteModal, 'start');
				$.ajax({
					url:url,
					type:'POST',
					dataType:"json",
					data: formValues,
					success:function (data) {
						if(data.status == 1){
							// success
							cf.quoteModal.foundation('reveal', 'close');
							self.notifyMe('Your enquiry has been made to the business owner.')
						}
						else{
							self.notifyMe('Oops! Something went wrong. You can try again later.');
						}
					},
					complete:function(){
						self.loading(cf.quoteModal, 'stop');
					}
				});
			});
		},
		fetch:function(){
			var self = this,
					cf = this.config;

			var storeItemsTemplate = $('#storeItemsTemplate').html();
			var filterTemplate = $('#filterListTemplate').html();
			var priceFilterTemplate = $('#priceFilterTemplate').html();
			var productFilterTemplate = $('#productFilterTemplate').html();

			if(!cf.isLoaded)self.loading(cf.container, 'start');
			else if(cf.isLoadingMore)self.loading(cf.$loadMoreWrapper, 'start');
			else self.loading(cf.$storeItemsWrapper, 'start');

			// Fetch data
			// Render content
			// - Render store items
			// - Render filters
			// Bind events
			// Show content
			$.getJSON(cf.storeDataUrl, cf.filterOptions, function(data){
				console.log(data);
				console.log(this.url);
				var storeItems = '';
				var filters = '';
				var priceFilter = '';
				var productFilter = '';
				var filterSet = {};
				var productsFilterSet = {};

				cf.itemsPerPage = data.selectedvalues.rowsPerPage;
				cf.totalCount = data.totalcount;

				// Filter object format
				// 'name': {
				// 	name: 'name',
				// 	id: 123,
				// 	options: [
				// 		{
				// 			name: '',
				// 			id: 123,
				// 			count: 10
				// 		}
				// 	]
				// }

				// data.product_services
				// filterSet.product_services = filterSet.product_services || {};
				productsFilterSet.name = "Categories"; //"Products";
				productsFilterSet.id = "products";
				productsFilterSet.options = productsFilterSet.options || [{name: "All Categories", id: ""}];
				$.each(data.product_services, function(index, val){
					productsFilterSet.options.push({
						name: val.productname,
						id: val.productid
					});
				});

				// data.brands
				if(data.brands[0]){ //Only create brand filters if there are brands to filter through
					filterSet.brands = filterSet.brands || {};
					var brandsFilter = filterSet.brands;
					brandsFilter.name = "Brands";
					brandsFilter.id = "brands";
					brandsFilter.options = brandsFilter.options || [];
					$.each(data.brands, function(index, val){
						brandsFilter.options.push({
							name: val.brandname,
							id: val.brandid
						});
					});
				}

				$.each(data.attributevalues, function(index, val){
					filterSet[val.attributeparentname] = filterSet[val.attributeparentname] || {};
					var curFilter = filterSet[val.attributeparentname];

					curFilter.name = val.attributeparentname;
					curFilter.id = val.attributeparentid;
					curFilter.options = curFilter.options || [];
					curFilter.options.push({
						name: val.attributename,
						id: val.attributeid
					});
				});

				console.log(filterSet);
				// console.log(storeItems);

				if(typeof doT !== 'undefined'){
					storeItems = doT.template(storeItemsTemplate)(data.itemdetails);

					if(cf.filterOptions.pageNum < 2)cf.list.empty(); //Empty the list if starting from the top
					cf.list.append(storeItems);

					if(cf.totalCount){
						cf.list.show();
						cf.$noResultContainer.hide();
					}
					else{
						cf.list.hide();
						cf.$noResultContainer.show();
					}


					// console.log('qqq');

					//if(!cf.isLoaded){ //only render filter if the store is loading for the first time
						filters = doT.template(filterTemplate)(filterSet);
						priceFilter = doT.template(priceFilterTemplate)({minprice: +data.minprice, maxprice: +data.maxprice})
						productFilter = doT.template(productFilterTemplate)(productsFilterSet);

						var $filters = $(filters);
						var $priceFilter = $(priceFilter);
						var $productFilter = $(productFilter);

						var selectedAttributes = data.selectedvalues.attributeid && data.selectedvalues.attributeid.split(',');
						var selectedBrands = data.selectedvalues.brandid && data.selectedvalues.brandid.split(',');
						var selectedProduct = +data.selectedvalues.productid;

						// console.log('Updating products');
						// update the product filter
						var productsLen = productsFilterSet.options.length;
						var curOption = {};
						var curProdLink;
						for(var i = 0; i < productsLen; i++){
							curOption = productsFilterSet.options[i];
							curProdLink = $productFilter.filter('[data-id="' + productsFilterSet.id + '"]').find('.link-filter[data-value="' + curOption.id + '"]');
							console.log(curProdLink);
							console.log($productFilter);

							// check the filter if it is the selected product
							if(curOption.id == selectedProduct)curProdLink.addClass('active');
							else curProdLink.removeClass('active');
							// console.log('Updated product filter.');
						}

						// console.log('Updating attributes.');
						// update all the filters
						for(filter in filterSet){
							console.log('Updating filter: ' + filter + ':' + +new Date());
							// cf.$filterContainer.find()
							var curFilter = filterSet[filter];
							var optLen = curFilter.options.length;
							var curOption = {};
							var curCheck;
							// filterSet[filter].id
							for(var i = 0; i < optLen; i++){
								curOption = curFilter.options[i];
								// curCheck = $filters.filter('#vc-filter-' + curFilter.id).find('input#vc-filteropt-' + curFilter.id + '-' + curOption.id);

								// console.log('loop');
								// check the filter if it is among the selected options
								if(filter == 'brands'){
									if($.inArray(''+curOption.id, selectedBrands) > -1){
										// Use ID selector to improve performance - http://learn.jquery.com/performance/optimize-selectors/
										curCheck = $filters.find('input#vc-filteropt-' + curFilter.id + '-' + curOption.id);
										curCheck.prop('checked', true);
									}
								}
								else{
									if($.inArray(''+curOption.id, selectedAttributes) > -1){
										curCheck = $filters.find('input#vc-filteropt-' + curFilter.id + '-' + curOption.id);
										curCheck.prop('checked', true);
									}
								}
								// console.log('checkers: ' + curOption.id);

								// curCheck.parent().children('span').text(curOption.name);
								// console.log('done loop.');
							}
							console.log('Updated filter: ' + filter + ':' + +new Date());
						}

						// console.log('Updating price...');
						// update the price filter
						if(data.selectedvalues.minprice || data.selectedvalues.maxprice){
							console.log(data.selectedvalues.minprice + ',' + data.selectedvalues.maxprice);
							$priceFilter.find('[data-vc-range]')
								.attr('data-vc-setmin', data.selectedvalues.minprice)
								.attr('data-vc-setmax', data.selectedvalues.maxprice);

							// self.publish('vc:store/setprice', {
							// 	range: [data.selectedvalues.minprice, data.selectedvalues.maxprice]
							// });
						}
						// console.log('Finished Updating.');

						console.log('rendering filters...' + +new Date());
						cf.$filterContainer.empty();
						cf.$filterContainer.append($productFilter);
						cf.$filterContainer.append($filters);
						// var $finalFilter = $productFilter + $filters;
						if(data.maxprice){
							// Only render the price filter if there is a price range
							cf.$filterContainer.append($priceFilter);
						}

						console.log('rendered filters.' + +new Date());

						self.publish('vc:store/refresh');
					//}
					// else{ // update filters
					// 	var selectedAttributes = data.selectedvalues.attributeid && data.selectedvalues.attributeid.split(',');
					// 	var selectedBrands = data.selectedvalues.brandid && data.selectedvalues.brandid.split(',');
					// 	var selectedProduct = +data.selectedvalues.productid;

					// 	// console.log('Updating products');
					// 	// update the product filter
					// 	var productsLen = productsFilterSet.options.length;
					// 	var curOption = {};
					// 	var curProdLink;
					// 	for(var i = 0; i < productsLen; i++){
					// 		curOption = productsFilterSet.options[i];
					// 		curProdLink = cf.$filterContainer.find('[data-id="' + productsFilterSet.id + '"] .link-filter[data-value="' + curOption.id + '"]');

					// 		// check the filter if it is the selected product
					// 		if(curOption.id == selectedProduct)curProdLink.addClass('active');
					// 		else curProdLink.removeClass('active');
					// 	}

					// 	// console.log('Updating attributes');
					// 	// update all the filters
					// 	for(filter in filterSet){
					// 		// cf.$filterContainer.find()
					// 		var curFilter = filterSet[filter];
					// 		var optLen = curFilter.options.length;
					// 		var curOption = {};
					// 		var curCheck;
					// 		// filterSet[filter].id
					// 		for(var i = 0; i < optLen; i++){
					// 			curOption = curFilter.options[i];
					// 			curCheck = cf.$filterContainer.find('[data-id="' + curFilter.id + '"] input[value="' + curOption.id + '"]');

					// 			// check the filter if it is among the selected options
					// 			curCheck.prop('checked', $.inArray(''+curOption.id, selectedAttributes) > -1);
					// 			curCheck.parent().children('span').text(curOption.name);
					// 		}
					// 	}

					// 	// console.log('Updating price...');
					// 	// update the price filter
					// 	if(data.selectedvalues.minprice || data.selectedvalues.maxprice){
					// 		console.log(data.selectedvalues.minprice + ',' + data.selectedvalues.maxprice);
					// 		self.publish('vc:store/setprice', {
					// 			range: [data.selectedvalues.minprice, data.selectedvalues.maxprice]
					// 		});
					// 	}
					// 	// console.log('Finished Updating.');
					// }
					self.bindScroll();
				}
				// console.log('Finished rendering');
				// Rebind quote modal
				self.quoteEvents();
				// Show store
				cf.$topbar.show();
				cf.$contentWrapper.show();
				if(!cf.isLoaded)self.loading(cf.container, 'stop');
				else if(cf.isLoadingMore)self.loading(cf.$loadMoreWrapper, 'stop');
				else self.loading(cf.$storeItemsWrapper, 'stop');
				cf.isLoaded = true;
				cf.isLoadingMore = false;
			});
		},
		renderStoreItem:function(data){
			var self = this,
					cf = this.config;

			var storeItemTemplate = $('#storeItemTemplate').html();
			// console.log(this);
			return this.render(storeItemTemplate, data);
		},
		renderProductsFilter:function(data){
		},
		renderBrandsFilter:function(data){},
		renderFilter:function(data){
			var self = this,
					cf = this.config;

			var filterTemplate = $('#filterListTemplate').html();
			var singleFilterTemplate = filterTemplate.find('.filter-list li').parent().html();

			var renderedFilter = '';
			var renderedSingles = '';

			renderedFilter = self.render(filterTemplate, data);

			for(var obj in data.options){
				renderedSingles += self.render(singleFilterTemplate, data);
			}

			$(renderedFilter).find('.filter-list').empty().append(renderedSingles);
		},
		setFilterOptions:function(data, checked){},
		updateFilter:function(data){
			// update filter text and checked states
		},
		bindScroll:function(){
			var self = this,
					cf = this.config;

			$(window).off('scroll.store').on('scroll.store', self.throttle(function(){
				if($(window).height() + $(window).scrollTop() > (cf.list.height() + cf.list.scrollTop() - cf.loadBottomThreshold)){
					if(!cf.container.is(':hidden')){ //Only load if the store is showing
						$(window).off('scroll.store');
						// Load next page
						if(cf.filterOptions.pageNum * cf.itemsPerPage < cf.totalCount){
							// console.log(cf.filterOptions.pageNum * cf.itemsPerPage);
							cf.filterOptions.pageNum++;
							cf.isLoadingMore = true;
							self.fetch();
						}
					}
				}
			}, 50));
		}
	};
}(jQuery, window, document));
