/**
	textchanger - Responsible for the text changer feature

*/

;(function($, window, document, undefined){
	window.app.comps.textchanger = {
		config: {
			typeValues: [
				'Restaurants',
				'Banquet Halls',
				'Local Businesses',
				'Hotels',
				'Spas',
				'Shopping Malls',
				'Cinemas',
				'Painters',
				'Car Dealers'
			]
		},
		init:function(){
			// Contains the initialization code
			var cf = this.config,
				self = this;
			//set change text animation
			this.require('textchanger', function(){
				return !!jQuery().changeText;
			},function(){
				if(cf.$jumbotronContainer.length > 0){
					cf.$introMessage.add('[' + self.attr_name('textchanger') + ']').changeText({
						changeValues: cf.typeValues,
						cursorOrientation: cf.typeCursorOrientation
					});
				}
			});
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
		}
	};
}(jQuery, window, document));
