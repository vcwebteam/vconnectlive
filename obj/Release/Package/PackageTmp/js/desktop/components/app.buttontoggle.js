/**
	buttontoggle - Handles the toggle states and behavior of action buttons

	Usage: 'How to use component'
	Options:
		Option_name - Option_description
			Values: Possible_option_values
	Example:
		Option_example
*/

;(function($, window, document, undefined){
	window.app.comps.buttontoggle = {
		init:function(){
			// Contains the initialization code
			// ...
			var self = this,
					cf = this.config;
			cf.buttontoggle.button = $('[' + self.attr_name('toggle') + ']');
			this.events();
		},
		events:function(){
			var self = this,
					cf = this.config;
			// Contains the event bindings and subscriptions
			$(document).on('click', '[' + self.attr_name('toggle') + ']', function(e){
				e.preventDefault();
				var el = this;
				var src = $(el).attr('href');
				var data = $(el).attr(self.attr_name('options'));
				var cls = $(el).attr(self.attr_name('toggle'));
				data = self.data_options(data);
				console.log(src);
				console.log(data);
				console.log(cls);

				$.getJSON(src, data).done(function(result){
					// console.log(src);
					// console.log('Hi!');
					$(el).toggleClass(cls);
				}).fail(function(){
					// console.log('AJAX endpoint: ' + src);
					// console.log('Bye. :(');
					// $(el).toggleClass(cls);
				});
				// When done, publish toggle state change or handle within this component
			});
		},
		data_options:function(options){
			var opts = {};
			// Idea from Foundation.js by Zurb
      if (typeof options === 'object') {
        return options;
      }

      opts_arr = (options || '=').split(';'),
      ii = opts_arr.length;

      function isNumber (o) {
        return ! isNaN (o-0) && o !== null && o !== "" && o !== false && o !== true;
      }

      // Polyfill for trim function in IE8 down
      if (!String.prototype.trim) {
			  String.prototype.trim = function () {
			    return this.replace(/^\s+|\s+$/g, '');
			  };
			}
      function trim(str) {
        if (typeof str === 'string') return str.trim();
        return str;
      }

      while (ii--) {
        p = opts_arr[ii].split('=');

        if (/true/i.test(p[1])) p[1] = true;
        if (/false/i.test(p[1])) p[1] = false;
        if (isNumber(p[1])) p[1] = parseInt(p[1], 10);

        if (p.length === 2 && p[0].length > 0) {
          opts[trim(p[0])] = trim(p[1]);
        }
      }

      return opts;
		}
	};
}(jQuery, window, document));
