/**
	dropzone - Script for dropzone initialisation and interactivity

*/


;(function($){
	window.app.comps.dropzone = {
		config:{
			$userpicThumb:$('.userpic-thumb'),
			$picLoading:$('.pic-loading')
		},
		init:function(){
			var self = this,
				cf = this.config;
			//Handles dropzone photo upload in details page
			// if(typeof Dropzone === 'undefined'){
			// 	return false;
			// }
			this.require('dropzone', function(){
				return (typeof Dropzone !== 'undefined' && cf.$addPhotoModal.length > 0);
			}, function(){
				//Only called when addPhotoModal is present
				pTemplate = '<div class="dz-preview dz-file-preview">'
				pTemplate += '<div class="dz-details">'
				pTemplate += '<div class="dz-filename"><span data-dz-name></span></div>'
				pTemplate += '<div class="dz-size" data-dz-size></div>'
				pTemplate += '<img data-dz-thumbnail />'
				pTemplate += '</div>'
				pTemplate += '<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>'
				pTemplate += '<div class="dz-success-mark"><span class="icon-ok"></span></div>'
				pTemplate += '<div class="dz-error-mark"><span class="icon-cancel"></span></div>'
				pTemplate += '<div class="dz-error-message"><span data-dz-errormessage></span></div>'
				pTemplate += '</div>';

				var getEditPreview = function(options){
					temp = '<div class="edit-preview">'
					temp += '<div class="edit-thumb-container">'
					temp += '<img class="edit-thumb" src="' + options.fileThumbUrl + '" alt="thumb">'
					temp += '</div>'
					temp += '<div class="edit-details-container">'
					temp += '<input type="text" id="title" name="' + options.fileNewName + '" data-original="' + options.fileName + '" placeholder="Image Caption">'
					temp += '<textarea name="' + options.fileNewName + '" id="description" cols="30" rows="10" data-original="' + options.fileName + '" placeholder="Write image description here..."></textarea>'
					temp += '</div>'
					temp += '</div>';
					return temp;
				}
				Dropzone.autoDiscover = false;
				//Initialisations
				var anySuccess = false;
				var fileTemp = [];
				var fileThumb = [];
				//End initialisations
				var addPhotoDrop = new Dropzone("#add-photo",{
					acceptedFiles: 'image/*',
					previewTemplate: pTemplate,
					maxFilesize: 4,
					autoProcessQueue: false,
					addRemoveLinks: true,
					init: function () {
						this.on("complete", function (file) {
							// console.log(addPhotoDrop.files);
							console.log(fileThumb);
							// console.log(file);
							if(file.xhr.statusText == "OK"){
								anySuccess = true;
								for(var i = 0; i < fileTemp.length; i++){
									if(fileTemp[i].fileName == file.name){
										fileThumb.push({
											fileName: fileTemp[i].fileName,
											fileThumbUrl: fileTemp[i].fileThumbUrl,
											fileNewName: JSON.parse(file.xhr.responseText)
										});
									}
								}
								// fileThumb.push();
							}
							$(file._removeLink).hide();
							// console.log('Removed link.');
							if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
								// $('#addPhotoModal').foundation('reveal','close');
								if(anySuccess){
									$('#addPhotoModal .btn-box').show();
									$('#addPhotoModal .upload-btn-box').hide();
								}
								else{
									//Dev:
									// $('#addPhotoModal .btn-box').show();
									// $('#addPhotoModal .upload-btn-box').hide();
									//Prod:
									this.notifyMe({
										message: 'Sorry there was an issue uploading files. Please try again later.',
										type:'error'
									});
								}
							}
						});
						this.on("thumbnail", function(file, dataUrl){
							fileTemp.push({
								fileName: file.name,
								fileThumbUrl: dataUrl,
								fileNewName: ''
							});
						});
						this.on("addedfile", function(file){
							$('#addPhotoModal .upload-btn-box').show();
						});
						this.on("processing", function(file){
							// console.log(addPhotoDrop.files);
							this.options.autoProcessQueue = true;
						});
						this.on("removedfile",function(file){
							if(this.getAcceptedFiles().length == 0){
								$('#addPhotoModal .upload-btn-box').fadeOut();
								$('#addPhotoModal .btn-box').fadeOut();
								$('#addPhotoModal .save-btn-box').fadeOut();
							}
						});
					}
				});

				$('#addPhotoModal .edit-btn').on('click', function(){
					fileThumb.forEach(function(elem, index){
						// getEditPreview(elem).appendTo('');
						$('.add-photo-2').append(getEditPreview(elem));
					});
					$('.add-photo-1').hide();
					$('.add-photo-2').fadeIn();
					$('#addPhotoModal .btn-box').hide();
					$('#addPhotoModal .save-btn-box').fadeIn();
				});

				$('#addPhotoModal .upload-btn').on('click', function(){
					addPhotoDrop.processQueue();
				});

				$('#addPhotoModal').on('closed',function(){
					// alert('closed!');
					addPhotoDrop.removeAllFiles();
					anySuccess = false;
					fileThumb = [];
					fileTemp = [];
					$('.add-photo-1').show();
					$('.add-photo-2').hide();
					$('#addPhotoModal .btn-box').hide();
					$('#addPhotoModal .save-btn-box').hide();
					$('#addPhotoModal .upload-btn-box').hide();
				});
				$('.no-business-image p').on('click',function(){
					$('.addphoto-btn').trigger('click');
				});
			});

			this.require('dropzone', function(){
				return (typeof Dropzone !== 'undefined' && cf.$uploadAvatar.length > 0);
			}, function(){
				Dropzone.autoDiscover = false;
				var uploadAvatarDrop = new Dropzone("#upload-avatar",{
					acceptedFiles: 'image/*',
					maxFilesize: 1,
					init:function(){
						this.on("complete", function (file) {
							cf.$picLoading.hide();
							if(file.xhr.statusText == "OK"){
								if(file.xhr.responseText){
									// show new image
									self.notifyMe('Your profile picture has been successfully updated.');
									cf.$userpicThumb.add('[' + self.attr_name('userpic') + ']').attr('src',JSON.parse(file.xhr.responseText));
								}
							}
						});
						this.on("addedfile", function(file){
							cf.$picLoading.show();
						});
						this.on("thumbnail", function(file, dataUrl){
							cf.$userpicThumb.attr('src', dataUrl);
						});
					}
				});
			});
		}
	};
}(jQuery));
