/**
	location update - component for updating the map location of businesses

	Usage: 'How to use component'
	Options:
		Option_name - Option_description
			Values: Possible_option_values
	Example:
		Option_example
*/

;(function($, window, document, undefined){
	window.app.comps.locationupdate = {
		init:function(){
			// Contains the initialization code
			// ...
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
			google.maps.event.addDomListener(window, 'load', $.proxy(this.initialize,this));
		},
		initialize:function(){
			var cf = this.config;
			cf.longEl = $('[' + this.attr_name('loc-long') + ']');
			cf.latEl = $('[' + this.attr_name('loc-lat') + ']');
			var mapOptions = {
				zoom: 4,
				center: new google.maps.LatLng(-25.363882, 131.044922)
			};

			var map = new google.maps.Map(document.getElementById('map-canvas'),
					mapOptions);

			var marker = new google.maps.Marker({
				position: map.getCenter(),
				map: map,
				draggable:true,
    		animation: google.maps.Animation.BOUNCE,
				title: 'Click to zoom'
			});

			google.maps.event.addListener(marker, 'click', function() {
				map.setZoom(8);
				map.setCenter(marker.getPosition());
			});

			google.maps.event.addListener(marker, 'dragend', function() {
				var curPos = marker.getPosition();
				map.setCenter(curPos);
				console.log(curPos.long);
				console.log(cf.longEl);
				cf.longEl.val(curPos.k);
				cf.latEl.val(curPos.B);
			});
		}
	};
}(jQuery, window, document));


function initialize() {
}


