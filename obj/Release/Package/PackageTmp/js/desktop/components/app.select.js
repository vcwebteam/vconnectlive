/**
	select - Handles the behavior of the select dropdown element

*/

;(function($, window, document, undefined){
	window.app.comps.select = {
		name: 'select',
		description: '',
		init:function(){
			// Contains the initialization code
			var self = this,
					cf = this.config;
			this.require('selectize', function(){
				return (jQuery().selectize && $('[' + self.attr_name('select') + ']').length > 0);
			}, function(){
				$('[' + self.attr_name('select') + ']').selectize({
					delimiter: ',',
					persist: false,
					create: false
					// create: function(input) {
					// 	return {
					// 		value: input,
					// 		text: input
					// 	}
					// }
				});
			});
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
		}
	};
}(jQuery, window, document));
