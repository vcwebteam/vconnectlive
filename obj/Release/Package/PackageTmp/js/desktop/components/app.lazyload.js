/**
	lazyload - Handles image lazyload logic

	Usage: Add the .lazy class to any image you want to lazy load.
	Example:
		<img src="img/boy.jpg" class="lazy">
*/

;(function($, window, document, undefined){
	window.app.comps.lazyload = {
		name: 'lazyload',
		description: '',
		config:{
			$lazyImage: $('.lazy')
		},
		init:function(){
			// Contains the initialization code
			var cf = this.config;
			//initialize lazyloading
			this.require('unveil', function(){
				return jQuery().unveil;
			}, function(){
				cf.$lazyImage.unveil();
			});
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
		}
	};
}(jQuery, window, document));
