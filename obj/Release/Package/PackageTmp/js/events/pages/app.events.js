;(function($, window, document, undefined){
	window.app.comps.events = {
		name: 'events',
		description: 'description',
		init:function(){
			// Contains the initialization code
			// ..
			var self = this, filterIsOpen = false, scheduleCounter = 0;



			//Handles MOBILE TOP BAR TOP PADDING
			if (window.innerWidth <= 640) {
				$('body').removeClass('f-topbar-fixed');
			};



			//Handles the Filter-Events on the home page
			$('html').on('click', function () {
				if (filterIsOpen) {
					$('.location-in').hide().prev().show();
					filterIsOpen = false;
				}
			});
			$('.filter-events').find('.events-in')
				.on('click', function () {
					$('.events-in').show();
					$(this).hide();
					$('.location-in').hide();
					$(this).next().removeClass('hide').show().on('click', function(e){
						e.stopPropagation();
					})
					filterIsOpen = true;
					return false;
			});



			//Handles the Filter on the Listing page
			$('.listing-filter-hold').find('.listing-b')
				.on('click', function () {
					$('.last-b').toggle("slow");
					$('.last-a').hide();
			});

			$('.listing-filter-hold').find('.listing-a')
				.on('click', function () {
					$('.last-a').toggle("slow");
					$('.last-b').hide();
			});

			$('.listing-filter-hold').find('.custom-date-i')
	       		.on('click', function(){
		        $('.custom-date').toggle("slow");
	      	});




			//Schedule List on the Details Page
			$('.calenda').find('.arrow-right-i')
				.on('click', function (e) {
					e.preventDefault();
					var currentSchedule = $('.schedule-list').eq(scheduleCounter);
					var nextSchedule    = currentSchedule.next();
					if (nextSchedule.length) {
						currentSchedule.hide();
						nextSchedule.show();
						scheduleCounter += 1;
						return false;
					}

			})

			$('.calenda').find('.arrow-left-i')
				.on('click', function (e) {
					e.preventDefault();
					var currentSchedule = $('.schedule-list').eq(scheduleCounter);
					var prevSchedule    = currentSchedule.prev();
					if (prevSchedule.length) {
						currentSchedule.hide();
						prevSchedule.show();
						scheduleCounter -= 1;
						return false;
					}


			})





			//Handles the Content on the Listing page
			$('.top-filter').find('.filter-b')
				.on('click', function (){
				$('.filter-a').removeClass('active-class');
				$('.filter-b').addClass('active-class');
				$('.date-content').hide();
				$('.relevance-content').show();
			});

			$('.top-filter').find('.filter-a')
				.on('click', function (){
				$('.filter-b').removeClass('active-class');
				$('.filter-a').addClass('active-class');
				$('.relevance-content').hide();
				$('.date-content').show();
			});




			// make the items selectable by toogling an 'current' class in pagination listing page
			$('.listing-pagination-count li').off('click.vc').on('click.vc', function (e) {
					// remove active class from all other items
					e.preventDefault();
					$('.listing-pagination-count li').not($(this)).removeClass('current');
					// toggle the active class on the selected item
					$(this).toggleClass('current');
			});




			// make the items selectable by toogling an 'active' class
			$('.cat-list li a').off('click.vc').on('click.vc', function (e) {
					// remove active class from all other items
					e.preventDefault();
					$('.cat-list li a').not($(this)).removeClass('active');
					// toggle the active class on the selected item
					$(this).toggleClass('active');
			});




			// Select First Choice
			$(".choice").change(function () {
				if($(this).val() == "0") $(this).addClass("empty");
				else $(this).removeClass("empty")
			});
			$(".choice").change();




			/*-- Modal-Remind-Me Mobile Switch Initialize */
				var mobileWidth = 640;
				var windowWidth = $(window).width();
					$('.Modal').off('click.vc').on('click.vc', function(e){
						if (windowWidth > mobileWidth) {
							e.preventDefault();
							$('.firstModal').foundation('reveal', 'open');
						};
					});



				this.subscribe('vc:events/refresh', function(){
					self.init();
				});


			this.events();
		 },
		events:function(){
			// Contains the event bindings and subscriptions
		}
	};
}(jQuery, window, document));
