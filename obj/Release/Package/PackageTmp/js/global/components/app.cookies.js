/**
	cookie - Handles the management of all the cookie-related tasks

	Usage: 'How to use component'
	Options:
		Option_name - Option_description
			Values: Possible_option_values
	Example:
		Option_example
*/

;(function($, window, document, undefined){
	window.app.comps.cookies = {
		utils: {
			getCookie: function (sKey) {
				return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
			},
			createCookie: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
			    if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return false; }
			    var sExpires = "";
			    if (vEnd) {
				    switch (vEnd.constructor) {
				        case Number:
				        	if (vEnd === Infinity) {
				        		sExpires =  "; expires=Fri, 31 Dec 9999 23:59:59 GMT";
				        	} else{
				        		var date = new Date();
				                	date.setTime(date.getTime() + (vEnd * 24 * 60 * 60 * 1000));
				                sExpires = "; expires=" + date.toGMTString();
				        	}
				        break;
				        case String:
				          	sExpires = "; expires=" + vEnd;
				        break;
				        case Date:
				          	sExpires = "; expires=" + vEnd.toUTCString();
				        break;
				    }
			    }
			    document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
			    return true;
			},
			removeCookie: function (sKey, sPath, sDomain) {
			    if (!sKey || !this.hasCookie(sKey)) { return false; }
			    document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + ( sDomain ? "; domain=" + sDomain : "") + ( sPath ? "; path=" + sPath : "");
			    return true;
			},
			hasCookie: function (sKey) {
			    return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
			 },
		},
		init:function(){
			// Contains the initialization code
			// ...
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
		},
	};
}(jQuery, window, document));
