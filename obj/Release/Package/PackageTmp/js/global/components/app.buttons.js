/**
	button - Component for the UI buttons behavior

*/

;(function($, window, document, undefined){
	window.app.comps.buttons = {
		init:function(){
			// Contains the initialization code
			// ...
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
			var self = this,
					cf = this.config;

			cf.roleEl.follow.on('click', function(e){
				e.preventDefault();

				self.sendFollowRequest(this);
			});

			cf.roleEl.like.on('click', function(e){
				e.preventDefault();

				self.sendRequest(this, 'like');
			});
		},
		sendFollowRequest:function(el){
			var self = this,
					cf = this.config;
			// $(el).addClass('disabled').prop('disabled', true).html('<img class="loader" src="img/vcloader-dots-white.gif">');
			$(el).addClass('disabled loading-btn').prop('disabled', true);

			var action = $(el).attr('href');
			var currentData = {
				userID: $(el).attr(self.attr_name('uid')),
				value: +!+$(el).attr(self.attr_name('value'))
			};

			$.get(action, currentData, function(data){
				var affectedEls = cf.roleEl.follow.filter('[' + self.attr_name('uid') + '=' + currentData.userID + ']');
				affectedEls.removeClass('disabled loading-btn').prop('disabled', false).attr(self.attr_name('value'), data.value);

				if(data.value){
					// followed
					console.log('Followed.');
					affectedEls.addClass('follow-btn').removeClass('unfollow-btn').html('<i class="icon-user-add"></i> Follow');
				}
				else{
					// unfollowed
					console.log('Unfollowed.');
					affectedEls.addClass('unfollow-btn').removeClass('follow-btn').html('<i class="icon-ok"></i> Following');
				}
			});
		},
		sendRequest:function(el, type){
			var self = this,
					cf = this.config;
			$(el).addClass('disabled').prop('disabled', true).html('<img class="loader" src="img/vcloader-dots-white.gif">');

			var action = $(el).attr('href');
			var currentData = {
				userID: $(el).attr(self.attr_name('uid')),
				value: +!+$(el).attr(self.attr_name('value'))
			};

			$.get(action, currentData, function(data){
				var affectedEls = cf.roleEl.follow.filter('[' + self.attr_name('uid') + '=' + currentData.userID + ']');
				affectedEls.removeClass('disabled').prop('disabled', false).attr(self.attr_name('value'), data.value);
				// fn(affectedEls, data);

				switch(type){
					case 'follow':
						if(data.value){
							// followed
							console.log('Followed.');
							affectedEls.addClass('follow-btn').removeClass('unfollow-btn').html('<i class="icon-user-add"></i> Follow');
						}
						else{
							// unfollowed
							console.log('Unfollowed.');
							affectedEls.addClass('unfollow-btn').removeClass('follow-btn').html('<i class="icon-ok"></i> Following');
						}
						break;
					case 'like':
						if(data.value){
							// Liked
							console.log('Liked.');
							affectedEls.addClass('active').html('<i class="icon-heart"></i> Liked');
						}
						else{
							// Unliked
							console.log('Unliked.');
							affectedEls.removeClass('active').html('<i class="icon-heart"></i> Like');
						}
						break;
				}

				// console.log(affectedEls);
				// console.log(currentData);
			});
		}
	};
}(jQuery, window, document));
