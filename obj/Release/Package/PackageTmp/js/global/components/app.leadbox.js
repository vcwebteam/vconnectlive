/**
	leadbox - Script for lead box related interactivity

	Usage: Add the .lead-box class to the leadbox element on the page
*/

;(function($){
	window.app.comps.leadbox = {
		config:{
			leadSuccessMessage: "Thank you for posting the requirement. We are finding out best suppliers matching your requirement and will soon send their details to you on your phone number.",
			leadErrorMessage: "Something went wrong. Please try again later.",
			$leadBox: $('.lead-box'),
			$leadFormItems: $('.form-item')
		},
		settings:{
			_curIndex: 0,
			_totItems: 0
		},
		init:function(){
			var self = this,
				cf = this.config,
				st = this.settings;
			st.$boxFormItems = cf.$leadBox.find(cf.$leadFormItems);
			st.$progressLevel = cf.$leadBox.find('.progress-level');
			st.$progressCounter = cf.$leadBox.find('.progress-counter');
			st.$prevButton = cf.$leadBox.find(cf.roleEl.prev);
			st.$nextButton = cf.$leadBox.find(cf.roleEl.next);
			st.$skipButton = cf.$leadBox.find(cf.roleEl.skip);
			st.$loadingView = cf.$leadBox.find(cf.roleEl.loading);
			st.$productDropdown = cf.$leadBox.find('[' + this.attr_name('pdrop') + ']');
			st.$productTypeDropdown = cf.$leadBox.find('[' + this.attr_name('ptypedrop') + ']');
			st.$productList = $('.products-list');
			st._totItems = st.$boxFormItems.length;
			st._curIndex = 0; //zero-based index of current form item

			if(cf.$leadBox.length <= 0){
				return false;
			}

			// Hide the loading container
			// cf.$leadFormsContainer.find('[data-vc-role="loading"]').hide();
			st.$loadingView.hide();

			// Show the first section
			self._showItem();
			this.events();
		},
    events:function(){
        var self = this,
            st = this.settings,
            cf = this.config;

        // Form navigation links event handling
        // Previous button event handling
        st.$prevButton.on('click', function(e){
          e.preventDefault();
          if($(this).attr('disabled'))return false;
          self.previousItem();
        });

        // Next button event handling
        st.$nextButton.on('click', function(e){
          e.preventDefault();
          if($(this).attr('disabled'))return false;
          self.nextItem();
        });

        // Skip button event handling
        st.$skipButton.on('click', function(e){
        	e.preventDefault();
            if($(this).attr('disabled'))return false;
        });

        $('.form-item').closest('form').on('submit',function(e, m){
            console.log(arguments);
        });

        // Dropdown and radio options change event handling
        st.$boxFormItems.find('select, input[type="radio"]').on('change',$.proxy(self.nextItem, this));

        // Dropdown for product list change event handling
        st.$productDropdown.on('change', function(){
        	self.fillProductType();
        });

        this.subscribe("vc:lead/submit", function(e, data){
        	// console.log('submit?')
        	if(data && data.status && data.status == "success"){
	        	self.notifyMe(data.message || cf.leadSuccessMessage);
	        	cf.$leadBox.fadeOut();
        	}
        	else if(data && data.status && data.status == "error"){
        		self.notifyMe({type: "error", message: data.message || cf.leadErrorMessage});
        	}
        	st.$loadingView.hide();
        });

        this.subscribe("vc:productlist/select", function(e, data){
        	st.$productDropdown.val($(data).attr(self.attr_name('pid'))).change();
        	$('html, body').animate({
        		scrollTop: cf.$leadBox.offset().top - 100
        	}, 300, function(){
        		cf.$leadBox.removeClass('shake').width(cf.$leadBox.width()).addClass('shake');
        	});
        });
        // Input value change event handling
        // st.$boxFormItems.find('input[type="checkbox"]').on('change',$.proxy(self._showItem, this));
    },
    getItem:function(curIndex){
        var st = this.settings;
        curIndex = (typeof curIndex == "undefined") ? st._curIndex : curIndex;
        return this.settings.$boxFormItems.filter(':eq(' + curIndex + ')');
    },
		_showItem:function(curIndex){
			var st = this.settings,
				curItem;

			st._curIndex = (typeof curIndex == "undefined") ? 0 : curIndex;
			st.$boxFormItems.hide();
			curItem = this.getItem(st._curIndex).fadeIn();

			st.$progressLevel.css('width', ((st._curIndex+1)/(st._totItems) * 100) + '%');
			st.$progressCounter.html('Question ' + (st._curIndex+1) + ' of ' + st._totItems + '');

			// Display the necessary button

			st.$nextButton.show();
			st.$prevButton.show();
			st.$skipButton.show();

			console.log(st._curIndex);
			if(st._curIndex <= 0){
				st.$prevButton.hide();
				st.$skipButton.hide();
				if(!this.hasInputValue(curItem)){
					st.$nextButton.hide();
				}
			}
			if(curItem.hasClass('no-skip'))st.$skipButton.hide();
			if(st._curIndex >= st._totItems-1)st.$nextButton.text('Get Result');
			else st.$nextButton.text('Continue');
		},
		previousItem:function(){
			// show the previous item
			var st = this.settings;
			if(st._curIndex > 0){
				st._curIndex--;
				this._showItem(st._curIndex);
			}
		},
    nextItem:function(){
        // show the next item
        var st = this.settings;
        if(!this.isValidItem())return false;
        if(st._curIndex < st._totItems-1){
            st._curIndex++;
            this._showItem(st._curIndex);
        }
        else{
            console.log('submitting lead request...');
            st.$nextButton.prop('disabled', true);
            this.publish('vc:lead/finalclick');
            st.$loadingView.attr(this.attr_name('value'), 'Submitting your request...').show();
            this.getItem().closest('form').attr('data-vc-value', 'complete').submit();
        }
    },
		hasInputValue:function($form){
			var flg = 0;
			// 'input,select,textarea'
			console.log($form.find(':checked'));
			// console.log('1: ' + flg);
			//For radio, checkbox and select items
			flg += !!$form.find(':checked').val();

			$form.find('input').not('[type="radio"],[type="checkbox"]').each(function(index, elem){
				if($(elem).val())flg++;
			});
			console.log('4: ' + flg);
			return !!flg;
		},
    isValidItem:function(curIndex){
      var st = this.settings,
          curItem;
      curIndex = (typeof curIndex == "undefined") ? st._curIndex : curIndex;
      curItem = this.getItem(curIndex);
      console.log(curItem);
      // $(curItem).closest('form').trigger('validate');
      Foundation.libs.abide.validate(curItem.find('input, select, textarea'), {type:''})
      // $('.form-item').closest('form').trigger('validate');
      return !curItem.find('[data-invalid]').length;
    },
    fillProductType:function(){
    	var self = this,
            st = this.settings,
            cf = this.config;
      var productTypes,
      		productValue = st.$productDropdown.val();

      productTypes = JSON.parse(st.$productList.find('[' + this.attr_name('pid') + '=' + productValue + ']').attr(this.attr_name('ptypes')) || "{}");
      // console.log(st.$productList.find('[' + this.attr_name('pid') + '=' + productValue + ']'));
			var optionsAsString = "<option value=''>Choose an option</option>";
			$.each(productTypes, function(key, value){
				optionsAsString += "<option value='" + key + "'>" + value + "</option>";
			});
			st.$productTypeDropdown.find('option').remove().end().append($(optionsAsString));
    }
	};
}(jQuery));
