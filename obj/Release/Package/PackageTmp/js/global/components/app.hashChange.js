/**
	component_name - component_description

	Usage: 'How to use component'
	Options:
		Option_name - Option_description
			Values: Possible_option_values
	Example:
		Option_example
*/

;(function($, window, document, undefined){
	window.app.comps.hashChange = {
		init:function(){
			// Contains the initialization code
			// ...
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
			var hashData = {changeEnabled: false};

			//disable the jumping effect of the page
			// $(document).on('click', 'a[href^="#"]', function(e){
			// 	e.preventDefault();

			// });
			if ("onhashchange" in window) {
				window.onhashchange = function(){
					hashData.changeEnabled = true;
					hashData.hash = window.location.hash.replace('#', '');
					app.publish('vc:hashChange', hashData);
				}
			}

			// Publish hash change initially
			hashData.hash = window.location.hash.replace('#', '');
			app.publish('vc:hashChange', hashData);
		}
	};
}(jQuery, window, document));
