;(function($, window, document, undefined){
	window.app.comps.ctv = {
		name: 'ctv',
		description: 'Responsible for the click-to-view logic',
		init:function(){
			$('[data-ctv]').children('.masked').addClass('hide');
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
			$('[data-ctv]').on('click', function (e) {
				e.preventDefault();
				if (!$(this).hasClass('open')) {
					$(this).children('.mask').addClass('hide');
					$(this).children('.masked').removeClass('hide').addClass('show');
					$(this).addClass('open');
				};
			});
		}
	};
}(jQuery, window, document));
