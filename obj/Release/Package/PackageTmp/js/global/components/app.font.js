/**
	font - Handles Google fonts

*/

;(function($){
	window.app.comps.font = {
		init:function(){
			//Initialise site font
			WebFontConfig = {
		    google: { families: [ 'Open+Sans:400italic,700italic,400,700,300:latin' ] }
		  };
		  (function() {
		    var wf = document.createElement('script');
		    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
		      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
		    wf.type = 'text/javascript';
		    wf.async = 'true';
		    var s = document.getElementsByTagName('script')[0];
		    s.parentNode.insertBefore(wf, s);
		  })();
		}
	};
}(jQuery));
