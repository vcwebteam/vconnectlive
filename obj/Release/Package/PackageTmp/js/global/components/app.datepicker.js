/**
	Datepicker - Responsible for the datepicker logic

	Usage: Add 'data-vc-datepicker' attribute to the element
	Options:
		'data-vc-mindate' - Specifies the minimum date of the datepicker
			Values: 'today' or a specific date with the YYYY-MM-DD format
		'data-vc-yearrange' - Specifies the number of years the datepicker should span
			Values: '[2010,2015]' (specifies the year range in an array) or '[200]' (specifies the number of years)
	Example:
		<input type="text" data-vc-datepicker data-vc-mindate="1920-09-12" data-vc-yearrange="[120]">
*/

;(function($, window, document, undefined){
	window.app.comps.datepicker = {
		config: {
			pickers:[]
		},
		init:function(){
			// Contains the initialization code
			var cf = this.config,
				self = this;
			//Handles datepicker
			this.require('pikaday', function(){
				return typeof Pikaday !== 'undefined'
			}, function(){
				// Formerly $('.datepicker')
				$('[' + self.attr_name('datepicker') + '], .datepicker').each(function(index, elem){
					// Each instance of the plugin can be customized with data attributes on the elements

				// console.log('in date');
					// data-vc-mindate
					// today - Specifies the current date
					// YYYY-MM-DD - The specific date
					var _minDate = $(elem).attr(self.attr_name('mindate'));
					if(!_minDate)_minDate = new Date('1890-01-01');
					else if(_minDate == 'today')_minDate = new Date();
					else _minDate = new Date(_minDate);

					var _maxDate = $(elem).attr(self.attr_name('maxdate'));
					if(!_maxDate)_maxDate = new Date('2050-12-31');
					else if(_maxDate == 'today')_maxDate = new Date();
					else _maxDate = new Date(_maxDate);
					// data-vc-yearrange
					// [2010,2015] - Specifies the year range in an array
					// [200] - Specifies the number of years
					var _yearRange = ($(elem).attr(self.attr_name('yearrange')))? JSON.parse($(elem).attr(self.attr_name('yearrange'))) : [1890,2050];

					cf.pickers.push({
						element:elem,
						picker: new Pikaday({
							field: $(elem)[0],
							format: 'YYYY-MM-DD',
							firstDay: 1,
							minDate: _minDate,
							maxDate: _maxDate,
							yearRange: _yearRange
						})
					});
				});
				// cf.picker = new Pikaday({
				// 	field: $('.datepicker')[0],
				// 	format: 'YYYY-MM-DD',
				// 	firstDay: 1,
				// 	minDate: new Date('1890-01-01'),
				// 	maxDate: new Date(),
				// 	yearRange: [100]
				// });
			});
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
		}
	};
}(jQuery, window, document));
