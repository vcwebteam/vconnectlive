/**
	gallery - Contains the logic for the photo gallery functionality

	Usage: Add the .th class to each of the image gallery images. Also you can add the data-vc-gallery attribute to
	an element on the page and add the image properties as a JSON object to the data-vc-value attribute.
	Options:
		data-vc-gallery - Specifies the gallery values element
	Example:
		<li class="see-more" data-vc-gallery data-vc-value='[{"imageTitle":"First Lasante","imageUrl":"img/lab-abstract2.jpg"},{"imageTitle":"Second Burritos","imageUrl":"img/lab-logo.jpg"}]'></li>
*/

;(function($, window, document, undefined){
	window.app.comps.gallery = {
		config:{
			galleryImgLists: {},
			defaultRel: 'gallery',
			bigGalleryClass: 'big-image-gallery',
			galleryContainerClass: 'gallery-container',
			galleryHeaderClass: 'gallery-header',
			galleryDescriptionHeaderClass: 'gallery-description-header',
			galleryImageContainerClass: 'gallery-image-container',
			galleryDescriptionContainerClass: 'gallery-description-container',
			galleryCloseLinkClass: 'gallery-close',
			galleryPreviousLinkClass: 'gallery-previous',
			galleryNextLinkClass: 'gallery-next',
			galleryDisabledLinkClass: 'disabled',
			galleryImageCountClass: 'image-count',
			galleryLoadingClass: 'gallery-loading',
			galleryTitleClass: 'gallery-title',
			galleryUploadDetailsClass: 'gallery-upload-details',
			galleryUploadDateClass: 'upload-date',
			galleryUploaderNameClass: 'uploader-name',
			galleryUploaderThumbnailClass: 'uploader-thumbnail',
			galleryBusinessNameClass: 'business-name',
			galleryBusinessThumbnailClass: 'business-thumbnail',
			galleryBusinessDetailsClass: 'business-details',
			galleryDescriptionClass: 'gallery-description',
			galleryImageClass: 'gallery-image',
			galleryActionsContainerClass: 'gallery-actions-container',
			showDescriptionButtonClass: 'show-description',
			$bigGallery: {},
			gallerySet: {},
			$galleryContainer: {},
			$galleryHeader: {},
			$galleryDescriptionHeader: {},
			$galleryImageContainer: {},
			$galleryDescriptionContainer: {},
			$galleryCloseLink: {},
			$galleryPreviousLink: {},
			$galleryNextLink: {},
			$galleryImageCount: {},
			$galleryLoading: {},
			$galleryTitle: {},
			$galleryUploadDetails: {},
			$galleryUploadDate: {},
			$galleryUploaderName: {},
			$galleryUploaderThumbnail: {},
			$galleryBusinessName: {},
			$galleryBusinessThumbnail: {},
			$galleryBusinessDetails: {},
			$galleryDescription: {},
			$galleryActionsContainer: {},
			$showDescriptionButton: {},
			$imgSet: $(),
			isOpen: false,
			isBuilt: false,
			hasDescription: false,
			container: $('.th, [data-vc-gallery-thumb]'),
			galleryImageArrayEls: {},
			imgsList: {},
			curIndex: 0,
			preloadCount: 2,
			galleryTemplate: '<div class="big-image-gallery"> <div class="gallery-container"> <div class="gallery-header"> <a href="#" class="gallery-close">&times;</a> <div class="gallery-upload-details"> <img class="uploader-thumbnail" src="img/lab-profile300.jpg" alt="uploader thumbnail"> <div class="upload-date">24 June, 2014</div> <div class="uploader-name">Jonathan Douglas</div> </div> <h4 class="gallery-title">Triple Tripulley</h4> </div> <div class="gallery-image-container"> <div class="gallery-loading"></div></div> <div class="gallery-description-container"> <div class="gallery-description-header"> <div class="business-details"> <img class="business-thumbnail" src="img/lab-logo.jpg" alt="business logo"> <div class="business-name">Eko Hotels and Suites</div> </div> <span class="image-count">2/10</span> <div class="actions-container"> <!--<a href="#" class="like">Like</a>--> <a href="#" class="show-description"><i class="icon-menu"></i></a> </div> </div> <div class="gallery-description"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim id consequuntur iure molestiae debitis, soluta distinctio tempore maxime rerum, in ipsa vero! Quas eos, officiis consectetur hic voluptate nulla vero! </div> </div> </div> <div class="gallery-navigation-container"> <a href="#" class="gallery-previous disabled"><i class="icon-angle-left"></i></a> <a href="#" class="gallery-next"><i class="icon-angle-right"></i></a> </div> </div>'
		},
		init:function(){
			var _ = this,
					cf = _.config;

			// Update the cached container object
			cf.container = $(cf.container.selector);
			cf.galleryImageArrayEls = $('[' + this.attr_name('gallery') + '][' + this.attr_name('value') + ']');

			this.subscribe('vc:gallery/refresh', function(){
				_.init();
			});
			if(cf.container.length <= 0 && cf.galleryImageArrayEls.length <= 0)return false;

			// cf.imgsList = [];
			cf.galleryImgLists = {};

			//Get the image set from the container elements, seperate into different galleries by rel attribute
			cf.container.each(function(index){
				var rel = $(this).attr('rel') || cf.defaultRel;
				cf.galleryImgLists[rel] = cf.galleryImgLists[rel] || [];
				cf.galleryImgLists[rel].push({
					imageUrl: $(this).attr('href'),
					imageTitle: $(this).attr('title'),
					imageDescription: $(this).next('.description').html(),
					imageThumbUrl: $(this).find('img').attr('src'),
					imageUploadDate: $(this).data('upload-date'),
					imageUploaderName: $(this).data('uploader-name'),
					imageUploaderThumbnail: $(this).data('uploader-thumbnail'),
					imageBusinessThumbnail: $(this).data('business-thumbnail'),
					imageBusinessName: $(this).data('business-name')
				});
				$(this).attr('data-index', cf.galleryImgLists[rel].length-1);
				// cf.imgsList.push({
				// 	imageUrl: $(this).attr('href'),
				// 	imageTitle: $(this).attr('title'),
				// 	imageDescription: $(this).next('.description').html(),
				// 	imageThumbUrl: $(this).find('img').attr('src'),
				// 	imageUploadDate: $(this).data('upload-date'),
				// 	imageUploaderName: $(this).data('uploader-name'),
				// 	imageUploaderThumbnail: $(this).data('uploader-thumbnail'),
				// 	imageBusinessThumbnail: $(this).data('business-thumbnail'),
				// 	imageBusinessName: $(this).data('business-name')
				// });
			});

			console.log(cf.galleryImgLists);
			// Get the image set from the VC Gallery elements
			cf.galleryImageArrayEls.each(function(index){
				var curArray = JSON.parse($(this).attr(_.attr_name('value')));
				var rel = $(this).attr('rel') || cf.defaultRel;
				cf.galleryImgLists[rel] = cf.galleryImgLists[rel].concat(curArray);
				// cf.imgsList = cf.imgsList.concat(curArray);
				console.log(cf.galleryImgLists);
			});

			//Build the gallery
			for(var rel in cf.galleryImgLists){
				if(cf.galleryImgLists.hasOwnProperty(rel)){
					_.buildGallery(rel);
				}
			}

			// console.log('gallery activate!');
			this.events();
		},
		events:function(){
			// contains event bindings
			var _ = this,
					cf = _.config,
					distance = 0,
					minSwipeDistance = 10,
					isSwipeVertical = false,
					startCoords = {},
					endCoords = {},
					data = {};

			if (window.navigator.msPointerEnabled) {
				//Internet Explorer 10 style
				_eventTouchstart    = "MSPointerDown";
				_eventTouchmove     = "MSPointerMove";
				_eventTouchend      = "MSPointerUp";
			} else {
				_eventTouchstart    = "touchstart";
				_eventTouchmove     = "touchmove";
				_eventTouchend      = "touchend";
			}

			//Set click event for image set
			cf.container.off('click.vc.gallery').on('click.vc.gallery',function(e){
				e.preventDefault();
				var rel = $(this).attr('rel') || cf.defaultRel;
				// cf.curIndex = cf.container.index(this);
				cf.gallerySet[rel].curIndex = +$(this).attr('data-index');
				console.log(cf.galleryImgLists[rel][cf.gallerySet[rel].curIndex]);
				console.log(rel);
				if(!cf.gallerySet[rel].isOpen){
					_.openGallery(rel);
				}
			});
			$(document).off('keyup.vc.gallery').on('keyup.vc.gallery',function(e){
			  // handle cursor keys
			  for(var rel in cf.gallerySet){
			  	if(cf.gallerySet.hasOwnProperty(rel)){
					  switch(e.keyCode){
					  	case 37:
						    // left key
						    _.previousImage(rel);
					  		break;
					  	case 39:
						    // right key
						    _.nextImage(rel);
					  		break;
					  	case 27:
					  		// escape key
					  		_.closeGallery(rel);
					  		break;
					  	case 38:
					  		// up key
					  		_.showDescription(rel);
					  		break;
					  	case 40:
					  		// down key
					  		_.hideDescription(rel);
					  		break;
					  }
					}
			  }
			  console.log(e.keyCode);
			});
			$(window).off('resize.vc.gallery').on('resize.vc.gallery', function(e){
				// adjust size
				_.adjustSize();
			});

			//Set event listeners

			for(var rel in cf.gallerySet){
				if(cf.gallerySet.hasOwnProperty(rel)){
					cf.gallerySet[rel].$galleryCloseLink.off('click.vc.gallery').on('click.vc.gallery',function(e){
						e.preventDefault();
						_.closeGallery(rel);
					});
					cf.gallerySet[rel].$galleryPreviousLink.off('click.vc.gallery').on('click.vc.gallery',function(e){
						e.preventDefault();
						_.previousImage(rel);
					});
					cf.gallerySet[rel].$galleryNextLink.off('click.vc.gallery').on('click.vc.gallery',function(e){
						e.preventDefault();
						_.nextImage(rel);
					});
					cf.gallerySet[rel].$showDescriptionButton.off('click.vc.gallery').on('click.vc.gallery', function(e){
						e.preventDefault();
						_.toggleDescription(rel);
					});
					cf.gallerySet[rel].$galleryImageContainer.off(_eventTouchstart).on(_eventTouchstart, function(e){
						if(!e.touches) e = e.originalEvent;
						console.log('Touch started.');

						data = {
							startPageX: (window.navigator.msPointerEnabled) ? e.pageX : e.touches[0].clientX,
							startPageY: (window.navigator.msPointerEnabled) ? e.pageY : e.touches[0].clientY,
							startTime: +new Date(),
							deltaX: 0,
							deltaY: 0,
							endPageX: 0,
							endPageY: 0
						};

						e.stopPropagation();
					}).off(_eventTouchmove).on(_eventTouchmove, function(e){

						console.log('Touch moving.');

						e.preventDefault();
					}).off(_eventTouchend).on(_eventTouchend, function(e){
						if(!e.touches) e = e.originalEvent;
						// Ignore pinch/zoom events
						if(e.touches.length > 1 || e.scale && e.scale !== 1)return;
						console.log(e);
						data.endPageX = (window.navigator.msPointerEnabled) ? e.pageX : e.changedTouches[0].clientX,
						data.endPageY = (window.navigator.msPointerEnabled) ? e.pageY : e.changedTouches[0].clientY,

						data.deltaX = data.endPageX - data.startPageX;
						data.deltaY = data.endPageY - data.startPageY;
						isSwipeVertical = (Math.abs(data.deltaX) < Math.abs(data.deltaY))
						console.log('Touch stopped');
						if(isSwipeVertical){
							// show/hide description
							if(data.deltaY >= minSwipeDistance){
								_.hideDescription(rel);
							}
							if(data.deltaY <= -minSwipeDistance){
								_.showDescription(rel);
							}
						}
						else{
							// previous/next image
							if(data.deltaX >= minSwipeDistance){
								_.previousImage(rel);
							}
							if(data.deltaX <= -minSwipeDistance){
								_.nextImage(rel);
							}
						}
						data = {};
						e.stopPropagation();
					});
					// Add close action to big gallery
					cf.gallerySet[rel].$bigGallery.off('click.vc.gallery').on('click.vc.gallery', function(e){
						if(e.target == e.currentTarget){
							_.closeGallery(rel);
						}
					});
				}
			}
		},
		preloadImages:function(rel, index){
			var _ = this,
					cf = _.config;

			var index = index || cf.gallerySet[rel].curIndex;
			//preload large images
			var curImgList = cf.galleryImgLists[rel][index];

			// Get preload range for the current rel image lists
			var startIndex = (index - cf.preloadCount > 0) ? index : 0;
			var endIndex = startIndex + (cf.preloadCount * 2);
			endIndex = (endIndex > cf.galleryImgLists[rel].length) ? cf.galleryImgLists[rel].length - 1 : endIndex;
			for(var i = startIndex; i < endIndex; i++){
				(new Image()).src = cf.galleryImgLists[rel][i].imageUrl; //preload the image
			}

			console.log('In preloadImages function.');
		},
		buildGallery:function(rel){
			var _ = this,
					cf = _.config;
			//build gallery

			cf.gallerySet[rel] = cf.gallerySet[rel] || {};
			cf.gallerySet[rel].$bigGallery = $(cf.galleryTemplate);

			// cf.$bigGallery = $(cf.galleryTemplate);

			cf.gallerySet[rel].$galleryImageContainer = cf.gallerySet[rel].$bigGallery.find('.' + cf.galleryImageContainerClass);
			cf.gallerySet[rel].$galleryContainer = cf.gallerySet[rel].$bigGallery.find('.' + cf.galleryContainerClass);
			cf.gallerySet[rel].$galleryDescriptionContainer = cf.gallerySet[rel].$bigGallery.find('.' + cf.galleryDescriptionContainerClass);
			cf.gallerySet[rel].$galleryHeader = cf.gallerySet[rel].$bigGallery.find('.' + cf.galleryHeaderClass);
			cf.gallerySet[rel].$galleryDescriptionHeader = cf.gallerySet[rel].$bigGallery.find('.' + cf.galleryDescriptionHeaderClass);
			cf.gallerySet[rel].$galleryDescription = cf.gallerySet[rel].$bigGallery.find('.' + cf.galleryDescriptionClass);
			cf.gallerySet[rel].$galleryCloseLink = cf.gallerySet[rel].$bigGallery.find('.' + cf.galleryCloseLinkClass);
			cf.gallerySet[rel].$galleryPreviousLink = cf.gallerySet[rel].$bigGallery.find('.' + cf.galleryPreviousLinkClass);
			cf.gallerySet[rel].$galleryNextLink = cf.gallerySet[rel].$bigGallery.find('.' + cf.galleryNextLinkClass);
			cf.gallerySet[rel].$galleryImageCount = cf.gallerySet[rel].$bigGallery.find('.' + cf.galleryImageCountClass);
			cf.gallerySet[rel].$galleryLoading = cf.gallerySet[rel].$bigGallery.find('.' + cf.galleryLoadingClass);
			cf.gallerySet[rel].$galleryTitle = cf.gallerySet[rel].$bigGallery.find('.' + cf.galleryTitleClass);
			cf.gallerySet[rel].$galleryUploadDetails = cf.gallerySet[rel].$bigGallery.find('.' + cf.galleryUploadDetailsClass);
			cf.gallerySet[rel].$galleryUploadDate = cf.gallerySet[rel].$bigGallery.find('.' + cf.galleryUploadDateClass);
			cf.gallerySet[rel].$galleryUploaderName = cf.gallerySet[rel].$bigGallery.find('.' + cf.galleryUploaderNameClass);
			cf.gallerySet[rel].$galleryUploaderThumbnail = cf.gallerySet[rel].$bigGallery.find('.' + cf.galleryUploaderThumbnailClass);
			cf.gallerySet[rel].$galleryBusinessName = cf.gallerySet[rel].$bigGallery.find('.' + cf.galleryBusinessNameClass);
			cf.gallerySet[rel].$galleryBusinessThumbnail = cf.gallerySet[rel].$bigGallery.find('.' + cf.galleryBusinessThumbnailClass);
			cf.gallerySet[rel].$galleryBusinessDetails = cf.gallerySet[rel].$bigGallery.find('.' + cf.galleryBusinessDetailsClass);
			cf.gallerySet[rel].$galleryActionsContainer = cf.gallerySet[rel].$bigGallery.find('.' + cf.galleryActionsContainerClass);
			cf.gallerySet[rel].$showDescriptionButton = cf.gallerySet[rel].$bigGallery.find('.' + cf.showDescriptionButtonClass);

			$('body').append(cf.gallerySet[rel].$bigGallery);
			console.log(cf.gallerySet[rel].$bigGallery);

			// cf.imgsList.forEach(function(elem, index){
			// 	imgDiv = $('<div>').addClass(cf.galleryImageClass);
			// 	imgObj = $('<img>')
			// 		.load(function(){
			// 			$(this).hide();
			// 			$(this).fadeIn();
			// 		})
			// 		.error(function(){
			// 			console.log('Error loading image: ' + elem.imageUrl);
			// 		})
			// 		.attr('src',elem.imageUrl)
			// 		.attr('data-index',index);
			// 		console.log(imgObj);
			// 	imgDiv.append(imgObj)
			// 		.appendTo(cf.$galleryImageContainer);
			// });

			$.each(cf.galleryImgLists[rel], function(index, elem){
				imgDiv = $('<div>').addClass(cf.galleryImageClass);
				imgObj = $('<img>')
					.load(function(){
						$(this).hide();
						$(this).fadeIn();
					})
					.error(function(){
						console.log('Error loading image: ' + elem.imageUrl);
					})
					.attr('data-src',elem.imageUrl)
					.attr('data-index',index);
					console.log(imgObj);
				imgDiv.append(imgObj)
					.appendTo(cf.gallerySet[rel].$galleryImageContainer);
			});

			console.log('In buildGallery function.');
			cf.gallerySet[rel].isBuilt = true;
		},
		openGallery: function(rel){
			var _ = this,
					cf = _.config
					rel = rel || cf.defaultRel;
			//open the big image gallery
			if(!cf.gallerySet[rel].isBuilt){
				_.buildGallery(rel);
			}
			// Load content into gallery
			_.loadContent(rel);

			//show the gallery
			cf.gallerySet[rel].$bigGallery.fadeIn();

			// Adjust size
			_.adjustSize(rel);
			console.log('In openGallery function.');
			cf.gallerySet[rel].isOpen = true;
		},
		closeGallery:function(rel){
			var _ = this,
					cf = _.config;
			//close the big image gallery
			cf.gallerySet[rel].$bigGallery.fadeOut();

			//Cleanup process
			_.emptyGallery(rel);
			console.log('In closeGallery function.');
			cf.gallerySet[rel].isOpen = false;
		},
		loadContent:function(rel, callback){
			var _ = this,
					cf = _.config;

			if(typeof callback == 'undefined'){
				callback = function(){};
			}

			//show current image
			var curImgList = cf.galleryImgLists[rel][cf.gallerySet[rel].curIndex];

			// Preload images
			_.preloadImages(rel, cf.gallerySet[rel].curIndex);

			//empty gallery first
			_.emptyGallery(rel);

			//load the gallery content:
			console.log(curImgList);

			//-image description
			if(curImgList.imageDescription){
				cf.gallerySet[rel].hasDescription = true;
				cf.gallerySet[rel].$showDescriptionButton.removeClass('hidden');
			}
			else{
				cf.gallerySet[rel].hasDescription = false;
				_.hideDescription(rel);
				cf.gallerySet[rel].$showDescriptionButton.addClass('hidden');
			}
			cf.gallerySet[rel].$galleryDescription.html(curImgList.imageDescription);

			//-image content
			var curImgContainer = cf.gallerySet[rel].$galleryImageContainer.find('.'+cf.galleryImageClass).filter(':eq(' + cf.gallerySet[rel].curIndex + ')').css('margin-left', 0);
			curImgContainer.find('img').attr('src', curImgList.imageUrl);

			//-image count
			cf.gallerySet[rel].$galleryImageCount.html(cf.gallerySet[rel].curIndex+1 +' of ' + cf.galleryImgLists[rel].length);

			//-image title
			cf.gallerySet[rel].$galleryTitle.html(curImgList.imageTitle || "");

			//-upload details
			cf.gallerySet[rel].$galleryUploadDate.html(curImgList.imageUploadDate || "");
			cf.gallerySet[rel].$galleryUploaderName.html(curImgList.imageUploaderName || "");
			cf.gallerySet[rel].$galleryUploaderThumbnail.attr('src',curImgList.imageUploaderThumbnail || "");

			curImgList.imageUploadDate ? cf.gallerySet[rel].$galleryUploadDate.show() : cf.gallerySet[rel].$galleryUploadDate.hide();
			curImgList.imageUploaderName ? cf.gallerySet[rel].$galleryUploaderName.show() : cf.gallerySet[rel].$galleryUploaderName.hide();
			curImgList.imageUploaderThumbnail ? cf.gallerySet[rel].$galleryUploaderThumbnail.show() : cf.gallerySet[rel].$galleryUploaderThumbnail.hide();
			cf.gallerySet[rel].$galleryUploadDetails.show();

			//-business details
			cf.gallerySet[rel].$galleryBusinessName.html(curImgList.imageBusinessName || "");
			cf.gallerySet[rel].$galleryBusinessThumbnail.attr('src',curImgList.imageBusinessThumbnail || "");

			curImgList.imageBusinessThumbnail ? cf.gallerySet[rel].$galleryBusinessThumbnail.show() : cf.gallerySet[rel].$galleryBusinessThumbnail.hide();
			cf.gallerySet[rel].$galleryBusinessDetails.show();

			//-gallery controls state
			if(_.isFirst(rel))cf.gallerySet[rel].$galleryPreviousLink.addClass('disabled');
			else cf.gallerySet[rel].$galleryPreviousLink.removeClass('disabled');
			if(_.isLast(rel))cf.gallerySet[rel].$galleryNextLink.addClass('disabled');
			else cf.gallerySet[rel].$galleryNextLink.removeClass('disabled');
			console.log('In loadContent function.');

			callback();
		},
		previousImage:function(rel){
			var _ = this,
					cf = _.config;
			//show previous image
			if(!_.isFirst(rel)){
				cf.gallerySet[rel].curIndex--;
				_.loadContent(rel);
			}
			console.log('In previousImage function.');
		},
		nextImage:function(rel){
			var _ = this,
					cf = _.config;
			//show next image
			if(!_.isLast(rel)){
				cf.gallerySet[rel].curIndex++;
				_.loadContent(rel);
			}
			console.log('In nextImage function.');
		},
		isFirst:function(rel){
			var _ = this,
					cf = _.config;
			//check if first image
			return !cf.gallerySet[rel].curIndex;
		},
		isLast:function(rel){
			var _ = this,
					cf = _.config;
			//check if last image
			return (cf.gallerySet[rel].curIndex >= cf.galleryImgLists[rel].length-1);
		},
		emptyGallery:function(rel){
			var _ = this,
					cf = _.config;

			//empty the gallery
			cf.gallerySet[rel].$galleryImageContainer.find('.'+cf.galleryImageClass).css('margin-left', '100%');

			// cf.gallerySet[rel].$bigGallery.find('img.current-image').detach();

			//-image count
			cf.gallerySet[rel].$galleryImageCount.empty();

			//-image title
			cf.gallerySet[rel].$galleryTitle.empty();

			//-image description
			// cf.gallerySet[rel].$galleryDescriptionContainer.fadeOut();
			cf.gallerySet[rel].$galleryDescription.empty();

			//-upload details
			cf.gallerySet[rel].$galleryUploadDetails.hide();
			cf.gallerySet[rel].$galleryUploaderName.empty();
			cf.gallerySet[rel].$galleryUploadDate.empty();
			cf.gallerySet[rel].$galleryUploaderThumbnail.attr('src','');

			//-business details
			cf.gallerySet[rel].$galleryBusinessDetails.hide();
			cf.gallerySet[rel].$galleryBusinessName.empty();
			cf.gallerySet[rel].$galleryBusinessThumbnail.attr('src','');

			console.log('In emptyGallery function.');
		},
		adjustSize:function(rel){
			var _ = this,
					cf = _.config,
					imageHeight = 0,
					rel = rel || cf.defaultRel;

			console.log('Adjusting the size.');
			imageHeight = cf.gallerySet[rel].$galleryContainer.height() - cf.gallerySet[rel].$galleryHeader.height() - cf.gallerySet[rel].$galleryDescriptionHeader.height();
			cf.gallerySet[rel].$galleryContainer.find('.' + cf.galleryImageClass).height(imageHeight);
			cf.gallerySet[rel].$galleryContainer.find('.' + cf.galleryLoadingClass).height(imageHeight);
			console.log('Adjusted image height: ' + imageHeight);
		},
		showDescription:function(rel){
			var _ = this,
					cf = _.config;

			if(cf.gallerySet[rel].isOpen & cf.gallerySet[rel].hasDescription){
				cf.gallerySet[rel].$galleryDescription.addClass('show');
				cf.gallerySet[rel].$showDescriptionButton.addClass('active');
			}
			console.log('In showDescription function.');
		},
		hideDescription:function(rel){
			var _ = this,
					cf = _.config;

			if(cf.gallerySet[rel].isOpen){
				cf.gallerySet[rel].$galleryDescription.removeClass('show');
				cf.gallerySet[rel].$showDescriptionButton.removeClass('active');
			}
			console.log('In hideDescription function.');
		},
		toggleDescription:function(rel){
			var _ = this,
					cf = _.config;

			if(cf.gallerySet[rel].isOpen){
				if(cf.gallerySet[rel].$galleryDescription.hasClass('show')){
					_.hideDescription(rel);
				}
				else{
					_.showDescription(rel);
				}
			}
			console.log('In toggleDescription function.');
		}
	};
}(jQuery, window, document));
