var catalog = (function () {
	// Begin catalog scope vars
	var
		configMap = {},

		stateMap  = {
			isSetNewBrand      : undefined,
			isBranded          : undefined,
			cropResultsCounter : 0,
			$thumbnail         : undefined,
			cropResultData     : undefined,
			productName        : undefined
		},

		jqMap = {
			$productInput         : $('#product-input'),
			$supplierInput        : $('#supplier-input'),
			$mandText             : '<i class="err">*</i>',
			$cropModal            : $('#image-croper-modal'),
			$imageCropper         : $('#image-cropper'),
		  $cropResultsContainer : $('#crop_result_input_container'),
			$productDetails       : $('#product-details'),
			$productAttributes    : $('#product-attributes'),
			$cropitInput          : $('.cropit-image-input'),
			$validateErrorMsg     : $('#validate-err-msg'),
			$submitBtn            : $('#submit-btn')
		},

		// $selectProduct,

    // Module scope functions
    // setConfigMap,
    resetGalleryWidth,
		typeahead,
		showBrandField,
		offAddnew,
		submitProductDetails,
		showNewEntryField,
		showOriginalField,
		showProductAttributes,
		setNewbrand,
		applyCrop,
		saveCroppedData,
		openModal,
		showProductDetails,
		submitProduct,
		popValidateError,
		clearValidateError,
		validateSelectAdd,
		validateForm,
		init
	;
	// End catalog scope vars

	resetGalleryWidth = function () {
		var
			fieldset_width = $('fieldset').eq(0)
				// .css({'overflow' : 'hidden', 'width' : '100%'})
				.width() + 'px'
		;
	}

	typeahead = function ( ) {

		var config = {// the Bloodhood cofig
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
		  queryTokenizer: Bloodhound.tokenizers.whitespace,
		  limit: 7,
		  prefetch: {
		    // url points to a json file that contains an array of country names, see
		    // https://github.com/twitter/typeahead.js/blob/gh-pages/data/countries.json
		    url: '',
		    // the json file contains an array of strings, but the Bloodhound
		    // suggestion engine expects JavaScript objects so this converts all of
		    // those strings
		    filter: function(list) {
		      return $.map(list, function(i) { return { name: i }; });
		    }
		  }
		}, make_typeahead, businesses, products;

		make_typeahead = function (the_name, $el, url) {
			config.prefetch.url = url;

			the_name = new Bloodhound(config);
			the_name.initialize();
			$el.typeahead(null, {
			  name: 'the_name',
			  displayKey: 'name',
			  // `ttAdapter` wraps the suggestion engine in an adapter that
			  // is compatible with the typeahead jQuery plugin
			  source: the_name.ttAdapter()
			});
		}

		make_typeahead(businesses, jqMap.$supplierInput, configMap.businessURL);
		make_typeahead(products, jqMap.$productInput, configMap.productsURL);
	};

	showBrandField = function () {
		var $label_for_photo = $('label[for="photos-input"]');
		$('.label-for-brand').html('What is the ' + stateMap.productName + '\'s brand? '
			+ jqMap.$mandText);

		if ( $(this).attr('id') === 'is-branded' ) {
			stateMap.isBranded = true;
		  $('.unbranded').fadeOut(200, function ( ) {
		   	$('.select-brand').fadeIn(400);
		  });
		  $label_for_photo.find('i.err').remove();
		  $('.barcode').show();
		}
		else if ( $(this).attr('id') === 'is-not-branded' ) {
			stateMap.isBranded = false;
		  $('.select-brand,.model').fadeOut(200, function ( ) {
		   	$('.unbranded').fadeIn(400);
		  });
		  if (! $label_for_photo.children('i.err').length) {
		  	$label_for_photo.append(jqMap.$mandText);
		  };
		  $('.barcode').hide();
		}
		else {
		  return false;
		};
		$('#brand-group').fadeIn(400);
	};

	// offAddnew = function () {
	// 	var $add_new = $(this).parent().next(),
	// 	    text     = $(this).chosen().val().toLowerCase()
	// 	;

	// 	if ( text.length ) {
	// 	  $add_new.off('click')
	// 	  		.css({
	// 	  			'color':'#E7E7E7',
	// 	  			'cursor':'default'
	// 	  		})
	// 	  ;
	// 	}
	// };

	submitProductDetails = function ( ) {
		jqMap.$submitBtn.attr('disabled','true').off('click');
		jQuery.ajax({
			url: '../js/catalog/business.json',
			contentType: 'application/json',
			processData: false,
			success: function( data, textStatus, jqXHR ) {
	     showProductAttributes();
			},
			error: function (data, textStatus, jqXHR) {
				//....
			}
		});
	};

	showNewEntryField = function ( ) {
		$(this).parent().fadeOut(200, function ( ) {
			$(this).next().fadeIn(400);
		})
	};

	showOriginalField = function ( ) {
		var $this = $(this);
		$this.closest('.new-entery').fadeOut(200, function ( ) {
			$(this).prev().fadeIn(400);
		}).find('input').val('');

		if ( stateMap.isSetNewBrand ) {
			$('.random-model').fadeOut(200);
			stateMap.isSetNewBrand = false;
		};
	};

	setNewBrand = function ( ) {
		stateMap.isSetNewBrand = true;

		$(this).parent().fadeOut(200, function ( ) {
			$(this).next().fadeIn(400);
		}).next()
				.find("label[for='new-brand']")
					.html('New '
						    + stateMap.productName
						    + ' brand '
						    + jqMap.$mandText
						   )
		;

		$('.random-model').fadeIn(400)
											.find('label')
												.html('New '
													     + stateMap.productName
													     + ' model '
													     + jqMap.$mandText
													    )
		;
	};

	showProductDetails = function () {
		$('#first-stage').fadeOut(200,function () {
    	jqMap.$productDetails.fadeIn(400);
  	});
    stateMap.productName = jqMap.$productInput.val();

  	$('label.is-branded').html('Is the ' + stateMap.productName + ' branded? '
  	+ jqMap.$mandText );

    jqMap.$submitBtn
    	.fadeIn(400)
    	.on('click', function (e) {
    		e.preventDefault();
    		validateForm(true);
    	})
    ;
  }

  showProductAttributes = function  () {
		jqMap.$productDetails.fadeOut(200,function () {
			jqMap.$productAttributes.fadeIn(400);
			jqMap.$submitBtn.removeAttr('disabled')
			                .text('Submit')
			                .on('click',function (e) {
			                	e.preventDefault();
			                	validateForm();
			                });
		})
	}

  submitProduct = function () {
  	if (! jqMap.$productInput.val().length || ! jqMap.$supplierInput.val().length) {
			return false;
		}

		$('input.typeahead').blur();

  	jQuery.ajax({
			url: '../js/catalog/business.json',
			contentType: 'application/json',
			processData: false,
			success: function( data, textStatus, jqXHR ) {
	     showProductDetails();
			},
			error: function (data, textStatus, jqXHR) {
				//....
			}
		});
  }

  applyCrop = function (e) {
  	// display the croped image
  	var rc = stateMap.cropResultsCounter;
  	stateMap.$thumbnail = $(new Image())
	    .attr({
	    	'src' : stateMap.cropResultData,
	    	'alt' : 'product photo',
	    	'id'  : 'thumbnail-' + rc
	    });
  	var $div = $('<div class = "thumbnail-holder"></div>');
  	$div.append(stateMap.$thumbnail);
	  $('.upload-gallery-inner').append($div);
	  $('.photo-number').text(rc);
	  // stateMap.file_input_count += 1;
	  // stateMap.crop_is_applied  = true;
	  // close modal
	  jqMap.$cropModal.foundation('reveal', 'close');

	  if (rc === 5){
	  	$('.upload-btn').attr('disabled','true');
	  }

	  return false;
  }

  saveCroppedData = function () {
  	stateMap.cropResultData = jqMap.$imageCropper.cropit('export', {
																  type: 'image/jpeg',
																  quality: .9
																});

  	var $crop_result_input = '<input type="hidden" value="'+stateMap.cropResultData+'">';

  	jqMap.$cropResultsContainer.append($crop_result_input);
  	stateMap.cropResultsCounter = jqMap.$cropResultsContainer.find('input').length;
  	applyCrop();
  }

	openModal = function () {
		jqMap.$cropModal
    	.foundation('reveal','open');
	}

	popValidateError = function () {
		var args = arguments;
    $.each(arguments,function  (i) {
    	if (args[i].is('#branded-inputs-holder')
    		|| args[i].is('#upload-btn-count') ) {
    		args[i].css('border','1px solid #f00');
    	}
    	else {
    		args[i].css('border-color','#f00');
    	}
    });
    jqMap.$validateErrorMsg.show();
	}

	clearValidateError = function () {
		var args = arguments;
		$.each(arguments,function  (i) {
    	if (args[i].is('#branded-inputs-holder')
    		|| args[i].is('#upload-btn-count') ) {
    		args[i].css('border','none');
    	}
    	else {
    		args[i].css('border-color','#ccc');
    	}
    });
    jqMap.$validateErrorMsg.hide();
	}

	validateSelectAdd = function ($select) {
		var status;
		$.each($select, function () {
			var
				$this          = $(this),
				$input         = $this.closest('.row')
											        .next()
											        .find('input'),
				$chosen_input  = $this.next()
															.children('.chosen-single'),
				select_has_val = $this.val().length,
				input_has_val  = $input.val().length
			;

			if (!select_has_val && !input_has_val) {
				popValidateError($chosen_input,$input);
				status = false;
				return false;
			}
			else {
				clearValidateError($chosen_input,$input);
				status = true;
			};
		});

		return status;
	}

  validateForm = function (is_proDetails) {
  	if (is_proDetails) {
  		if( ! $('input[name=branded]:checked').length) {
				popValidateError($('#branded-inputs-holder'));
				return false;
			}
			else {
				clearValidateError($('#branded-inputs-holder'));
			};

			if (stateMap.isBranded) {
				if ( !validateSelectAdd($('#select-brand')) ) {
					return false;
				};
			};

			if ( !stateMap.isBranded && !stateMap.cropResultsCounter) {
			  popValidateError( $('#upload-btn-count'));
			  return false;
			}
			else {
				clearValidateError( $('#upload-btn-count'));
			};

			return submitProductDetails();
  	}
  	else {
    	if ( !validateSelectAdd( jqMap.$productAttributes.find('select') )) {
				return false;
			};

			if ( !$('#bundle-size').val().length) {
			  popValidateError( $('#bundle-size'));
			  return false;
			}
			else {
				clearValidateError( $('#bundle-size'));
			};

			if ( !$('#price').val().length) {
			  popValidateError( $('#price'));
			  return false;
			}
			else {
				clearValidateError( $('#price'));
			};

			if ( !$('#product-description').val().length) {
			  popValidateError( $('#product-description'));
			  return false;
			}
			else {
				clearValidateError( $('#product-description'));
			};

  	}

		$( "form" ).submit();
  };

  init = function (cfMap) {
    $(document).foundation();

  	$.extend(true, configMap, cfMap);
  	var cropitConfigMap = {
			onFileChange : openModal
		};
		typeahead();
		// Reset styles
		$(".catalog-select").chosen().next().css('min-width','100%');
		$(
			'#product-details,'
			+ '#product-attributes,'
			+ 'div.unbranded,'
			+ 'div.random-model,'
			+ 'div.model,'
			+ '.select-brand,'
			+ '.new-entery'
		).hide();
		$('span.tt-dropdown-menu').css('top','70%');
		resetGalleryWidth();

		// Attach events
	  $('input[name=branded]').on('click', showBrandField);
	  $('.add-new').on('click', showNewEntryField);
	  $('#add-new-brand').on('click', setNewBrand);
	  $('.cancel-add-new').on('click', showOriginalField);
	  $('.upload-btn').on('click', function (e) {
	  	jqMap.$cropitInput.click();
      e.preventDefault();
	  });
	  $('input').keypress(function (e) {
	  	if ( e.which == 13 ) {
	    	e.preventDefault();
	    }
	  });
	  // $('#select-brand').on('change', submitProductDetails);;
	  // $('select').on('change', offAddnew);
	  $('input.typeahead').on('typeahead:selected', submitProduct);
		$('.apply-crop').on('click',saveCroppedData);
		$('.cancel-crop').on('click',function () {
			jqMap.$cropModal
    		.foundation('reveal','close');
    		return false;
		});
		$(window).on('resize',function () {
			resetGalleryWidth();
		});

		jqMap.$imageCropper.cropit(cropitConfigMap);
	}

	return {init : init}

})();
