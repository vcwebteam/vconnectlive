;(function($, window, document, undefined){
	window.app.comps.profileTabs = {
		name: 'profileTabs',
		description: 'description',
		init:function(){
			// Contains the initialization code
			$('.profile-tabs .profile-tab').each(function(el) {
				// console.log($(this).children('a').attr('href'));
				if (!$(this).hasClass('active')) {
					$($(this).children('a').attr('href')).addClass('hide');
				};
				$(this).click(function(e) {
					e.preventDefault();
					if (!$(this).hasClass('active')) {
						var self = this,
							actv = $('.profile-tabs .profile-tab.active');

						$(actv).removeClass('active');
						$($(actv).children('a').attr('href')).addClass('hide');
						$('[data-header='+$($(actv).children('a').attr('href')).attr('data-footer')+']').addClass('hide');

						$(self).addClass('active');
						$($(self).children('a').attr('href')).removeClass('hide');
						$('[data-header='+$($(self).children('a').attr('href')).attr('data-footer')+']').removeClass('hide');

					};
				});
			});
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
		}
	};
}(jQuery, window, document));