;(function($, window, document, undefined){
	window.app.comps.photoSlider = {
		name: 'photoSlider',
		description: 'description',
		config: {
			photoSlider: {
				sildeStarted: false,
				gallerySlider: null /*$('.gallery-slider')*/,
				mainSection: $('.main-section'),
				gallery : $('.gallery-photos'),
				galleryImgs : [],
				closeButton : $('.back-nav-link'),
				currentImgPos: null,
				silderCaption :{
					reviewer: null /*$('.slider-caption').children('.reviewer')*/,
					comment : null /*$('.slider-caption').children('.comment')*/,
				},
				slidePositionHolder: null /*$('.slider-position-counter')*/,
				currentImgHolder:  null /*$('.silder-img').children('img')*/,
				prevButton: null /*$('.silder-nav.left')*/,
				nextButton: null /*$('.silder-nav.right')*/,
				startPos: (window.location.hash) ?
							(
								(window.location.hash.indexOf("slide-") > -1) ? window.location.hash.charAt(window.location.hash.indexOf("slide-")+6 ) : null
							) : null,
				galleryContainer: $('.photo-gallery-container'),
				loderImg: null,
				slideHTML: "<div class=\"gallery-slider hide\"><div class=\"slider-pagination\"><div class=\"small-4 columns collapse\"><a href=\"#\" class=\"silder-nav left\"><i class=\"icon-angle-left\"></i></a></div><div class=\"small-4 columns collapse\"><p class=\"text-center slider-position-counter\">0/0</p></div><div class=\"small-4 columns collapse\"><a href=\"#\" class=\"silder-nav right\"><i class=\"icon-angle-right\"></i></a></div></div><div class=\"silder-img\"><div class=\"mobile-container\"><div class=\"loader\"></div><img src=\"#\" alt=\"picture\"></div></div><div class=\"slider-caption\"><div class=\"mobile-container\"><h5 class=\"reviewer\"></h5><p class=\"comment\"></p></div></div></div>"
			}
		},
		init:function(){
			// Contains the initialization code
			var self = this;
			if (self.config.photoSlider.gallery.length >= 1) {
				$('.mobile.body').append(self.config.photoSlider.slideHTML);
				self.config.photoSlider.gallerySlider = $('.gallery-slider');
				self.config.photoSlider.silderCaption.reviewer = $('.slider-caption .mobile-container').children('.reviewer');
				self.config.photoSlider.silderCaption.comment = $('.slider-caption .mobile-container').children('.comment');
				self.config.photoSlider.slidePositionHolder = $('.slider-position-counter');
				self.config.photoSlider.currentImgHolder = $('.silder-img .mobile-container').children('img');
				self.config.photoSlider.prevButton = $('.silder-nav.left');
				self.config.photoSlider.nextButton = $('.silder-nav.right');
				self.config.photoSlider.loderImg = $('.silder-img .loader');

				self.prepareImgs(self.config.photoSlider.gallery);
				self.handelClose();
				self.handelPrev();
				self.handelNext();
				self.config.photoSlider.gallerySlider.hide();
				self.config.photoSlider.gallerySlider.addClass('hide');
				self.config.photoSlider.loderImg.hide();

				// if(startPos){
				// 	self.config.photoSlider.startPos = startPos;
				// }
				if(self.config.photoSlider.startPos){
					self.startSlide(parseInt(self.config.photoSlider.startPos));
				}
				//console.log('jjj');

			};

			this.events();
		},
		prepareImgs: function (gallery) {
			var self = this;

			$(gallery).children('li').each(function(i){
				var details = {
					'img' : $(this).children('a').attr('href'),
					'uploader' : $(this).children('a').attr('data-uploder'),
					'comment' : $(this).children('a').attr('data-comment'),
					'business': $(this).children('a').children('img').attr('alt')
				}
				self.config.photoSlider.galleryImgs.push(details);
				$(this).children('a').click(function (e) {
					e.preventDefault();
					self.startSlide(i);
				});

			});
		},

		startSlide : function (imgPos) {
			var self = this,
				windowHeight = window.innerHeight+(window.innerHeight/100 * 6);

			self.config.photoSlider.gallerySlider.css({'height': windowHeight, 'overflow-y': 'scroll'});
			self.config.photoSlider.mainSection.css({'height' : windowHeight, 'overflow-y': 'hidden'});
			self.config.photoSlider.sildeStarted = true;
			self.config.photoSlider.galleryContainer.hide();
			self.config.photoSlider.gallerySlider.show();
			self.config.photoSlider.gallerySlider.removeClass('hide');

			if (imgPos != self.config.photoSlider.currentImgPos) {
				self.config.photoSlider.loderImg.show();
				window.setTimeout(function () {
					self.config.photoSlider.currentImgHolder[0].src = self.config.photoSlider.galleryImgs[imgPos].img;
					//console.log(self.config.photoSlider.galleryImgs[imgPos]);
					self.config.photoSlider.currentImgHolder[0].alt =  self.config.photoSlider.galleryImgs[imgPos].business;
				}, 800);
				self.config.photoSlider.currentImgHolder.load(function () {
					//self.config.photoSlider.loderImg.show();
			        if(!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0){
			        	self.config.photoSlider.loderImg.show();
			        	console.log(this.complete);
			        }

			        self.config.photoSlider.loderImg.hide();
			        self.updateCounter(imgPos);
					self.config.photoSlider.silderCaption.reviewer[0].innerHTML = self.config.photoSlider.galleryImgs[imgPos].uploader;
					self.config.photoSlider.silderCaption.comment[0].innerHTML = self.config.photoSlider.galleryImgs[imgPos].comment;
				});

			};

		},

		prev: function () {
			console.log('prev');
			var self = this,
				imgPos = self.config.photoSlider.currentImgPos;

			if (imgPos < 1) {
				$(self.config.photoSlider.prevButton).addClass('disabled');
			} else{
				imgPos = imgPos - 1;
				self.startSlide(imgPos);
			};

		},

		next: function () {
			console.log('nexr');
			var self = this,
				imgPos = self.config.photoSlider.currentImgPos;

			if (imgPos + 2 > self.config.photoSlider.galleryImgs.length) {
				$(self.config.photoSlider.nextButton).addClass('disabled');
			} else{
				imgPos = imgPos + 1;
				self.startSlide(imgPos);
			};

		},

		handelClose: function () {
			var self = this;

			self.config.photoSlider.closeButton.click(function (e) {
				if (self.config.photoSlider.sildeStarted) {
					e.preventDefault();
					self.config.photoSlider.gallerySlider.hide();
					self.config.photoSlider.gallerySlider.addClass('hide');
					self.config.photoSlider.galleryContainer.show();
					self.config.photoSlider.sildeStarted = false;
					self.config.photoSlider.gallerySlider.css({'height': '', 'overflow-y': ''});
					self.config.photoSlider.mainSection.css({'height' : '', 'overflow-y': ''});
				}
			});
		},


		handelPrev: function () {
			var self =  this;

			$(self.config.photoSlider.prevButton[0]).click(function (e) {
				e.preventDefault();
				self.prev();
			})
		},

		handelNext: function () {
			var self =  this;

			$(self.config.photoSlider.nextButton[0]).click(function (e) {
				e.preventDefault();
				self.next();
			})
		},
		handleHash: function() {
			var self = this;
			//console.log(window.location.hash);

			self.config.photoSlider.startPos = (window.location.hash.indexOf("slide-") > -1) ?
									 window.location.hash.charAt(window.location.hash.indexOf("slide-")+6 ) :
									 null;
			if(self.config.photoSlider.startPos){
				//alert('hey');
				self.startSlide(parseInt(self.config.photoSlider.startPos));
			}
		},

		updateCounter: function (imgPos) {
			var self = this;

			self.config.photoSlider.currentImgPos = imgPos;
			self.config.photoSlider.slidePositionHolder[0].innerHTML = (imgPos +1)+'/'+self.config.photoSlider.galleryImgs.length;
		},

		events:function(){
			// Contains the event bindings and subscriptions
			var self = this;
			$(window).on('hashchange', function () {
				self.handleHash()
			});
			$(window).trigger('hashchange');
		}
	};
}(jQuery, window, document));
