;(function($, window, document, undefined){
	window.app.comps.changeProductView = {
		name: 'changeProductView',
		description: 'description',
		init:function(){
			// Contains the initialization code
			$('[data-list-switch]').click(function (e) {
				console.log(e);
				e.preventDefault();
				var currentDisplay = $('.item-list').hasClass('grid') ? 'grid' : 'list';
				if (currentDisplay == 'grid') {
					$('.item-list').addClass('list').removeClass('grid');
					$('.cart-items').addClass('list').removeClass('grid');
					$('.store-items-list').addClass('list').removeClass('grid');
					$('[data-list-switch] > i').addClass('icon-th').removeClass('icon-list');
				} else{
					$('.item-list').addClass('grid').removeClass('list');
					$('.cart-items').addClass('grid').removeClass('list');
					$('.store-items-list').addClass('grid').removeClass('list');
						$('[data-list-switch] > i').removeClass('icon-th').addClass('icon-list')
				};
			});
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
		}
	};
}(jQuery, window, document));
