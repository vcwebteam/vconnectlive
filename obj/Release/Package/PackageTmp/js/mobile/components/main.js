(function () {
	Vc = function () {
		$(document).foundation();	
		Foundation.libs.abide.settings.patterns.password = /(?=^.{0,}$)[a-zA-Z0-9-\W+]+$/; /*/(?=^.{6,}$)([a-zA-Z0-9]+$)/;*/
		Foundation.libs.abide.settings.patterns.email = /[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/;
		//alert('foundation inited');	
	}

	// Photo slider for the photos page on mobile
	Vc.prototype.photoSlider = {

		settings : {
			sildeStarted: false,
			gallerySlider: null /*$('.gallery-slider')*/,
			mainSection: $('.main-section'),
			gallery : $('.gallery-photos'),
			galleryImgs : [],
			closeButton : null,
			currentImgPos: null,
			silderCaption :{
				reviewer: null /*$('.slider-caption').children('.reviewer')*/,
				comment : null /*$('.slider-caption').children('.comment')*/,
			},
			slidePositionHolder: null /*$('.slider-position-counter')*/,
			currentImgHolder:  null /*$('.silder-img').children('img')*/,
			prevButton: null /*$('.silder-nav.left')*/,
			nextButton: null /*$('.silder-nav.right')*/,
			startPos: (window.location.hash) ? 
						(
							(window.location.hash.indexOf("slide-") > -1) ? window.location.hash.charAt(window.location.hash.indexOf("slide-")+6 ) : null
						) : null,
			galleryContainer: $('.photo-gallery-container'),
			loderImg: null,
			slideHTML: "<div class=\"gallery-slider hide\"><div class=\"slider-pagination\"><div class=\"small-4 columns collapse\"><a href=\"#\" class=\"silder-nav left\"><i class=\"icon-angle-left\"></i></a></div><div class=\"small-4 columns collapse\"><p class=\"text-center slider-position-counter\">0/0</p></div><div class=\"small-4 columns collapse\"><a href=\"#\" class=\"silder-nav right\"><i class=\"icon-angle-right\"></i></a></div></div><div class=\"silder-img\"><div class=\"mobile-container\"><div class=\"loader\"></div><img src=\"#\" alt=\"picture\"></div></div><div class=\"slider-caption\"><div class=\"mobile-container\"><h5 class=\"reviewer\"></h5><p class=\"comment\"></p></div></div></div>"
		},

		init : function (closeButton, startPos) {
			var self = this;
			if (self.settings.gallery.length >= 1) {
				$('.mobile.body').append(self.settings.slideHTML);
				self.settings.gallerySlider = $('.gallery-slider');
				self.settings.silderCaption.reviewer = $('.slider-caption .mobile-container').children('.reviewer');
				self.settings.silderCaption.comment = $('.slider-caption .mobile-container').children('.comment');
				self.settings.slidePositionHolder = $('.slider-position-counter');
				self.settings.currentImgHolder = $('.silder-img .mobile-container').children('img');
				self.settings.prevButton = $('.silder-nav.left');
				self.settings.nextButton = $('.silder-nav.right');
				self.settings.loderImg = $('.silder-img .loader');
				self.settings.closeButton = closeButton;

				self.prepareImgs(self.settings.gallery);
				self.handelClose();
				self.handelPrev();
				self.handelNext();
				self.settings.gallerySlider.hide();	
				self.settings.gallerySlider.addClass('hide');
				self.settings.loderImg.hide();

				if(startPos){
					self.settings.startPos = startPos;
				}
				if(self.settings.startPos){
					self.startSlide(parseInt(self.settings.startPos));
				}
				$(window).on('hashchange', function () {
					self.handleHash()
				});
				$(window).trigger('hashchange');
				//console.log('jjj');

			};	
		},

		prepareImgs: function (gallery) { 
			var self = this;

			$(gallery).children('li').each(function(i){
				var details = {
					'img' : $(this).children('a').attr('href'),
					'uploader' : $(this).children('a').attr('data-uploder'),
					'comment' : $(this).children('a').attr('data-comment')
				}
				self.settings.galleryImgs.push(details);
				$(this).children('a').click(function (e) {
					e.preventDefault();
					self.startSlide(i);
				});

			});
		},

		startSlide : function (imgPos) {
			var self = this,
				windowHeight = window.innerHeight+(window.innerHeight/100 * 6);

			self.settings.gallerySlider.css({'height': windowHeight, 'overflow-y': 'scroll'});
			self.settings.mainSection.css({'height' : windowHeight, 'overflow-y': 'hidden'});
			self.settings.sildeStarted = true;
			self.settings.galleryContainer.hide();
			self.settings.gallerySlider.show();
			self.settings.gallerySlider.removeClass('hide');

			if (imgPos != self.settings.currentImgPos) {
				self.settings.loderImg.show();
				window.setTimeout(function () {
					self.settings.currentImgHolder[0].src = self.settings.galleryImgs[imgPos].img;
				}, 800);
				self.settings.currentImgHolder.load(function () {
					//self.settings.loderImg.show();
			        if(!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0){
			        	self.settings.loderImg.show();
			        	console.log(this.complete);
			        }

			        self.settings.loderImg.hide();
			        self.updateCounter(imgPos);
					self.settings.silderCaption.reviewer[0].innerHTML = self.settings.galleryImgs[imgPos].uploader;
					self.settings.silderCaption.comment[0].innerHTML = self.settings.galleryImgs[imgPos].comment;
				});
				
			};
		
		},

		prev: function () {
			console.log('prev');
			var self = this,
				imgPos = self.settings.currentImgPos;

			if (imgPos < 1) {
				$(self.settings.prevButton).addClass('disabled');
			} else{
				imgPos = imgPos - 1;
				self.startSlide(imgPos);
			};

		},

		next: function () {
			console.log('nexr');
			var self = this,
				imgPos = self.settings.currentImgPos;

			if (imgPos + 2 > self.settings.galleryImgs.length) {
				$(self.settings.nextButton).addClass('disabled');
			} else{
				imgPos = imgPos + 1;
				self.startSlide(imgPos);
			};

		},

		handelClose: function () {
			var self = this;

			self.settings.closeButton.click(function (e) {
				if (self.settings.sildeStarted) {
					e.preventDefault();
					self.settings.gallerySlider.hide();
					self.settings.gallerySlider.addClass('hide');
					self.settings.galleryContainer.show();
					self.settings.sildeStarted = false;
					self.settings.gallerySlider.css({'height': '', 'overflow-y': ''});
					self.settings.mainSection.css({'height' : '', 'overflow-y': ''});
				}
			});
		},


		handelPrev: function () {
			var self =  this;

			$(self.settings.prevButton[0]).click(function (e) {
				e.preventDefault();
				self.prev();
			})
		},

		handelNext: function () {
			var self =  this;

			$(self.settings.nextButton[0]).click(function (e) {
				e.preventDefault();
				self.next();
			})
		},
		handleHash: function() {
			var self = this;
			//console.log(window.location.hash);

			self.settings.startPos = (window.location.hash.indexOf("slide-") > -1) ? 
									 window.location.hash.charAt(window.location.hash.indexOf("slide-")+6 ) : 
									 null;
			if(self.settings.startPos){
				//alert('hey');
				self.startSlide(parseInt(self.settings.startPos));
			}
		},

		updateCounter: function (imgPos) {
			var self = this;

			self.settings.currentImgPos = imgPos;
			self.settings.slidePositionHolder[0].innerHTML = (imgPos +1)+'/'+self.settings.galleryImgs.length;
		}
	}

	Vc.prototype.searchNav = {
		settings : {
			searchButton : $('#search-nav'),
            normalHeader : $('#normal-header'),
            searchHeader : $('#search-header'),
            mainSection  : $('.main-section'),
            overLayHtml  : "<div class='search-overlay' style='display:block'></div>",
            searchHeaderClose : $('.close-nav-link')
		},

		init : function () {
			var self = this;
			$(self.settings.searchButton).click(function  (e) {
                e.preventDefault();
                self.settings.normalHeader.hide();
                self.settings.mainSection.prepend(self.settings.overLayHtml);
                self.settings.searchHeader.show();
                
            });
            $(self.settings.searchHeaderClose).click(function (e) {
                e.preventDefault();
                self.settings.searchHeader.hide();
                $('.search-overlay').remove();
                self.settings.normalHeader.show();
            });
		}
	}

	Vc.prototype.cookies = {

		createCookie: function(name, value, days) {
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                var expires = "; expires=" + date.toGMTString();
            }
            else var expires = "";
            document.cookie = name + "=" + value + expires + "; path=/";
        },

        getCookie: function(c_name) {
            if (document.cookie.length > 0) {
                c_start = document.cookie.indexOf(c_name);
                if (c_start != -1) {
                    c_start = c_start + c_name.length + 1;
                    c_end = document.cookie.indexOf(";", c_start);
                    if (c_end == -1) {
                        c_end = document.cookie.length;
                    }
                    return unescape(document.cookie.substring(c_start, c_end));
                }
            }
            return false;
        }
	};

	Vc.prototype.getlocation =  {
		init : function () {

			var self = this;
			// console.log(Vc.prototype.cookies.getCookie('_getLoc'));
			// console.log(!(Vc.prototype.cookies.getCookie('_getLoc'))? true : false);
			if ("geolocation" in navigator && 
				!Vc.prototype.cookies.getCookie('_getLoc') ||
				!Vc.prototype.cookies.getCookie('_lat') || 
				!Vc.prototype.cookies.getCookie('_log')) {

				console.log("geolocation is avaliable")
				var geo_options = {
					enableHighAccuracy: true, 
					maximumAge        : 30000, 
					timeout           : 27000
				};
				navigator.geolocation.getCurrentPosition(this.success, this.error, geo_options);
				
			}
		},


		success: function (position) {
			var self       = this;
				loc_coords = position.coords,
				latitude   = loc_coords.latitude,
				longitude  = loc_coords.longitude,
				accuracy   = loc_coords.accuracy;

			$.ajax({
                url:"http://maps.googleapis.com/maps/api/geocode/json?latlng="+position.coords.latitude+","+position.coords.longitude+"&sensor=true",
                success:function(result){
                    if (result.status === "OK") {
                         $.each(result.results, function(i){
                         	Vc.prototype.cookies.createCookie('_'+this.types[0],this.formatted_address, 1/4);
                         });
                    }else{
                        console.warn(result.status);
                    }
                }
            });

 			Vc.prototype.cookies.createCookie("_lat",latitude, 1/4);
 			Vc.prototype.cookies.createCookie("_log",longitude, 1/4);
 			Vc.prototype.cookies.createCookie("_accuracy",accuracy, 1/4);
 			Vc.prototype.setLocationBoxValue();
		},

		error: function (error) {
			console.warn('ERROR(' + error.code + '): ' + error.message);
			Vc.prototype.cookies.createCookie("_getLoc",false, 1/4);
		}
	};

	Vc.prototype.fancyPassword = {
		settings : {
			maskToggle     : null, 
			passwordElement: null, 
			// toggleELement  : null,
			// password       : null, 
			toggeled       : null 
		},

		init: function () {
			var self  = this;
			self.settings.maskToggle      = $('#toggle-mask'),
			self.settings.passwordElement = $("input[type='password']"),
			//self.settings.toggleELement   = $('#password-toggle'),
			//self.settings.password        =  self.settings.passwordElement.val(),
			self.settings.toggeled        =  ($('#toggle-mask:checked').length === 1)? true : false,
			//self.settings.toggleELement.hide();

			self.settings.maskToggle.click(function (e) {
				self.settings.toggeled = ($('#toggle-mask:checked').length === 1)? true : false;
				//self.settings.password =  self.settings.passwordElement.val();
				self.toggle(self.settings.toggeled, /*self.settings.password,*/ self.settings.passwordElement /*self.settings.toggleELement*/);
			})
		},

		toggle: function (toggle, passwordEle) {
			var self = this;

			passwordEle.attr('type', (toggle) ? 'text' : 'password');
			// if (toggle) {
			// 	passwordEle.hide();
			// 	toggleEle.val(self.settings.password);
			// 	toggleEle.show();
			// 	toggleEle.removeClass('hide');

			// 	toggleEle.keyup(function() {
			// 		self.settings.password = toggleEle.val();
			// 		passwordEle.val(self.settings.password);
			// 	})

			// } else{
			// 	passwordEle.show();
			// 	toggleEle.val(self.settings.password);
			// 	toggleEle.hide();
			// 	toggleEle.addClass('hide');

			// 	passwordEle.keyup(function() {
			// 		self.settings.password = passwordEle.val();
			// 		toggleEle.val(self.settings.password);
			// 	})
			// };
		}
	};

/*	Vc.prototype.fixInputScroll = function() {
		$(document).on('touchstart', function (e) {
			if (e.target.nodeName === 'INPUT') {
				alert('Hello');
				console.log(e);

				$(window).scroll(function (e) {
					console.log(e);
				});

		        //e.preventDefault();

		    }
		});
		$('input').focus(function() {
		  alert( "Handler for .focus() called." );
		});
	};*/

	Vc.prototype.autoComplete = {

		settings: {
			keyWordEngine: null,
			locationEngine: null,
			keywordSearch: $('.keyword-autocomplete'),
			locationSearch: $('.location-autocomplete'),
			keywordDatasource : null,
			locationDatasource: null,
			bodyWidth:window.innerWidth,
			leftOffest: $('.search-location .location-placeholder').outerWidth()
		},

		init: function (keywordDatasource, locationDatasource) {
			// body...
			var self = this;
			self.settings.keywordDatasource = (keywordDatasource)? keywordDatasource: '../HomeWEB/KeywordAutoComplete?term=%QUERY';
			self.settings.locationDatasource = (locationDatasource)? locationDatasource: '../HomeWEB/LocationAutoComplete?term=%QUERY';
			
			self.keywordAutoComplete();
			self.locationAutoComplete();
			self.fixSuggestionsWidth();
			//$('#search-header').children().find('.twitter-typeahead').css({'width': self.settings.bodyWidth, 'overflow-x': 'hidden'});
			//$('#search-header').children().find('.twitter-typeahead').css({'width': 'inherit', 'overflow-x': 'hidden'});
			//$('#search-header').children().find('.twitter-typeahead').children('.tt-dropdown-menu').css({'width': self.settings.bodyWidth+'px !important', 'overflow-x': 'hidden'});
			
			$( window ).resize(function() {
				self.settings.bodyWidth = window.innerWidth;
			  	self.fixSuggestionsWidth();
			});
			if ('orientation' in window) {
				self.settings.bodyWidth = window.innerWidth;
				window.addEventListener('orientationchange', self.fixSuggestionsWidth());
			}
			// if('matchMedia' in window){
			// 	var mql = window.matchMedia("(orientation: portrait)");
			// 	mql.addListener(function(m) {
			// 		if(m.matches) {
			// 			alert('portrait! Calculating....');
			// 			self.fixSuggestionsWidth();
			// 			alert('Done')
			// 		}
			// 		else {
			// 			alert('Landscape! Calculating....');
			// 			self.fixSuggestionsWidth();
			// 			alert('Done');
			// 		}
			// 	});

			// }
			self.fixOverlapbug();
		},

		keywordAutoComplete: function () {
			var self = this;
			self.settings.keyWordEngine = new Bloodhound({
			datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.label); },
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				limit: 5,
				remote: {
					url: self.settings.keywordDatasource,
				},
				local: [
				    { label: 'one' },
				    { label: 'two' },
				    { label: 'three' },
				    { label: 'four' },
				    { label: 'five' },
				    { label: 'six' },
				    { label: 'seven' },
				    { label: 'eight' },
				    { label: 'nine' },
				    { label: 'ten' }
	  			]
			});

			// self.settings.keyWordEngine.remote.url = self.settings.keywordDatasource;
			self.settings.keyWordEngine.initialize();
			// instantiate the typeahead UI
			self.settings.keywordSearch.typeahead(null, {
			  displayKey: 'label',
			  source: self.settings.keyWordEngine.ttAdapter()
			});
		},

		locationAutoComplete: function () {
			var self = this;
			self.settings.locationEngine = new Bloodhound({
			datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.label); },
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				limit: 5,
				remote: {
					url: self.settings.locationDatasource,
				},
				local: [
				    { label: 'one' },
				    { label: 'two' },
				    { label: 'three' },
				    { label: 'four' },
				    { label: 'five' },
				    { label: 'six' },
				    { label: 'seven' },
				    { label: 'eight' },
				    { label: 'nine' },
				    { label: 'ten' }
	  			]
			});
			// self.settings.locationEngine.remote.url = self.settings.locationDatasource;
			self.settings.locationEngine.initialize();
			// instantiate the typeahead UI
			self.settings.locationSearch.typeahead(null, {
			  displayKey: 'label',
			  source: self.settings.locationEngine.ttAdapter()
			});
		},

		fixOverlapbug: function () {
			var self=  this;

			$.each(self.settings.keywordSearch, function () {
				var placeholder = $(this).attr('placeholder');
				//console.log(placeholder);
				$(this).focus(function  () {
					//console.log('focus');
					$(this).attr('placeholder', '');
				});
				$(this).blur(function  () {
					//console.log('blur');
					if ($(this).val().length < 1) {
						$(this).attr('placeholder', placeholder);
					};
				});
				$(this).change(function  () {
					//console.log('change');
					if ($(this).val().length < 1 ) {
						$(this).attr('placeholder', placeholder);
					};
				});
			});

			$.each(self.settings.locationSearch, function () {
				var placeholder = $(this).attr('placeholder');
				//console.log(placeholder);
				$(this).focus(function  () {
					//console.log('focus');
					$(this).attr('placeholder', '');
				});
				$(this).blur(function  () {
					//console.log('blur');
					if ($(this).val().length < 1) {
						$(this).attr('placeholder', placeholder);
					};
				});
				$(this).change(function  () {
					//console.log('change');
					if ($(this).val().length < 1 ) {
						$(this).attr('placeholder', placeholder);
					};
				});
			})
		},

		fixSuggestionsWidth: function () {
			var self = this;
			$.each($('#search-header').children().find('.twitter-typeahead').children('.tt-dropdown-menu'), function () {
				//console.log(self.settings.bodyWidth);
				// $(this).css({
				// 	'width': self.settings.bodyWidth+'px !important'
				// });
				$(this).width(self.settings.bodyWidth);
				$('.search-location .tt-dropdown-menu')[0].style.left = self.settings.leftOffest+'px';
				//console.log($('.search-location .tt-dropdown-menu'));
				//console.log($('.search-location .tt-dropdown-menu')[0].style.left)
				//console.log(self.settings.leftOffest)
				//console.log($(this).width())
				//return $(this).width();
			});
		}
	};

	Vc.prototype.setLocationBoxValue = function() {
		var pageHasLocationBox = (
			$('.location-box').length >= 1 && 
			Vc.prototype.cookies.getCookie('_sublocality')
			)? true : false;
		if (pageHasLocationBox) {
			var location  = Vc.prototype.cookies.getCookie('_sublocality');
				location = location.split(',')[1];
				// console.log(location);
				// console.log($('.location-box'));
				$('.location-box').each(function() {
					if ($(this).val().length == 0) {
						if ($(this).hasClass('directions-box')) {
							location  = Vc.prototype.cookies.getCookie('_neighborhood');
						}
						$(this).val(location);
					};
				});
				// console.log($('.location-box').val());
			/**/
		};
	};

	Vc.prototype.profileTabs = function() {
		//console.log($('.profile-tabs .profile-tab'));
		$('.profile-tabs .profile-tab').each(function(el) {
			// console.log($(this).children('a').attr('href'));
			if (!$(this).hasClass('active')) {
				$($(this).children('a').attr('href')).addClass('hide');
			};
			$(this).click(function(e) {
				e.preventDefault();
				if (!$(this).hasClass('active')) {
					var self = this,
						actv = $('.profile-tabs .profile-tab.active');

					$(actv).removeClass('active');
					$($(actv).children('a').attr('href')).addClass('hide');
					$('[data-header='+$($(actv).children('a').attr('href')).attr('data-footer')+']').addClass('hide');

					$(self).addClass('active');
					$($(self).children('a').attr('href')).removeClass('hide');
					$('[data-header='+$($(self).children('a').attr('href')).attr('data-footer')+']').removeClass('hide');

				};
			});
		});
	};

})();

$app = new Vc();
//alert('created vc object');
$app.setLocationBoxValue();
$app.photoSlider.init($('.back-nav-link'));
$app.searchNav.init();
$app.getlocation.init();
$app.fancyPassword.init();
$app.autoComplete.init( 
	'../HomeWEB/KeywordAutoComplete?term=%QUERY', 
	'../HomeWEB/LocationAutoComplete?term=%QUERY'
);
$app.profileTabs();
// $app.fixInputScroll();