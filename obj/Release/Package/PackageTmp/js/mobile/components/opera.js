(function () {
	Vc = function () {
		$(document).foundation();
		Foundation.libs.abide.settings.patterns.password = /(?=^.{0,}$)[a-zA-Z0-9-\W+]+$/; /*/(?=^.{6,}$)([a-zA-Z0-9]+$)/;*/
		Foundation.libs.abide.settings.patterns.email = /[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/;
		//alert('foundation inited');
	}

	// Photo slider for the photos page on mobile
	Vc.prototype.photoSlider = {

		settings : {
			sildeStarted: false,
			gallerySlider: null /*$('.gallery-slider')*/,
			mainSection: $('.main-section'),
			gallery : $('.gallery-photos'),
			galleryImgs : [],
			closeButton : null,
			currentImgPos: null,
			silderCaption :{
				reviewer: null /*$('.slider-caption').children('.reviewer')*/,
				comment : null /*$('.slider-caption').children('.comment')*/,
			},
			slidePositionHolder: null /*$('.slider-position-counter')*/,
			currentImgHolder:  null /*$('.silder-img').children('img')*/,
			prevButton: null /*$('.silder-nav.left')*/,
			nextButton: null /*$('.silder-nav.right')*/,
			slideHTML: "<div class=\"gallery-slider\"><div class=\"mobile-container\"><div class=\"slider-pagination\"><div class=\"small-4 columns collapse\"><a href=\"#\" class=\"silder-nav left\"><i class=\"icon-angle-left\"></i></a></div><div class=\"small-4 columns collapse\"><p class=\"text-center slider-position-counter\">0/0</p></div><div class=\"small-4 columns collapse\"><a href=\"#\" class=\"silder-nav right\"><i class=\"icon-angle-right\"></i></a></div></div><div class=\"silder-img\"><img src=\"#\" alt=\"picture\"></div><div class=\"slider-caption\"><h5 class=\"reviewer\"></h5><p class=\"comment\"></p></div></div></div>"
		},

		init : function (closeButton) {
			var self = this;
			$('.mobile.body').append(self.settings.slideHTML);

			self.settings.gallerySlider = $('.gallery-slider');
			self.settings.silderCaption.reviewer = $('.slider-caption').children('.reviewer');
			self.settings.silderCaption.comment = $('.slider-caption').children('.comment');
			self.settings.slidePositionHolder = $('.slider-position-counter');
			self.settings.currentImgHolder = $('.silder-img').children('img');
			self.settings.prevButton = $('.silder-nav.left');
			self.settings.nextButton = $('.silder-nav.right');

			self.prepareImgs(self.settings.gallery);
			self.settings.closeButton = closeButton;
			self.handelClose();
			self.handelPrev();
			self.handelNext();
			self.settings.gallerySlider.hide();
		},

		prepareImgs: function (gallery) {
			var self = this;

			$(gallery).children('li').each(function(i){
				var details = {
					'img' : $(this).children('a').attr('href'),
					'uploader' : $(this).children('a').attr('data-uploder'),
					'comment' : $(this).children('a').attr('data-comment')
				}
				self.settings.galleryImgs.push(details);
				$(this).children('a').click(function (e) {
					e.preventDefault();
					self.startSlide(i);
				})
			});
		},

		startSlide : function (imgPos) {
			var self = this,
				windowHeight = window.innerHeight+(window.innerHeight/100 * 6);

			self.settings.gallerySlider.css({'height': windowHeight});
			self.settings.mainSection.css({'height' : windowHeight, 'overflow-y': 'hidden'});
			self.settings.sildeStarted = true;
			self.settings.gallerySlider.show();
			self.updateCounter(imgPos);
			self.settings.silderCaption.reviewer[0].innerHTML = self.settings.galleryImgs[imgPos].uploader;
			self.settings.silderCaption.comment[0].innerHTML = self.settings.galleryImgs[imgPos].comment;
			self.settings.currentImgHolder[0].src = self.settings.galleryImgs[imgPos].img;
		},

		prev: function () {
			var self = this,
				imgPos = self.settings.currentImgPos;

			if (imgPos < 1) {
				$(self.settings.prevButton).addClass('disabled');
			} else{
				imgPos = imgPos - 1;
				self.startSlide(imgPos);
			};

		},

		next: function () {
			var self = this,
				imgPos = self.settings.currentImgPos;

			if (imgPos + 2 > self.settings.galleryImgs.length) {
				$(self.settings.nextButton).addClass('disabled');
			} else{
				imgPos = imgPos + 1;
				self.startSlide(imgPos);
			};

		},

		handelClose: function () {
			var self = this;

			self.settings.closeButton.click(function (e) {
				if (self.settings.sildeStarted) {
					e.preventDefault();
					self.settings.gallerySlider.hide();
					self.settings.sildeStarted = false;
				}
			});
		},


		handelPrev: function () {
			var self =  this;

			$(self.settings.prevButton[0]).click(function (e) {
				e.preventDefault();
				self.prev();
			})
		},

		handelNext: function () {
			var self =  this;

			$(self.settings.nextButton[0]).click(function (e) {
				e.preventDefault();
				self.next();
			})
		},

		updateCounter: function (imgPos) {
			var self = this;

			self.settings.currentImgPos = imgPos;
			self.settings.slidePositionHolder[0].innerHTML = (imgPos +1)+'/'+self.settings.galleryImgs.length;
		}
	}

	Vc.prototype.searchNav = {
		settings : {
			searchButton : $('#search-nav'),
            normalHeader : $('#normal-header'),
            searchHeader : $('#search-header'),
            mainSection  : $('.main-section'),
            overLayHtml  : "<div class='search-overlay' style='display:block'></div>",
            searchHeaderClose : $('.close-nav-link')
		},

		init : function () {
			var self = this;
			$(self.settings.searchButton).click(function  (e) {
                e.preventDefault();
                self.settings.normalHeader.hide();
                self.settings.mainSection.prepend(self.settings.overLayHtml);
                self.settings.searchHeader.show();

            });
            $(self.settings.searchHeaderClose).click(function (e) {
                e.preventDefault();
                self.settings.searchHeader.hide();
                $('.search-overlay').remove();
                self.settings.normalHeader.show();
            });
		}
	}

	Vc.prototype.cookies = {

		createCookie: function(name, value, days) {
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                var expires = "; expires=" + date.toGMTString();
            }
            else var expires = "";
            document.cookie = name + "=" + value + expires + "; path=/";
        },

        getCookie: function(c_name) {
            if (document.cookie.length > 0) {
                c_start = document.cookie.indexOf(c_name);
                if (c_start != -1) {
                    c_start = c_start + c_name.length + 1;
                    c_end = document.cookie.indexOf(";", c_start);
                    if (c_end == -1) {
                        c_end = document.cookie.length;
                    }
                    return unescape(document.cookie.substring(c_start, c_end));
                }
            }
            return false;
        }
	};

	Vc.prototype.getlocation =  {
		init : function () {

			var self = this;

			if ("geolocation" in navigator &&
				!Vc.prototype.cookies.getCookie('_lat') ||
				!Vc.prototype.cookies.getCookie('_log')) {

				console.log("geolocation is avaliable")
				var geo_options = {
					enableHighAccuracy: true,
					maximumAge        : 30000,
					timeout           : 27000
				};
				navigator.geolocation.getCurrentPosition(this.success, this.error, geo_options);

			}
		},


		success: function (position) {
			var self       = this;
				loc_coords = position.coords,
				latitude   = loc_coords.latitude,
				longitude  = loc_coords.longitude,
				accuracy   = loc_coords.accuracy;

			$.ajax({
                url:"http://maps.googleapis.com/maps/api/geocode/json?latlng="+position.coords.latitude+","+position.coords.longitude+"&sensor=true",
                success:function(result){
                    if (result.status === "OK") {
                         $.each(result.results, function(i){
                         	Vc.prototype.cookies.createCookie('_'+this.types[0],this.formatted_address, 1/4);
                         });
                    }else{
                        console.warn(result.status);
                    }
                }
            });

 			Vc.prototype.cookies.createCookie("_lat",latitude, 1/4);
 			Vc.prototype.cookies.createCookie("_log",longitude, 1/4);
 			Vc.prototype.cookies.createCookie("_accuracy",accuracy, 1/4);
 			Vc.prototype.setLocationBoxValue();
		},

		error: function (error) {
			console.warn('ERROR(' + error.code + '): ' + error.message);
		}
	};

	Vc.prototype.fancyPassword = {
		settings : {
			maskToggle     : null,
			passwordElement: null,
			// toggleELement  : null,
			// password       : null,
			toggeled       : null
		},

		init: function () {
			var self  = this;
			self.settings.maskToggle      = $('#toggle-mask'),
			self.settings.passwordElement = $("input[type='password']"),
			//self.settings.toggleELement   = $('#password-toggle'),
			//self.settings.password        =  self.settings.passwordElement.val(),
			self.settings.toggeled        =  ($('#toggle-mask:checked').length === 1)? true : false,
			//self.settings.toggleELement.hide();

			self.settings.maskToggle.click(function (e) {
				self.settings.toggeled = ($('#toggle-mask:checked').length === 1)? true : false;
				//self.settings.password =  self.settings.passwordElement.val();
				self.toggle(self.settings.toggeled, /*self.settings.password,*/ self.settings.passwordElement /*self.settings.toggleELement*/);
			})
		},

		toggle: function (toggle, passwordEle) {
			var self = this;

			passwordEle.attr('type', (toggle) ? 'text' : 'password');
			// if (toggle) {
			// 	passwordEle.hide();
			// 	toggleEle.val(self.settings.password);
			// 	toggleEle.show();
			// 	toggleEle.removeClass('hide');

			// 	toggleEle.keyup(function() {
			// 		self.settings.password = toggleEle.val();
			// 		passwordEle.val(self.settings.password);
			// 	})

			// } else{
			// 	passwordEle.show();
			// 	toggleEle.val(self.settings.password);
			// 	toggleEle.hide();
			// 	toggleEle.addClass('hide');

			// 	passwordEle.keyup(function() {
			// 		self.settings.password = passwordEle.val();
			// 		toggleEle.val(self.settings.password);
			// 	})
			// };
		}
	};

	Vc.prototype.autoComplete = {

		settings: {
			engine: null,
			keywordSearch: $('.keyword-autocomplete'),
			locationSearch: $('.location-autocomplete'),
			keywordDatasource : null,
			locationDatasource: null,
			bodyWidth: window.innerWidth
		},

		init: function (keywordDatasource, locationDatasource) {
			// body...
			var self = this;
			self.settings.keywordDatasource = (keywordDatasource)? keywordDatasource: 'http://istage.vconnect.co/HomeWEB/KeywordAutoComplete?term=%QUERY';
			self.settings.locationDatasource = (locationDatasource)? locationDatasource: 'http://istage.vconnect.co/HomeWEB/KeywordAutoComplete?term=%QUERY';
			self.settings.engine = new Bloodhound({
			datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.label); },
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				limit: 5,
				remote: {
					url: self.settings.keywordDatasource,
				},
				local: [
				    { label: 'one' },
				    { label: 'two' },
				    { label: 'three' },
				    { label: 'four' },
				    { label: 'five' },
				    { label: 'six' },
				    { label: 'seven' },
				    { label: 'eight' },
				    { label: 'nine' },
				    { label: 'ten' }
	  			]
			});

			self.keywordAutoComplete();
			self.locationAutoComplete();
			$('#search-header').children().find('.twitter-typeahead').css({'width': self.settings.bodyWidth});
		},

		keywordAutoComplete: function () {
			var self = this;
			self.settings.engine.remote.url = self.settings.keywordDatasource;
			self.settings.engine.initialize();
			// instantiate the typeahead UI
			self.settings.keywordSearch.typeahead(null, {
			  displayKey: 'label',
			  source: self.settings.engine.ttAdapter()
			});
		},

		locationAutoComplete: function () {
			var self = this;
			self.settings.engine.remote.url = self.settings.locationDatasource;
			self.settings.engine.initialize();
			// instantiate the typeahead UI
			self.settings.locationSearch.typeahead(null, {
			  displayKey: 'label',
			  source: self.settings.engine.ttAdapter()
			});
		}
	};

	Vc.prototype.setLocationBoxValue = function() {
		var pageHasLocationBox = (
			$('.location-autocomplete').length >= 1 &&
			Vc.prototype.cookies.getCookie('_sublocality')
			)? true : false;
		if (pageHasLocationBox) {
			var location  = Vc.prototype.cookies.getCookie('_sublocality');
				location = location.split(',')[1];
			$('.location-autocomplete').val(location);
		};
	};

	Vc.prototype.offCanvas = function() {
		$('.left-off-canvas-toggle').click(function (e) {
			e.preventDefault();
			var left = (window.screen.width/100)*70;
				leftOffest = (window.screen.width/100)*70;
			$('.mobile-wrap').addClass('move-right');
			$('.inner-wrap').css({'left': left+'px'});
			$('.exit-off-canvas').css('display', 'block');
			$('.left-menu').css({'width': left+'px !important','left': -(leftOffest)+'px',  'z-index': 10002});
		});

		$('.exit-off-canvas').click(function (e) {
			e.preventDefault();
			$('.inner-wrap').css({'left': ''});
			$('.mobile-wrap').removeClass('move-right');
			$('.exit-off-canvas').css('display', 'none');
			//if (window.screen.width < 290) {
				var left = (window.screen.width/100)*70;
					leftOffest = (window.screen.width/100)*70;
					$('.left-menu').css({'left': -leftOffest+'px'});
			//};
		})
	};

	Vc.prototype.alerts = function() {
		$('.alert-box > a.close').click(function (e) {
			e.preventDefault();
			//alert('click');
			$('.alert-box').css('display', 'none');
		});
	};

	Vc.prototype.profileTabs = function () {

		$('.profile-tabs .profile-tab').each(function(el) {
				// console.log($(this).children('a').attr('href'));
				if (!$(this).hasClass('active')) {
					$($(this).children('a').attr('href')).addClass('hide');
				};
				$(this).click(function(e) {
					e.preventDefault();
					if (!$(this).hasClass('active')) {
						var self = this,
							actv = $('.profile-tabs .profile-tab.active');

						$(actv).removeClass('active');
						$($(actv).children('a').attr('href')).addClass('hide');
						$('[data-header='+$($(actv).children('a').attr('href')).attr('data-footer')+']').addClass('hide');

						$(self).addClass('active');
						$($(self).children('a').attr('href')).removeClass('hide');
						$('[data-header='+$($(self).children('a').attr('href')).attr('data-footer')+']').removeClass('hide');

					};
				});
			});
	};
/*	Vc.prototype.placeholder = function() {
		$('input[placeholder]').each(function (i) {
			if ($(this).attr('type') == 'password') {
				var clone = $(this).clone();
					$(this).val($(this).attr('placeholder'));
					$(this).attr('type', 'text');

				if (document.getElementById('toggle-mask')) {
					$('#toggle-mask').attr('checked','checked');
				} else{
					$(this).focus(function () {
						if ($(this).val() == $(this).attr('placeholder')) {
							$(this).attr('type', 'password');
							$(this).val('');
							//$(this).replaceWith(clone);
						}else{
							$(this).attr('type', 'text');
						}
					});

					$(this).blur(function () {
						if ($(this).val().length < 1 || $(this).val() == $(this).attr('placeholder')) {
							$(this).attr('type', 'text');
							$(this).val($(this).attr('placeholder'));
						}else{
							$(this).attr('type', 'password');
						}
					})
					$(this).change(function () {
						if ($(this).val().length < 1 || $(this).val() == $(this).attr('placeholder')) {
							$(this).val($(this).attr('placeholder'));
							$(this).attr('type', 'text');
						}else{
							$(this).attr('type', 'password');
						}
					})
				};
			} else{
				$(this).val($(this).attr('placeholder'));
				$(this).focus(function () {
					$(this).val(
						($(this).val() == $(this).attr('placeholder'))? '' : $(this).val()
					);
				});
				$(this).blur(function () {
					$(this).val(
						($(this).val().length < 1 || $(this).val() == $(this).attr('placeholder'))? $(this).attr('placeholder') : $(this).val()
					)
				})

				$(this).change(function () {
					$(this).val(
						($(this).val().length < 1 || $(this).val() == $(this).attr('placeholder'))? $(this).attr('placeholder') : $(this).val()
					)
				})
			};

		});
	};
	Vc.prototype.placeholder = {

		init: function() {
			var self = this,
				input = $('input[placeholder]');
				//console.log(input);
			self.placeholder(input);
		},

		placeholder : function(inputs){
			var phid=0;
			$.each(inputs, function() {
				//console.log($(this))
				return $(this).on({
		            focus: function(){
		            	var label = "label[for='"+this.id+"']";
		            		label = $(label);
		            	$(label).css('display', 'none');
		            	$(label).text('');
		                $(this).parent().addClass('placeholder-focus');
		            },blur: function(){
		            	if (this.value !== '') {
		            		var label = "label[for='"+this.id+"']";
		            			label = $(label);
		            		$(label).css('display', 'none');
		            	}else{
		            		var label = "label[for='"+this.id+"']";
		            			label = $(label);
		            		$(label).css('display', 'block');
		            		$(label).text($(this).attr('data-placeholder'));
		            	}
		                $(this).parent().removeClass('placeholder-focus');
		            }/*,'keyup input': function(){
		                $(this).parent().toggleClass('placeholder-changed',this.value!=='');
		            }*//*,change: function () {
		            	if (this.value !== '') {
		            		//console.log(this.value);

		            		//console.log($(this).siblings());
		            		//$(this).prev()[0].style.display = 'none';
		            		var label = "label[for='"+this.id+"']";
		            			label = $(label);
		            		$(label).css('display', 'none');
		            		$(this).parent().addClass('placeholder-changed');
		            	}else{
		            		//console.log(this.value);

		            		//console.log($(this).siblings());
		            		//$(this).prev()[0].style.display = 'block';
		            		var label = "label[for='"+this.id+"']";
		            			label = $(label);
		            		$(label).css('display', 'block');
		            		$(this).parent().removeClass('placeholder-changed');
		            	}
		            	//$(this).parent().toggleClass('placeholder-changed',this.value!=='');
		            	//console.log('changed')
		            }
		        }).each(function(){
		            var $this = $(this);
		            //Adds an id to elements if absent
		            if(!this.id) this.id='ph_'+(phid++);
		            $(this).attr('data-placeholder', $this.attr('placeholder'));
		            //Create input wrapper with label for placeholder. Also sets the for attribute to the id of the input if it exists.
		            $('<span class="placeholder-wrap"><label for="'+this.id+'" style="width: '+$(this).width()+'px;">'+$this.attr('placeholder')+'</label></span>')
		                .insertAfter($this)
		                .append($this);
		            //Disables default placeholder
		            $this.attr('placeholder','').keyup();
		        });
			});

	    }
	};*/
	Vc.prototype.placeholder = {
		init: function () {
			var self =  this;
			$('.keyword-autocomplete').each(function() {
				self.listen($(this));
				self.validate($(this));
			});
			$('.location-autocomplete').each(function() {
				self.listen($(this));
				self.validate($(this));
			});
		},

		listen: function (el) {
			$(el).val(
				($(el).val() === '')? $(el).attr('placeholder') : $(el).val()
			);
			$(el).on({
				focus: function() {
					$(this).val(
						($(this).val() == $(this).attr('placeholder'))? '' : $(this).val()
					);
				},
				blur: function () {
					$(this).val(
						($(this).val().length < 1 || $(this).val() == $(this).attr('placeholder'))? $(this).attr('placeholder') : $(this).val()
					)
				},
				change: function () {
					$(this).val(
						($(this).val().length < 1 || $(this).val() == $(this).attr('placeholder'))? $(this).attr('placeholder') : $(this).val()
					)
				}
			});
		},

		validate: function (el) {
			$(el).parents('form').submit(function (e) {
				if ($(el).val() ===$(el).attr('placeholder')) {
					e.preventDefault()
					if ($(el).hasClass('keyword-autocomplete')) { alert('Please enter search term')};
					if ($(el).hasClass('location-autocomplete')) { alert('Please enter a search location')};
				};
			})
		}

	};
	Vc.prototype.handleSearch = function() {
		$('[data-search]').click(function(e){

			  e.preventDefault();
			  $(this).toggleClass('active');
			  $('.search-bar').toggleClass('hide');
			  $('.search-bar').toggleClass('active');
			  $('#normal-header').toggleClass('has-search');
			});
	};
	Vc.prototype.tabs = function() {
		// Contains the initialization code
		var guid = null,
				self = this;

		$('[data-tab]').each(function (e) {
				$(this).children('li').each(function(el) {
					// console.log($(this).children('a').attr('href'));
					// guid = self.guid();
					// $(this).attr('data-guid', guid);
					// $(this).children('a').attr('data-guid', guid);
					// $($(this).children('a').attr('href')).attr('data-guid', guid);

					if (!$(this).hasClass('active')) {
						$($(this).children('a').attr('href')).addClass('hide');
					};
					$(this).click(function(e) {
						e.preventDefault();
						if (!$(this).hasClass('active')) {
							var self = this,
									// elid = $(self).attr('data-guid');
									actv = $(self).parent('[data-tab]').children('li.active');

							$(actv).removeClass('active');
							$($(actv).children('a').attr('href')).addClass('hide');
							if ($('[data-header='+$($(actv).children('a').attr('href')).attr('data-footer')+']')) {
								$('[data-header='+$($(actv).children('a').attr('href')).attr('data-footer')+']').addClass('hide');
							};

							$(self).addClass('active');
							$($(self).children('a').attr('href')).removeClass('hide');
							if ($('[data-header='+$($(self).children('a').attr('href')).attr('data-footer')+']')) {
								$('[data-header='+$($(self).children('a').attr('href')).attr('data-footer')+']').removeClass('hide');
							};

						};
					});
				});
		})
	};
	Vc.prototype.ctv = function() {
		$('[data-ctv]').children('.masked').addClass('hide');
		$('[data-ctv]').on('click', function (e) {
				e.preventDefault();
				if (!$(this).hasClass('open')) {
					$(this).children('.mask').addClass('hide');
					$(this).children('.masked').removeClass('hide').addClass('show');
					$(this).addClass('open');
				};
			});
	};
	Vc.prototype.changeProductView = function() {
			$('[data-list-switch]').click(function (e) {
				console.log(e);
				e.preventDefault();
				var currentDisplay = $('.item-list').hasClass('grid') ? 'grid' : 'list';
				if (currentDisplay == 'grid') {
					$('.item-list').addClass('list').removeClass('grid');
					$('.cart-items').addClass('list').removeClass('grid');
					$('.store-items-list').addClass('list').removeClass('grid');
					$('[data-list-switch] > i').addClass('icon-th').removeClass('icon-list');
				} else{
					$('.item-list').addClass('grid').removeClass('list');
					$('.cart-items').addClass('grid').removeClass('list');
					$('.store-items-list').addClass('grid').removeClass('list');
						$('[data-list-switch] > i').removeClass('icon-th').addClass('icon-list')
				};
			});
	};
	/*Vc.prototype.corporatelead = {

		config: {
			$cLeadPopup: $({}),
			dataNamespace: 'data-vc',
			$cLeadButton: $({})
		},

		attr_name:function(str){
			// Used to generate application-specific data- attributes
			// e.g. data-vc-role
			var cf = this.config;
			return cf.dataNamespace + '-' + str;
		},
		init:function(){
			// Contains the initialization code
			// ...
			var cf = this.config;
			cf.$cLeadPopup = $('[' + this.attr_name('clead-popup') + ']');
			cf.$cLeadButton = $('[' + this.attr_name('clead-btn') + ']');

			if(!cf.$cLeadPopup.length)return false;

			cf.$cLeadPopup.foundation('reveal', 'open');
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
			var self = this,
					cf = this.config;

			cf.$cLeadButton.on('click', function(e){
				e.preventDefault();
				cf.$cLeadPopup.foundation('reveal', 'open');
			});
		}
	};*/

})();

$app = new Vc();
//alert('created vc object');
$app.setLocationBoxValue();
$app.photoSlider.init($('.back-nav-link'));
$app.searchNav.init();
//$app.getlocation.init();
$app.fancyPassword.init();
// $app.autoComplete.init(
// 	'http://vc.dev/v3/vconnect-frontend/test.json',
// 	'http://vc.dev/v3/vconnect-frontend/test.json'
// );
// $app.fixInputScroll();
$app.offCanvas();
$app.alerts();
$app.placeholder.init();
$app.profileTabs();
$app.handleSearch();
$app.tabs();
$app.ctv();
$app.changeProductView();
// $app.corporatelead.init();
