;(function($){
    window.app = {
        comps: {
        	leadbox: {}
        }
    };
    // Override fade effects for opera mini
    $.fn.fadeIn = $.fn.show;
    $.fn.fadeOut = $.fn.hide;

    Foundation.libs.abide.settings.patterns.nigerianphone = /^0?(([1-6]\d{6,7})|([7-9]([0-1]\d{8}|[2-9]\d{6,7})))$/; /*/0?(([7-9][0-1]\d{8})|([1-8]\d{6,7}))/;*/

    // Updated content from app.leadbox.js for mobile -->
    window.app.comps.leadbox = {
			name: 'leadbox',
			description: 'Script for lead box related interactivity',
			config:{
				leadSuccessMessage: "Thank you for posting the requirement. We are finding out best suppliers matching your requirement and will soon send their details to you on your phone number.",
				leadErrorMessage: "Something went wrong. Please try again later.",
				$leadBox: $('.lead-box'),
				$leadFormItems: $('.form-item')
			},
			settings:{
				_curIndex: 0,
				_totItems: 0
			},
			init:function(){
				var self = this,
					cf = this.config,
					st = this.settings;
				st.$boxFormItems = cf.$leadBox.find(cf.$leadFormItems);
				st.$progressLevel = cf.$leadBox.find('.progress-level');
				st.$progressCounter = cf.$leadBox.find('.progress-counter');
				st.$prevButton = cf.$leadBox.find(cf.roleEl.prev);
				st.$nextButton = cf.$leadBox.find(cf.roleEl.next);
				st.$skipButton = cf.$leadBox.find(cf.roleEl.skip);
				st.$loadingView = cf.$leadBox.find(cf.roleEl.loading);
				st._totItems = st.$boxFormItems.length;
				st._curIndex = 0; //zero-based index of current form item

				if(cf.$leadBox.length <= 0){
					return false;
				}

				// Hide the loading container
				// cf.$leadFormsContainer.find('[data-vc-role="loading"]').hide();
				st.$loadingView.hide();

				// Show the first section
				self._showItem();
				this.events();
			},
	    events:function(){
	        var self = this,
	            st = this.settings,
	            cf = this.config;

	        // Form navigation links event handling
	        // Previous button event handling
	        st.$prevButton.on('click', function(e){
	          e.preventDefault();
	          if($(this).attr('disabled'))return false;
	          self.previousItem();
	        });

	        // Next button event handling
	        st.$nextButton.on('click', function(e){
	          e.preventDefault();
	          if($(this).attr('disabled'))return false;
	          self.nextItem();
	        });

	        // Skip button event handling
	        st.$skipButton.on('click', function(e){
	        	e.preventDefault();
	            if($(this).attr('disabled'))return false;
	        });

	        $('.form-item').closest('form').on('submit',function(e, m){
	            console.log(arguments);
	        });

	        // Dropdown and radio options change event handling
	        st.$boxFormItems.find('select, input[type="radio"]').on('change',$.proxy(self.nextItem, this));

	        this.subscribe("vc:lead/submit", function(e, data){
	        	// console.log('submit?')
	        	if(data && data.status && data.status == "success"){
		        	self.notifyMe(data.message || cf.leadSuccessMessage);
		        	cf.$leadBox.fadeOut();
	        	}
	        	else if(data && data.status && data.status == "error"){
	        		self.notifyMe({type: "error", message: data.message || cf.leadErrorMessage});
	        	}
	        	st.$loadingView.hide();
	        });
	        // Input value change event handling
	        // st.$boxFormItems.find('input[type="checkbox"]').on('change',$.proxy(self._showItem, this));
	    },
	    getItem:function(curIndex){
	        var st = this.settings;
	        curIndex = (typeof curIndex == "undefined") ? st._curIndex : curIndex;
	        return this.settings.$boxFormItems.filter(':eq(' + curIndex + ')');
	    },
			_showItem:function(curIndex){
				var st = this.settings,
					curItem;

				st._curIndex = (typeof curIndex == "undefined") ? 0 : curIndex;
				st.$boxFormItems.hide();
				curItem = this.getItem(st._curIndex).fadeIn();

				st.$progressLevel.css('width', ((st._curIndex+1)/(st._totItems) * 100) + '%');
				st.$progressCounter.html('Question ' + (st._curIndex+1) + ' of ' + st._totItems + '');

				// Display the necessary button

				st.$nextButton.show();
				st.$prevButton.show();
				st.$skipButton.show();

				console.log(st._curIndex);
				if(st._curIndex <= 0){
					st.$prevButton.hide();
					st.$skipButton.hide();
					if(!this.hasInputValue(curItem)){
						st.$nextButton.hide();
					}
				}
				if(curItem.hasClass('no-skip'))st.$skipButton.hide();
				if(st._curIndex >= st._totItems-1)st.$nextButton.text('Get Result');
				else st.$nextButton.text('Continue');
			},
			previousItem:function(){
				// show the previous item
				var st = this.settings;
				if(st._curIndex > 0){
					st._curIndex--;
					this._showItem(st._curIndex);
				}
			},
	    nextItem:function(){
	        // show the next item
	        var st = this.settings;
	        if(!this.isValidItem())return false;
	        if(st._curIndex < st._totItems-1){
	            st._curIndex++;
	            this._showItem(st._curIndex);
	        }
	        else{
	            console.log('submitting lead request...');
	            st.$nextButton.prop('disabled', true);
	            this.publish('vc:lead/finalclick');
	            st.$loadingView.attr(this.attr_name('value'), 'Submitting your request...').show();
	            // alert('done');
	            this.getItem().closest('form').attr('data-vc-value', 'complete').submit();
	        }
	    },
			hasInputValue:function($form){
				var flg = 0;
				// 'input,select,textarea'
				console.log($form.find(':checked'));
				// console.log('1: ' + flg);
				//For radio, checkbox and select items
				flg += !!$form.find(':checked').val();

				$form.find('input').not('[type="radio"],[type="checkbox"]').each(function(index, elem){
					if($(elem).val())flg++;
				});
				console.log('4: ' + flg);
				return !!flg;
			},
	    isValidItem:function(curIndex){
	      var st = this.settings,
	          curItem;
	      curIndex = (typeof curIndex == "undefined") ? st._curIndex : curIndex;
	      curItem = this.getItem(curIndex);
	      // $(curItem).closest('form').trigger('validate');
	      Foundation.libs.abide.validate(curItem.find('input, select, textarea'), {type:'submit'})
	      // alert(curItem.closest('form').html());
	      // alert('noOfInvalid: ' + curItem.find('[data-invalid]').length);
	      // $('.form-item').closest('form').trigger('validate');
	      return !curItem.find('[data-invalid], .input-wrapper.error').length;
	    }
		};
    // <-- End content update from app.leadbox.js



    // Set config options
    window.app.comps.leadbox.config = {
        $leadBox: $('.lead-box'),
        $leadFormItems: $('.form-item'),
        dataNamespace: 'data-vc',
        roleEl:{
            prev: '[data-vc-role="prev"]',
            next: '[data-vc-role="next"]',
            skip: '[data-vc-role="skip"]',
            loading: '[data-vc-role="loading"]'
        }
    }
    app.comps.leadbox.o = $({});
    app.comps.leadbox.initPubSub = function(){
    	var self = this;
			$.each({
				trigger:'publish',
				on:'subscribe',
				off:'unsubscribe'
			}, function(key, val){
				self[val] = function(){
					// Before: this for apply was app.o
					this.o[key].apply(this.o, arguments);
				}
			});
    };

    app.comps.leadbox.initPubSub();

    app.o = app.comps.leadbox.o;
    app.publish = app.comps.leadbox.publish;
    app.subscribe = app.comps.leadbox.subscribe;
    app.comps.leadbox = $.extend(app.comps.leadbox, {
    	attr_name: function(val){
    		return "data-vc-" + val;
    	}
    });

    app.comps.leadbox.init();
}(jQuery));


/*
	Changes added to fix abide validation issue
	-------------------------------------------

	For some reason, the data-invalid attribute isn't added to the input elements when they are invalidated,
	so we need to fallback to use the error class that is added to the input-wrapper.
	Also the type should be 'submit' for validation to work properly when using Foundation.libs.abide.validate(curItem.find('input, select, textarea'), {type:'submit'})
	label should be styled with cursor: default, not pointer.
*/
