$('input[placeholder]').each(function (i) {
			$(this).val($(this).attr('placeholder'));
			$(this).focus(function () {
				$(this).val(
					($(this).val() == $(this).attr('placeholder'))? '' : $(this).val()
				);
			});
			$(this).blur(function () {
				$(this).val(
					($(this).val().length < 1 || $(this).val() == $(this).attr('placeholder'))? $(this).attr('placeholder') : $(this).val()
				)
			})

			$(this).change(function () {
				$(this).val(
					($(this).val().length < 1 || $(this).val() == $(this).attr('placeholder'))? $(this).attr('placeholder') : $(this).val()
				)
			})
		});