if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
    var msViewportStyle = document.createElement("style");
    msViewportStyle.appendChild(
        document.createTextNode(
            "@-ms-viewport{width:auto!important}"
        )
    );
    document.getElementsByTagName("head")[0].appendChild(msViewportStyle);
}
var loadJs = function (scripts) {
  if (!scripts instanceof Array) {
    throw new Error("Array of scripts expected "+ typeof(scripts)+" supplied");
    return false;
  };

  for (var i = scripts.length - 1; i >= 0; i--) {
    script = scripts[i];
    if(/*!script instanceof Object*/ typeof(script) != 'object'){
      throw new Error("Object expected "+typeof(script)+" supplied");
      return false
    }else if(!script.src){
      throw new Error("scr property expected "+Object.keys(script)+" supplied");
      return false
    }
    var scr = script.src;
    delete script.src;
    var element = document.createElement("script");
        element.src = scr;
    while(Object.keys(script).length > 0){
      element.setAttribute(Object.keys(script)[0], script[Object.keys(script)[0]]);
      delete script[Object.keys(script)[0]];
    }
    document.body.appendChild(element);
  };
};
          
var userAgent   = window.navigator.userAgent,
    isOperaMini = userAgent.match(/Opera Mini/),
    isOperaMobi = userAgent.match(/Opera Mobi/),
    isIOS       = userAgent.match(/iPad/i) || userAgent.match(/iPhone/i),
    isAndroidv2 = userAgent.match(/(Android (2.0|2.1|2.2|2.3))/),
    isUC        = userAgent.match(/(UCWEB)/),
    isNokia     = userAgent.match(/(Nokia)/),
    isWP        = userAgent.match(/(Windows Phone)/),
    isOvi       = userAgent.match(/S40OviBrowser/),
    isSafari       = userAgent.match(/Safari/) || userAgent.match(/AppleWebKit/),
    root = document.documentElement;

root.removeClass = function (name) {
  var set = " " + root.className + " ";
  while ( set.indexOf(" " + name + " ") > -1 ) {
    set = set.replace(" " + name + " " , " ");
  }
  root.className = typeof set.trim === "function" ? set.trim() : set.replace(/^\s+|\s+$/g, "");
};

root.addClass =function(name) {
  root.className += " "+name+""; 
};

if(isOperaMini) {
  root.className += " opera-mini";
  root.addClass('no-js');
  root.removeClass("fontface");
  root.removeClass("js");
}
if(isOperaMobi) {
  root.addClass("opera-mobi");
  root.addClass("no-boxshadow");
  root.removeClass("boxshadow");
};
if(isIOS) { 
                 
  root.className += " IOS"; 
}
if (isSafari) {
  root.addClass('safari');
};
if (isWP) {
  root.addClass('WP');
};
if (isNokia) {
  root.addClass('Nokia')
};
if (isUC) {
  root.addClass('UC')
};
if (isAndroidv2) {
  root.addClass('isAndroidv2');
  root.addClass('no-boxshadow');
};
// if (userAgent.match(/(Android (2.0|2.1))|(Nokia)|(Opera (Mini))|(w(eb)?OSBrowser)|(UCWEB)|(Windows Phone)|(XBLWP)|(ZuneWP)/)) {
//   root.removeClass("fontface");
// }
if (isOvi) {
  root.addClass('Ovi');
  root.addClass('no-js');
  root.addClass("no-boxshadow");


  loadJs(
    [{'src':"//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"}, 
    {'src':"//s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js"},
    {'src':"//html5base.googlecode.com/svn-history/r38/trunk/js/selectivizr-1.0.3b.js"},
    {'src':"//cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"}]
    ); 

};

// if (isIOS && isSafari) {
//   alert('Safari on IOS detected');
//   loadJs(
//     ["http://192.10.10.146:9090/target/target-script-min.js#anonymous"]
//     ); 
//   $('.left-off-canvas-toggle').click(function (e) {
//     e.preventDefault();
//     alert('click');
//     console.log('click');
//   })
// };

/(Windows Phone)|(XBLWP)|(ZuneWP)/.test(navigator.userAgent)&&root.addClass("ms-ie");

//alert(window.screen.width);

//loadJs([{'src':"http://192.10.10.146:9090/target/target-script-min.js#anonymous"}]); 