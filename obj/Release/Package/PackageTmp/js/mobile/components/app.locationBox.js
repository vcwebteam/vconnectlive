;(function($, window, document, undefined){
	window.app.comps.locationBox = {
		name: 'locationBox',
		description: 'description',
		init:function(){
			// Contains the initialization code
			var self = this,
				pageHasLocationBox = ($('.location-box').length >= 1)? true : false;

			if (pageHasLocationBox && self.getLocation()) {

				var location  = self.getLocation();

				if (location.sublocality) {
					location = location.sublocality;
					location = location.split(',')[1];

					$('.location-box').each(function() {

						if ($(this).val().length == 0) {

							if ($(this).hasClass('directions-box')) {
								location  = self.getLocation();
								location  = location.neighborhood;
							}

							$(this).val(location);
						};
						$(this).text(location);
					});
				}else if(location.neighborhood){
					location = location.neighborhood;
					location = location.split(',')[1];

					$('.location-box').each(function() {

						if ($(this).val().length == 0) {

							if ($(this).hasClass('directions-box')) {
								location  = self.getLocation();
								location  = location.neighborhood;
							}

							$(this).val(location);
						};
						$(this).text(location);
					});
				}

			};
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
		}
	};
}(jQuery, window, document));
