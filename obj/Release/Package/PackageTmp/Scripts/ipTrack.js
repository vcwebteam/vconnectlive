﻿$(document).ready(function () {
    callOtherDomain();
});
var isIE8 = window.XDomainRequest ? true : false;
var invocation = createCrossDomainRequest();
var url = 'http://www.whatismyip.com/custom/wimi.php';
function createCrossDomainRequest(url, handler) {
    var request;
    if (isIE8) {
        request = new window.XDomainRequest();
    } else {
        request = new XMLHttpRequest();
    }
    return request;
}
function callOtherDomain() {
    if (invocation) {
        if (isIE8) {
            invocation.onload = outputResult;
            invocation.open("GET", url, true);
            invocation.send();
        } else {
            invocation.open('GET', url, true);
            invocation.onreadystatechange = handler;
            invocation.send();
        }
    } else {
        var text = "No Invocation TookPlace At All";
        var textNode = document.createTextNode(text);
        var textDiv = document.getElementById("ip");
        textDiv.appendChild(textNode);
    }
}
function handler(evtXHR) {
    if (invocation.readyState == 4) {
        if (invocation.status == 200) {
            outputResult();
        } else {
            //alert("Invocation Errors Occured");
        }
    }
}
function outputResult() {
    var response = invocation.responseText;
    var textDiv = document.getElementById("ip");
    var match = response.match(/\d+\.\d+\.\d+\.\d+/g);
    //textDiv.innerHTML += "Your IP : " + match[0];

    var url = "/homeweb/logging";
    var ip = match[0];


    $.ajax({
        type: 'GET',
        url: '/homeweb/logging',
        data: { ip: ip },
        contentType: "application/json; charset=UTF-8",
        success: function (data, status, settings) {
            //console.log("Successfully made GET request");
        },
        error: function (ajaxrequest, ajaxOptions, thrownError) {

            console.log('Error occurred when calling Logging Function.');
        }
    });
}