﻿function RateMe(rating) {
    var rate1 = 0.0;
    var rem = 0.0;
    try {
        //rate1 = System.Convert.ToDecimal$$Object(rating);
        rate1 = parseFloat(rating);
        if (parseFloat(rating.toString().substr(1)) != null && parseFloat(rating.toString().substr(1)) != "") {
            rem = parseFloat(rating.toString().substr(1));
        }
        //if (!System.String.IsNullOrEmpty(rating.toString().substr(1))) {
        //    rem = parseFloat(rating.toString().substr(1));
        //}
    }
    catch ($$e1) {
        rate1 = 0;
    }
    var rate = "";
    for (var i = 1; i <= 5; i++) {
        if (i <= rate1) {
            rate = rate + "<i class=\"icon-star\"></i>";
        }
        else if (rem > 0 && rem < 1) {
            if (rem == 0.5) {
                rate = rate + "<i class=\"icon-star-half\"></i>";
                rem = 0.0;
            }
            else if (rem > 0.5) {
                rate = rate + "<i class=\"icon-star-half\"></i>";
                rem = 0.0;
            }
            else {
                rate = rate + "<i class=\"icon-star-half\"></i>";
                rem = 0.0;
            }
        }
        else {
            rate = rate + "<i class=\"icon-star-empty\"></i>";
        }
    }
    return rate;
};
function RateMeListing(rating) {
    var rate1 = 0.0;
    var rem = 0.0;
    try {
        //rate1 = System.Convert.ToDecimal$$Object(rating);
        rate1 = parseFloat(rating);
        if (parseFloat(rating.toString().substr(1)) != null && parseFloat(rating.toString().substr(1)) != "") {
            rem = parseFloat(rating.toString().substr(1));
        }
        //if (!System.String.IsNullOrEmpty(rating.toString().substr(1))) {
        //    rem = parseFloat(rating.toString().substr(1));
        //}
    }
    catch ($$e1) {
        rate1 = 0;
    }
    var rate = "";
    for (var i = 1; i <= 5; i++) {
        if (i <= rate1) {
            rate = rate + "<li><i class=\"rating-star\"></i></li>";
        }
        else if (rem > 0 && rem < 1) {
            if (rem == 0.5) {
                rate = rate + "<li><i class=\"rating-star-half\"></i></li>";
                rem = 0.0;
            }
            else if (rem > 0.5) {
                rate = rate + "<li><i class=\"rating-star-half\"></i></li>";
                rem = 0.0;
            }
            else {
                rate = rate + "<li><i class=\"rating-star-half\"></i></li>";
                rem = 0.0;
            }
        }
        else {
            rate = rate + "<li><i class=\"rating-star-empty\"></i></li>";
        }
    }
    return rate;
};


function IsMobileNumber(txtMobId) {
   // debugger;
    var mob = /^[0]{1}[0-9]{10}$/;
    var msg = "VALID";
    var txtMobile = document.getElementById(txtMobId);
    if (mob.test(txtMobile.value) == false) {
        msg = "INVALID"
    }
    return msg;
}
function IsEmailAddress(txtEmailID) {
    var emlRegex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    var msg = "VALID";
    var txtemail = document.getElementById(txtEmailID);
    if (emlRegex.test(txtemail.value) == false) {
        msg = "INVALID"
    }
    return msg;
}
