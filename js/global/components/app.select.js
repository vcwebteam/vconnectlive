/**
	select - Handles the behavior of the select dropdown element

	element => [data-vc-select]
	options => [
		data-vc-items => "1, 2, 3, 4, 5" //Specifies the intial selected values
		data-vc-options => "1, 2, 3, 4, 5" || nameOfPredefinedOptions //Specifies the available options
		data-vc-remote => urlOfRemoteSource?q={QUERY} //Specifies the URL for the remote source of the options
			- returns an array of objects with text and value properties
		data-vc-preload => true || false || focus //Specifies when the load function is to be called
	]

*/

;(function($, window, document, undefined){
	window.app.comps.select = {
		config:{
			select:{
				queryHolder: '{QUERY}',
				options: {
					openingHours: [
						'12:00AM',
						'12:30AM',
						'1:00AM',
						'1:30AM',
						'2:00AM',
						'2:30AM',
						'3:00AM',
						'3:30AM',
						'4:00AM',
						'4:30AM',
						'5:00AM',
						'5:30AM',
						'6:00AM',
						'6:30AM',
						'7:00AM',
						'7:30AM',
						'8:00AM',
						'8:30AM',
						'9:00AM',
						'9:30AM',
						'10:00AM',
						'10:30AM',
						'11:00AM',
						'11:30AM',
						'12:00PM',
						'12:30PM',
						'1:00PM',
						'1:30PM',
						'2:00PM',
						'2:30PM',
						'3:00PM',
						'3:30PM',
						'4:00PM',
						'4:30PM',
						'5:00PM',
						'5:30PM',
						'6:00PM',
						'6:30PM',
						'7:00PM',
						'7:30PM',
						'8:00PM',
						'8:30PM',
						'9:00PM',
						'9:30PM',
						'10:00PM',
						'10:30PM',
						'11:00PM',
						'11:30PM',
						'Closed'
					]
				}
			}
		},
		init:function(){
			// Contains the initialization code
			var self = this,
					cf = this.config;
			this.require('selectize', function(){
				return (jQuery().selectize && $('[' + self.attr_name('select') + ']').length > 0);
			}, function(){
				var els = $('[' + self.attr_name('select') + ']');

				els.each(function(index, elem){
					var $el = $(elem), //the current element
							loadFn = null, //holds the load function
							remoteUrl = null, //holds the specified remoteUrl used for the load function
							options = null, //holds the specified options array
							items = null; //holds the specified items array

					// Set default options
					var selectOptions = {
						plugins: ['remove_button'],
						delimiter: ',',
						persist: false,
						create: false
					};

					// If the remote attribute is set, then set the loadFn else set it to null
					loadFn = (remoteUrl = $el.attr(self.attr_name('remote')) || null) ? function(query, callback) {
						// if (!query.length) return callback();
						$.ajax({
							url: remoteUrl.replace(cf.select.queryHolder, encodeURIComponent(query)),
							type: 'GET',
							error: function() {
								callback();
							},
							success: function(res) {
								callback(res);
							}
						});
					} : null;

					// Set load option if available
					if(loadFn)selectOptions.load = loadFn;

					// Set items option if available
					var elItems = $el.attr(self.attr_name('items'));
					if(elItems){
						items = elItems.split(',');
					}

					if(items)selectOptions.items = items;

					var elOptions = $el.attr(self.attr_name('options'));
					if(elOptions){
						// if options points to a predefined options object
						if(cf.select.options[elOptions]){
							options = cf.select.options[elOptions].map(function(item){
								return {
									text: item,
									value: item
								}
							});
						}
						// if options contains a comma-seperated list of options
						else{
							options = elOptions.split(',').map(function(item){
								return {
									text: item,
									value: item
								}
							});
						}
					}

					// Set element options if available
					if(options)selectOptions.options = options;

					// options: [{text: '1', value: '1'},{text: '2', value: '2'},{text: '3', value: '3'}]

					// Set preload option if available
					if($el.attr(self.attr_name('preload'))){
						selectOptions.preload = $el.attr(self.attr_name('preload'));
					}

					$el.selectize(selectOptions);
				});
			});

			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
		}
	};
}(jQuery, window, document));
