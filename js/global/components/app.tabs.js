;(function($, window, document, undefined){
	window.app.comps.tabs = {
		name: 'tabs',
		description: 'description',
		config:{
			hashPrefix: 'tabs_',
		},
		init:function(){
			// Contains the initialization code
			var guid = null,
					self = this,
					cf = this.config;

			$('[data-tab]').each(function (e) {
				$(this).children('li').each(function(el) {
					// console.log($(this).children('a').attr('href'));
					// guid = self.guid();
					// $(this).attr('data-guid', guid);
					// $(this).children('a').attr('data-guid', guid);
					// $($(this).children('a').attr('href')).attr('data-guid', guid);

					if (!$(this).hasClass('active')) {
						$($(this).children('a').attr('href')).addClass('hide');
					};
					// console.log('jhh');
					$(this).click(function(e) {
						e.preventDefault();
						if (!$(this).hasClass('active')) {
							var self = this,
									// elid = $(self).attr('data-guid');
									actv = $(self).parent('[data-tab]').children('li.active'),
									curTarget = $(self).children('a').attr('href').replace('#','');

							$(actv).removeClass('active');
							$($(actv).children('a').attr('href')).addClass('hide');
							if ($('[data-header='+$($(actv).children('a').attr('href')).attr('data-footer')+']')) {
								$('[data-header='+$($(actv).children('a').attr('href')).attr('data-footer')+']').addClass('hide');
							};

							$(self).addClass('active');
							$($(self).children('a').attr('href')).removeClass('hide');
							if ($('[data-header='+$($(self).children('a').attr('href')).attr('data-footer')+']')) {
								$('[data-header='+$($(self).children('a').attr('href')).attr('data-footer')+']').removeClass('hide');
							};

							// change the hash to trigger the hash change event
							if(history.pushState) {
								history.pushState(null, null, '#' + cf.hashPrefix + curTarget);
							}
							else {
								location.hash = '#' + cf.hashPrefix + curTarget;
							}
							$(window).trigger('hashchange');
						};
					});
				});
			});

			this.events();
		},
		/*guid:function () {
			var CHARS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
					LEN   = 10,
					result = '';
	    for (var i = LEN; i > 0; --i){
	    	result += CHARS[Math.round(Math.random() * (CHARS.length - 1))];
	    }
	    return result;
		},*/

		events:function(){
			// Contains the event bindings and subscriptions

			var self = this,
					cf = this.config;

			// quickfix: use hashchange event to display the relevant tab
			self.subscribe('vc:hashChange', function(e, data){
				console.log(data);
				hash = data.hash.replace(cf.hashPrefix, '');
				var curTab = $('[data-tab] li a[href=#' + hash +']');
				var curActiveTab = $('[data-tab] li.active a[href=#' + hash +']');

				if(curTab.length){
					if(!curActiveTab.length){
						var actv = curTab.closest('[data-tab]').children('li.active');
						var curLi = curTab.parent('li');

						$(actv).removeClass('active');
						console.log(curTab);
						console.log(actv);
						$($(actv).children('a').attr('href')).addClass('hide');

						if ($('[data-header='+$($(actv).children('a').attr('href')).attr('data-footer')+']')) {
							$('[data-header='+$($(actv).children('a').attr('href')).attr('data-footer')+']').addClass('hide');
						};
						// console.log('ggg');
						$(curLi).addClass('active');
						$($(curLi).children('a').attr('href')).removeClass('hide');
						if ($('[data-header='+$($(curLi).children('a').attr('href')).attr('data-footer')+']')) {
							$('[data-header='+$($(curLi).children('a').attr('href')).attr('data-footer')+']').removeClass('hide');
						};
					}
				}
			});
		}
	};
}(jQuery, window, document));
