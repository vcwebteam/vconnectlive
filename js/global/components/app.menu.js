/**
	menu - Script for the behaviour and interactivity of the topbar menu

*/

;(function($){
	window.app.comps.menu = {
		init:function(){
			var cf = this.config;
			//Display the appropriate menu
			cf.$searchBox = cf.$searchBox.add($('[' + this.attr_name('searchbox') + ']'));
			if(cf.$searchBox.length>0){
				if($(window).width() >= cf.tabletViewportSize){
					cf.$menuExpanded.show();
					cf.$menuCollapsed.hide();
				}
			}
			console.log(cf.$searchBox);
			this.events();
		},
		events:function(){
			var self = this,
				cf = this.config;
			// console.log(this);

			//Disable clicking of links for topbar dropdowns
			cf.$disableMenuSet.on('click',function(e){
				e.preventDefault();
			});

			//Tab search toggle search bar functionality
			$(cf.$tabSearch).on('click',function(e){
				cf.$searchForm.toggle();
			});

			//Initialise waypoint
			if(jQuery().waypoint){
				// console.log('d')
				cf.$searchBox.waypoint(function(direction){
					self.publish('vc:waypoint',direction);

					// console.log(direction);
					if (direction === 'down') {
						if($(window).width() >= cf.tabletViewportSize){
							cf.$menuExpanded.hide();
							cf.$menuCollapsed.fadeIn(500);
						}
					}
					else if (direction === 'up') {
						if($(window).width() >= cf.tabletViewportSize){
							cf.$menuCollapsed.hide();
							cf.$menuExpanded.fadeIn(500);
						}
					}
				});
			}

			//Window resize tasks
			$(window).on('resize.vc', $.proxy(this.onWindowResize, this));
		},
		subscriptions:function(){
		},
		onWindowResize:function(e){
			// self = e.data.self;
			// $.extend(self, self.utils);
			this.config.$searchForm.css('display','');
			//self.publish('ogf',{f:'G',h:'J'});
			// console.log(this);
			// this.notifyMe('You resized! :D');
			// this.publish('vc:notify', {message:'You resized! :D'});
		}
	};
}(jQuery));
