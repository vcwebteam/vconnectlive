/**
	typeahead - Script for typeahead-related interactivity

*/

;(function($){
	window.app.comps.typeahead = {
		config: {
			typeaheadOptions: {
				highlight: true,
				minLength: 0,
				hint: true
			}
		},
		init:function(){
			if(typeof Bloodhound === 'undefined'){
				return false;
			}

			var cf = this.config;
			// instantiate the bloodhound suggestion engine
			var searchTerms = new Bloodhound({
				datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.label); },
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				limit: 10,
				remote: {
					url: cf.termsURL //'http://istage.vconnect.co/HomeWEB/KeywordAutoComplete?term=%QUERY'
				}
			});

			// initialize the bloodhound suggestion engine
			searchTerms.initialize();

			// instantiate the typeahead UI
			cf.$searchTerm.typeahead(cf.typeaheadOptions, {
				displayKey: 'label',
				source: searchTerms.ttAdapter()
			}).on('typeahead:selected', function(obj, datum, name){
				selectedTTerm(datum);
			});
			cf.$jSearchTerm.typeahead(cf.typeaheadOptions, {
				displayKey: 'label',
				source: searchTerms.ttAdapter()
			}).on('typeahead:selected', function(obj, datum, name){
				selectedJTerm(datum);
			});
			// cf.$jSearchTermInput = cf.$jSearchTerm.filter('.tt-input');
			// cf.$searchTermInput = cf.$searchTerm.filter('.tt-input');

			// instantiate the bloodhound suggestion engine
			var searchLocations = new Bloodhound({
				datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.label); },
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				limit: 10,
				remote: {
					url: cf.locationsURL
				}
			});

			// initialize the bloodhound suggestion engine
			searchLocations.initialize();

			// instantiate the typeahead UI
			cf.$searchLocation.typeahead(cf.typeaheadOptions, {
				displayKey: 'label',
				source: searchLocations.ttAdapter()
			}).on('typeahead:selected', function(obj, datum, name) {
				// alert('HH');
				// console.log(datum);
				selectedTLocation(datum);
			});
			cf.$jSearchLocation.typeahead(cf.typeaheadOptions, {
				displayKey: 'label',
				source: searchLocations.ttAdapter()
			}).on('typeahead:selected', function(obj, datum, name){
				// alert('D');
				// console.log(datum);
				selectedJLocation(datum);
			});
			cf.$jSearchLocationInput = cf.$jSearchLocation.filter('.tt-input');
			cf.$searchLocationInput = cf.$searchLocation.filter('.tt-input');

			var selSearch = 2;
			//1 for jumbotron search
			//2 for topbar search
			$jInputs = cf.$jSearchTermInput.add(cf.$jSearchLocationInput);
			$tInputs = cf.$searchTermInput.add(cf.$searchLocationInput);
			$jInputs.on('keypress paste',function(){
				selSearch = 1;
				// console.log('selSearch: jumbotron');
			});
			$tInputs.on('keypress paste',function(){
				selSearch = 2;
				// console.log('selSearch: topbar');
			});
			cf.$jSearchTermInput.on('blur',function(){
				// kVal = $('.jsearchterm').val() + String.fromCharCode(e.keyCode);
				// console.log($('.jsearchterm').val() + String.fromCharCode(e.keyCode));
				// console.log('changed jumbotron text');
				if(selSearch === 1){
					// console.log($('.jsearchterm.tt-input').val());
					cf.$searchTerm.typeahead('val',cf.$jSearchTermInput.val())
					cf.$searchTerm.typeahead('close');
					// $('.searchterm.tt-input').val($('.jsearchterm.tt-input').val());
					// console.log($('.searchterm.tt-input').val());
				}
				else{
					cf.$jSearchTerm.typeahead('val',cf.$searchTermInput.val());
					cf.$jSearchTerm.typeahead('close');
					// $('.jsearchterm.tt-input').val($('.searchterm.tt-input').val());
				}
			});
			cf.$jSearchLocationInput.on('blur',function(){
				// console.log(e);
				// console.log($('.jsearchlocation').val() + String.fromCharCode(e.keyCode));
				// console.log($('.jsearchlocation').val());
				if(selSearch === 1){
					cf.$searchLocation.typeahead('val',cf.$jSearchLocationInput.val());
					cf.$searchLocation.typeahead('close');
					// $('.searchlocation.tt-input').val(cf.$jSearchLocationInput.val());
				}
				else{
					cf.$jSearchLocation.typeahead('val',$(cf.$searchLocationInput).val());
					cf.$jSearchLocation.typeahead('close');
					// cf.$jSearchLocationInput.val($('.searchlocation.tt-input').val());
				}
			});
			cf.$searchTermInput.on('blur',function(){
				// kVal = $('.searchterm').val() + String.fromCharCode(e.keyCode);
				// console.log($('.searchterm').val() + String.fromCharCode(e.keyCode));
				if(selSearch === 2){
					console.log(cf.$searchTermInput.val());
					cf.$jSearchTerm.typeahead('val',cf.$searchTermInput.val());
					cf.$jSearchTerm.typeahead('close');
					// $('.jsearchterm.tt-input').val($('.searchterm.tt-input').val());
					// console.log($('.jsearchterm.tt-input').val());
				}
				else{
					cf.$searchTerm.typeahead('val',cf.$jSearchTermInput.val());
					cf.$searchTerm.typeahead('close');
					// $('.searchterm.tt-input').val(cf.$jSearchTermInput.val());
				}
			});
			cf.$searchLocationInput.on('blur',function(){
				// console.log(e);
				// console.log(cf.$searchLocation.val() + String.fromCharCode(e.keyCode));
				// console.log(cf.$searchLocation.val());
				if(selSearch === 2){
					cf.$jSearchLocation.typeahead('val',cf.$searchLocationInput.val());
					cf.$jSearchLocation.typeahead('close');
					// cf.$jSearchLocationInput.val($('.searchlocation.tt-input').val());
				}
				else{
					cf.$searchLocation.typeahead('val',cf.$jSearchLocationInput.val());
					cf.$searchLocation.typeahead('close');
					// $('.searchlocation.tt-input').val($('.jsearchlocation.tt-input').val());
				}
			});
			this.events();
		},
		events:function(){
			//
			var self = this,
				cf = this.config;

			this.subscribe('vc:waypoint', function(e, data){
				var direction = (typeof data === 'undefined') ? e : data || data.direction;
				// console.log(cf);
				// console.log(cf.$jSearchTerm);
				// console.log(cf.$jSearchTermInput);
				if (direction === 'down') {
					if (cf.$jSearchTermInput.is(':focus')) {
						// console.log(cf);
						cf.$searchTermInput.focus();
						cf.$jSearchTerm.typeahead('close');
						cf.$searchLocation.typeahead('close');
						cf.$jSearchLocation.typeahead('close');
					}
					if (cf.$jSearchLocationInput.is(':focus')) {
						cf.$searchLocationInput.focus();
						cf.$jSearchLocation.typeahead('close');
						cf.$searchTerm.typeahead('close');
						cf.$jSearchTerm.typeahead('close');
					}
				}
				else if (direction === 'up') {
					if (cf.$searchTermInput.is(':focus')) {
						cf.$jSearchTermInput.focus();
						cf.$searchTerm.typeahead('close');
						cf.$searchLocation.typeahead('close');
						cf.$jSearchLocation.typeahead('close');
					}
					if (cf.$searchLocationInput.is(':focus')) {
						cf.$jSearchLocationInput.focus();
						cf.$searchLocation.typeahead('close');
						cf.$searchTerm.typeahead('close');
						cf.$jSearchTerm.typeahead('close');
					}
				}
			});
		}
	};
}(jQuery));
