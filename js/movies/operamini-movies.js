;(function($, window, document, undefined){

    var
    	showingDate, nextShowingDate, today, tomorrow, isSmall, isMedium, isLarge,
			now             = new Date().getTime(),
			aDay            = 86400000,//millisecnds
			currentIndex    = 0,
			$showTimeNavbar = $( ".show-time-navbar" ),
			$showDay        = $showTimeNavbar
				               .find( "span#showing-day" ),
			$timeHolder     = $showTimeNavbar
				               .find( 'span.date-holder' )
	  ;

	vcMovies = {

    formatDate: function (time) {
    	var d,t;
    	if ((time / 1000) < 1000000000) {// its timestamp
    		t = time * 1000;
    	}
    	else {// its js time
    		t = time;
    	};

    	d = new Date(t).toDateString().split(' ');

      date = d[0] + ', ' + d[1] + ' ' + d[2];

      return date;
    },

    searchView: {
    	setSmallFilter: function () {
     		var $listingHead       = $('.small-listing-head'),
            $movieWrapper      = $('.movie-wrapper'),
            $smallFilter       = $('.small-filter-wrapper'),
            $smallFilterHead   = $smallFilter.find('.filter-head'),
            $smallFilterCancel = $smallFilter.find('.cancel'),
            filterViewHeight   = window.innerHeight - ($('header').outerHeight(true))
        ;

        $listingHead
          .find('button.filter')
            .on('click',function() {
              $listingHead
                .removeClass('show-for-small-only')
                .hide();
              $movieWrapper.hide();
              $smallFilter.show();
              $smallFilterHead.show();
                $('.small-filter-view').height(filterViewHeight - 90);
        });

      	$smallFilterCancel.on('click',function() {
          $smallFilter.hide();
          $listingHead
            .addClass('show-for-small-only')
            .show();
          $movieWrapper.show();
      	});
   		},

   		setMoreView: function () {
      	var $moreMovies          = $('.more-movies-btn'),
            $morecinemas_list    = $('.more-cinemas_list-btn'),
            $cinemas_listWrapper = $('#cinemas_list-wrapper'),
            $moviesWrapper       = $('#movies-wrapper')
        ;

        $moreMovies.on('click',function(e) {
          e.preventDefault();
          $cinemas_listWrapper.hide();

          if (isMedium) {
            $moviesWrapper
              .removeClass('medium-6')
              .find('.movie')
                .addClass('medium-6');
              return;
          } else if (isLarge){
            $moviesWrapper
              .removeClass('large-8')
              .addClass('large-12')
              .find('.movie')
                .removeClass('large-6')
                .addClass('large-4');
          };
   		  });

        $('.more-cinemas_list-btn').on('click',function(e) {
          e.preventDefault();
          $moviesWrapper.hide();
          if (isMedium) {
            $cinemas_listWrapper
              .removeClass('medium-6')
              .addClass('medium-12')
              .find('.cinema-holder')
                .addClass('medium-6');
              return;
          } else if (isLarge){
            $cinemas_listWrapper
              .removeClass('large-4')
              .addClass('large-12')
              .find('.cinema-holder')
                .addClass('large-4');
          };
        });
      },

      init: function() {
        this.setSmallFilter();
        this.setMoreView();
        $('.tabs-content, .content').removeClass('tabs-content');
      }
    },

		reminder: {

		  $modal:  $('#reminderModal'),

		  showReminderSuccess: function () {
		  	var m = this.$modal;
		  	m.find('.first-stage')
		  		.find('.wait').hide().end()
		  		.fadeOut(200,function(){
          	m.find('.last-stage').fadeIn(400)
	            	.find('.close').on('click',function(){
		              m.foundation('reveal', 'close');
		              $(this).off('click');
		            });
        });
		  },

		  showReminderLogin: function  () {
		  	var m = this.$modal;
		  	m.find('.first-stage')
		  		.find('.wait').hide().end()
		  		.fadeOut(200,function(){
          	$(this).next().fadeIn(400);
        });
		  },

		  sendReminder: function () {
        var data = {},
            self = this;

        data["movieID"]  = this.movie_id;
        data["cinemaID"] = this.cinema_id;
        data["time"]     = this.timestamp;
        data             = JSON.stringify(data);

				jQuery.ajax({
					url: 'http://stagemovie.vconnect.com/reminder',
					type: 'PUT',
					contentType: 'application/json',
					processData: false,
					data: data,
					success: function( data, textStatus, jqXHR ) {
		        self.showReminderSuccess();
					},
					statusCode: {
				    401: function() {
				      self.showReminderLogin();
				    }
				  }
				});
		  },

		  setReminder: function () {// set and post the reminder
        var self = this;

      	self.movie_id  = this.$modal.find('.movie').data('movie-id');
    		self.cinema_id = this.$modal.find('.cinema').data('cinema-id');
    		self.timestamp = this.$modal.find('#reminder-time option:selected').data('time');

        this.$modal
        	.find('.ok')
        		.removeClass('disabled')
        		.on('click',function(e){
		          e.preventDefault();

		          var $this = $(this),
		          	$firstStage = $this.closest('.first-stage');

		          if (document.cookie.indexOf('AuthHash') >= 0) { // the user is loged in
		          	// var $lastStage = $firstStage.siblings('.last-stage');
		          	$firstStage.find('.wait').fadeIn(400);
		            $(this).addClass('disabled').off('click');
		            return self.sendReminder();
		          }
		          else{
		          	//set cookie to store selected reminder; show login;
		          	var xpireDate    = new Date(now + aDay).toUTCString();

		         		document.cookie = "vcMoviesReminderDate="
		         		                +self.movie_id
		         		                +"-"
		         		                +self.cinema_id
		         		                +"-"
		         		                +self.timestamp
		         		                +"; expires="
		         		                +xpireDate
		         		;

		            return self.showReminderLogin();
		          }
            });
      },

		  buildReminderModal: function (){//builds and opens the reminder modal
        var $this       = $( this ),
            $gParent    = $this.closest( '.cinema-time' ),
            movie       = $( 'h1.movie-name' ),
            cinema      = $gParent.find( '.cinema-name' ),
            times       = $gParent.find('.time'),
            timeOptions = ''
        ;

        times.each(function(i){
          var option ='<option data-time="'
            + $(this).data("time")
            + '" value="'
            + $(this).text()
            + '">'
            + $(this).text()
            +'</option>'
          ;

          timeOptions += option;
        });

        vcMovies.reminder.$modal
         .find('.cinema')
        	.data('cinema-id', cinema.data('cinema-id'))
        	.text(cinema.text())
        	.end()
         .find('.movie')
        	.data('movie-id', movie.data('movie-id'))
        	.text(movie.text())
        	.end()
         .find(' .date-holder')
       		.val(new Date($this.data('time')*1000).toDateString())
       		.end()
         .find('#reminder-time')
          	.html(timeOptions)
            .val($this.text())
            .end()
         .find('.wait')
      		.hide()
      		.end()
         .foundation('reveal', 'open')
        ;

        vcMovies.reminder.setReminder();
	    }
		},

		movieTimetable: {

			show_dates_list: [],

			showDate: function () {

			  if (vcMovies.formatDate(showingDate) === today ) {

					$showDay.text( "Today" );
				}
				else if (vcMovies.formatDate(showingDate) === tomorrow) {
					$showDay.text( "Tomorrow" );
				}
				else{
					$showDay.text(vcMovies.formatDate(showingDate));
				};

				if (vcMovies.formatDate(nextShowingDate) === tomorrow) {
			  	$timeHolder.text('Tomorrow');
				}
				else {
					$timeHolder.text(vcMovies.formatDate(nextShowingDate));
				};
			},

			buildTimeButtons: function ( timestamps, _showtimes ) {

				var showtimes = _showtimes.split(', '),
				    btn        = ''
				;

				$.each(timestamps, function(i){
					// use this button when you support reminder for operamini
					// btn += '<button data-time="'
	    //  				+ timestamps[i]
	    //         + '" class="small time radius">'
	    //     		+ showtimes[i]
     //          + '<i class="operamini-icon icon-angle-right"></i></button>';


     		btn += '<span data-time="'
	     				+ timestamps[i]
	            + '" class="operamini time">'
	        		+ showtimes[i]
              + '</span>';

  		  });

        return btn;
			},

			build_not_showing_html: function() {

				var
					style = '',
				    html  = ''
				;

				style =  'color: gray;';
				style += 'background-color: #F0F0F0;';
				style += 'padding: 10px';

				html += '<h6 class="text-center" style="'
						+ style
						+ '">Sorry, this movie is not currently showing in cinemas!'
						+ '</h6>'
				;

				this.$cinemas_list_showtimes_container.html(html);
			},

			build_showtimes_html: function( cinemas_list ) {

				var cinema    = '',
					cinema_html = '',
					self        = this
				;

				$.each(cinemas_list, function(i) {
					var
						el = '<dt>'+cinemas_list[i].cineman_name+'</dt>'
					     + '<dd>'
					       + self.buildTimeButtons(cinemas_list[i].timestamps, cinemas_list[i].show_times)
					     + '</dd>';

					cinema += el;
			    cinema_html += cinema;
			    cinema = '';
				});

				this.$cinemas_list_showtimes_container.html(cinema_html).off('click');

				// this.$cinemas_list_showtimes_container.html(cinema_html)
				// .find('button.time')
				// 	.on('click', vcMovies.reminder.buildReminderModal)
				// 	.end()
				// .find('a.cinema-name');
			},

			regulateDate: function() {

				var self = vcMovies.movieTimetable;

				if ( $( this ).attr( "id" ) === "prev-btn" ) {//its back btn
					currentIndex -= 1;

					$timeHolder.show();
					$( this ).siblings( '#next-btn' ).css( "visibility", "visible" );

					if (currentIndex === 0) {
						$( this ).css( "visibility","hidden" );
					};
				}
				else if ( $( this ).attr( "id" ) === "next-btn" ) { //its next btn
					currentIndex += 1;

					if (!self.show_dates_list[currentIndex  + 1]) {
						$( this ).css( "visibility","hidden" );
						$timeHolder.hide();
					}

					$( this ).siblings( '#prev-btn' ).css( "visibility","visible" );
				}

				nextShowingDate = self.show_dates_list[currentIndex + 1];
				showingDate     = self.show_dates_list[currentIndex];
				self.showDate();
				self.build_showtimes_html( self.showTimes[self.show_dates_list[currentIndex]] );
			},

			init: function( showtimes ) {

				var self = this,
				    date_counter = 0;

				this.$cinemas_list_showtimes_container = $( 'dl.small-time-table' );

				if ( typeof showtimes !== 'object' ) {
					$( ".show-time-navbar" ).hide();
					return this.build_not_showing_html();
				}

        // remove past dates and augment show_dates_list[]
				$.each(showtimes, function  (date, val) {
					if ((date * 1000 + aDay)  <  now) {

						delete showtimes[date];

						if ( $.isEmptyObject(showtimes) ) {
							$( ".show-time-navbar" ).hide();
							self.build_not_showing_html();
							return false;
						}

						return true;
					}
					else{
						self.show_dates_list[date_counter] = date;
						date_counter += 1;
					}
				});

				$(".show-time-navbar button#prev-btn")
				.css("visibility","hidden");

				if (this.show_dates_list.length < 2) {
					$(".show-time-navbar button#next-btn")
					.css("visibility","hidden");
					$timeHolder.hide();
				}
				else {
					nextShowingDate = this.show_dates_list[currentIndex + 1];
				}

				showingDate = this.show_dates_list[currentIndex];

				this.showTimes = showtimes;
				this.showDate();
				$(".show-time-navbar button").on('click',this.regulateDate);

				this.build_showtimes_html( this.showTimes[this.show_dates_list[currentIndex]] );
			}
		},

		searchNav: {
			name: 'searchNav',
			description: 'Mobile search navigation bar',
			config:{
				searchButton : $('#search-nav'),
	            normalHeader : $('#normal-header'),
	            searchHeader : $('#search-header'),
	            mainSection  : $('.main-section'),
	            overLayHtml  : "<div class='search-overlay' style='display:block'></div>",
	            searchHeaderClose : $('.close-nav-link')
			},
			oldSearch:function () {
				var self = this;
				$(self.config.searchButton).click(function  (e) {
	                e.preventDefault();
	                self.config.normalHeader.hide();
	                self.config.mainSection.prepend(self.config.overLayHtml);
	                self.config.searchHeader.show();

	            });
			},

			handleSearch:function () {
				var self = this;
				$('[data-search]').click(function(e){

				  e.preventDefault();
				  $(this).toggleClass('active');
				  $('.search-bar').toggleClass('hide');
				  $('.search-bar').toggleClass('active');
				  $('#normal-header').toggleClass('has-search');
				});

			},

			showLocation: function () {
				var self = this,
						val = "";
				$('[data-change-location]').click(function(e){
					e.preventDefault();
					$('.input-container').addClass('hide');
					$('.input-container').find('[' + self.attr_name('serach-active') + ']').each(function () {
						$(this).attr(self.attr_name('serach-active'), 0);
					})
					$('.input-container').find('[' + self.attr_name('serach-active') + ']').each(function () {
						val = $(this).val()
					});
					$('.location').addClass('hide');
					$('.location-search').removeClass('hide');
					$('.location-search').find('[' + self.attr_name('serach-active') + ']').each(function () {
						$(this).val(val);
					});
					$('.location-search').find('[' + self.attr_name('serach-active') + ']').each(function () {
						$(this).attr(self.attr_name('serach-active'), 1);
					});
				});
			},

			init:function(){
				// Contains the initialization code
				var self = this;
				this.oldSearch();
				this.handleSearch();
				this.events();
			},
			events:function(){
				// Contains the event bindings and subscriptions
				var self =  this;
				$(self.config.searchHeaderClose).click(function (e) {
	                e.preventDefault();
	                self.config.searchHeader.hide();
	                $('.search-overlay').remove();
	                self.config.normalHeader.show();
	            });
				this.showLocation();
			}
		},

		offCanvas: {
			init: function  () {

				$('.left-off-canvas-toggle').click(function (e) {
					e.preventDefault();
					var left = (window.screen.width/100)*70;
						leftOffest = (window.screen.width/100)*70;
					$('.mobile-wrap').addClass('move-right');
					$('.inner-wrap').css({'left': left+'px'});
					$('.exit-off-canvas').css('display', 'block');
					$('.left-menu').css({'width': left+'px !important','left': -(leftOffest)+'px',  'z-index': 10002});
				});

				$('.exit-off-canvas').click(function (e) {
					e.preventDefault();
					$('.inner-wrap').css({'left': ''});
					$('.mobile-wrap').removeClass('move-right');
					$('.exit-off-canvas').css('display', 'none');
					//if (window.screen.width < 290) {
						var left = (window.screen.width/100)*70;
							leftOffest = (window.screen.width/100)*70;
							$('.left-menu').css({'left': -leftOffest+'px'});
					//};
				});
			}
		},

		cookies: {
			getCookie: function (sKey) {
				return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
			},
			createCookie: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
			    if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return false; }
			    var sExpires = "";
			    if (vEnd) {
				    switch (vEnd.constructor) {
				        case Number:
				        	if (vEnd === Infinity) {
				        		sExpires =  "; expires=Fri, 31 Dec 9999 23:59:59 GMT";
				        	} else{
				        		var date = new Date();
				                	date.setTime(date.getTime() + (vEnd * 24 * 60 * 60 * 1000));
				                sExpires = "; expires=" + date.toGMTString();
				        	}
				        break;
				        case String:
				          	sExpires = "; expires=" + vEnd;
				        break;
				        case Date:
				          	sExpires = "; expires=" + vEnd.toUTCString();
				        break;
				    }
			    }
			    document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
			    return true;
			},
			removeCookie: function (sKey, sPath, sDomain) {
			    if (!sKey || !this.hasCookie(sKey)) { return false; }
			    document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + ( sDomain ? "; domain=" + sDomain : "") + ( sPath ? "; path=" + sPath : "");
			    return true;
			},
			hasCookie: function (sKey) {
			    return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
			 },
		},

		init: function ( showtimes ) {
			var self = this;
			// this.checkWindowWidth();

			$( 'body' ).removeClass( 'f-topbar-fixed' );//for parfect rendering of the off-canvas

			$(document).on('closed.fndtn.reveal', '[data-reveal]', function () {
       	var $this = $(this);

				if ($this.is('div#reminderModal')) {
				  $this.find('.first-stage').show().siblings().hide()
				};

				$(document).off('closed.fndtn.reveal');
			});

			$('header form')
				.find('button')
					.on('click',function (e) {
						console.log('I button');
						e.preventDefault();
						if ($(this).closest('form').find('input').val() === '') {
								app.utils.notifyMe({type: 'error', message: 'Please specify the movie keyword'});
								return false;
						}
						else {
							$(this).closest('form').trigger('submit');
						}
					});

			// if (document.cookie.indexOf('rSet') > -1) {
			// 	document.cookie = "rSet=''; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
			// 	$('#reminderModal').foundation('reveal', 'open');
			// 	this.reminder.showReminderSuccess();
			// };
			//checking for cookies
			if(this.cookies.getCookie('rSet')){
				this.cookies.removeCookie('rSet');
				$('#reminderModal').foundation('reveal', 'open');
				this.reminder.showReminderSuccess();
			}

			// this.resizeMobileWidth();
			this.searchView.init();
			this.searchNav.init();
			this.offCanvas.init();
			today           = vcMovies.formatDate(now),
			tomorrow        = vcMovies.formatDate(now + aDay),
   	  this.movieTimetable.init( showtimes );
		}
	}
})( jQuery, window, document );
//
