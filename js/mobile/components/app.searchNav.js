;(function($, window, document, undefined){
	window.app.comps.searchNav = {
		name: 'searchNav',
		description: 'Mobile search navigation bar',
		config:{
			searchButton : $('#search-nav'),
            normalHeader : $('#normal-header'),
            searchHeader : $('#search-header'),
            mainSection  : $('.main-section'),
            overLayHtml  : "<div class='search-overlay' style='display:block'></div>",
            searchHeaderClose : $('.close-nav-link')
		},
		oldSearch:function () {
			var self = this;
			$(self.config.searchButton).click(function  (e) {
                e.preventDefault();
                self.config.normalHeader.hide();
                self.config.mainSection.prepend(self.config.overLayHtml);
                self.config.searchHeader.show();

            });
		},

		handleSearch:function () {
			var self = this;
			$('[data-search]').click(function(e){

			  e.preventDefault();
			  $(this).toggleClass('active');
			  $('.search-bar').toggleClass('hide');
			  $('.search-bar').toggleClass('active');
			  $('#normal-header').toggleClass('has-search');
			});

		},

		showLocation: function () {
			var self = this,
					val = "";
			$('[data-change-location]').click(function(e){
				e.preventDefault();
				$('.input-container').addClass('hide');
				$('.input-container').find('[' + self.attr_name('serach-active') + ']').each(function () {
					$(this).attr(self.attr_name('serach-active'), 0);
				})
				$('.input-container').find('[' + self.attr_name('serach-active') + ']').each(function () {
					val = $(this).val()
				});
				$('.location').addClass('hide');
				$('.location-search').removeClass('hide');
				$('.location-search').find('[' + self.attr_name('serach-active') + ']').each(function () {
					$(this).val(val);
				});
				$('.location-search').find('[' + self.attr_name('serach-active') + ']').each(function () {
					$(this).attr(self.attr_name('serach-active'), 1);
				});
			});
		},

		init:function(){
			// Contains the initialization code
			var self = this;
			this.oldSearch();
			this.handleSearch();
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
			var self =  this;
			$(self.config.searchHeaderClose).click(function (e) {
                e.preventDefault();
                self.config.searchHeader.hide();
                $('.search-overlay').remove();
                self.config.normalHeader.show();
            });
			this.showLocation();
		}
	};
}(jQuery, window, document));
