/**
	component_name - component_description

	Usage: 'How to use component'
	Options:
		Option_name - Option_description
			Values: Possible_option_values
	Example:
		Option_example
*/

;(function($, window, document, undefined){
	window.app.comps.reviews = {
		config: {
			reviewsTemplate:  $('#reviewsTemplate').html(),
			LoadReviewstemplate: $('#loadReviewsTemplate').html(),
			cmplReviewsBtnTemplate: null,
			cmplReviewsTemplate: null,
			helpfulReviewsURL: null,
			allReviewsURL: null
		},

		init:function(){
			// Contains the initialization code
			// ...
			var self = this;
			//console.log(document.getElementById('reviewsTemplate').innerHTML());
			//console.log('attr: '+app.attr_name('load-reviews'));
			if ($(app.attr_name('load-reviews')).length > 0) {

				self.config.cmplReviewsTemplate = doT.template(self.config.reviewsTemplate);
				self.config.cmplReviewsBtnTemplate = doT.template(self.config.LoadReviewstemplate);
				self.config.helpfulReviewsURL = app.config.helpfulReviewsURL;
				self.config.allReviewsURL = app.config.allReviewsURL;
				this.events();

			};

		},
		attachLoadBtn: function (ele, type) {
			var self =  this;

			$(ele).append(self.config.cmplReviewsBtnTemplate({type: type}));
		},
		attachLoadTemplate: function (ele) {
			var self = this,
					template = $('#loadTemplate').html();
			$(ele).append(template);
		},
		attachLoadFailTemplate: function (ele) {
			var self = this,
					template = $('#LoadFailedTemplate').html();
			$(ele).append(template);
		},
		removeLoadFailTemplate: function (ele) {
			var self = this;
			$(ele).find('.load-error').detach();
		},
		removeLoadTemplate: function  (ele) {
			var self = this;
			$(ele).find('.loading-reviews').detach()
		},
		updatePageCount: function (ele, count) {
			$(ele).attr(app.attr_name('page'), count);
		},
		updateReviewsCount: function (ele, count) {
			$(ele).attr(app.attr_name('count'), count);
		},
		getReviews: function  (url, page, callback, errCallback) {
			$.getJSON(url, {page: page})
			 .done(function (data) {
			 		callback(data);
			 })
			 .fail(function (xhrObj, status, error) {
			 	err =  status + ", " + error;
			 	console.warn("Failed to load reviewsn"+ err);
			 	if(typeof errCallback === "function"){
			 		errCallback(xhrObj, status, error);
			 	}
			 	return false;
			 });
		},
		renderReviews: function (ele, data) {
			var self = this;
			// console.log($(ele));
			// console.log(data);
			// console.log(self.config.cmplReviewsTemplate);
			$.each(data, function(i){
				$(ele).append(self.config.cmplReviewsTemplate(this))
			});
		},
		loadMoreReviews: function (type, fn, efn) {
			console.log('hello');
			var self = this,
					currPage = parseInt($('['+app.attr_name('review-'+type)+']').attr(app.attr_name('page'))),
					nextPage = currPage + 1,
					reviewsCount = parseInt($('['+app.attr_name('review-'+type)+']').attr(app.attr_name('count'))),
					totalPages = $('['+app.attr_name('review-'+type)+']').attr(app.attr_name('total-pages'));

			// console.log('currentpage: '+currPage);
			// console.log('next Page: '+nextPage);
			// console.log('total Pages: '+totalPages);
			if(currPage < totalPages ){
				self.getReviews(self.config.helpfulReviewsURL, nextPage, function (response) {
					// console.log(response);
					// console.log(typeof response);
					// console.log(response.status);
					if (typeof response === 'object' && response.status === 1) {
						self.renderReviews($('['+app.attr_name('review-'+type)+']'), response.reviews);

						if(currPage < totalPages && nextPage != totalPages){
							self.updatePageCount($('['+app.attr_name('review-'+type)+']'), nextPage);
							self.updateReviewsCount($('['+app.attr_name('review-'+type)+']'), reviewsCount+response.reviews.length);
						}else {
							self.updatePageCount($('['+app.attr_name('review-'+type)+']'), nextPage);
							self.updateReviewsCount($('['+app.attr_name('review-'+type)+']'), reviewsCount+response.reviews.length);
							$('['+app.attr_name('load')+'="reviews-'+type+'"]').detach();
						}
						//console.log(nextPage);
						//console.log($('['+app.attr_name('review-helpful')+']').attr(app.attr_name('page', nextPage)));
					}else{
						console.warn('could not load reviews');
						if ((currPage >= totalPages )) {
								$('['+app.attr_name('load')+'="reviews-'+type+'"]').detach();
						};
					}
					fn();
				}, function (xhrObj, status, error) {
					// body...
					self.removeLoadTemplate($('#'+type+'-reviews'));
					self.attachLoadFailTemplate($('#'+type+'-reviews'));
					if (typeof efn === "function") {
						efn();
					};
				});
			}else{
				console.warn('no more reviews to load');
				$('['+app.attr_name('load')+'="reviews-'+type+'"]').detach();
			}
			//console.log(currPage <= totalPages );
			//console.log(response);
		},

		handleMoreReviews: function (type) {
			// console.log(type)
			// console.log('['+app.attr_name('load')+'="reviews-'+type+'"'+']');
			var self = this;
			$('#'+type+'-reviews').on('click', '['+app.attr_name('load')+'="reviews-'+type+'"'+']', function (e) {
				var el = this;
				e.preventDefault();
				$(this).html('<img src="../img/vcloader-dots.gif" />');
				self.loadMoreReviews(type, function () {
					$(el).html('View more reviews')
				}, function(){
					$(el).html('View more reviews');
					self.removeLoadFailTemplate($('#'+type+'-reviews'));
				});

			})
		},

		loadInitialReviews: function (type) {
			var self = this,
					reviewsCount = $('['+app.attr_name('review-'+type+'')+']').attr(app.attr_name('count'));
					reviewsCount = parseInt(reviewsCount);
			var	currPage = $('['+app.attr_name('review-'+type)+']').attr(app.attr_name('page'));
					currPage = parseInt(currPage);
			var	nextPage = currPage,
				  url = (type === 'helpful') ? self.config.helpfulReviewsURL : self.config.allReviewsURL;
			//console.log(reviewsCount);
			// console.log(type);
			// console.log('['+app.attr_name('review-'+type)+']');
			if (reviewsCount === 0) {
				self.attachLoadTemplate($('#'+type+'-reviews'));
				self.getReviews(url, nextPage+1, function (response) {
					if (typeof response === 'object' && response.status === 1) {
						self.renderReviews($('['+app.attr_name('review-'+type)+']'), response.reviews);
						self.updatePageCount($('['+app.attr_name('review-'+type)+']'), nextPage+1);
						self.updateReviewsCount($('['+app.attr_name('review-'+type)+']'), reviewsCount+response.reviews.length);

						self.removeLoadTemplate($('#'+type+'-reviews'));
						self.attachLoadBtn($('#'+type+'-reviews'), type);
					}else{
						console.warn('could not load reviews');
					}
				}, function (xhrObj, status, error) {
					// body...
					self.removeLoadTemplate($('#'+type+'-reviews'));
					self.attachLoadFailTemplate($('#'+type+'-reviews'));
				});
			};
		},

		// retry: function (type, fn) {
		// 	var self =  this,
		// 		  retryCount = 0
		// 	if (typeof fn === "function" && retryCount < 5) {
		// 		fn(type);
		// 		self.removeLoadFailTemplate($('#'+type+'-reviews'));
		// 		retryCount++;
		// 	};
		// },

		events:function(){
			// Contains the event bindings and subscriptions
			var self = this;
			this.handleMoreReviews('helpful');
			this.handleMoreReviews('all');
			app.subscribe('vc:hashChange', function (e, data) {
				//console.log(data);
				if(data.hash === 'tabs_all-reviews'){
					//console.log('booo');
					self.loadInitialReviews('all');
				}else if(data.hash === 'tabs_helpful-reviews'){
					self.loadInitialReviews('helpful');
				}else{
					self.loadInitialReviews('helpful');
				}

			});
		}
	};
}(jQuery, window, document));
