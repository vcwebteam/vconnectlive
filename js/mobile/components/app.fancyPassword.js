;(function($, window, document, undefined){
	window.app.comps.fancyPassword = {
		name: 'fancyPassword',
		description: 'description',
		config: {
			fancyPassword: {
				maskToggle     : null, 
				passwordElement: null, 
				toggeled       : null 
			}
		},
		init:function(){
			// Contains the initialization code
			var self  = this;
			self.config.fancyPassword.maskToggle      = $('#toggle-mask'),
			self.config.fancyPassword.passwordElement = $("input[type='password']"),
			self.config.fancyPassword.toggeled        =  ($('#toggle-mask:checked').length === 1)? true : false,

			this.events();
		},
		toggle: function (toggle, passwordEle) {
			var self = this;

			passwordEle.attr('type', (toggle) ? 'text' : 'password');
		},
		events:function(){
			// Contains the event bindings and subscriptions
			var self = this;
			self.config.fancyPassword.maskToggle.click(function (e) {
				self.config.fancyPassword.toggeled = ($('#toggle-mask:checked').length === 1)? true : false;
				self.toggle(self.config.fancyPassword.toggeled, self.config.fancyPassword.passwordElement );
			});
		}
	};
}(jQuery, window, document));