/**
	buttontoggle - Handles the toggle states and behavior of action buttons

	Usage: 'How to use component'
	Options:
		Option_name - Option_description
			Values: Possible_option_values
	Example:
		Option_example
*/

;(function($, window, document, undefined){
	window.app.comps.rateReview = {
		init:function(){
			// Contains the initialization code
			// ...
			var self = this,
				config = this.config;
			config.rateReview.button = $('[' + self.attr_name('review-toggle') + ']');
			this.events();
		},
		events:function(){
			var self = this,
				config = this.config;
			// Contains the event bindings and subscriptions
			$(document).on('click', '[' + self.attr_name('review-toggle') + ']', function(e){
				e.preventDefault();
				var el = this;
				var src = $(el).attr('href');
				var data = $(el).attr(self.attr_name('options'));
				var elId = $(el).attr(self.attr_name('review-id'));
				var cls = $(el).attr(self.attr_name('review-toggle'));
				var isActv = $(el).attr(self.attr_name('active'));
				var sibling = $('[' + self.attr_name('review-id') +'='+elId+ ']').not(el);
				var slbIsActv = $(sibling).attr(self.attr_name('active'));
				data = self.data_options(data);
				// console.log(src);
				// console.log(data);
				// console.log(cls);
				// console.log(isActv);
				// console.log(sibling);
				// console.log(elId);
				// console.log(slbIsActv);
				if (isActv == 1) {
					//do nothing write some code to give feedback to the user
				} else{
					$.getJSON(src, data).done(function(result){
							//console.log(result);
							if ('status' in result && result.status === 1){
								if (slbIsActv) {
									// removing active from sibling
									self.updateSate(sibling, cls, 0);
									$(sibling).attr(self.attr_name('active'), 0);

									//setting active to current element
									self.updateSate(el, cls, 1);
									$(el).attr(self.attr_name('active'), 1);
								} else{
									//setting active to current element
									self.updateSate(el, cls, 1);
									$(el).attr(self.attr_name('active'), 1);
								};
							}
					}).fail(function(){
						console.warn('AJAX endpoint: ' + src);
						self.notifyMe({type: "error", message:"sorry could not rate review"});
						// $(el).toggleClass(cls);
					});
					// When done, publish toggle state change or handle within this component
				};
			});
		},

		updateSate: function (el, cls, status) {
			// body...
			if (status == 1) {$(el).addClass(cls)} else{$(el).removeClass(cls)};
			//console.log(el);
			// console.log(cls);
			// console.log(status);
		},

		data_options:function(options){
			var opts = {};
			// Idea from Foundation.js by Zurb
      if (typeof options === 'object') {
        return options;
      }

      opts_arr = (options || '=').split(';'),
      ii = opts_arr.length;

      function isNumber (o) {
        return ! isNaN (o-0) && o !== null && o !== "" && o !== false && o !== true;
      }

      // Polyfill for trim function in IE8 down
      if (!String.prototype.trim) {
			  String.prototype.trim = function () {
			    return this.replace(/^\s+|\s+$/g, '');
			  };
			}
      function trim(str) {
        if (typeof str === 'string') return str.trim();
        return str;
      }

      while (ii--) {
        p = opts_arr[ii].split('=');

        if (/true/i.test(p[1])) p[1] = true;
        if (/false/i.test(p[1])) p[1] = false;
        if (isNumber(p[1])) p[1] = parseInt(p[1], 10);

        if (p.length === 2 && p[0].length > 0) {
          opts[trim(p[0])] = trim(p[1]);
        }
      }

      return opts;
		}
	};
}(jQuery, window, document));
