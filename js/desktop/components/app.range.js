/**
	range - Handles the input range behaviour

	Usage: 'How to use component'
	Options:
		Option_name - Option_description
			Values: Possible_option_values
	Example:
		Option_example
*/

;(function($, window, document, undefined){
	window.app.comps.range = {
		config:{
			$rangeSlider: $('[data-vc-range]'),
			$rangeTextLower: $('[data-vc-range-text-lower]'),
			$rangeTextUpper: $('[data-vc-range-text-upper]'),
			rangeMin: 0,
			rangeMax: 100
		},
		init:function(){
			// Contains the initialization code
			// ...
			var self = this,
					cf = self.config;

			cf.$rangeSlider = $(cf.$rangeSlider.selector);
			cf.$rangeTextLower = $(cf.$rangeTextLower.selector);
			cf.$rangeTextUpper = $(cf.$rangeTextUpper.selector);

			var _min = cf.$rangeSlider.attr(this.attr_name('min')); //set min range value
			var _max = cf.$rangeSlider.attr(this.attr_name('max')); //set max range value

			var _setmin = cf.$rangeSlider.attr(this.attr_name('setmin')); //set min position
			var _setmax = cf.$rangeSlider.attr(this.attr_name('setmax')); //set max position


			cf.rangeMin = _min || cf.rangeMin;
			cf.rangeMax = _max || cf.rangeMax;

			cf.setMin = _setmin || cf.rangeMin;
			cf.setMax = _setmax || cf.rangeMax;

			cf.$rangeSlider.noUiSlider({
				start: [cf.setMin, cf.setMax],
				connect: true,
				// format: wNumb({
				// 	thousand: ',',
				// 	decimals: 2
				// }),
			step: 100,
				range: {
					'min': +cf.rangeMin, //convert to integers...error with string
					'max': +cf.rangeMax
				}
			}, true);
			cf.$rangeSlider.Link('lower').to(cf.$rangeTextLower);
			cf.$rangeSlider.Link('upper').to(cf.$rangeTextUpper);
			this.events();

			// check so it doesn't subscribe multiple times
			if(!cf.isSubscribed)this.subscriptions();
			cf.isSubscribed = true;
		},
		events:function(){
			// Contains the event bindings and subscriptions
			var self = this,
					cf = self.config;

			cf.$rangeSlider.on('set', function(){
				self.publish('vc:store/filter', {filters:[{
					checked: true,
					type: 'minprice',
					val: +$(this).val()[0]
				},{
					checked: true,
					type: 'maxprice',
					val: +$(this).val()[1]
				}]});
			});
			cf.$rangeSlider.on('set', function(){
				// console.log(arguments);
			});
		},
		subscriptions:function(){
			var self = this,
					cf = self.config;

			self.subscribe('vc:store/refresh', function(){
				self.init();
			});

			self.subscribe('vc:store/setprice', function(e, data){
				console.log('Setting the price...');
				if(!self.isArrayEqual(data.range, cf.$rangeSlider.val()))cf.$rangeSlider.val(data.range);
			});
		},
		isArrayEqual:function(arr1, arr2){
			if(arr1.length != arr2.length)return false;
			for(var i=0; i < arr1.length; i++){
				if(arr1[i] != arr2[i])return false;
			}
			return true;
		}
	};
}(jQuery, window, document));
