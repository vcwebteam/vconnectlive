;(function($, window, document, undefined){
	window.app.comps.copy = {
		name: 'copy',
		description: 'Responsible for the copy-to-clipboard logic',
		init:function(){
			// Contains the initialization code
			var cf = this.config,
				self = this;
			//initialize ZClip - Copy text
			this.require('zclip', function(){
				return jQuery().zclip;
			}, function(){
				if(cf.$copyTextButton.length > 0){
					cf.$copyTextButton.zclip({
						path:cf.zClipSwfPath,
						copy:cf.$copyText.val(),
						afterCopy:function(){
							self.notifyMe('The link has been copied to your clipboard.');
						}
					});
				}
			});
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
		}
	};
}(jQuery, window, document));