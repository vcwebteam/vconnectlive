;(function($, window, document, undefined){
	window.app.comps.gallery = {
		name: 'gallery',
		description: 'Responsible for photo gallery logic',
		init:function(){
			// Contains the initialization code
			var cf = this.config;
			//Handles swipebox photo gallery
			this.require('swipebox', function(){
				return jQuery().swipebox;
			}, function(){
				cf.$imageThumbnail.swipebox();
			});
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
		}
	};
}(jQuery, window, document));