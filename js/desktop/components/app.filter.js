/**
	filter - For the functionality of the filter in the results page

	Usage: 'How to use component'
	Options:
		Option_name - Option_description
			Values: Possible_option_values
	Example:
		Option_example
*/

;(function($, window, document, undefined){
	window.app.comps.filter = {
		init:function(){
			// Contains the initialization code
			// ...
			var self = this,
					cf = self.config;

			cf.filterBox = $('[' + this.attr_name('filter') + ']');
			cf.sortEl = $('[' + this.attr_name('sort') + ']');

			cf.filterSearchBox = $('[' + self.attr_name('filter-search') + ']');
			cf.searchInput = cf.filterSearchBox.find('.filter-search-input');
			cf.openToggle = cf.filterBox.find('.filter-open-toggle');
			cf.filterList = cf.filterBox.find('.filter-list');
			cf.clearFilterLink = cf.filterBox.find('.clear-filter');
			this.events();
			if(!cf.isSubscribed)this.subscriptions();
			cf.isSubscribed = true;
		},
		events:function(){
			// Contains the event bindings and subscriptions
			var self = this,
					cf = self.config;

			cf.openToggle.off('click.vc.filter').on('click.vc.filter', function(e){
				e.preventDefault();
				$(this).closest('[' + self.attr_name('filter') + ']').toggleClass('close');
				$(this).toggleClass('icon-down-micro').toggleClass('icon-up-micro');
			});

			cf.clearFilterLink.off('click.vc.filter').on('click.vc.filter', function(e){
				e.preventDefault();
				var checked = $(this).prop('checked');
				var curFilterList = [];
				var parentFilter = $(this).closest('[' + self.attr_name('filter') + ']');
				var parentId = parentFilter.data('id');
				parentFilter.find('input[type="checkbox"]').each(function(index, elem){
					curFilterList.push({
						checked: false,
						type: parentId,
						val: $(elem).val()
					});
				});

				// console.log(curFilterList);
				self.publish('vc:store/filter', {filters:curFilterList});
			});

			cf.searchInput.off('keydown.vc.filter').on('keydown.vc.filter', self.debounce(function(){
				// self.filterList('one','two');
				self.filterList($(this).closest('[' + self.attr_name('filter') + ']'), $(this).val());
				// console.log(this);
			}, 300));

			// Use event delegation to improve performance
			cf.filterList.off('change.vc.filter').on('change.vc.filter', 'input[type="checkbox"]', function(){
				var checked = $(this).prop('checked');
				var parentId = $(this).closest('[' + self.attr_name('filter') + ']').data('id');
				var val = $(this).val();
				self.publish('vc:store/filter', {filters:[{
					checked: checked,
					type: parentId,
					val: val
				}]});

				// Clear the filter search
				$(this).closest('[' + self.attr_name('filter') + ']').find(cf.searchInput).val('').trigger('keydown.filter').trigger('blur.filter');
			});

			cf.filterList.off('click.vc.filter').on('click.vc.filter', '.link-filter', function(e){
				e.preventDefault();
				var parentId = $(this).closest('[' + self.attr_name('filter') + ']').data('id');
				self.publish('vc:store/filter', {filters:[{
					checked: true,
					type: parentId,
					val: $(this).data('value')
				}]});
			});
			cf.sortEl.off('change.vc.filter').on('change.vc.filter', function(){
				self.publish('vc:store/filter', {filters:[{
					checked: true,
					type: 'sort',
					val: $(this).val()
				}]});
			});
		},
		subscriptions:function(){
			var self = this,
					cf = self.config;

			self.subscribe('vc:store/refresh', function(){
				self.init();
			});
		},
		filterList:function(filterBox, val){
			// console.log(filterBox);
			var self = this,
					cf = self.config;

			filterBox.find(cf.filterList).find('li').removeClass('hide').not(function(){
				// console.log(this);
				return !val || $(this).text().toLowerCase().indexOf(val.toLowerCase()) != -1;
			}).addClass('hide');
		}
	};
}(jQuery, window, document));
