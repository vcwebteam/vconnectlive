/**
	jumbotron - controls the behavior of the jumbotron component

	Usage: 'How to use component'
	Options:
		Option_name - Option_description
			Values: Possible_option_values
	Example:
		Option_example
*/

;(function($, window, document, undefined){
	window.app.comps.jumbotron = {
		init:function(){
			// Contains the initialization code
			// ...
			var self = this,
					cf = this.config;
			// data-vc-src
			// data-vc-jumbotron
			var $jumbotron = $('[' + self.attr_name('jumbotron') + ']');
			var imgSrc = $jumbotron.attr(self.attr_name('src'));
			if(imgSrc)$jumbotron.css({
				background: 'url('+ imgSrc + ') no-repeat',
				backgroundSize: 'cover'
			});
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
		}
	};
}(jQuery, window, document));
