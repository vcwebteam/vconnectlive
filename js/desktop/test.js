;(function(){
	var Game = function(canvasId){
		var canvas = document.getElementById('screen');
		var screen = canvas.getContext('2d');
		var gameSize = {width: canvas.width, height: canvas.height};

		this.bodies = createInvaders(this).concat([new Player(this, gameSize)]);

		var self = this;
		loadSound('shoot.wav', function(shootSound){
			self.shootSound = shootSound;
			var tick = function(){
				self.update();
				self.draw(screen, gameSize);
				requestAnimationFrame(tick);
			};

			tick();
		});
	};

	Game.prototype = {
		update: function(){
			var bodies = this.bodies;
			var notCollidingWithAnythng = function(b1){
				return bodies.filter(function(b2){return colliding(b1, b2)}).length === 0;
			}
			this.bodies = this.bodies.filter(notCollidingWithAnythng);

			for(i = 0; i < this.bodies.length; i++){
				this.bodies[i].update();
			}
		},
		draw: function(screen, gameSize){
			// screen.fillRect(x, y, width, height);
			// screen.fillRect(30, 30, 40, 40);

			// clear screen before drawing
			screen.clearRect(0, 0, gameSize.width, gameSize.width);

			for(i = 0; i < this.bodies.length; i++){
				drawRect(screen, this.bodies[i]);
			}
		},
		addBody: function(body){
			this.bodies.push(body);
		},
		invadersBelow: function(invader){
			return this.bodies.filter(function(b){
				return b instanceof Invader &&
					b.center.y > invader.center.y &&
					b.center.x - invader.center.x < invader.size.width;
			}).length;
		}
	};

	var Player = function(game, gameSize){
		this.game = game;
		this.size = {width: 15, height: 15};
		this.center = {x: gameSize.width / 2, y: gameSize.height - this.size.height};
		this.keyboarder = new Keyboarder();
	}

	Player.prototype = {
		update: function(){
			if(this.keyboarder.isDown(this.keyboarder.KEYS.LEFT)){
				this.center.x -= 2;
			}
			else if(this.keyboarder.isDown(this.keyboarder.KEYS.RIGHT)){
				this.center.x += 2;
			}

			if(this.keyboarder.isDown(this.keyboarder.KEYS.SPACE)){
				var bullet = new Bullet({x: this.center.x, y: this.center.y - this.size.height / 2}, {x: 0, y: -6});

				this.game.addBody(bullet);
				this.game.shootSound.load();
				this.game.shootSound.play();
				// console.log('Firing...');
			}
		}
	};

	var Invader = function(game, center){
		this.game = game;
		this.size = {width: 15, height: 15};
		this.center = center;
		this.patrolX = 0;
		this.speedX = 0.3;
	}

	Invader.prototype = {
		update: function(){
			if(this.patrolX < 0 || this.patrolX > 40){
				this.speedX = -this.speedX;
			}

			this.center.x += this.speedX;
			this.patrolX += this.speedX;

			if(Math.random() > 0.995 && !this.game.invadersBelow(this)){
				var bullet = new Bullet({x: this.center.x, y: this.center.y + this.size.height / 2}, {x: Math.random() - 0.5, y: 3});

				this.game.addBody(bullet);
				// console.log('Firing...');
			}
		}
	};

	var createInvaders = function(game){
		var invaders = [];
		for(var i = 0; i < 24; i++){
			var x = 30 + (i % 8) * 30;
			var y = 30 + (i % 3) * 30;
			invaders.push(new Invader(game, {x: x, y: y}));
		}

		return invaders;
	}

	var Bullet = function(center, velocity){
		this.size = {width: 3, height: 3};
		this.center = center;
		this.velocity = velocity;
	}

	Bullet.prototype = {
		update: function(){
			this.center.x += this.velocity.x;
			this.center.y += this.velocity.y;
		}
	};
	var drawRect = function(screen, body){
		screen.fillRect(body.center.x - body.size.width / 2,
										body.center.y - body.size.height / 2,
										body.size.width, body.size.height);
	}

	var Keyboarder = function(){
		var keyState = {};

		window.onkeydown = function(e){
			keyState[e.keyCode] = true;
		};

		window.onkeyup = function(e){
			keyState[e.keyCode] = false;
		};

		this.isDown = function(keyCode){
			return keyState[keyCode] === true;
		};

		this.KEYS = {LEFT: 37, RIGHT: 39, SPACE: 32};
	};

	var colliding = function(b1, b2){
		return !(b1 === b2 ||
						b1.center.x + b1.size.width / 2 < b2.center.x - b2.size.width / 2 ||
						b1.center.y + b1.size.height / 2 < b2.center.y - b2.size.height / 2 ||
						b1.center.x - b1.size.width / 2 > b2.center.x + b2.size.width / 2 ||
						b1.center.y - b1.size.height / 2 > b2.center.y + b2.size.height / 2
						);
	};

	var loadSound = function(url, callback){
		var loaded = function(){
			callback(sound);
			sound.removeEventListener('canplaythrough', loaded);
		};

		var sound = new Audio(url);
		sound.addEventListener('canplaythrough', loaded);
		sound.load();
	}

	window.onload = function(){
		new Game("screen");
	};
})();
