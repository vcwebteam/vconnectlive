/**
	advert - Script containing advert-related interactivity

*/

;(function($){
	window.app.comps.advert = {
		config:{
			$advertContainer: $('.advert-container'),
			$footerBanner: $('.footer-banner'),
			elHeight: 0,
			topbarHeight: 55,
			footerOffset: 0
		},
		init:function(){
			var self = this,
					cf = this.config;
			// if(jQuery().waypoint){
			// 	// cf.$advertContainer.waypoint('sticky');
			// }
			if(cf.$advertContainer.length<=0)return false;
			this.events();
		},
		events:function(){
			//Handle document scroll tasks
			$(document).on('scroll',{self: this},this.onDocumentScroll);
		},
		onDocumentScroll:function(e){
			var self = e.data.self,
				cf = self.config;
			//Handles advertisements fixed position in listing page
			var scrollTop     = $(window).scrollTop(), //distance to the top of the page
				elementOffset = cf.$advertContainer.offset().top, // distance of advert container from the top
				footerOffset  = cf.$footerBanner.offset().top, //distance of footer from the top
				distance      = (elementOffset - scrollTop), // relative distance between the top of the page and the advert container
				adPadding = 0;
			cf.elHeight = cf.$advertContainer.height();
			//console.log(distance + ', ' + elementOffset + ', ftr:' + footerOffset);

			if(distance<=0){
				// start feeling sticky..
				//console.log(footerOffset - (scrollTop + cf.elHeight + cf.topbarHeight) + ', ' + cf.elHeight);
				if(footerOffset - (scrollTop + cf.elHeight + cf.topbarHeight)>0){
					adPadding = -(distance) + cf.topbarHeight;
				}
				else{
					adPadding = footerOffset - elementOffset - cf.elHeight;
				}
				cf.$advertContainer.css('padding-top', adPadding);
			}
			else{
				// stop feeling sticky..
				cf.$advertContainer.css('padding-top', '');
			}
		}
	};
}(jQuery));
