/**
	details - logic for the details page

*/

;(function($, window, document, undefined){
	window.app.comps.details = {
		config:{
		},
		init:function(){
			// Contains the initialization code
			// ...
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
		}
	};
}(jQuery, window, document));
