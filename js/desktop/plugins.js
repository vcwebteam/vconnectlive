// Plugins
// @prepros-append ../vendor/new/typeahead.bundle.min.js
// @prepros-append ../vendor/waypoints.min.js
// @prepros-append ../vendor/waypoints-sticky.min.js
// @prepros-append ../vendor/jquery.unveil.min.js
// @prepros-append ../desktop/components/contact.map.js
// @prepros-append ../vendor/jquery.cookie.js
// @prepros-append ../vendor/jquery.mobile.min.js
// @prepros-append ../vendor/jquery.bxslider/jquery.bxslider.js
// @prepros-append ../vendor/jquery.nouislider/jquery.nouislider.all.js
// @prepros-append ../vendor/doT.js
// @prepros-append ../desktop/pages/app-product-listing.js


// Deactivated plugins
// prepros-append ../desktop/components/textchanger.js
// prepros-append ../vendor/dropzone.min.js
// prepros-append ../vendor/pikaday.js
// prepros-append ../vendor/jquery.swipebox.min.js
// prepros-append ../vendor/jquery.zclip.js


// Quick statement to disable logging to the console in production
// console.log = function(){};
