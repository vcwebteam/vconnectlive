﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vconnect.Mapping.Listing;
using Vconnect.Mapping.CommonDb;
using Vconnect.Mapping.ListingWEB;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;



namespace Vconnect.Mapping
{
      public class SearchLocations
      {
          [Key]
          public string FullLocation { get; set; }
          public Int32 stateId { get; set; }
          public string statename { get; set; }
          public string searchlocationurl { get; set; }
          public int locationid { get; set; }
      }

      public class SearchKeywords
      {
          [Key]
          public string content { get; set; }
          public Int32 contenttype { get; set; }
          public string url { get; set; }
          public int isbranch { get; set; }


      }
      
}