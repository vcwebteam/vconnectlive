﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace Vconnect.Mapping.CommonDb
{
    public class CommonDb
    {
        //public List<DisplayFlashNumber> DisaplayFlashNumber { get; set; }
        public List<SearchListCYB> SearchListCYB { get; set; }
        public List<SearchListCYBWeb> SearchListCYBWeb { get; set; }
        public List<SearchListDetails> SearchListDetails { get; set; }
        public List<SearchBizDetailsClaim> SearchBizDetailsClaim { get; set; }
        public List<SMSGateway> SMSGateway { get; set; }
        public List<SearchKeywordsBname> SearchKeywordsBname { get; set; }

    }
    public class Category
    {
    }
  
    public class CategoryItemlist
    {
        [Key]
        public int contentid { set; get; }
        public string categoryname { set; get; }

    }
    //public class DisplayFlashNumber
    //{
    //    [Key]
    //    public virtual int? categoryid { get; set; }
    //    public virtual string category { get; set; }
    //    public virtual string location { get; set; }
    //    public virtual int? flashid { get; set; }
    //    public virtual string flashnumber { get; set; }
    //}
    public class SearchListCYB
    {

        [Key]
        public virtual int? businessid { get; set; }
        public virtual string businessname { get; set; }
        public virtual string address1 { get; set; }
        public virtual string phone { get; set; }
        public virtual string topcategory { get; set; }
        public virtual int isclaim { get; set; }
        public virtual string topproducts { get; set; }
        //public virtual string contactname { get; set; }
        //public virtual string email { get; set; }
        // public int? rank { get; set; }
        //  public virtual int? type { get; set; }


    }

    public class SearchListCYBWeb
    {

        [Key]
        public virtual Int32? businessid { get; set; }
        public virtual string email { get; set; }
        public virtual string contactname { get; set; }
        public virtual string UserPhone { get; set; }
        public virtual string businessurl { get; set; }
        public virtual string topcategory { get; set; }
        public virtual int reviewcount { get; set; }
        public virtual int avgrating { get; set; }
        public virtual string address1 { get; set; }
        public virtual string phone { get; set; }
        public virtual string alternatephone { get; set; }
        public virtual string alternatephone2 { get; set; }
        public virtual string alternatephone3 { get; set; }
        public virtual string topproducts { get; set; }
        public virtual string businessname { get; set; }
        public virtual int isclaim { get; set; }
    }
    public class SearchListDetails
    {
        [Key]
        public virtual Int64? contentid { get; set; }
        public virtual string businessname { get; set; }
        public virtual string phone { get; set; }
    }
    public class SearchBizDetailsClaim
    {
        [Key]
        public virtual Int32? contentid { get; set; }
        public virtual string businessname { get; set; }
        public virtual string phone { get; set; }
        public virtual string contactname {get;set;}
        public virtual string email { get; set; }
    }
    public class SearchKeywordsBname
    {
        [Key]

        public int contentid { get; set; }
        public string businessname { get; set; }


    }
    public class SMSGateway
    {
        [Key]
        public virtual int? contentid { get; set; }
        public virtual string gateway { get; set; }
        public virtual string url { get; set; }

    }
}