﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Vconnect.Mapping;

namespace Vconnect.Mapping.UserDBWEB
{
    
        public class ResultUserDBTable
        {
            public List<UserDBDetails> UserDBDetails { get; set; }
            public List<RecentlyViewedBusiness> RecentlyViewedBusiness { get; set; }
            public List<RecentlyPostedReviews> RecentlyPostedReviews { get; set; }
            public List<KeywordSearchLocation> KeywordSearchLocation { get; set; }
            public List<KeywordSearchedPage> KeywordSearchedPage { get; set; } 

        }
        public class WebResultUserDashboard

        {
            public BusinessVisitCount businessVisitCount {get ;set;}
            public KeywordSearched keywordSearched { get; set; }
            public BannerClicked bannerClicked { get; set; }
            public SMSToMobile smsToMobile { get; set; }
            public EmailSent emailSent { get; set; }
            public List<KeywordSearchedPage> keywordSearchedPage { get; set; }
        }
        public class ResultWatchList
        {
            public List<watchlist> watchlist { get; set; }
        }
        public class WebResultRequirementPosted
        {
            public List<TotalRec> totalresult { get; set; }
            public List <RequirementPosted> requirementPosted { get; set; }
           
        }
        public class WebResultReviewPosted
        {
           
            public List<ReviewPosted> reviewPosted { get; set; }

        }
        public class TotalRec
        {
            [Key]
            public Int32 TotalRecords{ get; set; }

        }
        public class UserDBDetails
        {

            [Key]
            public Int32 contentid { get; set; }
            public virtual string title { get; set; }
            public virtual string contactname { get; set; }
            public virtual string houseno { get; set; }
            public virtual string address { get; set; }
            public virtual int? areaid { get; set; }
            public virtual int? cityid { get; set; }
            public virtual int? stateid { get; set; }
            public virtual int? zipcode { get; set; }
            public virtual int? countryid { get; set; }
            public virtual string phone { get; set; }
            public virtual string alternatephone { get; set; }
            public virtual string  email { get; set; }
            public virtual string alternateemail { get; set; }
            public virtual string designation { get; set; }
            public virtual bool isprimarycontact { get; set; }
            public virtual string sex { get; set; }
            public virtual string photo { get; set; }
            public virtual string dateofbirth { get; set; }
            public virtual bool isemailveified { get; set; }
            public virtual bool ismobileverified { get; set; }
            public virtual bool isverified {get; set; }
            public virtual string state { get; set; }
            public virtual string city { get; set; }
        }
        public class RecentlyViewedBusiness
        {
            [Key]
            public virtual Int64? businessid { get; set; }
            public virtual string businessname { get; set; }
            public virtual string businessurl { get; set; }
            public virtual DateTime createddate { get; set; }
            public virtual int? avgrating { get; set; }
        }
        public class RecentlyPostedReviews
        {
            [Key]
            public virtual Int64? contentid { get; set; }
            public virtual string reviewdetail { get; set; }
            public virtual string businessname { get; set; }
            public virtual DateTime createddate { get; set; }
            
        }
        public class KeywordSearchLocation
        {
            [Key]
            public virtual Int64? contentid { get; set; }
            public virtual string searchkeyword{ get; set; }
            public virtual string searchloaction { get; set; }
            public virtual DateTime createddate { get; set; }
        }
        public class BusinessVisitCount
        {
            [Key]
            public int TotalCount { get; set; }
            public int ThisMonthCount { get; set; }
        }
        public class KeywordSearched  
        {
            [Key]
            public int TotalCount { get; set; }
            public int ThisMonthCount { get; set; }
        }
        public class BannerClicked
        {
            [Key]
            public int TotalCount { get; set; }
            public int ThisMonthCount { get; set; }
        }
        public class SMSToMobile 
        {
            [Key]
            public int TotalCount { get; set; }
            public int ThisMonthCount { get; set; }
        }
        public class EmailSent
        {
            [Key]
            public int TotalCount { get; set; }
            public int ThisMonthCount { get; set; }
        }
        public class KeywordSearchedPage
        {
            [Key]
            public virtual Int64? contentid { get; set; }
            public virtual string searchkeyword { get; set; }
            public virtual string searchloaction { get; set; }
            public virtual string createddate { get; set; }
            public virtual string Status { get; set; }
        }
        public class watchlist
        {
            [Key]
            public virtual Int64? businessid { get; set; }
            public virtual string businessname { get; set; }
            public virtual Int64 userid { get; set; }
            public virtual Int32 createdby { get; set; }
            public virtual string createddate { get; set; }
            public virtual string businessurl { get; set; }
        }

        //public class KeywordNotFound
        //{
        //    [Key]
        //    public virtual int? contentid { get; set; }
        //    public string searchkeyword { get; set; }
        //    public string searchlocation { get; set; }
        //    public string createddate { get; set; }
        //    public string Status { get; set; }
        //}
        public class RequirementPosted
        {
           
            public virtual string requirement { get; set; }
            public virtual string createddate { get; set; }
        }

        public class ReviewPosted
        {

            public virtual string reviewdetail { get; set; }
            public virtual string createddate { get; set; }
            public virtual int ReviewStatus { get; set; }
        }

       
   
}