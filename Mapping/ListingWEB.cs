﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;



namespace Vconnect.Mapping.ListingWEB
{
      public class ListSearchResultWEB
       {
        public List<SearchListBusinessWEB> SearchListBusiness { get; set; }
        public List<BusinessCountWEB> BusinessCount { get; set; }
        public List<SearchListPremiumBusinessWEB> SearchListPremiumBusiness { get; set; }
        //public List<SearchListSpecial> searchListSpecial { get; set; }
        public List<BannerWeb> bannerWEB { get; set; }
        // public List<FootBanner> FootBanner { get; set; }
       }
     public class SearchListBusinessWEB
    {
        [Key]
        public virtual Int64 ResultNum { get; set; }
      public virtual Int32 businessid { get; set; }
      public virtual int? rank { get; set; }
      public virtual string companylogo { get; set; }
      public virtual string businessname { get; set; }
      public virtual string location { get; set; }
      public virtual string url { get; set; }
      public virtual int membership { get; set; }
      public virtual double rate { get; set; }
      public virtual int reviewcount { get; set; }
      public virtual string category { get; set; }
      // public virtual string address { get; set; }
      // public virtual int userreviewcount { get; set; }
      public virtual int ratingcount { get; set; }
      // public virtual string ratingcontent { get; set; }
      public virtual Nullable<int> isflashsubscribe { get; set; }
      public virtual string longitude { get; set; }
      public virtual string latitude { get; set; }
      public virtual int fav { get; set; }
      public virtual int lke { get; set; }

    }
    public class BusinessCountWEB
    {
        [Key]
        public virtual int? totalcount { get; set; }
        public virtual int totalbiz { get; set; }
    }
    public class LeadWord
    {
        public virtual int value { get; set; }
    }
    public class LocationFilter
    {
        [Key]
        public virtual string location { get; set; }
        public virtual Int32? rank { get; set; }
        public virtual Int32? locationid { get; set; }
       // public virtual Int32 url {get;set;}
    }
    public class KeywordFilter
    {
        [Key]
        public virtual string keyword { get; set; }
        public virtual Int32? value { get; set; }
        public virtual  string url { get; set; }
    }
    public class RelatedSearch
    {
        [Key]
        public virtual string tag { get; set; }
        public virtual Int32? rank { get; set; }
        public virtual string url { get; set; }
    }
    public class SearchListPremiumBusinessWEB
    {
        [Key]
        public virtual Int32 businessid { get; set; }
        public virtual string companylogo { get; set; }
        public virtual string businessname { get; set; }
        public virtual string location { get; set; }
        public virtual string address { get; set; }
        public virtual string url { get; set; }
        public virtual int? membership { get; set; }
        public virtual double rate { get; set; }
        public virtual int? reviewcount { get; set; }
        public virtual string category { get; set; }
        public virtual string longitude { get; set; }
        public virtual string latitude { get; set; }
       
        public virtual int? fav { get; set; }
        public virtual int? lke { get; set; }
        public virtual int? type { get; set; }
        public virtual string title { get; set; }
    }
    public class BannerWeb
    {
        [Key]
        public virtual string bannerid { get; set; }
        public virtual int? membership { get; set; }
        public virtual string url { get; set; }
        public virtual string banner { get; set; }
    }
    public class getFeaturedBanner
    {
        [Key]
        public virtual int? businessid { get; set; }
        public virtual string businessname { get; set; }
        //public virtual string businessdescription { get; set; }
        public virtual string businessurl { get; set; }
        public virtual int bannerid { get; set; }
        public virtual int isscript { get; set; }
        public virtual string scripttext { get; set; }
        //public string _businessdescription
        //{
        //    get { return !string.IsNullOrWhiteSpace(businessdescription) && businessdescription.Length > 90 ? string.Format("{0}...<a href ='{1}'>Read More</a>", businessdescription.ToString().Substring(0, 90), businessurl) : businessdescription; }

        //}

        public virtual string businessbanner { get; set; }
        public virtual int clienttype { get; set; }
        public virtual int avgrating { get; set; }
        public virtual int reviewcount { get; set; }
        public virtual string businessaddress { get; set; }
        public virtual int Error { get; set; }

    }

    public class Bodashboardbannerlisting
    {
        public virtual Int64 bannerid { get; set; }
        public virtual string url { get; set; }
        public virtual string advimg { get; set; }
    }

}