﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Vconnect.Mapping;

namespace Vconnect.Mapping.Listing
{
    //public class ListResultSetTable
    //{
    //    public List<SearchListBusiness> SearchListBusiness { get; set; }
    //    public List<BusinessCount> BusinessCount { get; set; }
    //    public List<SearchListPremiumBusiness> SearchListPremiumBusiness { get; set; }
    //    //public List<BannerResult> BannerResult { get; set; }
        
    //   // public List<FootBanner> FootBanner { get; set; }
    //}

    //public class SearchListBusiness
    //{

    //    [Key]
    //    public int? rank { get; set; }
    //    public virtual int? type { get; set; }
    //    public virtual int? businessid { get; set; }
    //    public virtual string businessname { get; set; }
    //    public virtual string address { get; set; }
    //    public virtual string businessphone { get; set; }
    //    public virtual int? resultnum { get; set; }
    //    public virtual string category { get; set; }
    //    public virtual string service { get; set; }
    //    public virtual int rating { get; set; }
    //    public virtual int? membership { get; set; }
    //    public virtual int? photocount { get; set; }
    //    public virtual int? businesscount { get; set; }
    //    public virtual string state { get; set; }
    //    public virtual string businessurl { get; set; }
    //    public virtual int? specialsid { get; set; }
    //    public virtual string title { get; set; }
    //    public virtual string description { get; set; }

    //}
    //public class BusinessCount
    //{
    //    [Key]
    //    public virtual int? totalcount { get; set; }

    //}
    //public class SearchListPremiumBusiness
    //{
    //    [Key]
    //    public virtual Int32 Contentid { get; set; }
    //    public virtual string businessname { get; set; }
    //    public virtual string location { get; set; }
    //    public virtual string address { get; set; }
    //    public virtual string url { get; set; }
    //    public virtual string companylogo { get; set; }
    //    public virtual int? membership { get; set; }
    //    public virtual int? ratingcount { get; set; }
    //    public virtual string ratingcontent { get; set; }
    //    public virtual int? avgrating { get; set; }
    //    public virtual int? reviewcount { get; set; }
    //    public virtual string category { get; set; }


    //}
    //public class BannerWap
    //{
    //    [Key]
    //    public virtual Int32? bannerid { get; set; }
    //    public virtual int? membership { get; set; }
    //    public virtual string bannerimage { get; set; }
    //    public virtual string bannerurl { get; set; }
    //}
    public class FootBanner
    {
        [Key]
        public virtual Int64? bannerid { get; set; }
        public virtual int? membership { get; set; }
        public virtual string bannerimage { get; set; }
        public virtual string bannerurl { get; set; }
        public virtual Int32? bannertypeid { get; set; }
    }
    //public class BusinessBanner
    //{
    //    [Key]
    //    public virtual Int32? contentid {get;set;}
    //    public virtual Int32? bannertypeid {get;set;}
    //    public virtual string bannertypename{get;set;}
    //    public virtual Int32? bannerpageid {get;set;}
    //    public virtual string bannerpagename{get;set;}
    //    public virtual string banner{get;set;}
    //    public virtual Int32? businessid {get;set;}
    //    public virtual string businessname {get;set;}
    //    public virtual string url{get;set;}
    //    public virtual bool? isreview{get;set;}
    //    public virtual bool? isdeleted{get;set;}
    //    public virtual DateTime createddate {get;set;}
    //    public virtual int? createdby{get;set;}
    //    public virtual DateTime modifieddate {get;set;}
    //    public virtual int? modifiedby{get;set;}
    //    public virtual int? status{get;set;}
    //    public virtual DateTime subscribedate {get;set;}
    //    public virtual int? membership{get;set;}
    //    public virtual DateTime expirydate {get;set;}
    //    public virtual int? maximpression{get;set;}
    //    public virtual int? maxclicks{get;set;}
    //    public virtual int? totclicks{get;set;}
    //    public virtual int? avgimpression{get;set;}
    //    public virtual int? oldid{get;set;}
    //    public virtual string bannerimage{get;set;}
    //    public virtual string bannerurl{get;set;}
    //}







    public class getSmeEvent
    {
        [Key]
        public virtual Int64? contentid { get; set; }
        public virtual DateTime? eventdate { get; set; }
        public virtual string eventaddress { get; set; }

    }

	    
	


}