﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vconnect.Mapping.ListingWEB;

namespace Vconnect.Models
{
    public class CategoryListWebModel:SearchBarWebModel
    {
        //public SearchBarWebModel searchBarWebModel { get; set; }
        public List<CategoryList> CategoryList { get; set; }
       
        
        public string url { get; set; }
        public PYRModel pyrModel { get; set; }
        public BannerWeb bannerWeb { get; set; }

    }
}