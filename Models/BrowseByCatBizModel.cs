﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
namespace Vconnect.Models
{
    public class BrowseByCatBizModel:SearchBar
    {

        public int pageNum
        { get; set; }

        public int rowsPerPage
        { get; set; }
        public int type
        { get; set; }
        public int totalpage { get; set; }
        public int totalRes { get; set; }
        public SearchBar SearchBar { get; set; }
        public BrowseByCatBizTable browseByCatBizTable { get; set; }
        public void getResult()
        {

            using (VconnectDBContext29 waplistresult = new VconnectDBContext29())
            {
                browseByCatBizTable = BrowseByCategoryBusiness.BrowseByCatBizListfill(SearchText, type, SearchLocation, pageNum, rowsPerPage);
                totalpage = Convert.ToInt16(browseByCatBizTable.totalresult[0].TotalRecords / Convert.ToInt32(rowsPerPage));
                totalRes = Convert.ToInt32(browseByCatBizTable.totalresult[0].TotalRecords);
            }
        }
    }

    public class BrowseByCatBizfield
    {
        [Key]
        public  Int64 Resultnum { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public Int32 contentid { get; set; }
        public string address { get; set; }
    }
    public class TotalCount
    {
        [Key]
        public Int32 TotalRecords { get; set; }

    }
    public class BrowseByCatBizTable
    {
        public List<BrowseByCatBizfield> browseByCatBizList1 { get; set; }
        public List<BrowseByCatBizfield> browseByCatBizList2 { get; set; }
        public List<TotalCount> totalresult = new List<TotalCount>();
    }

    public class BrowseByCategoryBusiness : VconnectDBContext29
    {

        public static BrowseByCatBizTable BrowseByCatBizListfill(string keyword, int type, string location, int pageno, int rowsPerPage)
        {
            BrowseByCatBizTable BrowseByCatBizList = new BrowseByCatBizTable();
            string city = string.Empty;
            if (location.IndexOf("-") != -1)
            {
                string[] spt = location.Split('-');
                //location = spt[spt.GetUpperBound(0)].Replace("_", " ");
                location = spt[0].Replace("_", " ");
                city = spt[spt.GetUpperBound(0)].Replace("_", " ");
            }

            using (var db = new VconnectDBContext29())
            {

                var cmd = db.Database.Connection.CreateCommand();
                //cmd.CommandText = "[dbo].[prc_get_BrowseByAlphabets_opt]";
                cmd.CommandText = "[dbo].[prc_get_BrowseByAlphabets_new]";                 
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandTimeout = 600;
                cmd.Parameters.Add(new SqlParameter("@Alphabet", keyword));
                cmd.Parameters.Add(new SqlParameter("@type", type));
                cmd.Parameters.Add(new SqlParameter("@location", location.Replace("_"," ").Replace("-"," ")));
                if (!string.IsNullOrEmpty(city))
                    cmd.Parameters.Add(new SqlParameter("@city", city.Replace("_", " ").Replace("-", " ")));
                cmd.Parameters.Add(new SqlParameter("@pageNum", pageno));
                cmd.Parameters.Add(new SqlParameter("@rowsPerPage", rowsPerPage));

                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                        BrowseByCatBizList.browseByCatBizList1 = ((IObjectContextAdapter)db).ObjectContext.Translate<BrowseByCatBizfield>(reader).ToList();
                     
                        // Move to second result set and read Posts
                        reader.NextResult();

                        BrowseByCatBizList.browseByCatBizList2 = ((IObjectContextAdapter)db).ObjectContext.Translate<BrowseByCatBizfield>(reader).ToList();
                        reader.NextResult();
                        BrowseByCatBizList.totalresult = ((IObjectContextAdapter)db).ObjectContext.Translate<TotalCount>(reader).ToList(); ;
                        
                    return BrowseByCatBizList;
                   

                }
                finally
                {
                    cmd.Dispose();
                }
            }
        }
    }    
}