﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace Vconnect.Models
{
    public static class OptimizedImage
    {



        public static void ResizeImage(int MaxWidth, int MaxHeight, string FileName, string NewFileName)
        {
            // load up the image, figure out a "best fit" resize, and then save that new image
            Bitmap OriginalBmp = (System.Drawing.Bitmap)System.Drawing.Image.FromFile(FileName).Clone();
            Size ResizedDimensions = GetDimensions(MaxWidth, MaxHeight, ref OriginalBmp);
            Bitmap NewBmp = new Bitmap(OriginalBmp, ResizedDimensions);
            System.Drawing.Imaging.ImageFormat imageFormat = OriginalBmp.RawFormat;
            NewBmp.Save(NewFileName, imageFormat);


        }

        /// <summary>
        /// this function aims to give you a best fit for a resize.  It assumes width is more important 
        /// then height.  If an image is already smaller then max dimensions it will not resize it.
        /// </summary>
        /// <param name="MaxWidth">max width of the new image</param>
        /// <param name="MaxHeight">max height of the new image</param>
        /// <param name="Bmp">BMP of the current image, passing by ref so fast</param>
        /// <returns></returns>
        public static Size GetDimensions(int MaxWidth, int MaxHeight, ref Bitmap Bmp)
        {
            int Width;
            int Height;
            float Multiplier;

            Height = Bmp.Height;
            Width = Bmp.Width;

            // this means you want to shrink an image that is already shrunken!
            if (Height <= MaxHeight && Width <= MaxWidth)
                return new Size(Width, Height);

            // check to see if we can shrink it width first
            Multiplier = (float)((float)MaxWidth / (float)Width);
            if ((Height * Multiplier) <= MaxHeight)
            {
                Height = (int)(Height * Multiplier);
                return new Size(MaxWidth, Height);
            }

            // if we can't get our max width, then use the max height
            Multiplier = (float)MaxHeight / (float)Height;
            Width = (int)(Width * Multiplier);
            return new Size(Width, MaxHeight);
        }
    }
}