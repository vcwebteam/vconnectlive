﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Vconnect.Models
{
    public static class CopyImage
    {


        public static void Sync(string sourcePath, string destinationPath)
        {
            bool dirExisted = DirExists(destinationPath);

            //get the source files

            string[] srcFiles = Directory.GetFiles(sourcePath);

            Console.WriteLine(srcFiles.Length + " Files Selected ");

            foreach (string sourceFile in srcFiles)
            {
                FileInfo sourceInfo = new FileInfo(sourceFile);

                if (sourceInfo.CreationTime.Date >= DateTime.Now.AddDays(-1).Date)
                {
                    string destFile = Path.Combine(destinationPath, sourceInfo.Name);

                    if (!dirExisted && File.Exists(destFile))
                    {
                        FileInfo destInfo = new FileInfo(destFile);
                        DateTime lastdate = destInfo.CreationTime;

                        if (sourceInfo.LastWriteTime > destInfo.LastWriteTime)
                        {
                            //file is newer, so copy it

                            try
                            {
                                File.Copy(sourceFile, Path.Combine(destinationPath, sourceInfo.Name), true);
                            }
                            catch (Exception ex)
                            {
                                ex = null;
                            }

                            //Console.WriteLine(sourceFile + " copied (newer version)");

                        }

                    }

                    else
                    {
                        try
                        {

                            File.Copy(sourceFile, Path.Combine(destinationPath, sourceInfo.Name));
                        }
                        catch (Exception ex)
                        {
                            ex = null;
                            //AppLogs.LogError(ex);
                        }

                        //Console.WriteLine(sourceFile + " copied");

                    }
                }

            }//foreach ends



            //  DeleteOldDestinationFiles(srcFiles, destinationPath);

            //now process the directories if exist

            string[] dirs = Directory.GetDirectories(sourcePath);

            foreach (string dir in dirs)
            {
                DirectoryInfo dirInfo = new DirectoryInfo(dir);
                //recursive do the directories
                Sync(dir, Path.Combine(destinationPath, dirInfo.Name));

            }

        }//




        private static bool DirExists(string path)
        {

            //create destination directory if not exist

            if (!Directory.Exists(path))
            {

                Directory.CreateDirectory(path);

                return true;

            }

            else
            {

                return false;

            }

        }


        private static void DeleteOldDestinationFiles(string[] sourceFiles, string destinationPath)
        {

            //get the destination files

            string[] dstFiles = Directory.GetFiles(destinationPath);



            foreach (string dstFile in dstFiles)
            {

                FileInfo f = new FileInfo(dstFile);



                string[] found = Array.FindAll(sourceFiles, str => GetName(str).Equals(f.Name));

                if (found.Length == 0)
                {

                    //delete file if not found in destination

                    File.Delete(dstFile);



                }

            }

        }



        private static string GetName(string path)
        {

            FileInfo i = new FileInfo(path);

            return i.Name;

        }



    }//class ends
}