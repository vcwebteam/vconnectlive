﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Vconnect.Models
{
    public class UserReg
    {
        public class UserRegWeb : SearchBarWebModel
        {
            public bool ConditionaValue { get; set; }
            public string PhoneMsg { get; set; }
            public string CmpPhone { get; set; }
            public string validphone { get; set; }
            public string validaltphone { get; set; }

            //[Required(ErrorMessage = "cannot be blank.")]
            [Display(Name = "Contact Name:")]
            public string txtName { get; set; }
            public string surname { get; set; }
            //[Required(ErrorMessage = "cannot be blank.")]
            [Display(Name = "Contact No :")]
            //[RegularExpression(@"^[0-9]{0,11}$", ErrorMessage = "Phone Number should contain only numbers or valied")]
            public string txtMobile { get; set; }

            //[Required(ErrorMessage = "cannot be blank.")]
            [Display(Name = "Password :")]
            public string txtPassword { get; set; }
            //[Required(ErrorMessage = "cannot be blank.")]
            [Display(Name = "Confirm Password :")]
            //[System.ComponentModel.DataAnnotations.Compare("txtPassword", ErrorMessage = "Password does not match the confirm password.")]
            public string txtConfirmPassword { get; set; }
            //[Required(ErrorMessage = "cannot be blank.")]
            [DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
            [Display(Name = "Email Address (login id) :")]
            public string txtEmail { get; set; }
            public string zaq { get; set; }
            public string CAPTCHA { get; set; }
            public string checkbox { get; set; }
            public string SuccessMessage { get; set; }
            public string MessageError { get; set; }

            public IEnumerable<SelectListItem> ListCityNames { get; set; }
            public Int64 CityID { get; set; }
            public int SelectedCityId { get; set; }
            public SelectList CityOptions { get; set; }


            public string dob { get; set; }
            public string gender { get; set; }
        }
        public UserRegWeb userregweb { get; set; }
        public class UserCredentialsNew
        {
            [Key]
            public string ContactName { get; set; }
            public int Error { get; set; }
            public int newuserconentid { get; set; }
        }
        public class UserDetailEmail
        {
            [Key]
            public string verificationcode { get; set; }
            public string email { get; set; }
            public string password { get; set; }
            public string contactname { get; set; }
            public int customerid { get; set; }
        }
    }
}