﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vconnect.Mapping.Listing;
using Vconnect.Mapping.CommonDb;
using Vconnect.Mapping.ListingWEB;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Entity;

namespace Vconnect.Models
{
    public class BizRegModels : SearchBarWebModel
    {

        public string txtCompanyName { get; set; }
        public string txtContactPersonPhone { get; set; }
        public string txtAltPhone { get; set; }
        public string txtcontactname { get; set; }
        public string txtemail { get; set; }
        public string txtAddress { get; set; }
        public string txtProduct { get; set; }
        public Int32 lga { get; set; }
        public string txtwebsite { get; set; }
        public bool ConditionaValue { get; set; }
        public string SuccessMessage { get; set; }

        public string CAPTCHA { get; set; }

        //public IEnumerable<SelectListItem> ListStatesNames { get; set; }
        //public Int64 StateID { get; set; }
        //public int SelectedStateId { get; set; }
        //public SelectList StateOptions { get; set; }

        public IEnumerable<SelectListItem> ListCityNames { get; set; }
        public Int64 CityID { get; set; }
        public int SelectedCityId { get; set; }
        public SelectList CityOptions { get; set; }
    }
    public class ibtcModels
    {
        public string SuccessMessage { get; set; }
        public string txtname { get; set; }
        public string txtdesignation { get; set; }
        public string txtphone { get; set; }
        public string txtbizname { get; set; }
        public string txtbizcategory { get; set; }
        public string txtemail { get; set; }
    }
    public class niitModals
    {
        public string SuccessMessage { get; set; }
        public string txtname { get; set; }
        public string txtphone { get; set; }
        public string txtemail { get; set; }
        public string txtcenter { get; set; }

        public Int64 CityID { get; set; }
        public int SelectedCityId { get; set; }
        public SelectList CityOptions { get; set; }

        public Int64 CenterID { get; set; }
        public int SelectedCenterId { get; set; }
        public SelectList CenterOptions { get; set; }

        public Int64 StatusID { get; set; }
        public int SelectedStatusId { get; set; }
        public SelectList StatusOptions { get; set; }

        public Int64 CourseID { get; set; }
        public int SelectedCourseId { get; set; }
        public SelectList CourseOptions { get; set; }

        public Int64 TimeSlotID { get; set; }
        public int SelectedTimeSlotId { get; set; }
        public SelectList TimeSlotOptions { get; set; }
    }
    public class switchngModels
    {
        public string SuccessMessage { get; set; }
        public string txtName { get; set; }
        public string txtPhone { get; set; }
        public string txtEmail { get; set; }
        public string txtTypeField { get; set; }
        public string txtInquiry { get; set; }

        public Int64 BusinessTypeID { get; set; }
        public int SelectedBusinessTypeId { get; set; }
        public SelectList BusinessTypeOptions { get; set; }

        public Int64 PaymentID { get; set; }
        public int SelectedPaymentId { get; set; }
        public SelectList PaymentTypeOptions { get; set; }

        public bool InventoryStockValue { get; set; }
    }
}
