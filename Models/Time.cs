﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Vconnect.Models
{
    public class Time
    {
        public string timeID { get; set; }
        public string time { get; set; }
        public SelectList hourselectOptions { get; set; }

    }
}