﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vconnect.Mapping.Listing;
using Vconnect.Mapping.CommonDb;
using Vconnect.Mapping.ListingWEB;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using System.Web.DynamicData;
using System.Text;
using System.Data.SqlClient;
using System.Data.Entity.Infrastructure;
using Vconnect.Mapping;
using Vconnect.Common;
using System.Data;



namespace Vconnect.Models
{
    public class HomeModel : SearchBarWebModel
    {
        #region Properties
        public string bussinessID { get; set; }
        public int? contentIds { get; set; }
        public string _SelectedLocation { get; set; }
        public string SearchTextHeader { get; set; }
        public string SearchLocationHeader { get; set; }
        public List<getFeaturedBanner> lstFeaturedBusinesses { get; set; }
        public bool isclaim { get; set; }
        #endregion

        #region Methods
        public List<SearchKeywords> AutoCompleteSearchText(string term)
        {
            List<SearchKeywords> keywordCollection = new List<SearchKeywords>();
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "prc_get_autocompletesearchText";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@searchtext", term));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    keywordCollection = ((IObjectContextAdapter)db).ObjectContext.Translate<SearchKeywords>(reader).ToList();
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                    //ex.Message.ToString();
                }
                
                finally
                {
                    db.Database.Connection.Close();
                }
                return keywordCollection;
            }
        }

        public List<SearchLocations> AutoCompleteSearchLocation(string term)
        {
            List<SearchLocations> locationCollection = new List<SearchLocations>();
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "prc_get_autocompletesearchLocation";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@searchLoc", term));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    locationCollection = ((IObjectContextAdapter)db).ObjectContext.Translate<SearchLocations>(reader).ToList();
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                    //ex.Message.ToString();
                }
                finally
                {
                    db.Database.Connection.Close();
                }
                return locationCollection;
            }
        }


        public int submitdealsdata(string email,string location, string gender)
        {
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_newsletter_popup]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@email", email));
                cmd.Parameters.Add(new SqlParameter("@state", location));
                cmd.Parameters.Add(new SqlParameter("@gender", gender));
                cmd.Parameters.Add(new SqlParameter("@source", "vconnecthomepage"));
                SqlParameter Err = new SqlParameter("@Err", SqlDbType.Int);
                Err.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(Err);
                int result = 0;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteNonQuery();
                    result = (int)cmd.Parameters["@Err"].Value;
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
                finally
                {
                    db.Database.Connection.Close();
                }
                return result;
            }
        }

        public int AddSubscribeEmail(string email)
        {
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_NEwsletterSubscriber]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@subemail", email));
                cmd.Parameters.Add(new SqlParameter("@source", "WEB"));
                SqlParameter Err = new SqlParameter("@Err", SqlDbType.Int);
                Err.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(Err);
                int result = 0;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteNonQuery();
                    result = (int)cmd.Parameters["@Err"].Value;
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
                finally
                {
                    db.Database.Connection.Close();
                }
                return result;
            }
        }

        public List<getFeaturedBanner> FeaturedBannerResultFunc(string loc)
        {
            List<getFeaturedBanner> _FeaturedBannerResult = new List<getFeaturedBanner>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                //cmd.CommandText = "[dbo].[prc_get_featuredbusiness_beta]";
                //cmd.CommandText = "[dbo].[prc_get_featuredbusiness_live_tanay]";
                cmd.CommandText = "[dbo].[prc_get_featuredbusiness_live]";                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@location", loc));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    _FeaturedBannerResult = ((IObjectContextAdapter)db).ObjectContext.Translate<getFeaturedBanner>(reader).ToList();
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                    //ex.Message.ToString();
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            return _FeaturedBannerResult;
        }

        #endregion
    }

    public class StatesDropDown
    {
        public int StateId { get; set; }
        public string StateName { get; set; }
    }

    public class JsonModel
    {
        public string HTMLString { get; set; }
        public bool NoMoreData { get; set; }
    }

    public class getsuppliers
    {
        [Required(ErrorMessage = "please enter the product..")]
        [Display(Name = "What are you looking for?*")]
        public string txtProduct { get; set; }

        [Required(ErrorMessage = "please enter the email address")]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Please enter the valid email address..")]
        [Display(Name = "Email Id")]
        public string txtEmail { get; set; }

        [Required(ErrorMessage = "please enter the name..")]
        [Display(Name = "Your Name*")]
        public string txtName { get; set; }

        [Required(ErrorMessage = "Please Enter  Phone No.")]
        [Display(Name = "Mobile number*")]
        //[StringLength(10, ErrorMessage = "Phone Number length Should be less than 10")]
        //[RegularExpression(@"^[0-9]{0,11}$", ErrorMessage = "PhoneNumber should contain only numbers or valied")]
        [RegularExpression(@"\d{11}", ErrorMessage = "PhoneNumber should contain only numbers or valied")]
        public string txtPhonenumber { get; set; }

        [Required(ErrorMessage = "please enter the more detail")]
        [Display(Name = "Enter more details*")]
        public string txtDetails { get; set; }

        public string catList { get; set; }

        public string location { get; set; }
    }

    public class getsupplierwap
    {
        public string wapgetsupplier(getsuppliers gts, Int32 stateid)
        {
            string result = string.Empty;
            using (WaptDBContext29 wapdbcontext29 = new WaptDBContext29())
            {
                result = wapdbcontext29.prc_wap_add_lead(gts, stateid);
            }
            return result;
        }
    }

    #region Unwanted code
    //public class CategoryList
    //{
    //    [Key]
    //    public int? categoryid { set; get; }
    //    public string categoryname { set; get; }
    //}
    ////public class WapHeaderBanner
    //{
    //    public BannerResult bannerResult
    //    {
    //        get;
    //        set;
    //    }
    //}

    //public class WapCategoryList
    //{
    //    public List<CategoryList> CategoryList { get; set; }
    //    public getPYRModel getPYRModel { get; set; }
    //   // public WAPListModel WAPListModel { get; set; }
    //    public BannerResult bannerResult
    //    { get; set; }
    //    public ListResultSetTable listResSetTab
    //    { get; set; }
    //    public void fillbannerResult()
    //    {

    //        using (WaptDBContext29 waplistresult = new WaptDBContext29())
    //        {
    //            StringBuilder sb = new StringBuilder();

    //            //foreach (var slb in listResSetTab.SearchListBusiness)
    //            //{
    //            //    sb.Append(slb.businessid.ToString());
    //            //    sb.Append(',');
    //            //}

    //            bannerResult = waplistresult.HeaderBanner("6068,294,160600,223217,125397,188198", 1);


    //        }
    //    }
    //}
    //public class getPYRModel : SearchBar
    //{
    //    public PYRModel pyrModel { get; set; }


    //}

    //public class LeadModel
    //{
    //    public WAPListModel WAPListModel { get; set; }
    //    //  public getPYRModel GetPYRModel { get; set; }


    //}
    //public class SearchBarAdv : SearchBar
    //{
    //    public int? pageNum
    //    { get; set; }

    //    public int rowsPerPage
    //    { get; set; }

    //    public int sort
    //    { get; set; }

    //    public int type
    //    { get; set; }
    //}
    //public class WAPListModel : SearchBarAdv
    //{

    //    public int totalPages
    //    { get; set; }


    //    public ListResultSetTable listResSetTab
    //    { get; set; }
    //    public BannerResult bannerResult
    //    { get; set; }


    //    public void filllistResSetTab()
    //    {
    //        //WebMasterModel WebMasterModel = new WebMasterModel();
    //        using (WaptDBContext29 waplistresult = new WaptDBContext29())
    //        {

    //            listResSetTab = waplistresult.prc_get_wapsearch(SearchText, SearchLocation, pageNum, rowsPerPage, null, null);
    //            totalPages = listResSetTab.BusinessCount[0].totalcount.Value / rowsPerPage;
    //        }
    //    }

    //    public void fillbannerResult()
    //    {

    //        using (WaptDBContext29 waplistresult = new WaptDBContext29())
    //        {
    //            StringBuilder sb = new StringBuilder();

    //            foreach (var slb in listResSetTab.SearchListBusiness)
    //            {
    //                sb.Append(slb.businessid.ToString());
    //                sb.Append(',');
    //            }

    //            bannerResult = waplistresult.HeaderBanner(sb.ToString(), 1);


    //        }
    //    }
    //    public class state
    //{
    //    public int contentid { get; set; }
    //    public string statename { get; set; }
    //}
    //public class city
    //{
    //    public int contentid { get; set; }
    //    public int stateid { get; set; }
    //    public string cityname { get; set; }
    //}
    //}
    #endregion

    #region Unwanted code
    //public string ddlStateNames { get; set; }
    //public string ddlStateID { get; set; }
    //public int? pageNum { get; set; }
    //public int rowsPerPage { get; set; }

    //public int sort { get; set; }

    //public int type { get; set; }
    //public int totalPages { get; set; }
    //public ListResultSetTable listResSetTab
    //{ get; set; }
    //public BannerResult bannerResult
    //{
    //    get;
    //    set;
    //}
    //public FooterBanner footerBanner
    //{ get; set; }
    //public List<DisplayFlashNumber> displayFlashNumber
    //{ get; set; }
    //public ListSearchResultWEB listSearchResultWEB
    //{ get; set; }

    //public List<BannerWEB> bannerWEB { get; set; }
    //public string Review { get; set; }        
    //public int Rate { get; set; }

    //public List<CategoryList> categoryList { get; set; }
    //public int? contentId { get; set; }
    //   public List<CategoryList> categoryListItem { get; set; }
    //public List<CategoryItemlist> itemList { get; set; }
    // public IEnumerable<SelectListItem> ListStatesNames { get; set; }

    //public int StateID { get; set; }

    //public int SelectedStateId { get; set; }
    //public SelectList StateOptions { get; set; }
    //public List<LocationFilter> LocationFilter { get; set; }
    //public List<KeywordFilter> KeywordFilter { get; set; }



    //[Required(ErrorMessage = "please enter the username")]
    //public string UserName
    //{ get; set; }
    //public string ValidationCode
    //{ get; set; }


    //[Required]
    //[Display(Name = "can not be blank")]
    //public string Phone
    //{ get; set; }
    //[Required(ErrorMessage = "please enter the password")]
    //// [Display(Name="password*")]
    //[DataType(DataType.Password, ErrorMessage = "please enter the same password..")]
    //public string Password { get; set; }

    //[Required(ErrorMessage = "please enter the Old Password")]
    //[DataType(DataType.Password)]

    //public string OldPassword { get; set; }

    //[Required]
    //[System.ComponentModel.DataAnnotations.Compare("Password")]
    //public string confirmpassword { get; set; }
    //public class state
    //{
    //    public int contentid { get; set; }
    //    public string statename { get; set; }
    //}
    //public class city
    //{
    //    public int contentid { get; set; }
    //    public int stateid { get; set; }
    //    public string cityname { get; set; }
    //}
    //public class SearchBar
    //{
    //    public string SearchText { get; set; }
    //    public string SearchLocation { get; set; }
    //    public string SearchType { get; set; }
    //}
    #endregion
}
