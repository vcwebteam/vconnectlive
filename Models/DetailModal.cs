﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Vconnect.Mapping.Listing;
using Vconnect.Mapping.CommonDb;
using Vconnect.Mapping.ListingWEB;
using System.Data.SqlClient;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Net;
using System.Configuration;
using System.Net.Mail;
using System.Threading.Tasks;
using MailChimp.Types;
using Vconnect.Common;

namespace Vconnect.Models
{
    public class SimilarBusinesses
    {
        public int businessid { get; set; }
        public string businessurl { get; set; }
        public string businessname { get; set; }
        public string product { get; set; }
        public string service { get; set; }
        public int? avgrating { get; set; }
        public string companylogo { get; set; }
        public int? stateid { get; set; }
        public int? cityid { get; set; }
    }
    //[Serializable]
    public class Detail:SearchBarWebModel
    {
        public List<DetailWeb> BizMaster { get; set; }
        public List<WorkingHourBiz> BizWorkingHour { get; set; }
        public List<ProductBiz> BizProduct { get; set; }
        public List<ServiceBiz> BizService { get; set; }
        public dtBusinessURL BusinessURL { get; set; }
        public List<businesscntlike> businesscntlike { get; set; }
        public List<businesscntprsr> businesscntprsr { get; set; }
        public List<businesscntsrpr> businesscntsrpr { get; set; }
        public List<OpenAccountdt> openaccount { get; set; }
        public BusinessLeaddt businesslead { get; set; }
        public SelectList workinghourlist { get; set; }
    }
    public class ImproveBizInfoAddGet
    {
        public List<ImproveBiz> ImproveBizset { get; set; }
        public List<WorkingHourBiz> WorkingHourBiz { get; set; }
    }
    public class ImproveBizInfoData
    {
        public List<WorkingHourBiz> WorkingHourBiz { get; set; }
        public List<ProductList> ProductList { get; set; }
        public List<ProductBind> ProductBind { get; set; }
        public List<ServiceBind> ServiceBind { get; set; }
    }
    [Serializable]
    public class ProductList
    {
        public string productid { get; set; }
        public string productname { get; set; }
        public string active { get; set; }
    }
    public class ProductBind
    {
        public int? productmasterid { get; set; }
        public string productname { get; set; }
        public int? businessid { get; set; }
        public string image { get; set; }
        public int? categoryid { get; set; }
        public string categoryname { get; set; }
        public string productservice { get; set; }
    }
    public class ServiceBind
    {
        public int? servicemasterid { get; set; }
        public string servicename { get; set; }
        public int? categoryid { get; set; }
        public string categoryname { get; set; }
        public string serviceproduct { get; set; }
    }
    public class DetailBiz
    {
        public string businessurl { get; set; }
        public int photocount { get; set; }
        public int lke { get; set; }
        public int fav { get; set; }
        public int bizID { get; set; }
        public string businessname { get; set; }
        public string lattiude { get; set; }
        public string longitude { get; set; }
        public string contactperson { get; set; }
        public string address1 { get; set; }
        public string landmark { get; set; }
        public string phone { get; set; }
        public string didnumber { get; set; }
        public string website { get; set; }
        public string email { get; set; }
        public string Top3Products { get; set; }
        public string Top3Service { get; set; }
        public string Top3Categories { get; set; }
        public int? Rating { get; set; }
        public int reviewcount { get; set; }
        public string companylogo { get; set; }
        public string state { get; set; }
        public string zipcode { get; set; }
        public string alternatephone { get; set; }
        public string area { get; set; }
        public string city { get; set; }
    }
    public class DetailWeb : DetailBiz
    {
        public string fulladdress { get; set; }
        public int contentid { get; set; }
        public int? membership { get; set; }
        public string shortdesc { get; set; }
        public Nullable<bool> isheadoffice { get; set; }
        public int? headofficeid { get; set; }
        public int? isanybranch { get; set; }
        public string Top3Brands { get; set; }
        public int? stateid { get; set; }
        public string alternatephone2 { get; set; }
        public string alternatephone3 { get; set; }
        public string fax { get; set; }
        public int? areaid { get; set; }
        public string empstrength { get; set; }
        public int? isinternet { get; set; }
        public int? businessscale { get; set; }
        public string tollfree { get; set; }
        public string houseno { get; set; }
        public string categoryWithURL { get; set; }
        public int businessid { get; set; }
        public int? avgrating { get; set; }
        public int fav { get; set; }
        public int lke { get; set; }
        public string companylogo60 { get; set; }
        public string companylogo200 { get; set; }

        public string phoneBinary { get; set; }
        public string alternatePhoneBinary { get; set; }
        public string workinghours { get; set; }
        public int? type { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public int isvalidwebsite { get; set; }
        public int? cityid { get; set; }
        public int SelectedCityId { get; set; }
        public SelectList CityOptions { get; set; }

        public int? isstore { get; set; }     
        public int? photoct { get; set; }

        public string product { get; set; }
        public string service { get; set; }
        public string brand { get; set; }
        public string category { get; set; }
    }
    [Serializable]
    public class WorkingHourBiz
    {
        public string mondaystart { get; set; }
        public string mondayend { get; set; }
        public string tuesdaystart { get; set; }
        public string tuesdayend { get; set; }
        public string wednesdaystart { get; set; }
        public string wednesdayend { get; set; }
        public string thursdaystart { get; set; }
        public string thursdayend { get; set; }
        public string fridaystart { get; set; }
        public string fridayend { get; set; }
        public string saturdaystart { get; set; }
        public string saturdayend { get; set; }
        public string sundaystart { get; set; }
        public string sundayend { get; set; }
    }
    public class ProductBiz
    {
        public string area { get; set; }    //not using : Abhinav
        public string city { get; set; }    //not using
        public string state { get; set; }   //not using
        public int contentid { get; set; }  //not using
        public int? membership { get; set; }    //not using
        public string shortdesc { get; set; }   //not using
        public int? productmasterid { get; set; }   //not using
        public string productname { get; set; }
        public int? businessid { get; set; }
        public string businessname { get; set; }    //not using
        public string details { get; set; }     //not using
        public string modal { get; set; }   //not using
        public string price { get; set; }   //not using
        public string currency { get; set; }    //not using
        public string image { get; set; }
        public int? categoryid { get; set; }    //not using
        public string categoryname { get; set; }    //not using
        public string productservice { get; set; }  //not using
    }
    public class ImageObjbiz
    {
        public Int32 businessid { get; set; }
        public string name { get; set; }
    }
    public class ServiceBiz
    {
        public string area { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public int contentid { get; set; }
        public string businessname { get; set; }
        public string shortdesc { get; set; }
        public int? membership { get; set; }
        public int? servicemasterid { get; set; }
        public string servicename { get; set; }
        public int? businessid { get; set; }
        public string details { get; set; }
        public int? categoryid { get; set; }
        public string categoryname { get; set; }
        public Nullable<bool> isheadoffice { get; set; }
        public int? headofficeid { get; set; }
        public int? isanybranch { get; set; }
        public string createddate { get; set; }
        public string serviceproduct { get; set; }
    }
    public class dtBusinessURL
    {
        public string url { get; set; }
    }

    public class DetailReviewInfo
    {
        public List<DetailReview> businessReview { get; set; }
        public List<DetailReviewTotalRecords> totalRecords { get; set; }
        public int? mosthelpfulreviewcount {get; set;}
        public int? allreviewcount {get; set;}
    }

    public class DetailReview
    {
        //public Int32 reviewpostedby { get; set; }   //remove
        public string UserPhoto { get; set; }
        public int contentid { get; set; }
        public int userid { get; set; }
        public string reviewdetail { get; set; }
        public string createddate { get; set; }
        public int avgrating { get; set; }
        public int rating { get; set; }
        public int helpful { get; set; }
        public int inaccurate { get; set; }
        public string UserName { get; set; }
        public string location { get; set; }
        public string email { get; set; }
        //public int flag { get; set; }       //remove
        //public Nullable<long> businessid { get; set; }  //remove
        //public string businessname { get; set; }        //remove
        //public Nullable<int> reviewedby { get; set; }   //remove
        //public Nullable<int> createdby { get; set; }    //remove
        public int? followedby { get; set; }    //added by abhinav [21 Jan 2015 15:29]
        public string userurl { get; set; }
        public Int32? reviewCount { get; set; }
        public Int32? likes { get; set; }
        public int? isfollowed { get; set; }
        public int? isreported { get; set; }
        public int? isliked { get; set; }
    }

    public class DetailReviewTotalRecords
    {
        public Int32 TotalRecords { get; set; }
    }

    public class DetailInfoBiz
    {
        public string contactno { get; set; }
        public string address { get; set; }
        public string businessname { get; set; }
    }

    public class ClaimBusiness
    {
        public int userid { get; set; }
        public string contactname { get; set; }
        public string userurl { get; set; }
    }
    public class businesscntlike
    {
        public int? userid { get; set; }
        public string username { get; set; }
        public string userurl { get; set; }
        public int? cntlike { get; set; }
        //public string createddate { get; set; }
        public string image1 { get; set; }
    }
    public class businesscntprsr
    {
        public string productmasterid { get; set; }
        public string productname { get; set; }
    }
    public class businesscntsrpr
    {
        public string servicemasterid { get; set; }
        public string servicename { get; set; }
    }
    public class BusinessLeaddt
    {
        public int leadid { get; set; }
        public string leadname { get; set; }
        public int activecnt { get; set; }
        public string title { get; set; }
        public string dscription { get; set; }
        public string sussesssms { get; set; }
        public string buttontxt { get; set; }
        public string titlemodal { get; set; }
        public string buttontxtmodal { get; set; }
        public int bizsubscriptionid { get; set; }
    }
    public class OpenAccountdt
    {
        public int contentid { get; set; }
        //public int bizid { get; set; }
        public int leadid { get; set; }
        //public int Qid { get; set; }
        public string Qname { get; set; }
        public string Qtype { get; set; }
        public string compalsary { get; set; }
        public string validations { get; set; }
        public string uniquetype { get; set; }
        public string answers { get; set; }
        public DateTime creadeddate { get; set; }
        public int createdby { get; set; }
        public string active { get; set; }
        public DateTime modifydate { get; set; }
        //public int modifyby { get; set; }
        public int qorder { get; set; }
        public int length { get; set; }
        public string leadname { get; set; }
        public string columnname { get; set; }
        public string uniquename { get; set; }
        public string placeholder { get; set; }
        public string uniquemessage { get; set; }
        public string referenceid { get; set; }
    }
    public class DetailPhone
    {
        public string phone { get; set; }
        public string alternatephone { get; set; }
    }

    public class DetailPhotoGallery
    {
        public string photosmax { get; set; }
        public string photothumbs { get; set; }
    }

    public class StoreItems
    {
        public int resultnum { get; set; }
        public int itemid { get; set; }
        public string productname { get; set; }
        public string imagepath { get; set; }
        public int? discountprice { get; set; }     //nullable example : prc_get_store @supplierid = 74, @pageNum = 1, @rowsPerPage = 12, @sort = 2
        public string brandname { get; set; }
        public int? discountpercent { get; set; }   //same as above
        public int rating { get; set; }
        public string url { get; set; }
    }
    public class StoreVariety
    {
        public string productname { get; set; }
        public int productid { get; set; }
    }

    public class StoreBrands
    {
        public string brandname { get; set; }
        public int brandid { get; set; }
        public int totalcount { get; set; }
    }

    public class StoreAttributeVariety
    {
        public int attributeid { get; set; }
        public string attributename { get; set; }
    }

    public class StoreAttributeValues
    {
        public int attributeparentid { get; set; }
        public string attributeparentname { get; set; }
        public int attributeid { get; set; }
        public string attributename { get; set; }
        public int totalcount { get; set; }
    }

    public class StoreSelectedFilters
    {
        public int productid { get; set; }
        public string brandid { get; set; }
        public int minprice { get; set; }
        public int maxprice { get; set; }
        public int pageNum { get; set; }
        public int rowsPerPage { get; set; }
        public int sort { get; set; }
        public string attributeid { get; set; }
    }

    public class Store
    {
        public List<StoreItems> itemdetails;
        public List<StoreVariety> product_services;
        public List<StoreBrands> brands;
        public int? maxprice, minprice;
        public int totalcount;
        public List<StoreAttributeVariety> attributemaster;
        public List<StoreAttributeValues> attributevalues;
        public StoreSelectedFilters selectedvalues;
    }
    public class ImproveBiz
    {
        public int? contentidin { get; set; }
        public string businessid { get; set; }
        public string bname { get; set; }
        public string bemail { get; set; }
        public string bwebsite { get; set; }
        public string bphone { get; set; }
        public string baphone { get; set; }
        public string baddress { get; set; }
        public string bdescription { get; set; }
        public string bproducts { get; set; }
        public string yemailadd { get; set; }
        public string isybiz { get; set; }

        public string blga { get; set; }
        public int? blgaid { get; set; }
        public string bstate { get; set; }
        public int? bstateid { get; set; }
        public int? userid { get; set; }

    }
    public class bizWebsite
    {
        public string website { get; set; }
    }
    public class DetailWEBBiz : VconnectDBContext29
    {
        public Detail clentdesign(Int64? leadid, string widgettype)
        {
            Detail Rst = new Detail();
            List<OpenAccountdt> openact = new List<OpenAccountdt>();
            BusinessLeaddt businesslead = new BusinessLeaddt();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[Client_Questionnaire]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@leadid", leadid));
                cmd.Parameters.Add(new SqlParameter("@widgettype", widgettype));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    openact = ((IObjectContextAdapter)db).ObjectContext.Translate<OpenAccountdt>(reader).ToList();

                    reader.NextResult();
                    businesslead = ((IObjectContextAdapter)db).ObjectContext.Translate<BusinessLeaddt>(reader).FirstOrDefault();
                    Rst.businesslead = businesslead;

                    Rst.openaccount = openact;

                }
                catch (Exception ex)
                {
                }
                finally
                {
                    //db.Database.Connection.Close();
                }
            }
            return Rst;
        }
        public ImproveBizInfoData GetProductServiceWork(Int64 businessId, string from = "0")
        {
            ImproveBizInfoData improvlist = new ImproveBizInfoData();
            List<WorkingHourBiz> bizWorkingHour = new List<WorkingHourBiz>();
            List<ProductList> bizProduct = new List<ProductList>();
            List<ProductBind> ProductBind = new List<ProductBind>();
            List<ServiceBind> ServiceBind = new List<ServiceBind>();

            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_bizWorking_hours_from]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", businessId));
                cmd.Parameters.Add(new SqlParameter("@from", from));

                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();
                bizWorkingHour = ((IObjectContextAdapter)db).ObjectContext.Translate<WorkingHourBiz>(reader).ToList();
                if (from == "1")
                {
                    reader.NextResult();
                    bizProduct = ((IObjectContextAdapter)db).ObjectContext.Translate<ProductList>(reader).ToList();
                    improvlist.WorkingHourBiz = bizWorkingHour;
                    improvlist.ProductList = bizProduct;
                }
                else if (from == "2")
                {
                    improvlist.WorkingHourBiz = bizWorkingHour;
                }
                else
                {
                    reader.NextResult();
                    ServiceBind = ((IObjectContextAdapter)db).ObjectContext.Translate<ServiceBind>(reader).ToList();

                    reader.NextResult();
                    ProductBind = ((IObjectContextAdapter)db).ObjectContext.Translate<ProductBind>(reader).ToList();

                    improvlist.WorkingHourBiz = bizWorkingHour;
                    improvlist.ServiceBind = ServiceBind;
                    improvlist.ProductBind = ProductBind;
                }
            }
            //return result;
            return improvlist;
        }
        public Detail GetBizDetails(Int64 businessId, Int64? userId = null)
        {
            Detail bizDetails = new Detail();
            List<DetailWeb> bizMaster = new List<DetailWeb>();
            //List<WorkingHourBiz> bizWorkingHour = new List<WorkingHourBiz>();
            //List<ProductBiz> bizProduct = new List<ProductBiz>();
           // List<ServiceBiz> bizService = new List<ServiceBiz>();
            dtBusinessURL businessUrl = new dtBusinessURL();
            List<businesscntlike> businesscntlike = new List<businesscntlike>();
            //List<businesscntprsr> businesscntprsr = new List<businesscntprsr>();
            //List<businesscntsrpr> businesscntsrpr = new List<businesscntsrpr>();
            BusinessLeaddt businesslead = new BusinessLeaddt();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                //cmd.CommandText = "[dbo].[prc_get_businessdetailsnew_test_beta]";  
                //cmd.CommandText = "[dbo].[prc_get_businessdetailsnew_test_2015]";                
                cmd.CommandText = "[dbo].[prc_get_businessdetails_live]";

                cmd.CommandTimeout = 600;

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", businessId));
                cmd.Parameters.Add(new SqlParameter("@oldbusinessid", 0));
                cmd.Parameters.Add(new SqlParameter("@vcid", 0));
                if (userId != null && userId != 0)
                {
                    cmd.Parameters.Add(new SqlParameter("@userid", userId));
                }
                // try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();
                    bizMaster = ((IObjectContextAdapter)db).ObjectContext.Translate<DetailWeb>(reader).ToList();

                    //reader.NextResult();
                    //bizWorkingHour = ((IObjectContextAdapter)db).ObjectContext.Translate<WorkingHourBiz>(reader).ToList();
                    
                    //    reader.NextResult();
                    //    bizProduct = ((IObjectContextAdapter)db).ObjectContext.Translate<ProductBiz>(reader).ToList();
                  
                    //reader.NextResult();
                    //bizService = ((IObjectContextAdapter)db).ObjectContext.Translate<ServiceBiz>(reader).ToList();

                    reader.NextResult();
                    businessUrl = ((IObjectContextAdapter)db).ObjectContext.Translate<dtBusinessURL>(reader).FirstOrDefault();

                    reader.NextResult();
                    businesscntlike = ((IObjectContextAdapter)db).ObjectContext.Translate<businesscntlike>(reader).ToList();

                    //reader.NextResult();
                    //businesscntprsr = ((IObjectContextAdapter)db).ObjectContext.Translate<businesscntprsr>(reader).ToList();

                    //reader.NextResult();
                    //businesscntsrpr = ((IObjectContextAdapter)db).ObjectContext.Translate<businesscntsrpr>(reader).ToList();

                    reader.NextResult();
                    businesslead = ((IObjectContextAdapter)db).ObjectContext.Translate<BusinessLeaddt>(reader).FirstOrDefault();
                    bizDetails.businesslead = businesslead;
                    if (bizDetails.businesslead != null && bizDetails.businesslead.leadid != 0 && bizDetails.businesslead.leadid != null)
                    {
                        //for Lead display
                        bizDetails = clentdesign(businessId, "business");
                    }
                    bizDetails.BizMaster = bizMaster;
                    //bizDetails.BizWorkingHour = bizWorkingHour;
                    
                    //    bizDetails.BizProduct = bizProduct;
                    
                    //bizDetails.BizService = bizService;
                    bizDetails.BusinessURL = businessUrl;
                    bizDetails.businesscntlike = businesscntlike;
                    //bizDetails.businesscntprsr = businesscntprsr;
                    //bizDetails.businesscntsrpr = businesscntsrpr;
                    bizDetails.businesslead = businesslead;
                }
                // catch (Exception ex)
                {
                }
                // finally
                {
                    //db.Database.Connection.Close();
                }
            }
            //return result;
            return bizDetails;
        }
        public IList<SimilarBusinesses> GetRelatedBiz(Int64? bizId)
        {
            List<SimilarBusinesses> similarBiz = new List<SimilarBusinesses>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_similarBiz]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@bussinessId", bizId));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();

                similarBiz = ((IObjectContextAdapter)db).ObjectContext.Translate<SimilarBusinesses>(reader).ToList();
            }

            return similarBiz;
        }
        public DetailReviewInfo GetBizReviewDetails(Int64 bizId, Int64 contentId, int? pageNum, int? rowsPerPage, int sort)
        {
            DetailReviewInfo detailReviewInfo = new DetailReviewInfo();
            List<DetailReview> detailReview = new List<DetailReview>();
            List<DetailReviewTotalRecords> detailReviewTotalRecords = new List<DetailReviewTotalRecords>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                //cmd.CommandText = "[dbo].[prc_get_businessreviews_beta]";
                cmd.CommandText = "[dbo].[prc_get_businessreviews_live]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", bizId));
                cmd.Parameters.Add(new SqlParameter("@pageNum", pageNum));
                cmd.Parameters.Add(new SqlParameter("@rowsPerPage", rowsPerPage));
                cmd.Parameters.Add(new SqlParameter("@contentid", contentId));
                cmd.Parameters.Add(new SqlParameter("@sort", sort));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();
                //bizReview = ((IObjectContextAdapter)db).ObjectContext.Translate<BizReview>(reader).ToList();
                detailReview = ((IObjectContextAdapter)db).ObjectContext.Translate<DetailReview>(reader).Where(x => x.UserName != null && x.UserName != "").ToList();

                reader.NextResult();
                reader.Read();
                detailReviewInfo.mosthelpfulreviewcount = Convert.ToInt32(reader.GetValue(0));
                detailReviewInfo.allreviewcount = Convert.ToInt32(reader.GetValue(1));
                detailReviewTotalRecords = ((IObjectContextAdapter)db).ObjectContext.Translate<DetailReviewTotalRecords>(reader).ToList();

                detailReviewInfo.businessReview = detailReview;
                detailReviewInfo.totalRecords = detailReviewTotalRecords;
            }
            return detailReviewInfo;
        }
        public DetailInfoBiz GetBizDetailInfo(Int64 bizId)
        {
            DetailInfoBiz bizDetailInfo = new DetailInfoBiz();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_business_Info]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", bizId));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();
                // reader.NextResult();
                bizDetailInfo = ((IObjectContextAdapter)db).ObjectContext.Translate<DetailInfoBiz>(reader).FirstOrDefault();
            }

            return bizDetailInfo;
        }
        public int ReportIncorrect(string remarks, string whatswrong, string detail, string email, int bizId, int userid)
        {
            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_businessreportincorrect_Live]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@businessid", bizId));
                cmd.Parameters.Add(new SqlParameter("@email", email));
                cmd.Parameters.Add(new SqlParameter("@phone", ""));
                cmd.Parameters.Add(new SqlParameter("@name", ""));
                cmd.Parameters.Add(new SqlParameter("@source", "Web"));
                cmd.Parameters.Add(new SqlParameter("@whatswrong", whatswrong));
                cmd.Parameters.Add(new SqlParameter("@reportincorrectdetail", detail + remarks));
                cmd.Parameters.Add(new SqlParameter("@useridentity", 1));
                //cmd.Parameters.Add(new SqlParameter("@Err", 0));
                //cmd.Parameters.Add(new SqlParameter("@OUTPassword", ""));
                SqlParameter err = new SqlParameter("@Err", SqlDbType.Int);
                err.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(err);

                SqlParameter outpassword = new SqlParameter("@OUTPassword", SqlDbType.VarChar, 100);
                outpassword.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outpassword);

                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();

                var Err = cmd.Parameters["@Err"].Value;
                var OUTPassword = cmd.Parameters["@OUTPassword"].Value;
                result = (int)cmd.Parameters["@Err"].Value;
            }

            return result;
        }
        public int ImproveBizInfoADDget(ImproveBizInfoAddGet detailimprove, out int claimed1)
        {
            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_get_improvebizinfo_New]";
                //cmd.CommandText = "[dbo].[prc_add_get_improvebizinfo_Old]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@userid", detailimprove.ImproveBizset.FirstOrDefault().userid));
                cmd.Parameters.Add(new SqlParameter("@businesscontentid", detailimprove.ImproveBizset.FirstOrDefault().businessid));
                cmd.Parameters.Add(new SqlParameter("@businessname", detailimprove.ImproveBizset.FirstOrDefault().bname));
                cmd.Parameters.Add(new SqlParameter("@email", detailimprove.ImproveBizset.FirstOrDefault().bemail));
                cmd.Parameters.Add(new SqlParameter("@website", detailimprove.ImproveBizset.FirstOrDefault().bwebsite));
                cmd.Parameters.Add(new SqlParameter("@phone", detailimprove.ImproveBizset.FirstOrDefault().bphone));
                cmd.Parameters.Add(new SqlParameter("@alternatephone", detailimprove.ImproveBizset.FirstOrDefault().baphone));
                cmd.Parameters.Add(new SqlParameter("@bproducts", detailimprove.ImproveBizset.FirstOrDefault().bproducts));
                if (detailimprove.ImproveBizset.FirstOrDefault().bstateid == 0)
                {
                    cmd.Parameters.Add(new SqlParameter("@state", ""));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@state", detailimprove.ImproveBizset.FirstOrDefault().bstate));
                }
                cmd.Parameters.Add(new SqlParameter("@stateid", detailimprove.ImproveBizset.FirstOrDefault().bstateid));
                if (detailimprove.ImproveBizset.FirstOrDefault().bstateid == 0)
                {
                    cmd.Parameters.Add(new SqlParameter("@city", ""));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@city", detailimprove.ImproveBizset.FirstOrDefault().blga));
                }
                cmd.Parameters.Add(new SqlParameter("@cityid", detailimprove.ImproveBizset.FirstOrDefault().blgaid));
                cmd.Parameters.Add(new SqlParameter("@address1", detailimprove.ImproveBizset.FirstOrDefault().baddress));
                cmd.Parameters.Add(new SqlParameter("@Descriptions", detailimprove.ImproveBizset.FirstOrDefault().bdescription));

                cmd.Parameters.Add(new SqlParameter("@mondaystart", detailimprove.WorkingHourBiz.FirstOrDefault().mondaystart));
                cmd.Parameters.Add(new SqlParameter("@mondayend", detailimprove.WorkingHourBiz.FirstOrDefault().mondayend));
                cmd.Parameters.Add(new SqlParameter("@tuesdaystart", detailimprove.WorkingHourBiz.FirstOrDefault().tuesdaystart));
                cmd.Parameters.Add(new SqlParameter("@tuesdayend", detailimprove.WorkingHourBiz.FirstOrDefault().tuesdayend));
                cmd.Parameters.Add(new SqlParameter("@wednesdaystart", detailimprove.WorkingHourBiz.FirstOrDefault().wednesdaystart));
                cmd.Parameters.Add(new SqlParameter("@wednesdayend", detailimprove.WorkingHourBiz.FirstOrDefault().wednesdayend));
                cmd.Parameters.Add(new SqlParameter("@thursdaystart", detailimprove.WorkingHourBiz.FirstOrDefault().thursdaystart));
                cmd.Parameters.Add(new SqlParameter("@thursdayend", detailimprove.WorkingHourBiz.FirstOrDefault().thursdayend));
                cmd.Parameters.Add(new SqlParameter("@fridaystart", detailimprove.WorkingHourBiz.FirstOrDefault().fridaystart));
                cmd.Parameters.Add(new SqlParameter("@fridayend", detailimprove.WorkingHourBiz.FirstOrDefault().fridayend));
                cmd.Parameters.Add(new SqlParameter("@saturdaystart", detailimprove.WorkingHourBiz.FirstOrDefault().saturdaystart));
                cmd.Parameters.Add(new SqlParameter("@saturdayend", detailimprove.WorkingHourBiz.FirstOrDefault().sundayend));
                cmd.Parameters.Add(new SqlParameter("@sundaystart", detailimprove.WorkingHourBiz.FirstOrDefault().sundaystart));
                cmd.Parameters.Add(new SqlParameter("@sundayend", detailimprove.WorkingHourBiz.FirstOrDefault().sundayend));
                cmd.Parameters.Add(new SqlParameter("@isyrbiz", detailimprove.ImproveBizset.FirstOrDefault().isybiz));
                cmd.Parameters.Add(new SqlParameter("@isyremailid", detailimprove.ImproveBizset.FirstOrDefault().yemailadd));
                cmd.Parameters.Add(new SqlParameter("@source", "Web"));
                cmd.Parameters.Add(new SqlParameter("@Contentidin", detailimprove.ImproveBizset.FirstOrDefault().contentidin));
                SqlParameter Contentidout = new SqlParameter("@Contentidout", SqlDbType.Int);
                Contentidout.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(Contentidout);

                SqlParameter claimed = new SqlParameter("@claimed", SqlDbType.Int);
                claimed.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(claimed);
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();
                var Err = cmd.Parameters["@Contentidout"].Value;
                claimed1 = (int)cmd.Parameters["@claimed"].Value;
                result = (int)cmd.Parameters["@Contentidout"].Value;
            }

            return result;
        }
        public int DuplicatebusinessCheck(int businessId, int textid, string textvalue, int? userid = null)
        {
            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_duplicatebussinessInfo]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessId", businessId));
                cmd.Parameters.Add(new SqlParameter("@textid", textid));
                cmd.Parameters.Add(new SqlParameter("@textvalue", textvalue));
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@errorcode", SqlDbType.VarChar, 10));
                cmd.Parameters["@errorcode"].Direction = ParameterDirection.Output;
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();
                db.Database.Connection.Close();
                result = Convert.ToInt32(cmd.Parameters["@errorcode"].Value.ToString());
            }
            return result;
        }
        public int addbusinessphotos(int businessid, int contentid, int userid, string businessname, string photos)
        {
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_PhotosToBusiness]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                cmd.Parameters.Add(new SqlParameter("@contentid", contentid));
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@businessname", businessname));
                cmd.Parameters.Add(new SqlParameter("@photo", photos));
                cmd.Parameters.Add(new SqlParameter("@Err", ParameterDirection.Output));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();
                int result = (int)cmd.Parameters["@Err"].Value;
                return result;
            }
        }
        public string addbusinessreviews(Int64 bizId, Int64 userId, string Review, string bizName, int share)
        {
            string result;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_businessreviews]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", bizId));
                cmd.Parameters.Add(new SqlParameter("@businessname", bizName));
                cmd.Parameters.Add(new SqlParameter("@userid", userId));
                cmd.Parameters.Add(new SqlParameter("@useridentity", userId));
                cmd.Parameters.Add(new SqlParameter("@reviewtitle", "Review"));
                cmd.Parameters.Add(new SqlParameter("@reviewdetail", Review));
                cmd.Parameters.Add(new SqlParameter("@stateid", 2));
                cmd.Parameters.Add(new SqlParameter("@cityid", 1));
                cmd.Parameters.Add(new SqlParameter("@cybercafe", 1));
                cmd.Parameters.Add(new SqlParameter("@lga", 0));
                cmd.Parameters.Add(new SqlParameter("@isreview", 0));
                cmd.Parameters.Add(new SqlParameter("@share", share));
                //cmd.Parameters.Add(new SqlParameter("@Err", ParameterDirection.Output));
                //cmd.Parameters.Add(new SqlParameter("@OUTPassword", ParameterDirection.Output));
                //cmd.Parameters.Add(new SqlParameter("@OUTBusinessReviewContentID", ParameterDirection.Output));
                SqlParameter err = new SqlParameter("@Err", SqlDbType.Int);

                err.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(err);

                SqlParameter outpassword = new SqlParameter("@OUTPassword", SqlDbType.VarChar, 100);
                outpassword.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(outpassword);

                SqlParameter OUTBusinessReviewContentID = new SqlParameter("@OUTBusinessReviewContentID", SqlDbType.VarChar, 1000);
                OUTBusinessReviewContentID.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(OUTBusinessReviewContentID);
                //var err = new SqlParameter("@Err", SqlDbType.Int);
                //err.Direction = ParameterDirection.Output;
                //cmd.Parameters.Add(err);


                //var oUTPassword = new SqlParameter("@OUTPassword", SqlDbType.VarChar);
                //oUTPassword.Direction = ParameterDirection.Output;
                //cmd.Parameters.Add(oUTPassword);

                //var oUTBusinessReviewContentID = new SqlParameter("@OUTBusinessReviewContentID", SqlDbType.VarChar);
                //oUTBusinessReviewContentID.Direction = ParameterDirection.Output;
                //cmd.Parameters.Add(oUTBusinessReviewContentID);

                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();
                var Err = cmd.Parameters["@Err"].Value;
                var OUTPassword = cmd.Parameters["@OUTPassword"].Value;
                // string OUTBusinessReviewContentID = cmd.Parameters["@OUTBusinessReviewContentID"].Value.ToString();
                result = (string)cmd.Parameters["@OUTBusinessReviewContentID"].Value;
                //cmd.ExecuteReader();
            }
            return result;
        }
        public string addbusinessRate(Int32 bizId, Int32 userId, Int16? rate, Int32 reviewid)
        {
            string result = string.Empty;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[usp_AddUpdRatings_beta]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", bizId));
                cmd.Parameters.Add(new SqlParameter("@UserId", userId));
                cmd.Parameters.Add(new SqlParameter("@Rating", rate));
                cmd.Parameters.Add(new SqlParameter("@source", "WEB"));
                cmd.Parameters.Add(new SqlParameter("@reviewid", reviewid));
                cmd.Parameters.Add(new SqlParameter("@ipaddress", Vconnect.Common.MyGlobalVariables.GetIpAddress));
                //cmd.Parameters.Add(new SqlParameter("@errMsg", 0));
                cmd.Parameters.Add(new SqlParameter("@errMsg", ParameterDirection.Output));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();
                result = cmd.Parameters["@errMsg"].Value.ToString();
                //return result;
            }
            return result;
        }
        public IList<DetailPhotoGallery> GetPhotoGallery(long businessId, int? pageNum, int? rowsPerPage)
        {
            var bizPhotoGallery = new List<DetailPhotoGallery>();
            
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_businessphotos_beta]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", businessId));
                cmd.Parameters.Add(new SqlParameter("@pageNum", pageNum));
                cmd.Parameters.Add(new SqlParameter("@rowsPerPage", rowsPerPage));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();
                bizPhotoGallery = ((IObjectContextAdapter)db).ObjectContext.Translate<DetailPhotoGallery>(reader).ToList();

            }
            return bizPhotoGallery;
        }
        public IList<WorkingHourBiz> GetWorking_hours(long businessId)
        {
            var bizWorking_hours = new List<WorkingHourBiz>();

            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_bizWorking_hours]";
                //cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", businessId));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();
                bizWorking_hours = ((IObjectContextAdapter)db).ObjectContext.Translate<WorkingHourBiz>(reader).ToList();

            }
            return bizWorking_hours;
        }
        public ClaimBusiness GetBusinessClaim(long businessId)
        {
            var businessClaim = new ClaimBusiness();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_businessclaim]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", businessId));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                var reader = cmd.ExecuteReader();
                businessClaim = ((IObjectContextAdapter)db).ObjectContext.Translate<ClaimBusiness>(reader).FirstOrDefault();

            }
            return businessClaim;
        }

        public bool IPallowed(string ip)
        {
            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_is_ip_allowed]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ip", ip));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();
                // reader.NextResult();
                result = ((IObjectContextAdapter)db).ObjectContext.Translate<Int32>(reader).FirstOrDefault();
            }
            return result == 1;
        }

        public DetailPhone GetPhone(long businessId)
        {
            var detailPhone = new DetailPhone();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_businessphonenumber]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", businessId));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();
                detailPhone = ((IObjectContextAdapter)db).ObjectContext.Translate<DetailPhone>(reader).FirstOrDefault();

            }
            return detailPhone;
        }

        public void getBusinessAddress(int bizID, out string houseno, out string address1, out string area)
        {
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_business_address_details]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", bizID));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();
                reader.Read();
                houseno = reader.GetString(0);
                address1 = reader.GetString(1);
                area = reader.GetString(2);
            }
        }
        public int wasthishelpful(string whichnumber, string status, Int64 bizid, Int64 userid)
        {
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_wasthishelpful]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@businessid", bizid));
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@wasthishelpful", status));
                cmd.Parameters.Add(new SqlParameter("@whichnumber", whichnumber));
                cmd.Parameters.Add(new SqlParameter("@source", "WEB"));
                cmd.Parameters.Add(new SqlParameter("@Err", ParameterDirection.Output));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();
                int result = (int)cmd.Parameters["@Err"].Value;
                return result;
            }
        }
        public void IsHelpFull(Int64 bizId, Int64 reviewId, int? userId, int? ishelp)
        {
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_ishelpfull]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ReviewId", reviewId));
                cmd.Parameters.Add(new SqlParameter("@userid", userId));
                cmd.Parameters.Add(new SqlParameter("@bizid", bizId));
                cmd.Parameters.Add(new SqlParameter("@ishelpfoll", ishelp));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteReader();
            }
        }
        public void IsReviewAbusive(Int64 bizId, Int64 reviewId, int? userId, int? isabusive)
        {
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_reviewabusive]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ReviewId", reviewId));
                cmd.Parameters.Add(new SqlParameter("@userid", userId));
                cmd.Parameters.Add(new SqlParameter("@bizid", bizId));
                cmd.Parameters.Add(new SqlParameter("@isabusive", isabusive));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteReader();
            }
        }

        public Store GetStore(int supplierid, int productid=0, string brandid=null, int minprice=0, int maxprice=0, int pageNum = 1, int rowsPerPage =12, int sort = 1 , string attributeid=null)
        {
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_store]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@supplierid", supplierid));
                cmd.Parameters.Add(new SqlParameter("@productid", productid));
                cmd.Parameters.Add(new SqlParameter("@brandid", brandid));
                cmd.Parameters.Add(new SqlParameter("@minprice", minprice));
                cmd.Parameters.Add(new SqlParameter("@maxprice", maxprice));
                cmd.Parameters.Add(new SqlParameter("@pageNum", pageNum));
                cmd.Parameters.Add(new SqlParameter("@rowsPerPage", rowsPerPage));
                cmd.Parameters.Add(new SqlParameter("@sort", sort));
                cmd.Parameters.Add(new SqlParameter("@attributeid", attributeid));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                Store myStore = new Store();

                using (var reader = cmd.ExecuteReader())
                {

                    myStore.itemdetails = ((IObjectContextAdapter)db).ObjectContext.Translate<StoreItems>(reader).ToList();
                    reader.NextResult();

                    myStore.product_services = ((IObjectContextAdapter)db).ObjectContext.Translate<StoreVariety>(reader).ToList();
                    reader.NextResult();

                    myStore.brands = ((IObjectContextAdapter)db).ObjectContext.Translate<StoreBrands>(reader).ToList();
                    reader.NextResult();

                    reader.Read();
                    myStore.maxprice = DBNull.Value.Equals(reader.GetValue(0)) ? 0 : Convert.ToInt32(reader.GetValue(0));  //((IObjectContextAdapter)db).ObjectContext.Translate<int?>(reader).FirstOrDefault();
                    myStore.minprice = DBNull.Value.Equals(reader.GetValue(1)) ? 0 : Convert.ToInt32(reader.GetValue(1));

                    if (myStore.maxprice == myStore.minprice && myStore.maxprice != 0)
                        myStore.maxprice += 100;

                    reader.NextResult();

                    myStore.totalcount = ((IObjectContextAdapter)db).ObjectContext.Translate<int>(reader).FirstOrDefault();
                    reader.NextResult();

                    myStore.attributemaster = ((IObjectContextAdapter)db).ObjectContext.Translate<StoreAttributeVariety>(reader).ToList();
                    reader.NextResult();

                    myStore.attributevalues = ((IObjectContextAdapter)db).ObjectContext.Translate<StoreAttributeValues>(reader).ToList();
                    reader.NextResult();

                    myStore.selectedvalues = ((IObjectContextAdapter)db).ObjectContext.Translate<StoreSelectedFilters>(reader).FirstOrDefault();
                }

                connection.Close();

                return myStore;
            }
        }

        public void getBusinessEmail(int bizID, out string bname, out string bemail)
        {
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_business_email]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@bizID", bizID));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                Store myStore = new Store();

                using (var reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    bname = reader.GetValue(0).ToString();
                    bemail = reader.GetValue(1).ToString();
                }

                connection.Close();
            }
        }

        public Dictionary<string, string> addQuoteRequestToDB(int itemid, string name, string phone, int whenToBuy, string message,string sourceurl,string previousurl)
        {
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_GetQuote]";    //delete this proc : "[dbo].[prc_insert_get_quote_request]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@itemid", itemid));
                cmd.Parameters.Add(new SqlParameter("@sendername", name));
                cmd.Parameters.Add(new SqlParameter("@senderNumber", phone));
                cmd.Parameters.Add(new SqlParameter("@answerID", whenToBuy));
                cmd.Parameters.Add(new SqlParameter("@description", message));
                cmd.Parameters.Add(new SqlParameter("@source", "WEB"));
                cmd.Parameters.Add(new SqlParameter("@sourceURL", sourceurl));
                cmd.Parameters.Add(new SqlParameter("@previousURL", previousurl));             
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                Dictionary<string, string> result = new Dictionary<string, string>();
                string[] columnnames = new string[6] { "productname", "description", "bizphone", "bizemail", "ismasked", "imagepath" };

                using (var reader = cmd.ExecuteReader())
                {
                    reader.Read();

                    for (var i = 0; i < 6; i++)
                    {
                        result.Add(columnnames[i], DBNull.Value.Equals(reader.GetValue(i)) ? "" : reader.GetValue(i).ToString());
                    }
                }

                connection.Close();
                return result;
            }
        }

        public void AddEmailLog(int bizID, string bname, string email, string message, string name, int userid)	//Ankita's code
        {
            try
            {
                using (var db = new VconnectDBContext29())
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[prc_add_emaillog]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@businessid", bizID));
                    cmd.Parameters.Add(new SqlParameter("@businessname", bname));
                    cmd.Parameters.Add(new SqlParameter("@useremail", email));
                    cmd.Parameters.Add(new SqlParameter("@username", name));
                    cmd.Parameters.Add(new SqlParameter("@messagetype", "BOmail"));
                    cmd.Parameters.Add(new SqlParameter("@userid", userid));
                    cmd.Parameters.Add(new SqlParameter("@source", "WEB"));
                    cmd.Parameters.Add(new SqlParameter("@message", message));
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteReader();
                }
            }
            catch (Exception ex)
            {
                //log.LogMe(ex);
            }

        }

        public static DataTable getsmsgateways(int type)
        {
            using (SqlConnection _con = new SqlConnection("server=72.251.246.226;database=vconnectnew;uid=vcnew;pwd=vcnew1234;pooling=true"))
            //   using (SqlConnection _con = new SqlConnection("server=192.168.4.202;database=vconnectlatest;uid=teamaardee;pwd=teamaardee#1234;pooling=true"))

            using (SqlCommand _com = new SqlCommand())
            using (SqlDataAdapter da = new SqlDataAdapter())
            using (DataTable dt = new DataTable())
            {
                _com.CommandTimeout = 75;
                _com.CommandType = CommandType.StoredProcedure;
                _com.Connection = _con;
                _com.CommandText = "prc_get_smsgateway";
                _com.Parameters.AddWithValue("@type", type);
                da.SelectCommand = _com;
                _con.Open();
                da.Fill(dt);
                _con.Close();
                _con.Dispose();
                return dt;
            }
        }

        public string Sendmessage(string message, string phone)
        {
            string ResponseArray = string.Empty;
            string strQuery;
            HttpWebRequest HttpWReq;
            HttpWebResponse HttpWResp;

            //sendername = sendername.ToUpper();
            Console.WriteLine("1...");
            if (phone != string.Empty)
            {
                if (phone.StartsWith("0"))
                {
                    phone = phone.Substring(1, phone.Length - 1);
                }
                else if (phone.StartsWith("234"))
                {
                    phone = phone.Substring(3, phone.Length - 3);
                }
                phone = "234" + phone;

                string text = string.Empty;
                text = HttpUtility.UrlEncode(message);
                Console.WriteLine("2...");
                //strQuery = "http://sms.shreeweb.com/sendsms/sendsms.php?username=Chandrika&password=h78j6tmg&type=TEXT&sender=VConnect&mobile=" + phone + "&message=" + text;
                DataTable dt = getsmsgateways(1);
                Console.WriteLine("3...");
                if (dt.Rows.Count > 0)
                {
                    Console.WriteLine("4...");
                    strQuery = dt.Rows[0]["URL"].ToString();
                    if (strQuery.Contains("###") && strQuery.Contains("$$$") && strQuery.Contains("***"))
                    {
                        strQuery = strQuery.Replace("$$$", text);
                        strQuery = strQuery.Replace("###", phone);
                        strQuery = strQuery.Replace("***", "VConnect");

                    }

                    HttpWReq = (HttpWebRequest)WebRequest.Create(strQuery);
                    HttpWReq.Method = "GET";
                    HttpWResp = (HttpWebResponse)HttpWReq.GetResponse();
                    System.IO.StreamReader reader = new System.IO.StreamReader(HttpWResp.GetResponseStream());
                    ResponseArray = reader.ReadToEnd();
                    reader.Close();
                    HttpWResp.Close();
                }
                Console.WriteLine(ResponseArray);
            }

            return ResponseArray;
        }

        public bool SendEmail(string subject, string email, string name, string message)
        {
            var recipients = new List<Mandrill.Messages.Recipient>();

            recipients.Add(new Mandrill.Messages.Recipient(email, name));   //Testing
            var mandrill = new Mandrill.Messages.Message()
            {
                To = recipients.ToArray(),
                FromEmail = "business@vconnect.com",
                Subject = subject,

                Html = message
            };
            //===================================
            SendEmail send = new SendEmail();
            try
            {
                send.BOMails(mandrill);             
                //send.SendMailToUser(ref Email, ConfigurationManager.AppSettings["networkUserIdvcCustomer"].ToString(), ConfigurationManager.AppSettings["networkUserIdPasswordvcCustomer"].ToString());
                return true;
            }
            catch (Exception exp)
            {
                log.LogMe(exp);
                return false;
            }
        }
        

        public int addsmsemaillog(int customerid, int usertype, string receiveremail, string receiverphone, string receivername, int messagetype,
           string subject, string message, int businessid, string keyword, string location, string response, string ipaddress, int smslength,
            int noofsms, string pageurl, int actiontype, string reversedns)
        {
            using (SqlConnection _con = new SqlConnection(ConfigurationManager.ConnectionStrings["VconnectDBContext29"].ToString()))
            using (SqlCommand _com = new SqlCommand())
            {
                _com.CommandTimeout = 120;
                _com.CommandType = CommandType.StoredProcedure;
                _com.Connection = _con;
                _com.CommandText = "prc_add_smslog";
                _com.Parameters.AddWithValue("@customerid", customerid);
                _com.Parameters.AddWithValue("@usertype", usertype);
                _com.Parameters.AddWithValue("@receiveremail", receiveremail);
                _com.Parameters.AddWithValue("@receiverphone", receiverphone.Replace(",", ""));
                _com.Parameters.AddWithValue("@receivername", receivername);
                _com.Parameters.AddWithValue("@messagetype", messagetype);
                _com.Parameters.AddWithValue("@subject", subject);
                _com.Parameters.AddWithValue("@message", message);
                _com.Parameters.AddWithValue("@businessid", businessid);
                _com.Parameters.AddWithValue("@keyword", keyword);
                _com.Parameters.AddWithValue("@location", location);
                _com.Parameters.AddWithValue("@response", response);
                _com.Parameters.AddWithValue("@ipaddress", ipaddress);
                _com.Parameters.AddWithValue("@smslength", smslength);
                _com.Parameters.AddWithValue("@noofsms", noofsms);
                _com.Parameters.AddWithValue("@pageurl", pageurl);
                _com.Parameters.AddWithValue("@actiontype", actiontype);
                _com.Parameters.AddWithValue("@createdby", customerid);
                _com.Parameters.AddWithValue("@reversedns", reversedns);
                _com.Parameters.Add("@Err", SqlDbType.Int).Direction = ParameterDirection.Output;
                _con.Open();
                _com.ExecuteNonQuery();
                int result = (int)_com.Parameters["@Err"].Value;
                _con.Close(); _con.Dispose();
                return result;
            }

        }

        public string GetBizWebsite(Int32? bizid)
        {
            bizWebsite bizWebsite = new bizWebsite();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_bizwebsite]";
                cmd.CommandTimeout = 600;

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", bizid.Value));


                // try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();
                    bizWebsite = ((IObjectContextAdapter)db).ObjectContext.Translate<bizWebsite>(reader).FirstOrDefault();


                }
                // catch (Exception ex)
                {
                }
                // finally
                {
                    //db.Database.Connection.Close();
                }
            }
            return bizWebsite.website;
        }
    }

    #region Dynamic Design
    public class RegisterUserClientsdt
    {
        [Key]
        public int Error { get; set; }
        public int newuserconentid { get; set; }
    }
    public class Listdropbinddt
    {
        [Key]
        public int drpvalue { get; set; }
        public string drptext { get; set; }
    }
    #endregion
}