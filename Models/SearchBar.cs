﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vconnect.Models
{
   [Serializable]
    public class SearchBar
    {
        public SearchBar()
        {
            SearchText = string.Empty;
            SearchLocation = "Lagos";
        }
        
        public string SearchText { get; set; }
        
        public string SearchLocation { get; set; }
    }
}