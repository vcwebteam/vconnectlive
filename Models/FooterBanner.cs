﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Vconnect.Mapping.Listing;
using Vconnect.Models;
namespace Vconnect.Models
{
    public class FooterBanner
    {

        public FootBanner GetFooterBannerWAP()
        {
            var dataContext = new VconnectdbDataContext();
            DateTime getdate=System.DateTime.Now;
            
            var res=(from m in dataContext.businessbanners
                    where m.bannertypeid==15 && m.status==1
                     select new { m.contentid, m.membership, m.banner, m.bannertypeid, m.url }).ToList().FirstOrDefault();
            
            FootBanner footbanner = new FootBanner();
            footbanner.bannerid = res.contentid;
            footbanner.membership = res.membership;
            footbanner.bannerimage = res.banner;
            footbanner.bannertypeid = res.bannertypeid;
            footbanner.bannerurl = res.url;
            return footbanner;

            
        }

    }
}