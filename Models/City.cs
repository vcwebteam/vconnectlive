﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vconnect.Models
{
    public class City
    {
        public int contentid { get; set; }
        public int stateid { get; set; }
        public string cityname { get; set; }
    }
    public class Area
    {
        public int stateid { get; set; }
        public int cityid { get; set; }
        public int contentid { get; set; }
        public int Areastateid { get; set; }
        public string areaname { get; set; }
    }
}