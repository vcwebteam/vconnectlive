﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Vconnect.Mapping.Listing;
using Vconnect.Mapping.CommonDb;
using Vconnect.Mapping.ListingWEB;
using System.Data.SqlClient;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.ComponentModel.DataAnnotations;

namespace Vconnect.Models
{
    #region Bizdetail
    [Serializable]
    public class BizDetail
    {
        public string businessurl { get; set; }
        public int photocount { get; set; }
        public int lke { get; set; }
        public int fav { get; set; }
        public int bizID { get; set; }
        public string businessname { get; set; }
        public string lattiude { get; set; }
        public string longitude { get; set; }
        public string contactperson { get; set; }
        public string address1 { get; set; }
        public string landmark { get; set; }
        public string phone { get; set; }
        public string didnumber { get; set; }
        public string website { get; set; }
        public string email { get; set; }
        public string Top3Products { get; set; }
        public string Top3Service { get; set; }
        public string Top3Categories { get; set; }
        public int? Rating { get; set; }
        public int reviewcount { get; set; }
        public string companylogo { get; set; }
        public string state { get; set; }
        public string zipcode { get; set; }
        public string alternatephone { get; set; }
        public string area { get; set; }
        public string city { get; set; }

    }
    #region Wap Code
    //public class BizDetailWap : BizDetail
    //{
    //    public int productct { get; set; }
    //    public int servicect { get; set; }
    //    public int streetnme { get; set; }
    //    public int photocount { get; set; }
    //    public int businesscount { get; set; }
    //    public string businessurl { get; set; }
    //}
    #endregion
    public class BizDetailWeb : BizDetail
    {
        public string fulladdress { get; set; }
        public int contentid { get; set; }
        public int? membership { get; set; }
        public string shortdesc { get; set; }
        public Nullable<bool> isheadoffice { get; set; }
        public int? headofficeid { get; set; }
        public int? isanybranch { get; set; }
        public string Top3Brands { get; set; }
        public int? stateid { get; set; }
        public string alternatephone2 { get; set; }
        public string alternatephone3 { get; set; }
        public string fax { get; set; }
        public int? areaid { get; set; }
        public int? cityid { get; set; }
        public string empstrength { get; set; }
        public int? isinternet { get; set; }
        public int? businessscale { get; set; }
        public string tollfree { get; set; }
        public string houseno { get; set; }
        public string categoryWithURL { get; set; }
        public int businessid { get; set; }
        public int? avgrating { get; set; }
        public int fav { get; set; }
        public int lke { get; set; }
        public string companylogo60 { get; set; }
        public string companylogo200 { get; set; }

        public string phoneBinary { get; set; }
        public string alternatePhoneBinary { get; set; }
        public string workinghours { get; set; }
        public int? type { get; set; }
        public string title { get; set; }
        public string description { get; set; }

        public int isvalidwebsite { get; set; }
    }
    [Serializable]
    public class BizWorkingHour
    {
        public string mondaystart { get; set; }
        public string mondayend { get; set; }
        public string tuesdaystart { get; set; }
        public string tuesdayend { get; set; }
        public string wednesdaystart { get; set; }
        public string wednesdayend { get; set; }
        public string thursdaystart { get; set; }
        public string thursdayend { get; set; }
        public string fridaystart { get; set; }
        public string fridayend { get; set; }
        public string saturdaystart { get; set; }
        public string saturdayend { get; set; }
        public string sundaystart { get; set; }
        public string sundayend { get; set; }
    }
    public class CurrentWorkingHour
    {
        public string wday { get; set; }
        public string starttme { get; set; }
        public string endtme { get; set; }

    }
    [Serializable]
    public class LatestReview
    {
        public string userphoto { get; set; }
        public int contentid { get; set; }
        public int userid { get; set; }
        public string reviewdetail { get; set; }
        public string createddate { get; set; }
        public int rating { get; set; }
        public int helpful { get; set; }
        public int inaccurate { get; set; }
        public string username { get; set; }
        public string location { get; set; }
        public string email { get; set; }
        public int flag { get; set; }
    }
    [Serializable]
    public class BizBranches
    {
        public int? productcount { get; set; }
        public int? servicecount { get; set; }
        public int? attachmentcount { get; set; }
        public int? photocount { get; set; }
        public int? videocount { get; set; }
        public int? reviewcount { get; set; }
        public int businessid { get; set; }
        public int? avgrating { get; set; }
        public string topcategory { get; set; }
        public string businessurl { get; set; }
        public string businessname { get; set; }

    }
    public class BizAttachment
    {
        public int attachmentcount { get; set; }
        public string format { get; set; }
    }
    [Serializable]
    public class BizAttachmentDetail
    {
        public Nullable<long> contentid { get; set; }
        public string attachments_old { get; set; }
        public Nullable<long> businessid { get; set; }
        public string businessname { get; set; }
        public bool? isreview_old { get; set; }
        public Nullable<DateTime> createddate { get; set; }
        public int? createdby { get; set; }
        public Nullable<DateTime> modifieddate { get; set; }
        public int? modifiedby { get; set; }
        public int? status { get; set; }
        public int? formatid { get; set; }
        public string format { get; set; }
        public string filetype { get; set; }
        public int? filesize { get; set; }
        public int? oldid { get; set; }
        public string originalimage { get; set; }
        public int? stat { get; set; }
        public int? approvedby { get; set; }
        public Nullable<DateTime> approveddate { get; set; }
        public string attachments_20121009 { get; set; }
        public string attachments_20130104 { get; set; }
        public int? stat1 { get; set; }
        public int? isduplicate { get; set; }
        public int? isreview { get; set; }
        public int? fileexist { get; set; }
        public int? orgfile { get; set; }
        public string attachments_20130110 { get; set; }
        public string attachments { get; set; }
        public string attachmentsbak { get; set; }
        public string attachmentsmax { get; set; }
        public string attachmentsthumbs { get; set; }
        public int? isprofileimage { get; set; }
        public int? photocaption { get; set; }
        public string businessurl { get; set; }
        public string area { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string address1 { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string houseno { get; set; }
    }
    [Serializable]
    public class BizProduct
    {
        public string area { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public int contentid { get; set; }
        public int? membership { get; set; }
        public string shortdesc { get; set; }
        public int? productmasterid { get; set; }
        public string productname { get; set; }
        public int? businessid { get; set; }
        public string businessname { get; set; }
        public string details { get; set; }
        public string modal { get; set; }
        public string price { get; set; }
        public string currency { get; set; }
        public string image { get; set; }
        public int? categoryid { get; set; }
        public string categoryname { get; set; }
    }
    [Serializable]
    public class BizService
    {
        public string area { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public int contentid { get; set; }
        public string businessname { get; set; }
        public string shortdesc { get; set; }
        public int? membership { get; set; }
        public int? servicemasterid { get; set; }
        public string servicename { get; set; }
        public int? businessid { get; set; }
        public string details { get; set; }
        public int? categoryid { get; set; }
        public string categoryname { get; set; }
        public Nullable<bool> isheadoffice { get; set; }
        public int? headofficeid { get; set; }
        public int? isanybranch { get; set; }
        public string createddate { get; set; }
    }
    public class BusinessURL
    {
        public string url { get; set; }
    }
    public class BizPhotoCount
    {
        public int photoct { get; set; }
    }
    public class BizPhotoGallery
    {
        public int businessid { get; set; }
        public int contentid { get; set; }
        public string photo { get; set; }
        public string businessname { get; set; }
        public string createddate { get; set; }
        public int? membership { get; set; }
        public string contactname { get; set; }
        public string phone { get; set; }
        public string didnumber { get; set; }
        public string email { get; set; }
        public string alternatephone { get; set; }
        public string alternatephone2 { get; set; }
        public string alternatephone3 { get; set; }
        public Nullable<bool> isheadoffice { get; set; }
        public int? headofficeid { get; set; }
        public string businessaddress { get; set; }
        public string landmark { get; set; }
        public int? productcount { get; set; }
        public int? servicecount { get; set; }
        public int? attachmentcount { get; set; }
        public int? photocount { get; set; }
        public int? videocount { get; set; }
        public int? reviewcount { get; set; }
    }
    public class BusinessClaim
    {
        public long contentid { get; set; }
        public string username { get; set; }
        public string userurl { get; set; }
    }
    public class BusinessPhone
    {
        public string phone { get; set; }
        public string altenatephone { get; set; }
    }

    
    public class BusinessPhotoGallery
    {
        public long businessid { get; set; }
        public long contentid { get; set; }
        public string photo { get; set; }
        public string businessname { get; set; }
        public DateTime createddate { get; set; }
        public int? membership { get; set; }
        public string contactname { get; set; }
        public string phone { get; set; }
        public string didnumber { get; set; }
        public string email { get; set; }
        public string alternatephone { get; set; }
        public string alternatephone2 { get; set; }
        public string alternatephone3 { get; set; }
        public Nullable<bool> isheadoffice { get; set; }
        public int? headofficeid { get; set; }
        public string businessaddress { get; set; }
        public string landmark { get; set; }
        public int? productcount { get; set; }
        public int? servicecount { get; set; }
        public int? attachmentcount { get; set; }
        public int? photocount { get; set; }
        public int? videocount { get; set; }
        public int? reviewcount { get; set; }
        public string photosmax { get; set; }
        public string photothumbs { get; set; }
        public string businessurl { get; set; }
        public string area { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string address1 { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string houseno { get; set; }

    }
    public class BizVideo
    {
        public string businessurl { get; set; }
        public int VideoId { get; set; }
        public string VideoTitle { get; set; }
        public string VideoDescription { get; set; }
        public string VideoThumbNailExternal { get; set; }
        public string VideoEmbeddedPath { get; set; }
        public string createddate { get; set; }
    }
    public class BizPhotoContact
    {
        public string photo { get; set; }
        public string contactname { get; set; }
    }
    public class BizProductInfo
    {
        public string productname { get; set; }
        public string producturl { get; set; }
    }
    public class BizServiceInfo
    {
        public string servicename { get; set; }
        public string serviceurl { get; set; }
        public Nullable<System.DateTime> createddate { get; set; }
    }
    public class BizVideoInfo
    {
        public string videocode { get; set; }
    }
    public class ServiceName
    {
        public string servicename { get; set; }
        public string url { get; set; }
    }
    public class ProductName
    {
        public string productname { get; set; }
        public string url { get; set; }
    }
    public class BizDetailsWapRst
    {

        public string method { get; set; }
        public List<ServiceName> serviceName { get; set; }
        public List<ProductName> productName { get; set; }
        public List<String> Photothumb { get; set; }
        public List<BizDetail> bizDetail { get; set; }
        public BizWorkingHour bizWorkingHour { get; set; }
        public int totalreview { get; set; }
        public List<LatestReview> latestReview { get; set; }
        public int status { get; set; }
    }
    public class bizImageObj
    {
        public Int32 businessid { get; set; }
        public string name { get; set; }        
    }
    public class BizBanner
    {
        public long contentid { get; set; }
        public int? bannertypeid { get; set; }
        public string bannertypename { get; set; }
        public int? bannerpageid { get; set; }
        public string bannerpagename { get; set; }
        public string banner { get; set; }
        public int? businessid { get; set; }
        public string businessname { get; set; }
        public string url { get; set; }
        public bool? isreview { get; set; }
        public bool? isdeleted { get; set; }
        public DateTime createddate { get; set; }
        public int? createdby { get; set; }
        public Nullable<DateTime> modifieddate { get; set; }
        public int? modifiedby { get; set; }
        public int? status { get; set; }
        public Nullable<DateTime> subscribedate { get; set; }
        public int? membership { get; set; }
        public int? expirydate { get; set; }
        public int? maximpression { get; set; }
        public int? maxclicks { get; set; }
        public int? totimpression { get; set; }
        public int? totclicks { get; set; }
        public int? avgimpression { get; set; }
        public int? oldid { get; set; }
        public string bannerimage { get; set; }
        public string bannerurl { get; set; }
        public int stateid { get; set; }
    }

    
    public class Workinghours
    {
        public string weekday { get; set; }
        public string workinghours { get; set; }
    }
    public class BizDetails : SearchBarWebModel
    {
        public List<BizDetailWeb> BizMaster { get; set; }
        //public List<BizWorkingHour> BizWorkingHour { get; set; }
        //public List<BizBranches> BizBranches { get; set; }
        //public List<BizAttachment> BizAttachment { get; set; }
        public List<BizProduct> BizProduct { get; set; }
        public List<BizService> BizService { get; set; }
        public BusinessURL BusinessURL { get; set; }
        public List<Workinghours> WorkingHours { get; set; }
        //public List<BizPhotoCount> BizPhotoCount { get; set; }
        //public List<BizPhotoGallery> BizPhotoGallery { get; set; }
        //public List<BizVideo> BizVideo { get; set; }
        //public List<BizPhotoContact> BizPhotoContact { get; set; }
        //public List<BizProductInfo> BizProductInfo { get; set; }
        //public List<BizServiceInfo> BizServiceInfo { get; set; }
        //public List<BizVideoInfo> BizVideoInfo { get; set; }
        public List<OpenAccount> openaccount { get; set; }
        public BusinessLead businesslead { get; set; }
    }

    public class BizPhoneNumbers
    {
        public string phone { get; set; }
        public string alternatephone { get; set; }
        public string alternatephone2 { get; set; }
        public string alternatephone3 { get; set; }
    }
    //public class BizReview
    //{
    //    public Int64 contentid { get; set; }
    //    public string reviewdetail { get; set; }
    //    public string reviewtitle { get; set; }
    //    public string createdate { get; set; }
    //    public string UserName { get; set; }
    //    public Int32 reviewpostedby { get; set; }
    //    public string UserPhoto { get; set; }
    //    public string titleofreviewer { get; set; }
    //    public string titleofreviewerimage { get; set; }
    //    public Int32 avgrating { get; set; }
    //}
    //public partial class BizReview
    //{
    //    public Int32 reviewpostedby { get; set; }
    //    public int avgrating { get; set; }
    //    public string UserName { get; set; }
    //    public long contentid { get; set; }
    //    public Nullable<long> businessid { get; set; }
    //    public Nullable<long> userid_old { get; set; }
    //    public string reviewtitle { get; set; }
    //    public string reviewdetail { get; set; }
    //    public string oldreviewtitle { get; set; }
    //    public string oldreviewdetail { get; set; }
    //    public Nullable<int> isreview { get; set; }
    //    public string createddate { get; set; }
    //    public Nullable<int> createdby_old { get; set; }
    //    public Nullable<System.DateTime> modifieddate { get; set; }
    //    public Nullable<int> modifiedby { get; set; }
    //    public string businessname { get; set; }
    //    public Nullable<System.DateTime> reviewdate { get; set; }
    //    public Nullable<int> reviewedby { get; set; }
    //    public Nullable<int> oldreviewid { get; set; }
    //    public Nullable<int> isduplicate { get; set; }
    //    public Nullable<int> membership { get; set; }
    //    public Nullable<int> userid { get; set; }
    //    public Nullable<int> createdby { get; set; }
    //    public int isnew { get; set; }
    //    public Nullable<int> vcid { get; set; }
    //    public Nullable<int> stateid { get; set; }
    //    public Nullable<int> cityid { get; set; }
    //    public Nullable<int> olduserid { get; set; }
    //    public Nullable<int> cybercafe { get; set; }
    //    public Nullable<int> lga { get; set; }
    //    public string Source { get; set; }
    //    public string UserPhoto { get; set; }
    //}

    public class BizReview
    {
        public Int32 reviewpostedby { get; set; }
        public string UserPhoto { get; set; }
        public int contentid { get; set; }
        public int userid { get; set; }
        public string reviewdetail { get; set; }
        public string createddate { get; set; }
        public int avgrating { get; set; }
        public int rating { get; set; }
        public int helpful { get; set; }
        public int inaccurate { get; set; }
        public string UserName { get; set; }
        public string location { get; set; }
        public string email { get; set; }
        public int flag { get; set; }
        public Nullable<long> businessid { get; set; }
        public string businessname { get; set; }
        public Nullable<int> reviewedby { get; set; }
        public Nullable<int> createdby { get; set; }
        public int? isfollowed { get; set; }
        public string userurl { get; set; }
    }
    public class BizReviewTotalRecords
    {
        public Int32 TotalRecords { get; set; }
    }
    public class ReviewerDetails
    {
        public string contactname { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public int usertype { get; set; }
        public int? emailsetting { get; set; }
        public string followedid { get; set; }
        public List<Userfollowersuggestion> listfollowersug { get; set; }
        public List<Userfollowersuggestion> DBlistfollowersug(int userid)
        {
            List<Userfollowersuggestion> listfollowersug = new List<Userfollowersuggestion>();

            using (var db = new VconnectDBContext29())
            {
                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "prc_followsuggestions_url";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@userid", userid));
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    listfollowersug = ((IObjectContextAdapter)db).ObjectContext.Translate<Userfollowersuggestion>(reader).ToList().GetRange(0, 4);
                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);
                }
                finally { db.Database.Connection.Close(); }
            }
            return listfollowersug;
        }
    }

    public class BizReviewDetails
    {
        public List<BizReview> businessReview { get; set; }
        public List<BizReviewTotalRecords> totalRecords { get; set; }
    }

    public class Reportincorrect
    {
        public Int64 contentid { get; set; }
        public long? businessid { get; set; }
        public string businessname { get; set; }
        public Int64 userid { get; set; }
        public string username { get; set; }
        [Required(ErrorMessage = "Please Enter the Description")]
        public string reportincorrectdetail { get; set; }
        public int? isreview { get; set; }
        public DateTime createddate { get; set; }
        public int? createdby { get; set; }
        public Nullable<DateTime> modifieddate { get; set; }
        public int modifiedby { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string remarks { get; set; }
        public string source { get; set; }
        public int? offerid { get; set; }
    }
    public class SmeEventMaster
    {

        public string timeslot1 { get; set; }
        public string timeslot2 { get; set; }
        public string timeslot3 { get; set; }
        public string timeslot4 { get; set; }
        public long contentid { get; set; }
        public string eventdate { get; set; }
        public string eventaddress { get; set; }
        public int uid { get; set; }
        public string companyname { get; set; }
        public string contant { get; set; }
        public string businessaddress { get; set; }
        public int designation { get; set; }
        public string email { get; set; }
        public int stateid { get; set; }
        public int cityid { get; set; }
        public string phone { get; set; }
        public string timeslot { get; set; }
        public string stts { get; set; }
    }
    #endregion

    public class BizDetailInfo
    {
        public string contactno { get; set; }
        public string address { get; set; }
        public string fulladdress { get; set; }
        public string businessname { get; set; }
    }

    public class BizDetailPhotoAttachments
    {
        public string bizname { get; set; }
        public string bizid { get; set; }
        public string photo { get; set; }
        public string thumb { get; set; }
        public string url { get; set; }
        public string area { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string address1 { get; set; }
        public string houseno { get; set; }
        public string title { get; set; }
        public string description { get; set; }
    }
    #region WAP Code
    //public class BizDetailWAP : VconnectDBContext29
    //{
    //    public int bizID { get; set; }
    //    public int Rate { get; set; }
    //    public string Review { get; set; }
    //    public int prc_wap_add_ratereviews_new(Int64 BID, int rate, string review, string name, string email, string mobile, Int64 userid, string ipaddress, string pageurl)
    //    {
    //        int result = 0;
    //        using (var db = new VconnectDBContext29())
    //        {
    //            var cmd = db.Database.Connection.CreateCommand();
    //            cmd.CommandText = "[dbo].[prc_wap_add_ratereviews_new]";
    //            cmd.CommandType = System.Data.CommandType.StoredProcedure;
    //            //Add parameters to command object
    //            cmd.Parameters.Add(new SqlParameter("@BusinessID", BID));
    //            cmd.Parameters.Add(new SqlParameter("@Rate", rate));
    //            cmd.Parameters.Add(new SqlParameter("@Review", review));
    //            cmd.Parameters.Add(new SqlParameter("@Name", name));
    //            cmd.Parameters.Add(new SqlParameter("@Email", email));
    //            cmd.Parameters.Add(new SqlParameter("@Phone", mobile));
    //            cmd.Parameters.Add(new SqlParameter("@Status", "0"));
    //            cmd.Parameters.Add(new SqlParameter("@CreatedDate", DateTime.Now));
    //            cmd.Parameters.Add(new SqlParameter("@UserID", userid));
    //            cmd.Parameters.Add(new SqlParameter("@IPAddress", ipaddress));
    //            cmd.Parameters.Add(new SqlParameter("@PageURL", pageurl));
    //            cmd.Parameters.Add(new SqlParameter("@func", "Businesss.RateReview.Add_new"));
    //            cmd.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int));
    //            cmd.Parameters["@ErrorCode"].Direction = ParameterDirection.Output;
    //            // cmd.Parameters.Add(new SqlParameter("@ErrorCode", "0"));
    //            cmd.Parameters.Add(new SqlParameter("@NewID", SqlDbType.Int));
    //            cmd.Parameters["@NewID"].Direction = ParameterDirection.Output;



    //            try
    //            {
    //                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
    //                connection.Open();
    //                cmd.ExecuteNonQuery();
    //                //  reviewid = cmd.Parameters["@NewID"].Value.ToString();
    //                result = Convert.ToInt16(cmd.Parameters["@ErrorCode"].Value.ToString());
    //                //return Content("form is submitted successfully..");

    //            }
    //            catch (Exception ex)
    //            {
    //            }
    //            finally
    //            {
    //                db.Database.Connection.Close();
    //            }
    //        }
    //        return result;
    //    }
    //}
    #endregion

    public class CategoryWidget
    {
        public Int32 contentid { get; set; }
        public string url { get; set; }
    }
    public class BusinessLead
    {
        public int leadid { get; set; }
        public string leadname { get; set; }
        public int activecnt { get; set; }
        public string title { get; set; }
        public string dscription { get; set; }
        public string sussesssms { get; set; }
        public string buttontxt { get; set; }
        public string titlemodal { get; set; }
        public string buttontxtmodal { get; set; }
        public int bizsubscriptionid { get; set; }
    }
    public class OpenAccount
    {
        public int contentid { get; set; }
        //public int bizid { get; set; }
        public int leadid { get; set; }
        //public int Qid { get; set; }
        public string Qname { get; set; }
        public string Qtype { get; set; }
        public string compalsary { get; set; }
        public string validations { get; set; }
        public string uniquetype { get; set; }
        public string answers { get; set; }
        public DateTime creadeddate { get; set; }
        public int createdby { get; set; }
        public string active { get; set; }
        public DateTime modifydate { get; set; }
        //public int modifyby { get; set; }
        public int qorder { get; set; }
        public int length { get; set; }
        public string leadname { get; set; }
        public string columnname { get; set; }
        public string uniquename { get; set; }
        public string placeholder { get; set; }
        public string uniquemessage { get; set; }
        public string referenceid { get; set; }
    }
    public class BizDetailWEB : VconnectDBContext29
    {

        public int Follow(Int64 bizId, Int64 ReviewId, int? userId, int? status, int PostedbyId)
        {
            int result;
            List<BizReviewTotalRecords> bizReviewTotalRecords = new List<BizReviewTotalRecords>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_followthisreviewer_new]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ReviewId", ReviewId));
                cmd.Parameters.Add(new SqlParameter("@PostedbyId", PostedbyId));
                cmd.Parameters.Add(new SqlParameter("@FollowedId", userId));
                cmd.Parameters.Add(new SqlParameter("@bizid", bizId));
                cmd.Parameters.Add(new SqlParameter("@status", status));
                SqlParameter err = new SqlParameter("@Err", SqlDbType.Int);
                err.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(err);


                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteReader();
                result = (int)cmd.Parameters["@Err"].Value;
            }
            return result;
        }
        public ReviewerDetails getReviewerDetails(int PostedbyId, int followedby, int notificationid)
        {
            ReviewerDetails result = new ReviewerDetails();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_getReviewerDetailsSept_beta]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PostedbyId", PostedbyId));
                cmd.Parameters.Add(new SqlParameter("@followedid", followedby));
                cmd.Parameters.Add(new SqlParameter("@notificationid", notificationid));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();
                // reader.NextResult();
                result = ((IObjectContextAdapter)db).ObjectContext.Translate<ReviewerDetails>(reader).ToList().FirstOrDefault();
            }
            return result;
        }

        public void ReportIncorrect(Reportincorrect reportincorrect, string userid)
        {
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_businessreportincorrect]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@businessid", reportincorrect.businessid));

                cmd.Parameters.Add(new SqlParameter("@email", reportincorrect.email));

                cmd.Parameters.Add(new SqlParameter("@phone", reportincorrect.phone));
                cmd.Parameters.Add(new SqlParameter("@name", reportincorrect.username));
                cmd.Parameters.Add(new SqlParameter("@reportincorrectdetail", reportincorrect.reportincorrectdetail + reportincorrect.remarks));
                cmd.Parameters.Add(new SqlParameter("@useridentity", userid));

                SqlParameter err = new SqlParameter("@Err", SqlDbType.Int);
                err.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(err);

                SqlParameter outpassword = new SqlParameter("@OUTPassword", SqlDbType.VarChar, 100);
                outpassword.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outpassword);

                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();

                var Err = cmd.Parameters["@Err"].Value;
                var OUTPassword = cmd.Parameters["@OUTPassword"].Value;
                var result = (int)cmd.Parameters["@Err"].Value;
            }
        }

        public IList<SmeEventMaster> GetSmeEvent(int? eventId)
        {
            var smeEvent = new List<SmeEventMaster>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_wap_get_smeevent]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@eventid", eventId));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();
                // reader.NextResult();
                smeEvent = ((IObjectContextAdapter)db).ObjectContext.Translate<SmeEventMaster>(reader).ToList();
            }

            return smeEvent;
        }

        public void AddSmeEvent(SmeEventMaster smeeventmaster)
        {
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_wap_add_smeevent]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@uid", smeeventmaster.uid));
                cmd.Parameters.Add(new SqlParameter("@eventid", smeeventmaster.contentid));
                cmd.Parameters.Add(new SqlParameter("@companyname", smeeventmaster.companyname));
                cmd.Parameters.Add(new SqlParameter("@contant", smeeventmaster.contant));
                cmd.Parameters.Add(new SqlParameter("@businessaddress", smeeventmaster.businessaddress));
                cmd.Parameters.Add(new SqlParameter("@designation", smeeventmaster.designation));
                cmd.Parameters.Add(new SqlParameter("@email", smeeventmaster.email));
                cmd.Parameters.Add(new SqlParameter("@stateid", smeeventmaster.stateid));
                cmd.Parameters.Add(new SqlParameter("@cityid", smeeventmaster.cityid));
                cmd.Parameters.Add(new SqlParameter("@phone", smeeventmaster.phone));
                cmd.Parameters.Add(new SqlParameter("@timeslot", smeeventmaster.timeslot));
                cmd.Parameters.Add(new SqlParameter("@stts", "out"));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();
                // reader.NextResult(); 
            }

        }
        public CategoryWidget getWidgetContentid(string widgeturl)
        {
            int IDurl = 0;
            string url = "0";
            int IDurl1 = 0;
            CategoryWidget categoryWidget = new CategoryWidget();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_getWidgetContentid]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                if (int.TryParse(widgeturl, out IDurl1))
                {
                    cmd.Parameters.Add(new SqlParameter("@contentid", widgeturl));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@widgeturl", widgeturl));
                }
                cmd.Parameters.Add(new SqlParameter("@errorcode", SqlDbType.VarChar, 100));
                cmd.Parameters["@errorcode"].Direction = ParameterDirection.Output;

                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                var reader = cmd.ExecuteReader();

                categoryWidget = ((IObjectContextAdapter)db).ObjectContext.Translate<CategoryWidget>(reader).FirstOrDefault();
            }
            return categoryWidget;
        }



        public BizDetails clentdesign(Int64? leadid, string widgettype)
        {
            BizDetails Rst = new BizDetails();
            List<OpenAccount> openact = new List<OpenAccount>();
            BusinessLead businesslead = new BusinessLead();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[Client_Questionnaire]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@leadid", leadid));
                cmd.Parameters.Add(new SqlParameter("@widgettype", widgettype));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    openact = ((IObjectContextAdapter)db).ObjectContext.Translate<OpenAccount>(reader).ToList();

                    reader.NextResult();
                    businesslead = ((IObjectContextAdapter)db).ObjectContext.Translate<BusinessLead>(reader).FirstOrDefault();
                    Rst.businesslead = businesslead;

                    Rst.openaccount = openact;

                }
                catch (Exception ex)
                {
                }
                finally
                {
                    //db.Database.Connection.Close();
                }
            }
            return Rst;
        }
        public BusinessLead GetBizWidget(Int64? businessId)
        {
            BusinessLead businessLead = new BusinessLead();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_businesswidget]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", businessId));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();
                businessLead = ((IObjectContextAdapter)db).ObjectContext.Translate<BusinessLead>(reader).FirstOrDefault();
            }

            return businessLead;
        }

        public BizDetails GetBizDetails(Int64 businessId, Int64? userId = null)
        {
            BizDetails bizDetails = new BizDetails();
            List<BizDetailWeb> bizMaster = new List<BizDetailWeb>();
            //List<BizWorkingHour> bizWorkingHour = new List<BizWorkingHour>();
            //List<BizBranches> bizBranches = new List<BizBranches>();
            //List<BizAttachment> bizAttachment = new List<BizAttachment>();
            List<BizProduct> bizProduct = new List<BizProduct>();
            List<BizService> bizService = new List<BizService>();
            BusinessURL businessUrl = new BusinessURL();
            BusinessLead businesslead = new BusinessLead();
            //List<BizPhotoCount> bizPhotoCount = new List<BizPhotoCount>();
            //List<BizPhotoGallery> bizPhotoGallery = new List<BizPhotoGallery>();
            //List<BizVideo> bizVideo = new List<BizVideo>();
            //List<BizPhotoContact> bizPhotoContact = new List<BizPhotoContact>();
            //List<BizProductInfo> bizProductInfo = new List<BizProductInfo>();
            //List<BizServiceInfo> bizServiceInfo = new List<BizServiceInfo>();
            //List<BizVideoInfo> bizVideoInfo = new List<BizVideoInfo>();

            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_businessdetailsnew_test_beta]";
                cmd.CommandTimeout = 600;
                //cmd.CommandText = "[dbo].[prc_get_businessdetailsnew_test]";
                //cmd.CommandText = "[dbo].[prc_get_businessdetailsnew]";

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", businessId));
                cmd.Parameters.Add(new SqlParameter("@oldbusinessid", 0));
                cmd.Parameters.Add(new SqlParameter("@vcid", 0));
                if (userId != null && userId != 0)
                {
                    cmd.Parameters.Add(new SqlParameter("@userid", userId));
                }
                // try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();
                    bizMaster = ((IObjectContextAdapter)db).ObjectContext.Translate<BizDetailWeb>(reader).ToList();

                    //reader.NextResult();
                    //bizWorkingHour = ((IObjectContextAdapter)db).ObjectContext.Translate<BizWorkingHour>(reader).ToList();

                    //reader.NextResult();
                    //bizBranches = ((IObjectContextAdapter)db).ObjectContext.Translate<BizBranches>(reader).ToList();

                    //reader.NextResult();
                    //bizAttachment = ((IObjectContextAdapter)db).ObjectContext.Translate<BizAttachment>(reader).ToList();
                    
                    reader.NextResult();
                    bizProduct = ((IObjectContextAdapter)db).ObjectContext.Translate<BizProduct>(reader).ToList();

                    reader.NextResult();
                    bizService = ((IObjectContextAdapter)db).ObjectContext.Translate<BizService>(reader).ToList();

                    reader.NextResult();
                    businessUrl = ((IObjectContextAdapter)db).ObjectContext.Translate<BusinessURL>(reader).FirstOrDefault();
                    //reader.NextResult();
                    //bizPhotoCount = ((IObjectContextAdapter)db).ObjectContext.Translate<BizPhotoCount>(reader).ToList();

                    //reader.NextResult();
                    //bizPhotoGallery = ((IObjectContextAdapter)db).ObjectContext.Translate<BizPhotoGallery>(reader).ToList();

                    //reader.NextResult();
                    //bizVideo = ((IObjectContextAdapter)db).ObjectContext.Translate<BizVideo>(reader).ToList();

                    //reader.NextResult();
                    //bizPhotoContact = ((IObjectContextAdapter)db).ObjectContext.Translate<BizPhotoContact>(reader).ToList();

                    //reader.NextResult();
                    //bizProductInfo = ((IObjectContextAdapter)db).ObjectContext.Translate<BizProductInfo>(reader).ToList();

                    //reader.NextResult();
                    //bizServiceInfo = ((IObjectContextAdapter)db).ObjectContext.Translate<BizServiceInfo>(reader).ToList();

                    //reader.NextResult();
                    //bizVideoInfo = ((IObjectContextAdapter)db).ObjectContext.Translate<BizVideoInfo>(reader).ToList();
                    reader.NextResult();
                    businesslead = ((IObjectContextAdapter)db).ObjectContext.Translate<BusinessLead>(reader).FirstOrDefault();
                    bizDetails.businesslead = businesslead;
                    if (bizDetails.businesslead != null && bizDetails.businesslead.leadid != 0 && bizDetails.businesslead.leadid != null)
                    {
                        //for Lead display
                        //bizDetails = clentdesign(bizDetails.businesslead.leadid, "business");
                        bizDetails = clentdesign(businessId, "business");
                    }
                    bizDetails.BizMaster = bizMaster;
                    //bizDetails.BizWorkingHour = bizWorkingHour;
                    //bizDetails.BizBranches = bizBranches;
                    //bizDetails.BizAttachment = bizAttachment;
                    bizDetails.BizProduct = bizProduct;
                    bizDetails.BizService = bizService;
                    bizDetails.BusinessURL = businessUrl;
                    //bizDetails.BizPhotoCount = bizPhotoCount;
                    //bizDetails.BizPhotoGallery = bizPhotoGallery;
                    //bizDetails.BizVideo = bizVideo;
                    //bizDetails.BizPhotoContact = bizPhotoContact;
                    //bizDetails.BizProductInfo = bizProductInfo;
                    //bizDetails.BizServiceInfo = bizServiceInfo;
                    //bizDetails.BizVideoInfo = bizVideoInfo;
                    bizDetails.businesslead = businesslead;

                }
                // catch (Exception ex)
                {
                }
                // finally
                {
                    //db.Database.Connection.Close();
                }
            }
            //return result;
            return bizDetails;
        }
        #region Wap Code
        //public BizDetailsWapRst GetBizDetailWap(int businessId, int? userId)//, Int64 userId, string prvurl, string url, string rvsdns, string ip, string useragent)
        //{
        //    BizDetailsWapRst bizDetailsWapRst = new BizDetailsWapRst();
        //    using (var db = new VconnectDBContext29())
        //    {
        //        var cmd = db.Database.Connection.CreateCommand();
        //        cmd.CommandText = "[dbo].[prc_wap_get_businessdetails_newbeta]";
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        cmd.Parameters.Add(new SqlParameter("@businessid", businessId));
        //        cmd.Parameters.Add(new SqlParameter("@userid", userId));
        //        try
        //        {
        //            var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
        //            connection.Open();

        //            var reader = cmd.ExecuteReader();
        //            bizDetailsWapRst.bizDetail = ((IObjectContextAdapter)db).ObjectContext.Translate<BizDetail>(reader).ToList();
        //            reader.NextResult();

        //            bizDetailsWapRst.bizWorkingHour = ((IObjectContextAdapter)db).ObjectContext.Translate<BizWorkingHour>(reader).FirstOrDefault();
        //            reader.NextResult();
        //            bizDetailsWapRst.totalreview = ((IObjectContextAdapter)db).ObjectContext.Translate<int>(reader).FirstOrDefault();
        //            reader.NextResult();
        //            bizDetailsWapRst.latestReview = ((IObjectContextAdapter)db).ObjectContext.Translate<LatestReview>(reader).ToList();
        //            reader.NextResult();
        //            //bizDetailsWapRst.status = ((IObjectContextAdapter)db).ObjectContext.Translate<int>(reader).FirstOrDefault();
        //            // reader.NextResult();
        //            bizDetailsWapRst.serviceName = ((IObjectContextAdapter)db).ObjectContext.Translate<ServiceName>(reader).ToList();
        //            reader.NextResult();
        //            bizDetailsWapRst.productName = ((IObjectContextAdapter)db).ObjectContext.Translate<ProductName>(reader).ToList();
        //            reader.NextResult();
        //            bizDetailsWapRst.Photothumb = ((IObjectContextAdapter)db).ObjectContext.Translate<String>(reader).ToList();

        //        }
        //        catch (Exception ex)
        //        {

        //        }
        //        finally
        //        {
        //            db.Database.Connection.Close();
        //        }
        //    }
        //    return bizDetailsWapRst;
        //}
        #endregion

        public IList<BizDetailWeb> GetRelatedBiz(Int64? bizId)
        {
            List<BizDetailWeb> similarBiz = new List<BizDetailWeb>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_similarBiz]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@bussinessId", bizId));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();

                similarBiz = ((IObjectContextAdapter)db).ObjectContext.Translate<BizDetailWeb>(reader).ToList();
            }

            return similarBiz;
        }

        public BizReviewDetails GetBizReviewDetails(Int64 bizId, Int64 contentId, int? pageNum, int? rowsPerPage)
        {
            BizReviewDetails bizReviewDetails = new BizReviewDetails();
            List<BizReview> bizReview = new List<BizReview>();
            List<BizReviewTotalRecords> bizReviewTotalRecords = new List<BizReviewTotalRecords>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                //cmd.CommandText = "[dbo].[prc_get_businessreviews_beta]";
                cmd.CommandText = "[dbo].[prc_get_businessreviews_live]";                
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", bizId));
                cmd.Parameters.Add(new SqlParameter("@pageNum", pageNum));
                cmd.Parameters.Add(new SqlParameter("@rowsPerPage", rowsPerPage));
                cmd.Parameters.Add(new SqlParameter("@contentid", contentId));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();
                //bizReview = ((IObjectContextAdapter)db).ObjectContext.Translate<BizReview>(reader).ToList();
                bizReview = ((IObjectContextAdapter)db).ObjectContext.Translate<BizReview>(reader).Where(x => x.UserName != null && x.UserName != "").ToList();

                reader.NextResult();
                bizReviewTotalRecords = ((IObjectContextAdapter)db).ObjectContext.Translate<BizReviewTotalRecords>(reader).ToList();

                bizReviewDetails.businessReview = bizReview;
                bizReviewDetails.totalRecords = bizReviewTotalRecords;

            }

            return bizReviewDetails;
        }
        #region Business Claim
        public BusinessClaim GetBusinessClaim(long businessId)
        {
            var businessClaim = new BusinessClaim();           
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_businessclaim]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", businessId));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();
                businessClaim = ((IObjectContextAdapter)db).ObjectContext.Translate<BusinessClaim>(reader).FirstOrDefault();

            }
            return businessClaim;
        }
        #endregion

        public BusinessPhone GetPhone(long businessId)
        {
            var businessPhone = new BusinessPhone();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_businessphonenumber]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", businessId));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();
                businessPhone = ((IObjectContextAdapter)db).ObjectContext.Translate<BusinessPhone>(reader).FirstOrDefault();

            }
            return businessPhone;
        }

        #region Gallery
        public IList<BusinessPhotoGallery> GetPhotoGallery(long businessId)
        {
            var bizPhotoGallery = new List<BusinessPhotoGallery>();
            List<string> photo = new List<string>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_businessphotos_beta]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", businessId));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();
                bizPhotoGallery = ((IObjectContextAdapter)db).ObjectContext.Translate<BusinessPhotoGallery>(reader).ToList();

            }
            return bizPhotoGallery;
        }

        public IList<string> GetVideoGallery(Int64 businessId)
        {
            List<string> Video = new List<string>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[Usp_GetVideo]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", businessId));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();
                var bizVideo = ((IObjectContextAdapter)db).ObjectContext.Translate<BizVideo>(reader).ToList();
                Video = bizVideo.Select(x => x.VideoEmbeddedPath).ToList();

            }
            return Video;
        }
        public IList<BizAttachmentDetail> GetAttachmentGallery(Int64 businessId)
        {
            var bizAttachmentGallery = new List<BizAttachmentDetail>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_businessattachments_beta]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", businessId));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();
                bizAttachmentGallery = ((IObjectContextAdapter)db).ObjectContext.Translate<BizAttachmentDetail>(reader).ToList();
                //bizAttachmentGallery = bizAttchment.Select(x => x.attachments).ToList();

            }
            return bizAttachmentGallery;
        }

        #endregion

        /// <summary>
        /// Inserts data into the report incorrect table
        /// </summary>
        /// <param name="reportincorrect">report incorrect object</param> prc_add_businessreviews
        public int ReportIncorrect(string remarks,string whatswrong, string detail, string email, int bizId, int userid)
        {
            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_businessreportincorrect_Live]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@businessid", bizId));
                cmd.Parameters.Add(new SqlParameter("@email", email));
                cmd.Parameters.Add(new SqlParameter("@phone", ""));
                cmd.Parameters.Add(new SqlParameter("@name", ""));
                cmd.Parameters.Add(new SqlParameter("@whatswrong", whatswrong));                
                cmd.Parameters.Add(new SqlParameter("@reportincorrectdetail", detail + remarks));
                cmd.Parameters.Add(new SqlParameter("@useridentity", 1));
                //cmd.Parameters.Add(new SqlParameter("@Err", 0));
                //cmd.Parameters.Add(new SqlParameter("@OUTPassword", ""));
                SqlParameter err = new SqlParameter("@Err", SqlDbType.Int);
                err.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(err);

                SqlParameter outpassword = new SqlParameter("@OUTPassword", SqlDbType.VarChar, 100);
                outpassword.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outpassword);

                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();

                var Err = cmd.Parameters["@Err"].Value;
                var OUTPassword = cmd.Parameters["@OUTPassword"].Value;
                result = (int)cmd.Parameters["@Err"].Value;
            }

            return result;
        }

        /// <summary>
        /// TO add new review from business detail page
        /// </summary>
        /// <param name="businessid"></param>
        /// <param name="pagetype"></param>
        /// <param name="pageNum"></param>
        /// <param name="rowsPerPage"></param>
        /// <returns></returns>
        public void addbusinessreviews(BizReview bizReview)
        {
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_businessreviews]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", bizReview.businessid));
                cmd.Parameters.Add(new SqlParameter("@businessname", bizReview.businessname));
                cmd.Parameters.Add(new SqlParameter("@userid", bizReview.userid));
                cmd.Parameters.Add(new SqlParameter("@useridentity", bizReview.userid));
                cmd.Parameters.Add(new SqlParameter("@reviewtitle", "Review"));
                cmd.Parameters.Add(new SqlParameter("@reviewdetail", bizReview.reviewdetail));
                cmd.Parameters.Add(new SqlParameter("@stateid", 2));
                cmd.Parameters.Add(new SqlParameter("@cityid", 1));
                cmd.Parameters.Add(new SqlParameter("@cybercafe", 1));
                cmd.Parameters.Add(new SqlParameter("@lga", 0));
                cmd.Parameters.Add(new SqlParameter("@isreview", 0));
                cmd.Parameters.Add(new SqlParameter("@Err", SqlDbType.Int).Direction);
                cmd.Parameters.Add(new SqlParameter("@OUTPassword", SqlDbType.VarChar, 1000).Direction);
                cmd.Parameters.Add(new SqlParameter("@OUTBusinessReviewContentID", SqlDbType.VarChar, 1000).Direction);

                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();
            }
        }
        public int updateBusinessClicktoRate(Int32 bizid, Int32 userid)
        {

            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_update_businessclicktorate]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@bizid", bizid));
                cmd.Parameters.Add(new SqlParameter("@ipaddress", Vconnect.Common.MyGlobalVariables.GetIpAddress));
                cmd.Parameters.Add(new SqlParameter("@Err", ParameterDirection.Output));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();
                result = (int)cmd.Parameters["@Err"].Value;
            }
            return result;
        }

        public int updatebusinessreviews(Int64 reviewId, Int64 userId)
        {
            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_update_businessreviews]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object

                cmd.Parameters.Add(new SqlParameter("@userid", userId));
                cmd.Parameters.Add(new SqlParameter("@reviewid", reviewId));
                cmd.Parameters.Add(new SqlParameter("@Err", ParameterDirection.Output));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();
                result = (int)cmd.Parameters["@Err"].Value;
            }
            return result;
        }


        public int updatebusinessphoto(Int64 photoID, Int64 userId)
        {
            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_update_photo_contentid]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@contentid", photoID));
                cmd.Parameters.Add(new SqlParameter("@userid", userId));
                cmd.Parameters.Add(new SqlParameter("@Err", ParameterDirection.Output));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();
                result = (int)cmd.Parameters["@Err"].Value;
            }
            return result;
        }
        public string addbusinessreviews(Int64 bizId, Int64 userId, string Review, string bizName)
        {
            string result;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_businessreviews]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", bizId));
                cmd.Parameters.Add(new SqlParameter("@businessname", bizName));
                cmd.Parameters.Add(new SqlParameter("@userid", userId));
                cmd.Parameters.Add(new SqlParameter("@useridentity", userId));
                cmd.Parameters.Add(new SqlParameter("@reviewtitle", "Review"));
                cmd.Parameters.Add(new SqlParameter("@reviewdetail", Review));
                cmd.Parameters.Add(new SqlParameter("@stateid", 2));
                cmd.Parameters.Add(new SqlParameter("@cityid", 1));
                cmd.Parameters.Add(new SqlParameter("@cybercafe", 1));
                cmd.Parameters.Add(new SqlParameter("@lga", 0));
                cmd.Parameters.Add(new SqlParameter("@isreview", 0));
                //cmd.Parameters.Add(new SqlParameter("@Err", ParameterDirection.Output));
                //cmd.Parameters.Add(new SqlParameter("@OUTPassword", ParameterDirection.Output));
                //cmd.Parameters.Add(new SqlParameter("@OUTBusinessReviewContentID", ParameterDirection.Output));
                SqlParameter err = new SqlParameter("@Err", SqlDbType.Int);

                err.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(err);

                SqlParameter outpassword = new SqlParameter("@OUTPassword", SqlDbType.VarChar, 100);
                outpassword.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(outpassword);

                SqlParameter OUTBusinessReviewContentID = new SqlParameter("@OUTBusinessReviewContentID", SqlDbType.VarChar, 1000);
                OUTBusinessReviewContentID.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(OUTBusinessReviewContentID);
                //var err = new SqlParameter("@Err", SqlDbType.Int);
                //err.Direction = ParameterDirection.Output;
                //cmd.Parameters.Add(err);


                //var oUTPassword = new SqlParameter("@OUTPassword", SqlDbType.VarChar);
                //oUTPassword.Direction = ParameterDirection.Output;
                //cmd.Parameters.Add(oUTPassword);

                //var oUTBusinessReviewContentID = new SqlParameter("@OUTBusinessReviewContentID", SqlDbType.VarChar);
                //oUTBusinessReviewContentID.Direction = ParameterDirection.Output;
                //cmd.Parameters.Add(oUTBusinessReviewContentID);

                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();
                var Err = cmd.Parameters["@Err"].Value;
                var OUTPassword = cmd.Parameters["@OUTPassword"].Value;
                // string OUTBusinessReviewContentID = cmd.Parameters["@OUTBusinessReviewContentID"].Value.ToString();
                result = (string)cmd.Parameters["@OUTBusinessReviewContentID"].Value;

                //cmd.ExecuteReader();


            }
            return result;
        }


        public BizPhoneNumbers GetBizPhoneNumbers(Int64 bizId)
        {
            BizPhoneNumbers bizPhoneNumbers = new BizPhoneNumbers();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[Prc_get_BusinessPhoneNumbers]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", bizId));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();
                // reader.NextResult();
                bizPhoneNumbers = ((IObjectContextAdapter)db).ObjectContext.Translate<BizPhoneNumbers>(reader).FirstOrDefault();
            }

            return bizPhoneNumbers;
        }

        public void IsHelpFull(Int64 bizId, Int64 reviewId, int? userId, int? ishelp)
        {
            List<BizReviewTotalRecords> bizReviewTotalRecords = new List<BizReviewTotalRecords>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_ishelpfull]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ReviewId", reviewId));
                cmd.Parameters.Add(new SqlParameter("@userid", userId));
                cmd.Parameters.Add(new SqlParameter("@bizid", bizId));
                cmd.Parameters.Add(new SqlParameter("@ishelpfoll", ishelp));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteReader();

            }

        }

        public int addbusinessphotos(int businessid, int contentid, int userid, string businessname, string photos)
        {
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_PhotosToBusiness]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                cmd.Parameters.Add(new SqlParameter("@contentid", contentid));
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@businessname", businessname));
                cmd.Parameters.Add(new SqlParameter("@photo", photos));
                cmd.Parameters.Add(new SqlParameter("@Err", ParameterDirection.Output));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();
                int result = (int)cmd.Parameters["@Err"].Value;
                return result;
            }
        }

        public int wasthishelpful(string whichnumber, string status, Int64 bizid, Int64 userid)
        {
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_wasthishelpful]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@businessid", bizid));
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@wasthishelpful", status));
                cmd.Parameters.Add(new SqlParameter("@whichnumber", whichnumber));                
                cmd.Parameters.Add(new SqlParameter("@Err", ParameterDirection.Output));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();
                int result = (int)cmd.Parameters["@Err"].Value;
                return result;
            }
        }

        public string addbusinessRate(Int32 bizId, Int32 userId, Int16? rate, Int32 reviewid)
        {
            string result = string.Empty;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[usp_AddUpdRatings_beta]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", bizId));
                cmd.Parameters.Add(new SqlParameter("@UserId", userId));
                cmd.Parameters.Add(new SqlParameter("@Rating", rate));
                cmd.Parameters.Add(new SqlParameter("@source", "WEB"));
                cmd.Parameters.Add(new SqlParameter("@reviewid", reviewid));
                cmd.Parameters.Add(new SqlParameter("@ipaddress", Vconnect.Common.MyGlobalVariables.GetIpAddress));
                //cmd.Parameters.Add(new SqlParameter("@errMsg", 0));
                cmd.Parameters.Add(new SqlParameter("@errMsg", ParameterDirection.Output));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();
                result = cmd.Parameters["@errMsg"].Value.ToString();
                //return result;
            }
            return result;
        }

        public BizDetailInfo GetBizDetailInfo(Int64 bizId)
        {
            BizDetailInfo bizDetailInfo = new BizDetailInfo();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_business_Info]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", bizId));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();
                // reader.NextResult();
                bizDetailInfo = ((IObjectContextAdapter)db).ObjectContext.Translate<BizDetailInfo>(reader).FirstOrDefault();
            }

            return bizDetailInfo;
        }

        public int UpdateReviewWithUserid(Int64 userid, Int64 reviewid)
        {
            int result = 0;
            //prc_update_businessreviews


            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_update_businessreviews]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@reviewid", reviewid));
                SqlParameter err = new SqlParameter("@Err", SqlDbType.Int);
                err.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(err);

                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();

                var Err = cmd.Parameters["@Err"].Value;
                result = (int)cmd.Parameters["@Err"].Value;
            }

            return result;
        }

        public int getSEOMetaDetails(string PageFlag, int id)
        {
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_ui_seodetails]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@PageFlag", PageFlag));
                cmd.Parameters.Add(new SqlParameter("@Id", id));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();
                int result = (int)cmd.Parameters["@Err"].Value;
                return result;
            }
        }

    }
    #region Dynamic Design
    public class RegisterUserClients
    {
        [Key]
        public int Error { get; set; }
        public int newuserconentid { get; set; }
    }
    public class Listdropbind
    {
        [Key]
        public int drpvalue { get; set; }
        public string drptext { get; set; }
    }
    #endregion
}