﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Vconnect.Enums;



//Created by Nishant Dated 02/january/2014
namespace Vconnect.Models
{
    #region ForgotPassword

    public class SMSGateWayModel
    {
        [Key]
        public string GateWay { get; set; }
        public string Url { get; set; }

    }

    public class ForgotPasswordModel : SearchBar
    {
        [Key]
        public string EmailId { get; set; }
        public string MobileVerificationCode { get; set; }
        public string ConfirmMobileVerificationCode { get; set; }
        public int IsNotMobileVerified { get; set; }
        public string EmailVerificationCode { get; set; }
        public int IsSucceed { get; set; }
        public int IsResend { get; set; }
        public int isVerifiedResendCount { get; set; }
        public string userId { get; set; }
        public string message { get; set; }
        public string status { get; set; }
    }

    public class NewPasswordModel : ForgotPasswordModel
    {
        [Key]
        public string EmailId { get; set; }
        public string VerificationCode { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public int IsSucceed { get; set; }
    }

    #endregion

    #region Vconnect Web Login Classes in use

    public class AccountLoginResultWEB
    {
        public List<UserPassword> ListUserBusiness { get; set; }
        public List<UserDetails> ListUserDetails { get; set; }
        public List<UserBusinessDetails> ListUserBusinessDetails { get; set; }
        public List<UserState> ListUserState { get; set; }
        public List<UserCity> ListUserCity { get; set; }

    }
    [Serializable]
    public class ResultTableAccountBusinessMappingModel
    {

        public List<UserAccountMappingModel> UserAccountMappingModel { get; set; }
        public List<UserAccountMappingBusinessesModel> UserAccountMappingBusinessesModel { get; set; }

    }

    public class UserAccountMappingModel
    {

        [Key]
        public int? ContentId { get; set; }
        public string UserId { get; set; }
        public string UserLoginId { get; set; }
        public string ContactName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Status { get; set; }
        public int BizClaimed { get; set; }
        public bool IsEmailVerified { get; set; }
        public bool IsMobileVerified { get; set; }

    }

    public class UserAccountMappingBusinessesModel
    {
        [Key]
        public string BusinessName { get; set; }
        public string BizPhone { get; set; }
        public string BizEmail { get; set; }
        public int UserContentId { get; set; }
        public string UserEmail { get; set; }
        public string UserPhone { get; set; }

    }

    public class UserPassword
    {
        [Key]
        public string Password { get; set; }
    }

    public class UserDetails
    {
        [Key]
        public int? ContentId { get; set; }
        public string ContactName { get; set; }
        public string lastname { get; set; }
        public int? UserType { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int? isAgent { get; set; }
        public bool IsMobileVerified { get; set; }
        public bool IsEmailVerified { get; set; }
        public string password { get; set; }
        public string VerificationCode { get; set; }
        public string Photo { get; set; }
        public string PhotoMobile { get; set; }
        public string userUrl { get; set; }
    }

    public class UserBusinessDetails
    {
        [Key]
        public string BusinessName { get; set; }
        public string CompanyLogo { get; set; }
        public int ContentId { get; set; }
    }

    public class UserState
    {
        [Key]
        public string StateName { get; set; }
    }

    public class UserCity
    {
        [Key]
        public string CityName { get; set; }
    }

    public class UserHistory
    {
        [Key]
        public int SlNo { get; set; }
        public string CyberCafeName { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class LeadBusiness
    {
        [Key]
        public int BusinessId { get; set; }
        public string BusinessName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public int LocationCode { get; set; }
        public int Membership { get; set; }

    }

    #endregion

    #region Facebook External Login Classes in use

    public class FbUserFriendsList
    {
        public string Id { set; get; }
        public string Name { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string Gender { set; get; }
        public string Link { set; get; }
        public string Source { set; get; }
    }

    public class AccountFacebookResultWEB
    {

        public List<FacebookUserDetails> ListFacebookUserDetails { get; set; }
        public List<FacebookUserBusinessDetails> ListFacebookUserBusinessDetails { get; set; }
        public List<FacebookUserState> ListFacebookUserState { get; set; }
        public List<FacebookUserCity> ListFacebookUserCity { get; set; }

    }

    public class FacebookUserDetails
    {
        [Key]
        public int StateId { get; set; }
        public int CityId { get; set; }
        public int ContentId { get; set; }
        public string ContactName { get; set; }
        public int UserType { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Photo { get; set; }
        public int Error { get; set; }
    }

    public class FacebookUserBusinessDetails
    {
        [Key]
        public string BusinessName { get; set; }
        public string CompanyLogo { get; set; }
        public int ContentId { get; set; }
    }

    public class FacebookUserState
    {
        [Key]
        public string StateName { get; set; }
    }

    public class FacebookUserCity
    {
        [Key]
        public string CityName { get; set; }
    }

    public class CheckEmailAvailability
    {
        public int Count { get; set; }
    }

    #endregion

    #region Twitter External Login Classes in use
    public class TwiterFollowers
    {
        public string TwitterId { get; set; }
        public string ScreenName { get; set; }
        public string ProfileImage { get; set; }
        public string Source { get; set; }

    }

    //public class TwiterFriends
    //{
    //    public string TwitterId { get; set; }
    //    public string ScreenName { get; set; }
    //    public string ProfileImage { get; set; }
    //    public string Source { get; set; }
    //}
    #endregion

    #region Google External Login classes in Use
    public class GoogleResponse
    {
        public string access_token { get; set; }
        public string refresh_token { get; set; }
        public string expires_in { get; set; }
        public string token_type { get; set; }
    }

    public class GoogleOauthParameters
    {
        public string Code { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string RedirectURIs { get; set; }
        public string Rturl { get; set; }
    }

   
    public class GContacts
    {

        public List<FetchContactsModel> _list { get; set; }
    }

    public class FetchContactsModel
    {
        [Key]
        public string Name { get; set; }
        public string EmailId { get; set; }
        public string SecondaryEmail { get; set; }
        public string Phonenumber { get; set; }
        public string Source { get; set; }

    }

    public class GoogleId
    {
        public String id { get; set; }
        public String email { get; set; }
        public Boolean verified_email { get; set; }
        public String name { get; set; }
        public String given_name { get; set; }
        public String family_name { get; set; }
        public String link { get; set; }
        public String picture { get; set; }
        public String gender { get; set; }
        public String locale { get; set; }
        public List<FetchContactsModel> _list { get; set; }
    }

    public class AccountGoogleResultWEB
    {

        public List<GoogleUserDetails> ListGoogleUserDetails { get; set; }
        public List<GoogleUserBusinessDetails> ListGoogleUserBusinessDetails { get; set; }
        public List<GoogleUserState> ListGoogleUserState { get; set; }
        public List<GoogleUserCity> ListGoogleUserCity { get; set; }

    }

    public class GoogleUserDetails
    {
        [Key]
        public int StateId { get; set; }
        public int CityId { get; set; }
        public int ContentId { get; set; }
        public string ContactName { get; set; }
        public int UserType { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Photo { get; set; }
        public int Error { get; set; }
    }

    public class GoogleUserBusinessDetails
    {
        [Key]
        public string BusinessName { get; set; }
        public string CompanyLogo { get; set; }
        public int ContentId { get; set; }
    }

    public class GoogleUserState
    {
        [Key]
        public string StateName { get; set; }
    }

    public class GoogleUserCity
    {
        [Key]
        public string CityName { get; set; }
    }

    #endregion

    #region General for all

    public class UnverifiedUserDetails
    {
        [Key]
        public int UserId { get; set; }
        public string ScreenName { get; set; }
    }
    public class ExternalLoginUserDetails
    {
        [Key]
        public string LoginId { get; set; }
        public int? UserId { get; set; }
        public string ScreenName { get; set; }
        public string Password { get; set; }
        public string LoginEmail { get; set; }
        public int? UserType { get; set; }
        public string Mobile { get; set; }
        public string Photo { get; set; }
        public string PhotoMobile { get; set; }
        public string FacebookUid { get; set; }
        public string GoogleUid { get; set; }
        public string TwitterUid { get; set; }
        public int? IsIncomplete { get; set; }
        public int? IsVerified { get; set; }
        public int? IsActive { get; set; }
        public int? Status { get; set; }
        public bool? IsMobileVerified { get; set; }
        public bool? IsEmailVerified { get; set; }
        public Int64? OldUserId { get; set; }
        public string VerificationCode { get; set; }
        public int? StateId { get; set; }
        public int? CityId { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public int? ContentID { get; set; }
        public string userUrl { get; set; }
    }

    public class RegisterExternalLoginModel : SearchBar
    {
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Display(Name = "ProviderId")]
        public string ProviderId { get; set; }

        [Display(Name = "Provider")]
        public string Provider { get; set; }

        [Display(Name = "userData")]
        public Dictionary<string, string> userData { get; set; }

        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Mobile")]


        public string Gender { get; set; }

        public string Mobile { get; set; }
        public string rurl { get; set; }

        public int IsEmailNull { get; set; }

    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
    [Serializable]
    public class LoginModel : SearchBar
    {
        [Required]
        [Display(Name = "Login")]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        [DefaultValue("HomeWeb")]
        public string Successcontroller
        { get; set; }
        [DefaultValue("HomeWeb")]
        public string Successaction
        { get; set; }

        public int CheckMobileVerification { get; set; }

        public int CheckEmailVerification { get; set; }

        public string MobileVerificationCode { get; set; }

        public string ConfirmMobileVerificationCode { get; set; }

        public string QueryParameterEmailVerificationCode { get; set; }

        public string QueryParameterEmail { get; set; }

        public string PhoneNo { get; set; }

        public int IsPhoneRedirect { get; set; }

        public string IsDuplicate { get; set; }

        public ResultTableAccountBusinessMappingModel ListResultTableAccountBusinessMappingModel { get; set; }

        public string EmailId { get; set; }

        public int isRedirect { get; set; }

        public string BR { get; set; }

        public int BID { get; set; }

        public int UID { get; set; }

        public int isMobile { get; set; }

        public int IsPhonenoNotValid { get; set; }

        public int IsResendCode { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }

    public class VerifyModel : SearchBar
    {
        public int userid { get; set; }
        public string code { get; set; }
        public string provider { get; set; }
        public string providerID { get; set; }
    }

    #endregion

    #region Database Call Class

    public class WEBLoginDatabaseCall
    {

        #region Declarations

        #endregion

        #region CommonMethodsInUseForLogin-(Weblogin/Facebook/Twitter/GooglePlus)
        //Send email to user, on new registration.
        public static void SendNewRegistrationEmail(string Name, string LoginID, string EmailID, string Password, string verificationcode, string webconfigWebsiteRootPath)
        {

            //string Message = "";
            //System.Net.Mail.SmtpClient SMPT = new System.Net.Mail.SmtpClient();
            //System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage();
            //System.IO.FileStream FsContent = new System.IO.FileStream(HttpContext.Current.Server.MapPath("~/resource/email_templates/twitteruserregistration.html"), System.IO.FileMode.Open, System.IO.FileAccess.Read);
            //System.IO.StreamReader SRcontent = new System.IO.StreamReader(FsContent);
            //Message = SRcontent.ReadToEnd();
            //FsContent.Close();
            //SRcontent.Close();
            //FsContent.Dispose();
            //SRcontent.Dispose();
            //Message = Message.Replace("{0}", Name);
            //Message = Message.Replace("{1}", LoginID);
            //Message = Message.Replace("{2}", Password);
            //Message = Message.Replace("{3}", verificationcode);
            //Message = Message.Replace("{20}", webconfigWebsiteRootPath.ToString());
            //Email.Subject = ConfigurationManager.AppSettings["SubjectUserRegistration"].ToString();
            //Email.From = new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["FromUserRegistration"].ToString(), "VConnect.com");
            //Email.IsBodyHtml = true;
            //Email.Body = Message;
            //Email.To.Add(EmailID.ToString().Trim());
            //EmailSending(ref Email, ConfigurationManager.AppSettings["RegistrationUserId"].ToString().Trim(), ConfigurationManager.AppSettings["RegistrationUserPassword"].ToString().Trim());

        }

        public static DataTable getleadbusiness(string leadword, string location, int leadid)
        {
            DataTable tableLeadBusiness = new DataTable();
            List<LeadBusiness> listLeadBusiness = new List<LeadBusiness>();

            using (var db = new VconnectDBContext29())
            {

                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_get_leadbusiness_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@leadword", leadword));
                    cmd.Parameters.Add(new SqlParameter("@location", location));
                    cmd.Parameters.Add(new SqlParameter("@leadid", leadid));

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();
                    listLeadBusiness = ((IObjectContextAdapter)db).ObjectContext.Translate<LeadBusiness>(reader).ToList();

                    tableLeadBusiness = CommonMethods.ToDataTable(listLeadBusiness);

                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);

                }
                finally { db.Database.Connection.Close(); }
            }

            return tableLeadBusiness;

        }

        public static void SendLeadWordEmailSMS(string leadword, string location, int leadid, string EmailID, string Phone, string webconfigWebsiteRootPath)
        {

            //DataTable dt = getleadbusiness(leadword, location, leadid);
            //if (dt != null)
            //{
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        if (dt.Rows[0]["phone"].ToString() != "")
            //        {
            //            if (Phone != "0")
            //            {
            //                Phone = ", " + Phone;
            //            }
            //            else
            //            {
            //                Phone = "";
            //            }
            //            string Text = "VConnect user looking for: '" + leadword.ToString().Trim() + "' IN: '" + location.ToString().Trim() + "' Contact: " + EmailID + Convert.ToInt64(Phone).ToString("###-###-####").Trim();
            //            SendSMS objSendSMS = new SendSMS();
            //            objSendSMS.Sendmessage(Text, dt.Rows[0]["phone"].ToString());

            //        }
            //        else if (dt.Rows[0]["email"].ToString() != "")
            //        {
            //            string Message = "";
            //            System.Net.Mail.SmtpClient SMPT = new System.Net.Mail.SmtpClient();
            //            System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage();
            //            System.IO.FileStream FsContent = new System.IO.FileStream(HttpContext.Current.Server.MapPath("~/resource/email_templates/Sendleadtobusiness.html"), System.IO.FileMode.Open, System.IO.FileAccess.Read);
            //            System.IO.StreamReader SRcontent = new System.IO.StreamReader(FsContent);
            //            Message = SRcontent.ReadToEnd();
            //            FsContent.Close();
            //            SRcontent.Close();
            //            FsContent.Dispose();
            //            SRcontent.Dispose();
            //            Message = Message.Replace("{1}", dt.Rows[0]["businessname"].ToString());
            //            Message = Message.Replace("{2}", leadword);
            //            Message = Message.Replace("{3}", location);
            //            if (EmailID.ToString() != "" && EmailID.ToString() != "0")
            //            {
            //                Message = Message.Replace("{4}", EmailID.ToString());
            //            }
            //            else
            //            {
            //                Message = Message.Replace("{4}", "");
            //            }
            //            Message = Message.Replace("{20}", ConfigurationManager.AppSettings["WebsiteRootPath"].ToString());

            //            if (Phone.ToString() != "" && Phone.ToString() != "0")
            //            {
            //                Message = Message.Replace("{5}", ", " + Phone.ToString());
            //            }
            //            else
            //            {
            //                Message = Message.Replace("{5}", "");
            //            }
            //            Message = Message.Replace("{20}", webconfigWebsiteRootPath.ToString());
            //            Email.Subject = ConfigurationManager.AppSettings["SubjectRequestSuccess"].ToString();
            //            Email.From = new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["FromRequestSuccess"].ToString(), "VConnect.com");
            //            Email.IsBodyHtml = true;
            //            Email.Body = Message;
            //            Email.To.Add(dt.Rows[0]["email"].ToString());
            //            EmailSending(ref Email, ConfigurationManager.AppSettings["LeadUserId"].ToString().Trim(), ConfigurationManager.AppSettings["LeadUserPassword"].ToString().Trim());

            //        }

            //    }
            //}
        }

        public static void Getuserhistory(string SV_VCUserContentID)
        {
            DataSet ds = Getuserhistory(Convert.ToInt32(SV_VCUserContentID));
            if (ds.Tables.Count > 0)
            {

                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    HttpContext.Current.Session["VCHistory"] = 1;

                }
            }
        }

        public static DataSet Getuserhistory(int userid)
        {
            List<UserHistory> listUserHistory = new List<UserHistory>();
            DataSet dsUserhistory = new DataSet();
            using (var db = new VconnectDBContext29())
            {
                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_get_cyberhistory_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@Contentid", userid));
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();
                    listUserHistory = ((IObjectContextAdapter)db).ObjectContext.Translate<UserHistory>(reader).ToList();

                    DataTable tableUserHistory = CommonMethods.ToDataTable(listUserHistory);

                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);

                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }


            return dsUserhistory;
        }

        public static void SetLocationCokkie(string State, string City)
        {
            string sState = string.Empty;
            string sCity = string.Empty;
            if (City.ToString() != "")
            {
                sCity = City.ToString();
            }
            if (State.ToString() != "")
            {
                sState = State.ToString();
            }
            DateTime now = DateTime.Now;
            if (sState.ToString() != "")
            {
                //SET THE COOKIES
                HttpCookie MyDefaultStateCookie = new HttpCookie("DefaultStateCookie");
                MyDefaultStateCookie.Name = "MyDefaultStateCookie";

                // Set the cookie value.
                MyDefaultStateCookie.Value = sState.ToString().Trim().Replace(" ", "_");
                // Set the cookie expiration date.
                MyDefaultStateCookie.Expires = now.AddDays(6);
                HttpContext.Current.Response.Cookies.Add(MyDefaultStateCookie);
            }
            if (sCity.ToString() != "")
            {
                HttpCookie MyDefaultCityCookie = new HttpCookie("DefaultCityCookie");
                MyDefaultCityCookie.Name = "MyDefaultCityCookie";
                MyDefaultCityCookie.Value = sCity.ToString().Trim().Replace(" ", "_");
                MyDefaultCityCookie.Expires = now.AddDays(6);
                HttpContext.Current.Response.Cookies.Add(MyDefaultCityCookie);
            }
        }

        public static int DBAddLogoutTime(string userid, int flag)
        {
            using (var db = new VconnectDBContext29())
            {
                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_add_loginuserlog]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@userid", userid));
                    cmd.Parameters.Add(new SqlParameter("@flag", flag));
                    var err = new SqlParameter("@Err", SqlDbType.Int);
                    err.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(err);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();

                    int result = (int)cmd.Parameters["@Err"].Value;
                    return result;
                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);
                    return 0;

                }
                finally { db.Database.Connection.Close(); }
            }
        }

        public static void AddLoginUserLog(string logintype, string username, int usertype, string userid, string VCUserBusinessName, int VCUserContentID, int VCUserBusinessID, int flag, string reversedns, int sourceCodeVal)
        {
            try
            {
                int result = AddLoginUserLig(logintype, username, usertype, userid, VCUserBusinessName, VCUserContentID, VCUserBusinessID, 1, reversedns, sourceCodeVal);
            }
            catch (Exception exp) { }
        }

        public static int AddLoginUserLig(string logintype, string username, int usertype, string userid, string VCUserBusinessName, int VCUserContentID, int VCUserBusinessID, int flag, string reversedns, int sourceCodeVal)
        {
            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_add_loginuserlog_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@logintype", logintype));
                    cmd.Parameters.Add(new SqlParameter("@username", username));
                    cmd.Parameters.Add(new SqlParameter("@usertype", usertype));
                    cmd.Parameters.Add(new SqlParameter("@userid", userid));
                    cmd.Parameters.Add(new SqlParameter("@VCUserBusinessName", VCUserBusinessName));
                    cmd.Parameters.Add(new SqlParameter("@VCUserContentID", VCUserContentID));
                    cmd.Parameters.Add(new SqlParameter("@VCUserBusinessID", VCUserBusinessID));
                    cmd.Parameters.Add(new SqlParameter("@flag", flag));
                    cmd.Parameters.Add(new SqlParameter("@reversedns", reversedns));
                    cmd.Parameters.Add(new SqlParameter("@sourceCodeVal", sourceCodeVal));
                    var err = new SqlParameter("@Err", SqlDbType.Int);
                    err.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(err);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    result = Int32.Parse(cmd.Parameters["@Err"].Value.ToString());

                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);

                }
                finally { db.Database.Connection.Close(); }
            }
            return result;
        }

        public static void BusinessUpdate(Int64 BID, Int64 UID, Int64 UType)
        {
            using (var db = new VconnectDBContext29())
            {
                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_wap_update_businessusernew_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@bid", BID));
                    cmd.Parameters.Add(new SqlParameter("@uid", UID));
                    cmd.Parameters.Add(new SqlParameter("@Utype", UType));
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);

                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
        }

        public static string AddRequirement(Int32 senderid, string name, Int32 stateid, Int32 cityid, string emailid, string contactnumber, string leaddetail, string leadSubject, string previousurl, string sourceurl, string LFLocation, string StateName, string CityName, string IPAddress, string sourceid, string leadsourcetext, int isreview)
        {
            if (emailid == "")
            {
                emailid = "0";
            }
            if (contactnumber == "")
            {
                contactnumber = "0";
            }

            int result = 0;
            string OutPass = string.Empty;
            string OUTLeadID = string.Empty;

            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[prc_add_lead_opt]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object

                cmd.Parameters.Add(new SqlParameter("@senderid", senderid));
                cmd.Parameters.Add(new SqlParameter("@sendername", name));
                cmd.Parameters.Add(new SqlParameter("@senderstateid", stateid));
                cmd.Parameters.Add(new SqlParameter("@sendercityid", cityid));
                cmd.Parameters.Add(new SqlParameter("@senderemailid", emailid));
                cmd.Parameters.Add(new SqlParameter("@sendercontactnumber", contactnumber));
                cmd.Parameters.Add(new SqlParameter("@senderleaddetail", leaddetail));
                cmd.Parameters.Add(new SqlParameter("@requirement", leadSubject.Replace("'", "`")));
                cmd.Parameters.Add(new SqlParameter("@previousurl", previousurl));
                cmd.Parameters.Add(new SqlParameter("@sourceurl", sourceurl));
                cmd.Parameters.Add(new SqlParameter("@LFLocation", LFLocation));
                cmd.Parameters.Add(new SqlParameter("@SenderStateName", StateName));
                cmd.Parameters.Add(new SqlParameter("@SenderCityName", CityName));
                cmd.Parameters.Add(new SqlParameter("@source", "Website"));
                cmd.Parameters.Add(new SqlParameter("@ipaddress", IPAddress));
                cmd.Parameters.Add(new SqlParameter("@leadsourcetext", leadsourcetext));
                cmd.Parameters.Add(new SqlParameter("@leadsourceid", sourceid));
                cmd.Parameters.Add(new SqlParameter("@isreview", isreview));

                var Err = new SqlParameter("@Err", SqlDbType.Int);
                Err.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(Err);

                var OUTPassword = new SqlParameter("@OUTPassword", SqlDbType.VarChar, 1000);
                Err.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(OUTPassword);

                var OUTLead_ID = new SqlParameter("@OUTLeadID", SqlDbType.VarChar, 1000);
                Err.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(OUTLead_ID);

                cmd.ExecuteNonQuery();

                result = (int)cmd.Parameters["@Err"].Value;
                OutPass = (string)cmd.Parameters["@OUTPassword"].Value;
                OUTLeadID = (string)cmd.Parameters["@OUTLeadID"].Value;
            }

            return OutPass + ',' + result + ',' + OUTLeadID;

        }

        public static void AddLeadWordRequest(string VCLoginName, string VCLoginID, string SV_VCUserContentID, string webconfigWebsiteRootPath)
        {
            string IPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
            string OutPass = string.Empty;
            string OutLeadID = string.Empty;
            string ERR = string.Empty;
            string Result = string.Empty;
            string PREVIOUSURL = string.Empty;
            string SOURCEURL = string.Empty;
            if (HttpContext.Current.Request.Params["PREVIOUSURL"] == null || HttpContext.Current.Request.Params["PREVIOUSURL"].ToString().Trim() == "")
            {
                PREVIOUSURL = "-";
            }
            else
            {
                PREVIOUSURL = HttpContext.Current.Request.Params["PREVIOUSURL"].Replace("~~", "//").Replace("~", "/").ToString().Trim();
            }
            if (HttpContext.Current.Request.Params["SOURCEURL"] == null || HttpContext.Current.Request.Params["SOURCEURL"].ToString().Trim() == "")
            {
                SOURCEURL = "-";
            }
            else
            {
                SOURCEURL = HttpContext.Current.Request.Params["SOURCEURL"].Replace("~~", "//").Replace("~", "/").ToString().Trim();
            }
            Int32 SenderID = 0;
            if (SV_VCUserContentID != null && SV_VCUserContentID != "")
            {
                SenderID = Convert.ToInt32(SV_VCUserContentID);
            }
            else
            {
                SenderID = 0;
            }
            string isEmailID = string.Empty;
            string isContactNumber = string.Empty;
            if (new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,6}$").IsMatch(VCLoginID.ToString().Trim()))
            {
                isEmailID = VCLoginID.ToString().Trim();
                isContactNumber = "0";
            }
            else
            {
                isEmailID = "";
                isContactNumber = VCLoginID.ToString().Trim();
            }

            string resultRequest = string.Empty;

            resultRequest = WEBLoginDatabaseCall.AddRequirement(SenderID, VCLoginName.ToString().Trim(), 0, 0, isEmailID.Trim(), isContactNumber.Trim(), HttpContext.Current.Session["TweetLWPRODUCTDETAIL"].ToString().Trim(), HttpContext.Current.Session["OpenIDSearchKeyword"].ToString().Trim(), PREVIOUSURL.ToString().Trim(), SOURCEURL.ToString().Trim(), HttpContext.Current.Session["OpenIDSearchLocation"].ToString().Trim(), "", "", IPAddress, "", "", Convert.ToInt16(LeadIsReview.AutoResolved));



            string[] a = resultRequest.Split(',');

            OutPass = a[0];
            OutLeadID = a[2];
            ERR = a[1];
            if (ERR != "0")
            {
                Result = "An error occurred. Please try again.";
            }
            else
            {
                WEBLoginDatabaseCall.SendLeadWordEmailSMS(HttpContext.Current.Session["OpenIDSearchKeyword"].ToString().Trim(), HttpContext.Current.Session["OpenIDSearchLocation"].ToString().Trim(), Convert.ToInt32(OutLeadID), isEmailID.Trim(), isContactNumber.Trim(), webconfigWebsiteRootPath);

                Result = "Your request is submitted to relevant suppliers.";
            }
        }

        public static void EmailSending(ref MailMessage Email, string UserID, string Password)
        {
            //SmtpClient sc = new SmtpClient(ConfigurationManager.AppSettings["Emailserver"].ToString().Trim());
            //sc.Credentials = new System.Net.NetworkCredential(UserID, Password);
            //sc.EnableSsl = true; // Please SET IT FALSE in case of NON-GMAIL account            
            //sc.Port = 587;
            //sc.Send(Email);

        }

        #endregion

        #region FacebookUsers

        static public bool CheckFacebookID(string Emailid, string UID) // It will check the Email ID. If it exists, then it will set the Facebook ID on that else will send back the errorcode 12354
        {
            if (string.IsNullOrEmpty(Emailid))
            {
                return false;
            }
            using (var db = new VconnectDBContext29())
            {

                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_wap_add_userdetail_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@Uid", UID));
                    cmd.Parameters.Add(new SqlParameter("@email", Emailid));
                    cmd.Parameters.Add(new SqlParameter("@source", "Facebook"));
                    cmd.Parameters.Add(new SqlParameter("@func", "EditUser"));

                    var sttsPar = new SqlParameter("@stts", SqlDbType.Int);
                    sttsPar.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(sttsPar);

                    var userridPar = new SqlParameter("@userrid", SqlDbType.Int);
                    userridPar.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(userridPar);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    cmd.ExecuteNonQuery();

                    int stts;
                    stts = Convert.ToInt16(cmd.Parameters["@stts"].Value.ToString());

                    if (stts == 1)
                    {
                        return true;
                    }

                    else
                    {
                        return false;
                    }

                }
                catch
                {
                    return false;
                }
            }
        }

        public static string Generate(int Length)
        {
            /* Alternative method
            Random random = new Random();
            return MD5Hash(random.Next(0,1000).ToString());
            */

            if (Length < 5)
            {
                //Length = 5;
            }

            Random random = new Random();
            string password = MD5Hash(random.Next().ToString()).Substring(0, Length);
            string newPass = "";

            // Uppercase at random
            random = new Random();
            for (int i = 0; i < password.Length; i++)
            {
                if (random.Next(0, 2) == 1)
                    newPass += password.Substring(i, 1).ToUpper();
                else
                    newPass += password.Substring(i, 1);
            }

            return newPass;
        }

        private static string MD5Hash(string Data)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] hash = md5.ComputeHash(Encoding.ASCII.GetBytes(Data));

            StringBuilder stringBuilder = new StringBuilder();
            foreach (byte b in hash)
            {
                stringBuilder.AppendFormat("{0:x2}", b);
            }
            return stringBuilder.ToString();
        }

        public static string NewFacebookUser(string email, string name, string UID, string city, string state, string country, string zip, string designation, string sex, string DOB, string religion, string relationship, string password, string device, int flagIsverified)
        {
            using (var db = new VconnectDBContext29())
            {
                int valFlag = 0;
                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_wap_add_userdetail_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@Uid", UID));
                    cmd.Parameters.Add(new SqlParameter("@username", name));
                    cmd.Parameters.Add(new SqlParameter("@mobileemail", email));
                    cmd.Parameters.Add(new SqlParameter("@password", password));
                    cmd.Parameters.Add(new SqlParameter("@verificationcode", "54321"));
                    cmd.Parameters.Add(new SqlParameter("@func", "NewUser"));
                    cmd.Parameters.Add(new SqlParameter("@source", "facebook"));
                    cmd.Parameters.Add(new SqlParameter("@device", device));
                    if (flagIsverified == 0)
                    {
                        valFlag = 0;
                        cmd.Parameters.Add(new SqlParameter("@fbemailverified", valFlag));
                    }
                    else
                    {
                        valFlag = 1;
                        cmd.Parameters.Add(new SqlParameter("@fbemailverified", valFlag));

                    }

                    SqlParameter parameter1 = new SqlParameter("@stts", SqlDbType.Int);
                    parameter1.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(parameter1);
                    SqlParameter parameter2 = new SqlParameter("@userrid", SqlDbType.Int);
                    parameter2.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(parameter2);
                    cmd.Parameters.Add(new SqlParameter("@gender", sex));

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    cmd.ExecuteNonQuery();

                    int stts;
                    stts = Convert.ToInt16(cmd.Parameters["@stts"].Value.ToString());

                    if (stts == 1)
                    {
                        return "Already Exists";
                    }
                    else if (stts == 2)
                    {
                        return "Success";
                    }
                    else
                    {
                        return "Fail";
                    }

                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);
                    return "Fail";
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
        }

        public static DataTable GetUserInfo(string userid, string uid, string source, string gender)
        {
            List<ExternalLoginUserDetails> listExternalLoginUserDetails = new List<ExternalLoginUserDetails>();

            using (var db = new VconnectDBContext29())
            {
                try
                {

                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_wap_getuserinfo_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@loginid", userid));
                    cmd.Parameters.Add(new SqlParameter("@uid", uid));
                    cmd.Parameters.Add(new SqlParameter("@Source", source));
                    cmd.Parameters.Add(new SqlParameter("@gender", gender));

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();

                    listExternalLoginUserDetails = ((IObjectContextAdapter)db).ObjectContext.Translate<ExternalLoginUserDetails>(reader).ToList();


                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);

                }
                finally { db.Database.Connection.Close(); }

                return (DataTable)CommonMethods.ToDataTable(listExternalLoginUserDetails);
            }
        }
        public static DataTable CheckEmailVerified(string username)
        {
            List<ExternalLoginUserDetails> listExternalLoginUserDetails = new List<ExternalLoginUserDetails>();

            using (var db = new VconnectDBContext29())
            {
                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_CheckEmailVerified_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@loginid", username));


                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();

                    listExternalLoginUserDetails = ((IObjectContextAdapter)db).ObjectContext.Translate<ExternalLoginUserDetails>(reader).ToList();


                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);

                }
                finally { db.Database.Connection.Close(); }

                return (DataTable)CommonMethods.ToDataTable(listExternalLoginUserDetails);
            }
        }
        public static DataTable GetUserInfo(string username)
        {
            List<ExternalLoginUserDetails> listExternalLoginUserDetails = new List<ExternalLoginUserDetails>();

            using (var db = new VconnectDBContext29())
            {
                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_wap_getuserinfo_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@loginid", username));


                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();

                    listExternalLoginUserDetails = ((IObjectContextAdapter)db).ObjectContext.Translate<ExternalLoginUserDetails>(reader).ToList();


                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);

                }
                finally { db.Database.Connection.Close(); }

                return (DataTable)CommonMethods.ToDataTable(listExternalLoginUserDetails);
            }
        }

        public static DataTable GetUserInfoByProviderId(string loginid, string providerId, string source, string gender)
        {
            List<ExternalLoginUserDetails> listExternalLoginUserDetails = new List<ExternalLoginUserDetails>();

            using (var db = new VconnectDBContext29())
            {
                try
                {

                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_getUserDetailsByProviderId_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@loginid", loginid));
                    cmd.Parameters.Add(new SqlParameter("@uid", providerId));
                    cmd.Parameters.Add(new SqlParameter("@Source", source));
                    cmd.Parameters.Add(new SqlParameter("@gender", gender));

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();

                    listExternalLoginUserDetails = ((IObjectContextAdapter)db).ObjectContext.Translate<ExternalLoginUserDetails>(reader).ToList();


                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);

                }
                finally { db.Database.Connection.Close(); }

                return (DataTable)CommonMethods.ToDataTable(listExternalLoginUserDetails);
            }
        }



        public static DataTable DBFetchUnverifiedUserDetials(string emailid)
        {
            DataTable dt = new DataTable();
            using (var db = new VconnectDBContext29())
            {
                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "select top 1 uc.customerid as userid,ud.contactname as screenname from usercredential uc inner join userdetails ud on uc.customerid=ud.contentid where userloginid='" + emailid + "' order by uc.contentid desc";
                    cmd.CommandType = System.Data.CommandType.Text;

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();

                    dt = ((IObjectContextAdapter)db).ObjectContext.Translate<UnverifiedUserDetails>(reader).ToDataTable();

                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);

                }
                finally { db.Database.Connection.Close(); }

                return dt;
            }
        }

        public static bool CheckFacebookID(string UID) // It will check the Facebook ID exists or not
        {
            bool result = false;
            using (var db = new VconnectDBContext29())
            {

                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_wap_get_CheckEmailfbID_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@Uid", UID));
                    var errCode = new SqlParameter("@ErrorCode", SqlDbType.Int);
                    errCode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(errCode);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    cmd.ExecuteNonQuery();
                    int retVal = Convert.ToInt32(cmd.Parameters["@ErrorCode"].Value.ToString());

                    if (retVal == 1)
                    {
                        result = true;
                    }
                    else if (retVal == 0)
                    {
                        result = false;
                    }
                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);

                }
                finally { db.Database.Connection.Close(); }

                return result;
            }
        }

        static public DataTable GetUserInfo(string userid, string source)
        {
            using (var db = new VconnectDBContext29())
            {
                List<ExternalLoginUserDetails> listExternalLoginUserDetails = new List<ExternalLoginUserDetails>();

                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_wap_getuserinfo_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@loginid", userid));
                    cmd.Parameters.Add(new SqlParameter("@Source", source));

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();

                    listExternalLoginUserDetails = ((IObjectContextAdapter)db).ObjectContext.Translate<ExternalLoginUserDetails>(reader).ToList();


                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);

                }
                finally { db.Database.Connection.Close(); }

                return (DataTable)CommonMethods.ToDataTable(listExternalLoginUserDetails);
            }
        }

        #endregion

        #region GoogleUsers

        public static bool CheckGoogleID(string UID)
        {

            bool result = false;
            using (var db = new VconnectDBContext29())
            {

                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_wap_get_checkemailgoogleId_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@Uid", UID));
                    var errCode = new SqlParameter("@ErrorCode", SqlDbType.Int);
                    errCode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(errCode);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    cmd.ExecuteNonQuery();
                    int retVal = Convert.ToInt32(cmd.Parameters["@ErrorCode"].Value.ToString());

                    if (retVal == 1)
                    {
                        result = true;
                    }
                    else if (retVal == 0)
                    {
                        result = false;
                    }
                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);

                }
                finally { db.Database.Connection.Close(); }

                return result;
            }
        }

        public static string NewGoogleUser(string email, string name, string UID, string city, string state, string country, string zip, string designation, string sex, string DOB, string religion, string relationship, string password, string device)
        {
            using (var db = new VconnectDBContext29())
            {

                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_wap_add_userdetail_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@Uid", UID));
                    cmd.Parameters.Add(new SqlParameter("@username", name));
                    cmd.Parameters.Add(new SqlParameter("@mobileemail", email));
                    cmd.Parameters.Add(new SqlParameter("@password", password));
                    cmd.Parameters.Add(new SqlParameter("@verificationcode", "54321"));
                    cmd.Parameters.Add(new SqlParameter("@func", "NewUser"));
                    cmd.Parameters.Add(new SqlParameter("@source", "google"));
                    cmd.Parameters.Add(new SqlParameter("@device", device));

                    SqlParameter parameter1 = new SqlParameter("@stts", SqlDbType.Int);
                    parameter1.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(parameter1);
                    SqlParameter parameter2 = new SqlParameter("@userrid", SqlDbType.Int);
                    parameter2.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(parameter2);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    cmd.ExecuteNonQuery();

                    int stts;
                    stts = Convert.ToInt16(cmd.Parameters["@stts"].Value.ToString());

                    if (stts == 1)
                    {
                        return "Already Exists";
                    }
                    else if (stts == 2)
                    {
                        return "Success";
                    }
                    else
                    {
                        return "Fail";
                    }

                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);
                    return "Fail";
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
        }

        #endregion

        #region TwitterUsers

        public static string CheckTwiiterUserID(string userid, string source)
        {
            using (var db = new VconnectDBContext29())
            {

                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_wap_get_checkemailtwfbId_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@uid", userid));

                    if (source == "Facebook")
                    {
                        cmd.Parameters.Add(new SqlParameter("@source", "facebook"));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@source", "twitter"));
                    }

                    var ErrorCode = new SqlParameter("@ErrorCode", SqlDbType.BigInt);
                    ErrorCode.Direction = ParameterDirection.Output;

                    var Email = new SqlParameter("@email", SqlDbType.VarChar, 50);
                    Email.Direction = ParameterDirection.Output;

                    cmd.Parameters.Add(ErrorCode);
                    cmd.Parameters.Add(Email);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();

                    return cmd.Parameters["@ErrorCode"].Value.ToString() + "," + cmd.Parameters["@email"].Value.ToString();
                }

                finally
                {
                    db.Database.Connection.Close();
                }
            }
        }

        public static bool CheckTwitterID(string Emailid, string mobile, string UID) // It will check the Email ID. If it exists, then it will set the Facebook ID on that else will send back the errorcode 12354
        {
            if (string.IsNullOrEmpty(Emailid))
            {
                return false;
            }

            using (var db = new VconnectDBContext29())
            {

                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_wap_add_userdetail_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@UID", UID));
                    cmd.Parameters.Add(new SqlParameter("@mobileemail", mobile));
                    cmd.Parameters.Add(new SqlParameter("@email", Emailid));
                    cmd.Parameters.Add(new SqlParameter("@source", "twitter"));
                    cmd.Parameters.Add(new SqlParameter("@func", "EditUser"));

                    SqlParameter parameter1 = new SqlParameter("@stts", SqlDbType.Int);
                    parameter1.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(parameter1);
                    SqlParameter parameter2 = new SqlParameter("@userrid", SqlDbType.Int);
                    parameter2.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(parameter2);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    cmd.ExecuteNonQuery();

                    int stts;
                    stts = Convert.ToInt16(cmd.Parameters["@stts"].Value.ToString());

                    if (stts == 1)
                    {
                        return true;
                    }

                    else
                    {
                        return false;
                    }

                }

                finally
                {
                    db.Database.Connection.Close();
                }
            }
        }

        public static string NewUser(string uid, string Password, string ScreenName, string Mobile, string email, string SocialID, string source, string device, out int userid)
        {

            using (var db = new VconnectDBContext29())
            {

                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_wap_add_userdetail_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@uid", uid));
                    cmd.Parameters.Add(new SqlParameter("@username", ScreenName));
                    cmd.Parameters.Add(new SqlParameter("@mobileemail", Mobile));
                    cmd.Parameters.Add(new SqlParameter("@email", email));
                    cmd.Parameters.Add(new SqlParameter("@password", Password));
                    cmd.Parameters.Add(new SqlParameter("@encriptedpassword", WEBLoginDatabaseCall.encryptPassword(Password)));
                    cmd.Parameters.Add(new SqlParameter("@verificationcode", "54321"));
                    cmd.Parameters.Add(new SqlParameter("@func", SocialID));
                    cmd.Parameters.Add(new SqlParameter("@source", source));
                    cmd.Parameters.Add(new SqlParameter("@device", device));

                    SqlParameter parameter1 = new SqlParameter("@stts", SqlDbType.Int);
                    parameter1.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(parameter1);

                    SqlParameter parameter2 = new SqlParameter("@userrid", SqlDbType.Int);
                    parameter2.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(parameter2);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    cmd.ExecuteNonQuery();

                    int stts;
                    stts = Convert.ToInt16(cmd.Parameters["@stts"].Value.ToString());

                    userid = Convert.ToInt32(cmd.Parameters["@userrid"].Value.ToString());

                    if (stts == 1)
                    {
                        return "Already Exists";
                    }
                    else if (stts == 2)
                    {
                        return "Success";
                    }
                    else
                    {
                        return "Fail";
                    }

                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);
                    userid = 0;
                    return "Fail";
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
        }

        public static bool CheckEmailIDAvailability(string Email)
        {
            List<CheckEmailAvailability> listCheckEmailAvailability = new List<CheckEmailAvailability>();
            DataTable dt = new DataTable();
            using (var db = new VconnectDBContext29())
            {
                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_wap_get_CheckEmailIDAvailability_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@loginid", Email));

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();
                    listCheckEmailAvailability = ((IObjectContextAdapter)db).ObjectContext.Translate<CheckEmailAvailability>(reader).ToList();

                    dt = CommonMethods.ToDataTable(listCheckEmailAvailability);

                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);

                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            if (dt.Rows[0][0].ToString() == "0")
                return true; // dont exists 
            else
            {

            }
            return false; // exists
        }

        public static bool CheckEmailID(string email)
        {
            return WEBLoginDatabaseCall.CheckEmailIDAvailability(email.Trim());
        }

        public static string encryptPassword(string password)
        {
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(password.Trim()));
            StringBuilder strEncrptedPassword = new StringBuilder();
            // Loop through each byte of the hashed data and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                strEncrptedPassword.Append(data[i].ToString("x2"));
            }
            return strEncrptedPassword.ToString();
        }

        #endregion

        #region ForgotPassword

        public static string DBForgotPass(string loginid, string randpass, string encriptpassword, string vcode)
        {
            using (var db = new VconnectDBContext29())
            {
                string result = string.Empty;
                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_forgotpasstest_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object

                    cmd.Parameters.Add(new SqlParameter("@userloginid", loginid));
                    cmd.Parameters.Add(new SqlParameter("@randpass", randpass));
                    cmd.Parameters.Add(new SqlParameter("@encriptpassword", encriptpassword));
                    cmd.Parameters.Add(new SqlParameter("@source", "Web"));
                    cmd.Parameters.Add(new SqlParameter("@vcode", vcode));
                    var OUTPass = new SqlParameter("@OUTPass", SqlDbType.VarChar, 1000);
                    OUTPass.Direction = ParameterDirection.Output;
                    var OUTUserName = new SqlParameter("@OUTUserName", SqlDbType.VarChar, 1000);
                    OUTUserName.Direction = ParameterDirection.Output;
                    var outtest = new SqlParameter("@outtest", SqlDbType.VarChar, 1000);
                    outtest.Direction = ParameterDirection.Output;
                    var isVerified = new SqlParameter("@isVerified", SqlDbType.Int);
                    isVerified.Direction = ParameterDirection.Output;


                    cmd.Parameters.Add(OUTPass);
                    cmd.Parameters.Add(OUTUserName);
                    cmd.Parameters.Add(outtest);
                    cmd.Parameters.Add(isVerified);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    string pass = string.IsNullOrWhiteSpace(cmd.Parameters["@OUTPass"].Value.ToString()) ? "" : (string)cmd.Parameters["@OUTPass"].Value;
                    result = pass + "," + cmd.Parameters["@OUTUserName"].Value + "," + cmd.Parameters["@outtest"].Value + "," + cmd.Parameters["@isVerified"].Value.ToString();

                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);

                }
                finally { db.Database.Connection.Close(); }
                return result;
            }
        }


        public static int addsmsemaillog(int customerid, int usertype, string receiveremail, string receiverphone, string receivername, int messagetype,
          string subject, string message, int businessid, string keyword, string location, string response, string ipaddress, int smslength,
           int noofsms, string pageurl, int actiontype, string reversedns)
        {
            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_add_smslog_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@customerid", customerid));
                    cmd.Parameters.Add(new SqlParameter("@usertype", usertype));
                    cmd.Parameters.Add(new SqlParameter("@receiveremail", receiveremail));
                    cmd.Parameters.Add(new SqlParameter("@receiverphone", receiverphone.Replace(",", "")));
                    cmd.Parameters.Add(new SqlParameter("@receivername", receivername));
                    cmd.Parameters.Add(new SqlParameter("@messagetype", messagetype));
                    cmd.Parameters.Add(new SqlParameter("@subject", subject));
                    cmd.Parameters.Add(new SqlParameter("@message", message));
                    cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                    cmd.Parameters.Add(new SqlParameter("@keyword", keyword));
                    cmd.Parameters.Add(new SqlParameter("@location", location));
                    cmd.Parameters.Add(new SqlParameter("@response", response));
                    cmd.Parameters.Add(new SqlParameter("@ipaddress", ipaddress));
                    cmd.Parameters.Add(new SqlParameter("@smslength", smslength));
                    cmd.Parameters.Add(new SqlParameter("@noofsms", noofsms));
                    cmd.Parameters.Add(new SqlParameter("@pageurl", pageurl));
                    cmd.Parameters.Add(new SqlParameter("@actiontype", actiontype));
                    cmd.Parameters.Add(new SqlParameter("@createdby", customerid));
                    cmd.Parameters.Add(new SqlParameter("@reversedns", reversedns));
                    var Err = new SqlParameter("@Err", SqlDbType.Int);
                    Err.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(Err);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    cmd.ExecuteNonQuery();

                    result = (int)cmd.Parameters["@Err"].Value;

                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);

                }
                finally { db.Database.Connection.Close(); }
                return result;

            }
        }

        public static void UpdateNewVerificationCode(string LoginId, string verificationCode)
        {

            using (var db = new VconnectDBContext29())
            {
                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_Update_VerificationCode_opt]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@LoginId", LoginId));
                    cmd.Parameters.Add(new SqlParameter("@code", verificationCode));

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    cmd.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);

                }
                finally { db.Database.Connection.Close(); }

            }
        }

        public static int DBCheckValidVerificationLink(string contentid, string verifyCode)
        {
            using (var db = new VconnectDBContext29())
            {
                int result = 0;
                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_CheckValidVerifyLink_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object

                    cmd.Parameters.Add(new SqlParameter("@contentid", contentid));
                    cmd.Parameters.Add(new SqlParameter("@code", verifyCode));
                    var outValidLink = new SqlParameter("@outValidLink", SqlDbType.Int);
                    outValidLink.Direction = ParameterDirection.Output;

                    cmd.Parameters.Add(outValidLink);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    cmd.ExecuteNonQuery();
                    result = Int32.Parse(cmd.Parameters["@outValidLink"].Value.ToString());

                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);

                }
                finally { db.Database.Connection.Close(); }
                return result;
            }
        }

        public static string UpdatePassword(string contentid, string Password, string vcode)
        {
            using (var db = new VconnectDBContext29())
            {

                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_wap_update_userpassword_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@contentid", contentid));
                    cmd.Parameters.Add(new SqlParameter("@Password", Password));
                    cmd.Parameters.Add(new SqlParameter("@UpdateDate", DateTime.Now));
                    cmd.Parameters.Add(new SqlParameter("@encrptpassword", encryptPassword(Password)));
                    cmd.Parameters.Add(new SqlParameter("@vcode", vcode));
                    var ErrorCode = new SqlParameter("@ErrorCode", SqlDbType.BigInt);
                    ErrorCode.Direction = ParameterDirection.Output;
                    ErrorCode.Value = 0;

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    cmd.ExecuteNonQuery();
                    return "Success";

                }
                catch (Exception ex)
                {
                    return "Unspecified error occured";
                }
            }
        }

        public static string UpdateIsMobileEmailVerified(string loginID, int flagEmailMobile)
        {
            using (var db = new VconnectDBContext29())
            {

                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_update_ismobile_isemail_verifiedTest_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@userid", loginID));
                    cmd.Parameters.Add(new SqlParameter("@flagEmailMobile", flagEmailMobile));
                    var Errorflag = new SqlParameter("@errorflag", SqlDbType.BigInt);
                    Errorflag.Direction = ParameterDirection.Output;
                    Errorflag.Value = 0;
                    cmd.Parameters.Add(Errorflag);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    cmd.ExecuteNonQuery();

                    return cmd.Parameters["@errorflag"].Value.ToString();

                }
                catch (Exception ex)
                {
                    return "Unspecified error occured";
                }
            }
        }

        #endregion

        #region UserAccountBusinessMapping
        public static ResultTableAccountBusinessMappingModel DBGetAccountMappingDetails(string LoginId)
        {
            DataTable tableLeadBusiness = new DataTable();
            List<UserAccountMappingModel> listUserAccountMappingModel = new List<UserAccountMappingModel>();
            List<UserAccountMappingBusinessesModel> listUserAccountMappingBusinessesModel = new List<UserAccountMappingBusinessesModel>();
            ResultTableAccountBusinessMappingModel tableResultTableAccountBusinessMappingModel = new ResultTableAccountBusinessMappingModel();


            using (var db = new VconnectDBContext29())
            {

                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_get_MatchUserID_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@loginid", LoginId));

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();
                    listUserAccountMappingModel = ((IObjectContextAdapter)db).ObjectContext.Translate<UserAccountMappingModel>(reader).ToList();
                    reader.NextResult();
                    listUserAccountMappingBusinessesModel = ((IObjectContextAdapter)db).ObjectContext.Translate<UserAccountMappingBusinessesModel>(reader).ToList();

                    tableResultTableAccountBusinessMappingModel.UserAccountMappingBusinessesModel = listUserAccountMappingBusinessesModel;
                    tableResultTableAccountBusinessMappingModel.UserAccountMappingModel = listUserAccountMappingModel;


                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);

                }
                finally { db.Database.Connection.Close(); }
            }

            return tableResultTableAccountBusinessMappingModel;
        }

        public static string DBMapUserAccountWithBusiness(int contentID, string phone, string UserLoginId)
        {
            using (var db = new VconnectDBContext29())
            {
                string res = string.Empty;
                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_update_claimedbizuser_opt]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@userid", contentID));
                    cmd.Parameters.Add(new SqlParameter("@phone", phone));
                    cmd.Parameters.Add(new SqlParameter("@loginid", UserLoginId));
                    var err = new SqlParameter("@err", SqlDbType.Int);
                    err.Direction = ParameterDirection.Output;
                    var passwordOut = new SqlParameter("@passwordOut", SqlDbType.VarChar, 64);
                    passwordOut.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(err);
                    cmd.Parameters.Add(passwordOut);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    cmd.ExecuteNonQuery();

                    res = cmd.Parameters["@err"].Value.ToString() + "," + cmd.Parameters["@passwordOut"].Value.ToString();
                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);

                }
                finally { db.Database.Connection.Close(); }

                return res;
            }
        }
        #endregion


        public static int DBUpdateUserPhoneEmail(string loginid, string password, string updateField, int flag)
        {
            using (var db = new VconnectDBContext29())
            {
                int res = 3;
                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_UpdateUserEmailPhone_opt]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@loginid", loginid));
                    cmd.Parameters.Add(new SqlParameter("@password", password));
                    if (flag == 1)
                        cmd.Parameters.Add(new SqlParameter("@email", updateField));
                    else
                        cmd.Parameters.Add(new SqlParameter("@phone", updateField));

                    cmd.Parameters.Add(new SqlParameter("@flag", flag));
                    var err = new SqlParameter("@err", SqlDbType.Int);
                    err.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(err);
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    cmd.ExecuteNonQuery();


                    res = Int32.Parse(cmd.Parameters["@err"].Value.ToString());
                }
                catch
                {
                    res = 1;
                }
                finally { db.Database.Connection.Close(); }

                return res;
            }
        }

        public static string DBVerifyUser(int userid, string code)
        {
            using (var db = new VconnectDBContext29())
            {
                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_UserVerification_opt]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@userid", userid));
                    cmd.Parameters.Add(new SqlParameter("@code", code));
                    var err = new SqlParameter("@err", SqlDbType.Int);
                    err.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(err);
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    return cmd.Parameters["@err"].Value.ToString();
                }
                catch
                {
                    return "1";
                }
                finally { db.Database.Connection.Close(); }

            }
        }

        public static void DBSaveFBUserFriendList(List<FbUserFriendsList> fbUsers, int userid)
        {
            DataTable dtFBUsers = new DataTable();

            using (var db = new VconnectDBContext29())
            {
                try
                {
                    dtFBUsers = CommonMethods.ToDataTable(fbUsers);

                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_addfacebookusersfriendslist]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@userid", userid));
                    cmd.Parameters.Add(new SqlParameter("@fbuserfrnlist", dtFBUsers));
                    //var err = new SqlParameter("@err", SqlDbType.Int);
                    //err.Direction = ParameterDirection.Output;
                    //cmd.Parameters.Add(err);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    cmd.ExecuteNonQuery();

                    //return cmd.Parameters["@err"].Value.ToString();
                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);
                    //return "1";
                }
                finally { db.Database.Connection.Close(); }

            }
        }

        public static void DBSaveGoogleContactsList(List<FetchContactsModel> googleUsers, int userid)
        {
            DataTable dtGoogleUsers = new DataTable();

            using (var db = new VconnectDBContext29())
            {
                try
                {
                    dtGoogleUsers = CommonMethods.ToDataTable(googleUsers);

                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_addgoogleusercontactlist]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@userid", userid));
                    cmd.Parameters.Add(new SqlParameter("@googleusercontactlist", dtGoogleUsers));

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    cmd.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);
                }
                finally { db.Database.Connection.Close(); }
            }
        }

        public static bool IsEmailAssociatedWithOtherId(string email)
        {
            using (var db = new VconnectDBContext29())
            {

                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "select top 1 1 from userdetails where email='" + email + "' and twitterid<>'' and twitterid is not null ";
                    cmd.CommandType = CommandType.Text;

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);
                    return false;
                }
                finally { db.Database.Connection.Close(); }
            }
        }

        public static void DBSaveTWUserFollowersList(List<TwiterFollowers> twFollowers, int userid)
        {
            DataTable dtTwFollowers = new DataTable();

            using (var db = new VconnectDBContext29())
            {
                try
                {
                    dtTwFollowers = CommonMethods.ToDataTable(twFollowers);

                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_addtwfollowerslist]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@userid", userid));
                    cmd.Parameters.Add(new SqlParameter("@twFollowersList", dtTwFollowers));

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    cmd.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);
                }
                finally { db.Database.Connection.Close(); }
            }
        }


    }

    #endregion

    #region CommonFunctions

    static internal class CommonMethods
    {
        public static DataTable ToDataTable<T>(this IEnumerable<T> collection)
        {
            DataTable dt = new DataTable("DataTable");
            Type t = typeof(T);
            PropertyInfo[] pia = t.GetProperties();

            //Inspect the properties and create the columns in the DataTable
            foreach (PropertyInfo pi in pia)
            {
                Type ColumnType = pi.PropertyType;
                if ((ColumnType.IsGenericType))
                {
                    ColumnType = ColumnType.GetGenericArguments()[0];
                }
                dt.Columns.Add(pi.Name, ColumnType);
            }

            //Populate the data table
            foreach (T item in collection)
            {
                DataRow dr = dt.NewRow();
                dr.BeginEdit();
                foreach (PropertyInfo pi in pia)
                {
                    if (pi.GetValue(item, null) != null)
                    {
                        dr[pi.Name] = pi.GetValue(item, null);
                    }
                }
                dr.EndEdit();
                dt.Rows.Add(dr);
            }
            return dt;
        }
    }

    #endregion

}
