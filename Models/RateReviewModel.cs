﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vconnect.Models
{
    public class RateReviewModel
    {
        public string Review { get; set; }
        public int Rate { get; set; }
    }
}