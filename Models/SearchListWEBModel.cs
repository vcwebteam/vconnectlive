﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vconnect.Mapping.Listing;
using Vconnect.Mapping.CommonDb;
using Vconnect.Mapping.ListingWEB;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using System.Web.DynamicData;
using System.Text;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Data;
using Vconnect.Common;

namespace Vconnect.Models
{
    public class bizLikeSave
    {
        [Key]
        public virtual int BusinessId { get; set; }
        public virtual int UserId { get; set; }
        public virtual string statusFor { get; set; }
    }

    public class LeadQuestionaree
    {
        public Int32 questionid { get; set; }
        public string Content { get; set; }
        public Int32 contenttype { get; set; }
        public Int32 contenttypeid { get; set; }
        public Int32 isprimary { get; set; }
        public string options { get; set; }
    }
    public class ProductServiceURL
    {
        public string url { get; set; }
    }
    public class PageInfo
    {
        public string keyword { get; set; }
        public string url { get; set; }
        public string locationtag { get; set; }
    }
    [Serializable]
   public class SearchListWebModel:SearchBarWebModel
   {
       public string metaSearchLoc { get; set; }
       public ListSearchResultWEB listSearchResultWEB { get; set; }
       public List<LocationFilter> LocationFilter { get; set; }
       public List<KeywordFilter> KeywordFilter { get; set; }
       public List<RelatedSearch> RelatedSearch { get; set; }
       public ProductServiceURL productServiceURL { get; set; }
       public PageInfo pageInfo { get; set; }
       public LeadWord leadWord { get; set; }
       public List<LeadQuestionaree> leadQuestionaree { get; set; }
       public bizLikeSave bizLikeSave;
       public int? pageNum { get; set; }
       public int rowsPerPage { get; set; }
       public int sort { get; set; }
       public int type { get; set; }
       public int totalPages { get; set; }
       public Int32? contentId { get; set; }
       public Int32? locid { get; set; }
      // public int? userId { get; set; }
       public List<BannerWeb> bannerWeb { get; set; }
//       public PYRModel pyrModel { get; set; }

       [Required(ErrorMessage = "please enter the username")]
       public string UserName
       { get; set; }



       [Required(ErrorMessage = "please enter the Phone")]
       public string Phone
       { get; set; }
       public string ValidationCode

       { get; set; }

       public int UserWatchlist(Int64 userid, Int64 Businessid)
       {
           int result = 0;
           try
           {
             
               using (var db = new VconnectDBContext29())
               {
                   var cmd = db.Database.Connection.CreateCommand();
                   cmd.CommandText = "[dbo].[prc_AddUp_userwatchlist]";
                   cmd.CommandType = System.Data.CommandType.StoredProcedure;
                   cmd.Parameters.Add(new SqlParameter("@userid", userid));
                   cmd.Parameters.Add(new SqlParameter("@businessid", Businessid));
                   cmd.Parameters.Add(new SqlParameter("@source", "web"));
                   SqlParameter Err = new SqlParameter("@Err", SqlDbType.Int);
                   Err.Direction = ParameterDirection.Output;
                   cmd.Parameters.Add(Err);


                   var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                   connection.Open();

                   cmd.ExecuteNonQuery();
                   result = (int)cmd.Parameters["@Err"].Value;
                   
               }
           }
           catch (Exception ex)
           {
               log.LogMe(ex);
           }

           return result;

       }


       public int BusinessLike(Int64 userid, Int64 Businessid)
       {
           int result = 0;
           try
           {
               using (var db = new VconnectDBContext29())
               {
                   var cmd = db.Database.Connection.CreateCommand();
                   cmd.CommandText = "[dbo].[prc_AddUp_businessLikeList]";
                   cmd.CommandType = System.Data.CommandType.StoredProcedure;
                   cmd.Parameters.Add(new SqlParameter("@userid", userid));
                   cmd.Parameters.Add(new SqlParameter("@businessid", Businessid));
                   cmd.Parameters.Add(new SqlParameter("@source", "web"));
                   SqlParameter Err = new SqlParameter("@Err", SqlDbType.Int);
                   Err.Direction = ParameterDirection.Output;
                   cmd.Parameters.Add(Err);


                   var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                   connection.Open();
                   cmd.ExecuteNonQuery();
                   result = (int)cmd.Parameters["@Err"].Value;
                 
               }
           }
           catch (Exception ex)
           {
               log.LogMe(ex);
           }
           return result;
       }
       public IList<string> businessSaveLikeShare(Int64 userid, string Businessid)
       {
           List<string> msg=new List<string>();
           try
           {
               using (var db = new VconnectDBContext29())
               {
                   var cmd = db.Database.Connection.CreateCommand();
                   cmd.CommandText = "[dbo].[prc_get_businessSaveLikeShare]";
                   cmd.CommandType = System.Data.CommandType.StoredProcedure;
                   cmd.Parameters.Add(new SqlParameter("@userid", userid));
                   cmd.Parameters.Add(new SqlParameter("@businessid", Businessid));

                   //SqlParameter Err = new SqlParameter("@Err", SqlDbType.Int);
                   //Err.Direction = ParameterDirection.Output;
                   //cmd.Parameters.Add(Err);

                   //int result = (int)cmd.Parameters["@Err"].Value;
                   var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                   connection.Open();
                   var reader = cmd.ExecuteReader();
                   if (reader.HasRows)
                   {
                       while (reader.Read())
                       {

                           var str = reader[2].ToString() + reader[0].ToString();
                           msg.Add(str);

                       }
                   }
               }
           }
           catch(Exception ex)
           {
               log.LogMe(ex);
           }
               return msg;
           
       }

       public int leadResponse(string leadword, string location, int leadid)
       {
           int result = 0;
           using (var db = new VconnectDBContext29())
           {
               var cmd = db.Database.Connection.CreateCommand();
               cmd.CommandText = "[dbo].[prc_add_leadresponse]";
               cmd.CommandType = System.Data.CommandType.StoredProcedure;
               cmd.Parameters.Add(new SqlParameter("@leadword", leadword));
               cmd.Parameters.Add(new SqlParameter("@location", location));
               cmd.Parameters.Add(new SqlParameter("@leadid", leadid));
               cmd.Parameters.Add(new SqlParameter("@flag", 2));
               SqlParameter err = new SqlParameter("@Err", SqlDbType.Int);
               err.Direction = ParameterDirection.Output;
               cmd.Parameters.Add(err);


               var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
               connection.Open();
               cmd.ExecuteReader();
               result = (int)cmd.Parameters["@Err"].Value;
           }
           return result;
       }

       public int leadQuestion(Int32? questionid, Int32? answerid, Int32? queno, Int32? leadid, Int32? contenttype, Int32? contenttypeid)
       {
           int result;

           using (var db = new VconnectDBContext29())
           {
               var cmd = db.Database.Connection.CreateCommand();
               cmd.CommandText = "[dbo].[prc_add_leadQuestion]";
               cmd.CommandType = System.Data.CommandType.StoredProcedure;
               cmd.Parameters.Add(new SqlParameter("@questionid", questionid.Value));
               cmd.Parameters.Add(new SqlParameter("@answerid", answerid.Value));
               cmd.Parameters.Add(new SqlParameter("@queno", queno.Value));
               cmd.Parameters.Add(new SqlParameter("@leadid", leadid.Value));
               cmd.Parameters.Add(new SqlParameter("@contenttype", contenttype.Value));
               cmd.Parameters.Add(new SqlParameter("@contenttypeid", contenttypeid.Value));
               SqlParameter err = new SqlParameter("@Err", SqlDbType.Int);
               err.Direction = ParameterDirection.Output;
               cmd.Parameters.Add(err);


               var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
               connection.Open();
               cmd.ExecuteReader();
               result = (int)cmd.Parameters["@Err"].Value;
           }
           return result;
       }

      }
}