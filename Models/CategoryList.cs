﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vconnect.Models
{
    public class CategoryList
    {
        public string breadcrumb { set; get; }
        public int? categoryid { set; get; }
        public string categoryname { set; get; }
        public string url { set; get; }
      
    }
}