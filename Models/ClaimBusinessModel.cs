﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Vconnect.Mapping.Listing;
using Vconnect.Mapping.CommonDb;
namespace Vconnect.Models
{
    public class ClaimBusinessModel
    {
       
        public string claimno
        { get; set; }
        public string SearchBuisnesname
        { get; set; }
        public string SearchBuisnessaddress
        { get; set; }
        public string SearchBuisnesslogo
        { get; set; }
        public string ClaimVerifyCode
        { get; set; }
        public string RequestType { get; set; }
        public char typ { get; set; }
        public string SearchBusinessLocation
        { get; set; }
        public List<SearchListCYB> SearchListCYB

        { get; set; }
         public string bid
        { get; set; }
        public int isclaim
        { get; set; }
        public string burl
        { get; set; }
        [MaxLength(11)]
         public string SearchBusinessPhone
        { get; set; }
         public string ReportIncorrectComments
         { get; set; }
         public List<SearchListDetails> SearchListDetails { get; set; }
         public List<SearchBizDetailsClaim> SearchBizDetailsClaim { get; set; }
         public List<SearchListCYBWeb> SearchListCYBWeb { get; set; }
         public string claimType
         { get; set; }
    }

    public class ClaimBusinessDetailModel
    {
        public string SearchBusinessEmail
        { get; set; }
        public string SearchBusinessContactname
        { get; set; }
        public string SearchCYBBuisnesname
        { get; set; }
    }
    
}