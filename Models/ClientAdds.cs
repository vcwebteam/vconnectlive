﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Vconnect.Common;

namespace Vconnect.Models
{
    public class ClientAdds
    {
        #region Properties
        public string SuccessMessage { get; set; }
        public string clientName { get; set; }
        public string userName { get; set; }
        public string businessName { get; set; }
        public string emailId { get; set; }
        public string phoneNumber { get; set; }
        public string userQuery { get; set; }
        #endregion
        #region Methods

        public int AddUserReg(string clientName, string userName, string businessName, string email, string phoneNumber, string userQuery)
        {
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_clientadds]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@clientname", clientName ));
                cmd.Parameters.Add(new SqlParameter("@username",userName));
                 cmd.Parameters.Add(new SqlParameter("@bizname", businessName));
                cmd.Parameters.Add(new SqlParameter("@emailid", email));
                cmd.Parameters.Add(new SqlParameter("@phoneno", phoneNumber));               
                cmd.Parameters.Add(new SqlParameter("@query", userQuery));
                SqlParameter Err = new SqlParameter("@Err", SqlDbType.Int);
                Err.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(Err);
                int result = 0;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    result = (int)cmd.Parameters["@Err"].Value;
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
                finally
                {
                    db.Database.Connection.Close();
                }
                return result;
            }
        }

        #endregion



    }
}