﻿using System.Web;
using Vconnect.Mapping.Listing;
using Vconnect.Mapping.CommonDb;
using Vconnect.Mapping.ListingWEB;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using System.Web.DynamicData;
using Vconnect.Mapping.UserDBWEB;
using System;
using System.Collections.Generic;
using Vconnect.Common;
using System.Data.SqlClient;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections;
namespace Vconnect.Models
{



    public class GetUserDetails
    {
        public Int32 userid { get; set; }

    }
    public class ChangePassword
    {
        [Required]
        [DataType(DataType.Password)]
        [StringLength(50, MinimumLength = 6, ErrorMessage = "Password must be between 6 and 50 characters")]
        public string Password { get; set; }



        public string OldPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(50, MinimumLength = 6, ErrorMessage = "Confirm password must be between 6 and 50 characters")]
        public string confirmpassword { get; set; }
    }


    public class EditDetails
    {

        public string txtContactName { get; set; }

        //[Required(ErrorMessage = "Please Enter Company Phone No.")]
        //[RegularExpression(@"^[0-9]{0,11}$", ErrorMessage = "Number not valid! Please enter valid number.")]
        public string txtPhone { get; set; }


        public string txtAltPhone { get; set; }

        public string txtAddress { get; set; }


        public string txtpostbox { get; set; }

        //[Required]
        //[RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*",ErrorMessage="Email Not Valid!Please enter valid email address.")]
        public string txtemail { get; set; }

        //[Required]
        //[RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Email Not Valid!Please enter valid email address.")]
        public string txtaltemail { get; set; }

        public string txtdesg { get; set; }

        public IEnumerable<SelectListItem> ListStatesNames { get; set; }
        public Int64 StateID { get; set; }
        public int SelectedStateId { get; set; }
        public SelectList StateOptions { get; set; }

        public IEnumerable<SelectListItem> ListCityNames { get; set; }
        public Int64 CityID { get; set; }
        public int SelectedCityId { get; set; }
        public SelectList CityOptions { get; set; }


    }
    public class UserDashboardWebModel : SearchBarWebModel
    {
       
        //For Image cropping
        public ProfileViewModel Profile { get; set; }
        public bool IsUrl { get; set; }


        [Display(Name = "Upload Image")]
        public HttpPostedFileBase File { get; set; }

        public bool IsFile { get; set; }
        [Range(0, int.MaxValue)]
        public int X { get; set; }

        [Range(0, int.MaxValue)]
        public int Y { get; set; }

        [Range(1, int.MaxValue)]
        public int Width { get; set; }

        [Range(1, int.MaxValue)]
        public int Height { get; set; }

        public int pageNum
        { get; set; }
        public Int32 UserId { get; set; }
        public int rowsPerPage
        { get; set; }
        public int totalPages { get; set; }

        //public ResultUserDBTable ResultUserDBTab
        //{ get; set; }
        public WebResultUserDashboard webResultUserDashboard { get; set; }
        public EditDetails editDetails { get; set; }
        public ResultWatchList resultWatchList { get; set; }
        public WebResultRequirementPosted webResultRequirementPosted { get; set; }
        public WebResultReviewPosted webResultReviewPosted { get; set; }
        public ChangePassword changePassword { get; set; }
        //public void getWebUserDashboardDetails()
        //{
        //    ResultUserDBTab = prc_get_cuserdetails(UserId, "Vconnect Website");
        //    //totalPages = listResSetTab.BusinessCount[0].totalcount.Value / rowsPerPage;

        //}
        //123
        public void WebUserDashboardDetails()
        {
            webResultUserDashboard = prc_get_cuserdashboarddetails(UserId);
            //totalPages = listResSetTab.BusinessCount[0].totalcount.Value / rowsPerPage;

        }
        public int WebChangePassword(ChangePassword cpwd, string pwd)
        {
            int result = 0;

            result = prc_update_userPassword(cpwd, UserId, pwd);


            return result;
        }
        public void WebGetWatchlist()
        {
            resultWatchList = prc_AddUp_userwatchlist(UserId, 0);


        }
        public void WebGetUserReqPosted()
        {

            webResultRequirementPosted = prc_get_userbusinessrequests(pageNum, rowsPerPage, UserId);
            //  totalPages = webResultReuirementPosted.totalresult[0].TotalRecords / rowsPerPage;


        }
        public void WebGetUserReviewPosted()
        {

            webResultReviewPosted = prc_get_userbusinessreviews(pageNum, rowsPerPage, UserId);
            //  totalPages = webResultReuirementPosted.totalresult[0].TotalRecords / rowsPerPage;


        }

        public int WebEditDetails(EditDetails editdetails, int stateid, int cityid, string sex, string dob)
        {
            //123
            int result = 0;

            result = prc_cupdate_userdetails(editdetails, stateid, cityid, sex, dob, UserId);


            return result;
        }


        public int prc_cupdate_userphoto_opt(Int64 userid, string pathname, Int64 modifiedby)
        {
            int result=0;
          
                using (var db = new VconnectDBContext29())
                {
                    try
                    {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[prc_cupdate_userphoto_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@contentid", userid));
                    cmd.Parameters.Add(new SqlParameter("@photo", pathname));
                    cmd.Parameters.Add(new SqlParameter("@modifiedby", modifiedby));
                    SqlParameter parameter1 = new SqlParameter("@err", SqlDbType.Int);
                    parameter1.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(parameter1);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    result = Convert.ToInt16(cmd.Parameters["@err"].Value.ToString());
                }
           
                catch
            {
            }
            finally
            {
                db.Database.Connection.Close();
            }
             }
            return result;
            }
        public int BusinessReview(Int64 userid, Int64 Businessid)
        {
            int result = 0;
            try
            {
                using (var db = new VconnectDBContext29())
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[prc_AddUp_businessReviewList]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@userid", userid));
                    cmd.Parameters.Add(new SqlParameter("@businessid", Businessid));
                    cmd.Parameters.Add(new SqlParameter("@source", "web"));
                    SqlParameter Err = new SqlParameter("@Err", SqlDbType.Int);
                    Err.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(Err);
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    result = (int)cmd.Parameters["@Err"].Value;
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            return result;
        }
        public int Follow(int customerid,int followedby,int status,string source)
        {
            int result;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_followthisuser]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@customerid", customerid));
                cmd.Parameters.Add(new SqlParameter("@followedby", followedby));
                cmd.Parameters.Add(new SqlParameter("@source", source));
                cmd.Parameters.Add(new SqlParameter("@isfollowed", status));
                SqlParameter err = new SqlParameter("@err", SqlDbType.Int);
                err.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(err);
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteReader();
                result = (int)cmd.Parameters["@Err"].Value;
            }
            return result;
        }

        public int FollowCheck(int customerid, int followedby)
        {
            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_followthisuserCheckDetails]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@userid", customerid));
                cmd.Parameters.Add(new SqlParameter("@loginid", followedby));
                try
                {
                    SqlParameter Err = new SqlParameter("@Err", SqlDbType.Int);
                    Err.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(Err);
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    result = (int)cmd.Parameters["@Err"].Value;
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    //db.Database.Connection.Close();
                }
            }
            return result;
        }
        //notificationsReturn
        public UserInfo notificationsReturn(int? loginid,int Page=1)
        {
            UserInfo userInfo = new UserInfo();
            List<notificationActionmodel> notifications = new List<notificationActionmodel>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_user_notifications]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Page", Page));
                cmd.Parameters.Add(new SqlParameter("@userid", loginid));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();
                    notifications = ((IObjectContextAdapter)db).ObjectContext.Translate<notificationActionmodel>(reader).ToList();

                    userInfo.notifications = notifications;
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    //db.Database.Connection.Close();
                }
            }
            return userInfo;
        }
        //socialactivityReturn notificationsReturn
        public UserInfo socialactivityReturn(int? loginid)
        {
            UserInfo userInfo = new UserInfo();
            List<socialactivitynmodel> socialactivity = new List<socialactivitynmodel>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_user_feed_details_url]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@_top", 50));
                cmd.Parameters.Add(new SqlParameter("@userid", loginid));
                cmd.Parameters.Add(new SqlParameter("@source", "Web"));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();
                    socialactivity = ((IObjectContextAdapter)db).ObjectContext.Translate<socialactivitynmodel>(reader).ToList();

                    reader.NextResult();
                    userInfo.socialact = ((IObjectContextAdapter)db).ObjectContext.Translate<socialactivitydec>(reader).FirstOrDefault();

                    
                    userInfo.socialactivity = socialactivity;
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    //db.Database.Connection.Close();
                }
            }
            return userInfo;
        }

        public UserInfo getUsersInfo(string username)
        {
            UserInfo userInfo = new UserInfo();
            return userInfo;
        }


        public UserInfo getUserInfo(Int64? userid, int? Page, string logintype,int?loginid)
        {
            UserInfo userInfo = new UserInfo();
            UserDetail userDetail = new UserDetail();
            UserFollowid userFollowid = new UserFollowid();
            List<menucount> usermenucount = new List<menucount>();
            List<UserBusiness> userBusiness = new List<UserBusiness>();
            List<UserClaimedBusiness> userClaimedBusiness = new List<UserClaimedBusiness>();
            List<UserDetailPercentage> userDetailPercentage = new List<UserDetailPercentage>();
            List<UserActivity> userActivity = new List<UserActivity>();
            List<Userfollowersuggestion> userfollowersuggestion = new List<Userfollowersuggestion>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = loginid.HasValue && loginid.Value > 0 ? "[dbo].[prc_get_userdashboardNew_details]" : "[dbo].[prc_get_userprofile_details]";
                //cmd.CommandText = "[dbo].[prc_get_userdashboardNew_details]";
                //cmd.CommandText = "[dbo].[prc_get_userprofile_details]";                
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@Page", Page));
                cmd.Parameters.Add(new SqlParameter("@logintype", logintype));
                cmd.Parameters.Add(new SqlParameter("@loginid", loginid));
                 try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                 
                    if (loginid.HasValue && loginid.Value > 0)
                    {
                        userDetail = ((IObjectContextAdapter)db).ObjectContext.Translate<UserDetail>(reader).FirstOrDefault();
                       
                        //reader.NextResult();
                        //userBusiness = ((IObjectContextAdapter)db).ObjectContext.Translate<UserBusiness>(reader).ToList();
                        
                        reader.NextResult();
                        userClaimedBusiness = ((IObjectContextAdapter)db).ObjectContext.Translate<UserClaimedBusiness>(reader).ToList();
                        
                        reader.NextResult();
                        userDetailPercentage = ((IObjectContextAdapter)db).ObjectContext.Translate<UserDetailPercentage>(reader).ToList();
                       
                        reader.NextResult();
                        usermenucount = ((IObjectContextAdapter)db).ObjectContext.Translate<menucount>(reader).ToList();
                       
                        reader.NextResult();
                        userActivity = ((IObjectContextAdapter)db).ObjectContext.Translate<UserActivity>(reader).ToList();

                        reader.NextResult();
                        userfollowersuggestion = ((IObjectContextAdapter)db).ObjectContext.Translate<Userfollowersuggestion>(reader).ToList();

                        reader.NextResult();
                        userFollowid = ((IObjectContextAdapter)db).ObjectContext.Translate<UserFollowid>(reader).FirstOrDefault();
                        userInfo.userDetail = userDetail;
                        userInfo.usermenucount = usermenucount;
                        userInfo.userClaimedBusiness = userClaimedBusiness;                       
                        userInfo.userBusiness = userBusiness;
                        userInfo.userActivity = userActivity;
                        userInfo.userDetailPercentage = userDetailPercentage;
                        userInfo.userfollowersuggestion = userfollowersuggestion;
                        userInfo.userFollowid = userFollowid;
                                                
                    }
                    else
                    {
                        userDetail = ((IObjectContextAdapter)db).ObjectContext.Translate<UserDetail>(reader).FirstOrDefault();
                        
                        reader.NextResult();
                        userClaimedBusiness = ((IObjectContextAdapter)db).ObjectContext.Translate<UserClaimedBusiness>(reader).ToList();

                        reader.NextResult();
                        usermenucount = ((IObjectContextAdapter)db).ObjectContext.Translate<menucount>(reader).ToList();

                        userInfo.userDetail = userDetail;
                        userInfo.usermenucount = usermenucount;
                        userInfo.userClaimedBusiness = userClaimedBusiness;                        
                    }


                   
                    //userDetail = ((IObjectContextAdapter)db).ObjectContext.Translate<UserDetail>(reader).FirstOrDefault();
                    //if (loginid.HasValue && loginid.Value > 0)
                    //{
                    //    reader.NextResult();
                    //    userBusiness = ((IObjectContextAdapter)db).ObjectContext.Translate<UserBusiness>(reader).ToList();
                    //}
                    //reader.NextResult();
                    //userClaimedBusiness = ((IObjectContextAdapter)db).ObjectContext.Translate<UserClaimedBusiness>(reader).ToList();

                    //if (loginid.HasValue && loginid.Value>0)
                    //{
                        
                    //    reader.NextResult();
                    //    userDetailPercentage = ((IObjectContextAdapter)db).ObjectContext.Translate<UserDetailPercentage>(reader).ToList();
                    //}
                    //reader.NextResult();
                    //usermenucount = ((IObjectContextAdapter)db).ObjectContext.Translate<menucount>(reader).ToList();
                    //if (loginid.HasValue && loginid.Value>0)
                    //{
                    //    reader.NextResult();
                    //    userActivity = ((IObjectContextAdapter)db).ObjectContext.Translate<UserActivity>(reader).ToList();

                    //    reader.NextResult();
                    //    userfollowersuggestion = ((IObjectContextAdapter)db).ObjectContext.Translate<Userfollowersuggestion>(reader).ToList();

                    //    reader.NextResult();
                    //    userFollowid = ((IObjectContextAdapter)db).ObjectContext.Translate<UserFollowid>(reader).FirstOrDefault();
                    //}
                    //userInfo.userDetail = userDetail;
                    // userInfo.usermenucount = usermenucount;
                     
                    // userInfo.userClaimedBusiness = userClaimedBusiness;
                    // if (loginid.HasValue && loginid.Value > 0)
                    // {
                    //     userInfo.userBusiness = userBusiness;
                    //     userInfo.userActivity = userActivity;
                         
                    //     userInfo.userDetailPercentage = userDetailPercentage;
                    //     userInfo.userfollowersuggestion = userfollowersuggestion;
                    //     userInfo.userFollowid = userFollowid;
                    // }
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
                finally
                {
                    //db.Database.Connection.Close();
                }
            }

        //    public UserDetails userDetails { get; set; }
        //public List<UserBusiness> userBusiness { get; set; }
        //public List<UserReviewBusiness> userReviewBusiness { get; set; }
        //public List<UserLikeBusiness> userLikeBusiness { get; set; }
        //public List<UserSaveBusiness> userSaveBusiness { get; set; }

        //    BizDetails bizDetails = new BizDetails();
        //    List<BizDetailWeb> bizMaster = new List<BizDetailWeb>();
        //    //List<BizWorkingHour> bizWorkingHour = new List<BizWorkingHour>();
        //    //List<BizBranches> bizBranches = new List<BizBranches>();
        //    //List<BizAttachment> bizAttachment = new List<BizAttachment>();
        //    List<BizProduct> bizProduct = new List<BizProduct>();
        //    List<BizService> bizService = new List<BizService>();
        //    //List<BizPhotoCount> bizPhotoCount = new List<BizPhotoCount>();
        //    //List<BizPhotoGallery> bizPhotoGallery = new List<BizPhotoGallery>();
        //    //List<BizVideo> bizVideo = new List<BizVideo>();
        //    //List<BizPhotoContact> bizPhotoContact = new List<BizPhotoContact>();
        //    //List<BizProductInfo> bizProductInfo = new List<BizProductInfo>();
        //    //List<BizServiceInfo> bizServiceInfo = new List<BizServiceInfo>();
        //    //List<BizVideoInfo> bizVideoInfo = new List<BizVideoInfo>();

        //    using (var db = new VconnectDBContext29())
        //    {
        //        var cmd = db.Database.Connection.CreateCommand();
        //        cmd.CommandText = "[dbo].[prc_get_businessdetailsnew_test_beta]";
        //        cmd.CommandTimeout = 600;
        //        //cmd.CommandText = "[dbo].[prc_get_businessdetailsnew_test]";
        //        //cmd.CommandText = "[dbo].[prc_get_businessdetailsnew]";

        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        cmd.Parameters.Add(new SqlParameter("@businessid", businessId));
        //        cmd.Parameters.Add(new SqlParameter("@oldbusinessid", 0));
        //        cmd.Parameters.Add(new SqlParameter("@vcid", 0));
        //        if (userId != null && userId != 0)
        //        {
        //            cmd.Parameters.Add(new SqlParameter("@userid", userId));
        //        }
        //        // try
        //        {
        //            var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
        //            connection.Open();

        //            var reader = cmd.ExecuteReader();
        //            bizMaster = ((IObjectContextAdapter)db).ObjectContext.Translate<BizDetailWeb>(reader).ToList();

        //            //reader.NextResult();
        //            //bizWorkingHour = ((IObjectContextAdapter)db).ObjectContext.Translate<BizWorkingHour>(reader).ToList();

        //            //reader.NextResult();
        //            //bizBranches = ((IObjectContextAdapter)db).ObjectContext.Translate<BizBranches>(reader).ToList();

        //            //reader.NextResult();
        //            //bizAttachment = ((IObjectContextAdapter)db).ObjectContext.Translate<BizAttachment>(reader).ToList();

        //            reader.NextResult();
        //            bizProduct = ((IObjectContextAdapter)db).ObjectContext.Translate<BizProduct>(reader).ToList();

        //            reader.NextResult();
        //            bizService = ((IObjectContextAdapter)db).ObjectContext.Translate<BizService>(reader).ToList();

        //            //reader.NextResult();
        //            //bizPhotoCount = ((IObjectContextAdapter)db).ObjectContext.Translate<BizPhotoCount>(reader).ToList();

        //            //reader.NextResult();
        //            //bizPhotoGallery = ((IObjectContextAdapter)db).ObjectContext.Translate<BizPhotoGallery>(reader).ToList();

        //            //reader.NextResult();
        //            //bizVideo = ((IObjectContextAdapter)db).ObjectContext.Translate<BizVideo>(reader).ToList();

        //            //reader.NextResult();
        //            //bizPhotoContact = ((IObjectContextAdapter)db).ObjectContext.Translate<BizPhotoContact>(reader).ToList();

        //            //reader.NextResult();
        //            //bizProductInfo = ((IObjectContextAdapter)db).ObjectContext.Translate<BizProductInfo>(reader).ToList();

        //            //reader.NextResult();
        //            //bizServiceInfo = ((IObjectContextAdapter)db).ObjectContext.Translate<BizServiceInfo>(reader).ToList();

        //            //reader.NextResult();
        //            //bizVideoInfo = ((IObjectContextAdapter)db).ObjectContext.Translate<BizVideoInfo>(reader).ToList();

        //            bizDetails.BizMaster = bizMaster;
        //            //bizDetails.BizWorkingHour = bizWorkingHour;
        //            //bizDetails.BizBranches = bizBranches;
        //            //bizDetails.BizAttachment = bizAttachment;
        //            bizDetails.BizProduct = bizProduct;
        //            bizDetails.BizService = bizService;
        //            //bizDetails.BizPhotoCount = bizPhotoCount;
        //            //bizDetails.BizPhotoGallery = bizPhotoGallery;
        //            //bizDetails.BizVideo = bizVideo;
        //            //bizDetails.BizPhotoContact = bizPhotoContact;
        //            //bizDetails.BizProductInfo = bizProductInfo;
        //            //bizDetails.BizServiceInfo = bizServiceInfo;
        //            //bizDetails.BizVideoInfo = bizVideoInfo;

        //        }
        //        // catch (Exception ex)
        //        {
        //        }
        //        // finally
        //        {
        //            //db.Database.Connection.Close();
        //        }
        //    }
        //    //return result;
        //    return bizDetails;
            return userInfo;
        }

        //public ResultUserDBTable prc_get_cuserdetails(Int32 contentid, string source)
        //{
        //    ResultUserDBTable resultUserDBTableObj = new ResultUserDBTable();
        //    List<RecentlyViewedBusiness> RecentlyViewedBusiness = new List<RecentlyViewedBusiness>();
        //    List<RecentlyPostedReviews> RecentlyPostedReviews = new List<RecentlyPostedReviews>();
        //    List<KeywordSearchLocation> KeywordSearchLocation = new List<KeywordSearchLocation>();
        //    List<UserDBDetails> UserDBDetails = new List<UserDBDetails>();

        //    using (var db = new VconnectDBContext29())
        //    {

        //        // Create a SQL command to execute the sproc
        //        var cmd = db.Database.Connection.CreateCommand();
        //        cmd.CommandText = "[dbo].[prc_get_cuserdetails_beta]";
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        //Add parameters to command object
        //        cmd.Parameters.Add(new SqlParameter("@contentid", contentid));
        //        cmd.Parameters.Add(new SqlParameter("@source", "VConnect Website"));

        //        try
        //        {
        //            var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
        //            connection.Open();
        //            var reader = cmd.ExecuteReader();
        //            if (reader.HasRows)
        //            {
        //                UserDBDetails = ((IObjectContextAdapter)db).ObjectContext.Translate<UserDBDetails>(reader).ToList();


        //                // Move to second result set and read Posts
        //                reader.NextResult();

        //                RecentlyPostedReviews = ((IObjectContextAdapter)db).ObjectContext.Translate<RecentlyPostedReviews>(reader).ToList();
        //                //  homeWAPModel.totalPages = (int)Math.Ceiling((double)listResultSetTable.BusinessCount[0].totalcount.Value / homeWAPModel.rowsPerPage);
        //                reader.NextResult();
        //                KeywordSearchLocation = ((IObjectContextAdapter)db).ObjectContext.Translate<KeywordSearchLocation>(reader).ToList();
        //                reader.NextResult();
        //                RecentlyViewedBusiness = ((IObjectContextAdapter)db).ObjectContext.Translate<RecentlyViewedBusiness>(reader).ToList();
        //                //  KeywordSearchedPage = ((IObjectContextAdapter)db).ObjectContext.Translate<KeywordSearchedPage>(reader).ToList();

        //                resultUserDBTableObj.UserDBDetails = UserDBDetails;
        //                resultUserDBTableObj.KeywordSearchLocation = KeywordSearchLocation;
        //                resultUserDBTableObj.RecentlyPostedReviews = RecentlyPostedReviews;
        //                resultUserDBTableObj.RecentlyViewedBusiness = RecentlyViewedBusiness;

        //            }
        //            //listResSetTabParent.Add(listResultSetTableObj);
        //        }
        //        catch (Exception ex)
        //        {
        //            Console.WriteLine(ex.ToString());
        //        }

        //        finally
        //        {
        //            db.Database.Connection.Close();
        //        }
        //        return resultUserDBTableObj;
        //    }

        //}
        public WebResultUserDashboard prc_get_cuserdashboarddetails(Int32 contentid)
        {
            WebResultUserDashboard webResultUserDashboard = new WebResultUserDashboard();
            BusinessVisitCount businessVisitCount = new BusinessVisitCount();
            KeywordSearched keywordSearched = new KeywordSearched();
            BannerClicked bannerClicked = new BannerClicked();
            SMSToMobile smsToMobile = new SMSToMobile();
            EmailSent emailSent = new EmailSent();

            List<KeywordSearchedPage> keywordSearchedPage = new List<KeywordSearchedPage>();


            using (var db = new VconnectDBContext29())
            {

                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_cuserdashboarddetails]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@userid", contentid));

                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        businessVisitCount = ((IObjectContextAdapter)db).ObjectContext.Translate<BusinessVisitCount>(reader).SingleOrDefault();


                        // Move to second result set and read Posts
                        reader.NextResult();
                        keywordSearched = ((IObjectContextAdapter)db).ObjectContext.Translate<KeywordSearched>(reader).SingleOrDefault();


                        reader.NextResult();
                        bannerClicked = ((IObjectContextAdapter)db).ObjectContext.Translate<BannerClicked>(reader).SingleOrDefault();


                        reader.NextResult();
                        smsToMobile = ((IObjectContextAdapter)db).ObjectContext.Translate<SMSToMobile>(reader).SingleOrDefault();


                        reader.NextResult();
                        emailSent = ((IObjectContextAdapter)db).ObjectContext.Translate<EmailSent>(reader).SingleOrDefault();

                        reader.NextResult();
                        keywordSearchedPage = ((IObjectContextAdapter)db).ObjectContext.Translate<KeywordSearchedPage>(reader).ToList();


                        webResultUserDashboard.businessVisitCount = businessVisitCount;
                        webResultUserDashboard.bannerClicked = bannerClicked;
                        webResultUserDashboard.keywordSearched = keywordSearched;


                        webResultUserDashboard.smsToMobile = smsToMobile;
                        webResultUserDashboard.emailSent = emailSent;
                        webResultUserDashboard.keywordSearchedPage = keywordSearchedPage;
                        //resultUserDBTableObj.UserDBDetails = UserDBDetails;


                        //resultUserDBTableObj.KeywordSearchLocation = KeywordSearchLocation;
                        //resultUserDBTableObj.RecentlyPostedReviews = RecentlyPostedReviews;
                        //resultUserDBTableObj.RecentlyViewedBusiness = RecentlyViewedBusiness;

                    }
                    //listResSetTabParent.Add(listResultSetTableObj);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    db.Database.Connection.Close();
                }
                return webResultUserDashboard;
            }

        }
        public int prc_update_userPassword(ChangePassword cpwd, Int32 contentid, string pwd)
        {
            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_update_userPasswordSept]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@customerid", contentid));
                cmd.Parameters.Add(new SqlParameter("@Oldpassword", cpwd.OldPassword));
                cmd.Parameters.Add(new SqlParameter("@Password", pwd));
                cmd.Parameters.Add(new SqlParameter("@encriptedpassword", Vconnect.Common.Utility.Encrypt.encryptPassword(pwd.Trim())));
                cmd.Parameters.Add(new SqlParameter("@Err", SqlDbType.VarChar, 10));
                cmd.Parameters["@Err"].Direction = ParameterDirection.Output;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    result = Convert.ToInt32(cmd.Parameters["@Err"].Value.ToString());
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    db.Database.Connection.Close();
                }

            }
            return result;
        }
        public ResultWatchList prc_AddUp_userwatchlist(Int32 userid, Int64 businessid)
        {
            ResultWatchList resultWatchList = new ResultWatchList();
            List<watchlist> watchlist = new List<watchlist>();
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_AddUp_userwatchlist]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@Businessid", "0"));

                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        watchlist = ((IObjectContextAdapter)db).ObjectContext.Translate<watchlist>(reader).ToList();
                        resultWatchList.watchlist = watchlist;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    db.Database.Connection.Close();
                }
                return resultWatchList;
            }
        }

        public WebResultRequirementPosted prc_get_userbusinessrequests(int Pagenum, int rowsperpage, Int32 userid)
        {
            WebResultRequirementPosted webResReqPosted = new WebResultRequirementPosted();
            // List<RequirementPosted> requirementPosted = new List<RequirementPosted>();
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_userbusinessrequests]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@pageNum", 1));
                cmd.Parameters.Add(new SqlParameter("@rowsPerPage", 20));
                cmd.Parameters.Add(new SqlParameter("@senderid", userid));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        webResReqPosted.requirementPosted = ((IObjectContextAdapter)db).ObjectContext.Translate<RequirementPosted>(reader).ToList();
                        reader.NextResult();
                        webResReqPosted.totalresult = ((IObjectContextAdapter)db).ObjectContext.Translate<TotalRec>(reader).ToList();


                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    db.Database.Connection.Close();
                }
                return webResReqPosted;
            }
        }

        public WebResultReviewPosted prc_get_userbusinessreviews(int Pagenum, int rowsperpage, Int32 userid)
        {
            WebResultReviewPosted webResReviewPosted = new WebResultReviewPosted();
            // List<RequirementPosted> requirementPosted = new List<RequirementPosted>();
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_userbusinessreviews_opt]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@pageNum", 1));
                cmd.Parameters.Add(new SqlParameter("@rowsPerPage", 20));
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@totalreviews", SqlDbType.BigInt));
                cmd.Parameters["@totalreviews"].Direction = ParameterDirection.Output;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        webResReviewPosted.reviewPosted = ((IObjectContextAdapter)db).ObjectContext.Translate<ReviewPosted>(reader).ToList();
                        reader.NextResult();
                        //webResReviewPosted.totalresult = ((IObjectContextAdapter)db).ObjectContext.Translate<TotalRecords>(reader).ToList();


                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    db.Database.Connection.Close();
                }
                return webResReviewPosted;
            }
        }
        public int prc_cupdate_userdetails(EditDetails editdetails, int stateid, int cityid, string sex, string dob, Int32 contentid)
        {
            int result = 0;


            Regex rgx1 = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");

            ////for alternate number.
            //if (editdetails.txtAltPhone != null)
            //{
            //    if (!string.IsNullOrEmpty(editdetails.txtAltPhone.Trim().TrimEnd().TrimStart()) || !string.IsNullOrWhiteSpace(editdetails.txtAltPhone.Trim().TrimEnd().TrimStart()))
            //    {
            //        int result1 = Vconnect.Common.Utility.isValidPhone(editdetails.txtAltPhone.ToString());
            //        if (result1 == 0)
            //        {
            //            result = 1;   //Alternate phone no. is not valid.
            //            return result;
            //        }
            //    }
            //}
            //else
            //{
            //    editdetails.txtAltPhone = " ";
            //}

            ////for alternate email
            //if (editdetails.txtaltemail != null)
            //{
            //    if (!string.IsNullOrEmpty(editdetails.txtaltemail.Trim().TrimEnd().TrimStart()) || !string.IsNullOrWhiteSpace(editdetails.txtaltemail.Trim().TrimEnd().TrimStart()))
            //    {
            //        if (!rgx1.IsMatch(editdetails.txtaltemail.ToString()))
            //        {
            //            result = 3; //Enter a valid Email
            //            return result;
            //        }
            //    }
            //}
            //else
            //{
            //    editdetails.txtaltemail = " ";
            //}

            //for email
            if (editdetails.txtemail != null)
            {
                if (!string.IsNullOrEmpty(editdetails.txtemail.Trim().TrimEnd().TrimStart()) && !string.IsNullOrWhiteSpace(editdetails.txtemail.Trim().TrimEnd().TrimStart()))
                {
                    if (!rgx1.IsMatch(editdetails.txtemail.ToString()))
                    {
                        result = 2;  //Enter a valid Email
                        return result;
                    }
                }
            }

            //for phone number...
            int result5 = Vconnect.Common.Utility.isValidPhone(editdetails.txtPhone.ToString());
            if (result5 == 0)
            {

                result = 5;   //phone no. is not valid.
            }
            else
            {
                using (var db = new VconnectDBContext29())
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[prc_cupdate_userdetails_beta]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@contentid", contentid));
                    cmd.Parameters.Add(new SqlParameter("@contactname", editdetails.txtContactName));
                    //  if (!string.IsNullOrEmpty(editdetails.txtAddress.Trim().TrimEnd().TrimStart()) && !string.IsNullOrWhiteSpace(editdetails.txtAddress.Trim().TrimEnd().TrimStart()))
                    if (string.IsNullOrEmpty(editdetails.txtAddress))
                    {
                        cmd.Parameters.Add(new SqlParameter("@address", ""));
                        // cmd.Parameters.Add(new SqlParameter("@address", editdetails.txtAddress));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@address", editdetails.txtAddress));
                        //cmd.Parameters.Add(new SqlParameter("@address", ""));
                    }
                    cmd.Parameters.Add(new SqlParameter("@stateid", stateid));
                    cmd.Parameters.Add(new SqlParameter("@cityid", cityid));
                    //cmd.Parameters.Add(new SqlParameter("@countryid", 1));
                    cmd.Parameters.Add(new SqlParameter("@zipcode", editdetails.txtpostbox));
                    cmd.Parameters.Add(new SqlParameter("@phone1", editdetails.txtPhone));
                    //  cmd.Parameters.Add(new SqlParameter("@phone2", editdetails.txtAltPhone));
                    cmd.Parameters.Add(new SqlParameter("@email", editdetails.txtemail));
                    // cmd.Parameters.Add(new SqlParameter("@alternateemail", editdetails.txtaltemail));
                    // cmd.Parameters.Add(new SqlParameter("@photo", " "));
                    cmd.Parameters.Add(new SqlParameter("@sex", sex.Replace("Female", "2").Replace("Male", "1")));
                    cmd.Parameters.Add(new SqlParameter("@dateofbirth", dob));
                    // cmd.Parameters.Add(new SqlParameter("@EditProfile", 1));
                    cmd.Parameters.Add(new SqlParameter("@Err", SqlDbType.VarChar, 10));
                    cmd.Parameters["@Err"].Direction = ParameterDirection.Output;
                    try
                    {
                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        cmd.ExecuteNonQuery();
                        result = Convert.ToInt32(cmd.Parameters["@Err"].Value.ToString());
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        db.Database.Connection.Close();
                    }

                }
            }
            return result;
        }
        public string getUserContentid(string userid)
        {
            int IDurl = 0;
            string url = "0";
            int IDurl1 = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_getUserContentid]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                if (int.TryParse(userid, out IDurl1))
                {
                    cmd.Parameters.Add(new SqlParameter("@contentid", userid));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@userurl", userid));
                }
                cmd.Parameters.Add(new SqlParameter("@errorcode", SqlDbType.VarChar, 100));
                cmd.Parameters["@errorcode"].Direction = ParameterDirection.Output;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                        try
                        {
                            if (int.TryParse(userid, out IDurl1))
                            {
                                url = reader[1].ToString().Trim();
                            }
                            else
                            {
                                url = reader[0].ToString().Trim();
                            }
                        }
                        catch (Exception ex)
                        {
                            log.LogMe(ex);
                            IDurl = 0;
                        }

                }
                catch (Exception ex)
                {
                }
                finally
                {
                    //db.Database.Connection.Close();
                }
            }
            return url.ToLower();
        }
    }

    public class ProfileViewModel
    {
        [UIHint("ProfileImage")]
        public string ImageUrl { get; set; }
    }

    public class ResultUserDBTable
    {
        public List<UserDBDetails> UserDBDetails { get; set; }
        public List<RecentlyViewedBusiness> RecentlyViewedBusiness { get; set; }
        public List<RecentlyPostedReviews> RecentlyPostedReviews { get; set; }
        public List<KeywordSearchLocation> KeywordSearchLocation { get; set; }
        public List<KeywordSearchedPage> KeywordSearchedPage { get; set; }

    }
    public class WebResultUserDashboard
    {
        public BusinessVisitCount businessVisitCount { get; set; }
        public KeywordSearched keywordSearched { get; set; }
        public BannerClicked bannerClicked { get; set; }
        public SMSToMobile smsToMobile { get; set; }
        public EmailSent emailSent { get; set; }
        public List<KeywordSearchedPage> keywordSearchedPage { get; set; }
    }
    public class ResultWatchList
    {
        public List<watchlist> watchlist { get; set; }
    }
    public class WebResultRequirementPosted
    {
        public List<TotalRec> totalresult { get; set; }
        public List<RequirementPosted> requirementPosted { get; set; }

    }
    public class WebResultReviewPosted
    {

        public List<ReviewPosted> reviewPosted { get; set; }

    }
    public class TotalRec
    {
        [Key]
        public Int32 TotalRecords { get; set; }

    }
    public class UserDBDetails
    {

        [Key]
        public Int32 contentid { get; set; }
        public virtual string title { get; set; }
        public virtual string contactname { get; set; }
        public virtual string houseno { get; set; }
        public virtual string address { get; set; }
        public virtual int? areaid { get; set; }
        public virtual int? cityid { get; set; }
        public virtual int? stateid { get; set; }
        public virtual int? zipcode { get; set; }
        public virtual int? countryid { get; set; }
        public virtual string phone { get; set; }
        public virtual string alternatephone { get; set; }
        public virtual string email { get; set; }
        public virtual string alternateemail { get; set; }
        public virtual string designation { get; set; }
        public virtual bool? isprimarycontact { get; set; }
        public virtual string sex { get; set; }
        public virtual string photo { get; set; }
        public virtual string dateofbirth { get; set; }
        public virtual bool? isemailveified { get; set; }
        public virtual bool? ismobileverified { get; set; }
        public virtual int isverified { get; set; }
        public virtual string state { get; set; }
        public virtual string city { get; set; }
    }
    public class RecentlyPostedReviews
    {
        //123
        public virtual string businessname { get; set; }
        public virtual string businessurl { get; set; }
        public virtual DateTime createddate { get; set; }
        public virtual int? avgrating { get; set; }
    }
    public class RecentlyViewedBusiness
    {

        public virtual string reviewdetail { get; set; }
        public virtual string businessname { get; set; }
        public virtual DateTime createddate { get; set; }

    }
    public class KeywordSearchLocation
    {

        public virtual string searchkeyword { get; set; }
        public virtual string searchloaction { get; set; }
        public virtual DateTime createddate { get; set; }
    }
    public class BusinessVisitCount
    {
        [Key]
        public int TotalCount { get; set; }
        public int ThisMonthCount { get; set; }
    }
    public class KeywordSearched
    {
        [Key]
        public int TotalCount { get; set; }
        public int ThisMonthCount { get; set; }
    }
    public class BannerClicked
    {
        [Key]
        public int TotalCount { get; set; }
        public int ThisMonthCount { get; set; }
    }
    public class SMSToMobile
    {
        [Key]
        public int TotalCount { get; set; }
        public int ThisMonthCount { get; set; }
    }
    public class EmailSent
    {
        [Key]
        public int TotalCount { get; set; }
        public int ThisMonthCount { get; set; }
    }
    public class KeywordSearchedPage
    {
        [Key]
        public virtual Int64? contentid { get; set; }
        public virtual string searchkeyword { get; set; }
        public virtual string searchlocation { get; set; }
        public virtual string createddate { get; set; }
        public virtual string Status { get; set; }
    }
    public class watchlist
    {
        [Key]
        public virtual Int64? businessid { get; set; }
        public virtual string businessname { get; set; }
        public virtual Int64 userid { get; set; }
        public virtual Int32 createdby { get; set; }
        public virtual string createddate { get; set; }
        public virtual string businessurl { get; set; }
    }


    public class RequirementPosted
    {

        public virtual string requirement { get; set; }
        public virtual string createddate { get; set; }
    }

    public class ReviewPosted
    {

        public virtual string reviewdetail { get; set; }
        public virtual string createddate { get; set; }
        public virtual int ReviewStatus { get; set; }
    }


    public class UserBusiness
    {
        public  Int32? contentid { get; set; }
        public string businessname { get; set; }
        public int? isdefault { get; set; }
    }
    public class menucount
    {
        public Int32? likecount { get; set; }
        public Int32? reviewcount { get; set; }
        public Int32? followercount { get; set; }
        public Int32? followingcount { get; set; }
        public Int32? savedcount { get; set; }
        public Int32? photocount { get; set; }
        public Int32? activitycount { get; set; }
        public string contactname { get; set; }
        public Int32? usertype1 { get; set; }
    }
    public class UserSaveBusiness
    {
        public Int32? businessid { get; set; }
        public Int32? userid { get; set; }
        public int? status { get; set; }
        public DateTime? createddate { get; set; }
        public string businessname { get; set; }
        public string photo { get; set; }
        public string businessurl { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string address1 { get; set; } 
    }
    public class UserLikeBusiness
    {
        public Int32? ID { get; set; }
        public Int32? businessid { get; set; }
        public Int32? userid { get; set; }        
        public int? status { get; set; }
        public DateTime? createddate { get; set; }
        public string businessname { get; set; }        
        public string photo { get; set; }
        public string businessurl { get; set; }
        public string city { get; set; }
        public string state { get; set; } 
    }
    public class Userfollowers 
    {
        public Int32? contentid { get; set; }
        public string photo { get; set; }
        public string name { get; set; }
        public Int32? followerscnt { get; set; }
        public Int32? reviewscnt { get; set; }
        public Int32? isBoth { get; set; }
        public Int32? isCurFollow { get; set; }
        public string userurl { get; set; }
    }
    public class Userfollowersuggestion 
    {
        public Int32? userid { get; set; }
        public string photo { get; set; }
        public string name { get; set; }
        public Int32? followerscnt { get; set; }
        public Int32? followingscnt { get; set; }
        public Int32? reviewscnt { get; set; }
        public string userurl { get; set; }
    }
    public class Userfolloweing
    {
        public Int32? contentid { get; set; }
        public string photo { get; set; }
        public string name { get; set; }
        public Int32? followerscnt { get; set; }
        public Int32? reviewscnt { get; set; }
        public Int32? isBoth { get; set; }
        public Int32? isCurFollow { get; set; }
        public string userurl { get; set; }
        
    }
    public class notificationsetting
    {
        public int lke { get; set; }
        public string contactname { get; set; }
        public DateTime createddate { get; set; }
        public string detail { get; set; }
        public int activity { get; set; }
        public string businessname { get; set; }
        public string address { get; set; }
        public string url { get; set; }
        public string photo { get; set; }
        public string companylogo { get; set; }
        public string userphoto { get; set; }
        public string type { get; set; }
        public Int64 businessid { get; set; }
    }

    public class UserReviewBusiness 
    {
        public Int32? businessid { get; set; }
        public Int32? userid { get; set; }
        public int? status { get; set; }
        public DateTime? createddate { get; set; }
        public string businessname { get; set; }
        public string photo { get; set; }
        public string businessurl { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string details { get; set; }
        public int? rate { get; set; } 
    }
    public class UserDetailPercentage
    {
        public string pwd { get; set; }
        public string lastname { get; set; }
        public string contactname { get; set; }
        public int? areaid { get; set; }
        public int? cityid { get; set; }
        public int? stateid { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string sex { get; set; }
        public string photo { get; set; }
        public DateTime? dateofbirth { get; set; }
        public string description { get; set; }
        public Boolean? isemailverified { get; set; }
        public Boolean? ismobileverified { get; set; }
        public int? isverified { get; set; }
    }

    public class UserFollowid
    {
        public int? err { get; set; }
    }
    public class socialactivitydec
    {
        public string description { get; set; }
    }
    public class UserDetail
    {
        public string contactname { get; set; }
        public string address { get; set; }
        //public int areaid { get; set; }
        public int? cityid { get; set; }
        public int? stateid { get; set; }
        //public int zipcode { get; set; }
        //public int countryid { get; set; }
        public string phone { get; set; }
        public string alternatephone { get; set; }
        public string email { get; set; }
        public string sex { get; set; }
        public string photo { get; set; }
        public string dateofbirth { get; set; }
        public string description { get; set; }
        public string area { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string facebookid { get; set; }
        public string twitterid { get; set; }
        public string googleid { get; set; }
        public string userurl { get; set; }
        public string lastname { get; set; }
    }
    public class UserActivity
    {
        public Int32? businessid { get; set; }
        public Int32? activity { get; set; }
        public Int32? userid { get; set; }
        public Int32? ID { get; set; }
        public string businessname { get; set; }
        public string businessurl { get; set; }
        public DateTime? createddate { get; set; }
    }

    //public class UserActivity
    //{
    //    public int? businessid { get; set; }
    //    public int? activity { get; set; }
    //    public int? userid { get; set; }
    //    public Int64? ID { get; set; }
    //    public string businessname { get; set; }
    //    public string businessurl { get; set; }
    //    public Nullable<DateTime> createddate { get; set; }
    //}
    //public class UserActivity
    //{
    //    public Int32? businessid { get; set; }
    //    public Int32? activity { get; set; }
    //    public Int32? userid { get; set; }
    //    public Int32? ID { get; set; }
    //    public string businessname { get; set; }
    //    public string businessurl { get; set; }
    //    public DateTime? createddate { get; set; }
    //}
    public class UserPhotoList
    {
        public Int32? businessid { get; set; }
        public Int32? createdby { get; set; }
        public int? isreview { get; set; }
        public DateTime? createddate { get; set; }
        public string businessname { get; set; }
        public string photosmax { get; set; }
    }
    public class UserClaimedBusiness
    {
        public Int32? contentid { get; set; }
        public string businessname { get; set; }
        public string businessurl { get; set; }
    }
    public class UserInfo : HomeModel
    {
        public string description { get; set; }
        public string loginname { get; set; }
        public int? pageNum { get; set; }
        public int? percentage { get; set; }
        public Int64? userid { get; set; }
        public int? loginid { get; set; }
        public socialactivitydec socialact { get; set; }
        public UserDetail userDetail { get; set; }
        public List<UserBusiness> userBusiness { get; set; }
        public List<UserReviewBusiness> userReviewBusiness { get; set; }
        public List<menucount> usermenucount { get; set; }
        public List<UserLikeBusiness> userLikeBusiness { get; set; }
        public List<Userfollowers> userfollowers { get; set; }
        public List<Userfollowersuggestion> userfollowersuggestion { get; set; }
        public List<Userfolloweing> userfolloweing { get; set; }
        public List<UserSaveBusiness> userSaveBusiness { get; set; }
        public List<UserActivity> userActivity { get; set; }
        public List<UserPhotoList> userPhotoList { get; set; }
        public List<UserClaimedBusiness> userClaimedBusiness { get; set; }
        public List<UserDetailPercentage> userDetailPercentage { get; set; }
        public UserFollowid userFollowid { get; set; }
        public List<socialactivitynmodel> socialactivity { get; set; }
        public List<notificationActionmodel> notifications { get; set; }
        public List<UserfollowDisplay> userfollowDisplay { get; set; }
    }
    public class UserSharePreferenceModel
    {
        [Key]
        public string IsFBShare { get; set; }
        public string IsTwShare { get; set; }
    }
    #region Profile Header
    public class profilemodel 
    {
        /// notification setting
        public List<ListUserNotificationsModel> listNotifications { get; set; }
        public List<ListUserNotificationsModel> DBListNotificationsSettings(int userid)
        {
            List<ListUserNotificationsModel> listUserNotifications = new List<ListUserNotificationsModel>();

            using (var db = new VconnectDBContext29())
            {
                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "prc_listusernotificationssettings";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@userid", userid));

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();

                    listUserNotifications = ((IObjectContextAdapter)db).ObjectContext.Translate<ListUserNotificationsModel>(reader).ToList();

                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);
                }
                finally { db.Database.Connection.Close(); }
            }
            return listUserNotifications;
        }
        public int DBSetUserNotifications(List<UserNotificationsModel> listUserNotificationsModel, int flag)
        {
            int resErr = 0;
            DataTable dtUserNotifications = new DataTable();

            using (var db = new VconnectDBContext29())
            {
                try
                {
                    dtUserNotifications = CommonMethods.ToDataTable(listUserNotificationsModel);

                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "prc_manageusernotifcations";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@usernotifications", dtUserNotifications));
                    cmd.Parameters.Add(new SqlParameter("@flag", flag));
                    var err = new SqlParameter("@Err", SqlDbType.Int);
                    err.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(err);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteNonQuery();
                    resErr = Int32.Parse(cmd.Parameters["@Err"].Value.ToString());
                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);
                }
                finally { db.Database.Connection.Close(); }
            }

            return resErr;
        }
        /// </summary>
        public List<UserSharePreferenceModel> listUserSharePreferrenceModel { get; set; }
         public static List<UserSharePreferenceModel> DBGetUserSharePreferences(int UserId)
        {
            List<Vconnect.Models.UserSharePreferenceModel> listUserSharePreferenceModel = new List<UserSharePreferenceModel>();

            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();

                try
                {

                    cmd.CommandText = "[prc_getusersharepreferences]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@userid", UserId));

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();
                    listUserSharePreferenceModel = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.UserSharePreferenceModel>(reader).ToList();


                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);
                }
                finally { db.Database.Connection.Close(); }

                return listUserSharePreferenceModel;
            }
        }
        public static int DBShareLikesSaveActivities(int UserId, int isShared, string src, string oauthtoken, string oauthtokensecret)
        {
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();

                try
                {

                    cmd.CommandText = "[prc_updateisshared]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@userid", UserId));
                    cmd.Parameters.Add(new SqlParameter("@isShared", isShared));
                    cmd.Parameters.Add(new SqlParameter("@source", src));
                    cmd.Parameters.Add(new SqlParameter("@oauthtoken", oauthtoken));
                    cmd.Parameters.Add(new SqlParameter("@oauthtokensecret", oauthtokensecret));

                    var err = new SqlParameter("@err", SqlDbType.Int);
                    err.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(err);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    cmd.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);
                }
                finally { db.Database.Connection.Close(); }

                return Int32.Parse(cmd.Parameters["@err"].Value.ToString().Trim());
            }
        }
        public static int DBConnectUser(int UserId, string sourceid, string src, string accesstoken, string accesstokensecret)
        {
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();

                try
                {

                    cmd.CommandText = "[prc_userconnect]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@userid", UserId));
                    cmd.Parameters.Add(new SqlParameter("@sourceid", sourceid));
                    cmd.Parameters.Add(new SqlParameter("@source", src));
                    cmd.Parameters.Add(new SqlParameter("@accesstoken", accesstoken));
                    cmd.Parameters.Add(new SqlParameter("@accesstokensecret", accesstokensecret));

                    var err = new SqlParameter("@err", SqlDbType.Int);
                    err.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(err);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    cmd.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Vconnect.Common.log.LogMe(ex);
                }
                finally { db.Database.Connection.Close(); }

                //return Int32.Parse(cmd.Parameters["@err"].Value.ToString().Trim());
                return 0;
            }
        }
        //Facebook
        public int IsErr { get; set; }
        public string IsFBShare { get; set; }
        public string IsTWShare { get; set; }
        //Account
        public string txtemailid { get; set; }
        public string password { get; set; }
        public string txtpassword { get; set; }
        public string txtnewpwd { get; set; }
        public string txtcomfpwd { get; set; }
        //Notification
        public bool newfollowers { get; set; }
        public bool followauser { get; set; }
        public bool writesareview { get; set; }
        public bool postsanupdate { get; set; }
        //Profile
        public string txtfname { get; set; }
        public string txtsname { get; set; }
        public string txtdob { get; set; }
        public string txtphone { get; set; }
        public bool ismobileverified { get; set; }
        // public string txtphoneold { get; set; }
        public string txtdescription { get; set; }
        public string SuccessMessage { get; set; }
        public string ErrorMessage { get; set; }

        public int StateID { get; set; }
        public int SelectedStateId { get; set; }
        public SelectList StateOptions { get; set; }
       // public IEnumerable<SelectListItem> ListStatesNames { get; set; }

        public Int64 CityID { get; set; }
        public int SelectedCityId { get; set; }
        public SelectList CityOptions { get; set; }

        public Int64 gender { get; set; }
        public int SelectgenderId { get; set; }
        public SelectList genderoption { get; set; }

        public Int32 UserId { get; set; }
        public string loginid { get; set; }
        public string userurl { get; set; }
        public int verifyVerificationCode(Int32? userid, string verificationcode, string mobilenumber)
        {
            int result = 0;
            using (var db = new VconnectDBContext29())
            {

                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_Verify_verificationCode_New]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@LoginId", userid));
                cmd.Parameters.Add(new SqlParameter("@code", verificationcode));
                cmd.Parameters.Add(new SqlParameter("@phone", mobilenumber));
                SqlParameter parameter1 = new SqlParameter("@Err", SqlDbType.Int);
                parameter1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(parameter1);
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    result = Convert.ToInt16(cmd.Parameters["@Err"].Value.ToString());
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
                finally
                {
                    db.Database.Connection.Close();
                }

            }
            return result;
        }
        public class ResultUserDBTable
        {
            public List<UserDBDetailsView> UserDBDetails { get; set; }
        }
        public ResultUserDBTable ResultUserDBTab
        { get; set; }
        public ResultUserDBTable prc_get_cuserdetails(Int32 contentid, string source)
        {
            ResultUserDBTable resultUserDBTableObj = new ResultUserDBTable();
            List<UserDBDetailsView> UserDBDetails = new List<UserDBDetailsView>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_cuserdetailsSept_beta]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@contentid", contentid));
                cmd.Parameters.Add(new SqlParameter("@source", "VConnect Website"));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        UserDBDetails = ((IObjectContextAdapter)db).ObjectContext.Translate<UserDBDetailsView>(reader).ToList();
                        // Move to second result set and read Posts
                        reader.NextResult();
                        resultUserDBTableObj.UserDBDetails = UserDBDetails;
                    }
                    //listResSetTabParent.Add(listResultSetTableObj);
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }

                finally
                {
                    db.Database.Connection.Close();
                }
                return resultUserDBTableObj;
            }

        }
        public void getWebUserDashboardDetails()
        {
            ResultUserDBTab = prc_get_cuserdetails(UserId, "Vconnect Website");
        }
        public int prc_cupdate_userdetails(profilemodel edit, string gender)
        {
            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                //DateTime tg = Convert.ToDateTime(edit.txtdob);
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_cupdate_userdetailsNew_beta]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@contentid", edit.UserId));
                cmd.Parameters.Add(new SqlParameter("@contactname", edit.txtfname));
                cmd.Parameters.Add(new SqlParameter("@lastname", edit.txtsname));
                cmd.Parameters.Add(new SqlParameter("@description", (!string.IsNullOrEmpty(edit.txtdescription) ? edit.txtdescription : "")));
                cmd.Parameters.Add(new SqlParameter("@stateid", edit.SelectedStateId));
                cmd.Parameters.Add(new SqlParameter("@cityid", edit.SelectedCityId));
                //cmd.Parameters.Add(new SqlParameter("@countryid", 1));
                if (edit.ismobileverified == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@phone1", (!string.IsNullOrEmpty(edit.txtphone) ? edit.txtphone : "")));
                    cmd.Parameters.Add(new SqlParameter("@ismobileverified", 1));
                }
                else if (string.IsNullOrEmpty(edit.txtphone))
                {
                    cmd.Parameters.Add(new SqlParameter("@ismobileverified", 0));
                }
                //cmd.Parameters.Add(new SqlParameter("@email", edit.txtemail));
                // cmd.Parameters.Add(new SqlParameter("@photo", " "));
                cmd.Parameters.Add(new SqlParameter("@sex", (!string.IsNullOrEmpty(gender) ? gender : "")));
                if (!string.IsNullOrEmpty(edit.txtdob))
                    cmd.Parameters.Add(new SqlParameter("@dateofbirth", Convert.ToDateTime(edit.txtdob)));
                cmd.Parameters.Add(new SqlParameter("@Err", SqlDbType.VarChar, 10));
                cmd.Parameters["@Err"].Direction = ParameterDirection.Output;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    result = Convert.ToInt32(cmd.Parameters["@Err"].Value.ToString());
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
                finally
                {
                    db.Database.Connection.Close();
                }

            }
            return result;
        }
        public int prc_update_userPassword(string passwordaccount, Int32 contentidaccount, string newpasswordaccount)
        {
            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_update_userPasswordNew]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@customerid", contentidaccount));
                cmd.Parameters.Add(new SqlParameter("@Oldpassword", passwordaccount));
                cmd.Parameters.Add(new SqlParameter("@Password", newpasswordaccount));
                cmd.Parameters.Add(new SqlParameter("@encriptedpassword", Vconnect.Common.Utility.Encrypt.encryptPassword(newpasswordaccount.Trim())));
                cmd.Parameters.Add(new SqlParameter("@Err", SqlDbType.VarChar, 10));
                cmd.Parameters["@Err"].Direction = ParameterDirection.Output;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    result = Convert.ToInt32(cmd.Parameters["@Err"].Value.ToString());
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
                finally
                {
                    db.Database.Connection.Close();
                }

            }
            return result;
        }
    }
    public class UserNotificationsModel
    {
        [Key]
        public int ContentId { get; set; }
        public int NotificationId { get; set; }
        public string Source { get; set; }
    }
    public class ListUserNotificationsModel
    {
        [Key]
        public int ContentId { get; set; }
        public string Notifications { get; set; }
        public int CategoryId { get; set; }
        public int IsActive { get; set; }
    }
    public class accountmodel
    {
        public string txtemailid { get; set; }
        public string txtpassword { get; set; }
        public string txtnewpwd { get; set; }
        public string txtcomfpwd { get; set; }
    }
    public class UserDBDetailsView
    {
        [Key]
        public Int32 contentid { get; set; }
        public virtual string contactname { get; set; }
        public virtual string lastname { get; set; }
        public virtual int? cityid { get; set; }
        public virtual int? stateid { get; set; }
        public virtual string phone { get; set; }
        public virtual string alternatephone { get; set; }
        public virtual string email { get; set; }
        public virtual string alternateemail { get; set; }
        public virtual string sex { get; set; }
        public virtual string photo { get; set; }
        public virtual string dateofbirth { get; set; }
        public virtual string state { get; set; }
        public virtual string city { get; set; }
        public virtual string description { get; set; }
        public virtual string pwd { get; set; }
        public virtual bool ismobileverified { get; set; }
    }
    public class notificationmodel
    {
        public bool newfollowers { get; set; }
        public bool followauser { get; set; }
        public bool writesareview { get; set; }
        public bool postsanupdate { get; set; }
    }
    public class UserfollowDisplay 
    {
        public string customername { get; set; }
        public string customeremail { get; set; }
    }
    public class notificationActionmodel 
    {
        public Int32? ID { get; set; }
        public string username { get; set; }
        public Int32? userid { get; set; }
        public Int32? loginid { get; set; }
        public Int32? businessid { get; set; }
        public string businessname { get; set; }
        public string businessurl { get; set; }
        public string emailid { get; set; }
        public string photo { get; set; }
        public Int32? activity { get; set; }
        public DateTime? createddate { get; set; }
        public string userurl { get; set; }
    }
    public class socialactivitynmodel
    {
        public string UserPhoto { get; set; }
        public string detail { get; set; }
        public string businessname { get; set; }
        public string businessurl { get; set; }
        public string contactname { get; set; }
        public Int32? userid { get; set; }
        public string userurl { get; set; }
        public Int32? activity { get; set; }
        public DateTime? createddate { get; set; }
    }
    #endregion 
}
