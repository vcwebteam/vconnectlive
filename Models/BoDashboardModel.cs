﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using Vconnect.Mapping.Listing;
using Vconnect.Mapping.CommonDb;
using Vconnect.Mapping.ListingWEB;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data.SqlClient;

using System.Text.RegularExpressions;



namespace Vconnect.Models
{
 [Serializable]
    public class BoDashboardModel : SearchBarWebModel
    {

        //Global Declartions
        #region WAP
        public Updateclaimedbusiness getclaimedbizDetail { get; set; }
        public bool islead { get; set; }
        public bool isbanner { get; set; }
        public bool iscalllead { get; set; }
        public long? totalPages { get; set; }
        public int? pageNum { get; set; }
        public int? rowsPerPage { get; set; }

        #endregion
        public string Video_Title { get; set; }
        public string filter { get; set; }
        public string heading { get; set; }
        public List<SearchListProductDetails> _SearchListProductDetail { get; set; }
        public string productnames { get; set; }
        public string totalRv { get; set; }
        public string tdbar1 { get; set; }
        public string tdbar2 { get; set; }
        public string tdbar3 { get; set; }
        public string tdbar4 { get; set; }
        public string tdbar5 { get; set; }
        public List<string> Subscription { get; set; }
        public string TotalRate { get; set; }
        public List<Vconnect.Mapping.ListingWEB.Bodashboardbannerlisting> _bodashboardbannerlist { get; set; }
        public List<BodashboardWapSubscriptionList> _BodashboardWapSubscriptionList { get; set; }
        public List<BodashboardSubscriptionBannerList> _BodashboardSubscriptionBannerList { get; set; }
        public List<BodashboardSubscriptionLeadList> _BodashboardSubscriptionLeadList { get; set; }
        public List<BodashboardSubscriptionLeadDetailsList> _BodashboardSubscriptionLeadDetailsList { get; set; }
        public List<BusinessReviewsWAP> _Reviewlist { get; set; }
        public int reviewid { get; set; }
        public string reviewcontactname;
        public string revieweremail;
        public int BusinessAvgRating { set; get; }
        public string BusinessOwnerPhoto { get; set; }
        public string BusinessOwnerContact { get; set; }
        public string reviewcount { get; set; }
        public string reviewdate { get; set; }
        public string RootPath { get; set; }
        public string Pagename { get; set; }
        public string ProfileCompletionvalue { get; set; }
        public string CompanyLogo { get; set; }
        public string ProfileImage { get; set; }
        public string membership { get; set; }
        public List<ClaimedBusinessList> _claimedbusinesslist { get; set; }
        public List<ClaimedBusinessListWAP> _claimedbusinesslistWAP { get; set; }
        public string userid { get; set; }
        public string returncontroller { get; set; }
        public string returnaction { get; set; }
        public string Rate { get; set; }
        public string BusinessAddress { get; set; }
        public string Houseno { get; set; }
        public string BusinessLandmark { get; set; }
        public string BusinessPhone { get; set; }
        public string BusinessWebsite { get; set; }
        public string IsRecomended { get; set; }
        public string BusinessEmail { get; set; }
        public string BusinessAlternateEmail { get; set; }
        public string alternatephone { get; set; }
        public string Businessshortdesc { get; set; }
        public string GBusinessName { get; set; }
        public string GCompanyLogo { get; set; }
        public string modeadvertisemnet { get; set; }
        public string BusinessState { get; set; }
        public string LocalGovernment { get; set; }
        public int BusinessStateId { get; set; }
        public int BusienssLocalGovermentId { get; set; }
        public int Areaid { get; set; }
        public string Area { get; set; }
        public string EmployeeStrength { get; set; }
        public string productid { get; set; }
        public string IsInternet { get; set; }

        public IEnumerable<SelectListItem> ListCityNames { get; set; }

        public int CityID { get; set; }
        public int SelectedCityId { get; set; }
        public SelectList CityOptions { get; set; }

        public IEnumerable<SelectListItem> ListAreaNames { get; set; }
        public Int64 AreaCityID { get; set; }
        public int SelectedAreaId { get; set; }
        public SelectList AreaOptions { get; set; }

        public IEnumerable<SelectListItem> EmpListEmpOptionName { get; set; }
        public int EmpOptionId { get; set; }
        public int EmpSelectedOptionId { get; set; }
        public SelectList EmpOptionName { get; set; }
        public List<GetVedioGalary> _getVediogalary { get; set; }
        public List<SearchListServiceDetails> _SearchListServiceDetail { get; set; }

        public ResultTableBusinessProfileImages _ListBusinessProfileImages { get; set; }

        public List<BusinessImagesWAP> _Imageslist { get; set; }
        public string servicename { get; set; }
        public List<GetAllServices> _GetAllServices { get; set; }
        #region Productlist
        public class SearchListProductDetails
        {
            [Key]
            public virtual Int32? contentid { get; set; }
            public virtual string productname { get; set; }

        }

        public class SearchListServiceDetails
        {
            [Key]
            public virtual Int32? contentid { get; set; }
            public virtual string servicename { get; set; }
        }
        #endregion





        #region BuisnessVideos
        public string VideoTitle { get; set; }
        [AllowHtml]
        public string VideoDescription { get; set; }
        public string videocode { get; set; }
        public string vbusinessname { get; set; }
        public string vavgrating { get; set; }
        public string vcategory { get; set; }
        public string vfulladdress { get; set; }
        public string vcompanylogo { get; set; }
        public string vcreateddate { get; set; }
        public string vcontactname { get; set; }
        public string vphoto { get; set; }
        #endregion


        #region For Vedio Galary
        public class GetVedioGalary
        {
            public Int64 ResultNum { get; set; }
            public string businessurl { get; set; }
            public Int32 VideoId { get; set; }
            public string VideoTitle { get; set; }

            public string VideoDescription { get; set; }
            public string VideoThumbNailExternal { get; set; }
            public string VideoEmbeddedPath { get; set; }
            //public string VideoThumbNailExternal { get; set; }
            //public string VideoEmbeddedPath { get; set; }
            public string createddate { get; set; }
            public string contactname { get; set; }
            public string photo { get; set; }
            public string videovoteshelpfull { get; set; }
            public string TotalRecords { get; set; }
            public string imageurl { get; set; }

        }
        public List<GetVedioGalary> getbusinessVediolist(int ContentID, int businessid, BoDashboardModel obj)
        {
            List<GetVedioGalary> _getAllVedioGalary = new List<GetVedioGalary>();
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "Usp_GetVideolistBO_opt";
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                cmd.Parameters.Add(new SqlParameter("@contentid", 0));
                cmd.Parameters.Add(new SqlParameter("@pageNum ", 1));
                cmd.Parameters.Add(new SqlParameter("rowsPerPage", 100));
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                try
                {
                    // Run the sproc
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        _getAllVedioGalary = ((IObjectContextAdapter)db).ObjectContext.Translate<GetVedioGalary>(reader).ToList();

                        obj.videocode = _getAllVedioGalary[0].VideoEmbeddedPath.ToString();
                        obj.VideoTitle = _getAllVedioGalary[0].VideoTitle.ToString();
                        obj.contactname = _getAllVedioGalary[0].contactname.ToString();
                        obj.vcreateddate = _getAllVedioGalary[0].createddate.ToString();
                        obj.vphoto = _getAllVedioGalary[0].photo.ToString();


                    }
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            return _getAllVedioGalary;
        }
        #endregion
        #region
        public BoDashboardModel()
        {
            this.RootPath = ConfigurationManager.AppSettings["WebsiteRootPath"];

        }

        #endregion

        //Left Menu Declartions
        #region
        public string DefaultBusinessID { get; set; }
        public string DefaultBusinessName { get; set; }
        public string BusinessID { get; set; }
        public string BusinessName { get; set; }

        #endregion

        #region BusinessImages

        public class ResultTableBusinessProfileImages
        {
            public List<BusinessProfileImages> ListBusinessProfileImages { get; set; }
            public List<BusinessProfileImagesByBO> ListBusinessProfileImagesByBO { get; set; }
            public List<BusinessProfileImagesTotalRecords> ListBusinessProfileImagesTotalRecords { get; set; }
        }

        public class BusinessProfileImages
        {
            [Key]
            public Int64 ResultNum { get; set; }
            public int ContentId { get; set; }
            public int BusinessId { get; set; }
            public string Photo { get; set; }
            public string ContactName { get; set; }
            public string CreateDate { get; set; }
            public string IType { get; set; }
            public int TotalRecords { get; set; }
            public string UserPhoto { get; set; }
            public string PhotoCaption { get; set; }
            public int? IsProfileImage { get; set; }

        }


        public class BusinessProfileImagesByBO
        {
            [Key]
            public Int64 ResultNum { get; set; }
            public int ContentId { get; set; }
            public int BusinessId { get; set; }
            public string Photo { get; set; }
            public string ContactName { get; set; }
            public string CreateDate { get; set; }
            public int IStatus { get; set; }
            public string IType { get; set; }
            public int TotalRecords { get; set; }
            public string UserPhoto { get; set; }
            public string PhotoCaption { get; set; }
            public int? IsProfileImage { get; set; }

        }
        public class BusinessProfileImagesTotalRecords
        {
            [Key]
            public int TotalRecords { get; set; }
        }

        public class BusinessImagesWAP
        {
            [Key]

            public Int64 image { get; set; }
            public string Photothumb { get; set; }
            public string Photo { get; set; }
        }



        #endregion

        #region BusinessReviews

        public class BusinessReviews
        {
            [Key]
            public Int64 ResultNum { get; set; }
            public int ContentId { get; set; }
            public string ReviewDetail { get; set; }
            public string ReviewTitle { get; set; }
            public string CreatedDate { get; set; }
            public string UserName { get; set; }
            public string ReviewPostedBy { get; set; }
            public string UserPhoto { get; set; }
            public string TitleOfReviewer { get; set; }
            public string TitleOfTheReviewerImage { get; set; }
            public int AvgRating { get; set; }
            public string Source { get; set; }

        }

        public class BusinessReviewsTotalRecords
        {
            [Key]
            public int TotalRecords { get; set; }

        }

        public class BusinessProfileCompleteness
        {
            [Key]
            public int IsComplete { get; set; }
            public string CompanyLogo { get; set; }
            public int Membership { get; set; }
        }

        public class BusinessUserPhotoDetails
        {
            [Key]
            public string Photo { get; set; }
            public int IsProfileImage { get; set; }
        }

        public class BusinessRatingsDetails
        {
            [Key]
            public int RatingsCount { get; set; }
            public byte Rating { get; set; }
        }

        public class BusinessProfileTotalRatings
        {
            [Key]
            public int TotalRatings { get; set; }
        }

        public class BusinessProfileAverageRatings
        {
            [Key]
            public int AvgRating { get; set; }
        }

        public class BusinessProfileAttachments
        {
            [Key]
            public string Attachments { get; set; }
        }


        #endregion

        public class WorkingHoursModel
        {
            [Key]
            public int BusinessId { get; set; }
            public string MondayStart { get; set; }
            public string MondayEnd { get; set; }
            public string TuesdayStart { get; set; }
            public string Tuesdayend { get; set; }
            public string WednesdayStart { get; set; }
            public string WednesdayEnd { get; set; }
            public string ThursdayStart { get; set; }
            public string ThursdayEnd { get; set; }
            public string FridayStart { get; set; }
            public string FridayEnd { get; set; }
            public string SaturdayStart { get; set; }
            public string SaturdayEnd { get; set; }
            public string SundayStart { get; set; }
            public string SundayEnd { get; set; }
        }
        public class ClaimedBusinessList
        {
            [Key]
            public Int32 contentid { get; set; }
            public string businessname { get; set; }
            public int isdefault { get; set; }
        }

        public class ClaimedBusinessListWAP
        {
            [Key]
            public Int64 contentid { get; set; }
            public Int64 businessid { get; set; }
            public int bid { get; set; }
            public string businessname { get; set; }
            public int isdefault { get; set; }
            public string fulladdress { get; set; }
        }

        public string GetProfileCompletion(string businessid)
        {
            string result = "";
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "select isnull(iscomplete,0) as iscomplete from businessmaster where contentid = " + businessid;
                cmd.CommandType = CommandType.Text;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    reader.Read();
                    if (reader.HasRows)
                    {
                        result = reader["iscomplete"].ToString();
                    }
                    else
                    {
                        result = "0";
                    }

                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            return result;
        }
        public void Getuserbizsubscription(Int64 businessid, int flag, BoDashboardModel obj)
        {
            using (var db = new VconnectDBContext29())
            {


                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "prc_wap_get_userbizsubscription_opt";
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                cmd.Parameters.Add(new SqlParameter("@flag", flag));
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                var reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    if (flag == 2)
                    {
                        reader.Read();
                        if (reader.HasRows) obj.isbanner = true;
                        reader.NextResult();
                        reader.Read();
                        if (reader.HasRows) obj.islead = true;
                        reader.NextResult();
                        reader.Read();
                        if (reader.HasRows) obj.iscalllead = true;
                    }


                    if (flag == 1)
                    {
                        reader.Read();
                        if (reader.HasRows) obj.membership = "1";
                    }


                }
            }
        }
        #region Model and property For GetContact,Products And Service
        public businessproduct businessProduct { get; set; }
        public List<BusinessproductEdit> _editbusinessProduct { get; set; }
        public BusinessproductEdit editbusinessProduct { get; set; }
        public AddService addservices { get; set; }
        public AddService editservices { get; set; }

        public List<GetContactDetails> _getcontactDetails { get; set; }
        public GetContactDetails getcontactDetails { get; set; }

        public List<GetWorkingHours> _getWorkinghours { get; set; }

        public SelectList workinghourlist { get; set; }


        public List<GetProducts> _getproductswithimage { get; set; }
        // public List<GetVedioGalary> _getVediogalary { get; set; }
        public List<BusinessproductEdit> _Editproduct { get; set; }
        public string brandid { get; set; }
        public string brandname { get; set; }
        public Int32 businessidc { get; set; }
        public string title { get; set; }
        public string contactname { get; set; }
        public string designation { get; set; }
        public string phonec { get; set; }
        public string alternatephonec { get; set; }
        public string emailc { get; set; }

        public class GetContactDetails
        {
            [Key]
            public Int32 contentid { get; set; }
            public Int32 businessid { get; set; }
            public string title { get; set; }
            public string contactname { get; set; }
            public string designation { get; set; }
            public string phone { get; set; }
            public string alternatephone { get; set; }
            public string email { get; set; }
        }

        //public class ContactTitle
        //{
        //    public Int32 ContactTitleID { get; set; }
        //    public string ContactTitleName { get; set; }
        //    //public SelectList contacttiltelist { get; set; }
        //}

        public class GetWorkingHours
        {
            [Key]
            public Int32 businessid { get; set; }
            public string mondaystart { get; set; }
            public string mondayend { get; set; }
            public string tuesdaystart { get; set; }
            public string tuesdayend { get; set; }
            public string wednesdaystart { get; set; }
            public string wednesdayend { get; set; }
            public string thursdaystart { get; set; }
            public string thursdayend { get; set; }
            public string fridaystart { get; set; }
            public string fridayend { get; set; }
            public string saturdaystart { get; set; }
            public string saturdayend { get; set; }
            public string sundaystart { get; set; }
            public string sundayend { get; set; }
        }

        public class businessproduct
        {
            public string productmasterid { get; set; }
            [Required(ErrorMessage = "Please Enter Product Name")]
            public string productname { get; set; }
            public string businessid { get; set; }
            public string businessname { get; set; }
            public string details { get; set; }
            public string modal { get; set; }
            public string price { get; set; }
            public string currency { get; set; }
            public string Isreviewed { get; set; }
            public string createdby { get; set; }
            public string categoryid { get; set; }
            public string categoryname { get; set; }
            public string image { get; set; }
        }

        public class BusinessproductEdit
        {
            public string area { get; set; }
            public string state { get; set; }
            public string city { get; set; }
            public Int32? contentid { get; set; }
            public string businessname { get; set; }
            public string shortdesc { get; set; }
            public Int32? membership { get; set; }
            public Int32? productmasterid { get; set; }
            public string productname { get; set; }
            public Int32? businessid { get; set; }
            public string details { get; set; }
            public string modal { get; set; }
            public string price { get; set; }
            public string currency { get; set; }
            public Int32? categoryid { get; set; }
            public string categoryname { get; set; }
            public string brandname { get; set; }
            public string image { get; set; }
        }

        public class GetProducts
        {
            public Int32 contentid { get; set; }
            public Int32 businessid { get; set; }
            public string businessname { get; set; }
            public Int32 productmasterid { get; set; }
            public string productname { get; set; }
            public string details { get; set; }
            public string image { get; set; }
            public string brandname { get; set; }
        }

        public class AddService
        {
            public Int32? servicemasterid { get; set; }
            public string servicename { get; set; }
            public Int32 businessid { get; set; }
            public string businessname { get; set; }
            public string details { get; set; }
            public string pricerange { get; set; }
            public Int32 Isreviewed { get; set; }
            public Int32 createdby { get; set; }
            // public string categoryid { get; set; }
            public string categoryname { get; set; }
            public string image { get; set; }
            public string source { get; set; }
        }

        public class GetAllServices
        {
            public Int32 contentid { get; set; }
            public string businessname { get; set; }
            public Int32? servicemasterid { get; set; }
            public string servicename { get; set; }
            public Int32 businessid { get; set; }
            public string details { get; set; }
            //public string categoryid { get; set; }
            public string categoryname { get; set; }
            public string image { get; set; }
            public string companylogo { get; set; }
            public string pricerange { get; set; }
        }

        public class EditServices
        {
            public Int32 contentid { get; set; }
            public string businessname { get; set; }
            public Int32? servicemasterid { get; set; }
            public string servicename { get; set; }
            public Int32 businessid { get; set; }
            public string details { get; set; }
            // public string categoryid { get; set; }
            public string categoryname { get; set; }
            public string image { get; set; }
            public string companylogo { get; set; }
            public string pricerange { get; set; }

        }
        #endregion
        #region Review
        public class BusinessReviewsWAP
        {
            [Key]

            public Int64 businessid { get; set; }
            public Int64 RecordID { get; set; }
            public string review { get; set; }
            public string reviewtitle { get; set; }
            public string createddate { get; set; }
            public string Screenname { get; set; }
            public string UserPhoto { get; set; }
            //  public Int64 reviewpostedby { get; set; }            
            public int rate { get; set; }
            public bool EmailVerified { get; set; }
            public long totalreviews { get; set; }
        }
        #endregion

        public class SearchKeywordsPname
        {
            [Key]

            public int contentid { get; set; }
            public string Productname { get; set; }


        }

        public class SearchKeywordsSname
        {
            [Key]

            public int contentid { get; set; }
            public string Servicename { get; set; }


        }
        public class SearchKeywordsBname
        {
            [Key]

            public int contentid { get; set; }
            public string brandname { get; set; }


        }


        #region All Methods For GetContact,Products And Service
        public GetContactDetails getcntactDetails(string businessid, BoDashboardModel obj)
        {
            GetContactDetails resulttable = new GetContactDetails();
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_contact_details_BODashboard_opt]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    //reader.Read();
                    //obj.businessidc = Convert.ToInt32(businessid);
                    //obj.contactname = reader["contactname"].ToString();
                    //obj.designation = reader["designation"].ToString();
                    //obj.phonec = reader["phone"].ToString();
                    //obj.alternatephonec = reader["alternatephone"].ToString();
                    //obj.emailc = reader["email"].ToString();
                    if (reader.HasRows)
                    {
                        resulttable = ((IObjectContextAdapter)db).ObjectContext.Translate<GetContactDetails>(reader).FirstOrDefault();
                    }


                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    db.Database.Connection.Close();
                }

            }
            return resulttable;
        }

        public List<GetWorkingHours> getWorkinghoursBoDashboard(string businessid)
        {
            List<GetWorkingHours> resultWorkinghourstable = new List<GetWorkingHours>();
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_Working_hours_BoDashboard_opt]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        resultWorkinghourstable = ((IObjectContextAdapter)db).ObjectContext.Translate<GetWorkingHours>(reader).ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    db.Database.Connection.Close();
                }
                return resultWorkinghourstable;
            }

        }



        public int addupbusinesscontactDetails(string businessid, string contentid, string contactname, string designation, string phone, string alternatephone, string email, string title)
        {
            int result;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "prc_AddUp_businesscontactDetails_opt";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                cmd.Parameters.Add(new SqlParameter("@contentid", contentid));
                cmd.Parameters.Add(new SqlParameter("@PrimaryContact", "1"));
                cmd.Parameters.Add(new SqlParameter("@contactperson", contactname));
                cmd.Parameters.Add(new SqlParameter("@designation", designation));
                cmd.Parameters.Add(new SqlParameter("@address", ""));
                cmd.Parameters.Add(new SqlParameter("@State", ""));
                cmd.Parameters.Add(new SqlParameter("@City", ""));
                cmd.Parameters.Add(new SqlParameter("@area", ""));
                cmd.Parameters.Add(new SqlParameter("@zipcode", ""));
                cmd.Parameters.Add(new SqlParameter("@phone", phone));
                cmd.Parameters.Add(new SqlParameter("@AlternatePhone", alternatephone));
                cmd.Parameters.Add(new SqlParameter("@Email", email));
                cmd.Parameters.Add(new SqlParameter("@ContatcPersonPhoto", ""));
                cmd.Parameters.Add(new SqlParameter("@title ", title));
                SqlParameter parameter1 = new SqlParameter("@Err", SqlDbType.Int);
                parameter1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(parameter1);
                connection.Open();
                cmd.ExecuteNonQuery();
                result = (int)cmd.Parameters["@Err"].Value;
            }
            return result;
        }

        public int addworkinghours(string businessid, string mondaystart, string mondayend, string tuesdaystart, string tuesdayend, string wednesdaystart, string wednesdayend, string thursdaystart, string thursdayend,
                  string fridaystart, string fridayend, string saturdaystart, string saturdayend, string sundaystart, string sundayend)
        {
            int result;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_workinghours_opt]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                cmd.Parameters.Add(new SqlParameter("@mondaystart", mondaystart));
                cmd.Parameters.Add(new SqlParameter("@mondayend", mondayend));
                cmd.Parameters.Add(new SqlParameter("@tuesdaystart", tuesdaystart));
                cmd.Parameters.Add(new SqlParameter("@tuesdayend", tuesdayend));
                cmd.Parameters.Add(new SqlParameter("@wednesdaystart", wednesdaystart));
                cmd.Parameters.Add(new SqlParameter("@wednesdayend", wednesdayend));
                cmd.Parameters.Add(new SqlParameter("@thursdaystart", thursdaystart));
                cmd.Parameters.Add(new SqlParameter("@thursdayend ", thursdayend));
                cmd.Parameters.Add(new SqlParameter("@fridaystart", fridaystart));
                cmd.Parameters.Add(new SqlParameter("@fridayend", fridayend));
                cmd.Parameters.Add(new SqlParameter("@saturdaystart", saturdaystart));
                cmd.Parameters.Add(new SqlParameter("@saturdayend", saturdayend));
                cmd.Parameters.Add(new SqlParameter("@sundaystart", sundaystart));
                cmd.Parameters.Add(new SqlParameter("@sundayend", sundayend));
                connection.Open();
                result = cmd.ExecuteNonQuery();
            }
            return result;
        }

        public int addBusinessProductByBizID(string buisnessid, string productname, string productdetails, string productmodal, Int32 brandcointaintid, string productprice, string userid, string image)
        {
            int result;

            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "prc_AddUp_businessproductByBizID_BO_opt";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.Parameters.Add(new SqlParameter("@businessid", buisnessid));
                cmd.Parameters.Add(new SqlParameter("@contentid", "0"));
                cmd.Parameters.Add(new SqlParameter("@Productname", productname));
                cmd.Parameters.Add(new SqlParameter("@details", productdetails));
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@modal", productmodal));
                cmd.Parameters.Add(new SqlParameter("@price", productprice));
                cmd.Parameters.Add(new SqlParameter("@image", image));
                cmd.Parameters.Add(new SqlParameter("@businessname", ""));
                cmd.Parameters.Add(new SqlParameter("@createdby", userid));
                cmd.Parameters.Add(new SqlParameter("@createddate", ""));
                cmd.Parameters.Add(new SqlParameter("@brandid", brandcointaintid));
                cmd.Parameters.Add(new SqlParameter("@source", "WEBBoDashbaord"));
                // cmd.Parameters.Add(new SqlParameter("@pid", ""));
                SqlParameter parameter1 = new SqlParameter("@Err", SqlDbType.Int);
                parameter1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(parameter1);
                SqlParameter parameter2 = new SqlParameter("@ProductMasterID", SqlDbType.BigInt);
                parameter2.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(parameter2);
                connection.Open();
                cmd.ExecuteNonQuery();
                result = (int)cmd.Parameters["@Err"].Value;
            }
            return result;
        }

        public List<BoDashboardModel.GetProducts> getAllProducts(string businessid, string containtid)
        {
            List<BoDashboardModel.GetProducts> getAllProductsWitImages = new List<BoDashboardModel.GetProducts>();
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "prc_get_businessproductByBizID_BO_opt";
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                cmd.Parameters.Add(new SqlParameter("@contentid", "0"));
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                try
                {
                    // Run the sproc
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        getAllProductsWitImages = ((IObjectContextAdapter)db).ObjectContext.Translate<BoDashboardModel.GetProducts>(reader).ToList();
                    }
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            return getAllProductsWitImages;
        }

        public int DeleteProducts(string businessid, string containtid, string SV_VCUserContentID)
        {
            int result;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "prc_deletebusinessproduct_opt";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.Parameters.Add(new SqlParameter("@userid", SV_VCUserContentID));
                cmd.Parameters.Add(new SqlParameter("@contentid", containtid));
                SqlParameter parameter1 = new SqlParameter("@Err", SqlDbType.Int);
                parameter1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(parameter1);
                connection.Open();
                cmd.ExecuteNonQuery();
                result = (int)cmd.Parameters["@Err"].Value;
            }
            return result;
        }

        public List<BusinessproductEdit> EditProductList(string cid, string bid)
        {
            List<BusinessproductEdit> editbusinessProductResult = new List<BusinessproductEdit>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "prc_get_businessproductByBizID_Edit_opt";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", "0"));
                cmd.Parameters.Add(new SqlParameter("@contentid", cid));
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                try
                {
                    // Run the sproc
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        editbusinessProductResult = ((IObjectContextAdapter)db).ObjectContext.Translate<BusinessproductEdit>(reader).ToList();
                    }
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            return editbusinessProductResult;
        }

        public int UpdateProductDetails(string containtid, string businessid, string productname, string productdetails, string productmodal, string productbrand, string price, int brandcointaintid, string sThumbFileName)
        {
            int result;
            string SV_VCUserContentID = HttpContext.Current.Request.Cookies[ConfigurationManager.AppSettings["VCUserContentID"]].Value.ToString();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "prc_Up_businessproductByBizID_BO_opt";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;

                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                cmd.Parameters.Add(new SqlParameter("@contentid", containtid));
                cmd.Parameters.Add(new SqlParameter("@Productname", productname));
                cmd.Parameters.Add(new SqlParameter("@details", productdetails));
                cmd.Parameters.Add(new SqlParameter("@userid", ""));
                cmd.Parameters.Add(new SqlParameter("@modal", productmodal));
                cmd.Parameters.Add(new SqlParameter("@price", price));
                cmd.Parameters.Add(new SqlParameter("@image", sThumbFileName));
                cmd.Parameters.Add(new SqlParameter("@businessname", ""));
                cmd.Parameters.Add(new SqlParameter("@createdby", SV_VCUserContentID));
                cmd.Parameters.Add(new SqlParameter("@source", "WEBBoDashbaord"));
                cmd.Parameters.Add(new SqlParameter("@brandid", brandcointaintid));
                SqlParameter parameter1 = new SqlParameter("@Err", SqlDbType.Int);
                parameter1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(parameter1);
                connection.Open();
                cmd.ExecuteNonQuery();
                result = (int)cmd.Parameters["@Err"].Value;
            }
            return result;
        }

        public int addnewservices(string buisnessid, string servicename, string servicedetails, string pricerange, string userid, string sThumbFileName, string bname)
        {
            int result;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "prc_AddUp_businessserviceByBizID_BO_opt";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.Parameters.Add(new SqlParameter("@businessid", buisnessid));
                cmd.Parameters.Add(new SqlParameter("@contentid", "0"));
                cmd.Parameters.Add(new SqlParameter("@servicename", servicename));
                cmd.Parameters.Add(new SqlParameter("@details", servicedetails));
                cmd.Parameters.Add(new SqlParameter("@image", sThumbFileName));
                cmd.Parameters.Add(new SqlParameter("@businessname", bname.ToString()));
                cmd.Parameters.Add(new SqlParameter("@pricerange", pricerange));
                cmd.Parameters.Add(new SqlParameter("@userid ", userid));
                cmd.Parameters.Add(new SqlParameter("@ServiceMasterID", ""));
                cmd.Parameters.Add(new SqlParameter("@sid", ""));
             cmd.Parameters.Add(new SqlParameter("@source", "WEBBoDashbaord"));
                SqlParameter parameter1 = new SqlParameter("@Err", SqlDbType.Int);
                parameter1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(parameter1);
                connection.Open();
                cmd.ExecuteNonQuery();
                result = (int)cmd.Parameters["@Err"].Value;
            }
            return result;
        }

        public List<GetAllServices> getAllServicesList(string businessid, string containtid)
        {
            List<GetAllServices> getAllServices = new List<GetAllServices>();
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "prc_get_businessserviceByBizID_BO_opt";
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                cmd.Parameters.Add(new SqlParameter("@contentid", "0"));
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                try
                {
                    // Run the sproc
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        getAllServices = ((IObjectContextAdapter)db).ObjectContext.Translate<GetAllServices>(reader).ToList();
                    }
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            return getAllServices;
        }

        public List<GetAllServices> getSpecificServices(string businessid, string containtid)
        {
            List<GetAllServices> getSpecificServices = new List<GetAllServices>();
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "prc_get_businessserviceByBizID_BO_opt";
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                cmd.Parameters.Add(new SqlParameter("@contentid", containtid));
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                try
                {
                    // Run the sproc
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        getSpecificServices = ((IObjectContextAdapter)db).ObjectContext.Translate<GetAllServices>(reader).ToList();
                    }
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            return getSpecificServices;
        }

        public int updateservices(string containtid, string businessid, string servicename, string servicedetails, string pricerange, string sThumbFileName, string bname)
        {
            int result;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "prc_Up_businessserviceByBizID_BO_opt";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                cmd.Parameters.Add(new SqlParameter("@contentid", containtid));
                cmd.Parameters.Add(new SqlParameter("@servicename", servicename));
                cmd.Parameters.Add(new SqlParameter("@details", servicedetails));
                cmd.Parameters.Add(new SqlParameter("@image", sThumbFileName));
                cmd.Parameters.Add(new SqlParameter("@businessname", bname));
                cmd.Parameters.Add(new SqlParameter("@pricerange", pricerange));
                cmd.Parameters.Add(new SqlParameter("@source", "WeBBODasboard"));
                cmd.Parameters.Add(new SqlParameter("@userid ", ""));
                SqlParameter parameter1 = new SqlParameter("@Err", SqlDbType.Int);
                parameter1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(parameter1);
                connection.Open();
                cmd.ExecuteNonQuery();
                result = (int)cmd.Parameters["@Err"].Value;
            }
            return result;
        }

        public int DeleteSercices(string businessid, string cid, string SV_VCUserContentID)
        {
            int result;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "prc_deletebusinessService_opt";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.Parameters.Add(new SqlParameter("@userid", SV_VCUserContentID));
                cmd.Parameters.Add(new SqlParameter("@contentid", cid));
                SqlParameter parameter1 = new SqlParameter("@Err", SqlDbType.Int);
                parameter1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(parameter1);
                connection.Open();
                cmd.ExecuteNonQuery();
                result = (int)cmd.Parameters["@Err"].Value;
            }
            return result;
        }

        public int GetBrandName(string brandname)
        {
            // List<GetBrand> _getbrandname = new List<GetBrand>();
            int brandid = 0;
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "prc_get_branddetails_Product_opt";
                cmd.Parameters.Add(new SqlParameter("@brandname", brandname));
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                try
                {
                    // Run the sproc
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    reader.Read();
                    if (reader.HasRows)
                    {
                        brandid = Convert.ToInt32(reader["contentid"]);
                    }
                    else
                    {
                        brandid = 0;
                    }
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            return brandid;

        }

        public int getvalidProductservice(int Type, string name)
        {
            int vaildproductNservice = 0;
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "prc_get_Validproduct_service_opt";
                cmd.Parameters.Add(new SqlParameter("@type", Type));
                cmd.Parameters.Add(new SqlParameter("@name", name));

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                try
                {
                    // Run the sproc
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    reader.Read();
                    if (reader.HasRows)
                    {
                        vaildproductNservice = Convert.ToInt32(reader["errorcode"]);
                    }
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            return vaildproductNservice;
        }

        #endregion

        #region WAP Classes
        [Serializable]
        public class Updateclaimedbusiness
        {
            public Int64 businessid { get; set; }
            [Required(ErrorMessage = "Please Enter Business name")]
            [Display(Name = "Business name")]
            public string businessname { get; set; }
            [Required(ErrorMessage = "Please Enter Contact Person")]
            [Display(Name = "Contact Person")]
            public string contactperson { get; set; }
            [Required(ErrorMessage = "Please Enter  Phone No.")]
            [Display(Name = "Phone No.")]
            [RegularExpression(@"^[0-9]{0,11}$", ErrorMessage = "Phone Number should contain only numbers or valied")]
            public string phone { get; set; }
            [Required(ErrorMessage = "Please Enter Email Address")]
            [Display(Name = "Email")]
            //[RegularExpression(@"^[0-9]{0,11}$", ErrorMessage = "Email id not invalied")]
            public string email { get; set; }
            [Required(ErrorMessage = "Please Enter Email ")]
            [Display(Name = "Description")]
            public string website { get; set; }
            public int StateID { get; set; }
            [Required(ErrorMessage = "Please Enter State")]
            [Display(Name = "Description")]
            public string state { get; set; }
            public int cityid { get; set; }
            [Required(ErrorMessage = "Please Enter City")]
            [Display(Name = "Description")]
            public string city { get; set; }
            //public virtual ICollection<State> stateid { get; set; }
            //public virtual ICollection<City> cityid { get; set; }
            public int areaid { get; set; }
            [Required(ErrorMessage = "Please Enter Area")]
            [Display(Name = "Area")]
            public string area { get; set; }
            [Required(ErrorMessage = "Please Enter Address")]
            [Display(Name = "Address")]
            public string address1 { get; set; }
            [Required(ErrorMessage = "Please Enter House No")]
            [Display(Name = "House No")]
            public string houseno { get; set; }
            [Required(ErrorMessage = "Please Enter Descriptions about yor Companey")]
            [Display(Name = "Description")]
            public string shortdesc { get; set; }
            [Required(ErrorMessage = "Please Enter Company logo")]
            [Display(Name = "companey image")]
            public string companylogo { get; set; }
            // public Int64 errorcode { get; set; }   
        }
        public Updateclaimedbusiness prc_wap_get_businessinfo(Int64 bid)
        {
            Updateclaimedbusiness UpdateclaimedbusinessResult = new Updateclaimedbusiness();
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_wap_get_businessinfo_opt]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", bid));

                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        UpdateclaimedbusinessResult = ((IObjectContextAdapter)db).ObjectContext.Translate<Updateclaimedbusiness>(reader).SingleOrDefault();
                        //gcm.clb = ResultClaimedBiz;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    db.Database.Connection.Close();
                }
                return UpdateclaimedbusinessResult;
            }
        }

        public List<BusinessReviewsWAP> getAllReviewList(string businessid, string pageno)
        {
            List<BusinessReviewsWAP> getAllReviews = new List<BusinessReviewsWAP>();
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "prc_wap_get_reviews_oncurrent";
                cmd.Parameters.Add(new SqlParameter("@Businessid", businessid));
                cmd.Parameters.Add(new SqlParameter("@pageNum", pageno));
                cmd.Parameters.Add(new SqlParameter("@rowsPerPage", 10));
                cmd.Parameters.Add(new SqlParameter("@TotalReviews", ""));
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                try
                {
                    // Run the sproc
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        getAllReviews = ((IObjectContextAdapter)db).ObjectContext.Translate<BusinessReviewsWAP>(reader).ToList();
                    }
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            return getAllReviews;
        }

        public List<BusinessImagesWAP> getbusinessImageslist(int businessid)
        {
            List<BusinessImagesWAP> _getbusinessImageslist = new List<BusinessImagesWAP>();
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "prc_wap_get_businessimage_opt";
                cmd.Parameters.Add(new SqlParameter("@Bid", businessid));

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                try
                {
                    // Run the sproc
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        _getbusinessImageslist = ((IObjectContextAdapter)db).ObjectContext.Translate<BusinessImagesWAP>(reader).ToList();

                    }
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            return _getbusinessImageslist;
        }
        #endregion

        #region  Wap P,S,C,I
        public List<ClaimedBusinessListWAP> getbusiness(string userid)
        {
            List<ClaimedBusinessListWAP> lstClaimBusinessList = new List<ClaimedBusinessListWAP>();
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_wap_get_userbizlist_opt]";
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                try
                {
                    // Run the sproc
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    lstClaimBusinessList = ((IObjectContextAdapter)db).ObjectContext.Translate<ClaimedBusinessListWAP>(reader).ToList();

                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            return lstClaimBusinessList;
        }
        public GetAllServices getServicesDatails(string businessid, string containtid)
        {
            GetAllServices getSpecificServices = new GetAllServices();
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "prc_get_businessserviceByBizID_BO_opt";
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                cmd.Parameters.Add(new SqlParameter("@contentid", containtid));
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                try
                {
                    // Run the sproc
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        getSpecificServices = ((IObjectContextAdapter)db).ObjectContext.Translate<GetAllServices>(reader).SingleOrDefault();
                    }
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            return getSpecificServices;
        }
        public GetContactDetails getcntactDetails(string businessid)
        {
            GetContactDetails resulttable = new GetContactDetails();
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_contact_details_BODashboard_opt]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    //reader.Read();
                    //obj.businessidc = Convert.ToInt32(businessid);
                    //obj.contactname = reader["contactname"].ToString();
                    //obj.designation = reader["designation"].ToString();
                    //obj.phonec = reader["phone"].ToString();
                    //obj.alternatephonec = reader["alternatephone"].ToString();
                    //obj.emailc = reader["email"].ToString();
                    if (reader.HasRows)
                    {
                        resulttable = ((IObjectContextAdapter)db).ObjectContext.Translate<GetContactDetails>(reader).FirstOrDefault();
                    }


                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    db.Database.Connection.Close();
                }

            }
            return resulttable;
        }

        public string AddVideos(string VideoTitle, string VideoDesc, string userid)
        {
            string result;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[Usp_AddUpdVideoBO_opt]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.Parameters.Add(new SqlParameter("@createdBy", userid));
                cmd.Parameters.Add(new SqlParameter("@VideoTitle", VideoTitle));
                cmd.Parameters.Add(new SqlParameter("@VideoDescription", VideoDesc));

                SqlParameter parameter1 = new SqlParameter("@Err", SqlDbType.Int);
                parameter1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(parameter1);


                connection.Open();
                cmd.ExecuteNonQuery();
                result = (string)cmd.Parameters["@ERR"].Value.ToString();
            }


            return result;

        }
        public Int64 AddbusinessproductWap(string SV_VCUserContentID, string bid, string businessname, string pid, string pname, string imgname)
        {



            Int64 result = 0;
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "prc_wap_add_businessprocductBO_opt";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@userid", SV_VCUserContentID));
                cmd.Parameters.Add(new SqlParameter("@businessid", bid));
                cmd.Parameters.Add(new SqlParameter("@businessname", businessname));
                cmd.Parameters.Add(new SqlParameter("@productid", pid));
                cmd.Parameters.Add(new SqlParameter("@product", pname));
                cmd.Parameters.Add(new SqlParameter("@imgname", imgname));


                SqlParameter parameter1 = new SqlParameter("@ErrorCode", SqlDbType.BigInt);
                parameter1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(parameter1);
                //var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                //connection.Open();
                //cmd.ExecuteNonQuery();
                //Int64 result = (Int64)cmd.Parameters["@ErrorCode"].Value;

                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    result = Convert.ToInt64(cmd.Parameters["@ErrorCode"].Value);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    db.Database.Connection.Close();
                }

            }
            return result;
        }

        public Int64 AddbusinessServiceWap(string SV_VCUserContentID, string bid, string businessname, int sid, string sname)
        {
            Int64 result = 0;
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "prc_wap_add_businessservice_opt";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@userid", SV_VCUserContentID));
                cmd.Parameters.Add(new SqlParameter("@businessid", bid));
                cmd.Parameters.Add(new SqlParameter("@businessname", businessname));
                cmd.Parameters.Add(new SqlParameter("@serviceid", sid));
                cmd.Parameters.Add(new SqlParameter("@service", sname));
                cmd.Parameters.Add(new SqlParameter("@ErrorCode", 0));
                cmd.Parameters["@ErrorCode"].Direction = ParameterDirection.Output;

                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    result = Convert.ToInt64(cmd.Parameters["@ErrorCode"].Value);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    db.Database.Connection.Close();
                }

            }
            return result;
        }

        public int AddBusinessPhotosByBO(int businessid, int contentid, int userid, string businessname, string photos, string photocaption)
        {

            using (var db = new VconnectDBContext29())
            {

                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_add_PhotosToBusiness_BOProfile_add_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                    cmd.Parameters.Add(new SqlParameter("@userid", userid));
                    cmd.Parameters.Add(new SqlParameter("@contentid", contentid));
                    cmd.Parameters.Add(new SqlParameter("@businessname", businessname));
                    cmd.Parameters.Add(new SqlParameter("@photo", photos));
                    cmd.Parameters.Add(new SqlParameter("@photocaption", photocaption));


                    var err = new SqlParameter("@Err", SqlDbType.Int);
                    err.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(err);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteNonQuery();

                    return (int)cmd.Parameters["@Err"].Value; ;

                }
                catch (Exception ex)
                {
                    return 0;
                }
                finally { db.Database.Connection.Close(); }
            }

        }

        public int AddBusinessAttachmentsBO(int businessid, int contentid, int userid, string businessname, string attachments, string photocaption, int formatid, string format)
        {

            using (var db = new VconnectDBContext29())
            {

                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_add_Businessattachments_BO_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                    cmd.Parameters.Add(new SqlParameter("@userid", userid));
                    cmd.Parameters.Add(new SqlParameter("@contentid", contentid));
                    cmd.Parameters.Add(new SqlParameter("@businessname", businessname));
                    cmd.Parameters.Add(new SqlParameter("@attachments", attachments));
                    cmd.Parameters.Add(new SqlParameter("@photocaption", photocaption));
                    cmd.Parameters.Add(new SqlParameter("formatid", formatid));
                    cmd.Parameters.Add(new SqlParameter("@format", format));
                    var err = new SqlParameter("@Err", SqlDbType.Int);
                    err.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(err);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteNonQuery();

                    return (int)cmd.Parameters["@Err"].Value; ;

                }
                catch (Exception ex)
                {
                    return 0;
                }
                finally { db.Database.Connection.Close(); }
            }

        }

        #endregion


        public GetAllServices getAllServices { get; set; }
    }

    public class EmpOption
    {
        public int EmpOptionId { get; set; }
        public string EmpOptionName { get; set; }

    }

    public class updatebusinessprofile
    {
        public string GBusinessName { get; set; }
    }
    public class BodashboardWapSubscriptionList
    {
        public virtual int contentid { get; set; }
        public virtual string businessname { get; set; }
        public virtual string subscriptionname { get; set; }
        public virtual string startdate { get; set; }
        public virtual string expirydate { get; set; }
        public virtual string status { get; set; }
    }

    public class BodashboardSubscriptionBannerList
    {
        public virtual Int64 contentid { get; set; }
        public virtual string bannertype { get; set; }
        public virtual string startdate { get; set; }
        public virtual string expirydate { get; set; }
        public virtual string status { get; set; }
        public virtual int totimpression { get; set; }
        public virtual int totclicks { get; set; }
    }

    public class BodashboardSubscriptionLeadList
    {
        public virtual string Month { get; set; }
        public virtual string Year { get; set; }
        public virtual int count { get; set; }
    }
    public class BodashboardSubscriptionLeadDetailsList
    {
        public virtual string leaddate { get; set; }
        public virtual string sendername { get; set; }
        public virtual string senderemailid { get; set; }
        public virtual string sendercontactnumber { get; set; }
        public virtual string requirement { get; set; }
    }

}