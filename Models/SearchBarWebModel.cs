﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vconnect.Mapping.Listing;
using Vconnect.Mapping.CommonDb;
using Vconnect.Mapping.ListingWEB;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using System.Web.DynamicData;
using System.Text;

namespace Vconnect.Models
{
    [Serializable]
    public class SearchBarWebModel:SearchBar
    {
        public SearchBarWebModel()
        {
            SelectedStateId = 125;
           // StateOptions = Vconnect.Common.Utility.GetSelectList();
        }
      
        public string SearchType { get; set; }
        public int StateID { get; set; }
        public int SelectedStateId { get; set; }
        public SelectList StateOptions { get; set; }
        public IEnumerable<SelectListItem> ListStatesNames { get; set; }
    }
}