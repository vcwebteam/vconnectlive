﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Vconnect.Mapping.Listing;
using Vconnect.Mapping.CommonDb;
using Vconnect.Mapping.ListingWEB;
using System.Data.SqlClient;
using System.Data.Entity.Infrastructure;
using System.Data;
using Vconnect.Mapping.UserDBWEB;
using System.Text.RegularExpressions;
namespace Vconnect.Models
{
    public class VconnectDBContext29 : DbContext
    {
        public VconnectDBContext29()
            : base("name=VconnectDBContext29")
        {

        }
        //#region WAP
        //public DbSet<SearchListBusiness> SearchListBusinessTable { get; set; }
        //public DbSet<BusinessCount> BusinessCountTable { get; set; }
        //public DbSet<SearchListPremiumBusiness> SearchListPremiumBusinessTable { get; set; }
        //#endregion

        //#region common
        ////public DbSet<DisplayFlashNumber> DisplayFlashNumber { get; set; }
        //public DbSet<SearchListCYB> ListResultSetCYBTable { get; set; }
        //public DbSet<CategoryItemlist> ItemListModel { get; set; }
        //public DbSet<SearchListDetails> SearchListDetailsTable { get; set; }
        //public DbSet<SearchBizDetailsClaim> SearchBizDetailsClaim { get; set; }
        //public DbSet<getSmeEvent> SmeEventList { get; set; }
        //public DbSet<SMSGateway> SMSGatewayTable { get; set; }
        //#endregion

        //#region web
        //public DbSet<SearchListBusinessWEB> SearchListWEBBusinessTable { get; set; }
        //public DbSet<BusinessCountWEB> BusinessCountWEBTable { get; set; }
        //public DbSet<SearchListPremiumBusinessWEB> SearchListWEBPremiumBusinessTable { get; set; }
        //public DbSet<BannerWeb> BannerWEBTable { get; set; }
        //#endregion

    }
    public class WaptDBContext29 : VconnectDBContext29
    {


    //    public ListResultSetTable prc_get_wapsearch(string keyword, string location, Nullable<int> pageNum, Nullable<int> rowsPerPage, Nullable<int> sort, Nullable<int> type)
    //    {
    //        //List<ListResultSetTable> listResSetTabParent = new List<ListResultSetTable>();
    //        ListResultSetTable listResultSetTableObj = new ListResultSetTable();
    //        List<SearchListBusiness> searchListBuisnes = new List<SearchListBusiness>();
    //        List<BusinessCount> businessCount = new List<BusinessCount>();
    //        List<SearchListPremiumBusiness> searchListPremiumBusiness = new List<SearchListPremiumBusiness>();

    //        using (var db = new VconnectDBContext29())
    //        {

    //            // Create a SQL command to execute the sproc
    //            var cmd = db.Database.Connection.CreateCommand();
    //            cmd.CommandText = "[dbo].[prc_get_wapsearch_new2014]";
    //            cmd.CommandType = System.Data.CommandType.StoredProcedure;
    //            //Add parameters to command object
    //            cmd.Parameters.Add(new SqlParameter("@keyword", keyword));
    //            cmd.Parameters.Add(new SqlParameter("@location", location));
    //            cmd.Parameters.Add(new SqlParameter("@pageNum", pageNum));
    //            cmd.Parameters.Add(new SqlParameter("@rowsPerPage", rowsPerPage));
    //            cmd.Parameters.Add(new SqlParameter("@sort", 1));
    //            cmd.Parameters.Add(new SqlParameter("@type", 1));
    //            try
    //            {
    //                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
    //                connection.Open();
    //                var reader = cmd.ExecuteReader();
    //                if (reader.HasRows)
    //                {
    //                    searchListBuisnes = ((IObjectContextAdapter)db).ObjectContext.Translate<SearchListBusiness>(reader).ToList();

    //                    // Move to second result set and read Posts
    //                    reader.NextResult();

    //                    businessCount = ((IObjectContextAdapter)db).ObjectContext.Translate<BusinessCount>(reader).ToList();
    //                    //  homeWAPModel.totalPages = (int)Math.Ceiling((double)listResultSetTable.BusinessCount[0].totalcount.Value / homeWAPModel.rowsPerPage);
    //                    reader.NextResult();
    //                    searchListPremiumBusiness = ((IObjectContextAdapter)db).ObjectContext.Translate<SearchListPremiumBusiness>(reader).ToList();

    //                    listResultSetTableObj.SearchListBusiness = searchListBuisnes;
    //                    listResultSetTableObj.BusinessCount = businessCount;
    //                    listResultSetTableObj.SearchListPremiumBusiness = searchListPremiumBusiness;

    //                }
    //                //listResSetTabParent.Add(listResultSetTableObj);
    //            }
    //            catch (Exception ex)
    //            {
    //                Console.WriteLine(ex.ToString());
    //            }

    //            finally
    //            {
    //                db.Database.Connection.Close();
    //            }
    //        }

    //        return listResultSetTableObj;
    //    }


    //    public BannerResult HeaderBanner(string businessidlist, int type)
    //    {

    //        using (var db = new VconnectDBContext29())
    //        {

    //            var cmd = db.Database.Connection.CreateCommand();
    //            cmd.CommandText = "[dbo].[prc_get_wapbanneradvert]";
    //            cmd.CommandType = System.Data.CommandType.StoredProcedure;

    //            cmd.Parameters.Add(new SqlParameter("@businessid", businessidlist));
    //            cmd.Parameters.Add(new SqlParameter("@type", 1));
    //            try
    //            {
    //                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
    //                connection.Open();
    //                var reader = cmd.ExecuteReader();
    //                if (reader.HasRows)
    //                    return ((IObjectContextAdapter)db).ObjectContext.Translate<BannerResult>(reader).SingleOrDefault();
    //                else
    //                    return null;

    //            }
    //            finally
    //            {
    //                db.Database.Connection.Close();
    //            }
    //        }
    //    }

        public string prc_wap_add_lead(getsuppliers getsupplier,Int32 stateid)
        {
            string result=string.Empty;
            using (var db = new VconnectDBContext29())
                    {
                        var cmd = db.Database.Connection.CreateCommand();
                        cmd.CommandText = "[dbo].[prc_wap_add_lead]";
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@userid", 0));
                        cmd.Parameters.Add(new SqlParameter("@name",getsupplier.txtName.ToString()));
                        cmd.Parameters.Add(new SqlParameter("@phone", getsupplier.txtPhonenumber.ToString()));
                        cmd.Parameters.Add(new SqlParameter("@email", getsupplier.txtEmail.ToString()));
                        cmd.Parameters.Add(new SqlParameter("@requriment", getsupplier.txtProduct.ToString()));
                        cmd.Parameters.Add(new SqlParameter("@detail",getsupplier.txtDetails.ToString()));
                        if (getsupplier.location.ToString().Trim() == null)
                        {
                            cmd.Parameters.Add(new SqlParameter("@city", " "));
                        }
                        else
                        {
                            cmd.Parameters.Add(new SqlParameter("@city", getsupplier.location.ToString()));
                        }
                        cmd.Parameters.Add(new SqlParameter("@Stateid", stateid));
                        SqlParameter parameter1 = new SqlParameter("@err", SqlDbType.Int);
                        parameter1.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(parameter1);
                        try
                        {
                            var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                            connection.Open();
                            cmd.ExecuteNonQuery();
                            result=cmd.Parameters["@err"].Value.ToString();
                        }
                        catch (Exception ex)
                        {
                        }
                        finally
                        {
                            db.Database.Connection.Close();
                        }
                    }
            return result;
        }





    }
    public class WebUserDashboardDBContext : VconnectDBContext29
    {
        public ResultUserDBTable prc_get_cuserdetails(Int32 contentid, string source)
        {
            ResultUserDBTable resultUserDBTableObj = new ResultUserDBTable();
            List<RecentlyViewedBusiness> RecentlyViewedBusiness = new List<RecentlyViewedBusiness>();
            List<RecentlyPostedReviews> RecentlyPostedReviews = new List<RecentlyPostedReviews>();
            List<KeywordSearchLocation> KeywordSearchLocation = new List<KeywordSearchLocation>();
            List<UserDBDetails> UserDBDetails = new List<UserDBDetails>();

            using (var db = new VconnectDBContext29())
            {

                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_cuserdetails]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@contentid", 665586));
                cmd.Parameters.Add(new SqlParameter("@source", "VConnect Website"));

                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        UserDBDetails = ((IObjectContextAdapter)db).ObjectContext.Translate<UserDBDetails>(reader).ToList();


                        // Move to second result set and read Posts
                        reader.NextResult();

                        RecentlyPostedReviews = ((IObjectContextAdapter)db).ObjectContext.Translate<RecentlyPostedReviews>(reader).ToList();
                        //  homeWAPModel.totalPages = (int)Math.Ceiling((double)listResultSetTable.BusinessCount[0].totalcount.Value / homeWAPModel.rowsPerPage);
                        reader.NextResult();
                        KeywordSearchLocation = ((IObjectContextAdapter)db).ObjectContext.Translate<KeywordSearchLocation>(reader).ToList();
                        reader.NextResult();
                        RecentlyViewedBusiness = ((IObjectContextAdapter)db).ObjectContext.Translate<RecentlyViewedBusiness>(reader).ToList();
                        //  KeywordSearchedPage = ((IObjectContextAdapter)db).ObjectContext.Translate<KeywordSearchedPage>(reader).ToList();

                        resultUserDBTableObj.UserDBDetails = UserDBDetails;
                        resultUserDBTableObj.KeywordSearchLocation = KeywordSearchLocation;
                        resultUserDBTableObj.RecentlyPostedReviews = RecentlyPostedReviews;
                        resultUserDBTableObj.RecentlyViewedBusiness = RecentlyViewedBusiness;

                    }
                    //listResSetTabParent.Add(listResultSetTableObj);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    db.Database.Connection.Close();
                }
                return resultUserDBTableObj;
            }

        }
        public WebResultUserDashboard prc_get_cuserdashboarddetails(Int32 contentid)
        {
            WebResultUserDashboard webResultUserDashboard = new WebResultUserDashboard();
            BusinessVisitCount businessVisitCount = new BusinessVisitCount();
            KeywordSearched keywordSearched = new KeywordSearched();
            BannerClicked bannerClicked = new BannerClicked();
            SMSToMobile smsToMobile = new SMSToMobile();
            EmailSent emailSent = new EmailSent();

            List<KeywordSearchedPage> keywordSearchedPage = new List<KeywordSearchedPage>();


            using (var db = new VconnectDBContext29())
            {

                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_cuserdashboarddetails]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@userid", 665586));

                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        businessVisitCount = ((IObjectContextAdapter)db).ObjectContext.Translate<BusinessVisitCount>(reader).SingleOrDefault();


                        // Move to second result set and read Posts
                        reader.NextResult();
                        keywordSearched = ((IObjectContextAdapter)db).ObjectContext.Translate<KeywordSearched>(reader).SingleOrDefault();


                        reader.NextResult();
                        bannerClicked = ((IObjectContextAdapter)db).ObjectContext.Translate<BannerClicked>(reader).SingleOrDefault();


                        reader.NextResult();
                        smsToMobile = ((IObjectContextAdapter)db).ObjectContext.Translate<SMSToMobile>(reader).SingleOrDefault();


                        reader.NextResult();
                        emailSent = ((IObjectContextAdapter)db).ObjectContext.Translate<EmailSent>(reader).SingleOrDefault();

                        reader.NextResult();
                        keywordSearchedPage = ((IObjectContextAdapter)db).ObjectContext.Translate<KeywordSearchedPage>(reader).ToList();


                        webResultUserDashboard.businessVisitCount = businessVisitCount;
                        webResultUserDashboard.bannerClicked = bannerClicked;
                        webResultUserDashboard.keywordSearched = keywordSearched;


                        webResultUserDashboard.smsToMobile = smsToMobile;
                        webResultUserDashboard.emailSent = emailSent;
                        webResultUserDashboard.keywordSearchedPage = keywordSearchedPage;
                        //resultUserDBTableObj.UserDBDetails = UserDBDetails;


                        //resultUserDBTableObj.KeywordSearchLocation = KeywordSearchLocation;
                        //resultUserDBTableObj.RecentlyPostedReviews = RecentlyPostedReviews;
                        //resultUserDBTableObj.RecentlyViewedBusiness = RecentlyViewedBusiness;

                    }
                    //listResSetTabParent.Add(listResultSetTableObj);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    db.Database.Connection.Close();
                }
                return webResultUserDashboard;
            }

        }
        public int prc_update_userPassword(ChangePassword cpwd)
        {
            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_update_userPassword]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@customerid", 665586));
                cmd.Parameters.Add(new SqlParameter("@Oldpassword", cpwd.OldPassword));
                cmd.Parameters.Add(new SqlParameter("@Password", cpwd.Password));
                cmd.Parameters.Add(new SqlParameter("@encriptedpassword", Vconnect.Common.Utility.Encrypt.encryptPassword(cpwd.Password.Trim())));
                cmd.Parameters.Add(new SqlParameter("@Err", SqlDbType.VarChar, 10));
                cmd.Parameters["@Err"].Direction = ParameterDirection.Output;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    result = Convert.ToInt32(cmd.Parameters["@Err"].Value.ToString());
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    db.Database.Connection.Close();
                }

            }
            return result;
        }
        public ResultWatchList prc_AddUp_userwatchlist(Int64 userid, Int64 businessid, string businessname)
        {
            ResultWatchList resultWatchList = new ResultWatchList();
            List<watchlist> watchlist = new List<watchlist>();
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_AddUp_userwatchlist]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@Businessid", businessid));
                cmd.Parameters.Add(new SqlParameter("@Businessname", businessname));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        watchlist = ((IObjectContextAdapter)db).ObjectContext.Translate<watchlist>(reader).ToList();
                        resultWatchList.watchlist = watchlist;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    db.Database.Connection.Close();
                }
                return resultWatchList;
            }
        }

        public WebResultRequirementPosted prc_get_userbusinessrequests(int Pagenum, int rowsperpage, int userid)
        {
            WebResultRequirementPosted webResReqPosted = new WebResultRequirementPosted();
           // List<RequirementPosted> requirementPosted = new List<RequirementPosted>();
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_userbusinessrequests]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@pageNum",1));
                cmd.Parameters.Add(new SqlParameter("@rowsPerPage",20));
                cmd.Parameters.Add(new SqlParameter("@senderid", userid));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        webResReqPosted.requirementPosted = ((IObjectContextAdapter)db).ObjectContext.Translate<RequirementPosted>(reader).ToList();
                        reader.NextResult();
                        webResReqPosted.totalresult = ((IObjectContextAdapter)db).ObjectContext.Translate<TotalRec>(reader).ToList(); 
                     

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    db.Database.Connection.Close();
                }
                return webResReqPosted;
            }
        }

        public WebResultReviewPosted prc_get_userbusinessreviews(int Pagenum, int rowsperpage, int userid)
        {
            WebResultReviewPosted webResReviewPosted = new WebResultReviewPosted();
            // List<RequirementPosted> requirementPosted = new List<RequirementPosted>();
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_userbusinessreviews]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@pageNum", 1));
                cmd.Parameters.Add(new SqlParameter("@rowsPerPage", 20));
                cmd.Parameters.Add(new SqlParameter("@senderid", userid));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        webResReviewPosted.reviewPosted= ((IObjectContextAdapter)db).ObjectContext.Translate<ReviewPosted>(reader).ToList();
                        reader.NextResult();
                        //webResReviewPosted.totalresult = ((IObjectContextAdapter)db).ObjectContext.Translate<TotalRecords>(reader).ToList();


                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    db.Database.Connection.Close();
                }
                return webResReviewPosted;
            }
        }
        public int prc_cupdate_userdetails(EditDetails editdetails, int stateid, int cityid, string sex, string dob)
        {
            int result = 0;


            Regex rgx1 = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");

            //for alternate number.
            if (!string.IsNullOrEmpty(editdetails.txtAltPhone.Trim().TrimEnd().TrimStart()) && !string.IsNullOrWhiteSpace(editdetails.txtAltPhone.Trim().TrimEnd().TrimStart()))
            {
                int result1 = Vconnect.Common.Utility.isValidPhone(editdetails.txtAltPhone.ToString());
                if (result1 == 0)
                {
                    result = 1;   //Alternate phone no. is not valid.
                    return result;
                }
            }
            else
            {
                editdetails.txtAltPhone = " ";
            }

            //for alternate email
            if (!string.IsNullOrEmpty(editdetails.txtaltemail.Trim().TrimEnd().TrimStart()) && !string.IsNullOrWhiteSpace(editdetails.txtaltemail.Trim().TrimEnd().TrimStart()))
            {
                if (!rgx1.IsMatch(editdetails.txtaltemail.ToString()))
                {
                    result = 3; //Enter a valid Email
                    return result;
                }
            }
            else
            {
                editdetails.txtaltemail = " ";
            }

            //for email
            if (!string.IsNullOrEmpty(editdetails.txtemail.Trim().TrimEnd().TrimStart()) && !string.IsNullOrWhiteSpace(editdetails.txtemail.Trim().TrimEnd().TrimStart()))
            {
                if (!rgx1.IsMatch(editdetails.txtemail.ToString()))
                {
                    result = 2;  //Enter a valid Email
                    return result;
                }
            }


            //for phone number...
            int result5 = Vconnect.Common.Utility.isValidPhone(editdetails.txtPhone.ToString());
            if (result5 == 0)
            {

                result = 5;   //phone no. is not valid.
            }
            else
            {
                using (var db = new VconnectDBContext29())
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[prc_cupdate_userdetails]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@contentid", 665586));
                    cmd.Parameters.Add(new SqlParameter("@contactname", editdetails.txtContactName));
                    if (!string.IsNullOrEmpty(editdetails.txtAddress.Trim().TrimEnd().TrimStart()) && !string.IsNullOrWhiteSpace(editdetails.txtAddress.Trim().TrimEnd().TrimStart()))
                    {
                        cmd.Parameters.Add(new SqlParameter("@address", editdetails.txtAddress));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@address", ""));
                    }
                    cmd.Parameters.Add(new SqlParameter("@stateid", stateid));
                    cmd.Parameters.Add(new SqlParameter("@cityid", cityid));
                    cmd.Parameters.Add(new SqlParameter("@countryid", 1));
                    cmd.Parameters.Add(new SqlParameter("@zipcode", editdetails.txtpostbox));
                    cmd.Parameters.Add(new SqlParameter("@phone1", editdetails.txtPhone));
                    cmd.Parameters.Add(new SqlParameter("@phone2", editdetails.txtAltPhone));
                    cmd.Parameters.Add(new SqlParameter("@email", editdetails.txtemail));
                    cmd.Parameters.Add(new SqlParameter("@alternateemail", editdetails.txtaltemail));
                    // cmd.Parameters.Add(new SqlParameter("@photo", " "));
                    cmd.Parameters.Add(new SqlParameter("@sex", sex));
                    cmd.Parameters.Add(new SqlParameter("@dateofbirth", dob));
                    cmd.Parameters.Add(new SqlParameter("@EditProfile", 1));
                    cmd.Parameters.Add(new SqlParameter("@Err", SqlDbType.VarChar, 10));
                    cmd.Parameters["@Err"].Direction = ParameterDirection.Output;
                    try
                    {
                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        cmd.ExecuteNonQuery();
                        result = Convert.ToInt32(cmd.Parameters["@Err"].Value.ToString());
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        db.Database.Connection.Close();
                    }

                }
            }
            return result;
        }
    }
}