﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vconnect.Mapping.Listing;
using Vconnect.Mapping.CommonDb;
using Vconnect.Mapping.ListingWEB;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using System.Web.DynamicData;
using System.Text;
using System.Data.SqlClient;
using System.Data.Entity.Infrastructure;
using Vconnect.Mapping;
using System.Data;

namespace Vconnect.Models
{

    public class CurrentPageInfo
    {
        private string _sourceurl = null;
        public string sourceurl
        {
            get
            {
                if (System.Web.HttpContext.Current.Request.Url != null)
                {
                    _sourceurl = System.Web.HttpContext.Current.Request.Url.ToString().ToLower().Trim();
                }
                return _sourceurl;
            }
        }
        private string _previousurl = null;
        public string previousurl 
        { 
            get 
            {
                if (System.Web.HttpContext.Current.Request.UrlReferrer != null)
                {
                    _previousurl = System.Web.HttpContext.Current.Request.UrlReferrer.ToString().ToLower().Trim();
                }
                return _previousurl;
            } 
        }
    }
    public class StaticPagesModel
    {
        public resultcareer Career { get; set; }
        public string SuccessMessage { get; set; }
        public string ErrorMessage { get; set; }
        #region Careers/Job
        public string txtfirstname { get; set; }
        public string txtsurname { get; set; }
        public string txtemailid { get; set; }
        public string txtdocument { get; set; }
        
        public string hiddenjobid { get; set; }

        public int prc_add_JobProfiles(string fname,string sname,string email,string documentpath,int jobid)
        {
            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_JobProfiles]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@firstname", fname));
                cmd.Parameters.Add(new SqlParameter("@surname", sname));
                cmd.Parameters.Add(new SqlParameter("@email", email));
                cmd.Parameters.Add(new SqlParameter("@documentpath", documentpath));
                cmd.Parameters.Add(new SqlParameter("@jobid", jobid));
                cmd.Parameters.Add(new SqlParameter("@Err", SqlDbType.VarChar, 10));
                cmd.Parameters["@Err"].Direction = ParameterDirection.Output;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    result = Convert.ToInt32(cmd.Parameters["@Err"].Value.ToString());
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    db.Database.Connection.Close();
                }

            }
            return result;
        }

        public resultcareer GetJobList()
        {
            resultcareer Resultcarrer = new resultcareer();
            List<Careers> jobList = new List<Careers>();

            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "prc_get_career_opt";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                // cmd.Parameters.Add(new SqlParameter("@searchtext", term));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    jobList = ((IObjectContextAdapter)db).ObjectContext.Translate<Careers>(reader).ToList();
                    Resultcarrer.career = jobList;

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message.ToString());
                }
                finally
                {
                    db.Database.Connection.Close();
                }
                return Resultcarrer;
            }
        }
        #endregion
        #region Contactus
        public int prc_add_contactus(contactus Contactus)
        {
            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_contactus]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@contactname", Contactus.name));
                cmd.Parameters.Add(new SqlParameter("@email", Contactus.email));
                if (string.IsNullOrEmpty(Contactus.phoneno) || string.IsNullOrWhiteSpace(Contactus.phoneno))
                {
                    cmd.Parameters.Add(new SqlParameter("@phone", ""));

                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@phone", Contactus.phoneno));
                }
                cmd.Parameters.Add(new SqlParameter("@message", Contactus.message));
                cmd.Parameters.Add(new SqlParameter("@Err", SqlDbType.VarChar, 10));
                cmd.Parameters["@Err"].Direction = ParameterDirection.Output;

                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    result = Convert.ToInt32(cmd.Parameters["@Err"].Value.ToString());
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    db.Database.Connection.Close();
                }

            }
            return result;
        }
        public contactus Contactus { get; set; }
        #endregion

        #region Business Forum
        public enterevent Enterevent { get; set; }
        public Websmeevent websmeevent { get; set; }
        public void getevent(int enentid)
        {
            websmeevent = prc_get_smeevent(enentid);
        }
        public Websmeevent prc_get_smeevent(int eventid)
        {
            Websmeevent webSmeevent = new Websmeevent();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_smeevent]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@eventid", eventid));

                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        webSmeevent.Smeevent = ((IObjectContextAdapter)db).ObjectContext.Translate<smeevent>(reader).ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    db.Database.Connection.Close();
                }
                return webSmeevent;
            }
        }

        public int prc_add_smeeventuser(enterevent EnterEvent, int eventid, string timeslot, string abc, string hidden)
        {
            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_smeeventuser]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@eventid", eventid));
                cmd.Parameters.Add(new SqlParameter("@companyname", EnterEvent.companyname));
                cmd.Parameters.Add(new SqlParameter("@surname", EnterEvent.lname));
                cmd.Parameters.Add(new SqlParameter("@contactperson", EnterEvent.fname));
                cmd.Parameters.Add(new SqlParameter("@designation", abc));
                cmd.Parameters.Add(new SqlParameter("@businessaddress", EnterEvent.companyadddress));
                cmd.Parameters.Add(new SqlParameter("@phone", EnterEvent.phone));
                cmd.Parameters.Add(new SqlParameter("@email", EnterEvent.email));
                cmd.Parameters.Add(new SqlParameter("@selectedeventdate", hidden));
                cmd.Parameters.Add(new SqlParameter("@timeslote", timeslot));
                cmd.Parameters.Add(new SqlParameter("@Err", SqlDbType.VarChar, 10));
                cmd.Parameters["@Err"].Direction = ParameterDirection.Output;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    result = Convert.ToInt32(cmd.Parameters["@Err"].Value.ToString());
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    db.Database.Connection.Close();
                }

            }
            return result;
        }
        #endregion
    }

    public class AdvertwithusModels
    {
        public string txtname { get; set; }
        public string txtcompany { get; set; }
        public string txtcontactno { get; set; }
        public string txtemail { get; set; }
        public string txtdetail { get; set; }

        public int StateID { get; set; }
        public int SelectedStateId { get; set; }
        public SelectList StateOptions { get; set; }
        public IEnumerable<SelectListItem> ListStatesNames { get; set; }

        public int PackageID { get; set; }
        public int SelectedPackage { get; set; }
        public SelectList PackageOptions { get; set; }
        public IEnumerable<SelectListItem> ListPackageNames { get; set; }

        public string SuccessMessage { get; set; }
        public string MessageError { get; set; }

        public int addadvertise(int userid, string companyame, string state, string contactperson, string phone, string email, string detail, string package,string source)
        {
            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_advertise_opt]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@companyame", companyame));
                cmd.Parameters.Add(new SqlParameter("@state", state));
                cmd.Parameters.Add(new SqlParameter("@contactperson", contactperson));
                cmd.Parameters.Add(new SqlParameter("@phone", phone));
                cmd.Parameters.Add(new SqlParameter("@email", email));
                cmd.Parameters.Add(new SqlParameter("@detail", detail));
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@package", package));
                cmd.Parameters.Add(new SqlParameter("@source", source.ToString()));
                //cmd.Parameters.Add(new SqlParameter("@stts", ParameterDirection.Output));
                cmd.Parameters.Add(new SqlParameter("@Err", SqlDbType.Int));
                cmd.Parameters["@Err"].Direction = ParameterDirection.Output;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    result = Convert.ToInt32(cmd.Parameters["@Err"].Value.ToString());
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            return result;
        }
    }
    public class Careers
    {
        public virtual string jobcategoryid { get; set; }
        public virtual string jobsubject { get; set; }
        public virtual string jobdesc { get; set; }
        public virtual string jobposted { get; set; }
        public Int32? jobid { get; set; }
        public virtual string jobcategory { get; set; }
    }
    public class resultcareer
    {
        public List<Careers> career { get; set; }

    }
    #region Feedback for WEB
    public class FeedbackModels
    {
        //public string product { get; set; }
        public string feedback { get; set; }
        public string source { get; set; }
        //public int productID { get; set; }
        public string SuccessMessage { get; set; }
        public string MessageError { get; set; }
        public string txtEmail { get; set; }
        public string txtPhone { get; set; }
        public int SelectedproductId { get; set; }
        public SelectList productOptions { get; set; }
        public IEnumerable<SelectListItem> ListproductNames { get; set; }
        public void AddFeedBack(string phone, string email, string product, string feedback, string source)
        {
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_feedback_new_opt]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@phone", phone));
                cmd.Parameters.Add(new SqlParameter("@email", email));
                cmd.Parameters.Add(new SqlParameter("@product", product));
                cmd.Parameters.Add(new SqlParameter("@feedback", feedback));
                cmd.Parameters.Add(new SqlParameter("@source", source));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();
                // reader.NextResult();
            }

        }
    }
    #endregion
    public class contactus
    {
        public string name { get; set; }
        public string email { get; set; }
        public string phoneno { get; set; }
        public string message { get; set; }


    }

    #region Business Forum Properties
    public class Websmeevent
    {
        public List<smeevent> Smeevent { get; set; }

    }
    public class smeevent
    {
        [Key]
        public Int64? contentid { get; set; }
        public virtual string eventdate { get; set; }
        public virtual string eventaddress { get; set; }
        public virtual string timeslot1 { get; set; }
        public virtual string timeslot2 { get; set; }
        public virtual string timeslot3 { get; set; }
        public virtual string timeslot4 { get; set; }
    }
    public class enterevent
    {
        public string fname { get; set; }
        public string lname { get; set; }
        public string companyname { get; set; }
        public string companyadddress { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
    }
    #endregion

    #region Admin Tools
    public class clientsadmin
    {
        public int leadid { get; set; }
        public string leadname { get; set; }
        public string chkrdo { get; set; }
        public string SuccessMessage { get; set; }
        public string MessageError { get; set; }
        public string txtqname { get; set; }
        public string txtlength { get; set; }
        public string txtplaceholder { get; set; }
        public string txtvalidation { get; set; }
        public string txtanswers { get; set; }
        public List<Listleadid> listlead { get; set; }
        public List<ColumnNamelist> listcolumn { get; set; }
        public List<RefColumnNamelist> listrefcolumn { get; set; }
        public Questionorder maxorder { get; set; }
    }
    public class Listleadid
    {
        [Key]
        public int leadid { get; set; }
        public string leadname { get; set; }
        public int businessid { get; set; }
    }
    public class Questionorder
    {
        public int maxorder { get; set; }
    }
    public class ColumnNamelist
    {
        public string columnname { get; set; }
        public string data_type { get; set; }
        public int intlength { get; set; }
    }
    public class RefColumnNamelist
    {
        public string refcolumnname { get; set; }
        public string data_type { get; set; }
        public int intlength { get; set; }
    }
    public class Listdropcolumn
    {
        [Key]
        public string dropcolumn { get; set; }
        public string data_type { get; set; }
    }
    public class Listtablecolumn
    {
        [Key]
        public string tablecolumn { get; set; }
    }
    public class clientsadminUpdate
    {
        public int leadid { get; set; }
        public string chkrdo { get; set; }
        public string tablebind { get; set; }
        public List<Listleadid> listlead { get; set; }
        public List<ListRowbyId> listrowbyId { get; set; }
        public List<Listdropcolumn> listdropcolumn { get; set; }
        public CountRow countrow { get; set; }
    }
    public class CountRow
    {
        public int countrowid { get; set; }
        public int anyactive { get; set; }
    }
    public class ListRowbyId
    {
        public int contentid { get; set; }
        public int leadid { get; set; }
        public string Qname { get; set; }
        public string Qtype { get; set; }
        public string compalsary { get; set; }
        public string validations { get; set; }
        public string uniquetype { get; set; }
        public string answers { get; set; }
        public string active { get; set; }
        public int qorder { get; set; }
        public int length { get; set; }
        public string columnname { get; set; }
        public string uniquename { get; set; }
        public string placeholder { get; set; }
        public string uniquemessage { get; set; }
        public string referenceid { get; set; }
        public string data_type { get; set; }
    }
    #endregion
}