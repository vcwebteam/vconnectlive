﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Vconnect.Common;

namespace Vconnect.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ActionTamperCookieAttribute : FilterAttribute, IActionFilter
    {

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {

        }
        
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

            UserSession objUS = new UserSession();
            string encrptdId = (objUS.SV_VCEncriptedContentid != null &&
                 objUS.SV_VCEncriptedContentid.Trim() != "") ?
                 objUS.SV_VCEncriptedContentid.Trim() : "";

            string _VCUserContentID = (objUS.SV_VCUserContentID != null &&
                 objUS.SV_VCUserContentID.Trim() != "") ?
                 objUS.SV_VCUserContentID.Trim() : "";


            int indx = encrptdId.LastIndexOf("$");
            string nwEncrptdId = string.Empty;
            string decrptdId = string.Empty;
            string _Asp_SessionIdEncr = string.Empty;
            string _Asp_SessionId = string.Empty;

            if (indx > -1)
            {
                nwEncrptdId = encrptdId.Substring(0, indx);
                _Asp_SessionIdEncr = encrptdId.Substring((indx + 1), (encrptdId.Length - (indx + 1)));

                if (HttpContext.Current.Request.Cookies[ConfigurationManager.AppSettings["VCUserContentID"]] != null
                    && HttpContext.Current.Request.Cookies[ConfigurationManager.AppSettings["VCUserContentID"]].Value != null)
                {
                    _VCUserContentID = HttpContext.Current.Request.Cookies[ConfigurationManager.AppSettings["VCUserContentID"]].Value.Trim();
                    indx = _VCUserContentID.LastIndexOf("$");
                    _Asp_SessionId = _VCUserContentID.Substring((indx + 1), (_VCUserContentID.Length - (indx + 1)));
                }
                decrptdId = (nwEncrptdId != null
                && nwEncrptdId.Trim() != "")
                ? Utility.DecryptString(nwEncrptdId, "VCUserEnc_Id") : "";

                nwEncrptdId = nwEncrptdId + "$" + _Asp_SessionIdEncr;
            }


            if (objUS.SV_VCUserContentID == null || objUS.SV_VCUserContentID == "") { }
            else if (objUS.SV_VCUserContentID != null && objUS.SV_VCEncriptedContentid != null
                    && objUS.SV_VCUserContentID.Trim() != ""
                    && objUS.SV_VCEncriptedContentid.Trim() != ""
                // && decrptdId == HttpContext.Current.objUS.SV_VCUserContentID.Trim()
                    && nwEncrptdId == encrptdId
                    && objUS.SV_VCUserContentID.Trim() == decrptdId.Trim()
                    && _Asp_SessionIdEncr.Trim() == _Asp_SessionId.Trim()
                // || (HttpContext.Current.Request.QueryString["id"] != null && HttpContext.Current.Request.QueryString["id"].ToString().Trim() != "")
               ) { }
            //else if(filterContext.RouteData.Values["user"]!=null && filterContext.RouteData.Values["user"].ToString().Trim()!=""){
            //            Utility.ClearCookies();
            //            Utility.ClearProperties();
            //            filterContext.Controller.TempData["flagCookie"] = "1";
            //}
            else
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "action", "LogOff" }, { "controller", "Account" } });
            }
        }
    }
}