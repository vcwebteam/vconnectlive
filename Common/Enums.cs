﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Net.Mail;
using System.Configuration;
using System.Collections;
namespace Vconnect.Enums
{
    public enum Result
    {
        Failure=0,
        Success=1
        
    }
    public enum WAPLoginSource
    {
        Facebook = 0,
        Twitter = 1,
        VConnect = 2,
        Eskimi = 3,
    }
   
   public enum SearchType
    {
        business = 5,
        brands = 3,
        category = 4,
        city = 6,
        products = 1,
        services = 2,
        businesskeyword = 0,
        normal = 7
    }
    public enum ActionType
    {
        Email = 2,
        SMS = 1,
    }
    public enum LeadSourceidType
    {
        ListingPageAutoLead = 1,
        searchnotfoundpage = 6,
        PYR = 3,
        PYRAutoLead = 4,
        WAPPYR = 5,
        BizPageContactnow = 2,
        BizPagePYR = 7,
        VideoDetailPagePYR = 8,
        searchnotfoundpageAutoLead = 9,
        TeleSearch = 10,
        SMSConnect = 11,
        VCOrders = 12,
        HomePageLead = 13,
        FlashLead = 14,
        FlashLeadByPhone = 15
    }
    //Lead table review status
    public enum LeadIsReview
    {
        pending = 0,
        approved = 1,
        AutoResolved = 2

    }
    /// <summary>
    /// USED IN SMS SEARCH
    /// </summary>
    public enum SMSType
    {
        buzz = 0,
        movie = 1,
        reg = 2,
        search = 3,
        cinema = 4,
        biz = 5,
        bizdatacollection = 6,
        searchdatacollection = 7,
        unknown = 9,
        find = 10,
        Events = 11,
        Event = 12,
        Add = 14,
        User = 15
    }

    public enum Status
    {
        Silver = 20,
        Gold = 30,
        Platinum = 40,
        NotSure = 0,
        Website = 60,
        BannerAdvert = 50,
        FreeTrial = 10,
        OnlineServices = 70,
        SMSMarketing = 80,
        Free = 1
    }

    public enum MessageType
    {
        Email_Bulk = 1,
        Email_Top = 2,
        Email_Individual = 3,
        Email_Registration = 4,
        Email_Forgot = 5,
        Email_SearchNotFound = 6,
        Email_ReportIncorrect = 7,
        Email_Lead = 8,
        Email_Invitation = 9,
        Email_VerifyCode = 10,
        SMS_Bulk = 11,
        SMS_Top = 12,
        SMS_Individual = 13,
        SMS_Search = 14,
        SMS_Lead = 15,
        SMS_UserReg = 16,
        SMS_BizReg = 17,
        SMS_MovieSearch = 18,
        SMS_SearchNotFound = 19,
        SMS_MovieSearchNotFound = 20,
        SMS_ErrorType = 21,
        SMS_CinemaSearch = 22,
        SMS_CinemaSearchNotFound = 23,
        SMS_VerifyCode = 24,
        SMS_SearchNotFoundReply = 25,
        Email_SearchNotFoundReply = 26,
        Email_UploadedFileApproval = 27,
        Email_BlockedIP = 30,
        SMS_UserReq = 31,
        SMS_LeadUserReq = 32,
        Email_UserReq = 34,
        Email_LeadUserReq = 35,
        SMS_CallRequest = 36,
        SMS_LeadCallRequest = 37,
        SMS_FEBizReg = 38,
        SMS_ErrorFEBizReg = 39,
        SMS_FESearch = 40,
        SMS_FESearchNotFound = 41,
        SMS_SearchLeadUserReq = 42,
        SMS_TeleSearchLead = 43,
        SMS_WebSearchLead = 44,
        Email_ExpirySubAlert = 45,
        Email_Proposal = 46,
        Email_PaymentDetail = 47,
        Email_SubscriptionReport = 48,
        Email_ProspectReminderMail = 49,
        SMS_Forgot = 50,
        SMS_CYB = 51,
        Email_CYBLoginInfo = 52,
        SMS_CYBLoginInfo = 53,
        SMS_CYBSuccessTransfer = 54,
        SME_Registration = 55,
        Email_Review = 56,
        Email_ReviewSharing = 57,
        SMS_AutomatedLead = 58,
        Email_AutomatedLead = 59,
        SMS_AutomatedBusiness = 60,
        Email_AutomatedBusiness = 61,
        Email_Feedback = 62,
        Email_FeedbackReply = 63,
        Email_SmeEvent = 64,
        SMS_SubscriptionReport = 66,
        SMS_SubscriptionNotification = 67,
        Email_EskimiRegistration = 68,
        Email_EskimiVerification = 69,
        SMS_BusinessRegistration = 70,
        SMS_EventPosted = 71,
        Email_Posturjob = 72,
        Email_BizReg = 73,
        SMS_Free = 99,
        Email_SuccessStory = 100,
        Email_Telesearch = 101,
        SMS_ReviewAdmin = 102,
        Email_ReviewAdmin = 103,
        Email_ToReviewer_bizFollowed = 104,
        Email_ToUser_bizrating = 105,
        Email_ToBizOwner_bizrating = 106,
        Email_MobileApp_Verify_RegisterUser = 107,
        SMS_MobileApp_Verify_RegisterUser = 108,
        Email_MobileApp_ForgotPassword = 109,
        Email_ToBizOwner_MarkedFavourite = 112,
        SMS_touser_availoffer = 113,
        SMS_tobusinessowner_availoffer = 114,
        Email_tobusinessowner_availoffer = 115,
        Email_MobileInactivate = 65

    }
    public enum Usertype
    {
        AdminBusinessUser = 1,
        b2cuser = 53,
        SMSConnectuser = 54,
        MovieUser = 55,
        EventUser = 56,
        B2BUser = 57,
        OtherCampaignUser = 58,
        B2CbusinessOwner = 59,
        OtherUser = 60,
        ReviewUser = 96,
        VCExecutive = 124,
        Claim_Your_Business = 125,
        LeadForward = 126,

    }
    public enum Sitetype
    {
        websearch = 1,
        telesearch = 2,
        smssearch = 3,
        wap = 4
    }

    public static class getenum
    {
        public static Hashtable GetEnumForBind(Type enumeration)
        {
            string[] names = Enum.GetNames(enumeration);
            Array values = Enum.GetValues(enumeration);
            Hashtable ht = new Hashtable();
            for (int i = 0; i < names.Length; i++)
            {
                ht.Add(Convert.ToInt32(values.GetValue(i)).ToString(), names[i].Replace('_', ' '));
            }
            return ht;
        }

        public static int GetHashCode(string val, Type enumeration)
        {
            int rtn = -1;
            string[] names = Enum.GetNames(enumeration);
            Array values = Enum.GetValues(enumeration);

            for (int i = 0; i < names.Length; i++)
            {
                if (names[i].ToLower() == val.Trim().ToLower())
                {
                    return i;
                }
            }

            return rtn;
        }

    }


    public static class SendMail
    {
        static public void Send(string fromEmail, string toEmail, string subject, string body, bool html)
        {
            System.Diagnostics.Debug.Assert(!string.IsNullOrEmpty(toEmail));
            System.Diagnostics.Debug.Assert(!string.IsNullOrEmpty(fromEmail));
            if (html)
            {
                Send(new System.Net.Mail.MailAddress(fromEmail), new System.Net.Mail.MailAddress(toEmail), subject, body, body);
            }
            else
            {
                Send(new System.Net.Mail.MailAddress(fromEmail), new System.Net.Mail.MailAddress(toEmail), subject, body);
            }
        }
        static public void Send(string fromEmail, string fromName, string toEmail, string toName, string subject, string body, bool html)
        {
            if (html)
            {
                Send(new System.Net.Mail.MailAddress(fromEmail, fromName), new System.Net.Mail.MailAddress(toEmail, toName), subject, body, body);
            }
            else
            {
                Send(new System.Net.Mail.MailAddress(fromEmail, fromName), new System.Net.Mail.MailAddress(toEmail, toName), subject, body);
            }
        }
        static public void Send(string fromEmail, string toEmail, string subject, string body, bool html, Dictionary<string, string> attachments)
        {
            if (html)
            {
                Send(new System.Net.Mail.MailAddress(fromEmail), new System.Net.Mail.MailAddress(toEmail), subject, body, body, attachments);
            }
            else
            {
                Send(new System.Net.Mail.MailAddress(fromEmail), new System.Net.Mail.MailAddress(toEmail), subject, body, attachments);
            }
        }
        static public void Send(string fromEmail, string fromName, string toEmail, string toName, string subject, string body, bool html, Dictionary<string, string> attachments)
        {
            if (html)
            {
                Send(new System.Net.Mail.MailAddress(fromEmail, fromName), new System.Net.Mail.MailAddress(toEmail, toName), subject, body, body, attachments);
            }
            else
            {
                Send(new System.Net.Mail.MailAddress(fromEmail, fromName), new System.Net.Mail.MailAddress(toEmail, toName), subject, body, attachments);
            }
        }
        static private void Send(System.Net.Mail.MailAddress fromAddress, System.Net.Mail.MailAddress toAddress, string subject, string bodyText, Dictionary<string, string> attachments)
        {
            Send(fromAddress, toAddress, subject, bodyText, null, attachments);
        }
        static private void Send(System.Net.Mail.MailAddress fromAddress, System.Net.Mail.MailAddress toAddress, string subject, string bodyText)
        {
            Send(fromAddress, toAddress, subject, bodyText, null, null);
        }
        static private void Send(System.Net.Mail.MailAddress fromAddress, System.Net.Mail.MailAddress toAddress, string subject, string bodyText, string bodyHtml)
        {
            Send(fromAddress, toAddress, subject, bodyText, bodyHtml, null);
        }


        static private void Send(System.Net.Mail.MailAddress fromAddress, System.Net.Mail.MailAddress toAddress, string subject, string bodyText, string bodyHtml, Dictionary<string, string> attachments)
        {


            try
            {
                using (System.Net.Mail.MailMessage emailMessage = new System.Net.Mail.MailMessage())
                {
                    emailMessage.To.Add(toAddress);
                    emailMessage.From = fromAddress;
                    emailMessage.Subject = subject;
                    Attachment[] attachFile;
                    if (attachments != null && attachments.Count > 0)
                    {
                        attachFile = new Attachment[attachments.Count];
                        int cnt = 0;
                        foreach (KeyValuePair<string, string> att in attachments)
                        {
                            attachFile[cnt] = new Attachment(ConfigurationManager.AppSettings["ImagesRootPath"].ToString() + "Attachments");
                            attachFile[cnt].ContentDisposition.FileName = att.Value;
                            emailMessage.Attachments.Add(attachFile[cnt]);
                            cnt++;
                        }
                    }
                    Encoding textEncoding = Encoding.UTF8;

                    if (!Regex.IsMatch(bodyText, @"^([0-9a-z!@#\$\%\^&\*\(\)\-=_\+])", RegexOptions.IgnoreCase) ||
                            !Regex.IsMatch(subject, @"^([0-9a-z!@#\$\%\^&\*\(\)\-=_\+])", RegexOptions.IgnoreCase))
                    {
                        textEncoding = Encoding.Unicode;
                    }

                    // add text view...
                    emailMessage.AlternateViews.Add(System.Net.Mail.AlternateView.CreateAlternateViewFromString(bodyText, textEncoding, "text/plain"));

                    // see if html alternative is also desired...
                    if (!String.IsNullOrEmpty(bodyHtml))
                    {
                        emailMessage.AlternateViews.Add(System.Net.Mail.AlternateView.CreateAlternateViewFromString(bodyHtml, Encoding.UTF8, "text/html"));
                    }

                    System.Net.Mail.SmtpClient smtpSend = new System.Net.Mail.SmtpClient();
                    smtpSend.EnableSsl = true;
                    smtpSend.Timeout = 200000;
                    smtpSend.Host = ConfigurationManager.AppSettings["Emailserver"].ToString().Trim();
                    smtpSend.Port = 587;
                    smtpSend.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtpSend.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["networkUserId"].ToString(), ConfigurationManager.AppSettings["networkUserPassword"].ToString().Trim());
                    smtpSend.Send(emailMessage);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }

}

