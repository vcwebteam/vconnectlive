﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Vconnect.Mapping.CommonDb;
using Vconnect.Models;
using System.Configuration;

namespace Vconnect.Common
{
    public static class MyGlobalVariables
    {
        public static string IsWapWeb { get; set; }
        public static string GetIpAddress { get; set; }        
        public static string GetDomain { get; set; }
        public static string GetReverseDns { get; set; }
        public static string _previousUrl { get; set; }
        public static string _sourceUrl { get; set; }
        public static int? _checkLoggedin { get; set; }
        public static string  ImagesRootPath { get {return  ConfigurationManager.AppSettings["ImagesRootPath"].ToString(); }  }
        public static string Staticimage { get { return ConfigurationManager.AppSettings["StaticImagesRootPath"].ToString(); } }
        public static bool noRedirect { get; set; }

        private static string _VCIsLoggedIn = string.Empty;
        public static string SV_VCIsLoggedIn
        {
            get
            {
                return _VCIsLoggedIn;
            }
            set
            {
                _VCIsLoggedIn = value;
            }
        }
    }
    public class Validate
    {
        //static public bool isvalidphone(string phone)
        //{
        //    //return DB.GetData("VCP_GetData", "Advertise.ID", ID).Tables[0];
        //    using (var db = new VconnectDBContext29())
        //    {

        //        // Create a SQL command to execute the sproc
        //        var cmd = db.Database.Connection.CreateCommand();
        //        cmd.CommandText = "[dbo].[prc_get_isvalidphone]";
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        //Add parameters to command object
        //        cmd.Parameters.Add(new SqlParameter("@phone", 1));

        //        try
        //        {


        //            var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
        //            connection.Open();
        //            var reader = cmd.ExecuteReader();
        //            if (reader.HasRows)
        //            {

        //            }

        //        }
        //        catch (Exception ex)
        //        {
        //            //DAL.Errorlog.Add("VConnect.BLL.Util.Advertise", "ID", ex);
        //            return null;
        //        }
        //    }
        //}
    }
}