﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Mvc;

namespace Vconnect.Common.HtmlHelpers
{
    public static class PagingHelpers
    {
        public static string PageLinks(this HtmlHelper html, int currentPage, int TotalPages, Func<int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();


            if (currentPage == TotalPages || TotalPages - 1 == currentPage || TotalPages - 2 == currentPage || TotalPages - 3 == currentPage)
            {
                for (int i = (TotalPages - 3); i <= TotalPages; i++)
                {

                    TagBuilder tag = new TagBuilder("a");
                    // Construct an <a> tag 
                    tag.MergeAttribute("href", pageUrl(i));
                    tag.InnerHtml = i.ToString();
                    if (i == currentPage)
                        tag.AddCssClass("selected");
                    result.AppendLine(tag.ToString());
                }
            }
            else
            {
                for (int i = currentPage; i <= (currentPage + 3); i++)
                {
                    if (i > TotalPages) break;
                    TagBuilder tag = new TagBuilder("a");
                    // Construct an <a> tag 
                    tag.MergeAttribute("href", pageUrl(i));
                    tag.InnerHtml = i.ToString();
                    if (i == currentPage)
                        tag.AddCssClass("selected");
                    result.AppendLine(tag.ToString());
                }
            }

            return result.ToString();
        }
        public static string PageLinksURL(this HtmlHelper html, int currentPage, int TotalPages, int type, string location, string tags, Func<int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();


            if (currentPage == TotalPages || TotalPages - 1 == currentPage || TotalPages - 2 == currentPage || TotalPages - 3 == currentPage)
            {
                for (int i = (TotalPages - 3); i <= TotalPages; i++)
                {
                    if (i < 2) i = 1;
                    TagBuilder tag = new TagBuilder("a");
                    // Construct an <a> tag 
                    if (type == 2)
                    {
                        if (i == 1)
                            tag.MergeAttribute("href", RoutingUtility.GetCatSearch(location, tags));
                        else
                            tag.MergeAttribute("href", RoutingUtility.GetCatSearch(location, tags, i));
                    }
                    else if (type == 1)
                    {
                        if (i == 1)
                            tag.MergeAttribute("href", RoutingUtility.GetBizSearch(location, tags));
                        else
                            tag.MergeAttribute("href", RoutingUtility.GetBizSearch(location, tags, i));
                    }
                    tag.InnerHtml = i.ToString();
                    if (i == currentPage)
                        tag.AddCssClass("selected");
                    result.AppendLine(tag.ToString());
                }
            }
            else
            {
                for (int i = currentPage; i <= (currentPage + 3); i++)
                {
                    if (i < 2) i = 1;
                    if (i > TotalPages) break;
                    TagBuilder tag = new TagBuilder("a");
                    // Construct an <a> tag 
                    // Construct an <a> tag 
                    if (type == 2)
                    {
                        if (i == 1)
                            tag.MergeAttribute("href", RoutingUtility.GetCatSearch(location, tags));
                        else
                            tag.MergeAttribute("href", RoutingUtility.GetCatSearch(location, tags, i));
                    }
                    else if (type == 1)
                    {
                        if (i == 1)
                            tag.MergeAttribute("href", RoutingUtility.GetBizSearch(location, tags));
                        else
                            tag.MergeAttribute("href", RoutingUtility.GetBizSearch(location, tags, i));
                    }
                    tag.InnerHtml = i.ToString();
                    if (i == currentPage)
                        tag.AddCssClass("selected");
                    result.AppendLine(tag.ToString());
                }
            }

            return result.ToString();
        }
        public static string PageLinksCatListing(this HtmlHelper html, int currentPage, int TotalPages, int contentid, string location, string tags, Func<int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();


            if (currentPage == TotalPages || TotalPages - 1 == currentPage || TotalPages - 2 == currentPage || TotalPages - 3 == currentPage)
            {
                for (int i = (TotalPages - 3); i <= TotalPages; i++)
                {
                    if (i < 2) i = 1;
                    TagBuilder tag = new TagBuilder("a");
                    // Construct an <a> tag 

                    if (i == 1)
                        tag.MergeAttribute("href", RoutingUtility.GetCatSearchlist(location, tags, contentid));
                    else
                        tag.MergeAttribute("href", RoutingUtility.GetCatSearchlist(location, tags, contentid, i));


                    tag.InnerHtml = i.ToString();
                    if (i == currentPage)
                        tag.AddCssClass("selected");
                    result.AppendLine(tag.ToString());
                }
            }
            else
            {
                for (int i = currentPage; i <= (currentPage + 3); i++)
                {
                    if (i < 2) i = 1;
                    if (i > TotalPages) break;
                    TagBuilder tag = new TagBuilder("a");
                    // Construct an <a> tag 
                    // Construct an <a> tag 

                    if (i == 1)
                        tag.MergeAttribute("href", RoutingUtility.GetCatSearchlist(location, tags, contentid));
                    else
                        tag.MergeAttribute("href", RoutingUtility.GetCatSearchlist(location, tags, contentid, i));

                    tag.InnerHtml = i.ToString();
                    if (i == currentPage)
                        tag.AddCssClass("selected");
                    result.AppendLine(tag.ToString());
                }
            }

            return result.ToString();
        }

        //public static string PageLinks(this HtmlHelper html, int currentPage, int totalPages, Func<int, string> pageUrl)
        //{
        //    StringBuilder result = new StringBuilder();
        //    for (int i = 1; i <= totalPages; i++)
        //    {
        //        TagBuilder tag = new TagBuilder("a"); // Construct an <a> tag
        //        tag.MergeAttribute("href", pageUrl(i));
        //        tag.InnerHtml = i.ToString();
        //        if (i == currentPage)
        //            tag.AddCssClass("selected");
        //        result.AppendLine(tag.ToString());
        //    }
        //    return result.ToString();
        //    //return HttpContext.Current.Server.HtmlEncode(result.ToString());
        //}
        public static string AlphabetRouting(this HtmlHelper html, int currentPage, string title, string location, Func<string, string> pageUrl)
        {
            string[] alphabet = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < alphabet.Length; i++)
            {
                TagBuilder outertag = new TagBuilder("li");
                TagBuilder tag = new TagBuilder("a");
                if (pageUrl(alphabet[i].ToString()) != null)
                {
                    tag.MergeAttribute("href", pageUrl(alphabet[i].ToString()).ToLower());
                }
                else
                {
                    tag.MergeAttribute("href", pageUrl(alphabet[i].ToString()));
                }
                tag.MergeAttribute("title", string.Format("{0} for {1} in {2}", title, alphabet[i].ToString(), location));
                tag.InnerHtml = alphabet[i].ToString();
                if (i == currentPage)
                    tag.AddCssClass("selected");
                outertag.InnerHtml = tag.ToString();
                result.AppendLine(outertag.ToString());
            }

            return result.ToString();
        }
    }
}