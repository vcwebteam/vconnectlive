﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections;


namespace Vconnect.Common
{
    public class UserSession
    {
        public static HttpCookieCollection VC_HttpCookieCollection = new HttpCookieCollection();
        public StringDictionary arrCookies = new StringDictionary();
        public UserSession()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        #region old code for cookie creation
        
        #endregion

        public string _uniqueId = string.Empty;

        public string UniqueId
        {
            get
            {
                string cookieName = ConfigurationManager.AppSettings["unk"];
                if (HttpContext.Current.Request.Cookies[cookieName] != null
                       && HttpContext.Current.Request.Cookies[cookieName].Value != null
                       && HttpContext.Current.Request.Cookies[cookieName].Value.ToString().Trim() != "")
                {
                    _uniqueId = HttpContext.Current.Request.Cookies[cookieName].Value;
                }
                return _uniqueId;
            }
        }
        private string _VCLoginType = null;
        public string SV_VCLoginType
        {
            get
            {
                if (IsSessionAlive())
                {
                    string cookieName = ConfigurationManager.AppSettings["VCLoginType"];
                    if (HttpContext.Current.Request.Cookies[cookieName] != null
                        && HttpContext.Current.Request.Cookies[cookieName].Value != null
                        && HttpContext.Current.Request.Cookies[cookieName].Value.ToString().Trim() != "")
                    {
                        // _VCLoginType = UserSession.SV_VCLoginType;
                        //_VCLoginType = HttpContext.Current.Request.Cookies[cookieName].Value;
                        _VCLoginType = ValidateCookieTampering(HttpContext.Current.Request.Cookies[cookieName].Value, "VCLoginType");
                    }
                    else
                    {
                        _VCLoginType = null;
                       // Utility.ClearCookies();
                     //   Utility.ClearProperties();
                    }
                    return _VCLoginType;
                }
                else
                    return null;

            }
            set
            {
                _VCLoginType = value;

                // Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCLoginType"], _VCLoginType);
                Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCLoginType"], _VCLoginType, "VCLoginType");

            }

        }

        private string _VCLoginID = null;
        public string SV_VCLoginID
        {
            get
            {
                if (IsSessionAlive() && MyGlobalVariables.SV_VCIsLoggedIn != "0")
                {
                    //_VCLoginID = HttpContext.Current.objUS.SV_VCLoginID;
                    //_VCLoginID = HttpContext.Current.Request.Cookies[ConfigurationManager.AppSettings["VCLoginID"]].Value;
                    _VCLoginID = ValidateCookieTampering(HttpContext.Current.Request.Cookies[ConfigurationManager.AppSettings["VCLoginID"]].Value, "VCLoginID");
                    return _VCLoginID;
                }
                else
                    return null;

            }
            set
            {
                _VCLoginID = value;
                Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCLoginID"], _VCLoginID, "VCLoginID");

                //Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCLoginID"], _VCLoginID);
            }
        }

        private string _VCUserName = null;
        
        public string SV_VCUserName
        {
            get
            {
                if (IsSessionAlive())
                {
                    string cookieName = ConfigurationManager.AppSettings["VCUserName"];
                    if (HttpContext.Current.Request.Cookies[cookieName] != null
                        && HttpContext.Current.Request.Cookies[cookieName].Value != null
                        && HttpContext.Current.Request.Cookies[cookieName].Value.ToString().Trim() != "")
                    {
                        // _VCUserName = objUS.SV_VCUserName;
                        //_VCUserName = HttpContext.Current.Request.Cookies[cookieName].Value;
                        _VCUserName = ValidateCookieTampering(HttpContext.Current.Request.Cookies[cookieName].Value, "VCUserName");

                    }
                    else
                    {
                        _VCUserName = null;
                       // Utility.ClearCookies();
                      //  Utility.ClearProperties();
                    }
                    return _VCUserName;
                }
                else
                    return null;
            }
            set
            {
                _VCUserName = value;

                // Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserName"], _VCUserName);
                Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserName"], _VCUserName, "VCUserName");

            }
        }

        private string _VCEncriptedContentid = null;
        public string SV_VCEncriptedContentid
        {
            get
            {
                if (IsSessionAlive())
                {
                    string cookieName = ConfigurationManager.AppSettings["VCEncriptedContentid"];
                    if (HttpContext.Current.Request.Cookies[cookieName] != null
                        && HttpContext.Current.Request.Cookies[cookieName].Value != null
                        && HttpContext.Current.Request.Cookies[cookieName].Value.ToString().Trim() != "")
                    {
                        // _VCUserName = objUS.SV_VCUserName;
                        _VCEncriptedContentid = HttpContext.Current.Request.Cookies[cookieName].Value;
                    }
                    else
                    {
                        _VCEncriptedContentid = null;
                     //   Utility.ClearCookies();
                     //   Utility.ClearProperties();
                    }
                    return _VCEncriptedContentid;
                }
                else
                    return null;
            }
            set
            {
                _VCEncriptedContentid = value;

                Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCEncriptedContentid"], _VCEncriptedContentid, "VCUserEnc_Id");
            }
        }

        private string _VCUserPhoto = null;
        public string SV_VCUserPhoto
        {
            get
            {
                if (IsSessionAlive())
                {
                    string cookieName = ConfigurationManager.AppSettings["VCUserPhoto"];
                    if (HttpContext.Current.Request.Cookies[cookieName] != null
                        && HttpContext.Current.Request.Cookies[cookieName].Value != null
                        && HttpContext.Current.Request.Cookies[cookieName].Value.ToString().Trim() != "")
                    {
                        // _VCUserName = objUS.SV_VCUserName;
                        //_VCUserPhoto = HttpContext.Current.Request.Cookies[cookieName].Value;
                        _VCUserPhoto = ValidateCookieTampering(HttpContext.Current.Request.Cookies[cookieName].Value, "VCUserPhoto");

                    }
                    else
                    {
                        _VCUserPhoto = null;
                      //  Utility.ClearCookies();
                      //  Utility.ClearProperties();
                    }
                    return _VCUserPhoto;
                }
                else
                    return null;
            }
            set
            {
                _VCUserPhoto = value;

                // Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserPhoto"], _VCUserPhoto);
                Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserPhoto"], _VCUserPhoto, "VCUserPhoto");

            }
        }

        private string _VCUserProviderId = null;
        public string SV_VCProviderId
        {
            get
            {
                if (IsSessionAlive())
                {
                    string cookieName = ConfigurationManager.AppSettings["VCUserProviderId"];
                    if (HttpContext.Current.Request.Cookies[cookieName] != null
                        && HttpContext.Current.Request.Cookies[cookieName].Value != null
                        //&& HttpContext.Current.Request.Cookies[cookieName].Value.ToString().Trim() != ""
                        )
                    {
                        // _VCUserName = objUS.SV_VCUserName;
                        //_VCUserProviderId = HttpContext.Current.Request.Cookies[cookieName].Value;
                        _VCUserProviderId = ValidateCookieTampering(HttpContext.Current.Request.Cookies[ConfigurationManager.AppSettings[cookieName]].Value, "VCUserProviderId");
                    }
                    else
                    {
                        _VCUserProviderId = null;
                      //  Utility.ClearCookies();
                       // Utility.ClearProperties();
                    }
                    return _VCUserProviderId;
                }
                else
                    return null;
            }
            set
            {
                _VCUserProviderId = value;

                //Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserProviderId"], _VCUserProviderId);
                Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserProviderId"], _VCUserProviderId, "VCUserProviderId");

            }
        }

        private string _VCUserUrl = null;
        public string SV_VCUserUrl
        {
            get
            {
                if (IsSessionAlive())
                {
                    string cookieName = ConfigurationManager.AppSettings["VCUserUrl"];
                    if (HttpContext.Current.Request.Cookies[cookieName] != null
                        && HttpContext.Current.Request.Cookies[cookieName].Value != null
                        && HttpContext.Current.Request.Cookies[cookieName].Value.ToString().Trim() != "")
                    {
                        //  _VCUserUrl = HttpContext.Current.Request.Cookies[cookieName].Value;
                        _VCUserUrl = ValidateCookieTampering(HttpContext.Current.Request.Cookies[cookieName].Value, "VCUserUrl");
                    }
                    else
                    {
                        _VCUserUrl = null;
                      //  Utility.ClearCookies();
                      //  Utility.ClearProperties();
                    }
                    return _VCUserUrl;
                }
                else
                    return null;
            }
            set
            {
                _VCUserUrl = value;

                //Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserUrl"], _VCUserUrl);
                Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserUrl"], _VCUserUrl, "VCUserUrl");

            }
        }

        private string _VCUserContentID = null;
        public string SV_VCUserContentID
        {
            get
            {
                if (IsSessionAlive())
                {
                    string cookieName = ConfigurationManager.AppSettings["VCUserContentID"];
                    if (HttpContext.Current.Request.Cookies[cookieName] != null
                        && HttpContext.Current.Request.Cookies[cookieName].Value != null
                        && HttpContext.Current.Request.Cookies[cookieName].Value.ToString().Trim() != "")
                    {
                        //_VCUserContentID = HttpContext.Current.objUS.SV_VCUserContentID;
                        //_VCUserContentID = HttpContext.Current.Request.Cookies[cookieName].Value;
                        _VCUserContentID = ValidateCookieTampering(HttpContext.Current.Request.Cookies[cookieName].Value, "VCUserContentID");
                    }
                    else
                    {
                        _VCUserContentID = null;
                     //  Utility.ClearCookies();
                     //   Utility.ClearProperties();
                    }
                    return _VCUserContentID;
                }
                else
                    return null;
            }
            set
            {
                _VCUserContentID = value;

                //Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserContentID"], _VCUserContentID);
                Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserContentID"], _VCUserContentID, "VCUserContentID");

            }
        }

        private string _VCUserType = null;
        public string SV_VCUserType
        {
            get
            {
                if (IsSessionAlive())
                {
                    string cookieName = ConfigurationManager.AppSettings["VCUserType"];
                    if (HttpContext.Current.Request.Cookies[cookieName] != null
                        && HttpContext.Current.Request.Cookies[cookieName].Value != null
                        && HttpContext.Current.Request.Cookies[cookieName].Value.ToString().Trim() != "")
                    {
                        //_VCUserType = HttpContext.Current.objUS.SV_VCUserType;
                        //_VCUserType = HttpContext.Current.Request.Cookies[cookieName].Value;
                        _VCUserType = ValidateCookieTampering(HttpContext.Current.Request.Cookies[cookieName].Value, "VCUserType");
                    }
                    else
                    {
                        _VCUserType = null;
                      //  Utility.ClearCookies();
                      //  Utility.ClearProperties();
                    }
                    return _VCUserType;
                }
                else
                    return null;
            }
            set
            {
                _VCUserType = value;

                // Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserType"], _VCUserType);
                Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserType"], _VCUserType, "VCUserType");

            }
        }

        private string _VCUserPhone = null;
        public string SV_VCUserPhone
        {
            get
            {
                if (IsSessionAlive())
                {
                    string cookieName = ConfigurationManager.AppSettings["VCUserPhone"];
                    if (HttpContext.Current.Request.Cookies[cookieName] != null
                        && HttpContext.Current.Request.Cookies[cookieName].Value != null
                        && HttpContext.Current.Request.Cookies[cookieName].Value.ToString().Trim() != "")
                    {
                        _VCUserPhone = ValidateCookieTampering(HttpContext.Current.Request.Cookies[cookieName].Value, "VCUserPhone");
                    }
                    else
                    {
                        _VCUserPhone = null;
                      //  Utility.ClearCookies();
                     //   Utility.ClearProperties();
                    }
                    return _VCUserPhone;
                }
                else
                    return null;
            }
            set
            {
                _VCUserPhone = value;
                Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserPhone"], _VCUserPhone, "VCUserPhone");

            }
        }

        private string _VCUserEmail = null;
        public string SV_VCUserEmail
        {
            get
            {
                if (IsSessionAlive())
                {
                    string cookieName = ConfigurationManager.AppSettings["VCUserEmail"];
                    if (HttpContext.Current.Request.Cookies[cookieName] != null
                        && HttpContext.Current.Request.Cookies[cookieName].Value != null
                        && HttpContext.Current.Request.Cookies[cookieName].Value.ToString().Trim() != "")
                    {
                        _VCUserEmail = ValidateCookieTampering(HttpContext.Current.Request.Cookies[cookieName].Value, "VCUserEmail");
                    }
                    else
                    {
                        _VCUserEmail = null;
                      //  Utility.ClearCookies();
                     //   Utility.ClearProperties();
                    }
                    return _VCUserEmail;
                }
                else
                    return null;
            }
            set
            {
                _VCUserEmail = value;

                Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserEmail"], _VCUserEmail, "VCUserEmail");

            }
        }

        /* currently in use */

        private string _VCPassword = null;
        public string SV_VCPassword
        {
            get
            {
                if (IsSessionAlive())
                {
                    _VCPassword = HttpContext.Current.Request.Cookies["VCPassword"].Value;
                }
                return _VCPassword;
            }


        }


        private string _VCUserbusinesscompanylogo = null;
        public string SV_VCUserbusinesscompanylogo
        {
            get
            {
                if (IsSessionAlive())
                {
                    _VCUserbusinesscompanylogo = HttpContext.Current.Request.Cookies["VCUserbusinesscompanylogo"].Value;
                }
                return _VCUserbusinesscompanylogo;
            }
        }

        private string _VCUserbusinessname = null;
        public string SV_VCUserbusinessname
        {
            get
            {
                if (IsSessionAlive())
                {
                    _VCUserbusinessname = HttpContext.Current.Request.Cookies["VCUserbusinessname"].Value.ToString().Replace(" ", "-").Replace("/", "-").Replace("'", "`").Replace("&", "and").Replace("--", "-");
                }
                return _VCUserbusinessname;
            }
        }

        private string _VCUserbusinessID = null;
        public string SV_VCUserbusinessID
        {
            get
            {
                if (IsSessionAlive())
                {
                    if (HttpContext.Current.Request.Cookies["VCUserbusinessID"] != null)
                    {
                        _VCUserbusinessID = HttpContext.Current.Request.Cookies["VCUserbusinessID"].Value;
                    }
                }
                return _VCUserbusinessID;
            }
        }

        private string _VCOldUserID = null;
        public string SV_VCOldUserID
        {
            get
            {
                if (IsSessionAlive())
                {
                    _VCOldUserID = HttpContext.Current.Request.Cookies["VCOldUserID"].Value;
                }
                return _VCOldUserID;
            }
        }

        private string _VCUserStateid = null;
        public string SV_VCUserStateid
        {
            get
            {
                if (IsSessionAlive())
                {
                    _VCUserStateid = HttpContext.Current.Request.Cookies["VCUserStateid"].Value;
                }
                return _VCUserStateid;
            }
        }

        private string _VCUserState = null;
        public string SV_VCUserState
        {
            get
            {
                if (IsSessionAlive())
                {
                    _VCUserState = HttpContext.Current.Request.Cookies["VCUserState"].Value;
                }

                return _VCUserState;
            }
        }

        private string _VCUserCityid = null;
        public string SV_VCUserCityid
        {
            get
            {
                if (IsSessionAlive())
                {
                    _VCUserCityid = HttpContext.Current.Request.Cookies["VCUserCityid"].Value;
                }
                return _VCUserCityid;
            }
        }

        private string _VCUserCity = null;
        public string SV_VCUserCity
        {
            get
            {
                if (IsSessionAlive())
                {
                    _VCUserCity = HttpContext.Current.Request.Cookies["VCUserCity"].Value;
                }

                return _VCUserCity;
            }
        }

        private string _VCUserUserProfilePhoto = null;
        public string SV_VCUserUserProfilePhoto
        {
            get
            {
                if (IsSessionAlive())
                {
                    _VCUserUserProfilePhoto = HttpContext.Current.Request.Cookies["VCUserUserProfilePhoto"].Value;
                }
                return _VCUserUserProfilePhoto;
            }
        }

        private string _VCUserContactName = null;
        public string SV_VCUserContactName
        {
            get
            {
                if (IsSessionAlive())
                {
                    _VCUserContactName = HttpContext.Current.Request.Cookies["VCUserContactName"].Value;
                }
                return _VCUserContactName;
            }
        }

        private string _VCUserbusinessphoto = null;
        public string SV_VCUserbusinessphoto
        {
            get
            {
                if (IsSessionAlive())
                {
                    _VCUserbusinessphoto = HttpContext.Current.Request.Cookies["VCUserbusinessphoto"].Value;
                }
                return _VCUserbusinessphoto;
            }
        }

        private string _VCcybercafe = null;
        public string SV_VCcybercafe
        {
            get
            {
                if (IsSessionAlive())
                {
                    _VCcybercafe = HttpContext.Current.Request.Cookies["VCcybercafe"].Value;
                }
                return _VCcybercafe;
            }
        }

        private string _VClga = null;
        public string SV_VClga
        {
            get
            {
                if (IsSessionAlive())
                {
                    _VClga = HttpContext.Current.Request.Cookies["VClga"].Value;
                }
                return _VClga;
            }
        }

        public void CheckSession()
        {
            UserSession objUS = new UserSession();

            HttpContext.Current.Response.AppendHeader("Cache-Control", "no-store");

            if (objUS.SV_VCLoginID == null)
            {
                HttpContext.Current.Response.Redirect("~/login/", true);
            }
        }

        private string _VCisagent = null;
        public string SV_VCisagent
        {
            get
            {
                if (IsSessionAlive())
                {
                    _VCisagent = HttpContext.Current.Request.Cookies["VCisagent"].Value;
                }
                return _VCisagent;
            }
        }

        //public void CheckPermission(string pageid,string userid)
        //{
        //    int result = 0;
        //    string[] pages = HttpContext.Current.Request.Cookies["pages"].Value.Split(',');
        //    if (pages.Length > 0)
        //    {
        //        for (int i = 0; i < pages.Length; i++)
        //        {
        //            if (pageid == pages[i])
        //            {
        //                result++;
        //            }                
        //        }       

        //    }

        //    if (result == 0)
        //    {
        //        Response.Redirect(ResolveUrl("~/permissiondenied.aspx"));
        //    }
        //}


        public bool IsSessionAlive()
        {
            if (HttpContext.Current.Request.Cookies[ConfigurationManager.AppSettings["VCLoginID"]] == null)
            {

                return false;

            }
            else
            {
                //MyGlobalVariables.SV_VCIsLoggedIn = "1";
                return true;
            }
        }
        #region old cookie code
      
        #endregion
        private string ValidateCookieTampering(string val, string key)
        {

            if (val.Trim() == null) return string.Empty;


            UserSession objUS = new UserSession();
            string encVal = string.Empty;
            string encUniqueId = string.Empty;

            try
            {
                encVal = (val != null && val.Trim() != "") ? val.Trim() : "";
                int indx = encVal.LastIndexOf("$");
                int rlen = (encVal.Length) - indx;


                if (indx > -1)
                {
                    encUniqueId = encVal.Substring((indx + 1), (rlen - 1)).Trim();
                    if (!(UniqueId == null)
                        && !string.IsNullOrEmpty(UniqueId.Trim())
                        && encUniqueId == UniqueId.Trim())
                    {
                        encVal = encVal.Substring(0, indx);
                        try
                        {
                            //encVal = Utility.Unprotect(encVal,key);
                            encVal = Utility.DecryptString(encVal, key);
                            //MyGlobalVariables.SV_VCIsLoggedIn = "1";
                        }
                        catch (Exception ex)
                        {
                            //Utility.ClearCookies();
                            //Utility.ClearProperties();
                            //Utility.arrCookies.Clear();
                            encVal = string.Empty;
                        }
                    }
                    else
                    {
                        Utility.ClearCookies();
                        Utility.ClearProperties();
                        //Utility.arrCookies.Clear();
                        encVal = string.Empty;
                    }
                }
                else
                {
                   // Utility.ClearCookies();
                  //  Utility.ClearProperties();
                    //Utility.arrCookies.Clear();
                 //   encVal = string.Empty;
                }

                return encVal;
            }
            catch (Exception ex)
            {
              //  Utility.ClearCookies();
                //Utility.ClearProperties();
                //Utility.arrCookies.Clear();
                encVal = string.Empty;
                return string.Empty;
            }
        }
        public void ValidateCookieTamperingAll()
        {
            //StringDictionary cookiesList = new StringDictionary();
            //cookiesList = (StringDictionary)arrCookies;


            UserSession objUS = new UserSession();
            string encVal = string.Empty;
            string encUniqueId = string.Empty;
            MyGlobalVariables.SV_VCIsLoggedIn = "1";
            foreach (DictionaryEntry cooList in arrCookies)
            {
                // use value.Key and value.Value
                try
                {

                    encVal = (!(HttpContext.Current.Request.Cookies[cooList.Key.ToString().Trim()] == null)
                        && !(HttpContext.Current.Request.Cookies[cooList.Key.ToString().Trim()].Value == null)
                        && HttpContext.Current.Request.Cookies[cooList.Key.ToString().Trim()].Value != "")
                        ? HttpContext.Current.Request.Cookies[cooList.Key.ToString().Trim()].Value.Trim() : "";

                    int indx = encVal.LastIndexOf("$");
                    int rlen = (encVal.Length) - indx;

                    if (indx > -1)
                    {
                        encUniqueId = encVal.Substring((indx + 1), (rlen - 1)).Trim();
                        if (!(UniqueId == null)
                            && !string.IsNullOrEmpty(UniqueId.Trim())
                            && encUniqueId == UniqueId.Trim())
                        {
                            encVal = encVal.Substring(0, indx);
                            try
                            {
                                //encVal = Utility.Unprotect(encVal,key);
                                encVal = Utility.DecryptString(encVal, cooList.Value.ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                Utility.ClearCookies();
                                Utility.ClearProperties();
                                //arrCookies.Clear();
                                encVal = string.Empty;
                                break;
                            }
                        }
                        else
                        {
                            Utility.ClearCookies();
                            Utility.ClearProperties();
                            //arrCookies.Clear();
                            encVal = string.Empty;
                            break;
                        }
                    }
                    else
                    {
                        Utility.ClearCookies();
                        Utility.ClearProperties();
                        //arrCookies.Clear();
                        encVal = string.Empty;
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Utility.ClearCookies();
                    Utility.ClearProperties();
                    //arrCookies.Clear();
                    encVal = string.Empty;
                }
            }
        }
        public static bool GenerateSession(string strVCLoginType, string strVCLoginID, string strVCUserName, string strVCUserContentID,
                       string strVCUserType, string strVCProviderID, string strVCUserPhoto, string strVCUserUrl, string strVCUserEmail, string strVCUserPhone)
        {

            string _uniqueId = string.Empty;

            if (_uniqueId == null || _uniqueId == "")
            {
                _uniqueId = Guid.NewGuid().ToString();
                HttpCookie objCookie = new HttpCookie("unk", _uniqueId);
                if (!HttpContext.Current.Request.Url.AbsoluteUri.Contains("localhost") && !HttpContext.Current.Request.Url.AbsoluteUri.Contains("mtest.vconnect.com") )
                {
                    objCookie.Domain = "vconnect.com";
                    objCookie.Path = "/";
                }
                objCookie.Expires = DateTime.Now.AddDays(3);
                HttpContext.Current.Response.Cookies.Add(objCookie);
            }

            if (strVCUserPhoto.IndexOf("upload") != -1 && strVCUserPhoto.IndexOf("images") == -1)
            {
                strVCUserPhoto = ConfigurationManager.AppSettings["ImagesRootPath"].ToString() + strVCUserPhoto;
            }

            //if (strVCUserPhoto.IndexOf("upload") != -1)
            //{
            //    strVCUserPhoto = ConfigurationManager.AppSettings["ImagesRootPath"].ToString() + strVCUserPhoto;
            //}

            if (strVCProviderID != null && strVCProviderID.Trim() != "")
            {
                Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserProviderId"], strVCProviderID, "VCUserProviderId");
            }

            HttpContext.Current.Session.Add("_checkLoggedin","1");

            MyGlobalVariables.SV_VCIsLoggedIn = "1";

            Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserUrl"], strVCUserUrl, "VCUserUrl");
            Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCLoginType"], strVCLoginType, "VCLoginType");
            Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCLoginID"], strVCLoginID, "VCLoginID");
            Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserName"], strVCUserName, "VCUserName");
            Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserContentID"], strVCUserContentID, "VCUserContentID");
            Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCEncriptedContentid"], strVCUserContentID, "VCUserEnc_Id");
            Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserType"], strVCUserType, "VCUserType");
            Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserPhoto"], strVCUserPhoto, "VCUserPhoto");
            Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserEmail"], strVCUserEmail, "VCUserEmail");
            Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserPhone"], strVCUserPhone, "VCUserPhone");


            return true;

        }

    }


}


   

