;(function($, window, document, undefined){
  window.app.comps.best_in_town = {
    name: 'best in town',
    description: 'description',
    init:function(){
      // Contains the initialization code
      // ...


       $('ul.c-list').find(".cat-category a")
                           .on("click",function(e){
      	                      e.preventDefault();
                              $('ul.c-list')
                               .find("a.active-o")
      	                        .removeClass('active-o');
      	                  $(this).addClass('active-o');
      });

      var mobileWidth = 640;
      $('.js-mobile-o').click(function(e){
        if ($('body').width() < mobileWidth) {
          e.preventDefault();
         document.location = 'added-mobile.html';
        };
      });

      this.events();
     },
    events:function(){
      // Contains the event bindings and subscriptions
    }
  };
}(jQuery, window, document));
