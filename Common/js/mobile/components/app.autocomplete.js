;(function($, window, document, undefined){
	window.app.comps.autoComplete = {
		name: 'autoComplete',
		description: 'description',
		config: {
			autoComplete: {
				keyWordEngine: null,
				locationEngine: null,
				keywordSearch: $('.keyword-autocomplete'),
				locationSearch: $('.location-autocomplete'),
				keywordDatasource : '../HomeWEB/KeywordAutoComplete?term=%QUERY',
				locationDatasource: '../HomeWEB/LocationAutoComplete?term=%QUERY',
				bodyWidth:window.innerWidth,
				leftOffest: $('.search-location .location-placeholder').outerWidth()
			}
		},
		
		init:function(){
			// Contains the initialization code
			var self = this;
			
			self.keywordAutoComplete();
			self.locationAutoComplete();
			self.fixSuggestionsWidth();

			self.fixOverlapbug();
			this.events();
		},
		keywordAutoComplete: function () {
			var self = this;
			self.config.autoComplete.keyWordEngine = new Bloodhound({
			datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.label); },
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				limit: 5,
				remote: {
					url: self.config.autoComplete.keywordDatasource,
				},
				local: [
				    { label: 'one' },
				    { label: 'two' },
				    { label: 'three' },
				    { label: 'four' },
				    { label: 'five' },
				    { label: 'six' },
				    { label: 'seven' },
				    { label: 'eight' },
				    { label: 'nine' },
				    { label: 'ten' }
	  			]
			});

			// self.config.autoComplete.keyWordEngine.remote.url = self.config.autoComplete.keywordDatasource;
			self.config.autoComplete.keyWordEngine.initialize();
			// instantiate the typeahead UI
			self.config.autoComplete.keywordSearch.typeahead(null, {
			  displayKey: 'label',
			  source: self.config.autoComplete.keyWordEngine.ttAdapter()
			});
		},

		locationAutoComplete: function () {
			var self = this;
			self.config.autoComplete.locationEngine = new Bloodhound({
			datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.label); },
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				limit: 5,
				remote: {
					url: self.config.autoComplete.locationDatasource,
				},
				local: [
				    { label: 'one' },
				    { label: 'two' },
				    { label: 'three' },
				    { label: 'four' },
				    { label: 'five' },
				    { label: 'six' },
				    { label: 'seven' },
				    { label: 'eight' },
				    { label: 'nine' },
				    { label: 'ten' }
	  			]
			});
			// self.config.autoComplete.locationEngine.remote.url = self.config.autoComplete.locationDatasource;
			self.config.autoComplete.locationEngine.initialize();
			// instantiate the typeahead UI
			self.config.autoComplete.locationSearch.typeahead(null, {
			  displayKey: 'label',
			  source: self.config.autoComplete.locationEngine.ttAdapter()
			});
		},

		fixOverlapbug: function () {
			var self=  this;

			$.each(self.config.autoComplete.keywordSearch, function () {
				var placeholder = $(this).attr('placeholder');
				//console.log(placeholder);
				$(this).focus(function  () {
					//console.log('focus');
					$(this).attr('placeholder', '');
				});
				$(this).blur(function  () {
					//console.log('blur');
					if ($(this).val().length < 1) {
						$(this).attr('placeholder', placeholder);
					};
				});
				$(this).change(function  () {
					//console.log('change');
					if ($(this).val().length < 1 ) {
						$(this).attr('placeholder', placeholder);
					};
				});
			});

			$.each(self.config.autoComplete.locationSearch, function () {
				var placeholder = $(this).attr('placeholder');
				//console.log(placeholder);
				$(this).focus(function  () {
					//console.log('focus');
					$(this).attr('placeholder', '');
				});
				$(this).blur(function  () {
					//console.log('blur');
					if ($(this).val().length < 1) {
						$(this).attr('placeholder', placeholder);
					};
				});
				$(this).change(function  () {
					//console.log('change');
					if ($(this).val().length < 1 ) {
						$(this).attr('placeholder', placeholder);
					};
				});
			})
		},

		fixSuggestionsWidth: function () {
			var self = this;
			$.each($('#search-header').children().find('.twitter-typeahead').children('.tt-dropdown-menu'), function () {
				$(this).width(self.config.autoComplete.bodyWidth);
				$('.search-location .tt-dropdown-menu')[0].style.left = self.config.autoComplete.leftOffest+'px';
			});
		},
		events:function(){
			// Contains the event bindings and subscriptions
			var self = this;
			$( window ).resize(function() {
				self.config.autoComplete.bodyWidth = window.innerWidth;
			  	self.fixSuggestionsWidth();
			});
			if ('orientation' in window) {
				self.config.autoComplete.bodyWidth = window.innerWidth;
				window.addEventListener('orientationchange', self.fixSuggestionsWidth());
			}
		}
	};
}(jQuery, window, document));