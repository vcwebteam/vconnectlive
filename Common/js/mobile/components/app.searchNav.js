;(function($, window, document, undefined){
	window.app.comps.searchNav = {
		name: 'searchNav',
		description: 'Mobile search navigation bar',
		config:{
			searchButton : $('#search-nav'),
            normalHeader : $('#normal-header'),
            searchHeader : $('#search-header'),
            mainSection  : $('.main-section'),
            overLayHtml  : "<div class='search-overlay' style='display:block'></div>",
            searchHeaderClose : $('.close-nav-link')
		},
		oldSearch:function () {
			var self = this;
			$(self.config.searchButton).click(function  (e) {
                e.preventDefault();
                self.config.normalHeader.hide();
                self.config.mainSection.prepend(self.config.overLayHtml);
                self.config.searchHeader.show();

            });
		},

		handleSearch:function () {
			var self = this;
			$('[data-search]').click(function(e){

			  e.preventDefault();
			  $(this).toggleClass('active');
			  $('.search-bar').toggleClass('hide');
			  $('.search-bar').toggleClass('active');
			  $('#normal-header').toggleClass('has-search');
			});

		},

		init:function(){
			// Contains the initialization code
			var self = this;
			this.oldSearch();
			this.handleSearch();
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
			var self =  this;
			$(self.config.searchHeaderClose).click(function (e) {
                e.preventDefault();
                self.config.searchHeader.hide();
                $('.search-overlay').remove();
                self.config.normalHeader.show();
            });
		}
	};
}(jQuery, window, document));
