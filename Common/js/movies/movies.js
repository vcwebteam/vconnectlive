;(function($, window, document, undefined){

    var showingDate, nextShowingDate, today, tomorrow, isSmall, isMedium, isLarge,
				now             = new Date().getTime(),
				aDay            = 86400000,//millisecnds
				currentIndex    = 0,
				$showTimeNavbar = $( ".show-time-navbar" ),
				$showDay        = $showTimeNavbar
					               .find( "span#showing-day" ),
				$timeHolder     = $showTimeNavbar
					               .find( 'span.date-holder' )
	  ;

	vcMovies = {

    checkWindowWidth: function () {
      if (window.innerWidth <= 640) {
        isSmall = true;
        return
      } else if (window.innerWidth >= 641 && window.innerWidth < 1025) {
        isMedium = true;
        return;
      } else{
        isLarge = true;
        return;
      }
    },

    resizeMobileWidth: function (argument) {
    	window.onresize = function () {
    		if (window.innerWidth >= 641 && window.innerWidth < 1025) {
    			$('body').addClass('f-topbar-fixed');
    		}
    		else if (window.innerWidth <= 640) {

    			$('body').removeClass('f-topbar-fixed');
    		}
    	}
    },

    formatDate: function (time) {
    	var d,t;
    	if ((time / 1000) < 1000000000) {// its timestamp
    		t = time * 1000;
    	}
    	else {// its js time
    		t = time;
    	};

    	d = new Date(t).toDateString().split(' ');

      date = d[0] + ', ' + d[1] + ' ' + d[2];

      return date;
    },

    searchView: {
    	setSmallFilter: function () {
     		var $listingHead       = $('.small-listing-head'),
            $movieWrapper      = $('.movie-wrapper'),
            $smallFilter       = $('.small-filter-wrapper'),
            $smallFilterHead   = $smallFilter.find('.filter-head'),
            $smallFilterCancel = $smallFilter.find('.cancel'),
            filterViewHeight   = window.innerHeight - ($('header').outerHeight(true))
        ;

        $listingHead
          .find('button.filter')
            .on('click',function() {
              $listingHead
                .removeClass('show-for-small-only')
                .hide();
              $movieWrapper.hide();
              $smallFilter.show();
              $smallFilterHead.show();
                $('.small-filter-view').height(filterViewHeight - 90);
        });

      	$smallFilterCancel.on('click',function() {
          $smallFilter.hide();
          $listingHead
            .addClass('show-for-small-only')
            .show();
          $movieWrapper.show();
      	});
   		},

   		setMoreView: function () {
      	var $moreMovies          = $('.more-movies-btn'),
            $morecinemas_list    = $('.more-cinemas_list-btn'),
            $cinemas_listWrapper = $('#cinemas_list-wrapper'),
            $moviesWrapper       = $('#movies-wrapper')
        ;

        $moreMovies.on('click',function(e) {
          e.preventDefault();
          $cinemas_listWrapper.hide();

          if (isMedium) {
            $moviesWrapper
              .removeClass('medium-6')
              .find('.movie')
                .addClass('medium-6');
              return;
          } else if (isLarge){
            $moviesWrapper
              .removeClass('large-8')
              .addClass('large-12')
              .find('.movie')
                .removeClass('large-6')
                .addClass('large-4');
          };
   		  });

        $('.more-cinemas_list-btn').on('click',function(e) {
          e.preventDefault();
          $moviesWrapper.hide();
          if (isMedium) {
            $cinemas_listWrapper
              .removeClass('medium-6')
              .addClass('medium-12')
              .find('.cinema-holder')
                .addClass('medium-6');
              return;
          } else if (isLarge){
            $cinemas_listWrapper
              .removeClass('large-4')
              .addClass('large-12')
              .find('.cinema-holder')
                .addClass('large-4');
          };
        });
      },

      init: function() {
      	vcMovies.checkWindowWidth();
        this.setSmallFilter();
        this.setMoreView();

        if (!isSmall) {
          $('.tabs-content, .content').removeClass('tabs-content');
        }
      }
    },

		reminder: {

		  $modal:  $('#reminderModal'),

		  sendReminder: function () {
				// var $modal    = vcMovies.reminder.$modal,
	   //      	movie_id  = $modal.find('.movie').data('movie-id'),
    //     		cinema_id = $modal.find('.cinema').data('cinema-id'),
    //     		timestamp = $modal.find('#reminder-time option:selected').data('time')
    //     ;

        var data         = {};
        data["movieID"]  = this.movie_id;
        data["cinemaID"] = this.cinema_id;
        data["time"]     = this.timestamp;
        data             = JSON.stringify(data);

				jQuery.ajax({
					url: 'http://stagemovie.vconnect.com/reminder',
					type: 'PUT',
					contentType: 'application/json',
					processData: false,
					data: data,
					success: function( data, textStatus, jqXHR ) {
						// console.dir( data );
						// console.log( textStatus );
						// console.dir( jqXHR );

						$modal.find('.first-stage').find('.wait').hide().end().fadeOut(200,function(){
		          $modal.find('.last-stage').fadeIn(400)
		                 .find('.close').on('click',function(){
					              $modal.foundation('reveal', 'close');
					              $(this).off('click');
					           });
		        });
					}
				});
		  },

		  setReminder: function () {// set and post the reminder
        var self = this;

      	self.movie_id  = this.$modal.find('.movie').data('movie-id');
    		self.cinema_id = this.$modal.find('.cinema').data('cinema-id');
    		self.timestamp = this.$modal.find('#reminder-time option:selected').data('time');

        this.$modal
        	.find('.ok')
        		.removeClass('disabled')
        		.on('click',function(e){
		          e.preventDefault();

		          var $this = $(this),
		          	$firstStage = $this.closest('.first-stage');

		          if (document.cookie.indexOf('AuthHash') >= 0) { // the user is loged in
		          	var $lastStage = $firstStage.siblings('.last-stage');
		          	$firstStage.find('.wait').fadeIn(400);
		            vcMovies.reminder.sendReminder();
		            $(this).addClass('disabled').off('click');
		              return;
		          }
		          else{
		          	var $secondStage = $firstStage.next(),
		          	    xpireDate    = new Date(now + aDay).toUTCString()
		          	;
		         		document.cookie = "vcMoviesReminderDate=movieID="
		         		                +self.movie_id
		         		                +", cinemaID="
		         		                +self.cinema_id
		         		                +", time="
		         		                +self.timestamp
		         		                +"; expires="
		         		                +xpireDate
		         		;

		            $firstStage.fadeOut(200,function(){
		              $secondStage.fadeIn(400);
		            });
		          }
            });
      },

		  buildReminderModal: function (){//builds and opens the reminder modal
        var $this       = $( this ),
            $gParent    = $this.closest( '.cinema-time' ),
            movie       = $( 'h1.movie-name' ),
            cinema      = $gParent.find( '.cinema-name' ),
            times       = $gParent.find('.time'),
            timeOptions = ''
        ;

        times.each(function(i){
          var option ='<option data-time="'
            + $(this).data("time")
            + '" value="'
            + $(this).text()
            + '">'
            + $(this).text()
            +'</option>'
          ;

          timeOptions += option;
        });

        vcMovies.reminder.$modal
         .find('.cinema')
        	.data('cinema-id', cinema.data('cinema-id'))
        	.text(cinema.text())
        	.end()
         .find('.movie')
        	.data('movie-id', movie.data('movie-id'))
        	.text(movie.text())
        	.end()
         .find(' .date-holder')
       		.val(new Date($this.data('time')*1000).toDateString())
       		.end()
         .find('#reminder-time')
          	.html(timeOptions)
            .val($this.text())
            .end()
         .find('.wait')
      		.hide()
      		.end()
         .foundation('reveal', 'open')
        ;

        $(document).on('closed.fndtn.reveal', '[data-reveal]', function () {
       	  var $this = $(this);

				  if ($this.is('div#reminderModal')) {
				    $this.find('.first-stage').show().siblings().hide()
				  };

				  $(document).off('closed.fndtn.reveal');
				});

        vcMovies.reminder.setReminder();
	    }
		},

		movieTimetable: {

			show_dates_list: [],

			showDate: function () {

			  if (vcMovies.formatDate(showingDate) === today ) {

					$showDay.text( "Today" );
				}
				else if (vcMovies.formatDate(showingDate) === tomorrow) {
					$showDay.text( "Tomorrow" );
				}
				else{
					$showDay.text(vcMovies.formatDate(showingDate));
				};

				if (vcMovies.formatDate(nextShowingDate) === tomorrow) {
			  	$timeHolder.text('Tomorrow');
				}
				else {
					$timeHolder.text(vcMovies.formatDate(nextShowingDate));
				};
			},

			buildTimeButtons: function ( timestamps, _showtimes ) {

				var showtimes = _showtimes.split(', '),
				    li        = ''
				;

				$.each(timestamps, function(i){
			     li += '<li  class="time-reminder"> <button data-time="'
			     				+ timestamps[i]
			            + '" class="small time radius icon-plus">'
			        		+ showtimes[i]
                  + '</button> </li>';
    		    });

                return li;
			},

			build_not_showing_html: function() {

				var style = '',
				    html  = '';
					// $cinemas_list_showtimes_container
					// text_to_show =

				style =  'color: gray;';
				style += 'background-color: #F0F0F0;';
				style += 'padding: 10px';

				html += '<h6 class="text-center" style="'
						+ style
						+ '">Sorry, this movie is not currently showing in cinemas!'
						+ '</h6>'
				;

				this.$cinemas_list_showtimes_container.html(html);
			},

			build_showtimes_html: function( cinemas_list ) {

				var cinema    = '',
					cinema_html = '',
					self        = this
				;

				if ( isSmall ) {

					function rotate ( icon, all_other_icons, deg ) {
						icon.css({
							'-ms-transform':     'rotate( '+ deg + 'deg)', /* IE 9 */
 						'-webkit-transform': 'rotate( '+ deg + 'deg)', /* Chrome, Safari, Opera */
  					'transform':         'rotate( '+ deg + 'deg)'
						});

						all_other_icons.css({
							'-ms-transform':     'rotate( 0deg )', /* IE 9 */
 						'-webkit-transform': 'rotate( 0deg )', /* Chrome, Safari, Opera */
  					'transform':         'rotate( 0deg )'
						});
					}

					function rotateIcon () {
						var $this      = $(this),
							  $icon      = $this.children('i.icon-down-open'),
							  $all_icons = $this.parent().siblings().find('i.icon-down-open');

						if ( !$icon.hasClass( 'rotated' ) ) {
							rotate($icon
									.addClass('rotated')
									.css('color','black'),

								   $all_icons
								   	.removeClass('rotated')
								   	.css('color','#A2A2A2'),

								   -180
							);
						}
						else if ($icon.hasClass('rotated')) {
							rotate($icon.removeClass('rotated').css('color','#A2A2A2'), $all_icons, 0);
						};
					}

					$.each(cinemas_list, function(i) {
						var el = '<dd class="accordion-navigation cinema-time">';
						el += ' <a href="#cinema_'
								+ cinemas_list[i].cinema_id
								+ '" class="cinema-name" data-cinema-id="'
								+ cinemas_list[i].cinema_id
								+ '">'
								+ cinemas_list[i].cineman_name
								+ '<i class="icon-down-open right"></i></a>'
						;

						el += ' <ul data-cinema-id="'
						    + cinemas_list[i].cinema_id
						    + '" id="cinema_'
						    + cinemas_list[i].cinema_id
						    + '" class="content inline-list">'
						    + self.buildTimeButtons(cinemas_list[i].timestamps, cinemas_list[i].show_times)
						    + ' </ul> </dd>'
						;

						cinema += el;
					    cinema_html += cinema;
					    cinema = '';
					});
				} else {

					$.each(cinemas_list, function(i) {

						var	el = '<ul class="small-block-grid-3 cinema-time"> ';
					    el +=   ' <li> <span class="cinema-name" data-cinema-id="'
					    		  + cinemas_list[i].cinema_id
					    		  + '">'
					    		  +cinemas_list[i].cineman_name
					    		  +'</span> </li>'
					    		;
					    el +=   ' <li> <ul class="time-buttons inline-list medium-centered">'
					              + self.buildTimeButtons(cinemas_list[i].timestamps, cinemas_list[i].show_times)
					              +' </ul> </li>'
					             ;
					    el +=   '</ul>'; //Replace this line with this when adding the REMIND ME FETURE el +=   '<li class="hide"> <button class="remind-me small radius icon-ok right" > REMIND ME </button> </li> </ul>';

					    cinema += el;
					    cinema_html += cinema;
					    cinema = '';
					});
				}

				this.$cinemas_list_showtimes_container.html(cinema_html)
					.find('button.time')
						.on('click', vcMovies.reminder.buildReminderModal)
						.end()
					.find('a.cinema-name')
						.on('click',rotateIcon);
			},

			regulateDate: function() {

				var self = vcMovies.movieTimetable;

				if ( $( this ).attr( "id" ) === "prev-btn" ) {//its back btn
					currentIndex -= 1;

					$timeHolder.show();
					$( this ).siblings( '#next-btn' ).css( "visibility", "visible" );

					if (currentIndex === 0) {
						$( this ).css( "visibility","hidden" );
					};
				}
				else if ( $( this ).attr( "id" ) === "next-btn" ) { //its next btn
					currentIndex += 1;

					if (!self.show_dates_list[currentIndex  + 1]) {
						$( this ).css( "visibility","hidden" );
						$timeHolder.hide();
					}

					$( this ).siblings( '#prev-btn' ).css( "visibility","visible" );
				}

				nextShowingDate = self.show_dates_list[currentIndex + 1];
				showingDate     = self.show_dates_list[currentIndex];
				self.showDate();
				self.build_showtimes_html( self.showTimes[self.show_dates_list[currentIndex]] );
			},

			init: function( showtimes ) {

				var self = this,
				    date_counter = 0;

				if ( isSmall ) {
					this.$cinemas_list_showtimes_container = $( 'dl.small-time-table' );
				} else {
					this.$cinemas_list_showtimes_container = $( '.showing-at-for-medium-up' );
				};

				if ( typeof showtimes !== 'object' ) {
					$( ".show-time-navbar" ).hide();
					return this.build_not_showing_html();
				}

        // remove past dates and augment show_dates_list[]
				$.each(showtimes, function  (date, val) {
					if ((date * 1000 + aDay)  <  now) {

						delete showtimes[date];
						return true;
					}
					else{
						self.show_dates_list[date_counter] = date;
						date_counter += 1;
					}
				});

				$(".show-time-navbar button#prev-btn")
				.css("visibility","hidden");

				if (this.show_dates_list.length < 2) {
					$(".show-time-navbar button#next-btn")
					.css("visibility","hidden");
					$timeHolder.hide();
				}
				else {
					nextShowingDate = this.show_dates_list[currentIndex + 1];
				}

				showingDate = this.show_dates_list[currentIndex];

				this.showTimes = showtimes;
				this.showDate();
				$(".show-time-navbar button").on('click',this.regulateDate);

				this.build_showtimes_html( this.showTimes[this.show_dates_list[currentIndex]] );
			}
		},

		searchNav:{
			settings : {
				searchButton : $('#search-nav'),
	            normalHeader : $('#normal-header'),
	            searchHeader : $('#search-header'),
	            mainSection  : $('.main-section'),
	            overLayHtml  : "<div class='search-overlay' style='display:block'></div>",
	            searchHeaderClose : $('.close-nav-link')
			},

			init : function () {
				var self = this;
				$(self.settings.searchButton).click(function  (e) {
	                e.preventDefault();
	                self.settings.normalHeader.hide();
	                self.settings.mainSection.prepend(self.settings.overLayHtml);
	                self.settings.searchHeader.show();

	            });
	            $(self.settings.searchHeaderClose).click(function (e) {
	                e.preventDefault();
	                self.settings.searchHeader.hide();
	                $('.search-overlay').remove();
	                self.settings.normalHeader.show();
	            });
			}
		},

		init: function ( showtimes ) {
			this.checkWindowWidth();

			if ( isSmall ) {
				$( 'body' ).removeClass( 'f-topbar-fixed' );//for parfect rendering of the off-canvas
			}

			this.resizeMobileWidth();
			this.searchView.init();
			this.searchNav.init();
			today           = vcMovies.formatDate(now),
			tomorrow        = vcMovies.formatDate(now + aDay),
   	  this.movieTimetable.init( showtimes );
		}
	}
})( jQuery, window, document );

