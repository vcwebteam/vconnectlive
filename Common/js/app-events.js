//var ie = !(navigator.userAgent.indexOf("MSIE 8.0") < 0);
var ieVersion = /*@cc_on (function() {switch(@_jscript_version) {case 1.0: return 3; case 3.0: return 4; case 5.0: return 5; case 5.1: return 5; case 5.5: return 5.5; case 5.6: return 6; case 5.7: return 7; case 5.8: return 8; case 9: return 9; case 10: return 10;}})() || @*/ 0;
// var isIE = !+'\v1';
var isIE = document.all && !document.addEventListener;
// if(ieVersion >= 9){
// }


//Preprocessing Plugins
//@prepros-append vendor/consolefix.js

//Foundation plugins
//@prepros-append vendor/foundation/foundation.js
//@prepros-append vendor/foundation/foundation.topbar.js
//@prepros-append vendor/foundation/foundation.reveal.js
//@prepros-append vendor/foundation/foundation.dropdown.js
//@prepros-append vendor/foundation/foundation.tab.js
//@prepros-append vendor/foundation/foundation.orbit.js
//@prepros-append vendor/foundation/foundation.magellan.js
//@prepros-append vendor/foundation/foundation.accordion.js
//@prepros-append vendor/foundation/foundation.abide.js
//@prepros-append vendor/foundation/foundation.offcanvas.js


//Other Plugins
//@prepros-append events/plugins.js


// App Declaration
// @prepros-append events/app.full.js
