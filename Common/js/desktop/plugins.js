// Plugins
// @prepros-append ../vendor/new/typeahead.bundle.min.js
// @prepros-append ../vendor/waypoints.min.js
// @prepros-append ../vendor/waypoints-sticky.min.js
// @prepros-append ../vendor/jquery.unveil.min.js
// @prepros-append ../desktop/components/contact.map.js
// @prepros-append ../vendor/jquery.cookie.js
// @prepros-append ../vendor/jquery.bxslider/jquery.bxslider.js

// Deactivated plugins
// prepros-append ../desktop/components/textchanger.js
// prepros-append ../vendor/dropzone.min.js
// prepros-append ../vendor/pikaday.js
// prepros-append ../vendor/jquery.swipebox.min.js
// prepros-append ../vendor/jquery.zclip.js
