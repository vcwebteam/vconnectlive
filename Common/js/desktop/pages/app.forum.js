/**
	forum - The script for the forums page interactivity

*/

;(function($){
	window.app.comps.forum = {
		name: 'forum',
		description: '',
		config:{
			$forumList: $('.forum-list'),
			$forumListItem: $('.forum-list li'),
			$backToForums: $('.back-forums')
		},
		init:function(){
			var cf = this.config;
			//Handles business forum form opening logic
			if(cf.$forumList.length<=0){
				return false;
			}
			this.events();
		},
		events:function(){
			var cf = this.config;

			cf.$forumListItem.on('click',function(e){
				e.preventDefault();
				$('.forum-2 .forumid').val($(this).data('forumid'));
				$('.forum-2 .forum-date').html($(this).find('.forum-date').html());
				$('.forum-2 .forum-address').html($(this).find('.forum-address').html());
				$('.forum-2 .forum-time').html($(this).find('.forum-time').html());
				$('.forum-1').fadeOut(function(){
					$('.forum-2').fadeIn();
				});
			});
			cf.$backToForums.on('click',function(e){
				e.preventDefault();
				$('.forum-2').fadeOut(function(){
					$('.forum-1').fadeIn();
				});
			});
		}
	};
}(jQuery));
