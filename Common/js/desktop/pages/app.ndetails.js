/**
	new details page - component_description

	Usage: 'How to use component'
	Options:
		Option_name - Option_description
			Values: Possible_option_values
	Example:
		Option_example
*/

;(function($, window, document, undefined){
	window.app.comps.ndetails = {
		init:function(){
			// Contains the initialization code
			// ...
			var self = this,
					cf = this.config;

			cf.storeViewToggleBtn = $('.grid-toggle-btn');
			cf.storeList = $('.st-items-list');
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
			var self = this,
					cf = this.config;

			cf.storeViewToggleBtn.off('click').on('click', function(e){
				e.preventDefault();
				cf.storeList.toggleClass('grid-view').toggleClass('list-view');
			});
		}
	};
}(jQuery, window, document));
