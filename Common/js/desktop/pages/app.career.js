/**
	career - Script for careers page interactivity

*/

;(function($){
	window.app.comps.career = {
		config:{
			$careerListContainer: $('.career-list-container'),
			$careerApplyButton: $('.career-list-container a.apply-btn'),
			$applyForm: $('#apply-form')
		},
		init:function(){
			var cf = this.config;
			//Handles careers application form logic
			if(cf.$careerListContainer.length<=0){
				return false;
			}
			this.events();
		},
		events:function(){
			var cf = this.config;
			cf.$careerApplyButton.on('click', function(e){
				e.preventDefault();
				$content = $(this).parent('div.content');
				jobid = $content.data('jobid');
				$heading = $content.find('h3').html();
				$('.jobid', cf.$applyForm).val(jobid);
				$('h3', cf.$applyForm).html($heading);
				// console.log(jobid + ',' + $heading);
				cf.$applyForm
					.siblings()
						.removeClass('active')
					.end()
					.addClass('active');
			});
		}
	};
}(jQuery));
