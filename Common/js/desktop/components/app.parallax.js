;(function($, window, document, undefined){
	window.app.comps.parallax = {
		name: 'parallax',
		description: 'Responsible for all parallax effects on site',
		init:function(){
			// Contains the initialization code
			var cf = this.config;
			//Initialise skrollr
			this.require('skrollr', function(){
				return typeof skrollr !== 'undefined'
			}, function(){
				if(!cf.$bannerOverlay.length){
					var s = skrollr.init();
					if(Modernizr.touch){
						s.destroy();
					}
				}
			});
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
		}
	};
}(jQuery, window, document));