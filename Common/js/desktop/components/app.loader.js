/**
	loader - Script for custom-built loader interactivity

	Usage: Call the loader using the pattern $(container).vcLoader();
*/

;(function($){
	window.app.comps.loader = {
		config:{
			$loader: $('.loading'),
			$loaderParent: $('.loading-container')
		},
		init:function(){
			//initialize loader
			$.fn.vcLoader = function(state,lContain){
				return this.initLoader(this,state,lContain);
			};
		},
		initLoader:function(container,state,lContain){
			var cf = this.config;
			if(typeof container !== 'undefined'){
				cf.$loaderParent = $(container);
				cf.$loaderParent.append('<div class="loading"></div>');
				if(typeof lContain !== 'undefined'){
					if(lContain){
						cf.$loaderParent.addClass('loading-container');
					}
				}
				cf.$loader = $(container).find('div.loading');
				if(typeof state === 'string'){
					this.runLoader(state);
				}
			}
		},
		runLoader:function(state){
			var cf = this.config;
			if(state === 'show'){
				cf.$loader.fadeIn();
			}
			else if(state === 'hide'){
				cf.$loader.fadeOut();
			}
		}
	};
}(jQuery));
