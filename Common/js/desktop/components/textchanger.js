$(document).ready(function(){
	//**textChanger - change text animation
	textChanger = {
		defaults: {
			changeClass: '.change',
			$changeSpan: $('span.change'),
			forwardSpeed: 100,
			backwardSpeed: 40,
			changeSpeed: 2000,
			delayBetweenSpeed: 300,
			changeValues: [
				"ababababa",
				"bebebebebe",
				"cicicicicic",
				"dodododododod"
			],
			cursorOrientation: 'vertical'
		},
		config: {},
		container: '',
		removeIntervalID: 0,
		addIntervalID: 0,
		changeTimeoutID: 0,
		delayBetweenTimeoutID: 0,
		oriValue: '',
		$verticalCursor: $('<span class="tc-cursor vertical">|</span>'),
		$horizontalCursor: $('<span class="tc-cursor horizontal">_</span>'),
		$cursor: this.$horizontalCursor,
		init: function(container, options){
			var tc = textChanger,
				defaults = tc.defaults;
			//init function

			tc.container = container;

			if($.isArray(options)){
				changeValues = options;
				$.extend(tc.config,defaults);
				tc.config.changeValues = changeValues;
			}
			else if(typeof options === 'object'){
				tc.config = $.extend({}, defaults, options);
			}
			else{
				$.extend(tc.config, defaults);
			}
			tc.config.$changeSpan = container.find(tc.config.changeClass);

			//set cursor
			if(tc.config.cursorOrientation == 'vertical'){
				tc.$cursor = tc.$verticalCursor;
			}
			else if(tc.config.cursorOrientation == 'horizontal'){
				tc.$cursor = tc.$horizontalCursor;
			}
			tc.$cursor.insertAfter(tc.config.$changeSpan);
			// tc.config.$changeSpan = container.find();
			clearInterval(tc.addIntervalID);
			clearInterval(tc.removeIntervalID);
			tc.removeValue();
		},
		getRandomInt: function(min, max){
			return Math.floor(Math.random() * (max - min + 1)) + min;
		},
		removeValue: function(){ //Remove current value
			var tc = textChanger,
				config = tc.config;
			curValue = config.$changeSpan.text();
			tc.oriValue = curValue;
			curLength = curValue.length;

			tc.removeIntervalID = setInterval(function(){
				config.$changeSpan.text(curValue.substr(0, curValue.length - 1));
				curValue = curValue.substr(0, curValue.length - 1);
				//console.log(curValue + ', ' + config.$changeSpan.text());
				if(curValue.length <= 0){
					clearInterval(tc.removeIntervalID);
					tc.delayBetweenTimeoutID = setTimeout(function(){
						tc.addValue();
					},config.delayBetweenSpeed);
				}
			},config.backwardSpeed);
		},
		addValue: function(){ //Add new value
			var tc = textChanger,
				config = tc.config,
				$changeSpan = config.$changeSpan;
			do{
				//Prevent same text from appearing
				newValueInt = tc.getRandomInt(0,config.changeValues.length - 1);
				newValue = config.changeValues[newValueInt];
			}while(newValue == tc.oriValue);

			newLength = newValue.length;

			//console.log(newValue);
			var i = 0;
			tc.addIntervalID = setInterval(function(){
				$changeSpan.text(newValue.substr(0,i+1));
				i++;
				//console.log(newValue + ', ' + i + ', ' + $changeSpan.text());
				if(i >= newLength || i > newValue.length){
					clearInterval(tc.addIntervalID);
					tc.changeTimeoutID = setTimeout(function(){
						tc.removeValue();
					},config.changeSpeed);
				}
			},100);
		}
	};
	$.fn.changeText = function(options){
		textChanger.init(this,options);
	};
});
