;(function($){
	window.app.comps.location = {
		name: 'location',
		description: 'Script for the location-related interactivity',
		init:function(){
			var self = this;
			if (cf.$locationBox.length > 0 && "geolocation" in navigator) {
				console.log("geolocation is avaliable");
				var geo_options = {
					enableHighAccuracy: true,
					maximumAge        : 30000,
					timeout           : 27000
				};
				navigator.geolocation.getCurrentPosition(this.success, this.error, geo_options);
			}
			else{
				console.log('Oh no! No geolocation. :(');
			}
		},
		success:function (position) {
			var self       = this;
				loc_coords = position.coords,
				latitude   = loc_coords.latitude,
				longitude  = loc_coords.longitude,
				accuracy   = loc_coords.accuracy;

			$.ajax({
				url:"http://maps.googleapis.com/maps/api/geocode/json?latlng="+position.coords.latitude+","+position.coords.longitude+"&sensor=true",
				success:function(result){
						if (result.status === "OK") {
							$.each(result.results, function(i){
								$.cookie('_'+this.types[0],this.formatted_address, {expires: 1/4});
							});
						}else{
							console.warn(result.status);
						}
				}
			});

			$.cookie("_lat",latitude, {expires: 1/4});
			$.cookie("_log",longitude, {expires: 1/4});
			$.cookie("_accuracy",accuracy, {expires: 1/4});
			this.setLocationBoxValue();
		},
		error: function (error) {
			//
			console.warn('ERROR(' + error.code + '): ' + error.message);
		},
		setLocationBoxValue:function() {
			var cf = this.config;
			var pageHasLocationBox = (
				cf.$locationBox.length >= 1 &&
				$.cookie('_sublocality')
				)? true : false;
			if (pageHasLocationBox) {
				var location  = $.cookie('_sublocality');
					location = location.split(',')[1];
					cf.$locationBox.each(function() {
					if ($(this).val().length == 0) {
						if ($(this).hasClass('directions-box')) {
							location  = $.cookie('_neighborhood');
						}
						$(this).val(location);
					};
				});
			};
		}
	};
}(jQuery));
