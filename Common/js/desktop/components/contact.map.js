(function($, window, document, undefined){
	var dev = true;
	if(!$('#contact-map').length)return false;
	if(typeof google != 'undefined'){
		function initialize() {
			var lagOffice = new google.maps.LatLng(6.483100, 3.357714);
			var abjOffice = new google.maps.LatLng(9.0666667, 7.4833333);
			var ibaOffice = new google.maps.LatLng(7.396389, 3.916667);
			var officeBounds = new google.maps.LatLngBounds(lagOffice,abjOffice,ibaOffice);
			// officeBounds.extend();
			var mapOptions = {
				zoom: 14,
				center: lagOffice,
				scrollwheel: false,
				styles:[{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}]
			};

			var map = new google.maps.Map(document.getElementById('contact-map'), mapOptions);
			map.fitBounds(officeBounds);
			var lagosAddress = '<div id="content">'+
					'<div id="siteNotice">'+
					'</div>'+
					'<h3 id="firstHeading" class="firstHeading">Lagos Office</h3>'+
					'<div id="bodyContent">'+
					'<p><b>VConnect Global Services Ltd</b>, 44, Eric Moore Road, Surulere, <br>Lagos 101283, Nigeria. Contact Number: 070 0000 8888</p>'+
					'</div>'+
					'</div>',
					abujaAddress = '<div id="content">'+
					'<div id="siteNotice">'+
					'</div>'+
					'<h3 id="firstHeading" class="firstHeading">Abuja Office</h3>'+
					'<div id="bodyContent">'+
					'<p><b>VConnect Global Services Ltd</b>, 44, Eric Moore Road, Surulere, <br>Lagos 101283, Nigeria. Contact Number: 070 0000 8888</p>'+
					'</div>'+
					'</div>',
					ibadanAddress = '<div id="content">'+
					'<div id="siteNotice">'+
					'</div>'+
					'<h3 id="firstHeading" class="firstHeading">Ibadan Office</h3>'+
					'<div id="bodyContent">'+
					'<p><b>VConnect Global Services Ltd</b>, 44, Eric Moore Road, Surulere, <br>Lagos 101283, Nigeria. Contact Number: 070 0000 8888</p>'+
					'</div>'+
					'</div>';

			//Initialise the infoWindows
			var infoWindows = {
				lagInfo: new google.maps.InfoWindow({
						content: lagosAddress
				}),
				abjInfo: new google.maps.InfoWindow({
						content: abujaAddress
				}),
				ibaInfo: new google.maps.InfoWindow({
						content: ibadanAddress
				})
			};

			//Initialise the vconnect marker image
			if(dev)var image = 'img/vconnect-map.png';
			else var image = 'http://static.vconnect.co/content/img/vconnect-map.png';

			//Instantiate the office markers
			var lagMarker = new google.maps.Marker({
					position: lagOffice,
					map: map,
					animation: google.maps.Animation.DROP,
					title: 'VConnect Head Office',
					icon: image
			}),
			abjMarker = new google.maps.Marker({
					position: abjOffice,
					map: map,
					animation: google.maps.Animation.DROP,
					title: 'VConnect Abuja Office',
					icon: image
			}),
			ibaMarker = new google.maps.Marker({
					position: ibaOffice,
					map: map,
					animation: google.maps.Animation.DROP,
					title: 'VConnect Ibadan Office',
					icon: image
			});
			infoWindows.lagInfo.open(map,lagMarker);
			//Add Click events to open infoWindows
			google.maps.event.addListener(lagMarker, 'click', function() {
				infoWindows.lagInfo.open(map,lagMarker);
			});
			google.maps.event.addListener(abjMarker, 'click', function() {
				infoWindows.abjInfo.open(map,abjMarker);
			});
			google.maps.event.addListener(ibaMarker, 'click', function() {
				infoWindows.ibaInfo.open(map,ibaMarker);
			});
		}

		google.maps.event.addDomListener(window, 'load', initialize);
	}
}(jQuery, window, document));
