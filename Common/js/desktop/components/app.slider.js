/**
	slider - component_description

	Usage: 'How to use component'
	Options:
		Option_name - Option_description
			Values: Possible_option_values
	Example:
		Option_example
*/

;(function($, window, document, undefined){
	window.app.comps.slider = {
		init:function(){
			// Contains the initialization code
			// ...
			var self = this,
					cf = this.config;

			cf.sliderBox = $('[' + this.attr_name('slider') + ']');
			cf.sliderList = cf.sliderBox.children('ul').first();
			cf.sliderPager = cf.sliderBox.children('[' + this.attr_name('slider-pager') + ']').first();

			cf.sliderList.bxSlider({
				pagerCustom: '[' + this.attr_name('slider-pager') + ']'
			});
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
		}
	};
}(jQuery, window, document));
