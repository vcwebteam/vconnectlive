;(function($, window, document, undefined){
	window.app.comps.tour = {
		name: 'tour',
		description: 'Responsible for the site tour logic',
		init:function(){
			// Contains the initialization code
			var cf = this.config;
			//Handles website tour
			this.require('cookie', function(){
				return jQuery.cookie;
			}, function(){
				// alert('Cookie!!!');
				if(!isIE)$(document).foundation('joyride', 'start');
			});
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
		}
	};
}(jQuery, window, document));