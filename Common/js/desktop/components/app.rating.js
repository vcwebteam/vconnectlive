/**
	rating - Script containing rating related interactivity

	Usage: Add the .js-ratings class for the rating
*/

;(function($){
	window.app.comps.rating = {
		config:{
			$jsRating: $('.js-ratings')
		},
		init:function(){
			var cf = this.config;
			if(cf.$jsRating.length <= 0){
				return false;
			}
			var curTitle = '';
			$title = $('span.title');
			cf.$jsRating.find('label').on('mouseover', function(){
				$(this).addClass('active').prevAll('label').addClass('active');
				var _title = $(this).attr('title');
				$title.text(_title);
				console.log(_title);
			}).on('mouseout', function(){
				$(this).removeClass('active').prevAll('label').removeClass('active');
				$title.text(curTitle);
			}).on('click', function(){
				var _curindex = $(this).parent().find('label').index(this);
				console.log($(this).parent().find('label').index(this));
				$thisG = cf.$jsRating.find('label:eq('+_curindex+')');
				$thisG.removeClass('set active').siblings('label').removeClass('set active');
				$thisG.addClass('set').prevAll('label').addClass('set');
				curTitle = $(this).attr('title');
				$title.text(curTitle);
				console.log(curTitle);
			});
		}
	};
}(jQuery));
