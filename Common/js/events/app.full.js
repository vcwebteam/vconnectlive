// App Declaration and components
// @prepros-append ../global/app.init.js
// @prepros-append ../global/components/app.font.js
// @prepros-append ../global/components/app.typeahead.js
// @prepros-append ../global/components/app.menu.js
// @prepros-append ../global/components/app.datepicker.js
// @prepros-append ../global/components/app.gallery.js
// @prepros-append pages/app.events.js
// @prepros-append components/rest.js
// @prepros-append ../global/components/app.copy.js
