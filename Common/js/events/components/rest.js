$(document).ready(function () {
	$.restive.startMulti();

	  $('body').restive({
	      breakpoints: ['240', '320', '480', '560', '640','768', '960', '1024', '1280'],
	      classes: ['css-240', 'css-320', 'css-480', 'css-560', 'css-640', 'css-768', 'css-960', 'css-1024', 'css-1280'],
	      turbo_classes: 'is_mobile=mobi,is_phone=phone,is_tablet=tablet,is_portrait=portrait,is_landscape=landscape',
	      force_dip: true
	  });

	 /* $('body').restive({
	  	  platform: 'ios',
	  	  breakpoints: ['240', '320', '480', '640', '960', '1024'],
	  	  classes: ['css-i-240', 'css-i-320', 'css-i-480', 'css-i-640', 'css-i-960', 'css-i-1024']
	  });

	  $('body').restive({
	  	  platform: 'android',
	  	  breakpoints: ['240', '320', '480', '640', '960', '1024'],
	  	  classes: ['css-a-240', 'css-a-320', 'css-a-480', 'css-a-640', 'css-a-960', 'css-a-1024']
	  });*/

	 $.restive.endMulti();
});

