;(function($, window, document, undefined){
	window.app.comps.events = {
		name: 'events',
		description: 'description',
		init:function(){
			// Contains the initialization code
			// ..
				var self = this;
				/*$('li.events-category').on('click', function(e){
					e.preventDefault();
						$(this).addClass('activet')
										.siblings('.activet')
										.removeClass('activet');
				});*/

			//Handles MOBILE TOP BAR TOP PADDING
				if (window.innerWidth <= 640) {
					$('body').removeClass('f-topbar-fixed');
				};


			// make the items selectable by toogling an 'active' class
				$('.cat-list li a').off('click.vc').on('click.vc', function (e) {
						// remove active class from all other items
						e.preventDefault();
						$('.cat-list li a').not($(this)).removeClass('active');
						// toggle the active class on the selected item
						$(this).toggleClass('active');
				});


				$(".choice").change(function () {
					if($(this).val() == "0") $(this).addClass("empty");
					else $(this).removeClass("empty")
				});
				$(".choice").change();


			/*-- Modal-Remind-Me Mobile Switch Initialize */
				var mobileWidth = 640;
				var windowWidth = $(window).width();
					$('.Modal').off('click.vc').on('click.vc', function(e){
						if (windowWidth > mobileWidth) {
							e.preventDefault();
							$('.firstModal').foundation('reveal', 'open');
						};
					});

				this.subscribe('vc:events/refresh', function(){
					self.init();
				});


			this.events();
		 },
		events:function(){
			// Contains the event bindings and subscriptions
		}
	};
}(jQuery, window, document));
