;(function($){
	app = {
		// Contains the configuration options
		config:{
			tabletViewportSize: 1024,
			$singleMenu: $('.single-menu > a'),
			$loggedInMenu: $('.loggedin-menu'),
			$tabSearch: $('.tab-search > a'),
			$searchForm: $('li.search-form'),
			$disableMenuSet: $('.single-menu > a, .loggedin-menu, .tab-search > a'),
			$jumbotronContainer: $('.jumbotron-container'),
			$introMessage: $('.intro-message'),
			$searchBox: $('.search-box'),
			$menuExpanded: $('.menu-expanded'),
			$menuCollapsed: $('.menu-collapsed'),
			typeCursorOrientation: 'vertical',
			$bannerOverlay: $('.banner-overlay'),
			$jSearchTerm: $('.jsearchterm'),
			$searchTerm: $('.searchterm'),
			$jSearchLocation: $('.jsearchlocation'),
			$searchLocation: $('.searchlocation'),
			$jSearchTermInput: $('.jsearchterm.tt-input'),
			$searchTermInput: $('.searchterm.tt-input'),
			$jSearchLocationInput: $('.jsearchlocation.tt-input'),
			$searchLocationInput: $('.searchlocation.tt-input'),
			$copyTextButton: $('.copybutton'),
			$copyText: $('.copytext'),
			zClipSwfPath: '/js/vendor/ZeroClipboard.swf',
			$imageThumbnail: $('.th'),
			termsURL: '../test.json',
			locationsURL: '../test.json',
			$addPhotoModal: $('#addPhotoModal'),
			isJoyrideCookie: true,
			cookieName: 'newDealsQSearch',
			cookieDomain: false,
			picker:$.noop(),
			$locationBox: $('.location-box'),
			$uploadAvatar: $('#upload-avatar'),
			$leadFormsContainer: $('.lead-form-content .forms-container'),
			$leadFormSections: $('.lead-form-content .form-section'),
			$leadFormNavigationList: $('.lead-form-content .navigation-list'),
			$userProfileContainer: $('.userprofile-content'),
			dataNamespace: 'data-vc',
			alertContainerClass: 'alerts',
			alertTimeOut: 7000,
			roleEl: {},
			arrRoles: ['show', 'next', 'prev', 'skip', 'loading', 'review', 'follow', 'like', 'save'],
			scriptURLs: {
				cookie: '/js/vendor/jquery.cookie.js',
				textchanger: '/js/desktop/components/textchanger.js',
				pikaday: '/js/vendor/min/pikaday.min.js',
				skrollr: '/js/vendor/skrollr.min.js',
				dropzone: '/js/vendor/dropzone.min.js',
				swipebox: '/js/vendor/jquery.swipebox.min.js',
				unveil: '/js/vendor/jquery.unveil.min.js',
				selectize: '/js/vendor/selectize.min.js',
				test: '/js/test.js',
				zclip: '/js/vendor/jquery.zclip.js',
				gmap: 'https://maps.googleapis.com/maps/api/js?v=3&sensor=false'
			}
		},
		// Empty jQuery object for event publications and subscriptions
		o:$({}),
		// ???
		scope:this,
		// Contains the various components of the application
		comps:{},
		// Contains the utility functions
		utils:{
			notifyMe:function(data){
				// Used to notify the user
				/*
					data - an object containing the message and type
				*/
				var cf = this.config,
					self = this,
					tpl = '<div data-vc-role="alert" class="vc-alert alert-box"><p class="alert-content"></p><a href="#" class="close">&times;</a></div>';
				// console.log(arguments);
				var message = data.message || data,
					type = data.type || 'success';

				if($('body').find('.' + cf.alertContainerClass).length > 0){
					container = $('.' + cf.alertContainerClass);
				}
				else{
					$('body').append($('<div>').addClass(cf.alertContainerClass));
					container = $('.' + cf.alertContainerClass);
				}

				var alertItem = {
					alertBox: {},
					alertTimer: {}
				};

				alertItem.alertBox = $(tpl).hide().addClass(type);
				container.append(alertItem.alertBox);
				alertItem.alertBox.find('.alert-content').html(message);
				alertItem.alertBox.fadeIn();

				alertItem.alertTimer = setTimeout(function(){
					// console.log('hi');
					alertItem.alertBox.fadeOut(function(){
						$(this).remove();
					});
				}, cf.alertTimeOut);
				container.off('.alert').on('click.vc.alert','[' + self.attr_name('role') + '="alert"] a.close', function(e){
					e.preventDefault();
					$(this).closest('[' + self.attr_name('role') + '="alert"]').fadeOut(function(){
						$(this).remove();
					});
				});
				// console.log(container);
				// console.log(alertItem.alertBox);
				// $('#notificationModal .modal-content').html(text);
				// $('#notificationModal').foundation('reveal','open');
				return this;
			},
			attr_name:function(str){
				// Used to generate application-specific data- attributes
				// e.g. data-vc-role
				var cf = this.config;
				return cf.dataNamespace + '-' + str;
			},
			require:function(script, test, func){
				// Used to install required scripts
				/*
					script: The name of the script to include
					test:  Test to run before installing
					func: Function to run after installing

					Pseudo:
						- `script` is required
						- check for script by running `test` function
						- if available then run `func` - the callback function
						- else install `script`
						- after installing, check if available again by running `test` function
						- if available then run `func`
						- else do nothing. Script URL or test function is wrong
				*/
				var self = this,
					cf = this.config,
					scriptURLs = cf.scriptURLs,
					testExists = true;
				/*
					One option: to be considered later
				var getScriptURL = function () {
					var scripts = document.getElementsByTagName('script');
					var index = scripts.length - 1;
					return scripts[index].src;
				}
				var getAssetsPath = function () {
					return getScriptURL().replace(/js\/.+\.js$/, '')
				}
				var installExternalScript = function (url) {
					document.write('<script type="text/javascript" src="' + url + '"></' + 'script>');
				}
				var installThirdPartyScript = function (fileName) {
					var assetsPath = getAssetsPath();
					var thirdPartyPath = assetsPath + 'js/thirdparty/';
					installExternalScript(thirdPartyPath + fileName);
				}
				*/

				var getScript = function(url, callback, cache){
					return jQuery.ajax({
			      url: url,
			      type: "GET",
			      dataType: "script",
			      success: callback,
			      cache: cache || true
			    });//.done(callback);
				}

				// func = func || test || function(){};
				// test =
				if(typeof func === "undefined"){
					if(typeof test == "undefined"){
						func = function(){};
						test = true;
					}
					else{
						func = test;
						test = true;
					}
					testExists = false;
				}

				if(test() && testExists){
					// run func
					func();
					return true;
				}
				else{
					// install script
					getScript(scriptURLs[script], function(){
						if(test()){
							func();
							return true;
						}
					});
				}
				return this;
			},
			render:function(template, data){
				/* Nano Templates (Tomasz Mazur, Jacek Becela) */
				return template.replace(/\{([\w\.]*)\}/g, function(str, key) {
					var keys = key.split("."), v = data[keys.shift()];
					for (var i = 0, l = keys.length; i < l; i++) v = v[keys[i]];
					return (typeof v !== "undefined" && v !== null) ? v : "";
				});
			}
		},
		init:function(config){
			var self = this,
				cf = this.config;

			// Preinitialisation tasks
			this.initPubSub();

			// Provide utility functions in application
			$.extend(this, this.utils);

			// Set the config options
			$.extend(true, this.config, config || {});

			// Set the behavior of the elements in the app
			app.initRoles();

			// Subscriptions
			this.subscribe('vc:notify', $.proxy(this.notifyMe,this));

			//initialize foundation
			if(jQuery().foundation){
				if(Foundation.libs.abide){
					Foundation.libs.abide.settings.patterns.password = /(?=^.{0,}$)[a-zA-Z0-9-\W+]+$/; /*/(?=^.{6,}$)([a-zA-Z0-9]+$)/;*/ ///^[a-zA-Z0-9]+$/;
					Foundation.libs.abide.settings.patterns.oldpassword = /(?=^.{0,}$)[a-zA-Z0-9-\W+]+$/; /*/(?=^.{6,}$)([a-zA-Z0-9]+$)/;*/
					Foundation.libs.abide.settings.patterns.email = /[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/;
					Foundation.libs.abide.settings.patterns.nigerianphone = /^0?(([1-6]\d{6,7})|([7-9]([0-1]\d{8}|[2-9]\d{6,7})))$/; /*/0?(([7-9][0-1]\d{8})|([1-8]\d{6,7}))/;*/
				}
				if(Foundation.libs.reveal)Foundation.libs.reveal.settings.animation = "fade";
				$(document).foundation({
					joyride:{
						// expose:true,
						modal:true,
						prevButton:true,
						cookie_monster: cf.isJoyrideCookie,
						cookie_name: cf.cookieName,
						cookie_domain: cf.cookieDomain,
						template : { // HTML segments for tip layout
					    link        : '<a href="#close" class="joyride-close-tip">&times;</a>',
					    timer       : '<div class="joyride-timer-indicator-wrap"><span class="joyride-timer-indicator"></span></div>',
					    tip         : '<div class="joyride-tip-guide"><span class="joyride-nub"></span></div>',
					    wrapper     : '<div class="joyride-content-wrapper"></div>',
					    button      : '<a href="#" class="tiny button white-btn joyride-next-tip"></a>',
					    prev_button : '<a href="#" class="tiny button white-btn joyride-prev-tip"></a>',
					    modal       : '<div class="joyride-modal-bg"></div>',
					    expose      : '<div class="joyride-expose-wrapper"></div>',
					    expose_cover: '<div class="joyride-expose-cover"></div>'
					  }
					}
				});
				$(document).foundation('joyride', 'start');
			}

			// Idea from Foundation by Zurb
			for(comp in this.comps){
				this.init_comp(comp);
			}

			return this;
		},
		init_comp:function(comp){
			if(this.comps.hasOwnProperty(comp)){
				this.patch(this.comps[comp]);
				return this.comps[comp].init.apply(this.comps[comp], []);
			}
		},
		patch:function(comp){
			// Patch component: Assign component variables
			comp.config = $.extend(true, {}, comp.config, this.config);

			//Provide access to utilities within component to all components
			$.extend(this.utils, comp.utils);

			// Provide access to utilities within component to application
			$.extend(this, comp.utils);

			// Provide access to utility functions in component
			$.extend(comp, this.utils);
			comp.o = this.o;
		},
		initPubSub:function(){
			//..From Jeffrey Way - Nettuts+
			// Initializes the PubSub system
			var self = this;
			$.each({
				trigger:'publish',
				on:'subscribe',
				off:'unsubscribe'
			}, function(key, val){
				app.utils[val] = function(){
					// Before: this for apply was app.o
					app.o[key].apply(app.o, arguments);
				}
			});
		},
		initRoles:function(){
			// Sets the element roles in the application
			var cf = this.config,
				self = this;

			cf.roleEl = $.extend({}, cf.roleEl);

			// var attr_name = this.attr_name;

			var role_el = function(role){
				return $('[' + self.attr_name('role') + '="'+ role + '"]');
			};

			// cf.arrRoles = ['show', 'next', 'prev', 'skip', 'loading', 'review'];
			// cf.arrRoles.forEach(function(elem, index){
			// 	cf.roleEl[elem] = role_el(elem);
			// });
			$.each(cf.arrRoles, function(index, elem){
				cf.roleEl[elem] = role_el(elem);
			});

			// Show role
			var $showEls = cf.roleEl.show,
				$checkShowEls = $showEls.filter('input[type="checkbox"],input[type="radio"]');
				// $specificShowEls = $showEls.filter('[data-vc-role="show"]')
			$checkShowEls.on('change',function(){
				if($(this).is(':checked')){
					//when checked, show the input next to it
					$(this).next('input').fadeIn();
				}
				else{
					$(this).next('input').fadeOut();
				}
			}).next('input').hide();

			cf.roleEl.next.on('click', function(e){
				e.preventDefault();
				// e.stopPropagation();
			});
			cf.roleEl.prev.on('click', function(e){
				e.preventDefault();
			});
		}
	};
}(jQuery));
