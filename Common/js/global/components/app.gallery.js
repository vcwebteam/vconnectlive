/**
	gallery - Contains the logic for the photo gallery functionality

	Usage: Add the .th class to each of the image gallery images. Also you can add the data-vc-gallery attribute to
	an element on the page and add the image properties as a JSON object to the data-vc-value attribute.
	Options:
		data-vc-gallery - Specifies the gallery values element
	Example:
		<li class="see-more" data-vc-gallery data-vc-value='[{"imageTitle":"First Lasante","imageUrl":"img/lab-abstract2.jpg"},{"imageTitle":"Second Burritos","imageUrl":"img/lab-logo.jpg"}]'></li>
*/

;(function($, window, document, undefined){
	window.app.comps.gallery = {
		config:{
			bigGalleryClass: 'big-image-gallery',
			galleryContainerClass: 'gallery-container',
			galleryHeaderClass: 'gallery-header',
			galleryDescriptionHeaderClass: 'gallery-description-header',
			galleryImageContainerClass: 'gallery-image-container',
			galleryDescriptionContainerClass: 'gallery-description-container',
			galleryCloseLinkClass: 'gallery-close',
			galleryPreviousLinkClass: 'gallery-previous',
			galleryNextLinkClass: 'gallery-next',
			galleryDisabledLinkClass: 'disabled',
			galleryImageCountClass: 'image-count',
			galleryLoadingClass: 'gallery-loading',
			galleryTitleClass: 'gallery-title',
			galleryUploadDetailsClass: 'gallery-upload-details',
			galleryUploadDateClass: 'upload-date',
			galleryUploaderNameClass: 'uploader-name',
			galleryUploaderThumbnailClass: 'uploader-thumbnail',
			galleryBusinessNameClass: 'business-name',
			galleryBusinessThumbnailClass: 'business-thumbnail',
			galleryBusinessDetailsClass: 'business-details',
			galleryDescriptionClass: 'gallery-description',
			galleryImageClass: 'gallery-image',
			galleryActionsContainerClass: 'gallery-actions-container',
			showDescriptionButtonClass: 'show-description',
			$bigGallery: {},
			$galleryContainer: {},
			$galleryHeader: {},
			$galleryDescriptionHeader: {},
			$galleryImageContainer: {},
			$galleryDescriptionContainer: {},
			$galleryCloseLink: {},
			$galleryPreviousLink: {},
			$galleryNextLink: {},
			$galleryImageCount: {},
			$galleryLoading: {},
			$galleryTitle: {},
			$galleryUploadDetails: {},
			$galleryUploadDate: {},
			$galleryUploaderName: {},
			$galleryUploaderThumbnail: {},
			$galleryBusinessName: {},
			$galleryBusinessThumbnail: {},
			$galleryBusinessDetails: {},
			$galleryDescription: {},
			$galleryActionsContainer: {},
			$showDescriptionButton: {},
			$imgSet: $(),
			isOpen: false,
			isBuilt: false,
			hasDescription: false,
			container: $('.th'),
			galleryImageArrayEls: {},
			imgsList: [],
			curIndex: 0,
			galleryTemplate: '<div class="big-image-gallery"> <div class="gallery-container"> <div class="gallery-header"> <a href="#" class="gallery-close">&times;</a> <div class="gallery-upload-details"> <img class="uploader-thumbnail" src="img/lab-profile300.jpg" alt="uploader thumbnail"> <div class="upload-date">24 June, 2014</div> <div class="uploader-name">Jonathan Douglas</div> </div> <h4 class="gallery-title">Triple Tripulley</h4> </div> <div class="gallery-image-container"> <div class="gallery-loading"></div></div> <div class="gallery-description-container"> <div class="gallery-description-header"> <div class="business-details"> <img class="business-thumbnail" src="img/lab-logo.jpg" alt="business logo"> <div class="business-name">Eko Hotels and Suites</div> </div> <span class="image-count">2/10</span> <div class="actions-container"> <!--<a href="#" class="like">Like</a>--> <a href="#" class="show-description"><i class="icon-menu"></i></a> </div> </div> <div class="gallery-description"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim id consequuntur iure molestiae debitis, soluta distinctio tempore maxime rerum, in ipsa vero! Quas eos, officiis consectetur hic voluptate nulla vero! </div> </div> </div> <div class="gallery-navigation-container"> <a href="#" class="gallery-previous disabled"><i class="icon-angle-left"></i></a> <a href="#" class="gallery-next"><i class="icon-angle-right"></i></a> </div> </div>'
		},
		init:function(){
			var _ = this,
					cf = _.config;

			// Update the cached container object
			cf.container = $(cf.container.selector);
			cf.galleryImageArrayEls = $('[' + this.attr_name('gallery') + '][' + this.attr_name('value') + ']');

			this.subscribe('vc:gallery/refresh', function(){
				_.init();
			});
			if(cf.container.length <= 0 && cf.galleryImageArrayEls.length <= 0)return false;

			cf.imgsList = [];

			//Get the image set from the container elements
			cf.container.each(function(index){
				cf.imgsList.push({
					imageUrl: $(this).attr('href'),
					imageTitle: $(this).attr('title'),
					imageDescription: $(this).next('.description').html(),
					imageThumbUrl: $(this).find('img').attr('src'),
					imageUploadDate: $(this).data('upload-date'),
					imageUploaderName: $(this).data('uploader-name'),
					imageUploaderThumbnail: $(this).data('uploader-thumbnail'),
					imageBusinessThumbnail: $(this).data('business-thumbnail'),
					imageBusinessName: $(this).data('business-name')
				});
			});

			// Get the image set from the VC Gallery elements
			cf.galleryImageArrayEls.each(function(index){
				curArray = JSON.parse($(this).attr(_.attr_name('value')));
				cf.imgsList = cf.imgsList.concat(curArray);
				console.log(cf.imgsList);
			});

			//Build the gallery
			_.buildGallery();

			// console.log('gallery activate!');
			this.events();
		},
		events:function(){
			// contains event bindings
			var _ = this,
					cf = _.config,
					distance = 0,
					minSwipeDistance = 10,
					isSwipeVertical = false,
					startCoords = {},
					endCoords = {},
					data = {};

			if (window.navigator.msPointerEnabled) {
				//Internet Explorer 10 style
				_eventTouchstart    = "MSPointerDown";
				_eventTouchmove     = "MSPointerMove";
				_eventTouchend      = "MSPointerUp";
			} else {
				_eventTouchstart    = "touchstart";
				_eventTouchmove     = "touchmove";
				_eventTouchend      = "touchend";
			}

			//Set click event for image set
			cf.container.on('click',function(e){
				e.preventDefault();
				cf.curIndex = cf.container.index(this);
				console.log(cf.imgsList[cf.curIndex]);
				if(!cf.isOpen){
					_.openGallery();
				}
			});

			//Set event listeners
			cf.$galleryCloseLink.on('click',function(e){
				e.preventDefault();
				_.closeGallery();
			});
			cf.$galleryPreviousLink.on('click',function(e){
				e.preventDefault();
				_.previousImage();
			});
			cf.$galleryNextLink.on('click',function(e){
				e.preventDefault();
				_.nextImage();
			});
			cf.$showDescriptionButton.on('click', function(e){
				e.preventDefault();
				_.toggleDescription();
			});
			$(document).on('keyup',function(e){
			  // handle cursor keys
			  switch(e.keyCode){
			  	case 37:
				    // left key
				    _.previousImage();
			  		break;
			  	case 39:
				    // right key
				    _.nextImage();
			  		break;
			  	case 27:
			  		// escape key
			  		_.closeGallery();
			  		break;
			  	case 38:
			  		// up key
			  		_.showDescription();
			  		break;
			  	case 40:
			  		// down key
			  		_.hideDescription();
			  		break;
			  }
			  console.log(e.keyCode);
			});

			cf.$galleryImageContainer.on(_eventTouchstart, function(e){
				if(!e.touches) e = e.originalEvent;
				console.log('Touch started.');

				data = {
					startPageX: (window.navigator.msPointerEnabled) ? e.pageX : e.touches[0].clientX,
					startPageY: (window.navigator.msPointerEnabled) ? e.pageY : e.touches[0].clientY,
					startTime: +new Date(),
					deltaX: 0,
					deltaY: 0,
					endPageX: 0,
					endPageY: 0
				};

				e.stopPropagation();
			}).on(_eventTouchmove, function(e){

				console.log('Touch moving.');

				e.preventDefault();
			}).on(_eventTouchend, function(e){
				if(!e.touches) e = e.originalEvent;
				// Ignore pinch/zoom events
				if(e.touches.length > 1 || e.scale && e.scale !== 1)return;
				console.log(e);
				data.endPageX = (window.navigator.msPointerEnabled) ? e.pageX : e.changedTouches[0].clientX,
				data.endPageY = (window.navigator.msPointerEnabled) ? e.pageY : e.changedTouches[0].clientY,

				data.deltaX = data.endPageX - data.startPageX;
				data.deltaY = data.endPageY - data.startPageY;
				isSwipeVertical = (Math.abs(data.deltaX) < Math.abs(data.deltaY))
				console.log('Touch stopped');
				if(isSwipeVertical){
					// show/hide description
					if(data.deltaY >= minSwipeDistance){
						_.hideDescription();
					}
					if(data.deltaY <= -minSwipeDistance){
						_.showDescription();
					}
				}
				else{
					// previous/next image
					if(data.deltaX >= minSwipeDistance){
						_.previousImage();
					}
					if(data.deltaX <= -minSwipeDistance){
						_.nextImage();
					}
				}
				data = {};
				e.stopPropagation();
			});

			$(window).on('resize', function(e){
				// adjust size
				_.adjustSize();
			});

			// Add close action to big gallery
			cf.$bigGallery.on('click', function(e){
				if(e.target == e.currentTarget){
					_.closeGallery();
				}
			});
		},
		preloadImages:function(){
			var _ = this,
					cf = _.config;
			//preload large images
			console.log('In preloadImages function.');
		},
		buildGallery:function(){
			var _ = this,
					cf = _.config;
			//build gallery
			cf.$bigGallery = $(cf.galleryTemplate);

			cf.$galleryImageContainer = cf.$bigGallery.find('.' + cf.galleryImageContainerClass);
			cf.$galleryContainer = cf.$bigGallery.find('.' + cf.galleryContainerClass);
			cf.$galleryDescriptionContainer = cf.$bigGallery.find('.' + cf.galleryDescriptionContainerClass);
			cf.$galleryHeader = cf.$bigGallery.find('.' + cf.galleryHeaderClass);
			cf.$galleryDescriptionHeader = cf.$bigGallery.find('.' + cf.galleryDescriptionHeaderClass);
			cf.$galleryDescription = cf.$bigGallery.find('.' + cf.galleryDescriptionClass);
			cf.$galleryCloseLink = cf.$bigGallery.find('.' + cf.galleryCloseLinkClass);
			cf.$galleryPreviousLink = cf.$bigGallery.find('.' + cf.galleryPreviousLinkClass);
			cf.$galleryNextLink = cf.$bigGallery.find('.' + cf.galleryNextLinkClass);
			cf.$galleryImageCount = cf.$bigGallery.find('.' + cf.galleryImageCountClass);
			cf.$galleryLoading = cf.$bigGallery.find('.' + cf.galleryLoadingClass);
			cf.$galleryTitle = cf.$bigGallery.find('.' + cf.galleryTitleClass);
			cf.$galleryUploadDetails = cf.$bigGallery.find('.' + cf.galleryUploadDetailsClass);
			cf.$galleryUploadDate = cf.$bigGallery.find('.' + cf.galleryUploadDateClass);
			cf.$galleryUploaderName = cf.$bigGallery.find('.' + cf.galleryUploaderNameClass);
			cf.$galleryUploaderThumbnail = cf.$bigGallery.find('.' + cf.galleryUploaderThumbnailClass);
			cf.$galleryBusinessName = cf.$bigGallery.find('.' + cf.galleryBusinessNameClass);
			cf.$galleryBusinessThumbnail = cf.$bigGallery.find('.' + cf.galleryBusinessThumbnailClass);
			cf.$galleryBusinessDetails = cf.$bigGallery.find('.' + cf.galleryBusinessDetailsClass);
			cf.$galleryActionsContainer = cf.$bigGallery.find('.' + cf.galleryActionsContainerClass);
			cf.$showDescriptionButton = cf.$bigGallery.find('.' + cf.showDescriptionButtonClass);

			$('body').append(cf.$bigGallery);
			console.log(cf.$bigGallery);

			// cf.imgsList.forEach(function(elem, index){
			// 	imgDiv = $('<div>').addClass(cf.galleryImageClass);
			// 	imgObj = $('<img>')
			// 		.load(function(){
			// 			$(this).hide();
			// 			$(this).fadeIn();
			// 		})
			// 		.error(function(){
			// 			console.log('Error loading image: ' + elem.imageUrl);
			// 		})
			// 		.attr('src',elem.imageUrl)
			// 		.attr('data-index',index);
			// 		console.log(imgObj);
			// 	imgDiv.append(imgObj)
			// 		.appendTo(cf.$galleryImageContainer);
			// });

			$.each(cf.imgsList, function(index, elem){
				imgDiv = $('<div>').addClass(cf.galleryImageClass);
				imgObj = $('<img>')
					.load(function(){
						$(this).hide();
						$(this).fadeIn();
					})
					.error(function(){
						console.log('Error loading image: ' + elem.imageUrl);
					})
					.attr('src',elem.imageUrl)
					.attr('data-index',index);
					console.log(imgObj);
				imgDiv.append(imgObj)
					.appendTo(cf.$galleryImageContainer);
			});

			console.log('In buildGallery function.');
			cf.isBuilt = true;
		},
		openGallery: function(){
			var _ = this,
					cf = _.config;
			//open the big image gallery
			if(!cf.isBuilt){
				_.buildGallery();
			}
			_.loadContent();

			//show the gallery
			cf.$bigGallery.fadeIn();

			// Adjust size
			_.adjustSize();
			console.log('In openGallery function.');
			cf.isOpen = true;
		},
		closeGallery:function(){
			var _ = this,
					cf = _.config;
			//close the big image gallery
			cf.$bigGallery.fadeOut();

			//Cleanup process
			_.emptyGallery();
			console.log('In closeGallery function.');
			cf.isOpen = false;
		},
		loadContent:function(callback){
			var _ = this,
					cf = _.config;
			//show current image
			curImgList = cf.imgsList[cf.curIndex];

			//empty gallery first
			_.emptyGallery();

			//load the gallery content:
			console.log(curImgList);

			//-image description
			if(curImgList.imageDescription){
				cf.hasDescription = true;
				cf.$showDescriptionButton.removeClass('hidden');
			}
			else{
				cf.hasDescription = false;
				_.hideDescription();
				cf.$showDescriptionButton.addClass('hidden');
			}
			cf.$galleryDescription.html(curImgList.imageDescription);

			//-image content
			// cf.$galleryImageContainer.find('.'+cf.galleryImageClass).filter(':eq(' + cf.curIndex + ')').show();
			cf.$galleryImageContainer.find('.'+cf.galleryImageClass).filter(':eq(' + cf.curIndex + ')').css('margin-left', 0);


			//-image count
			cf.$galleryImageCount.html(cf.curIndex+1 +' of ' + cf.imgsList.length);

			//-image title
			cf.$galleryTitle.html(curImgList.imageTitle || "");

			//-upload details
			cf.$galleryUploadDate.html(curImgList.imageUploadDate || "");
			cf.$galleryUploaderName.html(curImgList.imageUploaderName || "");
			cf.$galleryUploaderThumbnail.attr('src',curImgList.imageUploaderThumbnail || "");

			curImgList.imageUploadDate ? cf.$galleryUploadDate.show() : cf.$galleryUploadDate.hide();
			curImgList.imageUploaderName ? cf.$galleryUploaderName.show() : cf.$galleryUploaderName.hide();
			curImgList.imageUploaderThumbnail ? cf.$galleryUploaderThumbnail.show() : cf.$galleryUploaderThumbnail.hide();
			cf.$galleryUploadDetails.show();

			//-business details
			cf.$galleryBusinessName.html(curImgList.imageBusinessName || "");
			cf.$galleryBusinessThumbnail.attr('src',curImgList.imageBusinessThumbnail || "");

			curImgList.imageBusinessThumbnail ? cf.$galleryBusinessThumbnail.show() : cf.$galleryBusinessThumbnail.hide();
			cf.$galleryBusinessDetails.show();

			//-gallery controls state
			if(_.isFirst())cf.$galleryPreviousLink.addClass('disabled');
			else cf.$galleryPreviousLink.removeClass('disabled');
			if(_.isLast())cf.$galleryNextLink.addClass('disabled');
			else cf.$galleryNextLink.removeClass('disabled');
			console.log('In loadContent function.');
			if(typeof callback != 'undefined'){
				callback();
			}
		},
		previousImage:function(){
			var _ = this,
					cf = _.config;
			//show previous image
			if(!_.isFirst()){
				cf.curIndex--;
				_.loadContent();
			}
			console.log('In previousImage function.');
		},
		nextImage:function(){
			var _ = this,
					cf = _.config;
			//show next image
			if(!_.isLast()){
				cf.curIndex++;
				_.loadContent();
			}
			console.log('In nextImage function.');
		},
		isFirst:function(){
			var _ = this,
					cf = _.config;
			//check if first image
			return !cf.curIndex;
		},
		isLast:function(){
			var _ = this,
					cf = _.config;
			//check if last image
			return (cf.curIndex >= cf.imgsList.length-1);
		},
		emptyGallery:function(){
			var _ = this,
					cf = _.config;

			//empty the gallery
			// cf.$galleryImageContainer.find('.'+cf.galleryImageClass).hide();
			cf.$galleryImageContainer.find('.'+cf.galleryImageClass).css('margin-left', '100%');

			// cf.$bigGallery.find('img.current-image').detach();

			//-image count
			cf.$galleryImageCount.empty();

			//-image title
			cf.$galleryTitle.empty();

			//-image description
			// cf.$galleryDescriptionContainer.fadeOut();
			cf.$galleryDescription.empty();

			//-upload details
			cf.$galleryUploadDetails.hide();
			cf.$galleryUploaderName.empty();
			cf.$galleryUploadDate.empty();
			cf.$galleryUploaderThumbnail.attr('src','');

			//-business details
			cf.$galleryBusinessDetails.hide();
			cf.$galleryBusinessName.empty();
			cf.$galleryBusinessThumbnail.attr('src','');

			console.log('In emptyGallery function.');
		},
		adjustSize:function(){
			var _ = this,
					cf = _.config,
					imageHeight = 0;
			console.log('Adjusting the size.');
			imageHeight = cf.$galleryContainer.height() - cf.$galleryHeader.height() - cf.$galleryDescriptionHeader.height();
			cf.$galleryContainer.find('.' + cf.galleryImageClass).height(imageHeight);
			cf.$galleryContainer.find('.' + cf.galleryLoadingClass).height(imageHeight);
			console.log('Adjusted image height: ' + imageHeight);
		},
		showDescription:function(){
			var _ = this,
					cf = _.config;

			if(cf.isOpen & cf.hasDescription){
				cf.$galleryDescription.addClass('show');
				cf.$showDescriptionButton.addClass('active');
			}
			console.log('In showDescription function.');
		},
		hideDescription:function(){
			var _ = this,
					cf = _.config;

			if(cf.isOpen){
				cf.$galleryDescription.removeClass('show');
				cf.$showDescriptionButton.removeClass('active');
			}
			console.log('In hideDescription function.');
		},
		toggleDescription:function(){
			var _ = this,
					cf = _.config;

			if(cf.isOpen){
				if(cf.$galleryDescription.hasClass('show')){
					_.hideDescription();
				}
				else{
					_.showDescription();
				}
			}
			console.log('In toggleDescription function.');
		}
	};
}(jQuery, window, document));
