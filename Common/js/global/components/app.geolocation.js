;(function($, window, document, undefined){
	window.app.comps.geolocation = {
		name: 'geolocation',
		description: 'description',
		utils: {
			getLocation: function () {
				var self = this;
				return (self.getCookie('_getLoc') != "false") ? {
						lat: self.getCookie('_lat'), 
						log: self.getCookie('_log'),
						neighborhood: self.getCookie('_neighborhood'),
						colloquial_area: self.getCookie('_colloquial_area'),
						sublocality: self.getCookie('_sublocality')
					} : false;
			}
		},
		init:function(){
			// Contains the initialization code
			var self = this;
			if ("geolocation" in navigator && 
				self.getCookie('_getLoc') ||
				self.getCookie('_lat') || 
				self.getCookie('_log')) {

				self.publish("vc:geolocation/enabled");

				var geo_options = {
					enableHighAccuracy: true, 
					maximumAge        : 30000, 
					timeout           : 27000
				};
				navigator.geolocation.getCurrentPosition(function(pos){self.success(pos, self)}, this.error, geo_options);
			}else{
				console.warn('ERROR(GL001): Unable to access geolocation');
				self.createCookie("_getLoc","false");
			}
			this.events();
		},
		success: function (position, app) {
			var self       = app,
				loc_coords = position.coords,
				latitude   = loc_coords.latitude,
				longitude  = loc_coords.longitude,
				locationData = {},
				accuracy   = loc_coords.accuracy;

			$.ajax({
                url:"http://maps.googleapis.com/maps/api/geocode/json?latlng="+position.coords.latitude+","+position.coords.longitude+"&sensor=true",
                success:function(result){
                    if (result.status === "OK") {
                         $.each(result.results, function(i){
                         	self.createCookie('_'+this.types[0],this.formatted_address, 1/4);
                         	//self.locationData.('_'+this.types[0]) = this.formatted_address
                         });
                    }else{
                        console.warn(result.status);
                        self.createCookie("_getLoc","false");
                    }
                }
            });

 			self.createCookie("_lat",latitude, 1/4);
 			self.createCookie("_log",longitude, 1/4);
 			self.createCookie("_accuracy",accuracy, 1/4);
 			self.publish("vc:geolocation/current-location");
		},

		error: function (error, app) {

			console.warn('ERROR(' + error.code + '): ' + error.message);
		},
		events:function(){
			// Contains the event bindings and subscriptions
		}
		
	};
}(jQuery, window, document));