;(function($){
	window.app.comps.leadform = {
		name: 'leadform',
		description: 'Script for lead forms related interactivity',
		init:function(){
			var cf = this.config,
					_curIndex = 1,
					_totSections = 0;

			if(cf.$leadFormSections.length <= 0){
				return false;
			}

			var _showSection = function(){
				// cf.$leadFormSections.hide(function(){
				// 	cf.$leadFormSections.filter('[data-section-id="' + _curIndex + '"]').fadeIn();
				// });
				
				cf.$leadFormNavigationList.find('[data-section-id]').filter('[data-section-id="' + _curIndex + '"]')
					.parent('li').addClass('active current')
					.nextAll('li').removeClass('active current').end()
					.prevAll('li').addClass('active').removeClass('current');

				cf.$leadFormSections.hide();
				cf.$leadFormSections.filter('[data-section-id="' + _curIndex + '"]').fadeIn();
			}
			// alert(cf.$leadFormSections.length);
			// cf.$leadFormSections.hide();
			// Get the number of sections
			_totSections = cf.$leadFormSections.length;

			// Hide the loading container
			cf.$leadFormsContainer.find(cf.roleEl.loading).hide();

			// Show the first section
			_showSection();

			// Form navigation links event handling
			// Previous button event handling
			cf.$leadFormSections.find(cf.roleEl.prev).on('click', function(e){
				e.preventDefault();
				if(_curIndex > 1){
					_curIndex--;
					_showSection();
				}
			});

			// Next button event handling
			cf.$leadFormSections.find(cf.roleEl.next).on('click', function(e){
				e.preventDefault();
				if(_curIndex < _totSections){
					_curIndex++;
					_showSection();
				}
			});

			// Form navigation list event handling
			cf.$leadFormNavigationList.find('[data-section-id]').on('click', function(e){
				e.preventDefault();
				if(_curIndex != $(this).data('section-id')){
					_curIndex = $(this).data('section-id');
					_showSection();
				}
			});
		}
	};
}(jQuery));