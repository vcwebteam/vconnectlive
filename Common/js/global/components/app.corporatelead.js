/**
	corporatelead - Handles the behavior of the corporate leads component

*/

;(function($, window, document, undefined){
	window.app.comps.corporatelead = {
		config:{
			$cLeadPopup: $({}),
			$cLeadButton: $({})
		},
		init:function(){
			// Contains the initialization code
			// ...
			var cf = this.config;
			cf.$cLeadPopup = $('[' + this.attr_name('clead-popup') + ']');
			cf.$cLeadButton = $('[' + this.attr_name('clead-btn') + ']');

			if(!cf.$cLeadPopup.length)return false;

			cf.$cLeadPopup.foundation('reveal', 'open');
			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
			var self = this,
					cf = this.config;

			cf.$cLeadButton.on('click', function(e){
				e.preventDefault();
				cf.$cLeadPopup.foundation('reveal', 'open');
			});
		}
	};
}(jQuery, window, document));
