;(function($, window, document, undefined){
	window.app.comps.tabs = {
		name: 'tabs',
		description: 'description',
		init:function(){
			// Contains the initialization code
			var guid = null,
					self = this;

			$('[data-tab]').each(function (e) {
				$(this).children('li').each(function(el) {
					// console.log($(this).children('a').attr('href'));
					// guid = self.guid();
					// $(this).attr('data-guid', guid);
					// $(this).children('a').attr('data-guid', guid);
					// $($(this).children('a').attr('href')).attr('data-guid', guid);

					if (!$(this).hasClass('active')) {
						$($(this).children('a').attr('href')).addClass('hide');
					};
					$(this).click(function(e) {
						e.preventDefault();
						if (!$(this).hasClass('active')) {
							var self = this,
									// elid = $(self).attr('data-guid');
									actv = $(self).parent('[data-tab]').children('li.active');

							$(actv).removeClass('active');
							$($(actv).children('a').attr('href')).addClass('hide');
							if ($('[data-header='+$($(actv).children('a').attr('href')).attr('data-footer')+']')) {
								$('[data-header='+$($(actv).children('a').attr('href')).attr('data-footer')+']').addClass('hide');
							};

							$(self).addClass('active');
							$($(self).children('a').attr('href')).removeClass('hide');
							if ($('[data-header='+$($(self).children('a').attr('href')).attr('data-footer')+']')) {
								$('[data-header='+$($(self).children('a').attr('href')).attr('data-footer')+']').removeClass('hide');
							};

						};
					});
				});
			});

			this.events();
		},
		/*guid:function () {
			var CHARS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
					LEN   = 10,
					result = '';
	    for (var i = LEN; i > 0; --i){
	    	result += CHARS[Math.round(Math.random() * (CHARS.length - 1))];
	    }
	    return result;
		},*/

		events:function(){
			// Contains the event bindings and subscriptions
		}
	};
}(jQuery, window, document));
