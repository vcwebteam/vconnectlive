/**
	modal - script for the behaviour of modal boxes

	Usage: 'How to use component'
	Options:
		Option_name - Option_description
			Values: Possible_option_values
	Example:
		Option_example
*/

;(function($, window, document, undefined){
	window.app.comps.modal = {
		config: {
			modalTrigger: {}
		},
		init:function(){
			// Contains the initialization code
			var cf = this.config;
			cf.modalTrigger = $('[' + this.attr_name('modal') + ']');

			this.events();
		},
		events:function(){
			// Contains the event bindings and subscriptions
			var self = this,
				cf = self.config;
			cf.modalTrigger.on('click', function(e){
				e.preventDefault();
				var modalBox = $(this).attr(self.attr_name('modal'));
				console.log($('#' + modalBox));
				$('#' + modalBox).foundation('reveal', 'open');
			});
		}
	};
}(jQuery, window, document));
