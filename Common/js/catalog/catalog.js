;(function ( $, window, document, undefined ) {

	var is_set_new_brand,
	    is_branded
	;

	catalog = {

		mand_text:  '<i class="err">*</i>',

		typeahead: function ( ) {
			var countries = new Bloodhound({
			  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
			  queryTokenizer: Bloodhound.tokenizers.whitespace,
			  limit: 10,
			  prefetch: {
			    // url points to a json file that contains an array of country names, see
			    // https://github.com/twitter/typeahead.js/blob/gh-pages/data/countries.json
			    url: '../js/catalog/business.json',
			    // the json file contains an array of strings, but the Bloodhound
			    // suggestion engine expects JavaScript objects so this converts all of
			    // those strings
			    filter: function(list) {
			      return $.map(list, function(country) { return { name: country }; });
			    }
			  }
			});

			// kicks off the loading/processing of `local` and `prefetch`
			countries.initialize();

			// passing in `null` for the `options` arguments will result in the default
			// options being used
			$('#supplier-input').typeahead(null, {
			  name: 'countries',
			  displayKey: 'name',
			  // `ttAdapter` wraps the suggestion engine in an adapter that
			  // is compatible with the typeahead jQuery plugin
			  source: countries.ttAdapter()
			});
		},


		show_brand_field: function () {
			$('label.brand').html('What is the ' + catalog.product_name + '\'s brand? '
				+ catalog.mand_text);

			if ( $(this).attr('id') === 'is-branded' ) {
				is_branded = true;
			  $('.unbranded,.random-model').fadeOut(200, function ( ) {
			   	$('.select-brand').fadeIn(400);
			  });
			  $('label[for="photos"]>i.err').remove();
			  $('.barcode').show();
			}
			else if ( $(this).attr('id') === 'is-not-branded' ) {
				is_branded = false;
			  $('.select-brand,.model').fadeOut(200, function ( ) {
			   	$('.unbranded,.random-model').fadeIn(400);
			  });
			  $('label[for="photos"]').append(catalog.mand_text);
			  $('.barcode').hide();

			}
			else {
			  return false;
			};
		},

		off_add_new: function ( $select ) {
			var $add_new = $select.parent().next(),
			    text     = $select.chosen().val().toLowerCase()
			;

			if ( text.length ) {
			  $add_new.off('click').children('label')
			  		.css({
			  			'color':'#E7E7E7',
			  			'cursor':'default'
			  		})
			  ;
			}
		},

		show_model_field: function ( ) {
			var $this = $(this),
					text  = $this.chosen().val().toLowerCase()
			;

		  $('.model').fadeIn(400)
		  					 .find('label.model')
		  					 		.html('What ' + text + ' '
		  					 			            + catalog.product_name
		  					 			            + ' model? '
		  					 			            + catalog.mand_text)
		  ;
		},

		show_new_entry_field: function ( ) {
			$(this).parent().fadeOut(200, function ( ) {
				$(this).next().fadeIn(400);
			})
		},

		show_original_field: function ( ) {
			var $this = $(this);
			$this.parent().fadeOut(200, function ( ) {
				$(this).prev().fadeIn(400);
			});

			$this.prev().children('input').val('');

			if ( is_set_new_brand ) {
				$('.random-model').fadeOut(200);
				is_set_new_brand = false;
			};

		},

		set_new_brand: function ( ) {

			is_set_new_brand = true;

			$(this).parent().fadeOut(200, function ( ) {
				$(this).next().fadeIn(400);
			}).next()
					.find("label[for='new-brand']")
						.html('New '
							    + catalog.product_name
							    + ' brand '
							    + catalog.mand_text
							   )
			;

			$('.random-model').fadeIn(400)
												.find('label')
													.html('New '
														     + catalog.product_name
														     + ' model '
														     + catalog.mand_text
														    )
			;
		},

		set_new_model: function ( ) {

			var brand_name = $('#select-brand').chosen().val().toLowerCase();

			$(this).parent().fadeOut(200, function ( ) {
				$(this).next().fadeIn(400);
			}).next()
					.find("label[for='new-model']")
						.html('New '
									+ brand_name
									+ ' ' + catalog.product_name
									+ ' brand '
									+ catalog.mand_text
								 )
			;
		},

		open_file: function(event) {
	    var input         = event.target,
	    		photos_list   = input.files,
	  			i
	  	;

	  	$('.upload-gallery').html('');

	  	for ( i = 0; i < photos_list.length; i++ ) {
	  		var reader    = new FileReader();

	  		reader.onload = function(e) {
		      photo       = new Image();
		      photo.src   = e.target.result;
		      photo.alt   = 'photo'+[i];
		      $('.upload-gallery').append(photo);
		    };

		    reader.readAsDataURL(photos_list[i]);
	  	}
	  },

	  validate_form: function ( e ) {
	  	e.preventDefault();

	  	var $this = $(this);

	  	function pop_error ( $label ) {
	  		$label.children('i.msg').remove();
	  		$label.html($label.html() + ' <i class="err msg" > This is required!</i>');

	  		$this
	  			.parent()
	  				.prev()
	  					.show()
	  					.children('i.err')
	  						.text('Seems you\'re missing something required. Please make sure you fill all required(*) fields!');

	  	}

	  	function clear_error ( $label ) {
	  		$label.children('i.msg').remove();
		  	$this.parent().prev().hide();
	  	}

	  	if(!$('input[name=branded]:checked').length) {
 				pop_error($('label.is-branded'));
 				return false;
			}
			else {
				clear_error($('label.is-branded'));
			}

			if ( is_set_new_brand && !$('#random-model').chosen().val().length ) {
				pop_error( $('label[for="random-model"]'));
				return false;
			}
			else {
				clear_error( $('label[for="random-model"]') );
			};

			if ( !is_branded && !$('#photos-input').val().length) {
			  pop_error( $('label[for="photos-input"]'));
			  return false;
			}
			else {
				clear_error( $('label[for="photos-input"]'));
			};

			if ( !$('#bundle-size').val().length) {
			  pop_error( $('label[for="bundle-size"]'));
			  return false;
			}
			else {
				clear_error( $('label[for="bundle-size"]'));
			};


			$.each( $('.required .catalog-select'), function (  ) {
				var $this     = $(this),
						$required = $this.closest('.required'),
						$newInput = $required.find('.new-cancel>input')
				;

				if ( !$this.chosen().val().length && !$newInput.val().length ) {

					if ( $this.is('#model') && is_set_new_brand ) {
						return true;
					};

					pop_error( $required.find('label[for]'));

					return false;
				}
				else {
			  	clear_error( $required.find('label[for]') );
			  };
			} );

			if ( !$('#price').val().length) {
			  pop_error( $('label[for="price"]'));
			  return false;
			}
			else {
				clear_error( $('label[for="price"]'));
			};

			$( "form" ).submit();
	  },

		init: function (  ) {
			catalog.typeahead();
			$(".catalog-select").chosen() .next() .css('min-width','100%');
			$('.all-details,div.unbranded,div.random-model,div.model,.select-brand,.new-entery,button[type="submit"]').hide();
			$('fieldset').eq(1).hide();
		  $('input[name=branded]').on('click', catalog.show_brand_field);
		  $('.add-new').on('click', catalog.show_new_entry_field);
		  $('.add-new-brand').on('click', catalog.set_new_brand);
		  $('.add-new-model').on('click', catalog.set_new_model);

		  $('.cancel').on('click', catalog.show_original_field);
		  $('#photos-input').on('change', catalog.open_file);
		  $('#select-brand').on('change', catalog.show_model_field);
		  $('select').on('change', function ( ) {
		    catalog.off_add_new($(this));
		  });

		  $('#select-product').on('change', function(evt, params) {
		  	catalog.$select_product = $('#select-product');
		  	catalog.product_name    = catalog.$select_product.chosen().val().toLowerCase();

		  	$('label.is-branded').html('Is the ' + catalog.product_name + ' branded? '
		  	+ catalog.mand_text );
		    $('.all-details').fadeIn(400);
		    $('fieldset').eq(1).fadeIn(400);

		    $('button[type="submit"]')
		    	.fadeIn(400)
		    	.removeAttr('disabled')
		    	.on('click', catalog.validate_form)
		    ;
		  });

		}
	}

})( jQuery, window, document );
//
