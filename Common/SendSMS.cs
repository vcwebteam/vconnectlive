﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vconnect.Models;
using Vconnect.Mapping.Listing;
using Vconnect.Common;
using System.IO;
using System.Text;
using Vconnect.Mapping.ListingWEB;
using Vconnect.Mapping.CommonDb;
using Vconnect.Enums;
using System.Net;

/// <summary>
/// Summary description for SendSMS
/// </summary>
public class SendSMS
{
    public SendSMS()
    {
        //
        // TODO: Add constructor logic here
        //
    }

  //  public int addsmsemaillog();



    public string Sendmessage(string message, string phone)
    {
        string ResponseArray = string.Empty;
        string strQuery;
        HttpWebRequest HttpWReq;
        HttpWebResponse HttpWResp;

        //sendername = sendername.ToUpper();

        if (phone != string.Empty)
        {
            if (phone.StartsWith("0"))
            {
                phone = phone.Substring(1, phone.Length - 1);
            }
            else if (phone.StartsWith("234"))
            {
                phone = phone.Substring(3, phone.Length - 3);
            }
            phone = "234" + phone;

            string text = string.Empty;
            text = HttpUtility.UrlEncode(message);
            List<SMSGateway> gateway= getGatewaySMS(1);
            if (gateway.Count > 0)
            {
                strQuery = gateway[0].url.ToString();
                if (strQuery.Contains("###") && strQuery.Contains("$$$") && strQuery.Contains("***"))
                {
                    strQuery = strQuery.Replace("$$$", text);
                    strQuery = strQuery.Replace("###", phone);
                    strQuery = strQuery.Replace("***", "VConnect");
                }
                HttpWReq = (HttpWebRequest)WebRequest.Create(strQuery);
                HttpWReq.Method = "GET";
                HttpWResp = (HttpWebResponse)HttpWReq.GetResponse();
                System.IO.StreamReader reader = new System.IO.StreamReader(HttpWResp.GetResponseStream());
                ResponseArray = reader.ReadToEnd();
                reader.Close();
                HttpWResp.Close();
            }
        }

        return ResponseArray;


    }

    public List<SMSGateway> getGatewaySMS(int type)
    {
        List<SMSGateway> smsGateway = new List<SMSGateway>();

        using (var db = new VconnectDBContext29())
        {
            try
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_smsgateway]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@type", type));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();

                var reader = cmd.ExecuteReader();
                smsGateway = ((IObjectContextAdapter)db).ObjectContext.Translate<SMSGateway>(reader).ToList() ;

                return smsGateway;
            }
            finally
            {
                db.Database.Connection.Close();
            }
        }
        //return SMSGateway;
        //List<getSMSGateway> SMSGateway = new List<getSMSGateway>();
        
        ////using (SqlCommand _com = new SqlCommand())
        //using (var db = new VconnectDBContext29())
        //{
        //    //using (DataTable dt = new DataTable())
        //    //{
        //    _com.CommandTimeout = 75;
        //    var _con = ((IObjectContextAdapter)db).ObjectContext.Connection;
        //    _com.CommandType = CommandType.StoredProcedure;
        //    _com.CommandText = "prc_get_smsgateway";
        //    _com.Parameters.AddWithValue("@type", type);
        //    _con.Open();
        //    var reader = _com.ExecuteReader();
        //    //SMSGateway = (((IObjectContextAdapter)db).ObjectContext.Translate<getSMSGateway>(reader)).to
        //    //SMSGateway = ((IObjectContextAdapter)db).ObjectContext.Translate<getSMSGateway>(reader)
        //    SMSGateway = (((IObjectContextAdapter)db).ObjectContext.Translate<getSMSGateway>(reader)).;

        //    _con.Close();
        //    _con.Dispose();
        //    return SMSGateway;
        //    //}
        //}

        //using (var db = new VconnectDBContext29())
        //{
        //    try
        //    {
        //        var cmd = db.Database.Connection.CreateCommand();
        //        cmd.CommandText = "[dbo].[prc_get_smsgateway]";
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        //Add parameters to command object
        //        cmd.Parameters.Add(new SqlParameter("@type", type));
        //        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
        //        connection.Open();

        //        var reader = cmd.ExecuteReader();
        //        SMSGateway = (((IObjectContextAdapter)db).ObjectContext.Translate<getSMSGateway>(reader)).ToList();

               
        //    }
        //    finally
        //    {
        //        db.Database.Connection.Close();
        //    }
        //}
        //return SMSGateway;
    }
    public string getdelieverystatus(string msgid)
    {
        string ResponseArray = string.Empty;
        string strQuery;
        HttpWebRequest HttpWReq;
        HttpWebResponse HttpWResp;

        if (msgid != string.Empty)
        {
            strQuery = "http://sms.shreeweb.com/sendsms/dlrstatus.php?username=Chandrika&password=h78j6tmg&messageid=" + msgid + "";
            HttpWReq = (HttpWebRequest)WebRequest.Create(strQuery);
            HttpWReq.Method = "GET";
            HttpWResp = (HttpWebResponse)HttpWReq.GetResponse();
            System.IO.StreamReader reader = new System.IO.StreamReader(HttpWResp.GetResponseStream());
            ResponseArray = reader.ReadToEnd();
            reader.Close();
            HttpWResp.Close();
        }

        return ResponseArray;


    }
    public int addsmsemaillog(int customerid, int usertype, string receiveremail, string receiverphone, string receivername, int messagetype,
          string subject, string message, int businessid, string keyword, string location, string response, string ipaddress, int smslength,
           int noofsms, string pageurl, int actiontype, string reversedns)
    {
        int result = 0;

        using (var db = new VconnectDBContext29())
        {
            var cmd = db.Database.Connection.CreateCommand();
            cmd.CommandText = "[dbo].[prc_add_smslog]";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@customerid", customerid));
            cmd.Parameters.Add(new SqlParameter("@usertype", usertype));
            cmd.Parameters.Add(new SqlParameter("@receiveremail", receiveremail));
            cmd.Parameters.Add(new SqlParameter("@receiverphone", receiverphone.Replace(",", "")));
            cmd.Parameters.Add(new SqlParameter("@receivername", receivername));
            cmd.Parameters.Add(new SqlParameter("@messagetype", messagetype));
            cmd.Parameters.Add(new SqlParameter("@subject", subject));
            cmd.Parameters.Add(new SqlParameter("@message", message));
            cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
            cmd.Parameters.Add(new SqlParameter("@keyword", keyword));
            cmd.Parameters.Add(new SqlParameter("@location", location));
            cmd.Parameters.Add(new SqlParameter("@response", response));
            cmd.Parameters.Add(new SqlParameter("@ipaddress", ipaddress));
            cmd.Parameters.Add(new SqlParameter("@smslength", smslength));
            cmd.Parameters.Add(new SqlParameter("@noofsms", noofsms));
            cmd.Parameters.Add(new SqlParameter("@pageurl", pageurl));
            cmd.Parameters.Add(new SqlParameter("@actiontype", actiontype));
            cmd.Parameters.Add(new SqlParameter("@createdby", customerid));
            cmd.Parameters.Add(new SqlParameter("@reversedns", reversedns));
            //cmd.Parameters.Add(new SqlParameter("@Err", SqlDbType.Int).Direction = ParameterDirection.Output);

            cmd.Parameters.Add(new SqlParameter("@Err", SqlDbType.Int));
            cmd.Parameters["@Err"].Direction = ParameterDirection.Output;
            var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
            connection.Open();
            cmd.ExecuteNonQuery();
            db.Database.Connection.Close();
            result = Convert.ToInt32(cmd.Parameters["@Err"].Value.ToString());
        }
        return result;
    }

}
