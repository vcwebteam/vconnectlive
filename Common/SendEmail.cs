﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Vconnect.Models;
using MailChimp;
using MailChimp.Types;
namespace Vconnect.Common
{
    public class SendEmail
    {
        public void BOMails(Mandrill.Messages.Message mandrill)     //Mandrill function to send emails to businesses
        {
            
            //var api = new MandrillApi("ApBQP7Yg9KtIzqSyrYqKVQ");
            var api = new MandrillApi(ConfigurationManager.AppSettings["MandrillBOmails"].ToString().Trim());
            api.Send(mandrill);
        }
        public void VCUsermails(Mandrill.Messages.Message mandrill)     //Mandrill function to send emails to users
        {
            
            //var api = new MandrillApi("vpVOLp43NC4br3a0WwvOdg");
            var api = new MandrillApi(ConfigurationManager.AppSettings["MandrillVCUsermails"].ToString().Trim());
            api.Send(mandrill);
        }
        //public void BOmails(Mandrill.Messages.Message mandrill) //Mandrill function to send emails to businesses
        //{
        //    var api = new MandrillApi(ConfigurationManager.AppSettings["MandrillBOmails"].ToString().Trim());
        //    //var api = new MandrillApi("ApBQP7Yg9KtIzqSyrYqKVQ");
        //    api.Send(mandrill);            
        //}
        //public void VCUsermails(Mandrill.Messages.Message mandrill) //Mandrill function to send emails to users
        //{
        //    var api = new MandrillApi(ConfigurationManager.AppSettings["MandrillVCUsermails"].ToString().Trim());
        //    //var api = new MandrillApi("ApBQP7Yg9KtIzqSyrYqKVQ");
        //   api.Send(mandrill);            
        //}
        public void SendMailToUser(ref MailMessage Email, string UserID, string Password)
        {
            SmtpClient sc = new SmtpClient(ConfigurationManager.AppSettings["Emailserver"].ToString().Trim());
            sc.UseDefaultCredentials = false;
            sc.Credentials = new System.Net.NetworkCredential(UserID, Password);
            sc.EnableSsl = false; // Please SET IT FALSE in case of NON-GMAIL account            
            sc.Port = 587;
            //sc.Timeout = 2000;
            sc.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            sc.Send(Email);
        }
        public void SendMailToUser2(ref MailMessage Email, string UserID, string Password)
        {
            SmtpClient sc = new SmtpClient("smtp.gmail.com");
            sc.Credentials = new System.Net.NetworkCredential(UserID, Password);
            sc.EnableSsl = true; // Please SET IT FALSE in case of NON-GMAIL account           
            sc.Port = 587;
            //sc.Timeout = 2000;
            sc.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            sc.Send(Email);
        }
    }
       
}