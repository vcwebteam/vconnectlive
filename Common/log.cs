﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Data.Entity.Infrastructure;
using Vconnect.Models;

namespace Vconnect.Common
{
      
    public static class log
    {
       
        public static string addsearchpagelog(string searchkeyword, string searchlocation, int businessid, string businessname,Int32 userid, string ipaddress, string pageurl, string previousurl, string searchType, string userType, string Domain, string Sitetype, string IPLocation, string TotalRecords, string sourcename, string reversedns, int wlimit,int islead)
        {
             using (var db = new VconnectDBContext29())
            {
                string result = string.Empty;
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandTimeout = 120;
                var _con = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "prc_add_searchpagelog";
                cmd.Parameters.Add(new SqlParameter("@searchkeyword", searchkeyword));
                cmd.Parameters.Add(new SqlParameter("@searchlocation", searchlocation));
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                cmd.Parameters.Add(new SqlParameter("@businessname", businessname));
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@ipaddress", ipaddress));
                cmd.Parameters.Add(new SqlParameter("@pageurl", pageurl));
                cmd.Parameters.Add(new SqlParameter("@previousurl", previousurl));
                cmd.Parameters.Add(new SqlParameter("@searchType", searchType));
                cmd.Parameters.Add(new SqlParameter("@userType", userType));
                cmd.Parameters.Add(new SqlParameter("@Domain", Domain));
                cmd.Parameters.Add(new SqlParameter("@Source", Sitetype));
                cmd.Parameters.Add(new SqlParameter("@Sourcename", sourcename));
                cmd.Parameters.Add(new SqlParameter("@IPLocation", IPLocation));
                cmd.Parameters.Add(new SqlParameter("@TotalRecords", TotalRecords));
                cmd.Parameters.Add(new SqlParameter("@reversedns", reversedns));
                cmd.Parameters.Add(new SqlParameter("@wlimit", wlimit));
                cmd.Parameters.Add(new SqlParameter("@islead", islead));
                 
                //SqlParameter Err = new SqlParameter("@Err", SqlDbType.Int);
                //Err.Direction = ParameterDirection.Output;
                //cmd.Parameters.Add(Err);
                //cmd.Parameters.Add(new SqlParameter("@Err",ParameterDirection.Output));
                //cmd.Parameters.Add(new SqlParameter("@newcontentid", ParameterDirection.Output));
                //cmd.Parameters.Add(new SqlParameter("@outexit",ParameterDirection.Output));
                //cmd.Parameters.Add(new SqlParameter("@warning",ParameterDirection.Output));
                //cmd.Parameters.Add(new SqlParameter("@outipaddress", ParameterDirection.Output));
                //cmd.Parameters.Add(new SqlParameter("@outsourcename",  ParameterDirection.Output));
                //cmd.Parameters.Add(new SqlParameter("@outsearchlocation", ParameterDirection.Output));
                //cmd.Parameters.Add(new SqlParameter("@outIPLocation",ParameterDirection.Output));
              try
              {
                 _con.Open();
                cmd.ExecuteNonQuery();
                // result = Convert.ToString(cmd.Parameters["@Err"].Value);
                _con.Close(); _con.Dispose();
                  }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                    //ex.Message.ToString();
                }

                finally
                {

                    db.Database.Connection.Close();
                }

                return "1";
            }
        }
        public static int addsearchnotfoundlog(string searchkeyword, string searchlocation, string ipaddress, string searchkeywordtype, int createdby, string reversedns)
        {
              using (var db = new VconnectDBContext29())
            {

           
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandTimeout = 120;
                var _con = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "prc_add_searchnotfoundlog";

                cmd.Parameters.Add(new SqlParameter("@searchkeyword", searchkeyword));
                cmd.Parameters.Add(new SqlParameter("@searchlocation", searchlocation));
                cmd.Parameters.Add(new SqlParameter("@ipaddress", ipaddress));
                cmd.Parameters.Add(new SqlParameter("@searchkeywordtype", searchkeywordtype));
                cmd.Parameters.Add(new SqlParameter("@createdby", createdby));
                cmd.Parameters.Add(new SqlParameter("@reversedns", reversedns));
                cmd.Parameters.Add(new SqlParameter("@source", "WEB"));
                // _con.Open();
                int result = 1;
                _con.Open();
                cmd.ExecuteNonQuery();
                _con.Close(); _con.Dispose();
                return result;
            }
        }

        public static int addphoneclicklog(string phone, string businessid, int userid, string ipaddress, string sourceurl, string previousurl)
        {
            int result=0;

            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_phoneclicklog]";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 600;
                cmd.Parameters.Add(new SqlParameter("@phone", phone));
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@ipaddress", ipaddress));
                cmd.Parameters.Add(new SqlParameter("@sourceurl", sourceurl));
                cmd.Parameters.Add(new SqlParameter("@previousurl", previousurl));
                //SqlParameter err = new SqlParameter("@Err", SqlDbType.Int);
                //err.Direction = ParameterDirection.Output;
                //cmd.Parameters.Add(err);


                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteReader();
                //result = (int)cmd.Parameters["@Err"].Value;
            }
            return result;

        }
        public static int AddwebsiteLog(int userId, string ipAddress, string previousUrl, string pageUrl, string pageType, string domain, string reverseDns)
        {
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandTimeout = 120;
                var _con = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "prc_add_websitevisitlog_beta";
                cmd.Parameters.Add(new SqlParameter("@userid", userId));
                cmd.Parameters.Add(new SqlParameter("@ipaddress", ipAddress));
                cmd.Parameters.Add(new SqlParameter("@previousurl", previousUrl));
                cmd.Parameters.Add(new SqlParameter("@pageurl", pageUrl));
                cmd.Parameters.Add(new SqlParameter("@pageType", pageType));
                cmd.Parameters.Add(new SqlParameter("@Domain", domain));
                cmd.Parameters.Add(new SqlParameter("@reversedns", reverseDns));
                //SqlParameter Err = new SqlParameter("@Err", SqlDbType.Int);
                //Err.Direction = ParameterDirection.Output;
                //cmd.Parameters.Add(Err);
                int result = 0;
                try
                {
                    _con.Open();
                    cmd.ExecuteNonQuery();
                    //result = (int)cmd.Parameters["@Err"].Value;
                    _con.Close(); _con.Dispose();
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }

                finally
                {
                    db.Database.Connection.Close();
                }
                return result;
            }
        }
        public static int addbusinessvisitorlog(int businessid, string businessname, int userid, string ipaddress, string pageurl, string previousurl, int pagetype, string reversedns)
        {

            using (var db = new VconnectDBContext29())
            {
                //ipaddress = "0";
                //reversedns = "0";
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandTimeout = 120;
                var _con = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[prc_add_businessvisitorlog_beta]";
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                cmd.Parameters.Add(new SqlParameter("@businessname", businessname));
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@ipaddress", ipaddress));
                cmd.Parameters.Add(new SqlParameter("@pageurl", pageurl));
                cmd.Parameters.Add(new SqlParameter("@previousurl", previousurl));
                cmd.Parameters.Add(new SqlParameter("@pagetype", pagetype));
                cmd.Parameters.Add(new SqlParameter("@reversedns", reversedns));
                //cmd.Parameters.Add(new SqlParameter("@Err", ParameterDirection.Output));
                // cmd.Parameters.Add(new SqlParameter("@ReturnCount", ParameterDirection.Output));

                _con.Open();
                cmd.ExecuteNonQuery();
                //int result = (int)cmd.parameters["@err"].value;

                //_con.close(); _con.dispose();

                //return result;
                return 1;
            }
        }

        public static int prioritybizlog(int businessid, string businessname, int prioityspot, string searchkeyword, string searchlocation, string ipaddress, string reversedns, int userid, string sourceurl, string previousurl)
        {
          
            using (var db = new VconnectDBContext29())
            {
                // ipaddress = "0";
                //reversedns = "0";
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandTimeout = 120;
                var _con = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[prc_insert_bizpriorityimpression]";
                cmd.Parameters.Add(new SqlParameter("@BUSINESSID", businessid));
                cmd.Parameters.Add(new SqlParameter("@BUSINESSNAME", businessname));
                cmd.Parameters.Add(new SqlParameter("@PRIORITYSPOT", prioityspot));
                cmd.Parameters.Add(new SqlParameter("@SEARCHWORD", searchkeyword));
                cmd.Parameters.Add(new SqlParameter("@SEARCHLOCATION", searchlocation));
                cmd.Parameters.Add(new SqlParameter("@IPADDRESS", ipaddress));
                cmd.Parameters.Add(new SqlParameter("@REVERSEDNS", reversedns));
                cmd.Parameters.Add(new SqlParameter("@SOURCE", "Web Search"));
                cmd.Parameters.Add(new SqlParameter("@sourceurl", sourceurl));
                cmd.Parameters.Add(new SqlParameter("@previousurl", previousurl));
                cmd.Parameters.Add(new SqlParameter("@USERID", userid));
                //cmd.Parameters.Add(new SqlParameter("@Err", ParameterDirection.Output));
                // cmd.Parameters.Add(new SqlParameter("@ReturnCount", ParameterDirection.Output));

                _con.Open();
                cmd.ExecuteNonQuery();
                //int result = (int)cmd.Parameters["@Err"].Value;

                _con.Close(); _con.Dispose();

                return 0;
            }
           
        }
        public static void NewLog(Exception ex)
        {
            string _statusCode, _errMsg, _errStack, _url, _referer, _browser, _os, _ip, _userid;
            string visitorIPAddress = string.Empty;
            try
            {
                visitorIPAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (string.IsNullOrEmpty(visitorIPAddress))
                {
                    visitorIPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

                }

                if (string.IsNullOrEmpty(visitorIPAddress))
                {
                    visitorIPAddress = HttpContext.Current.Request.UserHostAddress;

                }

                if (string.IsNullOrEmpty(visitorIPAddress))
                {
                    // GetLan = true;
                    visitorIPAddress = "0";

                }
            }
            catch (Exception ex1)
            {
               // log.LogMe(ex);
                visitorIPAddress = "0";
            }


            UserSession objUS = new UserSession();
            Int32 ContentId;
            string _username;
            if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                ContentId = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
            else
                ContentId = 0;
            if (!string.IsNullOrEmpty(objUS.SV_VCUserName))
                _username = objUS.SV_VCUserName.ToString();
            else
                _username = "";

          
            _statusCode = HttpContext.Current.Response.Status;
            _errMsg = ex.Message;
            _errStack = ex.StackTrace;
            _url = HttpContext.Current.Request.RawUrl;
            _referer = HttpContext.Current.Request.ServerVariables["HTTP_REFERER"];
            _browser = HttpContext.Current.Request.Browser.Browser + " " + HttpContext.Current.Request.Browser.Version;
            _os = HttpContext.Current.Request.Browser.Platform;
            _ip = visitorIPAddress;

            using (var db = new VconnectDBContext29())
            {

                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandTimeout = 120;
                var _con = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Usp_AddApplicationLog";
                cmd.Parameters.Add(new SqlParameter("@AppHttpStatus", _statusCode));
                cmd.Parameters.Add(new SqlParameter("@AppErrorMsg", _errMsg));
                cmd.Parameters.Add(new SqlParameter("@AppUrl", _url));
                cmd.Parameters.Add(new SqlParameter("@AppErrorStack", _errStack));
                cmd.Parameters.Add(new SqlParameter("@AppBrowser", _browser));
                cmd.Parameters.Add(new SqlParameter("@AppReferer", _referer));
                cmd.Parameters.Add(new SqlParameter("@AppOS", _os));
                cmd.Parameters.Add(new SqlParameter("@AppIP", _ip));
                cmd.Parameters.Add(new SqlParameter("@userid",ContentId));
                cmd.Parameters.Add(new SqlParameter("@username", _username));

                _con.Open();
                cmd.ExecuteNonQuery();
                

                _con.Close(); _con.Dispose();

                
            }
           
        }

        
        public static void LogMe(Exception exGenerated)
        {
            try
            {
                if (!exGenerated.Message.Equals("Thread was being aborted."))
                {
                    NewLog(exGenerated);
                }
            }
            catch
            {
                // code left empty intentionally due to recursive in nature as well as not necessary to tackle such errors.
                // It will left suppress the errors.
            }
        }

        public static int addbannerimpressionlog(string bannerids, int businessid, string businessname, int userid, string ipaddress, string pageurl, string previousurl, string reversedns)
        {
            using (var db = new VconnectDBContext29())
            {

                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandTimeout = 120;
                var _con = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "prc_add_bannerimpressionlog";
                cmd.Parameters.Add(new SqlParameter("@bannerids", bannerids));
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                cmd.Parameters.Add(new SqlParameter("@businessname", businessname));
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@ipaddress", ipaddress));
                cmd.Parameters.Add(new SqlParameter("@pageurl", pageurl));
                cmd.Parameters.Add(new SqlParameter("@previousurl", previousurl));
                cmd.Parameters.Add(new SqlParameter("@reversedns", reversedns));
                //SqlParameter Err = new SqlParameter("@Err", SqlDbType.Int);
                //Err.Direction = ParameterDirection.Output;
                //cmd.Parameters.Add(Err);
                int result = 0;
                try
                {
                    _con.Open();
                    cmd.ExecuteNonQuery();
                    //result = (int)cmd.Parameters["@Err"].Value;
                    _con.Close(); _con.Dispose();
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                    //ex.Message.ToString();
                }

                finally
                {

                    db.Database.Connection.Close();
                }
                return result;
            }

        }

        public static int addbannerclicklog(int bannerid, int businessid, string businessname, int userid, string ipaddress, string pageurl, string previousurl)
        {
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandTimeout = 120;
                var _con = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "prc_add_bannerclicklog";
                cmd.Parameters.Add(new SqlParameter("@bannerid", bannerid));
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                cmd.Parameters.Add(new SqlParameter("@businessname", businessname));
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@ipaddress", ipaddress));
                cmd.Parameters.Add(new SqlParameter("@pageurl", pageurl));
                cmd.Parameters.Add(new SqlParameter("@previousurl", previousurl));
                int result = 0;
                try
                {
                    _con.Open();
                    cmd.ExecuteNonQuery();
                    _con.Close(); _con.Dispose();
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                    //ex.Message.ToString();
                }

                finally
                {

                    db.Database.Connection.Close();
                }
                return result;
            }
        }
        
    }
}