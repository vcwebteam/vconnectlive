﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vconnect.Common
{
    public static class Messages
    {
        //Created By Nishant Dated - 17jan2014 / please keep the Access modifiers/Naming Conventions the same way for the proposed sections 
        #region Declarations
            private static string result;
        #endregion

            //An example to help you 

        #region DummyMessages

            //Dictionary Defination with private and static scopes so that it can be accessed without creating objects
            private static Dictionary<string, string> _dictDummyMessages = new Dictionary<string, string>
            {
                {"entry", "entries"},
                {"image", "images"},
                {"view", "views"},
                {"file", "files"},
                {"result", "results"},
                {"word", "words"},
                {"definition", "definitions"},
                {"item", "items"},
                {"megabyte", "megabytes"},
                {"game", "games"} 
            };
    
            //require to get you result for example Messages.GetDummyMessages("game"); 
            public static string GetDummyMessages(string key)
            {
                // Try to get the result in the static Dictionary
                if (_dictDummyMessages.TryGetValue(key, out result))
                {
                    return result;
                }
                else
                {
                    return null;
                }
            }

        #endregion

    }
}   