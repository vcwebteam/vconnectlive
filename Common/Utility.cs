﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Vconnect.Models;
using System.Data.Entity.Infrastructure;
using System.Security.Cryptography;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.Entity;
using System.Web.Mvc;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Threading;
using System.Web.UI;
using System.Xml.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web.Security;
using System.Collections.Specialized;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Vconnect.Common
{
    public static class Utility
    {
        public static string userdetail(int userid, string column)
        {
            string activity = "0";

            using (var db = new VconnectDBContext29())
            {
                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "select top 1 " + column + " from userdetails where contentid=" + userid;
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    // reader.NextResult();
                    activity = ((IObjectContextAdapter)db).ObjectContext.Translate<string>(reader).FirstOrDefault();
                    return activity;


                }
                catch (Exception ex)
                {
                    // log.LogMe(ex);
                    return activity;

                }

            }
        }
        private static CloudBlobContainer imagesBlobContainer;
        public static async Task<string> cloudUploadImages(HttpPostedFileBase uploadFile, string imgName, string path = "images")
        {
            #region CloudImageTransfer
            //------------------- 
            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["AzureWebJobsStorage"].ToString());
            var blobClient = storageAccount.CreateCloudBlobClient();
            imagesBlobContainer = blobClient.GetContainerReference(path);//webconfigpath
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
            //--------------------
            CloudBlockBlob imageBlob = null;
            // imageBlob = await UploadAndSaveBlobAsync(uploadFile, imgName);
            #endregion
            imageBlob = imagesBlobContainer.GetBlockBlobReference(imgName);
            // Create the blob by uploading a local file.
            using (var fileStream = uploadFile.InputStream)
            {
                await imageBlob.UploadFromStreamAsync(fileStream);
            }
            return "";
        }

        public static void cloudimageupload(byte[] imagebyte, string imageName, string imagelocation)
        {
            byte[] bytes = imagebyte;
            MemoryStream ms = new MemoryStream(bytes);

            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["AzureWebJobsStorage"].ToString());
            var blobClient = storageAccount.CreateCloudBlobClient();
            imagesBlobContainer = blobClient.GetContainerReference(imagelocation);

            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
            CloudBlockBlob imageBlob = null;

            string blobName = imageName;
            // Retrieve reference to a blob. 
            imageBlob = imagesBlobContainer.GetBlockBlobReference(blobName);
            //imageBlob = imagesBlobContainer.GetBlockBlobReference("images");

            imageBlob.UploadFromStream(ms);
            Trace.TraceInformation("Uploaded image file to {0}", imageBlob.Uri.ToString());
            // return imageBlob.Uri.ToString();
        }
        public static async Task<string> ByteCloudUploadImages(byte[] imagebyte, string imgName, string path = "images")
        {
            MemoryStream ms = new MemoryStream(imagebyte);

            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["AzureWebJobsStorage"].ToString());
            var blobClient = storageAccount.CreateCloudBlobClient();
            imagesBlobContainer = blobClient.GetContainerReference(path.Trim());

            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
            CloudBlockBlob imageBlob = null;

            imageBlob = imagesBlobContainer.GetBlockBlobReference(imgName);

            imageBlob.UploadFromStream(ms);


            return "";
        }

        public static UserSession objUS = new UserSession();
        //public static void CreateHttpCookie(string cookieName, string cookieValue, string key)
        //{
        //    HttpCookie objCookie;
        //    if (HttpContext.Current.Request.Cookies[cookieName] != null)
        //    {
        //        objCookie = new HttpCookie(cookieName);
        //        if (!HttpContext.Current.Request.Url.AbsoluteUri.Contains("localhost") && !HttpContext.Current.Request.Url.AbsoluteUri.Contains("mtest.vconnect.com"))
        //        {
        //            objCookie.Domain = "vconnect.com";
        //            objCookie.Path = "/";
        //        }
        //        objCookie.Expires = DateTime.Now.AddDays(-1);
        //        HttpContext.Current.Response.Cookies.Add(objCookie);
        //    }

        //    if (cookieValue != null)    //handling 'delete cookies' by passing null.
        //    {
        //        if (cookieValue.Trim() == string.Empty)
        //            return;
        //        else
        //        {
        //            objCookie = new HttpCookie(cookieName, cookieValue);
        //        }
        //        if (!HttpContext.Current.Request.Url.AbsoluteUri.Contains("localhost") && !HttpContext.Current.Request.Url.AbsoluteUri.Contains("mtest.vconnect.com"))
        //        {
        //            objCookie.Domain = "vconnect.com";
        //            objCookie.Path = "/";
        //        }
        //        objCookie.Expires = DateTime.Now.AddDays(3);
        //        HttpContext.Current.Response.Cookies.Add(objCookie);
        //    }

        //}
        public static string RateMeHome(object rating)
        {
            decimal rate1 = 0.0m;
            try
            {
                rate1 = Convert.ToDecimal(rating);
            }
            catch
            {
                rate1 = 0;
            }
            string rate = "";
            for (int i = 1; i <= 5; i++)
            {
                if (i <= rate1)
                {
                    rate = rate + "<i class=\"icon-star\"></i>";
                }
            }
            return rate;
        }
        public static HttpCookie vcBizRate = new HttpCookie("vcBizRate");
        public static string createCookieBiz(Int32 bizid, Int16 rate)
        {
            string result = string.Empty;
            if (HttpContext.Current.Request.Cookies["vcBizRate"] == null)
            {
                System.Collections.Specialized.NameValueCollection cookieBiz = new System.Collections.Specialized.NameValueCollection();
                cookieBiz.Add(bizid.ToString(), rate.ToString());
                vcBizRate.Values.Add(cookieBiz);
                vcBizRate.Expires = DateTime.Now.AddMonths(3);
                HttpContext.Current.Response.Cookies.Add(vcBizRate);
                result = "added";
            }
            HttpCookie cookieBizList = HttpContext.Current.Request.Cookies["vcBizRate"];
            bool isBizRateExists = false;
            if (cookieBizList.HasKeys)
            {
                for (int i = 0; i < cookieBizList.Values.Count; i++)
                {
                    System.Collections.Specialized.NameValueCollection item = ((System.Collections.Specialized.NameValueCollection)(cookieBizList.Values));
                    if (Convert.ToInt32(item.AllKeys[i]) == bizid)
                    {
                        isBizRateExists = true;
                        result = "already";
                    }
                }
            }
            if (isBizRateExists == false)
            {
                System.Collections.Specialized.NameValueCollection cookieBiz = new System.Collections.Specialized.NameValueCollection();
                cookieBiz.Add(bizid.ToString(), rate.ToString());
                vcBizRate.Values.Add(cookieBiz);
                vcBizRate.Expires = DateTime.Now.AddMonths(3);
                HttpContext.Current.Response.Cookies.Add(vcBizRate);
                result = "added";
            }

            return result;
        }

        public static HttpCookie vcBizPhoto = new HttpCookie("vcBizPhoto");
        public static void createCookieBizPhoto(Int32 bizid,string name)
        {
            //string result = string.Empty;
            //if (HttpContext.Current.Request.Cookies["vcBizPhoto"] == null)
            //{
            //    System.Collections.Specialized.NameValueCollection cookieBizPhoto = new System.Collections.Specialized.NameValueCollection();
            //    cookieBizPhoto.Add(bizid.ToString(), name.ToString());
            //    vcBizPhoto.Values.Add(cookieBizPhoto);
            //    vcBizPhoto.Expires = DateTime.Now.AddMonths(3);
            //    HttpContext.Current.Response.Cookies.Add(vcBizPhoto);
            //   // result = "added";
            //}
            //HttpCookie cookieBizList = HttpContext.Current.Request.Cookies["vcBizPhoto"];
            ////bool isBizPhotoExists = false;
            //List<bizImageObj> bizImgObj = new List<bizImageObj>();

            //if (cookieBizList.HasKeys)
            //{
            //    for (int i = 0; i < cookieBizList.Values.Count; i++)
            //    {
            //        System.Collections.Specialized.NameValueCollection item = ((System.Collections.Specialized.NameValueCollection)(cookieBizList.Values));
            //        //if (Convert.ToInt32(item.AllKeys[i]) == bizid)
            //        //{
            //            bizImageObj ObjbizImg = new bizImageObj();
            //            ObjbizImg.businessid = Convert.ToInt32(item.AllKeys[i]);
                        
            //            //isBizPhotoExists = true;
            //            result = "already"
            //        //}
            //    }
            //}
            //if (isBizPhotoExists == false)
            //{
                System.Collections.Specialized.NameValueCollection cookieBizPhoto = new System.Collections.Specialized.NameValueCollection();
                cookieBizPhoto.Add(bizid.ToString(), name.ToString());
                vcBizPhoto.Values.Add(cookieBizPhoto);
                vcBizPhoto.Expires = DateTime.Now.AddMonths(3);
                HttpContext.Current.Response.Cookies.Add(vcBizPhoto);
               // result = "added";
            //}

           // return result;
        }
        public static void CreateCokkies(string selectedLocid, string selectedLocation, string searchText, string searchLocation)
        {
            if (HttpContext.Current.Request.Cookies.AllKeys.Contains("vcSelectedLocId") && HttpContext.Current.Request.Cookies["vcSelectedLocId"] != null)
            {
                HttpContext.Current.Request.Cookies.Remove("vcSelectedLocId");
            }
            HttpCookie vcSelectedLocId = new HttpCookie("vcSelectedLocId");
            vcSelectedLocId.Value = string.IsNullOrEmpty(selectedLocid) ? "125" : selectedLocid;
            vcSelectedLocId.Expires = DateTime.Now.AddMonths(3);
            HttpContext.Current.Response.Cookies.Add(vcSelectedLocId);
            if (HttpContext.Current.Request.Cookies.AllKeys.Contains("vcSelectedLocation") && HttpContext.Current.Request.Cookies["vcSelectedLocation"] != null)
            {
                HttpContext.Current.Request.Cookies.Remove("vcSelectedLocation");
            }
            HttpCookie vcSelectedLocation = new HttpCookie("vcSelectedLocation");
            vcSelectedLocation.Value = string.IsNullOrEmpty(selectedLocation) ? "Lagos" : selectedLocation;
            vcSelectedLocation.Expires = DateTime.Now.AddMonths(3);
            HttpContext.Current.Response.Cookies.Add(vcSelectedLocation);
            if (HttpContext.Current.Request.Cookies.AllKeys.Contains("vcSearchText") && HttpContext.Current.Request.Cookies["vcSearchText"] != null)
            {
                HttpContext.Current.Request.Cookies.Remove("vcSearchText");
            }
            HttpCookie vcSearchText = new HttpCookie("vcSearchText");
            vcSearchText.Value = searchText;
            vcSearchText.Expires = DateTime.Now.AddDays(1);
            HttpContext.Current.Response.Cookies.Add(vcSearchText);
            if (HttpContext.Current.Request.Cookies.AllKeys.Contains("vcSearchLocation") && HttpContext.Current.Request.Cookies["vcSearchLocation"] != null)
            {
                HttpContext.Current.Request.Cookies.Remove("vcSearchLocation");
            }
            HttpCookie vcSearchLocation = new HttpCookie("vcSearchLocation");
            vcSearchLocation.Value = string.IsNullOrEmpty(searchLocation) ? "Lagos" : searchLocation;
            vcSearchLocation.Expires = DateTime.Now.AddMonths(3);
            HttpContext.Current.Response.Cookies.Add(vcSearchLocation);
        }
        public static string ToTitleCase(string mText)
        {
            string rText = "";
            try
            {
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                TextInfo TextInfo = cultureInfo.TextInfo;
                rText = TextInfo.ToTitleCase(mText.ToLower());
            }
            catch
            {
                rText = mText;
            }
            return rText;
        }

        public static string RateMe(object rating)
        {
            decimal rate1 = 0.0m;
            decimal rem = 0.0m;

            try
            {
                rate1 = Convert.ToDecimal(rating);
                if (!string.IsNullOrEmpty(rating.ToString().Substring(1)))
                {
                    rem = Convert.ToDecimal(rating.ToString().Substring(1));
                }
            }
            catch
            {
                rate1 = 0;
            }
            string rate = "";
            for (int i = 1; i <= 5; i++)
            {
                if (i <= rate1)
                {
                    //rate = rate + "<span class=\"pro_star\"><img src=\"/images/WEB/rating/filledstar.png\"   alt='star' width='16px'  height='15px'></img></span>";
                    rate = rate + "<i class=\"icon-star\"></i>";
                }
                else if (rem > 0 && rem < 1)
                {
                    if (rem == 0.5m)
                    {
                        //rate = rate + "<span class=\"pro_star\"><img src=\"/images/WEB/rating/half-star.png\"  alt='star' width='16px'  height='15px'></img></span>";
                        rate = rate + "<i class=\"icon-star-half\"></i>";
                        rem = 0.0m;
                    }
                    else if (rem > 0.5m)
                    {
                        //rate = rate + "<span class=\"pro_star\"><img src=\"/images/WEB/rating/more-half-star.png\"  alt='star' width='16px'  height='15px'></img></span>";
                        rate = rate + "<i class=\"icon-star-half\"></i>";
                        rem = 0.0m;
                    }
                    else
                    {
                        //rate = rate + "<span class=\"pro_star\"><img src=\"/images/WEB/rating/less-half-star.png\" alt='star' width='16px'  height='15px'></img></span>";                        
                        rate = rate + "<i class=\"icon-star-half\"></i>";
                        rem = 0.0m;
                    }
                }
                else
                {
                    //rate = rate + "<span class=\"pro_star\"><img src=\"/images/WEB/rating/emptystar.png\" alt='star' width='16px'  height='15px' ></img></span>";                    
                    rate = rate + "<i class=\"icon-star-empty\"></i>";
                }
            }
            return rate;
        }
        //only mobile numbers 
        public static int isValidPhonemobile(string phone)
        {
            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandTimeout = 75;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "prc_get_isphonemobile";
                cmd.Parameters.Add(new SqlParameter("@phone", phone));
                var _con = ((IObjectContextAdapter)db).ObjectContext.Connection;
                _con.Open();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                    result = int.Parse(reader[0].ToString().Trim());
                _con.Close();
                _con.Dispose();
            }
            return result;
        }
        //For Mobile validation
        public static int isValidPhone(string phone)
        {
            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandTimeout = 75;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "prc_get_isvalidphone";
                cmd.Parameters.Add(new SqlParameter("@phone", phone));
                var _con = ((IObjectContextAdapter)db).ObjectContext.Connection;
                _con.Open();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                    result = int.Parse(reader[0].ToString().Trim());
                _con.Close();
                _con.Dispose();
            }
            return result;
        }
        //For Checking phone no. alerady present or not
        public static int businessphonechk(string phone, int userid)
        {
            int result = 0;

            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_wap_get_bussinessphone1]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@phone", phone));
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@errorcode", SqlDbType.VarChar, 10));
                cmd.Parameters["@errorcode"].Direction = ParameterDirection.Output;
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();
                db.Database.Connection.Close();
                result = Convert.ToInt32(cmd.Parameters["@errorcode"].Value.ToString());
            }
            return result;
        }
        //Abhishek[27thDec13] for generating alphanumeric random number
        public static string GetAlphaNumericRandomNumber()
        {
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            Random random = new Random();
            string result = new string(Enumerable.Repeat(chars, 4).Select(s => s[random.Next(s.Length)]).ToArray());
            return result;
        }
        public static string GetNumericRandomNumber()
        {
            string chars = "0123456789";
            Random random = new Random();
            string result = new string(Enumerable.Repeat(chars, 4).Select(s => s[random.Next(s.Length)]).ToArray());
            return result;
        }
        public static int SaveSMSEmailLog(int customerid, int usertype, string receiveremail, string receiverphone, string receivername, int messagetype,
           string subject, string message, int businessid, string keyword, string location, string response, string ipaddress, int smslength,
            int noofsms, string pageurl, int actiontype, string reversedns, string source)
        {
            using (var db = new VconnectDBContext29())
            {

                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandTimeout = 120;
                var _con = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "prc_add_smslog";
                cmd.Parameters.Add(new SqlParameter("@customerid", customerid));
                cmd.Parameters.Add(new SqlParameter("@usertype", usertype));
                cmd.Parameters.Add(new SqlParameter("@receiveremail", receiveremail));
                cmd.Parameters.Add(new SqlParameter("@receiverphone", receiverphone.Replace(",", "")));
                cmd.Parameters.Add(new SqlParameter("@receivername", receivername));
                cmd.Parameters.Add(new SqlParameter("@messagetype", messagetype));
                cmd.Parameters.Add(new SqlParameter("@subject", subject));
                cmd.Parameters.Add(new SqlParameter("@message", message));
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                cmd.Parameters.Add(new SqlParameter("@keyword", keyword));
                cmd.Parameters.Add(new SqlParameter("@location", location));
                cmd.Parameters.Add(new SqlParameter("@response", response));
                cmd.Parameters.Add(new SqlParameter("@ipaddress", ipaddress));
                cmd.Parameters.Add(new SqlParameter("@smslength", smslength));
                cmd.Parameters.Add(new SqlParameter("@noofsms", noofsms));
                cmd.Parameters.Add(new SqlParameter("@pageurl", pageurl));
                cmd.Parameters.Add(new SqlParameter("@actiontype", actiontype));
                cmd.Parameters.Add(new SqlParameter("@createdby", customerid));
                cmd.Parameters.Add(new SqlParameter("@reversedns", reversedns));
                cmd.Parameters.Add(new SqlParameter("@source", source));
                cmd.Parameters.Add(new SqlParameter("@Err", ParameterDirection.Output));

                _con.Open();
                cmd.ExecuteNonQuery();
                int result = (int)cmd.Parameters["@Err"].Value;
                _con.Close(); _con.Dispose();
                return result;
            }
        }
        public static int SaveSMSEmailLog_lead(int customerid, int usertype, string receiveremail, string receiverphone, string receivername, int messagetype,
        string subject, string message, int businessid, string keyword, string location, string response, string ipaddress, int smslength,
         int noofsms, string pageurl, int actiontype, string reversedns, string source, int leadid)
        {
            using (var db = new VconnectDBContext29())
            {

                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandTimeout = 120;
                var _con = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "prc_add_smslog";
                cmd.Parameters.Add(new SqlParameter("@customerid", customerid));
                cmd.Parameters.Add(new SqlParameter("@usertype", usertype));
                cmd.Parameters.Add(new SqlParameter("@receiveremail", receiveremail));
                cmd.Parameters.Add(new SqlParameter("@receiverphone", receiverphone.Replace(",", "")));
                cmd.Parameters.Add(new SqlParameter("@receivername", receivername));
                cmd.Parameters.Add(new SqlParameter("@messagetype", messagetype));
                cmd.Parameters.Add(new SqlParameter("@subject", subject));
                cmd.Parameters.Add(new SqlParameter("@message", message));
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                cmd.Parameters.Add(new SqlParameter("@keyword", keyword));
                cmd.Parameters.Add(new SqlParameter("@location", location));
                cmd.Parameters.Add(new SqlParameter("@response", response));
                cmd.Parameters.Add(new SqlParameter("@ipaddress", ipaddress));
                cmd.Parameters.Add(new SqlParameter("@smslength", smslength));
                cmd.Parameters.Add(new SqlParameter("@noofsms", noofsms));
                cmd.Parameters.Add(new SqlParameter("@pageurl", pageurl));
                cmd.Parameters.Add(new SqlParameter("@actiontype", actiontype));
                cmd.Parameters.Add(new SqlParameter("@createdby", customerid));
                cmd.Parameters.Add(new SqlParameter("@reversedns", reversedns));
                cmd.Parameters.Add(new SqlParameter("@source", source));
                cmd.Parameters.Add(new SqlParameter("@leadid", leadid));
                cmd.Parameters.Add(new SqlParameter("@Err", ParameterDirection.Output));

                _con.Open();
                cmd.ExecuteNonQuery();
                int result = (int)cmd.Parameters["@Err"].Value;
                _con.Close(); _con.Dispose();
                return result;
            }
        }
        public static string GetReversedns(string visitorIPAddress)
        {
            try
            {
                return System.Net.Dns.GetHostEntry(visitorIPAddress).HostName.ToString();
            }
            catch
            {
                return "0";
            }
        }
        public static string GetIpAddress()
        {
            
            //string visitorIPAddress = "0";
            //try
            //{
            //  visitorIPAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            //    if (string.IsNullOrEmpty(visitorIPAddress))
            //    {
            //        visitorIPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

            //    }

            //    if (string.IsNullOrEmpty(visitorIPAddress))
            //    {
            //        visitorIPAddress = HttpContext.Current.Request.UserHostAddress;

            //    }

            //    if (string.IsNullOrEmpty(visitorIPAddress))
            //    {
            //        // GetLan = true;
            //        visitorIPAddress = "0";

            //    }
            //}
            //catch(Exception ex)
            //{
            //    log.LogMe(ex);
            //    visitorIPAddress = "0";
            //}
            bool GetLan = false;
            string visitorIPAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrEmpty(visitorIPAddress))
                visitorIPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

            if (string.IsNullOrEmpty(visitorIPAddress))
                visitorIPAddress = HttpContext.Current.Request.UserHostAddress;

            if (string.IsNullOrEmpty(visitorIPAddress) || visitorIPAddress.Trim() == "::1")
            {
                GetLan = true;
                visitorIPAddress = string.Empty;
            }

            if (GetLan)
            {
                if (string.IsNullOrEmpty(visitorIPAddress))
                {
                    //This is for Local(LAN) Connected ID Address
                    string stringHostName = Dns.GetHostName();
                    //Get Ip Host Entry
                    IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
                    //Get Ip Address From The Ip Host Entry Address List
                    IPAddress[] arrIpAddress = ipHostEntries.AddressList;

                    try
                    {
                        //visitorIPAddress = arrIpAddress[arrIpAddress.Length - 2].ToString();
                        foreach (var address in arrIpAddress)
                        {
                            if (address.AddressFamily == AddressFamily.InterNetwork)
                                visitorIPAddress = address.ToString();
                        }
                    }
                    catch
                    {
                        try
                        {
                            visitorIPAddress = arrIpAddress[0].ToString();
                        }
                        catch
                        {
                            try
                            {
                                arrIpAddress = Dns.GetHostAddresses(stringHostName);
                                visitorIPAddress = arrIpAddress[0].ToString();
                            }
                            catch
                            {
                                visitorIPAddress = "127.0.0.1";
                            }
                        }
                    }
                }
            }
            MyGlobalVariables.GetIpAddress = visitorIPAddress;
            return visitorIPAddress;
            //return visitorIPAddress;

        }

        public static string GetIpAddressNew()
        {
            bool GetLan = false;
            string visitorIPAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrEmpty(visitorIPAddress))
                visitorIPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

            if (string.IsNullOrEmpty(visitorIPAddress))
                visitorIPAddress = HttpContext.Current.Request.UserHostAddress;

            if (string.IsNullOrEmpty(visitorIPAddress) || visitorIPAddress.Trim() == "::1")
            {
                GetLan = true;
                visitorIPAddress = string.Empty;
            }

            if (GetLan)
            {
                if (string.IsNullOrEmpty(visitorIPAddress))
                {
                    //This is for Local(LAN) Connected ID Address
                    string stringHostName = Dns.GetHostName();
                    //Get Ip Host Entry
                    IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
                    //Get Ip Address From The Ip Host Entry Address List
                    IPAddress[] arrIpAddress = ipHostEntries.AddressList;

                    try
                    {
                        visitorIPAddress = arrIpAddress[arrIpAddress.Length - 2].ToString();
                    }
                    catch
                    {
                        try
                        {
                            visitorIPAddress = arrIpAddress[0].ToString();
                        }
                        catch
                        {
                            try
                            {
                                arrIpAddress = Dns.GetHostAddresses(stringHostName);
                                visitorIPAddress = arrIpAddress[0].ToString();
                            }
                            catch
                            {
                                visitorIPAddress = "127.0.0.1";
                            }
                        }
                    }
                }
            }
            MyGlobalVariables.GetIpAddress = visitorIPAddress;
            return visitorIPAddress;

        }

        public static string GetDomain()
        {
            string domain = "";
            try
            {
                domain = HttpContext.Current.Request.Url.Host.ToString();
            }

            catch
            {
                domain = "0";

            }
            return domain;
        }

        public static SelectList GetWorkingHours()
        {
            var listWorkingHours = new[] 
                { 
                new Time  { timeID = "0.00",time = "0.00"},
                new Time  { timeID = "0.30",time = "0.30"},
                new Time  { timeID = "1.00",time ="1.00" },
                new Time  { timeID = "1.30",time = "1.30"},
                new Time  { timeID = "2.00",time = "2.00"},
                new Time  { timeID = "2.30",time = "2.30"},
                new Time  { timeID = "3.00",time = "3.00"},
                new Time  { timeID = "3.30",time = "3.30"},
                new Time  { timeID = "4.00",time = "4.00"},
                new Time  { timeID = "4.30",time = "4.30"},
                new Time  { timeID = "5.00",time = "5.00"},
                new Time  { timeID = "5.30", time = "5.30"},
                new Time  { timeID = "6.00",time = "6.00"},
                new Time  { timeID = "6.30",time = "6.30"},
                new Time  { timeID = "7.00",time = "7.00"},
                new Time  { timeID = "7.30",time = "7.30"},
                new Time  { timeID = "8.00",time = "8.00"},
                new Time  { timeID = "8.30",time = "8.30"},
                new Time  { timeID = "9.00",time = "9.00"},
                new Time  { timeID = "9.30",time = "9.30"},
                new Time  { timeID = "10.00",time ="10.00"},
                new Time  { timeID = "10.30",time = "10.30"},
                new Time  { timeID = "11.00",time = "11.00"},
                new Time  { timeID = "11.30",time = "11.30"},
                new Time  { timeID = "12.00",time ="12.00"},
                new Time  { timeID = "12.30",time ="12.30"},
                new Time  { timeID = "13.00",time = "13.00"},
                new Time  { timeID = "13.30",time = "13.30"},
                new Time  { timeID = "14.00",time = "14.00"},
                new Time  { timeID = "14.30", time = "14.30"},
                new Time  { timeID = "15.00",time = "15.00"},
                new Time  { timeID = "15.30",time = "15.30"},
                new Time  { timeID = "16.00",time = "16.00"},
                new Time  { timeID = "16.30",time = "16.30"},
                new Time  { timeID = "17.00",time = "17.00"},
                new Time  { timeID = "17.30",time = "17.30"},
                new Time  { timeID = "18.00",time = "18.00"},
                new Time  { timeID = "18.30" ,time ="18.30"},
                new Time  { timeID = "19.00",time = "19.00"},
                new Time  { timeID = "19.30",time = "19.30"},
                new Time  { timeID = "20.00",time = "20.00"},
                new Time  { timeID = "20.30",time = "20.30"},
                new Time  { timeID = "21.00",time = "21.00"},
                new Time  { timeID = "21.30",time = "21.30"},
                new Time  { timeID = "22.00",time = "22.00"},
                new Time  { timeID = "22.30",time = "22.30"},
                new Time  { timeID = "23.00",time = "23.00"},
                new Time  { timeID = "23.30",time = "23.30"},
                new Time  { timeID = "Closed",time ="Closed"},
                };
            return new SelectList(listWorkingHours, "timeID", "time");
        }
        public static SelectList GetSelectList()
        {
            string selectedid = "125";
            var list = new[] 
                { 
                new StatesDropDown  { StateId = 101, StateName = "Abia"  },
                new StatesDropDown  { StateId = 102 ,StateName = "Adamawa"},
                new StatesDropDown  { StateId = 103 ,StateName = "Akwa Ibom" },
                new StatesDropDown  { StateId = 104 , StateName = "Anambra"},
                new StatesDropDown  { StateId = 105,StateName = "Abuja" },
                new StatesDropDown  { StateId = 106,StateName = "Bayelsa" },
                new StatesDropDown  { StateId = 107,StateName = "Benue" },
                new StatesDropDown  { StateId = 108,StateName = "Borno" },
                new StatesDropDown  { StateId = 109,StateName = "Bauchi" },
                new StatesDropDown  { StateId = 110,StateName = "Cross River", },
                new StatesDropDown  { StateId = 111,StateName = "Delta" },
                new StatesDropDown  { StateId = 112, StateName = "Ebonyi",  },
                new StatesDropDown  { StateId = 113 ,StateName = "Edo",},
                new StatesDropDown  { StateId = 114 ,StateName = "Ekiti"},
                new StatesDropDown  { StateId = 115,StateName = "Enugu"},
                new StatesDropDown  { StateId = 116,StateName = "Gombe"  },
                new StatesDropDown  { StateId = 117,StateName = "Imo" },
                new StatesDropDown  { StateId = 118 ,StateName = "Jigawa"},
                new StatesDropDown  { StateId = 119,StateName = "Kaduna" },
                new StatesDropDown  { StateId = 120,StateName = "Kano", },
                new StatesDropDown  { StateId = 121 ,StateName = "Katsina"},
                new StatesDropDown  { StateId = 122 ,StateName = "Kebbi"},
                new StatesDropDown  { StateId = 123 , StateName = "Kogi" },
                new StatesDropDown  { StateId = 124, StateName = "Kwara" },
                new StatesDropDown  { StateId = 125 ,StateName = "Lagos"},
                new StatesDropDown  { StateId = 126 ,StateName = "Nasarawa"},
                new StatesDropDown  { StateId = 127, StateName = "Niger" },
                new StatesDropDown  { StateId = 128,StateName = "Ogun" },
                new StatesDropDown  { StateId = 129,StateName = "Ondo" },
                new StatesDropDown  { StateId = 130 ,StateName = "Osun" },
                new StatesDropDown  { StateId = 131,StateName = "Oyo" },
                new StatesDropDown  { StateId = 132,StateName = "Plateau", },
                new StatesDropDown  { StateId = 133,StateName = "Rivers" },
                new StatesDropDown  { StateId = 134 ,StateName = "Sokoto"},
                new StatesDropDown  { StateId = 135 ,StateName = "Taraba"},
                new StatesDropDown  { StateId = 136, StateName = "Yobe" },
                new StatesDropDown  { StateId = 137,StateName = "Zamfara" }
                };


            if ((HttpContext.Current.Response.Cookies.AllKeys.Contains("vcSelectedLocId") || HttpContext.Current.Request.Cookies.AllKeys.Contains("vcSelectedLocId")) && HttpContext.Current.Request.Cookies["vcSelectedLocId"].Value != "")
            {
                selectedid = HttpContext.Current.Request.Cookies["vcSelectedLocId"].Value.ToString();
            }

            return new SelectList(list, "StateId", "StateName", Convert.ToInt32(selectedid));
        }
        public static SelectList GetSelectListCity(int stateid)
        {
            List<City> listCityName = new List<City>();
            using (var db = new VconnectDBContext29())
            {
                string sdr = ((System.Guid.NewGuid().ToString().GetHashCode().ToString("x"))).ToString();
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_StateCity]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@stateid", stateid));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();

                    listCityName = ((IObjectContextAdapter)db).ObjectContext.Translate<City>(reader).ToList();
                    connection.Close();
                    connection.Dispose();
                }
                catch (System.Exception e)
                {
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            List<City> cityDropDown = new List<City>();

            foreach (var item in listCityName)
            {
                City cityDropdownList =
                                  new City { contentid = item.contentid, cityname = item.cityname };
                cityDropDown.Add(cityDropdownList);
            }
            return new SelectList(cityDropDown, "contentid", "cityname");
        }

        //Area location Bind
        public static SelectList GetSelectListArea(int cityid, int stateid)
        {
            List<Area> listAreaName = new List<Area>();
            using (var db = new VconnectDBContext29())
            {
                string sdr = ((System.Guid.NewGuid().ToString().GetHashCode().ToString("x"))).ToString();
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_areaname]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@cityid", cityid));
                cmd.Parameters.Add(new SqlParameter("@stateid", stateid));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();

                    listAreaName = ((IObjectContextAdapter)db).ObjectContext.Translate<Area>(reader).ToList();
                    connection.Close();
                    connection.Dispose();
                }
                catch (System.Exception e)
                {
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            List<Area> cityDropDown = new List<Area>();

            foreach (var item in listAreaName)
            {
                Area cityDropdownList =
                                  new Area { contentid = item.contentid, areaname = item.areaname };
                cityDropDown.Add(cityDropdownList);
            }
            return new SelectList(cityDropDown, "contentid", "areaname");
        }
        //for userregistration for wap

        public static class Encrypt
        {
            public static string encryptPassword(string password)
            {
                MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();
                byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(password.Trim()));
                StringBuilder strEncrptedPassword = new StringBuilder();
                // Loop through each byte of the hashed data and format each one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    strEncrptedPassword.Append(data[i].ToString("x2"));
                }
                return strEncrptedPassword.ToString();
            }
        }
        public class CaptchaImageResult : ActionResult
        {
            public string GetCaptchaString(int length)
            {
                int intZero = '0';
                int intNine = '9';
                int intA = 'A';
                int intZ = 'Z';
                int intCount = 0;
                int intRandomNumber = 0;
                string strCaptchaString = "";

                Random random = new Random(System.DateTime.Now.Millisecond);

                while (intCount < length)
                {
                    intRandomNumber = random.Next(intZero, intZ);
                    if (((intRandomNumber >= intZero) && (intRandomNumber <= intNine) || (intRandomNumber >= intA) && (intRandomNumber <= intZ)))
                    {
                        strCaptchaString = strCaptchaString + (char)intRandomNumber;
                        intCount = intCount + 1;
                    }
                }
                return strCaptchaString;
            }

            public override void ExecuteResult(ControllerContext context)
            {
                Bitmap bmp = new Bitmap(90, 30);
                Graphics g = Graphics.FromImage(bmp);
                g.Clear(Color.WhiteSmoke);
                string randomString = GetCaptchaString(5);
                context.HttpContext.Session["captchastring"] = randomString;
                g.DrawString(randomString, new Font("High perturbation", 15), new SolidBrush(Color.Gray), 10, 2);
                HttpResponseBase response = context.HttpContext.Response;
                response.ContentType = "image/jpeg";
                bmp.Save(response.OutputStream, ImageFormat.Jpeg);
                bmp.Dispose();
            }
            //CaptchaImg ci = new CaptchaImg(CaptchaImage.ToString(), 200, 50, "High perturbation");


        }

        public static string StringReplace(string strtext)
        {
            strtext = strtext.Replace("?", "-");
            strtext = strtext.Replace("<", "-");
            strtext = strtext.Replace(">", "-");
            strtext = strtext.Replace("[", "-");
            strtext = strtext.Replace("]", "-");
            strtext = strtext.Replace("+", "-");
            strtext = strtext.Replace("~", "-");
            strtext = strtext.Replace("!", "-");
            strtext = strtext.Replace("@", "-");
            strtext = strtext.Replace("#", "-");
            strtext = strtext.Replace("$", "-");
            strtext = strtext.Replace("%", "-");
            strtext = strtext.Replace("^", "-");
            strtext = strtext.Replace("=", "-");
            strtext = strtext.Replace("*", "-");
            strtext = strtext.Replace("(", "-");
            strtext = strtext.Replace(")", "-");
            strtext = strtext.Replace("{", "-");
            strtext = strtext.Replace("}", "-");
            strtext = strtext.Replace("|", "-");
            strtext = strtext.Replace("'", "-");
            strtext = strtext.Replace(":", "-");
            strtext = strtext.Replace(".", "-");
            strtext = strtext.Replace(",", "-");
            strtext = strtext.Replace(";", "-");
            strtext = strtext.Replace("\\", "-");
            strtext = strtext.Trim().Replace("&", "and").Replace("/", "-").Replace("\"", "-").Replace(' ', '_');
            strtext = strtext.Replace("---", "-");
            strtext = strtext.Replace("--", "-");
            return strtext;

        }

        public static class CopyImage
        {

            public static void Sync(string sourcePath, string destinationPath)
            {
                //bool dirExisted = DirExists(destinationPath);

                //get the source files

                string[] srcFiles = Directory.GetFiles(sourcePath);

                Console.WriteLine(srcFiles.Length + " Files Selected ");

                foreach (string sourceFile in srcFiles)
                {
                    FileInfo sourceInfo = new FileInfo(sourceFile);

                    if (sourceInfo.CreationTime.Date >= DateTime.Now.AddDays(-1).Date)
                    {
                        string destFile = Path.Combine(destinationPath, sourceInfo.Name);

                        if (File.Exists(destFile))
                        {
                            FileInfo destInfo = new FileInfo(destFile);
                            DateTime lastdate = destInfo.CreationTime;

                            if (sourceInfo.LastWriteTime > destInfo.LastWriteTime)
                            {
                                //file is newer, so copy it

                                try
                                {
                                    File.Copy(sourceFile, Path.Combine(destinationPath, sourceInfo.Name), true);
                                }
                                catch (Exception ex)
                                {
                                    ex = null;
                                }

                                //Console.WriteLine(sourceFile + " copied (newer version)");

                            }

                        }

                        else
                        {
                            try
                            {

                                File.Copy(sourceFile, Path.Combine(destinationPath, sourceInfo.Name));
                            }
                            catch (Exception ex)
                            {
                                ex = null;
                                //AppLogs.LogError(ex);
                            }

                            //Console.WriteLine(sourceFile + " copied");

                        }
                    }

                }//foreach ends



                //  DeleteOldDestinationFiles(srcFiles, destinationPath);

                //now process the directories if exist

                string[] dirs = Directory.GetDirectories(sourcePath);

                foreach (string dir in dirs)
                {
                    DirectoryInfo dirInfo = new DirectoryInfo(dir);
                    //recursive do the directories
                    Sync(dir, Path.Combine(destinationPath, dirInfo.Name));

                }

            }//

            private static bool DirExists(string path)
            {

                //create destination directory if not exist

                if (!Directory.Exists(path))
                {

                    Directory.CreateDirectory(path);

                    return true;

                }

                else
                {

                    return false;

                }

            }

            private static void DeleteOldDestinationFiles(string[] sourceFiles, string destinationPath)
            {

                //get the destination files

                string[] dstFiles = Directory.GetFiles(destinationPath);



                foreach (string dstFile in dstFiles)
                {

                    FileInfo f = new FileInfo(dstFile);



                    string[] found = Array.FindAll(sourceFiles, str => GetName(str).Equals(f.Name));

                    if (found.Length == 0)
                    {

                        //delete file if not found in destination

                        File.Delete(dstFile);



                    }

                }

            }

            private static string GetName(string path)
            {

                FileInfo i = new FileInfo(path);

                return i.Name;

            }


        }//class ends
        public static string RateMeSearchListing(object rating)
        {
            decimal rate1 = 0.0m;
            decimal rem = 0.0m;

            try
            {
                rate1 = Convert.ToDecimal(rating);
                if (!string.IsNullOrEmpty(rating.ToString().Substring(1)))
                {
                    rem = Convert.ToDecimal(rating.ToString().Substring(1));
                }
            }
            catch
            {
                rate1 = 0;
            }
            string rate = "";
            for (int i = 1; i <= 5; i++)
            {
                if (i <= rate1)
                {
                    //rate = rate + "<span class=\"pro_star\"><img src=\"/images/WEB/rating/filledstar.png\"   alt='star' width='16px'  height='15px'></img></span>";
                    rate = rate + "<li><i class=\"rating-star\"></i></li>";
                }
                else if (rem > 0 && rem < 1)
                {
                    if (rem == 0.5m)
                    {
                        //rate = rate + "<span class=\"pro_star\"><img src=\"/images/WEB/rating/half-star.png\"  alt='star' width='16px'  height='15px'></img></span>";
                        rate = rate + "<li><i class=\"rating-star-half\"></i></li>";
                        rem = 0.0m;
                    }
                    else if (rem > 0.5m)
                    {
                        //rate = rate + "<span class=\"pro_star\"><img src=\"/images/WEB/rating/more-half-star.png\"  alt='star' width='16px'  height='15px'></img></span>";
                        rate = rate + "<li><i class=\"rating-star-half\"></i></li>";
                        rem = 0.0m;
                    }
                    else
                    {
                        //rate = rate + "<span class=\"pro_star\"><img src=\"/images/WEB/rating/less-half-star.png\" alt='star' width='16px'  height='15px'></img></span>";                        
                        rate = rate + "<li><i class=\"rating-star-half\"></i></li>";
                        rem = 0.0m;
                    }
                }
                else
                {
                    //rate = rate + "<span class=\"pro_star\"><img src=\"/images/WEB/rating/emptystar.png\" alt='star' width='16px'  height='15px' ></img></span>";                    
                    rate = rate + "<li><i class=\"rating-star-empty\"></i></li>";
                }
            }
            return rate;
        }
        public class ImageHandler
        {

            //        private int MinSize = 1;           //in bytes -- 1 byte
            private Int64 MaxSize = 5 * 1024 * 1024;   //in bytes -- 1MB

            private int thumbnailWidth = 0;    //optional
            private int thumbnailHeight = 0;
            private string thumbnailPrefix = "Thumb_";

            private HttpPostedFile gfile;
            private HttpPostedFileBase gfileBase;
            private string foldername = "";
            private string physicalfolderpath = "";

            private string originalFilename = "";
            private string newFilename = "";
            private string thumbnailFilename = "";
            public ImageHandler()
            {

            }
            public ImageHandler(HttpPostedFile file, string foldername, string sThumbFileName, string sLargeFileName)
            {
                gfile = file;
                this.foldername = foldername;
                CreateFolder(foldername, sThumbFileName, sLargeFileName);
            }

            public ImageHandler(HttpPostedFileBase file, string foldername, string sThumbFileName, string sLargeFileName)
            {
                gfileBase = file;
                this.foldername = foldername;
                CreateFolder(foldername, sThumbFileName, sLargeFileName);
            }

            public ImageHandler(HttpPostedFile file, string foldername, Int64 MaxFileSize, string sThumbFileName, string sLargeFileName)
            {
                gfile = file;
                MaxSize = MaxFileSize;
                CreateFolder(foldername, sThumbFileName, sLargeFileName);
            }

            public bool CreateFolder(string foldername, string sThumbFileName, string sLargeFileName)
            {

                try
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath(foldername)))
                    {
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("") + "\\" + foldername);
                    }
                    physicalfolderpath = HttpContext.Current.Server.MapPath(foldername);
                    if (IsImage(gfile))
                    {
                        originalFilename = gfile.FileName.Substring(gfile.FileName.LastIndexOf('\\') + 1);
                        newFilename = getRandomAlphaNumericFilename(physicalfolderpath, originalFilename);
                        //thumbnailFilename = thumbnailPrefix + newFilename;
                        thumbnailFilename = sThumbFileName;
                    }
                    else if (IsImage(gfileBase))
                    {
                        originalFilename = gfileBase.FileName.Substring(gfileBase.FileName.LastIndexOf('\\') + 1);
                        newFilename = getRandomAlphaNumericFilename(physicalfolderpath, originalFilename);
                        //thumbnailFilename = thumbnailPrefix + newFilename;
                        thumbnailFilename = sThumbFileName;

                    }
                }
                catch
                {
                    return false;
                }

                return true;
            }


            public int ThumbnailWidth
            {
                get
                {
                    return thumbnailWidth;
                }
                set
                {
                    thumbnailWidth = value;
                }
            }


            public int ThumbnailHeight
            {
                get
                {
                    return thumbnailHeight;
                }
                set
                {
                    thumbnailHeight = value;
                }
            }

            public string ThumbnailPrefix
            {
                get
                {
                    return thumbnailPrefix;
                }
                set
                {
                    thumbnailPrefix = value;
                }
            }


            public string NewFilename
            {
                get
                {
                    return newFilename;
                }
            }

            public string ThumbnailFilename
            {
                get
                {
                    return thumbnailFilename;
                }
            }



            private bool IsImage(HttpPostedFile file)
            {
                if (file != null && Regex.IsMatch(file.ContentType, "image/\\S+") && file.ContentLength > 0 && file.ContentLength <= MaxSize)
                    return true;

                return false;
            }

            private bool IsImage(HttpPostedFileBase file)
            {
                if (file != null && Regex.IsMatch(file.ContentType, "image/\\S+") && file.ContentLength > 0 && file.ContentLength <= MaxSize)
                    return true;

                return false;
            }


            public bool UploadImage()
            {
                if (!IsImage(gfile) && (!IsImage(gfileBase)))
                    return false;


                try
                {
                    string filePath = HttpContext.Current.Server.MapPath(foldername + "/" + newFilename);
                    if (gfile != null)
                    {
                        gfile.SaveAs(filePath);
                    }
                    else if (gfileBase != null)
                    {
                        gfileBase.SaveAs(filePath);
                    }
                }
                catch (Exception ex)
                {
                    string er = ex.Message;
                    return false;
                }

                return true;
            }



            public bool ResizeImage()
            {
                if (!IsImage(gfile))
                    return false;

                using (Bitmap orgbitmap = new Bitmap(gfile.InputStream, false))
                {
                    try
                    {
                        int orgWidth = orgbitmap.Width;
                        int orgHeight = orgbitmap.Height;

                        int newWidth = 0;
                        int newHeight = 0;

                        if (orgHeight == 0 || orgWidth == 0)
                            return false;




                        if ((orgWidth > thumbnailWidth) || (orgHeight > thumbnailHeight))
                        {

                            float widthRatio = 0F;
                            float heightRatio = 0F;
                            float newratio = 0F;

                            widthRatio = (float)orgWidth / (float)thumbnailWidth;
                            heightRatio = (float)orgHeight / (float)thumbnailHeight;

                            if (widthRatio > heightRatio)
                                newratio = (float)widthRatio;
                            else
                                newratio = (float)heightRatio;

                            newWidth = Convert.ToInt32(orgWidth / newratio);
                            newHeight = Convert.ToInt32(orgHeight / newratio);
                        }
                        else
                        {
                            newHeight = orgHeight;
                            newWidth = orgWidth;
                        }

                        using (System.Drawing.Image image = System.Drawing.Image.FromStream(gfile.InputStream))
                        using (Bitmap bitmap = new Bitmap(image, newWidth, newHeight))
                        {

                            bitmap.Save(HttpContext.Current.Server.MapPath(foldername + "/" + thumbnailFilename), image.RawFormat);
                            return true;
                        }
                    }
                    catch (Exception exp)
                    {
                        string ErrorMessage = exp.Message;
                        return false;
                    }
                }


            }

            public bool ResizeImage(int MaxWidth, int MaxHeight, string FileName, string NewFileName)
            {
                // load up the image, figure out a "best fit" resize, and then save that new image
                Bitmap OriginalBmp = (System.Drawing.Bitmap)System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(FileName)).Clone();
                Size ResizedDimensions = GetDimensions(MaxWidth, MaxHeight, ref OriginalBmp);
                Bitmap NewBmp = new Bitmap(OriginalBmp, ResizedDimensions);
                System.Drawing.Imaging.ImageFormat imageFormat = OriginalBmp.RawFormat;
                NewBmp.Save(HttpContext.Current.Server.MapPath(NewFileName), imageFormat);
                return true;

            }
            public static Size GetDimensions(int MaxWidth, int MaxHeight, ref Bitmap Bmp)
            {
                int Width;
                int Height;
                float Multiplier;

                Height = Bmp.Height;
                Width = Bmp.Width;

                // this means you want to shrink an image that is already shrunken!
                if (Height <= MaxHeight && Width <= MaxWidth)
                    return new Size(Width, Height);

                // check to see if we can shrink it width first
                Multiplier = (float)((float)MaxWidth / (float)Width);
                if ((Height * Multiplier) <= MaxHeight)
                {
                    Height = (int)(Height * Multiplier);
                    return new Size(MaxWidth, Height);
                }

                // if we can't get our max width, then use the max height
                Multiplier = (float)MaxHeight / (float)Height;
                Width = (int)(Width * Multiplier);
                return new Size(Width, MaxHeight);
            }


            private string GetRdm()
            {
                RandomNumberGenerator rm;
                rm = RandomNumberGenerator.Create();

                byte[] data = new byte[1];

                rm.GetNonZeroBytes(data);

                int nVal = Convert.ToInt32(data.GetValue(0));
                if ((nVal >= 48 && nVal <= 57) || (nVal >= 65 && nVal <= 90) || (nVal >= 97 && nVal <= 122))
                {
                    return Convert.ToChar(nVal).ToString();
                }
                else
                {
                    return GetRdm();
                }
            }


            private string getRandomAlphaNumericFilename(string PhysicalFilePath, string Filename)
            {
                int count = 0;
                string strRdm = "";
                string filetype = "";

                int n1 = Filename.LastIndexOf(".");
                if (n1 > 0)
                    filetype = Filename.Substring(n1);
                else
                    return Filename;

                while (count < 16)
                {
                    count++;
                    strRdm += GetRdm().ToString();
                }

                strRdm += filetype;

                if (Directory.Exists(PhysicalFilePath + "\\" + strRdm))
                    return getRandomAlphaNumericFilename(PhysicalFilePath, Filename);
                else
                    return strRdm;
            }

        }

        public class ImageResizing
        {

            // private Int64 MaxSize = 2 * 1024 * 1024;   //in bytes -- 5MB

            private int thumbnailWidth = 0;    //optional
            private int thumbnailHeight = 0;
            private string filename = "";
            private string foldername = "";



            public ImageResizing()
            {
                //
                // TODO: Add constructor logic here
                //
            }


            public int ThumbnailWidth
            {
                get
                {
                    return thumbnailWidth;
                }
                set
                {
                    thumbnailWidth = value;
                }
            }


            public int ThumbnailHeight
            {
                get
                {
                    return thumbnailHeight;
                }
                set
                {
                    thumbnailHeight = value;
                }
            }

            public string FolderName
            {
                get
                {
                    return foldername;
                }
                set
                {
                    foldername = value;
                }

            }
            public string NewFileName
            {
                get
                {
                    return filename;

                }
                set
                {
                    filename = value;
                }

            }


            public bool ResizeImage(string path)
            {
                if (path != "")
                {
                    System.Drawing.Image img = System.Drawing.Image.FromFile(path);

                    using (Bitmap orgbitmap = new Bitmap(img))
                    {
                        try
                        {
                            int orgWidth = orgbitmap.Width;
                            int orgHeight = orgbitmap.Height;

                            int newWidth = 0;
                            int newHeight = 0;

                            if (orgHeight == 0 || orgWidth == 0)
                                return false;




                            if ((orgWidth > thumbnailWidth) || (orgHeight > thumbnailHeight))
                            {

                                float widthRatio = 0F;
                                float heightRatio = 0F;
                                float newratio = 0F;

                                widthRatio = (float)orgWidth / (float)thumbnailWidth;
                                heightRatio = (float)orgHeight / (float)thumbnailHeight;

                                if (widthRatio > heightRatio)
                                    newratio = (float)widthRatio;
                                else
                                    newratio = (float)heightRatio;

                                newWidth = Convert.ToInt32(orgWidth / newratio);
                                newHeight = Convert.ToInt32(orgHeight / newratio);
                            }
                            else
                            {
                                newHeight = orgHeight;
                                newWidth = orgWidth;
                            }

                            //using (System.Drawing.Image image = System.Drawing.Image.FromStream(gfile.InputStream))
                            using (Bitmap bitmap = new Bitmap(img, newWidth, newHeight))
                            {
                                bitmap.Save(HttpContext.Current.Server.MapPath(foldername + "/" + filename), img.RawFormat);

                                return true;
                            }
                        }
                        catch
                        {
                            return false;
                        }
                    }

                }
                else
                {
                    return false;
                }
            }




        }

        public static void LoadMetaDetail(string BusinessID, string BusinessName, string BusinessCategory, string location, out string title, out string description, out string keyword, out string robots, out string canonical, string BusinessServices, out string ogdescription, out string imagealt, string bizcity, string BusinessState, string fulladdress, string BusinessProducts,string businesshouseno,string businesslandmark,string businessstreetname,string category, string service, string product)
        {
            
            robots = "index, follow";
            imagealt = string.Empty;
            ogdescription = string.Empty;
            canonical = string.Empty;
            keyword = string.Empty;
            description = string.Empty;
            //string category = "";            
            category = !string.IsNullOrEmpty(BusinessCategory) ? BusinessCategory.IndexOf(",") > 0 ? BusinessCategory.Substring(0, BusinessCategory.IndexOf(",")) : BusinessCategory : "";
            var xlext = ReadXML("business", BusinessID);
            if (xlext.Count() > 0)
            {
                title = "";
                description = "";
                keyword = "";
                robots = "";
                canonical = "";

                foreach (var meta in xlext)
                {
                    title = meta.Element("title").Value;
                    description = meta.Element("description").Value;
                    keyword = meta.Element("keywords").Value;
                    robots = meta.Element("robots").Value;
                    canonical = meta.Element("canonical").Value;

                    if (!string.IsNullOrEmpty(meta.Element("imagealt").Value))
                    {
                        imagealt = meta.Element("imagealt").Value;
                    }
                    else
                    {
                        imagealt = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in " + bizcity + ", " + BusinessState;
                    }


                }
            }

            else
            {
                
                //<Business Name> in <Address> deals in < service 1>, <service 2> OR <cat1> ,<cat2>. 
                //if (BusinessServices.ToString() != "")
                //{
                //    ogdescription = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(fulladdress.ToLower().Replace("  ", " ")).Trim() + " deals in " + BusinessServices + ". Get office/email address, contact tele phone number and their website details at Vconnect™ local business directory of Nigeria.";
                //    description = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(fulladdress.ToLower().Replace("  ", " ")).Trim() + " deals in " + BusinessServices + ". Get office/email address, contact tele phone number and their website details at Vconnect local business directory of Nigeria.";
                //}
                //else if (BusinessCategory.ToString() != "")
                //{
                //    ogdescription = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(fulladdress.ToLower().Replace("  ", " ")).Trim() + " deals in " + BusinessCategory + ". Get office/email address, contact tele phone number and their website details at Vconnect™ local business directory of Nigeria.";
                //    description = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(fulladdress.ToLower().Replace("  ", " ")).Trim() + " deals in " + BusinessCategory + ". Get office/email address, contact tele phone number and their website details at Vconnect™ local business directory of Nigeria.";
                //}
                //else
                //{
                //    ogdescription = "'Get the address, contact information and other details about " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in Nigeria on VConnect™ – the trusted local business directory of Nigeria.";
                //    description= BusinessName.ToString() +" provider of " + BusinessCategory.ToString() +" deals in "+ BusinessCategory.ToString() +","+ BusinessProducts.ToString() + " .Get office/email address, contact tele phone number, reviews & location on map and their website details at VConnect™.";
                //   // description = "Get the address, contact information and other details about " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(fulladdress.ToLower().Replace("  ", " ")) + " on VConnect™ – the trusted local business directory of Nigeria.";
                //}


                // changes done by divya on 5th june 2014(chhatrapal request)
                // changes done by devinder on 15th Dec 2014 (chhatrapal request)
                if (!string.IsNullOrEmpty(BusinessProducts.ToString()) && !string.IsNullOrEmpty(BusinessServices.ToString()))
                {
                    if (BusinessServices.ToString().Trim().IndexOf(',') > 0)
                    {
                        string[] servs = BusinessServices.ToString().Trim().Split(',');
                        service = servs[0].Trim();
                    }
                    else
                    {
                        service = BusinessServices.ToString().Trim();
                    }
                    if (BusinessProducts.ToString().Trim().IndexOf(',') > 0)
                    {
                        string[] prodts = BusinessProducts.ToString().Trim().Split(',');
                        product = prodts[0].Trim();
                    }
                    else
                    {
                        product = BusinessProducts.ToString().Trim();
                    }  
                    // old one// description = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " provider of " + BusinessCategory.ToString() + " deals in " + BusinessServices.ToString() + ". Get office/email address, contact tele phone number, reviews & location on map and their website details at VConnect™.";
                    description = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in " + businesshouseno + ", " + businessstreetname + (!string.IsNullOrEmpty(businesslandmark) ? " near " + businesslandmark : "") + " deals in " + category + " offering " + product +", " + service + ". Get email/ office address, customer care phone number, reviews and their Official website details at VConnect™";
                }
                else if (!string.IsNullOrEmpty(BusinessServices.ToString()))
                {
                   
                    if (BusinessServices.ToString().Trim().IndexOf(',') > 0)
                    {
                        string[] servs = BusinessServices.ToString().Trim().Split(',');
                        service = servs[0].Trim() + ", " + servs[1].Trim();
                    }
                    else
                    {
                        service = BusinessServices.ToString().Trim();
                    }             
                    //description = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " provider of " + BusinessCategory.ToString() + " deals in " + BusinessServices.ToString() + ". Get office/email address, contact tele phone number, reviews & location on map and their website details at VConnect™.";
                    description = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in " + businesshouseno + ", " + businessstreetname + (!string.IsNullOrEmpty(businesslandmark) ? " near " + businesslandmark : "") + " deals in " + category + " offering " + service + ". Get email/ office address, customer care phone number, reviews and their Official website details at VConnect™";
                }

                else if (!string.IsNullOrEmpty(BusinessProducts.ToString()))
                {                    
                   
                    if (BusinessProducts.ToString().Trim().IndexOf(',') >0)
                    {
                        string[] prodts = BusinessProducts.ToString().Trim().Split(',');
                    product = prodts[0].Trim() + ", " + prodts[1].Trim();
                    }
                    else{
                        product = BusinessProducts.ToString().Trim();
                    }                   
                    //description = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " provider of " + BusinessCategory.ToString() + " deals in " + BusinessProducts.ToString() + ". Get office/email address, contact tele phone number, reviews & location on map and their website details at VConnect™.";
                    description = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in " + businesshouseno + ", " + businessstreetname + (!string.IsNullOrEmpty(businesslandmark) ? " near " + businesslandmark : "") + " deals in " + category + " offering " + product + ". Get email/ office address, customer care phone number, reviews and their Official website details at VConnect™";
                }

                else
                {
                    if (BusinessCategory.ToString().Trim().IndexOf(',') > 0)
                    {
                        string[] cats = BusinessCategory.ToString().Trim().Split(',');
                        category = cats[0].Trim() + ", " + cats[1].Trim();
                    }
                    else
                    {
                        category = BusinessCategory.ToString().Trim();
                    }                   
                    //string[] cat = BusinessCategory.ToString().Trim().Split(',');
                    
                    //description = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " deals in " + BusinessCategory.ToString() + ". Get office/email address, contact tele phone number, reviews & location on map and their website details at VConnect™.";
                    description = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in " + businesshouseno + ", " + businessstreetname + (!string.IsNullOrEmpty(businesslandmark) ? " near " + businesslandmark : "") + " deals in " + category + ". Get email/ office address, customer care phone number, reviews and their Official website details at VConnect™";
                }
                ogdescription = "'Get the address, contact information and other details about " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in Nigeria on VConnect™ – the trusted local business directory of Nigeria.";


                keyword = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(fulladdress.Replace("  ", " ") + ", " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in Nigeria, " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessCategory.ToLower()) + " in " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(fulladdress.ToLower().Replace("  ", " ")));

                title = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(fulladdress.ToLower().Replace("  ", " ")).Trim() + " | VConnect™";


                imagealt = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in " + bizcity + ", " + BusinessState;

            }


        }

        public static void LoadMetaDetail(string BusinessID, string BusinessName, string BusinessCategory, string location, out string title, out string description, out string keyword, out string robots, out string canonical, string BusinessServices, out string ogdescription, out string imagealt, string bizcity, string BusinessState, string fulladdress,string BusinessProducts)
        {

            robots = "index, follow";
            imagealt = string.Empty;
            ogdescription = string.Empty;
            canonical = string.Empty;
            keyword = string.Empty;
            description = string.Empty;
            var xlext = ReadXML("business", BusinessID);
            if (xlext.Count() > 0)
            {
                title = "";
                description = "";
                keyword = "";
                robots = "";
                canonical = "";

                foreach (var meta in xlext)
                {
                    title = meta.Element("title").Value;
                    description = meta.Element("description").Value;
                    keyword = meta.Element("keywords").Value;
                    robots = meta.Element("robots").Value;
                    canonical = meta.Element("canonical").Value;

                    if (!string.IsNullOrEmpty(meta.Element("imagealt").Value))
                    {
                        imagealt = meta.Element("imagealt").Value;
                    }
                    else
                    {
                        imagealt = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in " + bizcity + ", " + BusinessState;
                    }


                }
            }

            else
            {
                //<Business Name> in <Address> deals in < service 1>, <service 2> OR <cat1> ,<cat2>. 
                //if (BusinessServices.ToString() != "")
                //{
                //    ogdescription = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(fulladdress.ToLower().Replace("  ", " ")).Trim() + " deals in " + BusinessServices + ". Get office/email address, contact tele phone number and their website details at Vconnect™ local business directory of Nigeria.";
                //    description = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(fulladdress.ToLower().Replace("  ", " ")).Trim() + " deals in " + BusinessServices + ". Get office/email address, contact tele phone number and their website details at Vconnect local business directory of Nigeria.";
                //}
                //else if (BusinessCategory.ToString() != "")
                //{
                //    ogdescription = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(fulladdress.ToLower().Replace("  ", " ")).Trim() + " deals in " + BusinessCategory + ". Get office/email address, contact tele phone number and their website details at Vconnect™ local business directory of Nigeria.";
                //    description = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(fulladdress.ToLower().Replace("  ", " ")).Trim() + " deals in " + BusinessCategory + ". Get office/email address, contact tele phone number and their website details at Vconnect™ local business directory of Nigeria.";
                //}
                //else
                //{
                //    ogdescription = "'Get the address, contact information and other details about " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in Nigeria on VConnect™ – the trusted local business directory of Nigeria.";
                //    description= BusinessName.ToString() +" provider of " + BusinessCategory.ToString() +" deals in "+ BusinessCategory.ToString() +","+ BusinessProducts.ToString() + " .Get office/email address, contact tele phone number, reviews & location on map and their website details at VConnect™.";
                //   // description = "Get the address, contact information and other details about " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(fulladdress.ToLower().Replace("  ", " ")) + " on VConnect™ – the trusted local business directory of Nigeria.";
                //}


                // changes done by divya on 5th june 2014(chhatrapal request)
                // changes done by devinder on 15th Dec 2014 (chhatrapal request)
              if (!string.IsNullOrEmpty(BusinessProducts.ToString()) && !string.IsNullOrEmpty(BusinessServices.ToString()))
                {

                    description = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " provider of " + BusinessCategory.ToString() + " deals in " + BusinessServices.ToString() + ". Get office/email address, contact tele phone number, reviews & location on map and their website details at VConnect™.";
                }
                else if (!string.IsNullOrEmpty(BusinessServices.ToString()))
                {


                    description = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " provider of " + BusinessCategory.ToString() + " deals in " + BusinessServices.ToString() + ". Get office/email address, contact tele phone number, reviews & location on map and their website details at VConnect™.";
                }

                else if (!string.IsNullOrEmpty(BusinessProducts.ToString()))
                {


                    description = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " provider of " + BusinessCategory.ToString() + " deals in " + BusinessProducts.ToString() + ". Get office/email address, contact tele phone number, reviews & location on map and their website details at VConnect™.";
                }

                else
                {

                    description = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " deals in " + BusinessCategory.ToString() + ". Get office/email address, contact tele phone number, reviews & location on map and their website details at VConnect™.";
                }
                ogdescription = "'Get the address, contact information and other details about " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in Nigeria on VConnect™ – the trusted local business directory of Nigeria.";
               

                keyword = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(fulladdress.Replace("  ", " ") + ", " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in Nigeria, " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessCategory.ToLower()) + " in " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(fulladdress.ToLower().Replace("  ", " ")));

                title = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(fulladdress.ToLower().Replace("  ", " ")).Trim() + " | VConnect™";
                

                imagealt = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(BusinessName.ToLower()) + " in " + bizcity + ", " + BusinessState;

            }
    

        }
        public static void LoadMetaBrowseByCat(string location, string type, string alphabet, int CurrentpageNo, out string title, out string description, out string MetaKeyword)
        {
            string sAlphabet = string.Empty;
            title = string.Empty;
            description = string.Empty;
            MetaKeyword = string.Empty;

            string city = string.Empty;
            string state = string.Empty;
            string sSearchLocation = "Nigeria";
            string PageTypeName = string.Empty;
            int iType = 0;

            if (location.IndexOf("-") != -1)
            {
                city = location.Substring(location.LastIndexOf("-") + 1, location.Length - location.LastIndexOf("-") - 1).Replace("_", " ").Replace("_", " ");
                state = location.Substring(0, location.LastIndexOf("-")).Replace("-", " ").Replace("  ", " ");
            }
            else
            {
                state = location.Replace("_", " ").Replace("  ", " ").ToString();
            }
            string scurrentpage = string.Empty;
            if (CurrentpageNo > 1)
            {
                scurrentpage = "-page" + CurrentpageNo;
            }
            if (type != null && alphabet != null && state != null)
            {
                sAlphabet = alphabet;
                iType = Convert.ToInt32(type);
                sSearchLocation = state;
            }
            else if (type != null && alphabet != null && state != null && city != null)
            {
                if (alphabet.IndexOf("?utm_source") != -1)
                {
                    var istartLength = alphabet.ToString().IndexOf("?utm_source");
                    sAlphabet = alphabet.Substring(0, istartLength);
                }
                else
                {
                    sAlphabet = alphabet;
                }
                sSearchLocation = state + "," + city.ToString().Replace("-", " ").Replace("_", " ");
                iType = Convert.ToInt32(type);
            }

            if (iType != 0 && sAlphabet != "")
            {
                if (Convert.ToInt32(iType) == 1)
                {
                    PageTypeName = "Businesses";
                    title = "Browse all Businesses from " + sAlphabet.ToString() + " in " + ToTitleCase(sSearchLocation) + " | VConnect™" + scurrentpage;
                    description = "Browse through various businesses from " + sAlphabet.ToString() + " in " + ToTitleCase(sSearchLocation) + " on VConnect™ – the trusted local business directory of Nigeria." + scurrentpage;
                    MetaKeyword = "Browse by businesses " + sAlphabet.ToString() + " in " + ToTitleCase(sSearchLocation) + scurrentpage;


                }
                else if (Convert.ToInt32(type) == 2)
                {

                    PageTypeName = "Category";
                    // lblPageTitle.Text = "Browse By Category";

                    title = "Browse all Category from " + sAlphabet.ToString() + " in " + ToTitleCase(sSearchLocation) + " | VConnect™" + scurrentpage;
                    description = "Browse through various categories from " + sAlphabet.ToString() + " in " + ToTitleCase(sSearchLocation) + " on VConnect™ – the trusted local business directory of Nigeria." + scurrentpage;
                    MetaKeyword = "Browse by category " + sAlphabet.ToString() + " in " + ToTitleCase(sSearchLocation) + scurrentpage;


                }
                else if (Convert.ToInt32(type) == 6)
                {

                    PageTypeName = "Product";
                    // lblPageTitle.Text = "Browse By Category";

                    title = "Browse all Products from " + sAlphabet.ToString() + " in " + ToTitleCase(sSearchLocation) + " | VConnect™" + scurrentpage;
                    description = "Browse through various products from " + sAlphabet.ToString() + " in " + ToTitleCase(sSearchLocation) + " on VConnect™ – the trusted local business directory of Nigeria." + scurrentpage;
                    MetaKeyword = "Browse by products " + sAlphabet.ToString() + " in " + ToTitleCase(sSearchLocation) + scurrentpage;


                }
                else if (Convert.ToInt32(type) == 7)
                {

                    PageTypeName = "Service";
                    // lblPageTitle.Text = "Browse By Category";

                    title = "Browse all Services from " + sAlphabet.ToString() + " in " + ToTitleCase(sSearchLocation) + " | VConnect™" + scurrentpage;
                    description = "Browse through various services from " + sAlphabet.ToString() + " in " + ToTitleCase(sSearchLocation) + " on VConnect™ – the trusted local business directory of Nigeria." + scurrentpage;
                    MetaKeyword = "Browse by services " + sAlphabet.ToString() + " in " + ToTitleCase(sSearchLocation) + scurrentpage;


                }
                else
                {
                    PageTypeName = "Brands";
                    // lblPageTitle.Text = "Browse By Brands";

                    title = "Browse all Brands from " + sAlphabet.ToString() + " in " + ToTitleCase(sSearchLocation) + " | VConnect™" + scurrentpage;
                    description = "Browse through various brands from " + sAlphabet.ToString() + " in " + ToTitleCase(sSearchLocation) + " on VConnect™ – the trusted local business directory of Nigeria." + scurrentpage;
                    MetaKeyword = "Browse by brands " + sAlphabet.ToString() + " in " + ToTitleCase(sSearchLocation) + scurrentpage;

                    //pagelink = "/brands-from-";
                    //Pageurl = sState + "/brands-from-" + sAlphabet.ToString();
                    //divNoBrowseByData.InnerHtml = "Sorry!! No category specific information available in <b>" + sSearchLocation + "</b>.";
                }

            }
        }
        public static IEnumerable<XElement> ReadXML(string contenttype, string contentid)
        {
            string filename = contenttype + ".xml";
            var main = XElement.Load(HttpContext.Current.Server.MapPath("~/common/files/seoxml/" + filename));
            IEnumerable<XElement> searched = from c in main.Elements("cmsmetaedit")
                                             where (string)c.Element("contenttypeid") == contentid
                                             && ((string)c.Element("contenttype")).ToLower() == contenttype.ToLower()
                                             && ((string)c.Element("status")).ToLower() == "true"
                                             select c;
            return searched;
        }
        public static void LoadMetalisting(string Pagetype, string keyword, string location, Int32 itemid, out string title, out string description, out string MetaKeyword, out string divBoilerText, out string sBreadcrumb, out string robots, out string canonical, out  string divHeadingTags)
        {
            string URL = System.Web.HttpContext.Current.Request.RawUrl.ToString();
            divHeadingTags = string.Empty;
            string sStateBoilerText = string.Empty;
            robots = "index,follow";
            canonical = string.Empty;
            divBoilerText = "";
            keyword = keyword.ToString().ToLower().Replace("providers-of", " ").Replace("dealers-of", " ").Replace("-", " ").Replace("_", " ").ToLower();
            string city = string.Empty, state = string.Empty, stateCityUrl = string.Empty, stateUrl = string.Empty, strloc = string.Empty, area = string.Empty;
            string[] strlocation = new string[] { };
            
            if (location.Contains('-'))
            {
                strlocation = location.Split('-');
                if (strlocation.Count() > 2)
                {
                    state = strlocation[0].Replace("_", " ").ToLower();
                    city = strlocation[1].Replace("_", " ").ToLower();
                    area = strlocation[2].Replace("_", " ").ToLower();
                    strloc = string.Format("{0}, {1}, {2}", area, city, state);
                }
                else
                {
                    state = strlocation[0].Replace("_", " ").ToLower();
                    city = strlocation[1].Replace("_", " ").ToLower();
                    strloc = string.Format("{0}, {1}", city, state);
                }
               stateCityUrl = string.Format("/{0}-{1}.html", strlocation[0], strlocation[1]).ToLower();
                //stateCityUrl = string.Format("/{0}.html", strlocation[1]).ToLower();
                stateUrl = string.Format("/{0}.html", state).Replace(" ", "_").ToLower();

            }

            else
            {
                state = location.Replace("_", " ").ToLower();
                strloc = state;
                stateUrl = string.Format("/{0}.html", location).ToLower();
            }

            if (state.ToLower() == "nigeria")
            {
                sBreadcrumb = string.Format("<li><a href='{0}'>{1}</a></li><li>{2}</li>", stateUrl, state.ToUpper(), keyword.ToUpper());
            }
            else if (state != "" && city != "" && (area != null && area !=""))
            {
                //sBreadcrumb = string.Format("<li><a href='/'>Home</a></li><li><a href='{0}'>{1}</a></li><li><a href='{2}'>{3}</a></li><li>{4}</li><li>{5}</li>", stateUrl, state.ToUpper(), stateCityUrl, city.ToUpper(), area.ToUpper(), keyword.ToUpper());
                sBreadcrumb = string.Format("<li><a href='/'>Home</a></li><li>{0}</li><li>{1}</li><li>{2}</li><li>{3}</li>", state.ToUpper(),  city.ToUpper(), area.ToUpper(), keyword.ToUpper());
            }
            else if (state != "" && city != "" && (area == null || area == "")) 
            {
                sBreadcrumb = string.Format("<li><a href='/'>Home</a></li><li><a href='{0}'>{1}</a></li><li><a href='{2}'>{3}</a></li><li>{4}</li>", stateUrl, state.ToUpper(), stateCityUrl, city.ToUpper(), keyword.ToUpper());
            }
            else
            {
                if (stateUrl.Contains('_'))
                {
                    sBreadcrumb = string.Format("<li><a href='/'>Home</a></li><li>{0}</li><li>{1}</li>", state.ToUpper(), keyword.ToUpper());
                }
                else
                {
                    sBreadcrumb = string.Format("<li><a href='/'>Home</a></li><li><a href='{0}'>{1}</a></li><li>{2}</li>", stateUrl, state.ToUpper(), keyword.ToUpper());
                }
            }
            var xlext = ReadXML(Pagetype, itemid.ToString(), state, city);
            if (xlext.Count() > 0)
            {
                title = "";
                description = "";
                MetaKeyword = "";
                robots = "";
                canonical = "";
                foreach (var meta in xlext)
                {
                    title = Convert.ToString(meta.Element("title").Value).Replace("-All-", ToTitleCase(strloc));
                    description = Convert.ToString(meta.Element("description").Value).Replace("-All-", ToTitleCase(strloc));
                    MetaKeyword = Convert.ToString(meta.Element("keywords").Value).Replace("-All-", ToTitleCase(strloc));
                    robots = Convert.ToString(meta.Element("robots").Value).Replace("-All-", ToTitleCase(strloc));
                    canonical = meta.Element("canonical").Value.Replace("-All-", ToTitleCase(strloc));                    
                    divHeadingTags = "<div class=\"listing-heading-bg\"><h1 title=\"" + ToTitleCase(meta.Element("heading").Value.Replace("-All-", ToTitleCase(strloc))) + "\">" + ToTitleCase(meta.Element("heading").Value.Replace("-All-", ToTitleCase(strloc))) + "</h1></div>";
                    if (location.Contains('-'))
                    {
                        strlocation = location.Split('-');                       
                        stateCityUrl = string.Format("{0}-{1}", strlocation[0], strlocation[1]).ToLower();
                        stateUrl = string.Format("{0}", state).ToLower();
                    }
                    else
                    {                        
                        strloc = state;
                        stateUrl = string.Format("{0}", state).ToLower();//string.Format("/{0}.html", location).ToLower();
                    }
                    string hostbusiness = ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + location; //System.Web.HttpContext.Current.Request.Url.ToString().ToLower().Trim().Substring(0,System.Web.HttpContext.Current.Request.Url.ToString().ToLower().Trim().LastIndexOf('/'));
                    string hostHeader = hostbusiness;
                    hostbusiness = hostbusiness + "/businesses-from-a";
                    if (System.Web.HttpContext.Current.Request.Url.ToString().ToLower().Trim().IndexOf("_s") != -1)
                    {
                        hostHeader = hostHeader + "/services-from-a";
                        divBoilerText = meta.Element("boilertext").Value.Replace("http://www.vconnect.com/-all-.html", hostbusiness).Replace("/-all-.html", hostbusiness).Replace("http://www.vconnect.com/" + strloc.ToLower() + ".html", hostbusiness).Replace("-All-", strloc.ToLower()).Replace("-all-", strloc.ToLower()).Replace("businesses.", "businesses and their <a href=\"" + hostHeader + "\"><u>services on VConnect</u></a>."); ;                     
                    }
                    else if (System.Web.HttpContext.Current.Request.Url.ToString().ToLower().Trim().IndexOf("_p") != -1)
                    {
                        hostHeader = hostHeader + "/products-from-a";
                        divBoilerText = meta.Element("boilertext").Value.Replace("http://www.vconnect.com/-all-.html", hostbusiness).Replace("/-all-.html", hostbusiness).Replace("http://www.vconnect.com/" + strloc.ToLower() + ".html", hostbusiness).Replace("-All-", strloc.ToLower()).Replace("-all-", strloc.ToLower()).Replace("businesses.", "businesses and their <a href=\"" + hostHeader + "\"><u>products on VConnect</u></a>."); ;
                    }
                    else if (System.Web.HttpContext.Current.Request.Url.ToString().ToLower().Trim().IndexOf("_c") != -1)
                    {
                        hostHeader = hostHeader + "/categories-from-a";
                        divBoilerText = meta.Element("boilertext").Value.Replace("http://www.vconnect.com/-all-.html", hostbusiness).Replace("/-all-.html", hostbusiness).Replace("http://www.vconnect.com/" + strloc.ToLower() + ".html", hostbusiness).Replace("-All-", strloc.ToLower()).Replace("-all-", strloc.ToLower()).Replace("businesses.", "<a href=\"" + hostHeader + "\"><u>businesses category wise</u></a>.");
                    }
                    //if (!string.IsNullOrEmpty(stateCityUrl))
                    //{
                    //    divBoilerText = meta.Element("boilertext").Value.Replace("/-all-.html", "/"+stateCityUrl+".html").Replace("-All-", strloc.ToLower()).Replace("-all-", strloc.ToLower());
                    //}
                    //else 
                    //{
                    //    divBoilerText = meta.Element("boilertext").Value.Replace("/-all-.html", "/"+stateUrl+".html").Replace("-All-", strloc.ToLower()).Replace("-all-", strloc.ToLower());
                    //}
                }
            }
            else
            {
                switch (Pagetype)
                {
                    case "1":
                        #region product

                        //string hostbusiness = System.Web.HttpContext.Current.Request.Url.ToString().ToLower().Trim().Substring(0,System.Web.HttpContext.Current.Request.Url.ToString().ToLower().Trim().LastIndexOf('/'));
                        string hostbusiness = ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + location; 
                        string hostHeader = hostbusiness;
                        hostbusiness = hostbusiness + "/businesses-from-a";
                        hostHeader = hostHeader + "/products-from-a";
                        if (city != "" && state != "")
                        {

                            divHeadingTags = "<div class=\"listing-heading-bg\"><h1 title=\"Dealers of " + FixDescription(ToTitleCase(keyword), 35) + " in " + ToTitleCase(strloc) + "\" >Dealers of " + FixDescription(ToTitleCase(keyword), 35) + " in " + ToTitleCase(strloc) + "</h1></div>";
                            divBoilerText = "VConnect™ is the most comprehensive local business directory providing information about dealers selling " + ToTitleCase(keyword) + " in " + ToTitleCase(city) + " and several other <a href=\"" + hostbusiness + "\" title=\"Businesses in " + ToTitleCase(city) + " - VConnect\"><u>businesses in " + ToTitleCase(city) + "</u></a>. We are a reliable and one-stop source of business addresses, contact information, reviews and complete details about businesses and their <a href=\"" + hostHeader + "\"><u>products on VConnect</u></a>.";
                        }
                        else
                        {
                            divHeadingTags = "<div class=\"listing-heading-bg\"><h1 title=\"Dealers of " + FixDescription(ToTitleCase(keyword), 35) + " in " + ToTitleCase(state) + "\" >Dealers of " + FixDescription(ToTitleCase(keyword), 35) + " in " + ToTitleCase(state) + "</h1></div>";
                            divBoilerText = "VConnect™ is the most comprehensive local business directory providing information about dealers selling " + ToTitleCase(keyword) + " in " + ToTitleCase(state) + " and several other <a href=\"" + hostbusiness + "\" title=\"Businesses in " + ToTitleCase(state) + " - VConnect\"><u>businesses in " + ToTitleCase(state) + "</u></a>. We are a reliable and one-stop source of business addresses, contact information, reviews and complete details about businesses and their <a href=\"" + hostHeader + "\"><u>products on VConnect</u></a>.";
                        }
                        
                        if (URL.ToLower().IndexOf("online") != -1)
                        {
                            if (strloc == "nigeria")
                            {

                                description = "Find " + keyword + " shops address, contact tele phone number, email id and website info online. " + ToTitleCase(keyword) + " sellers  in " + ToTitleCase(strloc) + " on VConnect™.";
                            }
                            else
                            {
                                description = "Find " + keyword + " shops address, contact tele phone number, email id and website info online. " + ToTitleCase(keyword) + " sellers in " + ToTitleCase(strloc) + ", Nigeria on VConnect™.";
                            }
                        }
                        else if (URL.ToLower().IndexOf("shop") != -1)
                        {
                            if (strloc == "nigeria")
                            {

                                description = "Find " + keyword + " address, contact tele phone number, email id and website info online. " + ToTitleCase(keyword) + " sellers  in " + ToTitleCase(strloc) + " on VConnect™.";
                            }
                            else
                            {
                                description = "Find " + keyword + " address, contact tele phone number, email id and website info online. " + ToTitleCase(keyword) + " sellers in " + ToTitleCase(strloc) + ", Nigeria on VConnect™.";
                            }
                        }
                        else if (URL.ToLower().IndexOf("seller") != -1)
                        {
                            if (strloc == "nigeria")
                            {

                                description = "Find " + ToTitleCase(keyword) + " address, contact tele phone number, email id and website info online. " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + " on VConnect™.";
                            }
                            else
                            {
                                description = "Find " + ToTitleCase(keyword) + " address, contact tele phone number, email id and website info online. " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + ", Nigeria on VConnect™.";
                            }
                        }
                        else
                        {
                            //if (strloc == "nigeria")
                            //{

                            //    description = "Find " + keyword + " shops address, contact tele phone number, email id and website info online. " + ToTitleCase(keyword) + " sellers online in " + ToTitleCase(strloc) + " on VConnect™.";
                            //}
                            //else
                            //{
                            //    description = "Find " + keyword + " shops address, contact tele phone number, email id and website info online. " + ToTitleCase(keyword) + " sellers online in " + ToTitleCase(strloc) + ", Nigeria on VConnect™.";
                            //}
                            description = "Find " + ToTitleCase(keyword) + " shops, stores and distributors with their address, phone number, email id and website info in " + ToTitleCase(strloc) + " on Vconnect. The trusted local business directory for Buyers & Sellers in Nigeria.";
                        }


                        MetaKeyword = "dealers of " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + ", vendors of " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc);
                        if (URL.ToLower().IndexOf("manufacturer") != -1)
                        {
                            title = "Dealers of " + ToTitleCase(keyword) + " & Supplier in " + ToTitleCase(strloc);
                        }
                        else if (URL.ToLower().IndexOf("supplier") != -1)
                        {
                            title = "Dealers of " + ToTitleCase(keyword) + " & Manufacturers in " + ToTitleCase(strloc);
                        }
                        else
                        {
                           // title = "Dealers of " + ToTitleCase(keyword) + " Manufacturers & Supplier in " + ToTitleCase(strloc);
                            title =  ToTitleCase(keyword) + " Dealers & Supplier in " +ToTitleCase(strloc);
                        }



                        // Dealers of General Suppliers & Manufacturers in Lagos | VConnect™

                        title += " | VConnect™";
                        #endregion
                        break;
                    case "2":
                        #region services
                        hostbusiness = ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + location; 
                        //hostbusiness = System.Web.HttpContext.Current.Request.Url.ToString().ToLower().Trim().Substring(0,System.Web.HttpContext.Current.Request.Url.ToString().ToLower().Trim().LastIndexOf('/'));
                        hostHeader = hostbusiness;
                        hostbusiness = hostbusiness + "/businesses-from-a";
                        hostHeader = hostHeader + "/services-from-a";
                        if (city != "" && state != "")
                        {
                            divHeadingTags = "<div class=\"listing-heading-bg\"><h1 title=\"Providers of " + FixDescription(ToTitleCase(keyword), 35) + " in " + ToTitleCase(strloc) + "\">Providers of " + FixDescription(ToTitleCase(keyword), 35) + " in " + ToTitleCase(strloc) + "</h1></div>";
                            divBoilerText = "VConnect™ is the most comprehensive local business directory providing information about vendors selling " + ToTitleCase(keyword) + " in " + ToTitleCase(city) + " and several other <a href=\"" + hostbusiness + "\" title=\"Businesses in " + ToTitleCase(city) + "- VConnect\"><u>businesses in " + ToTitleCase(city) + " </u></a>. We are a reliable and one-stop source of business addresses, contact information, reviews and complete details about businesses and their <a href=\"" + hostHeader + "\"><u>services on VConnect</u></a>.";                            
                        }
                        else
                        {
                            divHeadingTags = "<div class=\"listing-heading-bg\"><h1 title=\"Providers of " + FixDescription(ToTitleCase(keyword), 35) + " in " + ToTitleCase(state) + "\">Providers of " + FixDescription(ToTitleCase(keyword), 35) + " in " + ToTitleCase(state) + "</h1></div>";
                            divBoilerText = "VConnect™ is the most comprehensive local business directory providing information about vendors selling " + ToTitleCase(keyword) + " in "+ ToTitleCase(state) +" and several other <a href=\"" + hostbusiness + "\" title=\"Businesses in " + ToTitleCase(state) + "- VConnect\"><u>businesses in " + ToTitleCase(state) + " </u></a>. We are a reliable and one-stop source of business addresses, contact information, reviews and complete details about businesses and their <a href=\"" + hostHeader + "\"><u>services on VConnect</u></a>.";                            
                        }
                        // description= "Find customer care number, Vendor’s  office & agency address, email id and website info to get"+  <Services Name> +"Service in "+<Location>+", Nigeria on VConnect™";
                        if (URL.ToLower().IndexOf("service") != -1)
                        {
                            //if (strloc == "nigeria")
                            //{
                            //    description = "Find customer care number, Vendor’s  office & agency address, email id and website info to get " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + " on VConnect™.";
                            //}
                            //else
                            //{
                            //    description = "Find customer care number, Vendor’s  office & agency address, email id and website info to get " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + ", Nigeria on VConnect™.";
                            //}
                            description = "Find list of best " + ToTitleCase(keyword) + " service providers with their address, customer care number and website info in " + ToTitleCase(strloc) + " on Vconnect. The trusted local business directory for Buyers & Sellers in Nigeria";                                
                        }
                        else
                        {
                            //if (strloc == "nigeria")
                            //{
                            //    description = "Find customer care number, Vendor’s  office & agency address, email id and website info to get " + ToTitleCase(keyword) + " Service in " + ToTitleCase(strloc) + " on VConnect™.";
                            //}
                            //else
                            //{
                            //    description = "Find customer care number, Vendor’s  office & agency address, email id and website info to get " + ToTitleCase(keyword) + " Service in " + ToTitleCase(strloc) + ", Nigeria on VConnect™.";
                            //}
                            //if (strloc == "nigeria")
                            //{
                            //    description = "Find list of best " + ToTitleCase(keyword) + " service providers with their address, customer care number and website info " + ToTitleCase(strloc) + " on Vconnect. The trusted local business directory for Buyers & Sellers in Nigeria";
                            //}
                            //else
                            //{
                                description = "Find list of best " + ToTitleCase(keyword) + " service providers with their address, customer care number and website info in " + ToTitleCase(strloc) + " on Vconnect. The trusted local business directory for Buyers & Sellers in Nigeria";                                
                            //}
                        }
                        if (URL.ToLower().IndexOf("services-vendors") != -1)
                        {
                            title = ToTitleCase(keyword) + " & Vendors in " + ToTitleCase(strloc);
                        }
                        else if (URL.ToLower().IndexOf("service") != -1)
                        {
                            title = ToTitleCase(keyword) + " Vendors in " + ToTitleCase(strloc);
                        }
                        else
                        {
                            //title = ToTitleCase(keyword) + " Vendors & Services Provider in " + ToTitleCase(strloc);
                            title = ToTitleCase(keyword) + " Service Providers in " + ToTitleCase(strloc);
                        }
                        title += " | VConnect™";
                        MetaKeyword = "Providers of  " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + ", vendors providing " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc);
                        #endregion
                        break;

                    case "3":
                        #region brand

                        if (city != "" && state != "")
                        {
                            divBoilerText = "VConnect™ is the most comprehensive local business directory providing information about vendors offering " + ToTitleCase(keyword) + " in  " + ToTitleCase(city) + " and several other <a href=\"" + stateCityUrl + "\" title=\"Businesses in " + ToTitleCase(city) + " - VConnect\"><u>businesses in " + ToTitleCase(city) + "</u></a>. We are a reliable and one-stop source of business addresses, contact information, reviews and complete details about businesses.";
                            divHeadingTags = "<div class=\"listing-heading-bg\"><h1 title=\"List of " + FixDescription(ToTitleCase(keyword), 35) + " in " + ToTitleCase(strloc) + "\">Providers of " + FixDescription(ToTitleCase(keyword), 35) + " Supplier in " + ToTitleCase(strloc) + "</h1></div>";
                        }
                        else
                        {
                            divBoilerText = "VConnect™ is the most comprehensive local business directory providing information about vendors offering " + ToTitleCase(keyword) + " in  " + ToTitleCase(state) + " and several other <a href=\"" + stateUrl + "\" title=\"Businesses in " + ToTitleCase(state) + " - VConnect\"><u>businesses in " + ToTitleCase(state) + "</u></a>. We are a reliable and one-stop source of business addresses, contact information, reviews and complete details about businesses.";
                            divHeadingTags = "<div class=\"listing-heading-bg\"><h1 title=\"List of " + FixDescription(ToTitleCase(keyword), 35) + " in " + ToTitleCase(state) + "\">Providers of " + FixDescription(ToTitleCase(keyword), 35) + " Supplier in " + ToTitleCase(state) + "</h1></div>";
                        }

                        if (URL.ToLower().IndexOf("store") != -1)
                        {
                            title = ToTitleCase(keyword) + " Outlets in " + ToTitleCase(strloc);
                        }
                        else if (URL.ToLower().IndexOf("outlet") != -1)
                        {
                            title = ToTitleCase(keyword) + " Stores in " + ToTitleCase(strloc);
                        }
                        else
                        {
                            title = ToTitleCase(keyword) + " Stores & Outlets in " + ToTitleCase(strloc);
                        }
                        if (URL.ToLower().IndexOf("brand") != -1)
                        {
                            if (strloc == "nigeria")
                            {

                                description = "Find " + ToTitleCase(keyword) + " showrooms office information email address, Contact Tele phone numbers and other website details in " + ToTitleCase(strloc) + " - VConnect™";
                            }
                            else
                            {
                                description = "Find " + ToTitleCase(keyword) + " showrooms office information email address, Contact Tele phone numbers and other website details in " + ToTitleCase(strloc) + ", Nigeria - VConnect™";
                            }
                        }
                        else
                        {
                            if (strloc == "nigeria")
                            {

                                description = "Find " + ToTitleCase(keyword) + " brand showrooms office information email address, Contact Tele phone numbers and other website details in " + ToTitleCase(strloc) + " - VConnect™";
                            }
                            else
                            {
                                description = "Find " + ToTitleCase(keyword) + " brand showrooms office information email address, Contact Tele phone numbers and other website details in " + ToTitleCase(strloc) + ", Nigeria - VConnect™";
                            }
                        }
                        MetaKeyword = ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + ", " + ToTitleCase(keyword) + " Dealers, " + ToTitleCase(keyword) + " Dealer Phone number in " + ToTitleCase(strloc) + ", " + ToTitleCase(keyword) + " Dealer Address in " + ToTitleCase(strloc);

                        title += " | VConnect™";

                        break;
                        #endregion
                    case "4":
                        #region category
                        //hostbusiness = System.Web.HttpContext.Current.Request.Url.ToString().ToLower().Trim().Substring(0,System.Web.HttpContext.Current.Request.Url.ToString().ToLower().Trim().LastIndexOf('/'));
                        hostbusiness = ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + location; 
                        hostHeader = hostbusiness;
                        hostbusiness = hostbusiness + "/businesses-from-a";
                        hostHeader = hostHeader + "/categories-from-a";
                        if (state != "" && city != "")
                        {
                            divHeadingTags = "<div class=\"listing-heading-bg\"><h1 title=\"" + FixDescription(ToTitleCase(keyword), 35) + " in " + ToTitleCase(strloc) + "\">List of " + FixDescription(ToTitleCase(keyword), 35) + " in  " + ToTitleCase(strloc) + "</h1></div>";
                            divBoilerText = "VConnect™ is the most comprehensive local business directory providing information about " + ToTitleCase(keyword) + " in  " + ToTitleCase(city) + " and several other <a href=\"" + hostbusiness + "\" title=\"Businesses in " + ToTitleCase(city) + " - VConnect\"><u>businesses in " + ToTitleCase(city) + "</u></a>. We are a reliable and one-stop source of business addresses, contact information, reviews and complete details about <a href=\"" + hostHeader + "\"><u>businesses category wise</u></a>.";                                               
                        }
                        else
                        {
                            divHeadingTags = "<div class=\"listing-heading-bg\"><h1 title=\"" + FixDescription(ToTitleCase(keyword), 35) + " in " + ToTitleCase(state) + "\">List of " + FixDescription(ToTitleCase(keyword), 35) + " in  " + ToTitleCase(state) + "</h1></div>";
                            divBoilerText = "VConnect™ is the most comprehensive local business directory providing information about " + ToTitleCase(keyword) + " in  " + ToTitleCase(state) + " and several other <a href=\"" + hostbusiness + "\" title=\"Businesses in " + ToTitleCase(state) + " - VConnect\"><u>businesses in " + ToTitleCase(state) + "</u></a>. We are a reliable and one-stop source of business addresses, contact information, reviews and complete details about <a href=\"" + hostHeader + "\"><u>businesses category wise</u></a>.";                                               
                        }

                        //description = "Find addresses, contact information and other details of " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + " on VConnect™ – the trusted local business directory of Nigeria" + ".";

                        description = "Find list of top " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + " with their name, address, contact number and other information on VConnect™. The trusted local business directory for Buyers & Sellers in Nigeria.";
                        MetaKeyword = ToTitleCase(keyword.ToLower()) + " in " + ToTitleCase(strloc) + ", list of " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + ", " + ToTitleCase(keyword) + ", " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + " list, addresses of " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + ", phone numbers of " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + ", reviews of " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc);
                        title = "List of " + ToTitleCase(keyword.ToLower()) + " in " + ToTitleCase(strloc);                        
                        title += " | VConnect™";
                        #endregion
                        break;
                    default:
                        #region Default
                        if (city != "" && state != "")
                        {
                            divHeadingTags = "<div class=\"listing-heading-bg\"><h1 title=\"Search Results for " + FixDescription(ToTitleCase(keyword), 35) + " in  " + ToTitleCase(strloc) + "\">" + FixDescription(ToTitleCase(keyword), 35) + " in  " + ToTitleCase(strloc) + "</h1></div>";
                        }
                        else
                        {
                            divHeadingTags = "<div class=\"listing-heading-bg\"><h1 title=\"Search Results for " + FixDescription(ToTitleCase(keyword), 35) + " in  " + ToTitleCase(state) + "\">" + FixDescription(ToTitleCase(keyword), 35) + " in  " + ToTitleCase(state) + "</h1></div>";
                        }



                        if (URL.Contains("_p"))
                        {
                            divBoilerText = "";
                            #region Description based on URL
                            if (URL.ToLower().IndexOf("online") != -1)
                            {
                                if (strloc == "nigeria")
                                {

                                    description = "Find " + keyword + " shops address, contact tele phone number, email id and website info online. " + ToTitleCase(keyword) + " sellers  in " + ToTitleCase(strloc) + " on VConnect™.";
                                }
                                else
                                {
                                    description = "Find " + keyword + " shops address, contact tele phone number, email id and website info online. " + ToTitleCase(keyword) + " sellers in " + ToTitleCase(strloc) + ", Nigeria on VConnect™.";
                                }
                            }
                            else if (URL.ToLower().IndexOf("shop") != -1)
                            {
                                if (strloc == "nigeria")
                                {

                                    description = "Find " + keyword + " address, contact tele phone number, email id and website info online. " + ToTitleCase(keyword) + " sellers  in " + ToTitleCase(strloc) + " on VConnect™.";
                                }
                                else
                                {
                                    description = "Find " + keyword + " address, contact tele phone number, email id and website info online. " + ToTitleCase(keyword) + " sellers in " + ToTitleCase(strloc) + ", Nigeria on VConnect™.";
                                }
                            }
                            else if (URL.ToLower().IndexOf("seller") != -1)
                            {
                                if (strloc == "nigeria")
                                {
                                    description = "Find " + keyword + " address, contact tele phone number, email id and website info online. " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + " on VConnect™.";
                                }
                                else
                                {
                                    description = "Find " + keyword + " address, contact tele phone number, email id and website info online. " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + ", Nigeria on VConnect™.";
                                }
                            }
                            else
                            {
                                if (strloc == "nigeria")
                                {
                                    description = "Find " + keyword + " shops address, contact tele phone number, email id and website info online. " + ToTitleCase(keyword) + " sellers online in " + ToTitleCase(strloc) + " on VConnect™.";
                                }
                                else
                                {
                                    description = "Find " + keyword + " shops address, contact tele phone number, email id and website info online. " + ToTitleCase(keyword) + " sellers online in " + ToTitleCase(strloc) + ", Nigeria on VConnect™.";
                                }
                            }
                            #endregion
                            #region Metakeyword based on URL
                            MetaKeyword = "dealers of " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + ", vendors of " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc);
                            #endregion
                            #region Title based on URL
                            if (URL.ToLower().IndexOf("manufacturer") != -1)
                            {
                                title = "Dealers of " + ToTitleCase(keyword) + " & Supplier in " + ToTitleCase(strloc);
                            }
                            else if (URL.ToLower().IndexOf("supplier") != -1)
                            {
                                title = "Dealers of " + ToTitleCase(keyword) + " & Manufacturers in " + ToTitleCase(strloc);
                            }
                            else
                            {
                                title = "Dealers of " + ToTitleCase(keyword) + " Manufacturers & Supplier in " + ToTitleCase(strloc);
                            }
                            // Dealers of General Suppliers & Manufacturers in Lagos | VConnect™
                            title += " | VConnect™";
                            #endregion
                        }

                        else if (URL.Contains("_s"))
                        {
                            divBoilerText = "";
                            #region Description based on URL
                            if (URL.ToLower().IndexOf("service") != -1)
                            {
                                if (strloc == "nigeria")
                                {
                                    description = "Find customer care number, Vendor’s  office & agency address, email id and website info to get " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + " on VConnect™.";
                                }
                                else
                                {
                                    description = "Find customer care number, Vendor’s  office & agency address, email id and website info to get " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + ", Nigeria on VConnect™.";
                                }
                            }
                            else
                            {
                                if (strloc == "nigeria")
                                {
                                    description = "Find customer care number, Vendor’s  office & agency address, email id and website info to get " + ToTitleCase(keyword) + " Service in " + ToTitleCase(strloc) + " on VConnect™.";
                                }
                                else
                                {
                                    description = "Find customer care number, Vendor’s  office & agency address, email id and website info to get " + ToTitleCase(keyword) + " Service in " + ToTitleCase(strloc) + ", Nigeria on VConnect™.";
                                }
                            }
                            #endregion
                            #region Metakeyword based on URL
                            MetaKeyword = "Providers of  " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + ", vendors providing " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc);
                            #endregion
                            #region Title based on URL
                            if (URL.ToLower().IndexOf("services-vendors") != -1)
                            {
                                title = ToTitleCase(keyword) + " & Vendors in " + ToTitleCase(strloc);
                            }
                            else if (URL.ToLower().IndexOf("service") != -1)
                            {
                                title = ToTitleCase(keyword) + " Vendors in " + ToTitleCase(strloc);
                            }
                            else
                            {
                                title = ToTitleCase(keyword) + " Vendors & Services Provider in " + ToTitleCase(strloc);
                            }
                            title += " | VConnect™";
                            #endregion
                        }
                        else if (URL.Contains("_b"))
                        {
                            divBoilerText = "";
                            #region Description based on URL
                            if (URL.ToLower().IndexOf("brand") != -1)
                            {
                                if (strloc == "nigeria")
                                {

                                    description = "Find " + ToTitleCase(keyword) + " showrooms office information email address, Contact Tele phone numbers and other website details in " + ToTitleCase(strloc) + " - VConnect™";
                                }
                                else
                                {
                                    description = "Find " + ToTitleCase(keyword) + " showrooms office information email address, Contact Tele phone numbers and other website details in " + ToTitleCase(strloc) + ", Nigeria - VConnect™";
                                }
                            }
                            else
                            {
                                if (strloc == "nigeria")
                                {

                                    description = "Find " + ToTitleCase(keyword) + " brand showrooms office information email address, Contact Tele phone numbers and other website details in " + ToTitleCase(strloc) + " - VConnect™";
                                }
                                else
                                {
                                    description = "Find " + ToTitleCase(keyword) + " brand showrooms office information email address, Contact Tele phone numbers and other website details in " + ToTitleCase(strloc) + ", Nigeria - VConnect™";
                                }
                            }
                            #endregion
                            #region Metakeyword based on URL
                            MetaKeyword = ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + ", " + ToTitleCase(keyword) + " Dealers, " + ToTitleCase(keyword) + " Dealer Phone number in " + ToTitleCase(strloc) + ", " + ToTitleCase(keyword) + " Dealer Address in " + ToTitleCase(strloc);
                            #endregion
                            #region Title based on URL
                            if (URL.ToLower().IndexOf("store") != -1)
                            {
                                title = ToTitleCase(keyword) + " Outlets in " + ToTitleCase(strloc);
                            }
                            else if (URL.ToLower().IndexOf("outlet") != -1)
                            {
                                title = ToTitleCase(keyword) + " Stores in " + ToTitleCase(strloc);
                            }
                            else
                            {
                                title = ToTitleCase(keyword) + " Stores & Outlets in " + ToTitleCase(strloc);
                            }
                            title += " | VConnect™";
                            #endregion
                        }
                        else if (URL.Contains("_c"))
                        {
                            divBoilerText = "";
                            #region Description based on URL
                            description = "Find addresses, contact information and other details of " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + " on VConnect™ – the trusted local business directory of Nigeria" + ".";
                            #endregion
                            #region Metakeyword based on URL
                            MetaKeyword = ToTitleCase(keyword.ToLower()) + " in " + ToTitleCase(strloc) + ", list of " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + ", " + ToTitleCase(keyword) + ", " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + " list, addresses of " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + ", phone numbers of " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + ", reviews of " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc);
                            #endregion
                            #region Title based on URL
                            title = "List of " + ToTitleCase(keyword.ToLower()) + " in " + ToTitleCase(strloc);
                            title += " | VConnect™";
                            #endregion
                        }
                        else
                        {
                            divBoilerText = "";
                            description = "Find addresses, contact information and other details of " + ToTitleCase(keyword) + " in  " + ToTitleCase(strloc) + " on VConnect™ – the trusted local business directory of Nigeria" + ".";
                            MetaKeyword = ToTitleCase(keyword) + " in  " + ToTitleCase(strloc) + ", list of " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc);
                            title = "Search Results for " + ToTitleCase(keyword) + " in " + ToTitleCase(strloc) + " | VConnect™";
                        }
                        #endregion
                        break;
                }
            }

        }
        public static IEnumerable<XElement> ReadXML(string pagetype, string contentid, string state, string city)
        {
            string contenttype = string.Empty;
            if (pagetype == "1")
            {
                contenttype = "product";
            }
            else if (pagetype == "2")
            {
                contenttype = "service";
            }
            else if (pagetype == "3")
            {
                contenttype = "brand";
            }
            else if (pagetype == "4")
            {
                contenttype = "category";
            }

            if (string.IsNullOrEmpty(state))
            {
                state = city;
                city = "";
            }
            string filename = contenttype + ".xml";
            if (File.Exists(HttpContext.Current.Server.MapPath("~/common/files/seoxml/" + filename)))
            {
                var main = XElement.Load(HttpContext.Current.Server.MapPath("~/common/files/seoxml/" + filename));
                IEnumerable<XElement> searched = from c in main.Elements("cmsmetaedit")
                                                 where (string)c.Element("contenttypeid") == contentid
                                                 && ((string)c.Element("contenttype")).ToLower() == contenttype.ToLower()
                                                 && ((string)c.Element("state")).ToLower() == state.ToLower().Trim()
                                                 && ((string)c.Element("city")).ToLower() == city.ToLower().Trim()
                                                 && ((string)c.Element("status")).ToLower() == "true"
                                                 select c;
                if (searched.Count() < 1)
                {
                    searched = from c in main.Elements("cmsmetaedit")
                               where (string)c.Element("contenttypeid") == contentid
                               && ((string)c.Element("contenttype")).ToLower() == contenttype.ToLower()
                               && ((string)c.Element("state")).ToLower() == "-all-"
                               && ((string)c.Element("status")).ToLower() == "true"
                               select c;
                }

                return searched;
            }
            else
            {
                return Enumerable.Empty<XElement>();
            }
        }
        public static string FixDescription(string Desc, int Length)
        {
            string strNewDesc = "";
            strNewDesc = (new System.Text.RegularExpressions.Regex("<[^>]*>")).Replace(Desc, "");
            if (strNewDesc.Length > Length)
            {
                strNewDesc = strNewDesc.Substring(0, Length) + "...";
            }
            return strNewDesc;
        }
        #region new cookie implementation
        public static string EncryptString(string Message, string key)
        {
            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(key));
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            byte[] DataToEncrypt = UTF8.GetBytes(Message);
            try
            {
                ICryptoTransform Encryptor = TDESAlgorithm.CreateEncryptor();
                Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }
            return Convert.ToBase64String(Results);
        }

        public static string DecryptString(string Message, string key)
        {
            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(key));
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            byte[] DataToDecrypt = Convert.FromBase64String(Message);
            try
            {
                ICryptoTransform Decryptor = TDESAlgorithm.CreateDecryptor();
                Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }
            return UTF8.GetString(Results);
        }
        // COOKIE ENCRYPTION/DECRYPTION IN USE

        public static void ClearCookies()
        {

            int limit = HttpContext.Current.Request.Cookies.Count;
            string userid = string.Empty;



            //== cookies created at the time of userconnect in userdashboard//


            CreateHttpCookie("PhoneNOList", null, null);  //delete cookie by passing null
            CreateHttpCookie(ConfigurationManager.AppSettings["VCLoginID"], null, null);
            CreateHttpCookie(ConfigurationManager.AppSettings["VCUserName"], null, null);
            CreateHttpCookie(ConfigurationManager.AppSettings["VCUserType"], null, null);
            CreateHttpCookie(ConfigurationManager.AppSettings["VCLoginType"], null, null);
            CreateHttpCookie(ConfigurationManager.AppSettings["VCUserContentID"], null, null);
            CreateHttpCookie(ConfigurationManager.AppSettings["VCUserPhoto"], null, null);
            CreateHttpCookie(ConfigurationManager.AppSettings["VCUserProviderId"], null, null);
            CreateHttpCookie(ConfigurationManager.AppSettings["VCUserUrl"], null, null);
            CreateHttpCookie(ConfigurationManager.AppSettings["VCEncriptedContentid"], null, null);
            CreateHttpCookie(ConfigurationManager.AppSettings["VCUserEmail"], null, null);
            CreateHttpCookie(ConfigurationManager.AppSettings["VCUserPhone"], null, null);
            CreateHttpCookie(ConfigurationManager.AppSettings["unk"], null, null);


            CreateHttpCookie("VCisfbconnect", null, null);
            CreateHttpCookie("VCistwconnect", null, null);
            CreateHttpCookie("VCisgmconnect", null, null);
            CreateHttpCookie("VCfboauth_token", null, null);
            CreateHttpCookie("VCtwoauth_token_secret", null, null);
            CreateHttpCookie("VCtwoauth_token", null, null);
            CreateHttpCookie("VCisfbShare", null, null);
            CreateHttpCookie("VCistwshare", null, null);


            MyGlobalVariables.SV_VCIsLoggedIn = "0";

            try
            {
                if (!(System.Web.HttpContext.Current.Session["VCHistory"] == null))
                    System.Web.HttpContext.Current.Session["VCHistory"] = null;
            }
            catch (Exception ex) { }

            try
            {
                if (!(System.Web.HttpContext.Current.Session["model"] == null))
                {
                    HttpContext.Current.Session.Remove("model");
                }
            }
            catch (Exception ex) { }
        }
        public static void CreateHttpCookie(string cookieName, string cookieValue, string key)
        {
            HttpCookie objCookie;
            if (HttpContext.Current.Request.Cookies[cookieName] != null)
            {
                objCookie = new HttpCookie(cookieName);
                if (!HttpContext.Current.Request.Url.AbsoluteUri.Contains("localhost") && !HttpContext.Current.Request.Url.AbsoluteUri.Contains("stage.vconnect.com") && !HttpContext.Current.Request.Url.AbsoluteUri.Contains("beta.vconnect.com") )
                {
                    objCookie.Domain = "vconnect.com";
                    objCookie.Path = "/";
                }
                objCookie.Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies.Add(objCookie);
            }

            if (cookieValue != null)    //handling 'delete cookies' by passing null.
            {
                if (cookieValue.Trim() == string.Empty)
                    return;
                else
                {
                    

                    //   objCookie = new HttpCookie(cookieName, Utility.EncryptString(cookieValue, key) + "$" + HttpContext.Current.Request.Cookies["ASP.NET_SessionId"].Value);

                    objCookie = new HttpCookie(cookieName, Utility.EncryptString(cookieValue, key) + "$" + HttpContext.Current.Request.Cookies["unk"].Value.ToString().Trim());
                }
                if (!HttpContext.Current.Request.Url.AbsoluteUri.Contains("localhost") && !HttpContext.Current.Request.Url.AbsoluteUri.Contains("stage.vconnect.com") && !HttpContext.Current.Request.Url.AbsoluteUri.Contains("beta.vconnect.com"))
                {
                    objCookie.Domain = "vconnect.com";
                    objCookie.Path = "/";
                }
                objCookie.Expires = DateTime.Now.AddDays(3);
                HttpContext.Current.Response.Cookies.Add(objCookie);
                
            }

        }
        public static void ClearProperties()
        {
            UserSession objUS = new UserSession();
            objUS.SV_VCUserContentID = null;
            objUS.SV_VCUserType = null;
            objUS.SV_VCUserUrl = null;
            objUS.SV_VCProviderId = null;
            objUS.SV_VCUserPhoto = null;
            objUS.SV_VCUserType = null;
            objUS.SV_VCEncriptedContentid = null;
            objUS.SV_VCUserName = null;
            objUS.SV_VCLoginID = null;
            objUS.SV_VCLoginType = null;
        }
        #endregion        
    }
}