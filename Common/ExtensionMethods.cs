﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Vconnect.Common
{
    public static class ExtensionMethods
    {
        public static string UppercaseFirstLetter(this string value)
        {
            //
            // Uppercase the first letter in the string this extension is called on.
            //
            if (!string.IsNullOrWhiteSpace(value) && value.Length > 0)
            {
                char[] array = value.ToCharArray();
                array[0] = char.ToUpper(array[0]);
                return new string(array);
            }
            return value;
        }
        //public static string PageLinks(this HtmlHelper html, int currentPage,
        //int totalPages, Func<int, string> pageUrl)
        //{
        //    StringBuilder result = new StringBuilder();
        //    for (int i = 1; i <= totalPages; i++)
        //    {
        //        TagBuilder tag = new TagBuilder("a"); // Construct an <a> tag
        //        tag.MergeAttribute("href", pageUrl(i));
        //        tag.InnerHtml = i.ToString();
        //        if (i == currentPage)
        //            tag.AddCssClass("selected");
        //        result.AppendLine(tag.ToString());
        //    }
        //    return result.ToString();
        //    return HttpContext.Current.Server.HtmlEncode(result.ToString());
        //}
        public static string AlphabetRouting(this HtmlHelper html, int currentPage, string title, string location, Func<string, string> pageUrl)
        {
            string[] alphabet = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < alphabet.Length; i++)
            {
                TagBuilder tag = new TagBuilder("a");
                tag.MergeAttribute("href", pageUrl(alphabet[i].ToString()));
                tag.MergeAttribute("title", string.Format("{0} for {1} in {2}", title, alphabet[i].ToString(), location));
                tag.InnerHtml = alphabet[i].ToString();
                if (i == currentPage)
                    tag.AddCssClass("selected");
                result.AppendLine(tag.ToString());
            }
            return result.ToString();
        }

    }
}