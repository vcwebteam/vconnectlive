﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DotNetOpenAuth.AspNet;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using DotNetOpenAuth.ApplicationBlock;
using DotNetOpenAuth.ApplicationBlock.Facebook;
using DotNetOpenAuth.OAuth2;
using System.Configuration;
namespace Vconnect.Common
{
    public class FacebookScopedClient : IAuthenticationClient
    {
        private string appId;
        private string appSecret;

        private const string baseUrl = "https://www.facebook.com/dialog/oauth?client_id=";
        public const string graphApiToken = "https://graph.facebook.com/oauth/access_token?";
        public const string graphApiMe = "https://graph.facebook.com/me?";
        public const string graphApiFrns = "https://graph.facebook.com/me/friends? ";
        private static readonly FacebookClient client = new FacebookClient
        {
            ClientIdentifier = ConfigurationManager.AppSettings["facebookAppID"],
            ClientCredentialApplicator = ClientCredentialApplicator.PostParameter(ConfigurationManager.AppSettings["facebookAppSecret"]),
        };

        private static string GetHTML(string URL)
        {
            string connectionString = URL;

            try
            {
                System.Net.HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(connectionString);
                myRequest.Credentials = CredentialCache.DefaultCredentials;
                //// Get the response
                WebResponse webResponse = myRequest.GetResponse();
                Stream respStream = webResponse.GetResponseStream();
                ////
                StreamReader ioStream = new StreamReader(respStream);
                string pageContent = ioStream.ReadToEnd();
                //// Close streams
                ioStream.Close();
                respStream.Close();
                return pageContent;
            }
            catch (Exception)
            {
            }
            return null;
        }

        

        private IDictionary<string, string> GetUserData(string accessCode, string redirectURI)
        {

            string token = GetHTML(graphApiToken + "client_id=" + appId + "&redirect_uri=" + HttpUtility.UrlEncode(redirectURI) + "&client_secret=" + appSecret + "&code=" + accessCode);
           
            if (token == null || token == "")
            {
                return null;
            }


            string dataMe = GetHTML(graphApiMe + "fields=id,name,email,username,gender,link,picture,birthday&access_token=" + token.Substring("access_token=", "&"));

            string dataFrns = GetHTML(graphApiFrns + "fields=id,name,email,username,gender,link,picture,birthday&access_token=" + token.Substring("access_token=", "&"));

            JObject joDataMe = JObject.Parse(dataMe);
            JObject joDataFrns = JObject.Parse(dataFrns);

            Dictionary<string, string> dictME = ConvertToDictionary(joDataMe);
            Dictionary<string, string> dictFrns = ConvertToDictionary(joDataFrns);


            Dictionary<string, string> dictFacebookResult = dictME.Concat(dictFrns).ToDictionary(x => x.Key, x => x.Value);

            return dictFacebookResult;
        }


        public static Dictionary<string, string> ConvertToDictionary(JObject jo)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();

            foreach (JProperty prop in jo.Properties())
            {
                if (prop.Value.Type == JTokenType.Array ||
                    prop.Value.Type == JTokenType.Object)
                {
                    // JSON string for complex object
                    dict.Add(prop.Name, prop.Value.ToString(Formatting.None));
                }
                else
                {
                    // primitive value converted to string
                    object value = ((JValue)prop.Value).Value;
                    dict.Add(prop.Name, value != null ? value.ToString() : null);
                }
            }
            return dict;
        }

        public FacebookScopedClient(string appId, string appSecret)
        {
            this.appId = appId;
            this.appSecret = appSecret;
            
        }

        public string ProviderName
        {
            get { return "Facebook"; }
        }

        public void RequestAuthentication(System.Web.HttpContextBase context, Uri returnUrl)
        {
            string url = baseUrl + appId + "&redirect_uri=" + HttpUtility.UrlEncode(returnUrl.ToString()) + "&scope=email,user_about_me,user_birthday,read_friendlists,user_photos";
            context.Response.Redirect(url);
        }

        public AuthenticationResult VerifyAuthentication(System.Web.HttpContextBase context)
        {
            HttpContext.Current.Session["ud1"] = "1";
            string code = context.Request.QueryString["code"];

            string rawUrl = context.Request.Url.OriginalString;
            //From this we need to remove code portion
            rawUrl = Regex.Replace(rawUrl, "&code=[^&]*", "");

            IDictionary<string, string> userData = GetUserData(code, rawUrl);
            HttpContext.Current.Session["ud"] = userData;
            //session["code"] = userData;
            if (userData == null)
                return new AuthenticationResult(false, ProviderName, null, null, null);

            string id = userData["id"];
            string username = userData["name"];
            //userData.Remove("id");
            //userData.Remove("username");

            AuthenticationResult result = new AuthenticationResult(true, ProviderName, id, username, userData);
            return result;
        }
    }

    public static class String
    {
        public static string Substring(this string str, string StartString, string EndString)
        {
            if (str.Contains(StartString))
            {
                int iStart = str.IndexOf(StartString) + StartString.Length;
                int iEnd = str.IndexOf(EndString, iStart);
                return str.Substring(iStart, (iEnd - iStart));
            }
            return null;
        }
    }
}