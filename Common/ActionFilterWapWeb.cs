﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Vconnect.Common
{
    public  class ActionFilterWapWeb : ActionFilterAttribute,IExceptionFilter
    {
        private static void RedirectToRoute(ActionExecutingContext context, object routeValues)
        {
            var rc = new RequestContext(context.HttpContext, context.RouteData);
            string url = RouteTable.Routes.GetVirtualPath(rc,
                new RouteValueDictionary(routeValues)).VirtualPath;
            if(url!="/")

            context.HttpContext.Response.Redirect(url, true);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            //base.OnActionExecuted(filterContext);
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string controllerName = System.Web.HttpContext.Current.Request.Browser.IsMobileDevice ? filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.Replace("WEB", "WAP") :
               "";
            
               // filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
           
            string actionName = System.Web.HttpContext.Current.Request.Browser.IsMobileDevice ? filterContext.ActionDescriptor.ActionName.Replace("WEB", "WAP") :
                "";
            
            RedirectToRoute(filterContext,
              new { controller = controllerName, action = actionName }
              );
        }

       
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            //base.OnResultExecuted(filterContext);
        }
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);
        }
        public void OnException(ExceptionContext filterContext)
        {
            //base.OnException(filterContext);
        }
    }
}