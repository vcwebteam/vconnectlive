﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Diagnostics;

/// <summary>
/// Summary description for ImageOptimization
/// </summary>
public static class ImageOptimization
{
    

    private static CloudBlobContainer imagesBlobContainer;

    public static System.Drawing.Image DownloadImageFromUrl(string imageUrl)
    {
        System.Drawing.Image image = null;
        System.Net.WebResponse webResponse = null;

        try
        {
            System.Net.HttpWebRequest webRequest = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(imageUrl);
            webRequest.AllowWriteStreamBuffering = true;
            webRequest.Timeout = 30000;

            webResponse = webRequest.GetResponse();

            System.IO.Stream stream = webResponse.GetResponseStream();

            image = System.Drawing.Image.FromStream(stream);


        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            webResponse.Close();
        }
        return image;
    }

    public static void cloudimageupload(byte[] imagebyte, string imageName, string imagelocation)
    {
        byte[] bytes = imagebyte;
        MemoryStream ms = new MemoryStream(bytes);

        var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["AzureWebJobsStorage"].ToString());
        var blobClient = storageAccount.CreateCloudBlobClient();
        imagesBlobContainer = blobClient.GetContainerReference(imagelocation);

        CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
        CloudBlockBlob imageBlob = null;

        string blobName = imageName;
        // Retrieve reference to a blob. 
        imageBlob = imagesBlobContainer.GetBlockBlobReference(blobName);
        //imageBlob = imagesBlobContainer.GetBlockBlobReference("images");

        imageBlob.UploadFromStream(ms);
        Trace.TraceInformation("Uploaded image file to {0}", imageBlob.Uri.ToString());
       // return imageBlob.Uri.ToString();
    }


    #region Image Optization
    public static void ResizeImage(int MaxWidth, int MaxHeight, string FileName, string NewFileName)
    {
       
            string savedimage = string.Empty;
            Image OriginalBmp = DownloadImageFromUrl(FileName);
            string imagename = FileName.Substring(FileName.LastIndexOf('/')).ToLower().TrimStart('/');

            // load up the image, figure out a "best fit" resize, and then save that new image
            // Bitmap OriginalBmp = (System.Drawing.Bitmap)System.Drawing.Image.FromFile(FileName).Clone();
            Size ResizedDimensions = GetDimensions(MaxWidth, MaxHeight, ref OriginalBmp);
            Bitmap NewBmp = new Bitmap(OriginalBmp, ResizedDimensions);
            byte[] resizebyte = ImageToByte(NewBmp);
            //System.Drawing.Imaging.ImageFormat imageFormat = OriginalBmp.RawFormat;
            //NewBmp.Save(NewFileName, imageFormat);

            ImageOptimization.cloudimageupload(resizebyte, imagename, NewFileName);
       
        
    }

    public static Size GetDimensions(int MaxWidth, int MaxHeight, ref Image Bmp)
    {
        int Width;
        int Height;
        float Multiplier;

        Height = Bmp.Height;
        Width = Bmp.Width;

        // this means you want to shrink an image that is already shrunken!
        if (Height <= MaxHeight && Width <= MaxWidth)
            return new Size(Width, Height);

        // check to see if we can shrink it width first
        Multiplier = (float)((float)MaxWidth / (float)Width);
        if ((Height * Multiplier) <= MaxHeight)
        {
            Height = (int)(Height * Multiplier);
            return new Size(MaxWidth, Height);
        }

        // if we can't get our max width, then use the max height
        Multiplier = (float)MaxHeight / (float)Height;
        Width = (int)(Width * Multiplier);
        return new Size(Width, MaxHeight);
    }


    #endregion


    public static byte[] ImageToByte(Image img)
    {
        ImageConverter converter = new ImageConverter();
        return (byte[])converter.ConvertTo(img, typeof(byte[]));
    }
}