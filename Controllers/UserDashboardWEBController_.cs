﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vconnect.Models;
using System.Data.SqlClient;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Web.Helpers;
using Vconnect.Common;
using System.Text.RegularExpressions;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Net;
using System.Configuration;

namespace Vconnect.Controllers
{
    public class UserDashboardWebController : Controller
    {

        //
        // GET: /UserDashboardWEB/
        #region image path
        public string webconfigSourceImagesRootPath = ConfigurationManager.AppSettings["SourceImagesRootPath"].ToString();
        public string webconfigDestinationImagesRootPath = ConfigurationManager.AppSettings["DestinationImagesRootPath"].ToString();
        #endregion
        public ActionResult Index()
        {
            return View();
        }
        #region editdetail
        public ActionResult UserDetailsWeb()
        {
            //123
            Int32 userid = 0;
            UserDashboardWebModel UserDbWEBModel = new UserDashboardWebModel();
            UserDbWEBModel.SearchLocation = "Lagos";
            UserDbWEBModel.SearchText = "Hotel";
            try
            {
                if (Request.Cookies["VCUserContentID"] != null)
                {
                    userid = Convert.ToInt32(Request.Cookies["VCUserContentID"].Value.ToString());
                    UserDbWEBModel.UserId = userid;
                }
                else
                {
                    return RedirectToAction("Login", "Account", new { rturl = "UserDashboardWeb/UserDetailsWeb" });
                }

                UserDbWEBModel.getWebUserDashboardDetails();
                UserDbWEBModel.WebUserDashboardDetails();


                EditDetails editdetail = new EditDetails();
                List<SelectListItem> listStatesNames = new List<SelectListItem>();
                editdetail.StateOptions = Utility.GetSelectList();


                if (UserDbWEBModel.ResultUserDBTab.UserDBDetails[0].stateid != 0)
                {
                    //editdetail.StateID = Convert.ToInt64(UserDbWEBModel.ResultUserDBTab.UserDBDetails[0].stateid);
                    //editdetail.CityID = Convert.ToInt64(UserDbWEBModel.ResultUserDBTab.UserDBDetails[0].cityid);
                    editdetail.CityOptions = Utility.GetSelectListCity(Convert.ToInt32(UserDbWEBModel.ResultUserDBTab.UserDBDetails[0].stateid));
                }
                else
                {
                    //editdetail.StateID = 0;
                    //editdetail.CityID = 0;

                    editdetail.CityOptions = Utility.GetSelectListCity(0);
                }
                UserDbWEBModel.editDetails = editdetail;

                ViewBag.message = TempData["message"];

                ViewBag.userdb = UserDbWEBModel;
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            return View(UserDbWEBModel);
        }



        [HttpPost]
        public ActionResult UserDetailsWeb(UserDashboardWebModel UserDbWEBModel, FormCollection form)
        {
            Int32 userid = 0;
            string errorMsg = string.Empty;
            try
            {
                if (Request.Cookies["VCUserContentID"] != null)
                {
                    userid = Convert.ToInt32(Request.Cookies["VCUserContentID"].Value.ToString());
                    UserDbWEBModel.UserId = userid;
                }
                UserDbWEBModel.getWebUserDashboardDetails();
                List<SelectListItem> listStatesNames = new List<SelectListItem>();
                UserDbWEBModel.editDetails.StateOptions = Vconnect.Common.Utility.GetSelectList();
                List<SelectListItem> listCiyNames = new List<SelectListItem>();
                UserDbWEBModel.editDetails.CityOptions = Vconnect.Common.Utility.GetSelectListCity(UserDbWEBModel.editDetails.SelectedStateId);
                int result = 6;
                int state_id = Convert.ToInt16(form["editDetails.StateID"]);
                int city_id = Convert.ToInt16(form["editDetails.CityID"]);
                string gender_id = Convert.ToString(form["Gender"]);
                string dob = Convert.ToString(form["dob"]);
                if (state_id == 0)
                {
                    errorMsg = "Enter the Sate";
                    // a = 0;
                }
                else
                    if (city_id == 0)
                    {
                        errorMsg = "Enter the City";
                        // b = 0;
                    }

                    else
                        if (string.IsNullOrEmpty(gender_id))
                        {
                            errorMsg = "Select the Gender";
                            //c = null;
                        }
                        else
                            if (string.IsNullOrEmpty(dob))
                            {
                                errorMsg = "Select the Date Of Birth";
                                // d = null;
                            }
                            else
                            {
                                result = UserDbWEBModel.WebEditDetails(UserDbWEBModel.editDetails, state_id, city_id, gender_id, dob);

                                if (result == 0)
                                {
                                    errorMsg = "User detail saved successfully";
                                    if (UserDbWEBModel.ResultUserDBTab.UserDBDetails[0].contactname != UserDbWEBModel.editDetails.txtContactName)
                                    {
                                        HttpCookie username = new HttpCookie("VCUserName");
                                        if (!Request.Url.AbsoluteUri.Contains("localhost"))
                                        {
                                            username.Domain = "vconnect.com";
                                            username.Path = "/";
                                        }
                                        if (username.Name == "VCUserName")
                                        {
                                            username.Value = "";
                                            username.Expires = DateTime.Now.AddDays(-1d);
                                            HttpContext.Response.Cookies.Add(username);
                                        }

                                        HttpCookie VCUserName = new HttpCookie("VCUserName", UserDbWEBModel.editDetails.txtContactName);

                                        if (!Request.Url.AbsoluteUri.Contains("localhost"))
                                        {
                                            VCUserName.Domain = "vconnect.com";
                                            VCUserName.Path = "/";
                                        }
                                        VCUserName.Expires = DateTime.Now.AddDays(3d);
                                        HttpContext.Response.Cookies.Add(VCUserName);
                                    }

                                }
                                //else if (result == 1)
                                //{
                                //    errorMsg = "Alternate phone no. is not valid.";
                                //}
                                else if (result == 2)
                                {
                                    errorMsg = "Enter a valid Email";
                                }
                                //else if (result == 3)
                                //{
                                //      errorMsg="Enter a valid Alternate Email.";
                                //}
                                else if (result == 4)
                                {
                                    errorMsg = "phone no. is not valid.";
                                }
                                else if (result == 5)
                                {
                                    errorMsg = "Number not valid! Please enter valid number.";
                                }
                            }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            return Content(errorMsg);

        }

        #endregion

        #region cropping

        public ActionResult SaveUploadedFile()
        {

            string userName = "";
            int newwidth = 0;
            int newheight = 0;
            Int64 userid = 0;
            string imagepath = string.Empty;
            //bool isSavedSuccessfully = true;
            bool FileSaved = false;
            if (Request.Cookies["VCUserContentID"] != null)
            {
                userName = Request.Cookies["VCUserName"].Value.ToString();
                userid = Convert.ToInt32(Request.Cookies["VCUserContentID"].Value.ToString());
            }

            string contacname = userName.ToString();
            string randomno = DateTime.Now.Ticks.ToString();
            try
            {
                foreach (string fileName in Request.Files)
                {

                    HttpPostedFileBase file = Request.Files[fileName];
                    string filename1 = file.FileName.ToString();
                    string ext = string.Empty;
                    if (filename1.LastIndexOf('.') > 0)
                    {
                        ext = filename1.Substring(filename1.LastIndexOf('.')).ToLower();
                    }
                    if (ext == ".jpg" || ext == ".jpeg" || ext == ".gif" || ext == ".bmp" || ext == ".png" || ext == ".tif" || ext == ".tiff")
                    {

                        string filename = contacname.Trim().Replace(" ", "-").Replace("&", "and").Replace("?", "-").Replace("%", "-").Replace(".", "-").Replace("'", "-").Replace("#", "-") + "_" + randomno.ToString();
                        filename = filename + "_1" + ext;
                        var fn1 = Server.MapPath("~/Resource/uploads/userphotos/original/" + filename);
                        (file).SaveAs(fn1);
                        FileSaved = true;
                        //Save file content goes here
                        if (FileSaved)
                        {

                            // pnlUpload.Visible = false;
                            //pnlCrop.Visible = true;
                            //divupload.Visible = false;
                            // string UploadFolder = "../../resource/uploads/UserPhotos/";
                            System.Drawing.Image uploadImage = System.Drawing.Image.FromFile(Server.MapPath("~/Resource/uploads/userphotos/original/" + filename));
                            float imageWidth = uploadImage.PhysicalDimension.Width;
                            float imageHeight = uploadImage.PhysicalDimension.Height;
                            if (imageWidth > imageHeight)
                            {
                                float ratio = imageWidth / imageHeight;
                                newwidth = Convert.ToInt16(imageWidth / ratio);
                                newheight = Convert.ToInt16(imageHeight / ratio);
                                if (newwidth > 250)
                                {
                                    newwidth = 250;
                                    ratio = imageWidth / newwidth;
                                    newheight = Convert.ToInt16(imageHeight / ratio);
                                }
                                else if (newheight < 100)
                                {
                                    newheight = 100;
                                    ratio = imageHeight / newheight;
                                    newwidth = Convert.ToInt16(imageWidth / ratio);
                                }
                                //float lowdim = imageHeight / 250;

                            }
                            else
                            {
                                float ratio = imageHeight / imageWidth;
                                newwidth = Convert.ToInt16(imageWidth / ratio);
                                newheight = Convert.ToInt16(imageHeight / ratio);
                                if (newheight > 250)
                                {
                                    newheight = 250;
                                    ratio = imageHeight / newheight;
                                    newwidth = Convert.ToInt16(imageWidth / ratio);
                                }
                                else if (newwidth < 100)
                                {
                                    newwidth = 100;
                                    ratio = imageWidth / newwidth;
                                    newheight = Convert.ToInt16(imageHeight / ratio);
                                }
                            }
                            ResizeImage(newwidth, newheight, Server.MapPath("~/Resource/uploads/userphotos/original/" + filename), Server.MapPath("~/Resource/uploads/userphotos/resize/" + filename));
                            try
                            {
                                CopyImage.Sync(webconfigSourceImagesRootPath + "userphotos\\original", webconfigDestinationImagesRootPath + "userphotos\\original");
                                CopyImage.Sync(webconfigSourceImagesRootPath + "userphotos\\resize", webconfigDestinationImagesRootPath + "userphotos\\resize");
                            }
                            catch (Exception ex)
                            {
                                log.LogMe(ex);
                            }

                            imagepath = filename;

                        }
                    }
                    else
                    {
                        return Json("Photo format is not valid.", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            return Json(imagepath, JsonRequestBehavior.AllowGet);
        }


        public JsonResult SaveDisplayCropImage(string imgurl, int? x, int? y, int? width, int? height)
        {
            UserDashboardWebModel userdashboard = new UserDashboardWebModel();
            string userName = string.Empty;
            Int64 userid = 0;
            //string imgName = imgurl.Substring(imgurl.LastIndexOf("\\"));
            string fileName = Path.GetFileName(imgurl);
            string filePath = Path.Combine(Server.MapPath("~/Resource/uploads/userphotos/resize/"), fileName);
            string cropFileName = "";
            string cropFilePath = "";
            string cropThumbFilePath = "";
            string thumb = string.Empty;
            string error = string.Empty;

            int result = 0;
            if (Request.Cookies["VCUserContentID"] != null)
            {
                userName = Request.Cookies["VCUserName"].Value.ToString();
                userid = Convert.ToInt32(Request.Cookies["VCUserContentID"].Value.ToString());
            }
            try
            {
                //if (Convert.ToInt32(width.Value) <= 250 || Convert.ToInt32(height.Value) <= 250)
                //{
                //    error = "Image must be at least 250px in width and height";
                //    return Json(error, JsonRequestBehavior.AllowGet);
                //}
                //else
                //{
                if (System.IO.File.Exists(filePath))
                {
                    System.Drawing.Image orgImg = System.Drawing.Image.FromFile(filePath);
                    Rectangle CropArea = new Rectangle(
                        Convert.ToInt32(x.Value),
                        Convert.ToInt32(y.Value),
                        Convert.ToInt32(width.Value),
                        Convert.ToInt32(height.Value));

                    Bitmap bitMap = new Bitmap(CropArea.Width, CropArea.Height);
                    using (Graphics g = Graphics.FromImage(bitMap))
                    {
                        g.DrawImage(orgImg, new Rectangle(0, 0, bitMap.Width, bitMap.Height), CropArea, GraphicsUnit.Pixel);
                    }
                    cropFileName = "crop_" + fileName;
                    cropFilePath = Path.Combine(Server.MapPath("~/Resource/uploads/userphotos/crop/"), cropFileName);
                    bitMap.Save(cropFilePath);
                    ResizeImage(100, 100, Server.MapPath("~/Resource/uploads/userphotos/crop/" + cropFileName), Server.MapPath("~/Resource/uploads/userphotos/thumb/" + cropFileName));

                    cropThumbFilePath = Path.Combine(Server.MapPath("~/Resource/uploads/userphotos/thumb/"), cropFileName);
                    thumb = "uploads/userphotos/thumb/" + cropFileName;
                    result = userdashboard.prc_cupdate_userphoto_opt(userid, thumb, userid);
                    try
                    {
                        CopyImage.Sync(webconfigSourceImagesRootPath + "userphotos\\crop", webconfigDestinationImagesRootPath + "userphotos\\crop");
                        CopyImage.Sync(webconfigSourceImagesRootPath + "userphotos\\thumb", webconfigDestinationImagesRootPath + "userphotos\\thumb");
                        string strVCUserPhoto = MyGlobalVariables.ImagesRootPath + "uploads/userphotos/thumb/" + cropFileName;

                        HttpCookie userphoto = new HttpCookie("VCUserPhoto");
                        if (!Request.Url.AbsoluteUri.Contains("localhost"))
                        {
                            userphoto.Domain = "vconnect.com";
                            userphoto.Path = "/";
                        }
                        if (userphoto.Name == "VCUserPhoto")
                        {

                            //  userphoto=Request.Cookies["VCUserPhotoMobile"];
                            userphoto.Value = "";
                            userphoto.Expires = DateTime.Now.AddDays(-1d);
                            HttpContext.Response.Cookies.Add(userphoto);
                        }

                        HttpCookie VCUserPhoto = new HttpCookie("VCUserPhoto", strVCUserPhoto);
                        if (!Request.Url.AbsoluteUri.Contains("localhost"))
                        {
                            VCUserPhoto.Domain = "vconnect.com";
                            VCUserPhoto.Path = "/";
                        }
                        VCUserPhoto.Expires = DateTime.Now.AddDays(3d);
                        HttpContext.Response.Cookies.Add(VCUserPhoto);


                    }
                    catch (Exception ex)
                    {
                        log.LogMe(ex);
                    }
                }
                return Json(cropFileName, JsonRequestBehavior.AllowGet);
            }
            //   }
            catch (Exception ex)
            {
                error = "File could not be uploaded.";
                log.LogMe(ex);
                return Json(error, JsonRequestBehavior.AllowGet);
            }
            // return Json(cropFileName, JsonRequestBehavior.AllowGet);
        }


        #region Image Optization
        public static void ResizeImage(int MaxWidth, int MaxHeight, string FileName, string NewFileName)
        {
            // load up the image, figure out a "best fit" resize, and then save that new image
            Bitmap OriginalBmp = (System.Drawing.Bitmap)System.Drawing.Image.FromFile(FileName).Clone();
            Size ResizedDimensions = GetDimensions(MaxWidth, MaxHeight, ref OriginalBmp);
            Bitmap NewBmp = new Bitmap(OriginalBmp, ResizedDimensions);
            System.Drawing.Imaging.ImageFormat imageFormat = OriginalBmp.RawFormat;
            NewBmp.Save(NewFileName, imageFormat);
            OriginalBmp.Dispose();
            OriginalBmp = null;
            NewBmp.Dispose();
            NewBmp = null;
        }

        public static Size GetDimensions(int MaxWidth, int MaxHeight, ref Bitmap Bmp)
        {
            int Width;
            int Height;
            float Multiplier;

            Height = Bmp.Height;
            Width = Bmp.Width;

            // this means you want to shrink an image that is already shrunken!
            if (Height <= MaxHeight && Width <= MaxWidth)
                return new Size(Width, Height);

            // check to see if we can shrink it width first
            Multiplier = (float)((float)MaxWidth / (float)Width);
            if ((Height * Multiplier) <= MaxHeight)
            {
                Height = (int)(Height * Multiplier);
                return new Size(MaxWidth, Height);
            }

            // if we can't get our max width, then use the max height
            Multiplier = (float)MaxHeight / (float)Height;
            Width = (int)(Width * Multiplier);

            return new Size(Width, MaxHeight);
        }
        #endregion

        #endregion



        #region change password

        [HttpGet]
        public ActionResult WebChangePassword()
        {
            UserDashboardWebModel UserDbWEBModel = new UserDashboardWebModel();
            Int32 userid = 0;
            if (Request.Cookies["VCUserContentID"] != null)
            {
                userid = Convert.ToInt32(Request.Cookies["VCUserContentID"].Value.ToString());
                UserDbWEBModel.UserId = userid;
            }
            else
            {
                return RedirectToAction("Login", "Account", new { rturl = "UserDashboardWeb/UserDetailsWeb" });
            }
            return View();
        }

        [HttpPost]
        public ActionResult WebChangePassword(UserDashboardWebModel UserDbWEBModel, FormCollection form)
        {

            Int32 userid = 0;
            string msg = string.Empty;
            try
            {
                if (Request.Cookies["VCUserContentID"] != null)
                {
                    userid = Convert.ToInt32(Request.Cookies["VCUserContentID"].Value.ToString());
                    UserDbWEBModel.UserId = userid;
                }
                int result = 0;

                string pwd = form["pwd"].ToString();
                string confirmpwd = form["confirmpwd"].ToString();
                // UserDashboardWebModel UserDbWEBModel = new UserDashboardWebModel();
                if (UserDbWEBModel.changePassword.OldPassword == null || UserDbWEBModel.changePassword.OldPassword == "")
                {
                    msg = "Please enter old password";
                    ViewBag.msg = "Please enter old password";
                }

                else
                    if (pwd == null || pwd == "")
                    {
                        msg = "Please enter new  password";
                        ViewBag.msg = "Please enter new  password";
                    }
                    else
                        if (confirmpwd == null || confirmpwd == "")
                        {
                            msg = "Plesae enter confirm password";
                            ViewBag.msg = "Plesae enter confirm password";
                        }
                if (pwd != null && UserDbWEBModel.changePassword.OldPassword != null && confirmpwd != null)
                {
                    if (pwd != confirmpwd)
                    {
                        msg = "New Password And Confirm Password must be equal";

                    }
                    else
                    {
                        result = UserDbWEBModel.WebChangePassword(UserDbWEBModel.changePassword, pwd);
                        if (result == 0)
                        {
                            msg = "Password Updated Successfully..Thankyou";
                        }
                        else if (result == 2)
                        {
                            msg = "Old Password does not match! try again.";
                        }
                        else
                        {
                            msg = "An error occurred. Please try again.";
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            return Content(msg);
        }

        #endregion

        #region validaion
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetProductByCategory(int? Stateid)
        {
            //return Json(Vconnect.Common.Utility.GetSelectListCity(Stateid.Value), JsonRequestBehavior.AllowGet);
            if (Stateid.HasValue)
            {
                return Json(Vconnect.Common.Utility.GetSelectListCity(Stateid.Value), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        //for client side phone and alternate phone number check
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult phonenumbercheck(string phonenumber)
        {
            int d = 4;
            if (!string.IsNullOrEmpty(phonenumber.Trim().TrimEnd().TrimStart()) && !string.IsNullOrWhiteSpace(phonenumber.Trim().TrimEnd().TrimStart()))
            {
                int result = Vconnect.Common.Utility.isValidPhone(phonenumber);

                string sContactNumber = phonenumber;
                string subSection = sContactNumber.Substring(0, 1);

                //if (subSection != "0")
                //{
                //    d = 3;
                //}
                //else if (result == 0)
                //{
                //    d = 2;
                //}
                if (result == 0)
                {
                    d = 2;
                }
                else
                {
                    d = 4;
                }
            }
            else
            {
                d = 4;
            }

            return Json(d, JsonRequestBehavior.AllowGet);
        }


        //for client side email and alternate email check
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult emailcheck(string email)
        {
            int d = 5;
            Regex rgx = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
            if (!string.IsNullOrEmpty(email.Trim().TrimEnd().TrimStart()) && !string.IsNullOrWhiteSpace(email.Trim().TrimEnd().TrimStart()))
            {
                if (!rgx.IsMatch(email))
                {
                    d = 1;
                }
            }
            else
            {
                d = 5;
            }
            return Json(d, JsonRequestBehavior.AllowGet);
        }
        #endregion

        
        #region Get all the Users Infomation
        //public JsonResult getUserDetails(Int64 userid)
        //{
        //    UserDashboardWebModel UserDbWEBModel = new UserDashboardWebModel();            
        //    UserInfo userInfo = new UserInfo();
        //    userInfo=UserDbWEBModel.getUserInfo(userid);
        //    return Json(userInfo,JsonRequestBehavior.AllowGet);
        //}
        public ActionResult getUserDetails(Int64 userid)
        {
            UserDashboardWebModel UserDbWEBModel = new UserDashboardWebModel();
            UserInfo userInfo = new UserInfo();            
            userInfo = UserDbWEBModel.getUserInfo(userid);            
            //return Json(userInfo, JsonRequestBehavior.AllowGet);
            return View("UserProfile", userInfo);
        }
        #endregion
        #region partial
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetWebUserDashboard(UserDashboardWebModel UserDashboardWeb, int num=1)
        {
            if ((!Request.AcceptTypes.Contains("text/html")))
            {
                Int32 userid = 0;

                if (UserDashboardWeb == null) UserDashboardWeb = new UserDashboardWebModel();

                if (Request.Cookies["VCUserContentID"] != null)
                {
                    userid = Convert.ToInt32(Request.Cookies["VCUserContentID"].Value.ToString());
                    UserDashboardWeb.UserId = userid;
                }

                UserDashboardWeb.getWebUserDashboardDetails();
                EditDetails editdetail = new EditDetails();
                List<SelectListItem> listStatesNames = new List<SelectListItem>();
                editdetail.StateOptions = Utility.GetSelectList();
                if (UserDashboardWeb.ResultUserDBTab != null)
                {
                    if (UserDashboardWeb.ResultUserDBTab.UserDBDetails != null && UserDashboardWeb.ResultUserDBTab.UserDBDetails.Count > 0)
                    {
                        editdetail.CityOptions = Utility.GetSelectListCity(((UserDashboardWeb.ResultUserDBTab.UserDBDetails[0].stateid != null && UserDashboardWeb.ResultUserDBTab.UserDBDetails[0].stateid.ToString().Trim() != "") ? Convert.ToInt32(UserDashboardWeb.ResultUserDBTab.UserDBDetails[0].stateid) : 0));
                    }
                }
                else
                {
                    editdetail.CityOptions = Utility.GetSelectListCity(0);
                }

                JsonModel jsonModel = new JsonModel();
                string viewName = "";
                switch (num)
                {
                    case 1:

                        viewName = "_WebUserDashBoard";
                        UserDashboardWeb.WebUserDashboardDetails();


                        break;

                    case 2:

                        viewName = "_WebChangePassword";

                        break;

                    case 3:

                        viewName = "_WebMyWatchList";
                        UserDashboardWeb.WebGetWatchlist();

                        break;

                    case 4:

                        viewName = "_WebKeywordNotFound";
                        UserDashboardWeb.WebUserDashboardDetails();

                        break;
                    case 5:

                        viewName = "_WebRequestPosted";
                        UserDashboardWeb.WebGetUserReqPosted();

                        break;
                    case 6:

                        viewName = "_WebReviewsPosted";
                        UserDashboardWeb.WebGetUserReviewPosted();

                        break;
                    case 7:

                        viewName = "UploadImage";


                        break;
                    case 8:

                        viewName = "_WebEditDetails";
                        if (UserDashboardWeb.ResultUserDBTab.UserDBDetails[0].stateid != 0)
                        {
                            editdetail.StateID = Convert.ToInt64(UserDashboardWeb.ResultUserDBTab.UserDBDetails[0].stateid);
                            editdetail.CityID = Convert.ToInt64(UserDashboardWeb.ResultUserDBTab.UserDBDetails[0].cityid);
                        }
                        else
                        {
                            editdetail.StateID = 0;
                            editdetail.CityID = 0;
                        }
                        editdetail.txtContactName = UserDashboardWeb.ResultUserDBTab.UserDBDetails[0].contactname;
                        UserDashboardWeb.editDetails = editdetail;
                        //UserDashboardWeb.getWebUserDashboardDetails();
                        break;
                }
                ViewBag.userdb = UserDashboardWeb;
                jsonModel.HTMLString = RenderPartialViewToString(viewName, UserDashboardWeb);
                //return Json(jsonModel);
                return Json(jsonModel,
                    JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }


        protected string RenderPartialViewToString(string viewName, object model)
        {
            // controller.ViewData.Model = model;

            try
            {
                if (string.IsNullOrWhiteSpace(viewName))
                    viewName = ControllerContext.RouteData.GetRequiredString("action");
                ViewData.Model = model;
                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                    ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                    viewResult.View.Render(viewContext, sw);

                    return sw.GetStringBuilder().ToString();
                }
            }
            catch (Exception e)
            {

                ViewData["IsError"] = "y";
                return "";
            }
        }

        #endregion
    }
}

