﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Vconnect.Models;
using Vconnect.Mapping.ListingWEB;
using Vconnect.Mapping;
using System.Data;
using Vconnect.Common;
using Vconnect.Enums;



namespace Vconnect.Controllers
{
    public class CategorylistController : Controller
    {
        //
        // GET: /Categorylist/
        UserSession objUS = new UserSession();
        public ActionResult Index()
        {
            return View();
        }


        //public ActionResult ItemlistactionWAP(int id, string loc,string Keyword)
        //{
        //    //PYRModel pyrModel = new PYRModel();
        //    //if (Keyword != null)
        //    //{
        //    //    pyrModel.SearchText = Keyword;
        //    //}
        //    //if (loc != null)
        //    //{                
        //    //    pyrModel.SearchLocation = string.IsNullOrEmpty(loc) ? "lagos" : loc;
        //    //}
        //    //pyrModel.SearchLocation = string.IsNullOrEmpty(pyrModel.SearchLocation) ? "lagos" : pyrModel.SearchLocation;

        //    CategoryListWapModel objCategoryList = new CategoryListWapModel();
        //    objCategoryList.fillbannerResult();
        //    SearchBar searchBar = new SearchBar();
        //    searchBar.SearchLocation = string.IsNullOrEmpty(loc) ? "lagos" : loc;
        //    searchBar.SearchText = Keyword;
        //    //Keyword = pyrModel.SearchText;
        //    //loc = pyrModel.SearchLocation;
        //    //objCategoryList.pyrModel = pyrModel;
        //    objCategoryList.searchBar = searchBar;
        //    ItemlistactionWAP(objCategoryList, id, loc);
        //    if (objCategoryList.CategoryList.Count > 0)
        //    {
        //        return View();
        //    }
        //    else
        //    {
        //        //pyrModel.SearchText = Keyword;
        //        //pyrModel.SearchLocation = loc;
        //        string url = RoutingUtility.GetSearchWebURLNew(loc,Keyword);

        //        return Redirect(url);
        //    }
        //}

        //[HttpPost]
        //[AllowAnonymous]
        //public ActionResult ItemlistactionWAP(CategoryListWapModel objCategoryList, int id, string location)
        //{

        //    List<CategoryList> ItemListModel = new List<CategoryList>();

        //    //Retreive all the Data base on the Category ID

        //    using (var db = new VconnectDBContext29())
        //    {
        //        // Create a SQL command to execute the sproc
        //        var cmd = db.Database.Connection.CreateCommand();
        //        cmd.CommandText = "[dbo].[prc_get_itemlistnew_opt]";
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        //Add parameters to command object                
        //        cmd.Parameters.Add(new SqlParameter("@parentid", id));
        //        cmd.Parameters.Add(new SqlParameter("@location", location));
        //        try
        //        {
        //            var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
        //            connection.Open();
        //            var reader = cmd.ExecuteReader();
        //            ItemListModel = ((IObjectContextAdapter)db).ObjectContext.Translate<CategoryList>(reader).ToList();

        //            objCategoryList.CategoryList = ItemListModel;

        //        }
        //        finally
        //        {
        //            db.Database.Connection.Close();
        //        }
        //    }

        //    return View("ItemListactionWAP", objCategoryList);

        //}

        public ActionResult Itemlistaction(int id, string keyword, string loc)
        {

            ViewBag.id = id;
            // PYRModel pyrModel = new PYRModel();
            CategoryListWebModel objCategoryList = new CategoryListWebModel();
            if (keyword != null)
            {
                // pyrModel.SearchText = keyword;
            }

            if (loc != null)
            {
                //pyrModel.SearchLocation = loc;
            }
            //SearchBarWebModel searchBarWebModel = new SearchBarWebModel();
            objCategoryList.SearchLocation = string.IsNullOrEmpty(loc) ? "lagos" : loc;
            objCategoryList.SearchText = keyword;
            objCategoryList.StateOptions = Utility.GetSelectList();
            //keyword = pyrModel.SearchText;
            //loc = pyrModel.SearchLocation;
            //objCategoryList.pyrModel = pyrModel;
            //objCategoryList.searchBarWebModel = searchBarWebModel;           
            if (System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("/all_categories") != -1)
            {
                if (id == 0)
                {
                    ItemlistactionWEB(objCategoryList, id, loc);


                    if (objCategoryList.CategoryList.Count > 0)
                    {

                        return View("ItemlistactionNew");
                    }
                    else
                    {
                        string url = RoutingUtility.GetSearchWebURLNew(loc, keyword);
                        return Redirect(url);
                    }
                }
                else
                {
                    return Redirect("/searchnotfound");
                }

            }
            else
            {

                ItemlistactionWEB(objCategoryList, id, loc);


                if (objCategoryList.CategoryList.Count > 0)
                {

                    return View("ItemlistactionNew");
                }
                else
                {
                    string url = RoutingUtility.GetSearchWebURLNew(loc, keyword);
                    return Redirect(url);
                }
            }


        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult ItemlistactionWEB(CategoryListWebModel objCategoryList, int? id, string location)
        {
            List<CategoryList> ItemListModel = new List<CategoryList>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = id > 0 ? "[dbo].prc_get_itemlistnewbeta" : "[dbo].[prc_get_itemlistnew]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@parentid", id));
                if (System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("/list-of-") == -1)
                {
                    cmd.CommandText = "prc_get_itemlistnew";
                    cmd.Parameters.Add(new SqlParameter("@location", location));
                }
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    ItemListModel = ((IObjectContextAdapter)db).ObjectContext.Translate<CategoryList>(reader).ToList();

                    objCategoryList.CategoryList = ItemListModel;

                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }

            return View("ItemListactionWAP", objCategoryList);
            //List<CategoryList> ItemListModel = new List<CategoryList>();
            //using (var db = new VconnectDBContext29())
            //{                
            //    var cmd = db.Database.Connection.CreateCommand();
            //    cmd.CommandText = "[dbo].[prc_get_itemlist]";
            //    cmd.CommandType = System.Data.CommandType.StoredProcedure;                   
            //    cmd.Parameters.Add(new SqlParameter("@parentid", id));
            //    try
            //    {
            //        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
            //        connection.Open();
            //        var reader = cmd.ExecuteReader();
            //        ItemListModel = ((IObjectContextAdapter)db).ObjectContext.Translate<CategoryList>(reader).ToList();
            //        objCategoryList.CategoryList = ItemListModel;                    
            //    }
            //    finally
            //    {
            //        db.Database.Connection.Close();
            //    }
            //}            
            //return View("ItemlistactionWEB", objCategoryList);
        }



    }
}
