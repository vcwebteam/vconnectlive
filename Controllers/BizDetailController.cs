﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vconnect.Models;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using Vconnect.Common;
using System.IO;
using System.Text;
using Vconnect.Enums;
using System.Web.UI;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Configuration;
using System.Text.RegularExpressions;

namespace Vconnect.Controllers
{
    public class BizDetailController : Controller
    {
        UserSession objUS = new UserSession();
        public BizDetailController()
        { 

        }
        //
        // GET: /BizDetail/
        public string webconfigSourceImagesRootPath = ConfigurationManager.AppSettings["SourceImagesRootPath"].ToString();
        public string webconfigDestinationImagesRootPath = ConfigurationManager.AppSettings["DestinationImagesRootPath"].ToString();
        public static Int32 BusinessId = 0;
        //
        // GET: /BusinessDetailWEB/
        

        #region Ankita_WAP

      
        #endregion

        public ActionResult BusinessPhoneNumbers(long? bizID)
        {
            if (bizID.HasValue)//  && Request.ContentType.IndexOf("json") > 0)
            {
                BizDetailWEB bizDetailWEB = new BizDetailWEB();
                var data = bizDetailWEB.GetPhone(bizID.Value);
                data.phone = !string.IsNullOrEmpty(data.phone) ? GetPhoneinBinary(data.phone, "phone") : "";
                data.altenatephone = !string.IsNullOrEmpty(data.altenatephone) ? GetPhoneinBinary(data.altenatephone, "alternatephone") : "";

                //RedirectToAction("BusinessDetail", "BizDetailWEB"); 
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult BussinesImage(long? bizID)
        {
            if (bizID.HasValue)//  && Request.ContentType.IndexOf("json") > 0)
            {
                BizDetailWEB bizDetailWEB = new BizDetailWEB();
                var data = bizDetailWEB.GetPhotoGallery(bizID.Value);
                string Html=string.Empty;
                string module ="photogallery_pg";
                if (data != null && data.Count > 0)
                {
                    Html += "<div class='images-box clearfix'>";
                    Html += "    <ul class='medium-block-grid-4'>";
                    if (data.Count>= 3) {
                         for (var i = 0; i < 3; i++) {
                             Html += "<li>  <a class='th' data-lightbox='biz-photos' href='"+Vconnect.Common.MyGlobalVariables.ImagesRootPath + data[i].photosmax + "'> <img src='" +Vconnect.Common.MyGlobalVariables.ImagesRootPath + data[i].photothumbs + "' alt='"+data[i].businessname+"'/></a></li> ";
                         }
                         if (data.Count > 3) {
                             Html += "<li class='see-more'><a href='"+ module +data[0].businessid+"'><i class='icon-camera'></i><br>See all (" + data.Count + ")</a></li>";
                         }
                         for (var i = 3; i < data.Count; i++) {
                             Html += "<li style='display:none'>  <a class='th' data-lightbox='biz-photos' href='"+Vconnect.Common.MyGlobalVariables.ImagesRootPath + data[i].photosmax + "'> <img src='"+Vconnect.Common.MyGlobalVariables.ImagesRootPath + data[i].photothumbs + "' alt='"+data[i].businessname+"'/></a></li> ";
                         }
                     }
                     else {
                         for (var i = 0; i < data.Count; i++) {
                             Html += "    <li><a class='th' data-lightbox='biz-photos' href='"+Vconnect.Common.MyGlobalVariables.ImagesRootPath + data[i].photosmax + "'><img src='"+Vconnect.Common.MyGlobalVariables.ImagesRootPath + data[i].photothumbs + "' alt='"+data[i].businessname+"'/></a></li> ";
                         }
                     }
                     Html += "    </ul>";
                     Html += "</div>";
                }
                return Json(Html, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult BusinessClaim(long? bizID)
        {
            if (bizID.HasValue)//  && Request.ContentType.IndexOf("json") > 0)
            {
                BizDetailWEB bizDetailWEB = new BizDetailWEB();
                var data = bizDetailWEB.GetBusinessClaim(bizID.Value);
                //RedirectToAction("BusinessDetail", "BizDetailWEB"); 
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

    
        public ActionResult BussinesAttachment(long? bizID)
        {

            if (bizID.HasValue)// && Request.ContentType.IndexOf("json") > 0)
            {
                BizDetailWEB bizDetailWEB = new BizDetailWEB();
                var data = bizDetailWEB.GetAttachmentGallery(bizID.Value);
                //RedirectToAction("BusinessDetail", "BizDetailWEB"); 
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        protected string RenderPartialViewToString(string viewName, object model)
        {
            // controller.ViewData.Model = model;

            try
            {
                if (string.IsNullOrWhiteSpace(viewName))
                    viewName = ControllerContext.RouteData.GetRequiredString("action");
                ViewData.Model = model;
                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                    ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                    viewResult.View.Render(viewContext, sw);

                    return sw.GetStringBuilder().ToString();
                }
            }
            catch (Exception e)
            {

                ViewData["IsError"] = "y";
                return "";
            }
        }
        public static string RenderPartialToString(Controller controller, string partialViewName, object model, ViewDataDictionary viewData, TempDataDictionary tempData)
        {
            ViewEngineResult result = ViewEngines.Engines.FindPartialView(controller.ControllerContext, partialViewName);

            if (result.View != null)
            {
                controller.ViewData.Model = model;
                StringBuilder sb = new StringBuilder();
                using (StringWriter sw = new StringWriter(sb))
                {
                    using (HtmlTextWriter output = new HtmlTextWriter(sw))
                    {
                        ViewContext viewContext = new ViewContext(controller.ControllerContext, result.View, viewData, tempData, output);
                        result.View.Render(viewContext, output);
                    }
                }

                return sb.ToString();
            }

            return string.Empty;
        }
        [HttpGet]
        public ActionResult LoadLoginPartial(int? ID)
        {
            JsonModel jsonModel = new JsonModel();
            string viewName = string.Empty;
            viewName = "_LoginStaticPopup";

            string str = RenderPartialToString(this, "_LoginStaticPopup", null, ViewData, TempData);
            jsonModel.HTMLString = str;
            return Json(jsonModel,
              JsonRequestBehavior.AllowGet);
        }
        public ActionResult Map(int bizId)
        {
            BizDetailWEB bizDetailWEB = new BizDetailWEB();
            BizDetails bizDetails = bizDetailWEB.GetBizDetails(bizId);
            return View(bizDetails.BizMaster.FirstOrDefault());

        }
        

        public string urlRedirectionBizDetail()
        {
            string WebsiteRawPath = ConfigurationManager.AppSettings["WebsiteRawPath"].ToString();
            //SEO ISSUE Duplicacy issue      --http://localhost:1202/code/nigeria/businesses-from-power-up-limited-ikeja-lagos_b250
            if (System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("businesses-from-") != -1 && System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("_b") != -1)
            {
                int istartLength = 0;
                string newUrl = System.Web.HttpContext.Current.Request.RawUrl.ToLower();
                istartLength = newUrl.ToLower().IndexOf("businesses-from-");
                newUrl = (newUrl.Substring(istartLength).ToString());
                return WebsiteRawPath + newUrl.Replace("businesses-from-", "/");


            }
            //www.vconnect.com/business/united-bank-for-africa-plc-lagos_island-lagos_b146757
            else if (System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("business/") != -1 && System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("_b") != -1)
            {
                string newUrl = System.Web.HttpContext.Current.Request.RawUrl.ToLower();
                return WebsiteRawPath + newUrl.Replace("business/", "");


            }
            else
            {
                return string.Empty;
            }
        }
        public JsonResult updatebusinessreviews(Int64? reviewId, Int64? userId)
        {
            if (reviewId.HasValue && userId.HasValue)// && Request.ContentType.IndexOf("json") > 0)
            {
                BizDetailWEB bizDetailWEB = new BizDetailWEB();
                int result = bizDetailWEB.updatebusinessreviews(reviewId.Value, userId.Value);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult updatebusinessphoto(Int64? photoId, Int64? userId)
        {
            if (photoId.HasValue && userId.HasValue)// && Request.ContentType.IndexOf("json") > 0)
            {
                BizDetailWEB bizDetailWEB = new BizDetailWEB();
                int result = bizDetailWEB.updatebusinessphoto(photoId.Value, userId.Value);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        

        
        
        //[HttpPost]
        //public ActionResult BizDetail(FormCollection collection)
        //{
        //    long  bizID;
        //    bizID = Convert.ToInt64(collection["businessId"]);
        //    bizID = (bizID == null) ? 333251 : bizID;

        //    BizDetailWEB bizDetailWEB = new BizDetailWEB();
        //    BizDetails bizDetails = bizDetailWEB.GetBizDetails(bizID);
        //    return View(bizDetails);
        //}
       
       

        [ValidateInput(false)]
        public void BizDetailrv(Int32 bizID, string businessname)
        {

            BusinessId = bizID;


            try
            {
                //long bizID;
               

                string newUrl = urlRedirectionBizDetail();
                if (newUrl != "")
                {
                    Response.Clear(); Response.Status = "301 Moved Permanently";
                    Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();
                }
                string returnVal = string.Empty;
                BizDetailWEB bizDetailWEB = new BizDetailWEB();
                string str = string.Empty;
                Int64 userID = 0;
                if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                {
                    userID = Convert.ToInt64(objUS.SV_VCUserContentID.ToString());
                }
                BizDetails bizDetails = bizDetailWEB.GetBizDetails(bizID, userID);

                if (bizDetails == null || bizDetails.BizMaster == null || bizDetails.BizMaster.Count == 0)
                {
                    Response.Redirect("/");
                }
                else if (businessname != null &&  (businessname.ToString().Trim().Contains("videogallery")  || businessname.ToString().Trim().Contains("viewallreviews")))
                {

                    businessname = bizDetails.BizMaster.FirstOrDefault().businessname.Replace(" ", "-");
                    businessname = businessname + "-" + bizDetails.BizMaster.FirstOrDefault().city.Replace(" ", "_");
                    businessname = businessname + "-" + bizDetails.BizMaster.FirstOrDefault().state.Replace(" ", "-") + "_b" + bizID;
                    //Response.Clear(); Response.Status = "301 Moved Permanently";
                    //Response.AddHeader("Location", businessname.Trim().ToLower()); Response.End();
                    Response.RedirectPermanent("~/" + businessname);

                }

            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }

        }

        protected string get12HrFormat(decimal time)
        {
            string result;
            if (time - 12 >= 0)
            {
                result = (((time - 13) >= 0) ? (time - 12) : time).ToString() + "pm";
            }
            else
            {
                result = (time).ToString() + "am";
            }
            return result;
        }

        protected List<Workinghours> workinghours(string workinghours)
        {
            List<Workinghours> workHours = new List<Workinghours>();
            decimal[,] timeList = new decimal[2, 7];
            string[] workingHours = workinghours.ToString().Split(',');
            string[] dayListLong = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
            string[] partition;
            string[] days, times;
            int start, end, lim;
            string[] dayList = { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };
            Regex rgx = new Regex(@"\d+\.\d+");
            foreach (string item in workingHours) //Mon-Sat:7.00-22.00
            {
                if (!string.IsNullOrEmpty(item.Trim()))
                {
                    partition = item.ToString().Split(':');
                    days = partition[0].Split('-'); // {"Mon", "Sat"}
                    days[0] = days[0].Substring(0, 3);
                    for (start = 0; start <= 6; start++)
                    {
                        if (days[0].CompareTo(dayList[start]) == 0)
                        {
                            break;
                        }
                    }
                    end = 7;
                    if (days.Length > 1)
                    {
                        days[1] = days[1].Substring(0, 3);
                        for (end = 0; end <= 6; end++)
                        {
                            if (days[1].CompareTo(dayList[end]) == 0)
                            {
                                break;
                            }
                        }
                    }
                    lim = ((end != 7) ? end : start);
                    if (rgx.IsMatch(partition[1]) == true)
                    {
                        times = partition[1].Split('-');    // {"7.00", "22.00"}
                        for (int i = start; i <= lim; i++)
                        {
                            timeList[0, i] = Convert.ToDecimal(times[0]);
                            timeList[1, i] = Convert.ToDecimal(times[1]);
                            // bizDetails.WorkingHours.Add(new Workinghours(){weekday = timeList[0, i].ToString(),workinghours = timeList[1, i].ToString() });                                        
                            //workHours.Add(new Workinghours() { weekday = dayListLong[i], workinghours = timeList[0, i].ToString() + '-' + timeList[1, i].ToString() });
                        }
                    }
                }
            }
            for (int i = 0; i <= 6; i++)
            {
                if (timeList[0, i] == timeList[1, i])
                {
                    workHours.Add(new Workinghours() { weekday = dayListLong[i], workinghours = "Closed" });
                }
                else
                {
                    workHours.Add(new Workinghours() { weekday = dayListLong[i], workinghours = get12HrFormat(timeList[0, i]) + '-' + get12HrFormat(timeList[1, i]) });
                }
            }
            return workHours;
        }
       
        [ValidateInput(false)]
        public ActionResult BizDetail(Int32? bizID, string businessname, string method)
        {
            if (bizID.HasValue)
            {
                BusinessId = bizID.Value;
                try
                {
                    //long bizID;
                    if (Request.Url.AbsoluteUri.ToString() != Request.Url.AbsoluteUri.ToString().ToLower())
                    {
                        if (Request.Url.AbsoluteUri.ToString().IndexOf("?") != -1)
                        {
                            if (Request.Url.AbsoluteUri.ToString().Substring(0, Request.Url.AbsoluteUri.ToString().IndexOf("?") - 1) != Request.Url.AbsoluteUri.ToString().Substring(0, Request.Url.AbsoluteUri.ToString().IndexOf("?") - 1).ToLower())
                            {
                                Response.Clear(); Response.Status = "301 Moved Permanently";
                                Response.AddHeader("Location", Request.Url.AbsoluteUri.ToString().ToLower()); Response.End();
                                //return new HttpStatusCodeResult(301, "301 Moved Permanently");
                                return null;
                            }
                        }
                        else
                        {
                            Response.Clear(); Response.Status = "301 Moved Permanently";
                            Response.AddHeader("Location", Request.Url.AbsoluteUri.ToString().ToLower()); Response.End();
                            //return new HttpStatusCodeResult(301, "301 Moved Permanently");
                            return null;
                        }
                    }

                    string newUrl = urlRedirectionBizDetail();
                    if (newUrl != "")
                    {
                        Response.Clear(); Response.Status = "301 Moved Permanently";
                        Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();
                    }
                    string returnVal = string.Empty;
                    BizDetailWEB bizDetailWEB = new BizDetailWEB();
                    string str = string.Empty;
                    Int32 userid = 0;
                    //if (Request.Url.AbsoluteUri.ToString().IndexOf("returnval") != -1 && Request.Url.AbsoluteUri.ToString().IndexOf("bizid") != -1)
                    if (Request.Url.AbsoluteUri.ToString().IndexOf("returnval") != -1 || Request.QueryString["returnVal"] != null)
                    {
                        //Int64 bizId = Convert.ToInt64(Request.QueryString["bizid"].ToString());
                        SearchListWEBController searchListWEBController = new SearchListWEBController();
                        // string bname = Request.QueryString["bname"].ToString();
                        if (Request.QueryString["returnval"].ToString().ToLower() == "save" || Request.QueryString["returnval"].ToString().ToLower() == "2")
                        {
                            //str = searchListWEBController.Favourite(bizId);
                            userid = (!string.IsNullOrEmpty(objUS.SV_VCUserContentID)) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                            ViewBag.returnVal = "save";

                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "like" || Request.QueryString["returnval"].ToString().ToLower() == "1")
                        {
                            //str = searchListWEBController.Like(bizId);
                            userid = (!string.IsNullOrEmpty(objUS.SV_VCUserContentID)) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                            ViewBag.returnVal = "like";
                        }
                        //else if (Request.QueryString["returnval"].ToString().ToLower() == "rate" || Request.QueryString["returnval"].ToString().ToLower() == "3")
                        //{
                        //    ViewBag.returnVal = "rate";
                        //    Int16 rate = 0;
                        //    if (!string.IsNullOrEmpty(Request.QueryString["rate"].ToString().ToLower()))
                        //    {
                        //        rate = Convert.ToInt16(Request.QueryString["rate"].ToString());
                        //    }
                        //    if (Request != null && Request.Cookies != null && !string.IsNullOrEmpty(objUS.SV_VCLoginID))
                        //    {
                        //        userid = (!string.IsNullOrEmpty(objUS.SV_VCUserContentID)) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                        //    }
                        //    userid = (!string.IsNullOrEmpty(objUS.SV_VCUserContentID)) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                        //    if (userid == 0)
                        //    {
                        //        return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                        //    }
                        //    bizDetailWEB.addbusinessRate(Convert.ToInt32(bizID), userid, rate);
                        //    str = "rate";
                        //}
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "claim" || Request.QueryString["returnval"].ToString().ToLower() == "4")
                        {
                            ViewBag.returnVal = "claim";
                            return Redirect("claimbusiness?bid=" + bizID);
                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "review" || Request.QueryString["returnval"].ToString().ToLower() == "5")
                        {
                            ViewBag.returnVal = "review";
                            userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                            //Int32 reviewid = Convert.ToInt32(Request.QueryString["reviewid"].ToString());
                            //int result = bizDetailWEB.updatebusinessreviews(reviewid, userid);
                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "ishelpfull" || Request.QueryString["returnval"].ToString().ToLower() == "7" || Request.QueryString["returnval"].ToString().ToLower() == "8")
                        {
                            ViewBag.returnVal = "ishelpfull";
                            userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                            //Int32 reviewid = Convert.ToInt32(Request.QueryString["reviewid"].ToString());
                            //Int32 ishelpfull = Convert.ToInt32(Request.QueryString["ishelpfull"].ToString());
                            //bizDetailWEB.IsHelpFull(bizId, reviewid, userid, ishelpfull);
                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "follow" || Request.QueryString["returnval"].ToString().ToLower() == "6")
                        {
                            ViewBag.returnVal = "follow";

                            userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                            //Int32 reviewid = Convert.ToInt32(Request.QueryString["reviewid"].ToString());
                            //int reviewpostedby = Convert.ToInt32(Request.QueryString["reviewpostedby"].ToString());
                            //int? status = 1;
                            // int follow = bizDetailWEB.Follow(bizId, reviewid, userid, status, reviewpostedby);
                            ViewBag.follow = "follow";
                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "photo" || Request.QueryString["returnval"].ToString().ToLower() == "9")
                        {
                            ViewBag.returnVal = "photo";

                            userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                            //Int32 photoid = Convert.ToInt32(Request.QueryString["photoid"].ToString());
                            //int reviewpostedby = Convert.ToInt32(Request.QueryString["reviewpostedby"].ToString());
                            //int? status = 1;
                            //int follow = bizDetailWEB.Follow(bizId, reviewid, userid, status, reviewpostedby);
                            ViewBag.follow = "photo";
                        }

                        //   "ClaimBusiness/CYBReportIncorrectComments?bid=@Model.BizMaster.FirstOrDefault().businessid&bname=@Model.BizMaster.FirstOrDefault().businessname";
                    }
                    Int64 userID = 0;
                    if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                    {
                        userID = Convert.ToInt64(objUS.SV_VCUserContentID.ToString());
                    }
                    BizDetails bizDetails = bizDetailWEB.GetBizDetails(bizID.Value, userID);


                    if (bizDetails == null || bizDetails.BizMaster == null || bizDetails.BizMaster.Count == 0)
                    {
                        if (bizDetails.BusinessURL != null && !string.IsNullOrEmpty(bizDetails.BusinessURL.url))
                        {
                            return Redirect(ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + bizDetails.BusinessURL.url);
                        }
                        //Response.Clear(); Response.Status = "301 Moved Permanently";
                        //Response.AddHeader("Location", "http://www.vconnect.com/"); Response.End();
                        //return new HttpStatusCodeResult(301, "301 Moved Permanently");
                        Response.Clear(); Response.StatusCode = 404;
                        Response.Status = "404 NotFound";
                        return View("../Static/404");
                    }
                    //RedirectToAction("BusinessDetail", "BizDetailWEB");
                    if (!string.IsNullOrEmpty(bizDetails.BizMaster.FirstOrDefault().workinghours))
                    {
                        try
                        {
                            bizDetails.WorkingHours = workinghours(bizDetails.BizMaster.FirstOrDefault().workinghours.ToString());//workHours;
                            //RedirectToAction("BusinessDetail", "BizDetailWEB");
                            if (!string.IsNullOrEmpty(bizDetails.BizMaster.FirstOrDefault().workinghours))
                            {
                                bizDetails.WorkingHours = workinghours(bizDetails.BizMaster.FirstOrDefault().workinghours.ToString());//workHours;
                            }
                        }
                        catch { }
                     }
                    bizDetails.BizMaster.FirstOrDefault().phoneBinary = !string.IsNullOrEmpty(bizDetails.BizMaster.FirstOrDefault().phone) ? GetPhoneinBinary(bizDetails.BizMaster.FirstOrDefault().phone, "phone") : "";
                    bizDetails.BizMaster.FirstOrDefault().alternatePhoneBinary = !string.IsNullOrEmpty(bizDetails.BizMaster.FirstOrDefault().alternatephone) ? GetPhoneinBinary(bizDetails.BizMaster.FirstOrDefault().alternatephone, "alternatephone") : "";

                    bizDetails.SearchLocation = "Lagos";
                    bizDetails.StateOptions = Utility.GetSelectList();

                   // AddBusinessLog(bizID.Value, businessname);
                    return View("BizDetail", bizDetails);
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
                return View();
            }
            else
            {
                return Redirect("/");
            }

        }
        //public ActionResult BusinessDetail()
        //{
        //    BizDetailWEB bizDetailWEB = new BizDetailWEB();
        //    BizDetails bizDetails = bizDetailWEB.GetBizDetails(54);
        //    return View(bizDetails);
        //}
        //[HttpPost]
        //public ActionResult BusinessDetail(long bizID)
        //{
        //    //bizID = (bizID == null) ? 333251 : bizID;
        //    BizDetailWEB bizDetailWEB = new BizDetailWEB();
        //    BizDetails bizDetails = bizDetailWEB.GetBizDetails(bizID);
        //    return View(bizDetails);
        //}
        /// <summary>
        /// Function to display the review on page load
        /// </summary>
        /// <param name="bizID">Bussiness Id</param>
        /// <returns></returns>
        /// 
        public ActionResult BussinesReview(long? bizID)
        {
            Int64 userID = 0;
            if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
            {
                userID = Convert.ToInt64(objUS.SV_VCUserContentID.ToString());
            }
            if (bizID.HasValue)// && Request.ContentType.IndexOf("json") > 0)
            {
                BizDetailWEB bizDetailWEB = new BizDetailWEB();
                var data = bizDetailWEB.GetBizReviewDetails(bizID.Value,userID, 1, 2).businessReview;
                //RedirectToAction("BusinessDetail", "BizDetailWEB"); 
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult RelatedBiz(int? bizid)
        {
            //if (bizid.HasValue)// && Request.ContentType.IndexOf("json") > 0)
            if (bizid.HasValue && (!Request.AcceptTypes.Contains("text/html")))// && Request.ContentType.IndexOf("json") > 0)
            {
                BizDetailWEB bizDetailWEB = new BizDetailWEB();
                var similar = bizDetailWEB.GetRelatedBiz(bizid);
                var result = similar.Select(m => new { bizname = m.businessname, bizid = m.contentid, rate = m.avgrating, businessurl = m.businessurl, businessLogo = m.companylogo });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            { 
                return Json(null, JsonRequestBehavior.AllowGet); 
            }
        }
        [HttpGet]
        public ActionResult GetDirection(long? bizid)
        {
            if (bizid.HasValue)
            {
                return RedirectToActionPermanent("GetDirection","Detail",new {bizid=bizid});
                BizDetailWEB bizDetailWEB = new BizDetailWEB();
                BizDetails bizDetails = bizDetailWEB.GetBizDetails(bizid.Value);
                if (bizDetails.BizMaster != null && bizDetails.BizMaster.Count > 0)
                {
                    //return RedirectPermanent("getdirection?bizid=" + bizID);
                    //var Address = bizDetails.BizMaster.FirstOrDefault();
                    //ViewBag.BizAddress = Address.address1 + " , " + Address.area + " , " + Address.city + " , " + Address.state;
                    return View(bizDetails.BizMaster.FirstOrDefault());
                }
                else
                {
                    //return Redirect("/"); //bizID not present in business Master
                    Response.Clear(); Response.StatusCode = 404;
                    Response.Status = "404 NotFound";
                    return View("../Static/404");
                }
            }
            else
            {
                //return Redirect("/"); //bizID is null
                Response.Clear(); Response.StatusCode = 404;
                Response.Status = "404 NotFound";
                return View("../Static/404");
            }
        }
        [HttpGet]
        public ActionResult ReportIncorrect(long? bizID)
        {
            return View();
        }
        #region Photos and Attachments
        //public ActionResult SaveUploadedFile()
        //{
        //    bool isSavedSuccessfully = true;
        //    try
        //    {
        //        foreach (string fileName in Request.Files)
        //        {
        //            HttpPostedFileBase file = Request.Files[fileName];
        //            //Save file content goes here
        //            //int id = biz.id;
        //            //string image = biz.image;
        //            BizDetailWEB bizDetailWEB = new BizDetailWEB();
        //            BizDetails bizDetails = bizDetailWEB.GetBizDetails(BusinessId);
        //            string businessname = bizDetails.BizMaster.FirstOrDefault().businessname;
        //            int contentid = bizDetails.BizMaster.FirstOrDefault().contentid;
        //            Int32 userid = 0;
        //            if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
        //                userid = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());



        //            //if (ModelState.IsValid)
        //            // {
        //            //if (file == null)
        //            //{
        //            //    ModelState.AddModelError("File", "Please Upload Your file");
        //            //}
        //            //else if (file.ContentLength > 0)
        //            //{
        //            int MaxContentLength = 1024 * 1024 * 3; //3 MB
        //            string[] AllowedFileExtensions = new string[] { ".jpg", ".gif", ".png", ".bmp" };

        //            if (!AllowedFileExtensions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
        //            {
        //                //ModelState.AddModelError("File", "Please file of type: " + string.Join(", ", AllowedFileExtensions));

        //                return Json(new { Message = "Invaild file extension" });
        //            }
        //            else if (file.ContentLength > MaxContentLength)
        //            {
        //                //ModelState.AddModelError("File", "Your file is too large, maximum allowed size is: " + MaxContentLength + " MB");
        //                return Json(new { Message = "Your file is too large, maximum allowed size is: " + MaxContentLength + " MB" });
        //            }
        //            else
        //            {
        //                //TO:DO
        //                //uploads/photogallery/first-bank-of-nigeria-plc_thumb_55013122.jpg
        //                // var fileName = Path.GetFileName(file.FileName);
        //                var path = Path.Combine(Server.MapPath("~/Resource/uploads/photogallery"), file.FileName);
        //                file.SaveAs(path);
        //                ModelState.Clear();
        //                //public string webconfigSourceImagesRootPath = ConfigurationManager.AppSettings["SourceImagesRootPath"].ToString();
        //                //public string webconfigDestinationImagesRootPath = ConfigurationManager.AppSettings["DestinationImagesRootPath"].ToString();
        //                //CopyImage.Sync(path, @"C:\vcsites\vcimages\Resource\uploads\photogallery");
        //                Vconnect.Common.Utility.CopyImage.Sync(webconfigSourceImagesRootPath + "photogallery", webconfigDestinationImagesRootPath + "photogallery");
        //                bizDetailWEB.addbusinessphotos(BusinessId, contentid, userid, businessname, ("uploads/photogallery/" + file.FileName).ToString().Trim());
        //                TempData["SuccessMessage"] = "File uploaded successfully";
        //            }
        //            //}
        //            // BusinessId
        //        }

        //        /////////////////////////////////////////////////////////////////////
        //        /*
        //        int id = biz.id;
        //        string image = biz.image;
        //        BizDetailWEB bizDetailWEB = new BizDetailWEB();
        //        BizDetails bizDetails = bizDetailWEB.GetBizDetails(id);
        //        string businessname = bizDetails.BizMaster.FirstOrDefault().businessname;
        //        int contentid = bizDetails.BizMaster.FirstOrDefault().contentid;
        //        Int32 userid = 0;
        //        if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
        //            userid = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());

        //        //if (ModelState.IsValid)
        //        // {
        //        if (file == null)
        //        {
        //            ModelState.AddModelError("File", "Please Upload Your file");
        //        }
        //        else if (file.ContentLength > 0)
        //        {
        //            int MaxContentLength = 1024 * 1024 * 3; //3 MB
        //            string[] AllowedFileExtensions = new string[] { ".jpg", ".gif", ".png", ".pdf" };

        //            if (!AllowedFileExtensions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
        //            {
        //                ModelState.AddModelError("File", "Please file of type: " + string.Join(", ", AllowedFileExtensions));
        //            }

        //            else if (file.ContentLength > MaxContentLength)
        //            {
        //                ModelState.AddModelError("File", "Your file is too large, maximum allowed size is: " + MaxContentLength + " MB");
        //            }
        //            else
        //            {
        //                //TO:DO
        //                //uploads/photogallery/first-bank-of-nigeria-plc_thumb_55013122.jpg
        //                var fileName = Path.GetFileName(file.FileName);
        //                var path = Path.Combine(Server.MapPath("~/uploads/photogallery"), fileName);
        //                file.SaveAs(path);
        //                ModelState.Clear();
        //                addbusinessphotos(id, contentid, userid, businessname, ("uploads/photogallery/" + fileName).ToString().Trim());
        //                TempData["SuccessMessage"] = "File uploaded successfully";
        //            }
        //        }
        //        //}
        //        return RedirectToAction("AddPhotosBiz", "BizDetailWEB", new { id = id, image = image });
        //        /////////////////////////////////////////////////////////////////////
        //        */
        //    }
        //    catch
        //    {
        //        return Json(new { Message = "Error in saving file" });
        //    }
        //    if (isSavedSuccessfully)
        //    {
        //        return Json(new { Message = "file saved" });
        //    }
        //    else
        //    {
        //        return Json(new { Message = "Error in saving file" });
        //    }
        //}
        public ActionResult SaveUploadedFile()
        {
            bool isSavedSuccessfully = true;
            string imgName = string.Empty;
            try
            {
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    //Save file content goes here
                    //int id = biz.id;
                    //string image = biz.image;
                    BizDetailWEB bizDetailWEB = new BizDetailWEB();
                    BusinessId = Convert.ToInt32(Request.QueryString["bizid"]);
                    BizDetails bizDetails = bizDetailWEB.GetBizDetails(BusinessId);
                    string businessname = bizDetails.BizMaster.FirstOrDefault().businessname;
                    int contentid = bizDetails.BizMaster.FirstOrDefault().contentid;
                    Int32 userid = 0;
                    if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                        userid = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                    int MaxContentLength = 1024 * 1024 * 5; //3 MB
                    int imagelength=int.Parse(file.ContentLength.ToString());
                    string[] AllowedFileExtensions = new string[] { ".jpeg", ".jpg", ".gif", ".png", ".bmp" };
                    if (!AllowedFileExtensions.Contains((file.FileName.Substring(file.FileName.LastIndexOf('.')).ToLower())))
                    {
                        //ModelState.AddModelError("File", "Please file of type: " + string.Join(", ", AllowedFileExtensions));
                        return Json(new { Message = "Invaild file extension" });
                    }
                    else if (imagelength > MaxContentLength)
                    {
                        //ModelState.AddModelError("File", "Your file is too large, maximum allowed size is: " + MaxContentLength + " MB");
                        return Json(new { Message = "Your file is too large, maximum allowed size is: " + MaxContentLength + " MB" });
                    }
                    else
                    {
                        //string finalfilename = renameUploadFile(file, 0);
                        //TO:DO
                        //uploads/photogallery/first-bank-of-nigeria-plc_thumb_55013122.jpg
                        imgName = (DateTime.Now.ToString("dd-mm-yyyy-hh-mm-ss-tt") + file.FileName.Replace(" ","_").Replace("#","_").Replace("%","_").Replace("^","_").Replace("'","_")).ToString();
                        var path = Path.Combine(Server.MapPath("~/Resource/uploads/photogallery/")+imgName);                        
                        file.SaveAs(path);
                        ModelState.Clear();
                        //public string webconfigSourceImagesRootPath = ConfigurationManager.AppSettings["SourceImagesRootPath"].ToString();
                        //public string webconfigDestinationImagesRootPath = ConfigurationManager.AppSettings["DestinationImagesRootPath"].ToString();
                        //CopyImage.Sync(path, @"C:\vcsites\vcimages\Resource\uploads\photogallery");
                        Utility.createCookieBizPhoto(Convert.ToInt32(BusinessId), imgName);
                        bizDetailWEB.addbusinessphotos(BusinessId, contentid, userid, businessname, ("uploads/photogallery/" + imgName).ToString().Trim());
                        Vconnect.Common.Utility.CopyImage.Sync(webconfigSourceImagesRootPath + "photogallery", webconfigDestinationImagesRootPath + "photogallery");
                        TempData["SuccessMessage"] = "File uploaded successfully";
                    }
                    //}
                    // BusinessId
                }

               
            }
            catch(Exception ex)
            {
                log.LogMe(ex);
                imgName="ERROR";           
               // return Json(imgName, JsonRequestBehavior.AllowGet);
                return Json(imgName, JsonRequestBehavior.AllowGet);
            }
            if (isSavedSuccessfully)
            {
                //return Json(new { Message = "file saved" });
                //return Json(new {imgName});
                return Json(imgName, JsonRequestBehavior.AllowGet);
            }
            else
            {
                imgName = "ERROR";
                //return Json(new { imgName });
                return Json(imgName, JsonRequestBehavior.AllowGet);
               // return Json(new { Message = "Error in saving file" });
            }
        }
      
        public ActionResult getdirection(long? bizid)
        {
            if (bizid.HasValue)
            {
                BizDetailWEB bizDetailWEB = new BizDetailWEB();
                BizDetails bizDetails = bizDetailWEB.GetBizDetails(bizid.Value);
                return RedirectPermanent("getdirection?bizid=" + bizid.Value);
            }
            else
            {
                return null;
            }
            //return View(bizDetailWEB);
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult EditPhotoTitlejson(string[][] updateimage, int divclass)
        {
            
            for (int i = 0; i < divclass; i++)
            {
                //updateBusinessPhoto(Request.QueryString[],Request.QueryString[],Request.QueryString[]);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditPhotoTitleDescription(string bizid, string name, string title, string description)
        {
            //string[][] updatearray=Request.QueryString["updateimage"];    
            string status = string.Empty;
            if (!string.IsNullOrEmpty(bizid) && !string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(title) && !string.IsNullOrEmpty(description))
            {
                string photopath = "uploads/photogallery/" + name;
                Utility.createCookieBizPhoto(Convert.ToInt32(bizid), name);               
                updateBusinessPhoto(bizid, photopath, title, description,0);
                return Json("SUCCESS", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult UpdateBizPhoto()
        {
            List<bizImageObj> bizImgObj = new List<bizImageObj>();
            if (Request.Cookies["vcBizPhoto"] != null)
            {
                HttpCookie cookieBizList = Request.Cookies["vcBizPhoto"];
                if (cookieBizList.HasKeys)
                {
                    //string[] imagename = cookieBizList.Value.ToString().Split('=');
                    //string bizid = imagename[0].ToString();
                    //Int32 useridnew = int.Parse(bizid.ToString());
                    //foreach (string selectimage in imagename)
                    //{
                    //    Int32 createdby = 0;
                    //    if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                    //        createdby = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                    //    string newimagename = "uploads/photogallery/" + selectimage.Replace("&" + bizid.ToString(), "").ToString();
                    //    updateBusinessPhoto("", newimagename, "", "", createdby);
                    //}
                    //return Json("SUCCESS", JsonRequestBehavior.AllowGet);


                    string[] imagename = cookieBizList.Value.ToString().Split('&');
                    //string bizid = imagename[0].ToString();
                    //Int32 useridnew = int.Parse(bizid.ToString());
                    foreach (string selectimage in imagename)
                    {
                        //string[] arr = selectimage.ToString().Split('=');
                        string[] imgdetails = selectimage.ToString().Split('=');
                        Int32 createdby = 0;
                        if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                            createdby = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                        //string newimagename = "uploads/photogallery/" + selectimage.Replace("&" + imgdetails[0].ToString(), "").ToString();
                        updateBusinessPhoto("", "uploads/photogallery/" + imgdetails[1].ToString(), "", "", createdby);
                    }
                    return Json("SUCCESS", JsonRequestBehavior.AllowGet);
                }
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
        public int updateBusinessPhoto(string bizid,string photoName, string photoTitle, string photoDescription,Int32? createdby)
        {
            
            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_edit_PhotosToBusiness_live]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                
                cmd.Parameters.Add(new SqlParameter("@photo", photoName.ToString()));
                cmd.Parameters.Add(new SqlParameter("@title", photoTitle.ToString()));
                cmd.Parameters.Add(new SqlParameter("@description", photoDescription.ToString()));
                if (createdby.HasValue)
                {
                    cmd.Parameters.Add(new SqlParameter("@userid", createdby.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@userid", 0));
                }
                cmd.Parameters.Add(new SqlParameter("@Err", ParameterDirection.Output));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();
                result = (int)cmd.Parameters["@Err"].Value;
            }
            return result;
        }
        public ActionResult GetBizPhotosAttachments(int bizid, string module, string businessurl)
        {
            BizDetailWEB bizDetailWEB = new BizDetailWEB();
            List<BizDetailPhotoAttachments> BizDetailPhotoAttachments = new List<BizDetailPhotoAttachments>();
            if (module == "attachment")
            {
                Response.Clear(); Response.StatusCode = 404;
                Response.Status = "404 NotFound";
                return View("../Static/404");
                //var data = bizDetailWEB.GetAttachmentGallery(bizid);
                
                
                //var result = data.Select(m => new { bizname = m.businessname, bizid = m.businessid, photo = m.attachments, thumb = m.attachmentsthumbs, url = m.businessurl, area = m.area, city = m.city, state = m.state, address1 = m.address1, title =m.title, description = m.description,houseno = m.houseno  });
                //foreach (var item in result)
                //{
                //    BizDetailPhotoAttachments  bizDetailPhotoAttachmentsItem = new BizDetailPhotoAttachments();
                //    bizDetailPhotoAttachmentsItem.bizname = item.bizname;
                //    bizDetailPhotoAttachmentsItem.bizid = item.bizid.ToString();
                //    bizDetailPhotoAttachmentsItem.photo = item.photo;
                //    bizDetailPhotoAttachmentsItem.thumb = item.thumb;
                //    bizDetailPhotoAttachmentsItem.url = item.url;
                //    bizDetailPhotoAttachmentsItem.area = item.area;
                //    bizDetailPhotoAttachmentsItem.city = item.city;
                //    bizDetailPhotoAttachmentsItem.state = item.state;
                //    bizDetailPhotoAttachmentsItem.address1 = item.address1;
                //    bizDetailPhotoAttachmentsItem.title = item.title;
                //    bizDetailPhotoAttachmentsItem.description = item.description;
                //    bizDetailPhotoAttachmentsItem.houseno = item.houseno;
                //    BizDetailPhotoAttachments.Add(bizDetailPhotoAttachmentsItem);
                //}
                //if (BizDetailPhotoAttachments.Count == 0)
                //{
                //    Response.Clear(); Response.StatusCode = 404;
                //    Response.Status = "404 NotFound";
                //    return View("../Static/404");
                    
                //}
                //return View(BizDetailPhotoAttachments);                
            }
            else // attachment
            {
                var data = bizDetailWEB.GetPhotoGallery(bizid);
                //var result = data.Select(m => new { bizname = m.businessname, bizid = m.businessid, photo = m.photo, url = businessurl });
                var result = data.Select(m => new { bizname = m.businessname, bizid = m.businessid, photo = m.photosmax, thumb = m.photothumbs, url = m.businessurl, area = m.area, city = m.city, state = m.state, address1 = m.address1, title = m.title, description = m.description,houseno=m.houseno });
                foreach (var item in result)
                {
                    BizDetailPhotoAttachments bizDetailPhotoAttachmentsItem = new BizDetailPhotoAttachments();
                    bizDetailPhotoAttachmentsItem.bizname = item.bizname;
                    bizDetailPhotoAttachmentsItem.bizid = item.bizid.ToString();
                    bizDetailPhotoAttachmentsItem.photo = item.photo;
                    bizDetailPhotoAttachmentsItem.thumb = item.thumb;
                    bizDetailPhotoAttachmentsItem.url = item.url;
                    bizDetailPhotoAttachmentsItem.area = item.area;
                    bizDetailPhotoAttachmentsItem.city = item.city;
                    bizDetailPhotoAttachmentsItem.state = item.state;
                    bizDetailPhotoAttachmentsItem.address1 = item.address1;
                    bizDetailPhotoAttachmentsItem.title = item.title;
                    bizDetailPhotoAttachmentsItem.description = item.description;
                    bizDetailPhotoAttachmentsItem.houseno = item.houseno;
                    BizDetailPhotoAttachments.Add(bizDetailPhotoAttachmentsItem);
                }
                if (BizDetailPhotoAttachments.Count == 0)
                {
                    Response.Clear(); Response.StatusCode = 404;
                    Response.Status = "404 NotFound";
                    return View("../Static/404");
                   // return RedirectToAction("BizDetail", "BizDetail");
                }
                return View(BizDetailPhotoAttachments);
                //return View(result);
                //return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetAllPhotosAttachments(int bizid, string module, string businessurl)
        {
            BizDetailWEB bizDetailWEB = new BizDetailWEB();
            if (module == "attachment")
            {
                var data = bizDetailWEB.GetAttachmentGallery(bizid);
                //var result = data.Select(m => new { bizname = m.businessname, bizid = m.businessid, photo = m.originalimage, url = businessurl});
                var result = data.Select(m => new { bizname = m.businessname, bizid = m.businessid, photo = m.attachments, thumb = m.attachmentsthumbs, url = m.businessurl , area = m.area, city = m.city, state= m.state, address1 = m.address1});
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else // attachment
            {
                var data = bizDetailWEB.GetPhotoGallery(bizid);
                //var result = data.Select(m => new { bizname = m.businessname, bizid = m.businessid, photo = m.photo, url = businessurl });
                var result = data.Select(m => new { bizname = m.businessname, bizid = m.businessid, photo = m.photosmax, thumb = m.photothumbs, url = m.businessurl, area = m.area, city = m.city, state = m.state, address1 = m.address1 });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult DisplayPhotosAttachmentBiz(int bizid, string module, string businessurl)
        {

            return View();
        }
        
        //public ActionResult AddPhotosBiz(int bizID, HttpPostedFileBase uploadFile)
        //{
        //     if (uploadFile != null && uploadFile.ContentLength > 0)
        //     { string filePath1 = Path.Combine(HttpContext.Server.MapPath("~/resource/uploads/photogallery"));


        //     string filePath = Path.Combine(filePath1, Path.GetFileName(uploadFile.FileName));

        //         uploadFile.SaveAs(filePath);
        //     } 
        //    Int32 userid = 0;
        //    if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
        //    {
        //        userid = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
        //    }
        //    else
        //    {
        //        return Redirect("");
        //    }


        //    string photo = string.Empty;

        //    BizDetailWEB bizDetailWEB = new BizDetailWEB();
        //    var data=bizDetailWEB.GetBizDetailWap(bizID,null).bizDetail.FirstOrDefault();
        //   // bizDetailWEB.addbusinessphotos(bizID, 0, userid, data.businessname, "Photo");
        //       return View();
        // }
        [HttpPost]
        
        #endregion

        /// <summary>
        /// Click to View
        /// </summary>
        /// <returns></returns>
        #region Clicktoview
        public FileContentResult StringImage(string str, string whichNumber)
        {
            var rand = new Random((int)DateTime.Now.Ticks);

            //generate new question
            int a = rand.Next(10, 99);
            int b = rand.Next(0, 9);
            var captcha = str;//string.Format("{0} + {1} = ?", a, b);

            //store answer
            //Session["Captcha" + prefix] = a + b;


            //image stream
            FileContentResult img = null;

            using (var mem = new MemoryStream())
            using (var bmp = new Bitmap(91, 21))
            using (var gfx = Graphics.FromImage((Image)bmp))
            {               
                gfx.SmoothingMode = SmoothingMode.AntiAlias;
                gfx.Clear(Color.Transparent);
                gfx.TextRenderingHint = TextRenderingHint.AntiAlias; 
                
                gfx.FillRectangle(Brushes.Transparent, 0, 0, bmp.Width, bmp.Height);
                
                gfx.DrawString(captcha, new Font("Tahoma", 10), Brushes.White, 0,0);
              
                //render as Jpeg
                bmp.Save(mem, System.Drawing.Imaging.ImageFormat.Png);
                img = this.File(mem.GetBuffer(), "image/png");

            }
            img.FileDownloadName = whichNumber;
            return img;
        }
        public string GetPhoneinBinary(Int64 bizId, string phoneAlternatePhone)
        {

            BizDetailWEB bizDetailWEB = new BizDetailWEB();
            BizPhoneNumbers bizPhoneNumbers = new BizPhoneNumbers();
            bizPhoneNumbers = bizDetailWEB.GetBizPhoneNumbers(bizId);
            FileContentResult img = null;
            JsonModel jsonModel = new JsonModel();
            string phone = string.Empty;
            string alternatephone = string.Empty;
            string phoneNumbers = string.Empty;

            if (phoneAlternatePhone == "phone" && bizPhoneNumbers.phone != null && bizPhoneNumbers.phone != "")
            {
                jsonModel.HTMLString = RenderPartialViewToString("_ClicktoViewPhone", bizPhoneNumbers).Replace("\n", "").Replace("\r", "");
                img = StringImage(jsonModel.HTMLString.Replace("\n", "").Replace("\r", ""), "phone");
                //phone = "<img src='data:image/jpg;base64," + Convert.ToBase64String(img.FileContents) + "' alt='' style='border:solid 0px white' />";
                phone = "<img src='data:image/png;base64," + Convert.ToBase64String(img.FileContents) + "' alt='' style='border:solid 0px white' />";
                //jsonModel.HTMLString = "<img src='data:image/jpg;base64," + Convert.ToBase64String(img.FileContents) + "' alt='' />";
                return phone;
            }
            else if (phoneAlternatePhone == "alternatephone" && bizPhoneNumbers.alternatephone != null && bizPhoneNumbers.alternatephone != "")
            {
                jsonModel.HTMLString = RenderPartialViewToString("_ClicktoViewAlternatePhone", bizPhoneNumbers);
                img = StringImage(jsonModel.HTMLString.Replace("\n", "").Replace("\r", ""), "alternatephone");
                //jsonModel.HTMLString = "<img src='data:image/jpg;base64," + Convert.ToBase64String(img.FileContents) + "' alt='' />";
                //alternatephone = "<img src='data:image/jpg;base64," + Convert.ToBase64String(img.FileContents) + "' alt='' style='border:solid 0px white'/>";
                alternatephone = "<img src='data:image/png;base64," + Convert.ToBase64String(img.FileContents) + "' alt='' style='border:solid 0px white'/>";
                return alternatephone;
            }
            //phoneNumbers = (!string.IsNullOrEmpty(phone) && !string.IsNullOrEmpty(alternatephone)) ? phone + "¿" + alternatephone :
            //    (!string.IsNullOrEmpty(phone) && string.IsNullOrEmpty(alternatephone)) ? phone :
            //    (string.IsNullOrEmpty(phone) && !string.IsNullOrEmpty(alternatephone)) ? alternatephone :
            //    (string.IsNullOrEmpty(phone) && string.IsNullOrEmpty(alternatephone)) ? "" : "";            
            return string.Empty;
        }
        public string GetPhoneinBinary(string phoneno, string phoneAlternatePhone)
        {
            FileContentResult img = null;
            JsonModel jsonModel = new JsonModel();
            string phone = string.Empty;
            string alternatephone = string.Empty;
            string phoneNumbers = string.Empty;

            if (phoneAlternatePhone == "phone" && !string.IsNullOrEmpty(phoneno))
            {                
                img = StringImage(phoneno, "phone");                
                phone = "<img src='data:image/png;base64," + Convert.ToBase64String(img.FileContents) + "' alt='phone' style='border:solid 0px white' />";                
                return phone;
            }
            else if (phoneAlternatePhone == "alternatephone" && !string.IsNullOrEmpty(phoneno))
            {                
                img = StringImage(phoneno, "alternatephone");                
                alternatephone = "<img src='data:image/png;base64," + Convert.ToBase64String(img.FileContents) + "' alt='alternate phone' style='border:solid 0px white'/>";
                return alternatephone;
            }            
            return string.Empty;
        }
        public JsonResult GetBizPhoneNumbers(Int64? bizId)
        {
            if (bizId.HasValue)
            {
                BizDetailWEB bizDetailWEB = new BizDetailWEB();
                BizPhoneNumbers bizPhoneNumbers = new BizPhoneNumbers();
                bizPhoneNumbers = bizDetailWEB.GetBizPhoneNumbers(bizId.Value);
                FileContentResult img = null;
                JsonModel jsonModel = new JsonModel();
                string phone = string.Empty;
                string alternatephone = string.Empty;
                string phoneNumbers = string.Empty;
                if (bizPhoneNumbers.phone != null && bizPhoneNumbers.phone != "")
                {
                    jsonModel.HTMLString = RenderPartialViewToString("_ClicktoViewPhone", bizPhoneNumbers).Replace("\n", "").Replace("\r", "");
                    img = StringImage(jsonModel.HTMLString.Replace("\n", "").Replace("\r", ""), "phone");
                    phone = "<img src='data:image/jpg;base64," + Convert.ToBase64String(img.FileContents) + "' alt='phone' style='border:solid 0px white' />";
                    //jsonModel.HTMLString = "<img src='data:image/jpg;base64," + Convert.ToBase64String(img.FileContents) + "' alt='' />";
                }
                if (bizPhoneNumbers.alternatephone != null && bizPhoneNumbers.alternatephone != "")
                {
                    jsonModel.HTMLString = RenderPartialViewToString("_ClicktoViewAlternatePhone", bizPhoneNumbers);
                    img = StringImage(jsonModel.HTMLString.Replace("\n", "").Replace("\r", ""), "alternatephone");
                    alternatephone = "<img src='data:image/jpg;base64," + Convert.ToBase64String(img.FileContents) + "' alt='alternatephone' style='border:solid 0px white'/>";
                }
                phoneNumbers = (!string.IsNullOrEmpty(phone) && !string.IsNullOrEmpty(alternatephone)) ? phone + "¿" + alternatephone :
                    (!string.IsNullOrEmpty(phone) && string.IsNullOrEmpty(alternatephone)) ? phone :
                    (string.IsNullOrEmpty(phone) && !string.IsNullOrEmpty(alternatephone)) ? alternatephone :
                    (string.IsNullOrEmpty(phone) && string.IsNullOrEmpty(alternatephone)) ? "" : "";
                return Json(phoneNumbers,JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null,JsonRequestBehavior.AllowGet);
            }
        }
        //public ActionResult GetPhoneNumbers(Int64 bizId, string whichNumber)
        
        #endregion

        #region Gallery
        /// <summary>
        /// Function to get the list of the BussinesAttachment for particullar bussiness 
        /// </summary>
        /// <param name="bizID">Bissines Id</param>
        /// <returns>List of path of images</returns>
        public ActionResult wasthishelpful(string whichnumber, string status, Int64? bizid)
        {
            if (bizid.HasValue)
            {
                Int64 userid = 0;
                BizDetailWEB bizDetailWEB = new BizDetailWEB();
                if (Request.Cookies != null && !string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                {
                    userid = Convert.ToInt64(objUS.SV_VCUserContentID);
                }
                int result = bizDetailWEB.wasthishelpful(whichnumber, status, bizid.Value, userid);
                if (result == 2)
                {
                    return Json("already", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("helpfull", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult IsHelpFull(Int64 bizId, Int64 reviewId, int? userId, int? ishelp)
        {
            BizDetailWEB bizDetailWEB = new BizDetailWEB();
            userId = Convert.ToInt32(objUS.SV_VCUserContentID);
            bizDetailWEB.IsHelpFull(bizId, reviewId, userId, ishelp);
            if (ishelp == 1)
            {
                return Json("HelpFull", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("InAccurate", JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Follow(Int64 bizId, Int64 ReviewId, int? userId, int? status, int PostedbyId)
        {
            BizDetailWEB bizDetailWEB = new BizDetailWEB();
            userId = Convert.ToInt32(objUS.SV_VCUserContentID);
            int result = bizDetailWEB.Follow(bizId, ReviewId, userId, status, PostedbyId);
            string userName = objUS.SV_VCUserName;
            ReviewerDetails reviewerDetails = new ReviewerDetails();

            reviewerDetails = bizDetailWEB.getReviewerDetails(PostedbyId, PostedbyId, 1);
            if (!string.IsNullOrEmpty(reviewerDetails.email) && result == 0)
            {
                SendFollowReviewer(reviewerDetails.contactname, reviewerDetails.email, userName);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Function to get the list of the Video for particullar bussiness 
        /// </summary>
        /// <param name="bizID">Bissines Id</param>
        /// <returns>List of Videos in Iframe</returns>
       

        /// <summary>
        /// Function to get the list of the Attackment for particullar bussiness 
        /// </summary>
        /// <param name="bizID">Bissines Id</param>
        /// <returns>List of Attchmnet paths</returns>

        public ActionResult RepotIncorrect(string remarks, string whatswrong, string detail, string email, int? bizId)
        {
            try
            {
                
                int userid = 0;
                userid = (!string.IsNullOrEmpty(objUS.SV_VCUserContentID)) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                if (bizId.HasValue)// && Request.ContentType.IndexOf("json") > 0)
                {
                    BizDetailWEB bizDetailWEB = new BizDetailWEB();
                    BizDetailInfo bizDetailInfo = new BizDetailInfo();
                    bizDetailInfo = bizDetailWEB.GetBizDetailInfo(bizId.Value);
                    int result = bizDetailWEB.ReportIncorrect(remarks,whatswrong, detail, email, bizId.Value, userid);
                    if (result != 0)
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                    EmailTOAdminForIncorrectReport(bizDetailInfo.businessname, email, detail, email);
                }
                else
                {
                    return Json("error", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            return Json("Informatsion is added", JsonRequestBehavior.AllowGet);

        }

        [ValidateInput(false)]
        public ActionResult AddRating_Review(string userid, string bizid, string review, string businessname, Int16? rate)
        {
            string OUTBusinessReviewContentID = string.Empty;
            string result = string.Empty;
            if (Session["rturl"] != null && Session["rturl"] != "" && Session["rturl"].ToString().ToLower().IndexOf("reviewtext") != -1)
            {
                Session["rturl"] = "";
            }
            try
            {
                if (rate == null || rate == 0)
                    OUTBusinessReviewContentID = "-1";
                else if (review != null && review != "" && review != " ")
                {
                    BizDetailWEB bizDetailWEB = new BizDetailWEB();
                    OUTBusinessReviewContentID = bizDetailWEB.addbusinessreviews(Convert.ToInt64(bizid), Convert.ToInt64(userid), review, businessname);
                    result = bizDetailWEB.addbusinessRate(Convert.ToInt32(bizid), Convert.ToInt32(userid), rate, Convert.ToInt32(OUTBusinessReviewContentID));
                }

                else
                    OUTBusinessReviewContentID = "0";
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            return Json(OUTBusinessReviewContentID, JsonRequestBehavior.AllowGet);
        }
     
        [ValidateInput(false)]
        public ActionResult AddReview(string userid, string bizid, string review, string businessname)
        {
            string OUTBusinessReviewContentID = string.Empty;
            if (Session["rturl"] != null && Session["rturl"] != "" && Session["rturl"].ToString().ToLower().IndexOf("reviewtext") != -1)
            {
                Session["rturl"] = "";
            }
            try
            {
                if (review != null && review != "" && review != " ")
                {
                    BizDetailWEB bizDetailWEB = new BizDetailWEB();
                    OUTBusinessReviewContentID = bizDetailWEB.addbusinessreviews(Convert.ToInt64(bizid), Convert.ToInt64(userid), review, businessname);
                }
                else
                {
                    OUTBusinessReviewContentID = "0";
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            return Json(OUTBusinessReviewContentID, JsonRequestBehavior.AllowGet);
        }
        //public ActionResult AddRate(Int32? userid, Int32? bizid, Int16? rate)
        //{
        //    Int32? Userid = userid ?? 0;
        //    string result = string.Empty;
        //    string status = string.Empty;
        //    int updateResult = 0;
        //    try
        //    {
        //        BizDetailWEB bizDetailWEB = new BizDetailWEB();
        //        if (bizid.HasValue && rate.HasValue)
        //        {
        //            status = Utility.createCookieBiz(bizid.Value, rate.Value);
        //        }
        //        if (Userid.Value > 0 && bizid.HasValue && rate.HasValue)
        //        {
        //            updateResult = bizDetailWEB.updateBusinessClicktoRate(bizid.Value, Userid.Value);
        //        }
        //        if (bizid.HasValue && rate.HasValue)
        //        {
        //            if (status != "already")
        //            {
        //                result = bizDetailWEB.addbusinessRate(bizid.Value, Userid.Value, rate.Value);
        //            }
        //            else
        //            {
        //                result = "already";
        //            }
        //        }
        //        else
        //        {
        //            result = "ERROR";
        //        }
        //        if (result == "ERROR")
        //        {
        //            return Json("error", JsonRequestBehavior.AllowGet);
        //        }
        //        else
        //        {
        //            return Json(result.ToLower().Trim(), JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.LogMe(ex);
        //    }
        //    return Json("added", JsonRequestBehavior.AllowGet);

        //}
        //public void AddBuinsesscookies(Int32 bizid, Int16 rate)
        //{
        //    System.Collections.Specialized.NameValueCollection cookieBiz = new System.Collections.Specialized.NameValueCollection();
        //    cookieBiz.Add(bizid.ToString(), rate.ToString());
        //    HttpCookie cookieBizList = new HttpCookie("BizRate");
        //    cookieBizList.Values.Add(cookieBiz);
        //    System.Web.HttpContext.Current.Request.Cookies.Add(cookieBizList);
        //}

        //public void GetBuinsesscookies()
        //{
        //    //System.Collections.Specialized.NameValueCollection cookiecoll = new System.Collections.Specialized.NameValueCollection();
        //    //cookiecoll.Add(bizid.ToString(), rate.ToString());
        //    //HttpCookie cookieBizList = new HttpCookie("BizRate");
        //    //cookieBizList.Values.Add(cookiecoll);
        //    //System.Web.HttpContext.Current.Request.Cookies.Add(cookieBizList);
        //    HttpCookie cookieBizList = Request.Cookies["BizRate"];
        //    System.Collections.Specialized.NameValueCollection cookieBiz = new System.Collections.Specialized.NameValueCollection();
        //    if (cookieBizList.HasKeys)
        //    {
        //        //foreach (HttpCookie item in cookieBizList)
        //        //{

        //        //}
        //    }
        //    //myCookie.HasKeys
        //}
        
        public ActionResult SendBizDetails(Int64? bizid, string RecipientPhoneNumber)
        {
            if (bizid.HasValue)// && Request.ContentType.IndexOf("json") > 0)
            {
                string msg = string.Empty;
                try
                {
                    int vaildphone = 0;
                    vaildphone = Vconnect.Common.Utility.isValidPhone(RecipientPhoneNumber.ToString());
                    if (vaildphone == 0)
                    {
                        msg = "invalid";
                    }
                    else
                    {
                        SendSMS send = new SendSMS();

                        BizDetailInfo bizDetailInfo = new BizDetailInfo();
                        BizDetailWEB bizDetailWEB = new BizDetailWEB();
                        bizDetailInfo = bizDetailWEB.GetBizDetailInfo(bizid.Value);
                        string[] contactNos = bizDetailInfo.contactno.Split(',');
                        string allContactNo = string.Empty;
                        foreach (var item in contactNos)
                        {
                            if (!string.IsNullOrEmpty(item))
                            {
                                if (string.IsNullOrEmpty(allContactNo))
                                {
                                    allContactNo = "0" + Convert.ToInt64(item).ToString("##-####-####");
                                }
                                else
                                {
                                    allContactNo = allContactNo + ", " + "0" + Convert.ToInt64(item).ToString("##-####-####");
                                }
                            }
                        }
                        msg = bizDetailInfo.businessname + ", " + bizDetailInfo.address + ", " + allContactNo;
                        string SMSResponse = send.Sendmessage(msg, RecipientPhoneNumber);
                       
                        int usertype = !string.IsNullOrEmpty(objUS.SV_VCUserType) ? Convert.ToInt16(objUS.SV_VCUserType) : 0;

                        string source = "WEB";
                        
                            
                        if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID) && Convert.ToInt32(objUS.SV_VCUserContentID) != 0)
                        {
                            Utility.SaveSMSEmailLog(Convert.ToInt32(objUS.SV_VCUserContentID), usertype, "", RecipientPhoneNumber, "", MessageType.SMS_UserReq.GetHashCode(), ConfigurationManager.AppSettings["SubjectBusinessDetail"].ToString(), "SMSed details send for: " + msg, Convert.ToInt32(bizid.Value), "", "", SMSResponse, Utility.GetIpAddressNew(), 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.Email.GetHashCode(), Utility.GetReversedns(Utility.GetIpAddressNew()), source);
                        }
                        else
                        {
                            Utility.SaveSMSEmailLog(0, usertype, "", RecipientPhoneNumber, "", MessageType.SMS_UserReq.GetHashCode(), ConfigurationManager.AppSettings["SubjectBusinessDetail"].ToString(), "SMSed details send for: " + msg, Convert.ToInt32(bizid.Value), "", "", SMSResponse, Utility.GetIpAddressNew(), 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.Email.GetHashCode(), Utility.GetReversedns(Utility.GetIpAddressNew()), source);
                        }
                        msg = "";
                    }
                }
                catch (Exception ex)
                {
                    msg = "invalid";
                    log.LogMe(ex);
                }
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Logs
        public ActionResult BusinessLog(Int32? bizid, string bizName,string sourceurl, string previousurl)
        {
             ///string sourceurl = string.Empty;
             ///string previousurl = string.Empty;
            try
            {
                //sourceurl = TempData["sourcedetail"] != null ? TempData["sourcedetail"].ToString() : "";
                //previousurl = TempData["previousdetail"] != null ? TempData["previousdetail"].ToString() : "";

                

                //sourceurl = TempData.Peek("sourcedetail") != null ? TempData.Peek("sourcedetail").ToString() : "";
                //previousurl = TempData.Peek("previousdetail") != null ? TempData.Peek("previousdetail").ToString() : "";

                //TempData.Keep("sourcedetail");
                //TempData.Keep("previousdetail");
                
                //string IPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();   
                string IPAddress = string.Empty;
                string ReverseDNS = string.Empty;
                if (bizid.HasValue && bizid.Value > 0)
                {
                    #region  sourceurl, previousurl
                    //if (Request.UrlReferrer == null || Request.UrlReferrer.ToString().Trim() == "")
                    //{
                    //    PREVIOUSURL = "-";
                    //}
                    //else
                    //{
                    //    PREVIOUSURL = Request.UrlReferrer.ToString().ToLower().Replace("//", "~~").Replace("/", "~").Replace(".html", ".indexpage").Replace("_", "~").Replace("-", "~").Trim();
                    //}
                    //if (System.Web.HttpContext.Current.Request.Url.AbsoluteUri == null || System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString().Trim() == "")
                    //{
                    //    SOURCEURL = "-";
                    //}
                    //else
                    //{
                    //    SOURCEURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString().ToLower().Replace("//", "~~").Replace("/", "~").Replace(".html", ".indexpage").Replace("_", "~").Replace("-", "~").Trim();
                    //}
                    #endregion
                    int result = 0;

                    //string keyword = Request.Cookies["SearchKeyword"].ToString();
                    //if (bizid != null && Request.Cookies["VCSearchLocation"] != null)
                    //{
                    if (bizName == null)
                    {
                        bizName = string.Empty;
                    }
                    IPAddress = Vconnect.MvcApplication.GetVisitorIPAddress();
                    ReverseDNS = !string.IsNullOrEmpty(IPAddress)? Vconnect.MvcApplication.GetReverseDns(IPAddress.ToString()) : "";
                    //Session["IPADDRESS"] = MyGlobalVariables.GetIpAddress;
                    //Session["REVERSEDNS"] = MyGlobalVariables.GetReverseDns;

                    //if (MyGlobalVariables.GetIpAddress == "0")
                    //{
                    //    try
                    //    {
                    //        Session["IPADDRESS"] = Vconnect.MvcApplication.GetVisitorIPAddress();
                    //    }
                    //    catch
                    //    { }
                    //}
                    //if (MyGlobalVariables.GetReverseDns == "0")
                    //{
                    //    try
                    //    {
                    //        Session["REVERSEDNS"] = Vconnect.MvcApplication.GetReverseDns(Session["IPADDRESS"].ToString());
                    //    }
                    //    catch
                    //    {}
                    //}
                    if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                    {
                        result = log.addbusinessvisitorlog(bizid.Value, bizName, Convert.ToInt32(objUS.SV_VCUserContentID.ToString()), IPAddress, System.Web.HttpUtility.HtmlDecode(sourceurl), System.Web.HttpUtility.HtmlDecode(previousurl), 6, ReverseDNS);
                    }
                    else
                    {
                        result = log.addbusinessvisitorlog(bizid.Value, bizName, 0, IPAddress, sourceurl, previousurl, 6, ReverseDNS);

                    }
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{

            //}
        }
       
        //protected void AddBusinessLog(Int32 bizid, string bizName)
        //{
        //    try
        //    {
        //        //string IPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
        //        string PREVIOUSURL = string.Empty;
        //        string SOURCEURL = string.Empty;


        //        if (Request.UrlReferrer == null || Request.UrlReferrer.ToString().Trim() == "")
        //        {
        //            PREVIOUSURL = "-";
        //        }
        //        else
        //        {
        //            PREVIOUSURL = Request.UrlReferrer.ToString().ToLower().Replace("//", "~~").Replace("/", "~").Replace(".html", ".indexpage").Replace("_", "~").Replace("-", "~").Trim();
        //        }
        //        if (System.Web.HttpContext.Current.Request.Url.AbsoluteUri == null || System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString().Trim() == "")
        //        {
        //            SOURCEURL = "-";
        //        }
        //        else
        //        {
        //            SOURCEURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString().ToLower().Replace("//", "~~").Replace("/", "~").Replace(".html", ".indexpage").Replace("_", "~").Replace("-", "~").Trim();
        //        }
        //        int result = 0;

        //        //string keyword = Request.Cookies["SearchKeyword"].ToString();
        //        //if (bizid != null && Request.Cookies["VCSearchLocation"] != null)
        //        //{
        //        if (bizName == null)
        //        {
        //            bizName = string.Empty;
        //        }
        //        if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
        //        {
        //            result = log.addbusinessvisitorlog(bizid, bizName, Convert.ToInt32(objUS.SV_VCUserContentID.ToString()), Utility.GetIpAddressNew(), SOURCEURL, PREVIOUSURL, 6, Utility.GetReversedns(Utility.GetIpAddressNew()));
        //        }
        //        else
        //        {
        //            result = log.addbusinessvisitorlog(bizid, bizName, 0, Utility.GetIpAddressNew(), SOURCEURL, PREVIOUSURL, 6, Utility.GetReversedns(Utility.GetIpAddressNew()).ToString());

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.LogMe(ex);
        //    }

        //    //}
        //    //else
        //    //{

        //    //}
        //}

        public ActionResult AddPhoneClickLog(Int64? bizId, string whichNumber,string sourceurl, string previousurl)
        {
            //string sourceurl = string.Empty;
            //string previousurl = string.Empty;

            ////sourceurl = TempData["sourcedetail"] != null ? TempData["sourcedetail"].ToString() : "";
            ////previousurl = TempData["previousdetail"] != null ? TempData["previousdetail"].ToString() : "";

            
            //sourceurl = TempData.Peek("sourcedetail") != null ? TempData.Peek("sourcedetail").ToString() : "";
            //previousurl = TempData.Peek("previousdetail") != null ? TempData.Peek("previousdetail").ToString() : "";

            //TempData.Keep("sourcedetail");
            //TempData.Keep("previousdetail");

                
            if (bizId.HasValue)
            {
                BizDetailWEB bizDetailWEB = new BizDetailWEB();
                BizPhoneNumbers bizPhoneNumbers = new BizPhoneNumbers();
                bizPhoneNumbers = bizDetailWEB.GetBizPhoneNumbers(bizId.Value);
                JsonModel jsonModel = new JsonModel();
                string phoneno = string.Empty;
                int result = 0;

                FileContentResult img = null;
                int userid = (!string.IsNullOrEmpty(objUS.SV_VCUserContentID)) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;

                if (whichNumber == "phone")
                {
                    //jsonModel.HTMLString = RenderPartialViewToString("_ClicktoViewPhone", bizPhoneNumbers).Replace("\n", "").Replace("\r", "");
                    //img = StringImage(jsonModel.HTMLString.Replace("\n", "").Replace("\r", ""), whichNumber);
                    //jsonModel.HTMLString = "<img src='data:image/jpg;base64," + Convert.ToBase64String(img.FileContents) + "' alt='' />";
                    phoneno = bizPhoneNumbers.phone;
                }
                else if (whichNumber == "alternatephone")
                {
                    //jsonModel.HTMLString = RenderPartialViewToString("_ClicktoViewAlternatePhone", bizPhoneNumbers);
                    //img = StringImage(jsonModel.HTMLString.Replace("\n", "").Replace("\r", ""), whichNumber);
                    //jsonModel.HTMLString = "<img src='data:image/jpg;base64," + Convert.ToBase64String(img.FileContents) + "' alt='' />";
                    phoneno = bizPhoneNumbers.alternatephone;
                }
                else {
                    phoneno = bizPhoneNumbers.phone;
                }
                Session["IPADDRESS"] = MyGlobalVariables.GetIpAddress;
                Session["REVERSEDNS"] = MyGlobalVariables.GetReverseDns;
                if (MyGlobalVariables.GetIpAddress == "0")
                {
                    try
                    {
                        Session["IPADDRESS"] = Vconnect.MvcApplication.GetVisitorIPAddress();
                    }
                    catch
                    { }
                }
                if (MyGlobalVariables.GetReverseDns == "0")
                {
                    try
                    {
                        Session["REVERSEDNS"] = Vconnect.MvcApplication.GetReverseDns(Session["IPADDRESS"].ToString());
                    }
                    catch
                    { }
                }
                result = log.addphoneclicklog(whichNumber, bizId.ToString(), userid, MyGlobalVariables.GetIpAddress, System.Web.HttpUtility.HtmlDecode(sourceurl), System.Web.HttpUtility.HtmlDecode(previousurl));
                return Json(result,
                    JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null,
                    JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region send mail in predefined format
        

        protected void SendFollowReviewer(string ContactPerson, string EmailID, string UserName)
        {
            string Message = "";
            System.Net.Mail.SmtpClient SMPT = new System.Net.Mail.SmtpClient();
            System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage();
            System.IO.FileStream FsContent = new System.IO.FileStream(Server.MapPath("~/resource/email_templates/FollowReviewer.html"), System.IO.FileMode.Open, System.IO.FileAccess.Read);
            System.IO.StreamReader SRcontent = new System.IO.StreamReader(FsContent);
            Message = SRcontent.ReadToEnd();
            FsContent.Close();
            SRcontent.Close();
            FsContent.Dispose();
            SRcontent.Dispose();
            Message = Message.Replace("{0}", ContactPerson);
            Message = Message.Replace("{1}", UserName);
            Message = Message.Replace("{20}", ConfigurationManager.AppSettings["WebsiteRootPath"].ToString());
            Email.Subject = ConfigurationManager.AppSettings["FollowerReviewer"].ToString();
            Email.From = new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["networkUserIdSupport"].ToString(), "VConnect.com");
            Email.IsBodyHtml = true;
            Email.Body = Message;
            Email.To.Add(EmailID.ToString().Trim());
            //Email.To.Add("devinder.kaur@aardeesoft.com");
            //Email.Bcc.Add(ConfigurationManager.AppSettings["BCCEmailId"].ToString());

            SendEmail send = new SendEmail();
            try
            {
                send.SendMailToUser(ref Email, ConfigurationManager.AppSettings["networkUserIdSupport"].ToString(), ConfigurationManager.AppSettings["networkUserIdPasswordSupport"].ToString());
            }
            catch (Exception e)
            {
                log.LogMe(e);
            }

        }

        protected void EmailTOAdminForIncorrectReport(string businessname, string username, string reportdetail, string email)
        {
            string Message = "";
            System.Net.Mail.SmtpClient SMPT = new System.Net.Mail.SmtpClient();
            System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage();
            System.IO.FileStream FsContent = new System.IO.FileStream(Server.MapPath("~/resource/email_templates/reportincorrect.html"), System.IO.FileMode.Open, System.IO.FileAccess.Read);
            System.IO.StreamReader SRcontent = new System.IO.StreamReader(FsContent);
            Message = SRcontent.ReadToEnd();
            FsContent.Close();
            SRcontent.Close();
            FsContent.Dispose();
            SRcontent.Dispose();
            Message = Message.Replace("{0}", username);
            Message = Message.Replace("{1}", businessname);
            Message = Message.Replace("{2}", reportdetail);
            Message = Message.Replace("{3}", email);
            
            Message = Message.Replace("{20}", ConfigurationManager.AppSettings["WebsiteRootPath"].ToString());
            Email.Subject = ConfigurationManager.AppSettings["SubjectreportincorrectTOAdmin"].ToString() + " - " + businessname;
            Email.From = new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["networkUserIdvcHelpdesk"].ToString(), "VConnect.com");
            Email.IsBodyHtml = true;
            Email.Body = Message;
            Email.To.Add(ConfigurationManager.AppSettings["EmailToreportincorrectTOAdmin"].ToString());
            //Email.To.Add("devinder.kaur@aardeesoft.com");
            //BLLEmail.EmailSending(ref Email, ConfigurationManager.AppSettings["OthersUserId"].ToString().Trim(), ConfigurationManager.AppSettings["OthersUserPassword"].ToString().Trim());
            SendEmail send = new SendEmail();
            try
            {
                send.SendMailToUser(ref Email, ConfigurationManager.AppSettings["networkUserIdvcHelpdesk"].ToString(), ConfigurationManager.AppSettings["networkUserIdPasswordvcHelpdesk"].ToString());
            }
            catch (Exception e)
            {
                log.LogMe(e);
            }

        }
        #endregion

        #region Widget
        public ActionResult BizDetailWidget(long? bizID)
        {
            BizDetailWEB bizDetailWEB = new BizDetailWEB();
            BizDetails bizDetails = new BizDetails();
            //BusinessLead businesslead = new BusinessLead();
            if (bizID.HasValue)
            {
                int leadid = 0;
                if (bizDetailWEB.GetBizWidget(bizID.Value) != null)
                    leadid = bizDetailWEB.GetBizWidget(Convert.ToInt32(bizID.Value)).leadid;
                if (leadid != 0)
                {
                    bizDetails = bizDetailWEB.clentdesign(bizID, "business");
                    //bizDetails.businesslead = businesslead;
                    if (bizDetails.businesslead != null && bizDetails.openaccount != null && bizDetails.openaccount.Count > 0)
                    {
                        return View("bizdetailwidget", bizDetails);
                    }
                    else
                    {
                        return RedirectPermanent("/");
                    }
                }
            }
            return RedirectPermanent("/");
        }
        public ActionResult catWidget(string widgeturl)
        {
            BizDetailWEB bizDetailWEB = new BizDetailWEB();
            BizDetails bizDetails = new BizDetails();
            CategoryWidget categoryWidget = new CategoryWidget();
            try
            {
                categoryWidget = bizDetailWEB.getWidgetContentid(widgeturl);                
                if (categoryWidget.contentid > 0)
                {                    
                    bizDetails = bizDetailWEB.clentdesign(categoryWidget.contentid, "category");                    
                    if (bizDetails.businesslead != null && bizDetails.openaccount != null && bizDetails.openaccount.Count > 0)
                    {
                        return View("CategoryWidget", bizDetails);
                    }
                    else
                    {
                        return RedirectPermanent("/");
                    }                    
                }
                else
                {
                    return RedirectPermanent("/");
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }            
            return RedirectPermanent("/");
            //return RedirectToActionPermanent("CategoryWidget", new { catID = categoryWidget.contentid });
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult phonenumberWidget(string phonenumber, int? userid)
        {
            if ((!Request.AcceptTypes.Contains("text/html")))
            {
                int d = 4;
                if (!string.IsNullOrEmpty(phonenumber))
                {
                    int result1 = 0, result = 0;
                    result = Vconnect.Common.Utility.isValidPhone(phonenumber);
                    //if (userid.HasValue)
                    //    result1 = Vconnect.Common.Utility.businessphonechk(phonenumber, userid.Value);
                    string sContactNumber = phonenumber;
                    string subSection = sContactNumber.Substring(0, 1);
                    if (subSection != "0")
                    {
                        d = 5;
                    }
                    else if (result == 0)
                    {
                        d = 2;
                    }
                    //else if (result1 == 1)
                    //{
                    //    d = 1;
                    //}
                    else
                    {
                        d = 0;
                    }
                }
                else
                {
                    d = 4;
                }

                return Json(d, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CategoryWidget(long? catID)
        {
            BizDetailWEB bizDetailWEB = new BizDetailWEB();
            BizDetails bizDetails = new BizDetails();          
            if (catID.HasValue)
            {
                bizDetails = bizDetailWEB.clentdesign(catID.Value, "category");                
                if (bizDetails.businesslead != null && bizDetails.openaccount != null && bizDetails.openaccount.Count > 0)
                {
                    return View("CategoryWidget", bizDetails);
                }
                else
                {
                    return RedirectPermanent("/");
                }                
            }
            return RedirectPermanent("/");
        }
        #endregion

        #region For Dynamic Design Submit
        public ActionResult SaveLeadData(string listid, string listvalue, string leadid, string listmessage, string listRequired, string uniqueid, string submit, string widgetname, string listpattern, string sussesssms)
        {
            if (string.IsNullOrEmpty(listid) && string.IsNullOrEmpty(listvalue) && string.IsNullOrEmpty(leadid) && string.IsNullOrEmpty(listmessage) && string.IsNullOrEmpty(listRequired) && string.IsNullOrEmpty(uniqueid) && string.IsNullOrEmpty(submit) && string.IsNullOrEmpty(widgetname) && string.IsNullOrEmpty(listpattern))
            {
                 return Json(null, JsonRequestBehavior.AllowGet);
            }
            else
            {
            string widget = "";
            if (string.IsNullOrEmpty(widgetname))
                widgetname = "Widget";
            if (!string.IsNullOrEmpty(submit) && submit == "category")
                widget = "cw_" + leadid;
            else
                widget = "bw_" + leadid;
            List<RegisterUserClients> registerclients = new List<RegisterUserClients>();
            int Error = 0;
            string uniquevalue = string.Empty;
            string result = string.Empty;
            var columnname = listid.Split(';').ToList();
            var columnvalue = listvalue.Split(';').ToList();
            var patterntype = listpattern.Split(';').ToList();
            var columnRequired = listRequired.Split(';').ToList();
            var columnmessage = listmessage.Split(';').ToList();

            for (int j = 0; j < columnRequired.Count - 1; j++)
            {
                if (columnRequired[j].Trim() == "Required" && columnRequired[j].Trim() != "," && (string.IsNullOrEmpty(columnvalue[j].Trim()) || columnvalue[j].Trim() == "," || columnvalue[j].Trim() == "undefined" || columnvalue[j].Trim() == "Please select"))
                {
                    return Json(columnmessage[j].Trim()+"    ", JsonRequestBehavior.AllowGet);
                }
            }
            string query = string.Empty;
            var queryvalue = "GETDATE()," + "$$" + leadid + "$$,$$WEB$$," + "$$" + widget + "$$," + "$$" + widgetname + "$$,";
            //Pass query string
            var querycolumn = "createddate,leadid,source,widget,clientname,";
            for (int k = 0; k < columnvalue.Count - 1; k++)
            {
                if (!string.IsNullOrEmpty(uniqueid) && columnname[k] == uniqueid)
                {
                    uniquevalue = columnvalue[k];
                }
                if (!string.IsNullOrEmpty(columnvalue[k].Trim()))
                {
                    //for values of query
                    if (columnvalue[k].Substring(0, 1) == ",")
                    {
                        if (k == 0)
                        {
                            if ((patterntype[k].Trim() == "calender" || patterntype[k].Trim() == "dob") && !string.IsNullOrEmpty(columnvalue[k].Trim()))
                                queryvalue += "$$" + Convert.ToDateTime(columnvalue[k].Remove(0, 1).Trim()) + "$$";
                            else
                                queryvalue += "$$" + columnvalue[k].Remove(0, 1).Trim() + "$$";
                        }
                        else
                        {
                            if ((patterntype[k].Trim() == "calender" || patterntype[k].Trim() == "dob") && !string.IsNullOrEmpty(columnvalue[k].Trim()))
                                queryvalue += ",$$" + Convert.ToDateTime(columnvalue[k].Remove(0, 1).Trim()) + "$$";
                            else
                                queryvalue += ",$$" + columnvalue[k].Remove(0, 1).Trim() + "$$";
                        }
                    }
                    else
                    {
                        if (k == 0)
                        {
                            if ((patterntype[k].Trim() == "calender" || patterntype[k].Trim() == "dob") && !string.IsNullOrEmpty(columnvalue[k].Trim()))
                                queryvalue += "$$" + Convert.ToDateTime(columnvalue[k].Trim()) + "$$";
                            else
                                queryvalue += "$$" + columnvalue[k].Trim() + "$$";
                        }
                        else
                        {
                            if ((patterntype[k].Trim() == "calender" || patterntype[k].Trim() == "dob") && !string.IsNullOrEmpty(columnvalue[k].Trim()))
                                queryvalue += ",$$" + Convert.ToDateTime(columnvalue[k].Trim()) + "$$";
                            else
                                queryvalue += ",$$" + columnvalue[k].Trim() + "$$";
                        }
                    }
                    //for column name of query
                    if (columnname[k].Substring(0, 1) == ",")
                    {
                        if (k == 0)
                            querycolumn += columnname[k].Remove(0, 1).Trim();
                        else
                            querycolumn += "," + columnname[k].Remove(0, 1).Trim();
                    }
                    else
                    {
                        if (k == 0)
                            querycolumn += columnname[k].Trim();
                        else
                            querycolumn += "," + columnname[k].Trim();
                    }
                }
            }
            query = "insert into clientadds (" + querycolumn.Replace(",,", ",").Replace(" ", "_") + ") values(" + queryvalue.Replace(",,", ",") + ")";
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[ClientUser_Registraion]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@leadid", leadid.ToString().Trim()));
                cmd.Parameters.Add(new SqlParameter("@query", query.ToString().Trim()));
                cmd.Parameters.Add(new SqlParameter("@Column", uniqueid.ToString().Trim()));
                cmd.Parameters.Add(new SqlParameter("@Value", uniquevalue.ToString().Trim()));
                cmd.Parameters.Add(new SqlParameter("@widget", widget.Trim()));
                cmd.Parameters.Add(new SqlParameter("@Error", SqlDbType.Int));
                cmd.Parameters["@Error"].Direction = ParameterDirection.Output;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    Error = Convert.ToInt32(cmd.Parameters["@Error"].Value.ToString());
                    if (Error == 4 && leadid == "21" && submit == "category")
                    {
                        if (!string.IsNullOrEmpty(sussesssms))
                            TempData["SuccessMessage"] = sussesssms;
                        else
                            TempData["SuccessMessage"] = "Thank You";
                        sussesssms = "1";
                    }
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
                finally
                {
                    //db.Database.Connection.Close();
                }
            }
            
            var resultdata = new { Error = Error, sussesssms = sussesssms };
            return Json(resultdata, JsonRequestBehavior.AllowGet);
            // return Redirect("/");
            }
        }
        public ActionResult AutoBinddatadrop(string query)
        {
            List<Listdropbind> dropbind = new List<Listdropbind>();
            string drop1 = "";
            int drop2 = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[Client_Binddatadrop]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@query", query));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    dropbind = ((IObjectContextAdapter)db).ObjectContext.Translate<Listdropbind>(reader).ToList();
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    //db.Database.Connection.Close();
                }
                drop1 += "<option value=''>Please select</option>";
                int i = 1;
                foreach (var item in dropbind.OrderBy(m => m.drptext))
                {
                    drop1 += "<option value=" + item.drpvalue + ">" + item.drptext + "</option>";
                    i++;
                }
                drop2 = dropbind.Count;
                var result = new { drop1 = drop1, drop2 = drop2 };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}
