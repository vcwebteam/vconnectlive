﻿using MailChimp.Types;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using Vconnect.Common;
using Vconnect.Mapping;
using Vconnect.Models;


namespace Vconnect.Controllers
{
    public class HomeWEBController : Controller
    {       
        UserSession objUS = new UserSession();
        private static string locState = string.Empty, locSearchLocation = string.Empty;
        public ActionResult UnderConstruction()
        {
            return Content("This part is Under Construction");
        }

        private int emailcheck(string email)
        {
            int d = 5;
            Regex rgx = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
            if (!string.IsNullOrEmpty(email.Trim().TrimEnd().TrimStart()) && !string.IsNullOrWhiteSpace(email.Trim().TrimEnd().TrimStart()))
            {
                if (!rgx.IsMatch(email))
                {
                    d = 1;
                }
            }
            return d;
        }
        public ActionResult submitdealsdata( string email, string location, string gender)
        {
            int result = 0;
            if (emailcheck(email) == 1)
            {
                result = 5;               
            }
            else
            {
                if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(location) && !string.IsNullOrEmpty(gender))
                {
                    HomeModel model = new HomeModel();
                    result = model.submitdealsdata(email, location, gender);
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddwebsiteLogg(string sourceurl, string previousurl)
        {           
            //string sourceurl = string.Empty;
            //string previousurl = string.Empty;          


            //sourceurl = TempData.Peek("source") != null ? TempData.Peek("source").ToString() : "";
            //previousurl = TempData.Peek("previous") != null ? TempData.Peek("previous").ToString() : "";


            //TempData.Keep("source");
            //TempData.Keep("previous");
            int result = 0;
            string strmsg = string.Empty;
            try
            {
                int userId = 0;
                if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) )
                {
                    userId = int.Parse(objUS.SV_VCUserContentID);
                }
                //AssignHTTPVariables();
                //Session["IPADDRESS"] = MyGlobalVariables.GetIpAddress;
                //Session["REVERSEDNS"] = MyGlobalVariables.GetReverseDns;
                //if (MyGlobalVariables.GetIpAddress == "0")
                //{
                //    try
                //    {
                //        Session["IPADDRESS"] = Vconnect.MvcApplication.GetVisitorIPAddress();
                //    }
                //    catch { }
                //}
                //if (MyGlobalVariables.GetReverseDns == "0")
                //{
                //    try{Session["REVERSEDNS"] = Vconnect.MvcApplication.GetReverseDns(Session["IPADDRESS"].ToString());}catch { }
                //}
                string IPAddress = string.Empty;
                string ReverseDNS = string.Empty;

                IPAddress = Vconnect.MvcApplication.GetVisitorIPAddress();
                ReverseDNS = !string.IsNullOrEmpty(IPAddress) ? Vconnect.MvcApplication.GetReverseDns(IPAddress.ToString()) : "";
                result = log.AddwebsiteLog(userId, IPAddress, previousurl, sourceurl, "", Utility.GetDomain(), ReverseDNS);
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            return Json(strmsg, JsonRequestBehavior.AllowGet);
        }
      
        public JsonResult AddwebsiteLog()
        {
            int result = 0;
            string strmsg = string.Empty;
            try
            {
                int userId = 0;
                if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && !string.IsNullOrEmpty(objUS.SV_VCLoginID))
                {
                    userId = Convert.ToInt32(objUS.SV_VCUserContentID);
                }
                AssignHTTPVariables();
                //Session["IPADDRESS"] = MyGlobalVariables.GetIpAddress;
                //Session["REVERSEDNS"] = MyGlobalVariables.GetReverseDns;
                try
                {
                    Session["IPADDRESS"] = Vconnect.MvcApplication.GetVisitorIPAddress();
                }
                catch
                { }
                try
                {
                    Session["REVERSEDNS"] = Vconnect.MvcApplication.GetReverseDns(Session["IPADDRESS"].ToString());
                }
                catch
                { }
                //if (MyGlobalVariables.GetIpAddress == "0")
                //{
                //    try
                //    {
                //        Session["IPADDRESS"] = Vconnect.MvcApplication.GetVisitorIPAddress();
                //    }
                //    catch
                //    { }
                //}
                //if (MyGlobalVariables.GetReverseDns == "0")
                //{
                //    try
                //    {
                //        Session["REVERSEDNS"] = Vconnect.MvcApplication.GetReverseDns(Session["IPADDRESS"].ToString());
                //    }
                //    catch
                //    { }
                //}
                result = log.AddwebsiteLog(userId, MyGlobalVariables.GetIpAddress, MyGlobalVariables._previousUrl, MyGlobalVariables._sourceUrl, "", Utility.GetDomain(), MyGlobalVariables.GetReverseDns);
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            return Json(strmsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult HomeWEB(string loc)
        {
            HomeModel model = new HomeModel();
            loc = string.IsNullOrEmpty(loc) ? "Lagos" : loc;
            try
            {
                model.lstFeaturedBusinesses = model.FeaturedBannerResultFunc(loc);
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            try
            {
                if (model.lstFeaturedBusinesses.FirstOrDefault().Error == 1)
                {
                    Response.Clear(); Response.StatusCode = 404;
                    Response.Status = "404 NotFound";
                    return View("../Static/404");
                    //return Redirect("/");
                }
                else
                {
                    if (!string.IsNullOrEmpty(loc))
                    {
                        if (loc.Contains('-'))
                        {
                            locState = loc.Split('-')[0].Replace('_', ' ');
                            locSearchLocation = loc.Split('-')[1].Replace('_', ' ');
                            model.SearchLocation = loc.Split('-')[1].Replace('_', ' ');
                        }
                        else
                        {
                            locState = loc.Replace('_', ' ');
                            locSearchLocation = loc.Replace('_', ' ');
                            model.SearchLocation = loc.Replace('_', ' ');
                        }
                        model._SelectedLocation = loc.Replace(' ', '_');
                        Utility.CreateCokkies(model.SelectedStateId.ToString(), model._SelectedLocation, model.SearchText, model.SearchLocation);
                    }
                    model.SearchTextHeader = model.SearchText;
                    model.SearchLocationHeader = model.SearchLocation;
                    FetchRandomFooterBanner();

                    #region check the isclaim for BO dropdown
                    if (Session["isclaim"] == null || string.IsNullOrEmpty(Session["isclaim"].ToString()))
                    {
                        List<Vconnect.Models.BoDashboardModel.ClaimedBusinessList> claimedbusinesslist = new List<BoDashboardModel.ClaimedBusinessList>();
                        Vconnect.Controllers.BoDashboardWebController BOdashboard = new Vconnect.Controllers.BoDashboardWebController();
                        int userId = 0;
                        if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && !string.IsNullOrEmpty(objUS.SV_VCLoginID))
                        {
                            userId = Convert.ToInt32(objUS.SV_VCUserContentID);
                            claimedbusinesslist = BOdashboard.getbusiness(userId.ToString());
                            if (claimedbusinesslist != null && claimedbusinesslist.Count > 0)
                            {
                                model.isclaim = true;
                                Session["isclaim"] = true;
                            }
                        }
                    }
                    else if (Convert.ToBoolean(Session["isclaim"]) == true)
                    {
                        model.isclaim = true;
                    }
                    #endregion


                    Response.StatusCode = 200;
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            ViewBag.isHomePage = true;
            return View(model);
        }

        private void AssignHTTPVariables()
        {
            string AbsoluteURI = string.Empty;
            if (Request.Url.AbsoluteUri == null || Request.Url.AbsoluteUri.ToString().Trim() == "")
            {
                AbsoluteURI = "-";
            }
            else
            {
                AbsoluteURI = Request.Url.AbsoluteUri.ToString().Replace("~", "");
            }
            if (Request.UrlReferrer == null || Request.UrlReferrer.ToString().Trim() == "")
            {
                MyGlobalVariables._previousUrl = "-";
            }
            else
            {
                MyGlobalVariables._previousUrl = Request.UrlReferrer.ToString().ToLower().Replace("//", "~~").Replace("/", "~").Replace(".html", ".indexpage").Replace("_", "~").Replace("-", "~").Trim();
            }
            MyGlobalVariables._sourceUrl = AbsoluteURI.ToLower().Replace("//", "~~").Replace("/", "~").Replace(".html", ".indexpage").Replace("_", "~").Replace("-", "~").Trim();
        }


        private void AddwebsiteLog(int userId, string ipAddress, string previousUrl, string pageUrl, string pageType, string domain, string reverseDns)
        {
            try
            {
                int result = 0;
                result = log.AddwebsiteLog(userId, ipAddress, previousUrl, pageUrl, pageType, domain, reverseDns);
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }

        }

        //private void FetchRandomFooterBanner()
        //{
        //    try
        //    {
        //        var doc = new XmlDocument();
        //        doc.Load(Server.MapPath("~/Common/files/randomfooterbanner.xml"));
        //        var node = doc.SelectSingleNode("radfooterbanner/footerbanner/key");
        //        if (node != null)
        //        {
        //            ViewBag.sRandomfooterbanner = node.InnerText;
        //            if (!string.IsNullOrEmpty(ViewBag.sRandomfooterbanner))
        //            {
        //                node.InnerText = ViewBag.sRandomfooterbanner == "1" ? "2" : "1";
        //                doc.Save(Server.MapPath("~/Common/files/randomfooterbanner.xml"));
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.LogMe(ex);
        //    }
        //}
        
        private void FetchRandomFooterBanner()
        {
            try
            {
                Random rand = new Random();
                int randno = rand.Next(1, 5);
                ViewBag.sRandomfooterbanner = randno.ToString();                
                //var doc = new XmlDocument();
                //doc.Load(Server.MapPath("~/Common/files/randomfooterbanner.xml"));
                //var node = doc.SelectSingleNode("radfooterbanner/footerbanner/key");
                //if (node != null)
                //{
                //    ViewBag.sRandomfooterbanner = node.InnerText;
                //    if (!string.IsNullOrEmpty(ViewBag.sRandomfooterbanner))
                //    {
                //        node.InnerText = ViewBag.sRandomfooterbanner == "1" ? "2" : ViewBag.sRandomfooterbanner == "2" ? "3" : ViewBag.sRandomfooterbanner == "3" ? "4" : "1";
                //        doc.Save(Server.MapPath("~/Common/files/randomfooterbanner.xml"));
                //    }
                //}
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
        }

        
        [ValidateInput(false)]
        [HttpPost]

        public ActionResult submit(FormCollection collection)
        {

            string url = string.Empty;
            string keyword = string.Empty, location = string.Empty;
            int searchtype = 0, isbranch = 0, locationid = 0;
            string[] strSrchLoc = new string[] { };
            string pattern = @"[^a-zA-Z0-9_]", searchTextUrl = string.Empty, searchLocationUrl = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(collection["SearchText"]))
                {
                    keyword = collection["SearchText"].ToString();
                }
                else if (!string.IsNullOrEmpty(collection["SearchTextHeader"]))
                {
                    keyword = collection["SearchTextHeader"].ToString();
                }
                if (!string.IsNullOrEmpty(collection["SearchLocation"]))
                {
                    location = collection["SearchLocation"].ToString();
                }
                else if (!string.IsNullOrEmpty(collection["SearchLocationHeader"]))
                {
                    location = collection["SearchLocationHeader"].ToString();
                }
                HomeModel model = new HomeModel();                
                model.SearchText = Regex.Replace(Regex.Replace(Regex.Replace(keyword.Trim(), pattern, " ").Trim(), @"\s+", " ").Trim(), pattern, "-");
                model.SearchTextHeader = model.SearchText;

                if (location.Contains(','))
                {
                    strSrchLoc = location.Split(',');
                    if (strSrchLoc.Count() > 2)
                    {
                        model.SearchLocation = string.Format("{0}-{1}-{2}", strSrchLoc[2].Trim().Replace(' ', '_').Replace("-", "_"), strSrchLoc[1].Trim().Replace(' ', '_').Replace("-", "_"), strSrchLoc[0].Trim().Replace(' ', '_').Replace("-", "_"));
                    }
                    else
                    {
                        model.SearchLocation = string.Format("{0}-{1}", strSrchLoc[1].Trim().Replace(' ', '_').Replace("-", "_"), strSrchLoc[0].Trim().Replace(' ', '_').Replace("-", "_"));
                    }
                }
                else
                {
                    model.SearchLocation = Regex.Replace(Regex.Replace(Regex.Replace(location.Trim(), pattern, " ").Trim(), @"\s+", " ").Trim(), pattern, "_"); 
                }
                
               // model.SearchLocation = location.Contains(',') ? location.Split(',')[0].Trim().Replace(' ', '_') : location.Replace(' ', '_');
                if (!string.IsNullOrEmpty(collection["KeyWordValue"]))
                {
                    searchtype = Convert.ToInt16(collection["KeyWordValue"].ToString().Split(',')[0]);
                    isbranch = Convert.ToInt16(collection["KeyWordValue"].ToString().Split(',')[2]);
                    searchTextUrl = collection["KeyWordValue"].ToString().Trim().Split(',')[1];
                }
                else if (!string.IsNullOrEmpty(collection["KeyWordValueHeader"]))
                {
                    searchtype = Convert.ToInt16(collection["KeyWordValueHeader"].ToString().Split(',')[0]);
                    searchTextUrl = collection["KeyWordValueHeader"].ToString().Trim().Split(',')[1];
                    isbranch = Convert.ToInt16(collection["KeyWordValueHeader"].ToString().Split(',')[2]);
                }
                else
                {
                    searchTextUrl = Regex.Replace(Regex.Replace(Regex.Replace(keyword.Trim(), pattern, " ").Trim(), @"\s+", " ").Trim(), pattern, "-");
                }
                if (!string.IsNullOrEmpty(collection["LocationValue"]))
                {
                    model._SelectedLocation = collection["LocationValue"].ToString().Split(',')[2];
                    locSearchLocation = model._SelectedLocation.Contains("-") ? model._SelectedLocation.Split('-')[1].Replace('_', ' ') : model._SelectedLocation.Replace('_', ' ');
                    model.SelectedStateId = Convert.ToInt32(collection["LocationValue"].Split(',')[0]);
                    searchLocationUrl = collection["LocationValue"].ToString().Split(',')[2];
                    locationid = Convert.ToInt32(collection["LocationValue"].ToString().Split(',')[3]);
                    model.SearchLocation = string.Format("{0},{1}", model.SearchLocation, locationid.ToString());
                }
                else if (!string.IsNullOrEmpty(collection["LocationValueHeader"]))
                {
                    model._SelectedLocation = collection["LocationValueHeader"].ToString().Split(',')[2];
                    locSearchLocation = model._SelectedLocation.Contains("-") ? model._SelectedLocation.Split('-')[1].Replace('_', ' ') : model._SelectedLocation.Replace('_', ' ');
                    //locSearchLocation = model._SelectedLocation;
                    model.SelectedStateId = Convert.ToInt32(collection["LocationValueHeader"].Split(',')[0]);
                    searchLocationUrl = collection["LocationValueHeader"].ToString().Split(',')[2];
                    locationid = Convert.ToInt32(collection["LocationValueHeader"].ToString().Split(',')[3]);
                    model.SearchLocation = string.Format("{0},{1}", model.SearchLocation, locationid.ToString());
                }
                else if (locSearchLocation.ToLower() == location.ToLower() && Request.Cookies["vcSelectedLocation"]!=null)
                {
                    searchLocationUrl = Request.Cookies["vcSelectedLocation"].Value;
                    model._SelectedLocation = searchLocationUrl;
                }

                else
                {
                    //searchLocationUrl = Regex.Replace(Regex.Replace(Regex.Replace(location.Trim(), pattern, " ").Trim(), @"\s+", " ").Trim(), pattern, "_");
                    searchLocationUrl = model.SearchLocation;
                    model._SelectedLocation = string.Empty;
                    //model.SearchLocation = location;
                }
                Utility.CreateCokkies(model.SelectedStateId.ToString(), string.IsNullOrEmpty(model._SelectedLocation) ? "Lagos" : model._SelectedLocation, model.SearchText, model.SearchLocation);

                if (searchtype > 0)
                {
                    if (searchtype < 50)
                    {
                        if (!string.IsNullOrEmpty(model._SelectedLocation))
                        {
                            url = string.Format("/{0}/{1}", searchLocationUrl, searchTextUrl);
                        }
                        else
                        {
                            url = RoutingUtility.GetSearchWebURLNew(searchLocationUrl, Regex.Replace(Regex.Replace(Regex.Replace(keyword.Trim(), pattern, " ").Trim(), @"\s+", " ").Trim(), pattern, "-"));
                        }
                    }
                    else if (searchtype == 50 && isbranch > 0)
                    {
                        if (locationid > 0)
                        {
                            url = RoutingUtility.GetSearchLocWebURLNew(searchLocationUrl, Regex.Replace(Regex.Replace(Regex.Replace(keyword.Trim(), pattern, " ").Trim(), @"\s+", " ").Trim(), pattern, "-"), locationid);
                        }
                        else
                        {
                            url = RoutingUtility.GetSearchWebURLNew(searchLocationUrl, Regex.Replace(Regex.Replace(Regex.Replace(keyword.Trim(), pattern, " ").Trim(), @"\s+", " ").Trim(), pattern, "-"));
                        }
                    }
                    else
                    {
                        url = string.Format("/{0}", searchTextUrl);
                        Utility.CreateCokkies(model.SelectedStateId.ToString(), string.IsNullOrEmpty(model._SelectedLocation) ? "Lagos" : model._SelectedLocation, model.SearchText, model._SelectedLocation);
                    }
                }
                else if (locationid > 0)
                {
                    url = RoutingUtility.GetSearchLocWebURLNew(searchLocationUrl, searchTextUrl, locationid);
                }
                else
                {
                    url = RoutingUtility.GetSearchWebURLNew(searchLocationUrl, searchTextUrl);
                }
                Response.StatusCode = 200;
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            //Response.Clear(); Response.StatusCode = 200;
            //Response.Status = "200 ok";
           
            return Redirect(string.IsNullOrEmpty(url) ? "/searchnotfound" : url.ToLower());
        }

        [ValidateInput(false)]
        public ActionResult getSearchResult(string SearchText, string SearchTextHeader, string SearchLocation, string SearchLocationHeader, string KeyWordValue, string KeyWordValueHeader, string LocationValue, string LocationValueHeader)
        {
            string url = string.Empty;
            string keyword = string.Empty, location = string.Empty;
            int searchtype = 0, isbranch = 0, locationid = 0;
            string[] strSrchLoc = new string[] { };
            string pattern = @"[^a-zA-Z0-9_]", searchTextUrl = string.Empty, searchLocationUrl = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(SearchText))
                {
                    keyword = SearchText.ToString();
                }
                //else if (!string.IsNullOrEmpty(SearchTextHeader))
                //{
                //    keyword = SearchTextHeader.ToString();
                //}
                if (!string.IsNullOrEmpty(SearchLocation))
                {
                    location = SearchLocation.ToString();
                }
                //else if (!string.IsNullOrEmpty(SearchLocationHeader))
                //{
                //    location = SearchLocationHeader.ToString();
                //}
                HomeModel model = new HomeModel();
                model.SearchText = Regex.Replace(Regex.Replace(Regex.Replace(keyword.Trim(), pattern, " ").Trim(), @"\s+", " ").Trim(), pattern, "-");
                model.SearchTextHeader = model.SearchText;

                if (location.Contains(','))
                {
                    strSrchLoc = location.Split(',');
                    if (strSrchLoc.Count() > 2)
                    {
                        //model.SearchLocation = string.Format("{0}-{1}-{2}", strSrchLoc[2].Trim().Replace(' ', '_').Replace("-", "_"), strSrchLoc[1].Trim().Replace(' ', '_').Replace("-", "_"), strSrchLoc[0].Trim().Replace(' ', '_').Replace("-", "_"));
                        model.SearchLocation = string.Format("{0}-{1}-{2}", strSrchLoc[2].Trim().Replace(' ', '_').Replace("-", "_"), strSrchLoc[1].Trim().Replace(' ', '_').Replace("-", "_"), strSrchLoc[0].Trim().Replace(' ', '_').Replace("-", "_"));
                    }
                    else
                    {
                        //model.SearchLocation = string.Format("{0}-{1}", strSrchLoc[1].Trim().Replace(' ', '_').Replace("-", "_"), strSrchLoc[0].Trim().Replace(' ', '_').Replace("-", "_"));
                        model.SearchLocation = string.Format("{0}-{1}", strSrchLoc[1].Trim().Replace(' ', '_').Replace("-", "_"), strSrchLoc[0].Trim().Replace(' ', '_').Replace("-", "_"));
                    }
                }
                else
                {
                    model.SearchLocation = Regex.Replace(Regex.Replace(Regex.Replace(location.Trim(), pattern, " ").Trim(), @"\s+", " ").Trim(), pattern, "_");
                }

                // model.SearchLocation = location.Contains(',') ? location.Split(',')[0].Trim().Replace(' ', '_') : location.Replace(' ', '_');
                if (!string.IsNullOrEmpty(KeyWordValue))
                {
                    searchtype = Convert.ToInt16(KeyWordValue.ToString().Split(',')[0]);
                    isbranch = Convert.ToInt16(KeyWordValue.ToString().Split(',')[2]);
                    searchTextUrl = KeyWordValue.ToString().Trim().Split(',')[1];
                }
                else if (!string.IsNullOrEmpty(KeyWordValueHeader))
                {
                    searchtype = Convert.ToInt16(KeyWordValueHeader.ToString().Split(',')[0]);
                    searchTextUrl = KeyWordValueHeader.ToString().Trim().Split(',')[1];
                    isbranch = Convert.ToInt16(KeyWordValueHeader.ToString().Split(',')[2]);
                }
                else
                {
                    searchTextUrl = Regex.Replace(Regex.Replace(Regex.Replace(keyword.Trim(), pattern, " ").Trim(), @"\s+", " ").Trim(), pattern, "-");
                }
                if (!string.IsNullOrEmpty(LocationValue))
                {
                    model._SelectedLocation = LocationValue.ToString().Split(',')[2];
                    locSearchLocation = model._SelectedLocation.Contains("-") ? model._SelectedLocation.Split('-')[1].Replace('_', ' ') : model._SelectedLocation.Replace('_', ' ');
                    model.SelectedStateId = Convert.ToInt32(LocationValue.Split(',')[0]);
                    searchLocationUrl = LocationValue.ToString().Split(',')[2];
                    locationid = Convert.ToInt32(LocationValue.ToString().Split(',')[3]);
                    model.SearchLocation = string.Format("{0},{1}", model.SearchLocation, locationid.ToString());
                }
                else if (!string.IsNullOrEmpty(LocationValueHeader))
                {
                    model._SelectedLocation = LocationValueHeader.ToString().Split(',')[2];
                    locSearchLocation = model._SelectedLocation.Contains("-") ? model._SelectedLocation.Split('-')[1].Replace('_', ' ') : model._SelectedLocation.Replace('_', ' ');
                    //locSearchLocation = model._SelectedLocation;
                    model.SelectedStateId = Convert.ToInt32(LocationValueHeader.Split(',')[0]);
                    searchLocationUrl = LocationValueHeader.ToString().Split(',')[2];
                    locationid = Convert.ToInt32(LocationValueHeader.ToString().Split(',')[3]);
                    model.SearchLocation = string.Format("{0},{1}", model.SearchLocation, locationid.ToString());
                }
                else if (locSearchLocation.ToLower() == location.ToLower() && Request.Cookies["vcSelectedLocation"] != null)
                {
                    searchLocationUrl = Request.Cookies["vcSelectedLocation"].Value;
                    model._SelectedLocation = searchLocationUrl;
                }

                else
                {
                    searchLocationUrl = model.SearchLocation;
                    //searchLocationUrl = Regex.Replace(Regex.Replace(Regex.Replace(location.Trim(), pattern, " ").Trim(), @"\s+", " ").Trim(), pattern, "_");
                    model._SelectedLocation = string.Empty;
                    //model.SearchLocation = location;
                }
                Utility.CreateCokkies(model.SelectedStateId.ToString(), string.IsNullOrEmpty(model._SelectedLocation) ? "Lagos" : model._SelectedLocation, model.SearchText, model.SearchLocation);

                if (searchtype > 0)
                {
                    if (searchtype < 50)
                    {
                        if (!string.IsNullOrEmpty(model._SelectedLocation))
                        {
                            url = string.Format("/{0}/{1}", searchLocationUrl, searchTextUrl);
                        }
                        else
                        {
                            url = RoutingUtility.GetSearchWebURLNew(searchLocationUrl, Regex.Replace(Regex.Replace(Regex.Replace(keyword.Trim(), pattern, " ").Trim(), @"\s+", " ").Trim(), pattern, "-"));
                        }
                    }
                    else if (searchtype == 50 && isbranch > 0)
                    {
                        if (locationid > 0)
                        {
                            url = RoutingUtility.GetSearchLocWebURLNew(searchLocationUrl, Regex.Replace(Regex.Replace(Regex.Replace(keyword.Trim(), pattern, " ").Trim(), @"\s+", " ").Trim(), pattern, "-"), locationid);
                        }
                        else
                        {
                            url = RoutingUtility.GetSearchWebURLNew(searchLocationUrl, Regex.Replace(Regex.Replace(Regex.Replace(keyword.Trim(), pattern, " ").Trim(), @"\s+", " ").Trim(), pattern, "-"));
                        }
                    }
                    else
                    {
                        url = string.Format("/{0}", searchTextUrl);
                        Utility.CreateCokkies(model.SelectedStateId.ToString(), string.IsNullOrEmpty(model._SelectedLocation) ? "Lagos" : model._SelectedLocation, model.SearchText, model._SelectedLocation);
                    }
                }
                else if (locationid > 0)
                {
                    url = RoutingUtility.GetSearchLocWebURLNew(searchLocationUrl, searchTextUrl, locationid);
                }
                else
                {
                    url = RoutingUtility.GetSearchWebURLNew(searchLocationUrl, searchTextUrl);
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            if (string.IsNullOrEmpty(url))
            {
                return Json("/searchnotfound", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(url.ToLower(), JsonRequestBehavior.AllowGet);
            }
            //return Redirect(string.IsNullOrEmpty(url) ? "/searchnotfound" : url.ToLower());
        }

        public ActionResult KeywordAutoComplete(string term)
        {
            HomeModel homeModel = new HomeModel();
            List<SearchKeywords> keywordCollection = new List<SearchKeywords>();
            keywordCollection = homeModel.AutoCompleteSearchText(term);
            var dataJson = keywordCollection.Select(m => new { label = m.content, value = string.Format("{0},{1},{2}", m.contenttype.ToString(), m.url.ToString(), m.isbranch.ToString()) });
            return Json(dataJson, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LocationAutoComplete(string term)
        {
            HomeModel homeModel = new HomeModel();
            List<SearchLocations> locationCollection = new List<SearchLocations>();
            locationCollection = homeModel.AutoCompleteSearchLocation(term);
            var dataJson = locationCollection.Select(m => new { label = m.FullLocation, value = string.Format("{0},{1},{2},{3}", m.stateId.ToString(), m.statename, m.searchlocationurl, m.locationid) });
            return Json(dataJson, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddSubscribe(string email)
        {
            int result = 0;
            string strmsg = string.Empty;
            try
                {
            if (!string.IsNullOrEmpty(email))
            {
                HomeModel homeModel = new HomeModel();                
                
                    result = homeModel.AddSubscribeEmail(email);
               
                if (result == 2)
                {
                    strmsg = "Email Already Exists";
                }
                else if (result == 1)
                {
                    strmsg = "Some Error Occured";
                }
                else
                {
                    strmsg = "Subscribed Successfully";
                    SendAckMailtoNewsletterSubscriber(email);
                }
            }
            else
            {
                strmsg = "Please specify an Email Id.";
            }
                }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            return Json(strmsg, JsonRequestBehavior.AllowGet);
        }
        protected void SendAckMailtoNewsletterSubscriber(string EmailID)
        {
            string Message = ""; 
            System.IO.FileStream FsContent = new System.IO.FileStream(Server.MapPath("~/resource/email_templates/AckMailToNewsletterSubscriber.html"), System.IO.FileMode.Open, System.IO.FileAccess.Read);
            System.IO.StreamReader SRcontent = new System.IO.StreamReader(FsContent);
            Message = SRcontent.ReadToEnd();
            FsContent.Close();
            SRcontent.Close();
            FsContent.Dispose();
            SRcontent.Dispose();
            var recipients = new List<Mandrill.Messages.Recipient>();
            recipients.Add(new Mandrill.Messages.Recipient(EmailID, ""));
            //===================================
            var mandrill = new Mandrill.Messages.Message()
            {
                To = recipients.ToArray(),
                FromEmail = "business@vconnect.com",
                Subject = "VConnect Newsletter Subscription Confirmation",

                Html = Message
            };
            //===================================
 
            SendEmail send = new SendEmail();
            try
            {
                send.VCUsermails(mandrill);
               // send.SendMailToUser(ref Email, ConfigurationManager.AppSettings["networkUserId"].ToString(), ConfigurationManager.AppSettings["networkUserIdpassword"].ToString());
            }
            catch (Exception exp)
            {
                log.LogMe(exp);
            }
        }

        public void logging()
        {
            try
            {
                if (Session["IPADDRESS"] == null || Session["IPADDRESS"] == "")
                    Session["IPADDRESS"] = Request.QueryString["ip"];           
                MyGlobalVariables.GetIpAddress = Session["IPADDRESS"].ToString();
            }
             catch
             {
                 MyGlobalVariables.GetIpAddress = "0";
             }

            try
            {
                if (Session["REVERSEDNS"] == null || Session["REVERSEDNS"] == "")
                    Session["REVERSEDNS"] = MvcApplication.GetReverseDns(Request.QueryString["ip"]);
                MyGlobalVariables.GetReverseDns = Session["REVERSEDNS"].ToString();
            }
            catch
            {
                MyGlobalVariables.GetReverseDns = "0";
            }
        }
        public void HomePageLog()
        {
            int userid = 0;
            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && !string.IsNullOrEmpty(objUS.SV_VCLoginID))
            {
                userid = Convert.ToInt32(objUS.SV_VCUserContentID);
            }
            AssignHTTPVariables();
            Session["IPADDRESS"] = MyGlobalVariables.GetIpAddress;
            Session["REVERSEDNS"] = MyGlobalVariables.GetReverseDns;

            if (MyGlobalVariables.GetIpAddress == "0")
            {
                try
                {
                    Session["IPADDRESS"] = Vconnect.MvcApplication.GetVisitorIPAddress();
                }
                catch
                { }
            }
            if (MyGlobalVariables.GetReverseDns == "0")
            {
                try
                {
                    Session["REVERSEDNS"] = Vconnect.MvcApplication.GetReverseDns(Session["IPADDRESS"].ToString());
                }
                catch
                { }
            }
            Thread thrd_AddwebsiteLog = new Thread(delegate() { AddwebsiteLog(userid, MyGlobalVariables.GetIpAddress, MyGlobalVariables._sourceUrl, MyGlobalVariables._previousUrl, "", Utility.GetDomain(), MyGlobalVariables.GetReverseDns); });
            thrd_AddwebsiteLog.Start();
            thrd_AddwebsiteLog.Join();
        }
#if unused

        //public ActionResult submit(FormCollection collection)
        //{
        //    string url = string.Empty;
        //    string keyword = string.Empty, location = string.Empty;
        //    int searchtype = 0, isbranch = 0, locationid = 0;
        //    string pattern = @"[^a-zA-Z0-9_]", searchTextUrl = string.Empty, searchLocationUrl = string.Empty;
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(collection["SearchText"]))
        //        {
        //            keyword = collection["SearchText"].ToString();
        //        }
        //        else if (!string.IsNullOrEmpty(collection["SearchTextHeader"]))
        //        {
        //            keyword = collection["SearchTextHeader"].ToString();
        //        }
        //        if (!string.IsNullOrEmpty(collection["SearchLocation"]))
        //        {
        //            location = collection["SearchLocation"].ToString();
        //        }
        //        else if (!string.IsNullOrEmpty(collection["SearchLocationHeader"]))
        //        {
        //            location = collection["SearchLocationHeader"].ToString();
        //        }
        //        HomeModel model = new HomeModel();
        //        model.SearchText = Regex.Replace(Regex.Replace(Regex.Replace(keyword.Trim(), pattern, " ").Trim(), @"\s+", " ").Trim(), pattern, "-");
        //        model.SearchTextHeader = model.SearchText;
        //        model.SearchLocation = location;
        //        if (!string.IsNullOrEmpty(collection["KeyWordValue"]))
        //        {
        //            searchtype = Convert.ToInt16(collection["KeyWordValue"].ToString().Split(',')[0]);
        //            isbranch = Convert.ToInt16(collection["KeyWordValue"].ToString().Split(',')[2]);
        //            searchTextUrl = collection["KeyWordValue"].ToString().Trim().Split(',')[1];
        //        }
        //        else if (!string.IsNullOrEmpty(collection["KeyWordValueHeader"]))
        //        {
        //            searchtype = Convert.ToInt16(collection["KeyWordValueHeader"].ToString().Split(',')[0]);
        //            searchTextUrl = collection["KeyWordValueHeader"].ToString().Trim().Split(',')[1];
        //            isbranch = Convert.ToInt16(collection["KeyWordValueHeader"].ToString().Split(',')[2]);
        //        }
        //        else
        //        {
        //            searchTextUrl = Regex.Replace(Regex.Replace(Regex.Replace(keyword.Trim(), pattern, " ").Trim(), @"\s+", " ").Trim(), pattern, "-");
        //        }
        //        if (!string.IsNullOrEmpty(collection["LocationValue"]))
        //        {
        //            model._SelectedLocation = collection["LocationValue"].ToString().Split(',')[2];
        //            model.SelectedStateId = Convert.ToInt32(collection["LocationValue"].Split(',')[0]);
        //            searchLocationUrl = collection["LocationValue"].ToString().Split(',')[2];
        //            locationid = Convert.ToInt32(collection["LocationValue"].ToString().Split(',')[3]);
        //           // model.SearchLocation = location;
        //        }
        //        else if (!string.IsNullOrEmpty(collection["LocationValueHeader"]))
        //        {
        //            model._SelectedLocation = collection["LocationValueHeader"].ToString().Split(',')[2];
        //            model.SelectedStateId = Convert.ToInt32(collection["LocationValueHeader"].Split(',')[0]);
        //            searchLocationUrl = collection["LocationValueHeader"].ToString().Split(',')[2];
        //            locationid = Convert.ToInt32(collection["LocationValueHeader"].ToString().Split(',')[3]);
        //            //model.SearchLocation = location;
        //        }
        //        else
        //        {
        //            searchLocationUrl = Regex.Replace(Regex.Replace(Regex.Replace(location.Trim(), pattern, " ").Trim(), @"\s+", " ").Trim(), pattern, "_");
        //            //model.SearchLocation = location;
        //        }
        //        Utility.CreateCokkies(model.SelectedStateId.ToString(), string.IsNullOrEmpty(model._SelectedLocation) ? "Lagos" : model._SelectedLocation, model.SearchText, model.SearchLocation);

        //        if (searchtype > 0)
        //        {
        //            if (searchtype < 50)
        //            {
        //                url = string.Format("/{0}/{1}", searchLocationUrl, searchTextUrl);
        //            }
        //            else if (searchtype == 50 && isbranch > 0)
        //            {
        //                if (locationid > 0)
        //                {
        //                    url = RoutingUtility.GetSearchLocWebURLNew(searchLocationUrl, Regex.Replace(Regex.Replace(Regex.Replace(keyword.Trim(), pattern, " ").Trim(), @"\s+", " ").Trim(), pattern, "-"), locationid);
        //                }
        //                else
        //                {
        //                    url = RoutingUtility.GetSearchWebURLNew(searchLocationUrl, Regex.Replace(Regex.Replace(Regex.Replace(keyword.Trim(), pattern, " ").Trim(), @"\s+", " ").Trim(), pattern, "-"));
        //                }
        //            }
        //            else
        //            {
        //                url = string.Format("/{0}", searchTextUrl);
        //            }
        //        }
        //        else if (locationid > 0)
        //        {
        //            url = RoutingUtility.GetSearchLocWebURLNew(searchLocationUrl, searchTextUrl, locationid);
        //        }
        //        else
        //        {
        //            url = RoutingUtility.GetSearchWebURLNew(searchLocationUrl, searchTextUrl);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.LogMe(ex);
        //    }
        //    return Redirect(string.IsNullOrEmpty(url) ? "/searchnotfound" : url.ToLower());
        //}
                public ActionResult submit(FormCollection collection)
        {
            string keyword = string.Empty;
            if (!string.IsNullOrEmpty(collection["SearchText"]))
            {
                keyword = collection["SearchText"].ToString();
            }
            else
            {
                keyword = collection["SearchTextHeader"].ToString();
            }
            string location = collection["SearchLocation"].ToString().Contains(",") ? collection["SearchLocation"].Split(',')[1] : collection["SearchLocation"].ToString();
            int searchtype = string.IsNullOrWhiteSpace(collection["KeyWordValue"]) ? 0 : Convert.ToInt16(collection["KeyWordValue"].ToString().Split(',')[0]);
            string locationId = collection["LocationValue"].Split(',')[0];
            if (HttpContext.Request.Cookies["vcSelectedLocId"] != null)
            {
                HttpContext.Request.Cookies.Remove("vcSelectedLocId");
                HttpCookie vcSelectedLocId = new HttpCookie("vcSelectedLocId");
                vcSelectedLocId.Value = string.IsNullOrEmpty(collection["LocationValue"]) ? "125" : collection["LocationValue"].Split(',')[1];
                vcSelectedLocId.Expires = DateTime.Now.AddMonths(3);
                Response.Cookies.Add(vcSelectedLocId);
            }
            HomeModel model = new HomeModel();
            model.SearchText = keyword;
            model.SearchTextHeader = keyword;
            model.SearchLocation = location;
            model.SelectedStateId = Convert.ToInt32(HttpContext.Request.Cookies["vcSelectedLocId"].Value);
            model._SelectedLocation = Utility.GetSelectList().Where(x => x.Value == model.SelectedStateId.ToString()).First().Text;
            if (HttpContext.Request.Cookies.Count > 0)
            {
                if (HttpContext.Request.Cookies["vcSelectedLocation"] != null)
                {
                    HttpContext.Request.Cookies.Remove("vcSelectedLocation");
                    HttpCookie vcSelectedLocation = new HttpCookie("vcSelectedLocation");
                    vcSelectedLocation.Value = model._SelectedLocation;
                    vcSelectedLocation.Expires = DateTime.Now.AddMonths(3);
                    Response.Cookies.Add(vcSelectedLocation);
                }
                if (HttpContext.Request.Cookies["vcSearchText"] != null)
                {
                    HttpContext.Request.Cookies.Remove("vcSearchText");
                    HttpCookie vcSearchText = new HttpCookie("vcSearchText");
                    vcSearchText.Value = keyword;
                    vcSearchText.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(vcSearchText);
                }
                if (HttpContext.Request.Cookies["vcSearchLocation"] != null)
                {
                    HttpContext.Request.Cookies.Remove("vcSearchLocation");
                    HttpCookie vcSearchLocation = new HttpCookie("vcSearchLocation");
                    vcSearchLocation.Value = location;
                    vcSearchLocation.Expires = DateTime.Now.AddMonths(3);
                    Response.Cookies.Add(vcSearchLocation);
                }
            }
            else
            {
                Utility.CreateCokkies(model.SelectedStateId.ToString(), string.IsNullOrEmpty(model._SelectedLocation) ? "Lagos" : model._SelectedLocation, model.SearchText, model.SearchLocation);
                //CreateCokkies(model);
            }
            string url = string.Empty;
            if (searchtype > 0)
            {
                if (searchtype < 50)
                {
                    url = string.Format("/{0}/{1}", location, collection["KeyWordValue"].ToString().Split(',')[1]);
                }
                else if (searchtype == 50 && Convert.ToInt16(collection["KeyWordValue"].ToString().Split(',')[2]) > 0)
                {
                    url = RoutingUtility.GetSearchWebURLNew(location, keyword);
                }
                else
                {
                    url = collection["KeyWordValue"].ToString().Split(',')[1];
                }
            }
            else
            {
                url = RoutingUtility.GetSearchWebURLNew(location, keyword);
            }
            return Redirect(url);
        }

        public ActionResult HomeWebNew(string loc)
        {
            HomeModel model = new HomeModel();
            ViewData["featuredbusiness"] = model.FeaturedBannerResultFunc();

            if (MyGlobalVariables.IsWapWeb == "WAP")
            {
                return View();
            }
            else
            {
                model.SearchTextHeader = model.SearchText;
                model.SearchLocationHeader = model.SearchLocation;
                FetchRandomFooterBanner();
                if (!HttpContext.Request.Cookies.AllKeys.Contains("vcSelectedLocation"))
                {
                    Utility.CreateCokkies(model.SelectedStateId.ToString(), string.IsNullOrEmpty(model._SelectedLocation) ? "Lagos" : model._SelectedLocation, model.SearchText, model.SearchLocation);
                   // CreateCokkies(model);
                }
                return View(model);
            }
        }
        [HttpPost]
        public ActionResult InfinateScroll(int pvCount, string loc)
        {

            JsonModel jsonModel = new JsonModel();
            HomeModel objHomeModel = new HomeModel();
            objHomeModel._SelectedLocation = loc;
            string viewName = "";
            switch (pvCount)
            {
                case 0:
                    jsonModel.NoMoreData = false;
                    viewName = "_HomeSecondScrollPartial";
                    //objHomeModel._FeaturedBannerList = objHomeModel.FeaturedBannerResultFunc();
                    FetchRandomFooterBanner();
                    break;
                //case 1:
                //    jsonModel.NoMoreData = true;
                //    objHomeModel.SearchLocation = objHomeModel._SelectedLocation;
                //    viewName = "../Shared/_WebFooterPartialNew";
                //    break;
            }

            jsonModel.HTMLString = RenderPartialViewToString(viewName, objHomeModel);

            return Json(jsonModel);
        }

        protected string RenderPartialViewToString(string viewName, HomeModel model)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(viewName))
                    viewName = ControllerContext.RouteData.GetRequiredString("action");
                ViewData.Model = model;
                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                    ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                    viewResult.View.Render(viewContext, sw);

                    return sw.GetStringBuilder().ToString();
                }
            }
            catch (Exception e)
            {

                ViewData["IsError"] = "y";
                return "";
            }
        }

        private void CreateCokkies(HomeModel model)
        {
            if (HttpContext.Request.Cookies.AllKeys.Contains("vcSelectedLocId") && HttpContext.Request.Cookies["vcSelectedLocId"] != null)
            {
                HttpContext.Request.Cookies.Remove("vcSelectedLocId");
            }
            if (HttpContext.Request.Cookies.AllKeys.Contains("vcSelectedLocation") && HttpContext.Request.Cookies["vcSelectedLocation"] != null)
            {
                HttpContext.Request.Cookies.Remove("vcSelectedLocation");
            }
            if (HttpContext.Request.Cookies.AllKeys.Contains("vcSearchText") && HttpContext.Request.Cookies["vcSearchText"] != null)
            {
                HttpContext.Request.Cookies.Remove("vcSearchText");
            }
            if (HttpContext.Request.Cookies.AllKeys.Contains("vcSearchLocation") && HttpContext.Request.Cookies["vcSearchLocation"] != null)
            {
                HttpContext.Request.Cookies.Remove("vcSearchLocation");
            }
            HttpCookie vcSelectedLocId = new HttpCookie("vcSelectedLocId");
            vcSelectedLocId.Value = model.SelectedStateId.ToString();
            vcSelectedLocId.Expires = DateTime.Now.AddMonths(3);
            Response.Cookies.Add(vcSelectedLocId);
            HttpCookie vcSelectedLocation = new HttpCookie("vcSelectedLocation");
            vcSelectedLocation.Value = string.IsNullOrEmpty(model._SelectedLocation) ? "Lagos" : model._SelectedLocation;
            vcSelectedLocation.Expires = DateTime.Now.AddMonths(3);
            Response.Cookies.Add(vcSelectedLocation);
            HttpCookie vcSearchText = new HttpCookie("vcSearchText");
            vcSearchText.Value = model.SearchText;
            vcSearchText.Expires = DateTime.Now.AddDays(1);
            Response.Cookies.Add(vcSearchText);
            HttpCookie vcSearchLocation = new HttpCookie("vcSearchLocation");
            vcSearchLocation.Value = model.SearchLocation;
            vcSearchLocation.Expires = DateTime.Now.AddMonths(3);
            Response.Cookies.Add(vcSearchLocation);
        }  
#endif
    }
}
