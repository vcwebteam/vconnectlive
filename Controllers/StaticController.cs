﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Vconnect.Common;
using Vconnect.Models;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Entity.Infrastructure;
using MailChimp.Types;


namespace Vconnect.Controllers
{
    public class StaticController : Controller
    {
        UserSession objUS = new UserSession();
        //
        // GET: /Static/
        //string sourceurl = string.Empty;
        //string previousurl = string.Empty;
        public void bannerclick(string sourceurl, string previousurl)
        {
            try
            {
                if (Request.QueryString["url"] != null)
                {
                    if (TempData.Peek("source") != null && Request.QueryString["url"].ToString().Trim() == TempData.Peek("source").ToString())
                    {
                        sourceurl = TempData.Peek("source") != null ? TempData.Peek("source").ToString() : "";
                        previousurl = TempData.Peek("previous") != null ? TempData.Peek("previous").ToString() : "";

                        TempData.Keep("source");
                        TempData.Keep("previous");
                    }
                    else if (TempData.Peek("sourcehome") != null && Request.QueryString["url"].ToString().Trim() == TempData.Peek("sourcehome").ToString())
                    {
                        sourceurl = TempData.Peek("sourcehome") != null ? TempData.Peek("sourcehome").ToString() : "";
                        previousurl = TempData.Peek("previoushome") != null ? TempData.Peek("previoushome").ToString() : "";

                        TempData.Keep("sourcehome");
                        TempData.Keep("previoushome");
                    }
                    else if (TempData.Peek("sourcelisting") != null && Request.QueryString["url"].ToString().Trim() == TempData.Peek("sourcelisting").ToString())
                    {
                        sourceurl = TempData.Peek("sourcelisting") != null ? TempData.Peek("sourcelisting").ToString() : "";
                        previousurl = TempData.Peek("previouslisting") != null ? TempData.Peek("previouslisting").ToString() : "";

                        TempData.Keep("sourcelisting");
                        TempData.Keep("previouslisting");
                    }
                    else if (TempData.Peek("sourcedetail") != null && Request.QueryString["url"].ToString().Trim() == TempData.Peek("sourcedetail").ToString())
                    {
                        sourceurl = TempData.Peek("sourcedetail") != null ? TempData.Peek("sourcedetail").ToString() : "";
                        previousurl = TempData.Peek("previousdetail") != null ? TempData.Peek("previousdetail").ToString() : "";

                        TempData.Keep("sourcedetail");
                        TempData.Keep("previousdetail");
                    }
                }

                if (Request.QueryString["id"] != null)
                {

                    //Check if user agent is not search engine Crawler
                    if (!System.Web.HttpContext.Current.Request.Browser.Crawler)
                    {
                        SearchListWEBController searchList = new SearchListWEBController();
                        //int userid = (!string.IsNullOrEmpty(objUS.SV_VCLoginID)) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                        int userid = (Request != null && !string.IsNullOrEmpty(objUS.SV_VCUserContentID)) ? Convert.ToInt32(objUS.SV_VCUserContentID) : (System.Web.HttpContext.Current.Request != null && !string.IsNullOrEmpty(objUS.SV_VCUserContentID)) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                        Int64 id = 0;
                        long chk;
                        if (long.TryParse(Request.QueryString["id"].ToString(), out chk))
                        {
                            id = Convert.ToInt64(Request.QueryString["id"].ToString());
                        }

                        

                        searchList.BannerClickLogwithSource(Convert.ToInt32(Request.QueryString["id"].ToString()), System.Web.HttpUtility.HtmlDecode(sourceurl), System.Web.HttpUtility.HtmlDecode(previousurl));
                        
                    }
                    //return Response.Write(System.String.Format("window.open('{0}','_blank')", Request["link"].ToString().Replace("$", "&")));
                    //this.ClientScript.RegisterStartupScript(this.GetType(), this.GetType().Name, string.Format("window.open('{0}', '_blank');", Request["link"].ToString().Replace("$", "&")), true);
                     
                     //Response.RedirectPermanent(Request["link"].ToString().Replace("$", "&"), false);

                    string newUrl = Request["link"].ToString().Replace("$", "&");
                    Response.Clear(); Response.ClearContent(); Response.Status = "301 Moved Permanently";
                    Response.AddHeader("Location", newUrl.Trim()); Response.End();
                    //Write(System.String.Format("window.open('{0}','_blank')", Request["link"].ToString().Replace("$", "&")));
                }
                else
                {
                    Redirect(ConfigurationManager.AppSettings["WebsiteRawPath"].ToString());
                }
            }
            finally
            {
            }
        }
        //public ActionResult bannerclick(string sourceurl,string previousurl)
        //{
        //    try
        //    {
        //        if (Request.QueryString["url"] != null)
        //        {
        //            if (TempData.Peek("source") != null && Request.QueryString["url"].ToString().Trim() == TempData.Peek("source").ToString())
        //            {
        //                sourceurl = TempData.Peek("source") != null ? TempData.Peek("source").ToString() : "";
        //                previousurl = TempData.Peek("previous") != null ? TempData.Peek("previous").ToString() : "";

        //                TempData.Keep("source");
        //                TempData.Keep("previous");
        //            }
        //            else if (TempData.Peek("sourcehome") != null && Request.QueryString["url"].ToString().Trim() == TempData.Peek("sourcehome").ToString())
        //            {
        //                sourceurl = TempData.Peek("sourcehome") != null ? TempData.Peek("sourcehome").ToString() : "";
        //                previousurl = TempData.Peek("previoushome") != null ? TempData.Peek("previoushome").ToString() : "";

        //                TempData.Keep("sourcehome");
        //                TempData.Keep("previoushome");
        //            }
        //            else if (TempData.Peek("sourcelisting") != null && Request.QueryString["url"].ToString().Trim() == TempData.Peek("sourcelisting").ToString())
        //            {
        //                sourceurl = TempData.Peek("sourcelisting") != null ? TempData.Peek("sourcelisting").ToString() : "";
        //                previousurl = TempData.Peek("previouslisting") != null ? TempData.Peek("previouslisting").ToString() : "";

        //                TempData.Keep("sourcelisting");
        //                TempData.Keep("previouslisting");
        //            }
        //            else if (TempData.Peek("sourcedetail") != null && Request.QueryString["url"].ToString().Trim() == TempData.Peek("sourcedetail").ToString())
        //            {
        //                sourceurl = TempData.Peek("sourcedetail") != null ? TempData.Peek("sourcedetail").ToString() : "";
        //                previousurl = TempData.Peek("previousdetail") != null ? TempData.Peek("previousdetail").ToString() : "";

        //                TempData.Keep("sourcedetail");
        //                TempData.Keep("previousdetail");
        //            }
        //        }
            
        //        if (Request.QueryString["id"] != null)
        //        {

        //            //Check if user agent is not search engine Crawler
        //            if (!System.Web.HttpContext.Current.Request.Browser.Crawler)
        //            {
        //                SearchListWEBController searchList = new SearchListWEBController();
        //                //int userid = (!string.IsNullOrEmpty(objUS.SV_VCLoginID)) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
        //                int userid = (Request != null && !string.IsNullOrEmpty(objUS.SV_VCUserContentID)) ? Convert.ToInt32(objUS.SV_VCUserContentID) : (System.Web.HttpContext.Current.Request != null && !string.IsNullOrEmpty(objUS.SV_VCUserContentID)) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
        //                Int64 id = 0;
        //                long chk;
        //                if (long.TryParse(Request.QueryString["id"].ToString(), out chk))
        //                {
        //                    id = Convert.ToInt64(Request.QueryString["id"].ToString());
        //                }
                       
        //                //sourceurl = TempData["sourceURL"] != null ? TempData["sourceURL"].ToString() : "";
        //                //previousurl = TempData["previousURL"] != null ? TempData["previousURL"].ToString() : "";


        //                //sourceurl = TempData["source"] != null ? TempData["sourcelisting"].ToString() : "";
        //                //previousurl = TempData["previous"] != null ? TempData["previous"].ToString() : "";

        //                //TempData.Keep("source");
        //                //TempData.Keep("previous");

        //                //sourceurl = TempData.Peek("source") != null ? TempData.Peek("source").ToString() : "";
        //                //previousurl = TempData.Peek("previous") != null ? TempData.Peek("previous").ToString() : "";

        //                searchList.BannerClickLogwithSource(Convert.ToInt32(Request.QueryString["id"].ToString()), System.Web.HttpUtility.HtmlDecode(sourceurl), System.Web.HttpUtility.HtmlDecode(previousurl));
        //                //if (Request.UrlReferrer == null)
        //                //{
        //                //    Utility.bannerClick(id, "0", "", Convert.ToString(userid), "", Request.UrlReferrer == null ? "" : Request.UrlReferrer.ToString(), Request.ServerVariables["REMOTE_ADDR"].ToString());
        //                //}
        //                //else
        //                //{
        //                //    searchList.BannerClickLog(Convert.ToInt32(Request["id"].ToString()));
        //                //    //if (Request.UrlReferrer.ToString().IndexOf("_b") > 0)
        //                //    //{
        //                //    //    string[] bslst = new string[5];
        //                //    //    bslst = Request.UrlReferrer.ToString().Split('_');

        //                //    //    //string id = bslst[1].ToString().Replace("b", "");
        //                //    //    string biz = bslst[0].ToString().Replace(ConfigurationManager.AppSettings["WebsiteRootPath"].ToString(), "");
        //                //    //   // Utility.bannerClick(Convert.ToInt64(Request["id"].ToString()), "0", biz, Convert.ToString(userid), "", Request.UrlReferrer == null ? "" : Request.UrlReferrer.ToString(), Request.ServerVariables["REMOTE_ADDR"].ToString());
        //                //    //    searchList.BannerClickLog(Convert.ToInt32(Request["id"].ToString()));
        //                //    //}
        //                //    //else
        //                //    //{
        //                //    //    searchList.BannerClickLog(Convert.ToInt32(Request["id"].ToString()));
        //                //    //    //Utility.bannerClick(Convert.ToInt64(Request["id"].ToString()), "0", "", Convert.ToString(userid), "", Request.UrlReferrer == null ? "" : Request.UrlReferrer.ToString(), Request.ServerVariables["REMOTE_ADDR"].ToString());
        //                //    //}
        //                //}
        //            }
        //            //return Response.Write(System.String.Format("window.open('{0}','_blank')", Request["link"].ToString().Replace("$", "&")));
        //            //this.ClientScript.RegisterStartupScript(this.GetType(), this.GetType().Name, string.Format("window.open('{0}', '_blank');", Request["link"].ToString().Replace("$", "&")), true);
                    
        //            return  Response.RedirectPermanent(Request["link"].ToString().Replace("$", "&"),true);
                    
        //        }
        //        else
        //        {
        //            return Redirect(ConfigurationManager.AppSettings["WebsiteRawPath"].ToString());
        //        }
        //    }            
        //    finally
        //    {
        //    }
        //}


        public ActionResult bannerclickJson(int? bannerid, string businessurl, string sourceurl, string previousurl)
        {
            if (!System.Web.HttpContext.Current.Request.Browser.Crawler && bannerid.HasValue && !string.IsNullOrEmpty(businessurl))
            {
                SearchListWEBController searchList = new SearchListWEBController();
                int userid = (Request != null && !string.IsNullOrEmpty(objUS.SV_VCUserContentID)) ? Convert.ToInt32(objUS.SV_VCUserContentID) : (System.Web.HttpContext.Current.Request != null && !string.IsNullOrEmpty(objUS.SV_VCUserContentID)) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                searchList.BannerClickLogwithSource(bannerid.Value, System.Web.HttpUtility.HtmlDecode(sourceurl), System.Web.HttpUtility.HtmlDecode(previousurl));
            }
            return Json(businessurl, JsonRequestBehavior.AllowGet);
        }


        public ActionResult BusinessSolutions()
        {
            return View("~/Views/Static/BusinessSolutions.cshtml");
        }

        public ActionResult Advertize()
        {
            return View("~/Views/Static/Advertize.cshtml");
        }
        public ActionResult LearnClaim()
        {
            return View("~/Views/Static/learnclaim.cshtml");
        }
        public ActionResult Career()
        {
            StaticPagesModel staticpages = new StaticPagesModel();
            staticpages.Career = staticpages.GetJobList();
            staticpages.SuccessMessage = TempData["SuccessMessage"] as string;
            staticpages.ErrorMessage = TempData["ErrorMessage"] as string;
            return View(staticpages);
        }
        public JsonResult Subscribe(string email)
        {
            HomeModel homeModel = new HomeModel();
            if (!string.IsNullOrEmpty(email))
            {
                homeModel.AddSubscribeEmail(email.ToString().Trim());
                SendSubscribeMail(email.ToString().Trim());
                return Json("Sucessfully subscribed with the following (" + email.ToString().Trim() + ") email id. ", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult Career(StaticPagesModel staticpages,string email,FormCollection frm)
        {
           
            HomeModel homeModel = new HomeModel();
            staticpages.Career = staticpages.GetJobList();
            string documentName = string.Empty;
            if (!string.IsNullOrEmpty(staticpages.txtemailid))
            {
                 if (string.IsNullOrEmpty(staticpages.txtfirstname))
                    ViewBag.Msg = "Enter your first name";
                    else if(string.IsNullOrEmpty(staticpages.txtsurname))
                    ViewBag.Msg = "Enter your surname";
                        else if(string.IsNullOrEmpty(staticpages.txtemailid))
                    ViewBag.Msg = "Enter your email address";
                          else if(string.IsNullOrEmpty(staticpages.txtdocument))
                    ViewBag.Msg = "Select your CV file";
                else{
                    string fileName = staticpages.txtdocument;
                   // FileInfo file = new FileInfo(fileName);
                    HttpPostedFileBase file = Request.Files["txtdocument"];
                    int MaxContentLength = 1024 * 1024 * 5; //5 MB
                    int imagelength = int.Parse(file.ContentLength.ToString());
                    string[] AllowedFileExtensions = new string[] { ".doc", ".docx", ".pdf"};
                    if (!AllowedFileExtensions.Contains((file.FileName.Substring(file.FileName.LastIndexOf('.')).ToLower())))
                    {
                        ViewBag.Msg = "Invaild file extension";
                    }
                    else if (imagelength > MaxContentLength)
                    {
                        ViewBag.Msg = "Your file is too large, maximum allowed size is: " + MaxContentLength + " MB";
                    }
                    else
                    {
                        documentName = (DateTime.Now.ToString("dd-mm-yyyy-hh-mm-ss-tt") + file.FileName.Replace(" ", "_").Replace("#", "_").Replace("%", "_").Replace("^", "_").Replace("'", "_")).ToString();
                        var path = Path.Combine(Server.MapPath("~/Resource/uploads/jobprofile/") + documentName);
                        string documentpathnew=("uploads/jobprofile/" + documentName).ToString().Trim();
                        int jobidnew=int.Parse((staticpages.hiddenjobid).ToString().Replace("#","").ToString());
                        file.SaveAs(path);
                        ModelState.Clear();
                        int error=staticpages.prc_add_JobProfiles(staticpages.txtfirstname, staticpages.txtsurname, staticpages.txtemailid, documentpathnew, jobidnew);
                        if (error == 0)
                            TempData["SuccessMessage"] = "You have sucessfully applied for the job.";
                        else
                        {
                            ViewBag.Msg = "You have already applied for the job.";
                            return View(staticpages);
                        }
                        return RedirectToAction("Career", "Static");
                    }
                    }
            }
            else
            {
                int result = 0;
                string strmsg = string.Empty;
                if (!string.IsNullOrEmpty(email))
                {
                    result = homeModel.AddSubscribeEmail(email.ToString().Trim());
                    if (result == 2)
                    {
                        ViewBag.Msg = "Email Already Exists";
                    }
                    else if (result == 1)
                    {
                        ViewBag.Msg = "Some Error Occured";
                    }
                    else
                    {
                        SendSubscribeMail(email.ToString().Trim());
                        TempData["SuccessMessage"] = "Sucessfully subscribed with the following (" + email.ToString().Trim() + ") email id. ";
                        return RedirectToAction("Career", "Static");
                    }
                }
            }
            return View(staticpages);
        }

        public ActionResult FAQs()
        {
            return View("~/Views/Static/FAQs.cshtml");
        }
        public ActionResult About()
        {
            return View("~/Views/Static/About.cshtml");
        }
        #region Privacy Policy
        public ActionResult privacypolicy()
        {
            return View();
        }
        #endregion
        public ActionResult PressRelease()
        {
            return View("~/Views/Static/PressRelease.cshtml");
        }
        public ActionResult DGO()
        {
            return View("~/Views/Static/DGO.cshtml");
        }
        #region contact us
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult phonenumbercheck(string phonenumber)
        {
            int d = 4;
            if (!string.IsNullOrEmpty(phonenumber))
            {
                int result = Vconnect.Common.Utility.isValidPhone(phonenumber);
                int result1 = Vconnect.Common.Utility.businessphonechk(phonenumber, 0);
                string sContactNumber = phonenumber;
                string subSection = sContactNumber.Substring(0, 1);
                if (subSection != "0")
                {
                    d = 5;
                }
                else if (result == 0)
                {
                    d = 2;
                }
                else
                {
                    d = 0;
                }
            }
            else
            {
                d = 4;
            }

            return Json(d, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Contactus()
        {
            ViewBag.message = TempData["message"];
            return View();
        }

        [HttpPost]
        public ActionResult Contactus(StaticPagesModel staticpages)
        {
            string error = string.Empty;
            int result = 0;
            int result1 = 1;

            if (!string.IsNullOrEmpty(staticpages.Contactus.phoneno) || !string.IsNullOrWhiteSpace(staticpages.Contactus.phoneno))
            {
                result1 = Vconnect.Common.Utility.isValidPhone(staticpages.Contactus.phoneno);
            }

            if (result1 == 0)
            {
                ViewBag.message = "Invalid Phone Number";

            }

            else
            {
                string phone = string.Empty;
                if (!string.IsNullOrEmpty(staticpages.Contactus.phoneno) || !string.IsNullOrWhiteSpace(staticpages.Contactus.phoneno))
                {
                    phone = staticpages.Contactus.phoneno.ToString();
                }
                else
                    phone = "";
                result = staticpages.prc_add_contactus(staticpages.Contactus);
                if (result == 0)
                {
                    SendContactusAdmMail(staticpages.Contactus.name, staticpages.Contactus.email, phone, staticpages.Contactus.message);
                    SendContactususerMail(staticpages.Contactus.name, staticpages.Contactus.email);
                    TempData["message"] = "Thank you for contacting us, we will respond to your message as soon as possible";
                    return RedirectToAction("Contactus");
                }
            }
            return View();
        }

        public ActionResult PageNotFound()
        {
            Response.Clear(); Response.StatusCode = 404;
            Response.Status = "404 NotFound";
            return View("404");
        }
        #endregion
        #region Business Forum

        [HttpGet]
        public ActionResult smeevent()
        {
            StaticPagesModel staticpages = new StaticPagesModel();
            staticpages.getevent(0);
            ViewBag.error = TempData["message"];
            return View(staticpages);
        }

        [HttpPost]
        public ActionResult smeevent(StaticPagesModel staticpages, FormCollection form)
        {
            bool a = true;
            int result = 0;
            int result1 = 1;
            string error = string.Empty;
            if (staticpages.Enterevent.email != null)
            {
                Regex regEmail = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
                a = regEmail.IsMatch(staticpages.Enterevent.email);
            }
            if (staticpages.Enterevent.phone != null)
            {
                result = Vconnect.Common.Utility.isValidPhone(staticpages.Enterevent.phone);
            }
            if (a == false)
            {
                ViewBag.error = "Invalid Email-Id";

            }
            else if (result == 0)
            {
                ViewBag.error = "Invalid contact number";
            }
            else
            {

                string abc = form["designation"].ToString();

                int eventid = Convert.ToInt16(form["forumid"]);
                string hidden = string.Empty;
                string timeslot = string.Empty;
                if (form["eventDate"]!=null && form["eventDate"]!="")
                 hidden = Convert.ToDateTime(form["eventDate"]).ToString("yyyy/MM/dd").Trim();
                //Int64 timeslot = Convert.ToInt64(form["eventSlot"]); 
                if (form["eventSlot"] != null && form["eventSlot"] !="")
                 timeslot = Convert.ToString(form["eventSlot"]);
                result1 = staticpages.prc_add_smeeventuser(staticpages.Enterevent, eventid, timeslot, abc, hidden);
                if (result1 == 0)
                {
                    TempData["message"] = "Registered successfully.";
                    return RedirectToAction("smeevent");
                    // ViewBag.error = "Registered successfully.";
                }
                else
                {
                    TempData["message"] = "The company name already exists. Try again with other phone number";
                    return RedirectToAction("smeevent");
                    // ViewBag.error = "The company name already exists. Try again with other phone number";
                }
            }
            ///staticpages.getevent(0);
            return View(staticpages);
        }
        #endregion
        #region Feedback
        public ActionResult Feedback()
        {
            FeedbackModels fdmodel = new FeedbackModels();
            fdmodel.productOptions = ProductList();
            fdmodel.SuccessMessage = TempData["SuccessMessage"] as string;
            fdmodel.MessageError = TempData["MessageError"] as string;
            return View(fdmodel);
        }
        [HttpPost]
        public ActionResult Feedback(FeedbackModels fdmodel)
        {
            string productname = null;
            fdmodel.productOptions = ProductList();
            SelectList selecttProduct = new SelectList(fdmodel.productOptions, "Id", "Name");
            foreach (SelectListItem item in selecttProduct.Items)
            {
                if (item.Value == fdmodel.SelectedproductId.ToString())
                {
                    productname = item.Text;
                    break;
                }
            }

            if (!string.IsNullOrEmpty(productname) && !string.IsNullOrEmpty(fdmodel.feedback))
            {
                fdmodel.AddFeedBack(fdmodel.txtPhone, fdmodel.txtEmail, productname, fdmodel.feedback, "WEB");
                SendFeebackMail(productname, fdmodel.feedback);
                TempData["SuccessMessage"] = "Thank you for submitting feedback.";
                return RedirectToAction("Feedback", "Static");
            }
            else
            {
                TempData["MessageError"] = "Please select a option";
                return RedirectToAction("Feedback", "Static");
            }
        }
        public static SelectList ProductList()
        {
            var list = new[]
                {
                 new StatesDropDown  { StateId = 1005 ,StateName = "Satisfied"},
                   new StatesDropDown  { StateId = 1004 ,StateName = "Slightly Satisfied"},
                    new StatesDropDown  { StateId = 1003 ,StateName = "Neutral"},
                    new StatesDropDown  { StateId = 1002 ,StateName = "Slightly Dissatisfied"},
                 new StatesDropDown  { StateId = 1001, StateName = "Dissatisfied"  }
                           
              
               
                };
            return new SelectList(list, "StateId", "StateName");
        }

        #endregion
        #region Advertize with us
        public ActionResult Advertwithus()
        {
            AdvertwithusModels model = new AdvertwithusModels();
            model.StateOptions = Utility.GetSelectList();
            model.PackageOptions = PackageList();
            model.SuccessMessage = TempData["SuccessMessage"] as string;
            model.MessageError = TempData["MessageError"] as string;
            return View(model);
        }
        [HttpPost]
        public ActionResult Advertwithus(AdvertwithusModels model)
        {
            string statename = null, packagename = null;
            UserSession objUS = new UserSession();
            model.StateOptions = Utility.GetSelectList();
            SelectListItem SelectListItem = new SelectListItem();
            SelectListItem.Value = model.SelectedStateId.ToString();
            model.PackageOptions = PackageList();
            int result = 1;
            SelectList selectStatet = new SelectList(model.StateOptions, "Id", "Name");
            foreach (SelectListItem item in selectStatet.Items)
            {
                if (item.Value == model.SelectedStateId.ToString())
                {
                    statename = item.Text;
                    break;
                }
            }
            SelectList selecttPackage = new SelectList(model.PackageOptions, "Id", "Name");
            foreach (SelectListItem item in selecttPackage.Items)
            {
                if (item.Value == model.SelectedPackage.ToString())
                {
                    packagename = item.Text;
                    break;
                }
            }
            if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
            {
                result = model.addadvertise(Convert.ToInt32(objUS.SV_VCUserContentID.ToString()), model.txtcompany.ToString(), statename, model.txtname.ToString().Trim(), model.txtcontactno.ToString(), model.txtemail.ToString().Trim(), model.txtdetail.ToString(), packagename, "WEB");
            }
            else
            {
                result = model.addadvertise(0, model.txtcompany.ToString(), statename, model.txtname.ToString().Trim(), model.txtcontactno.ToString(), model.txtemail.ToString().Trim(), model.txtdetail.ToString(), packagename, "WEB");
            }
            if (result == 0)
            {
                TempData["SuccessMessage"] = "Thank you for choosing to advertize on VConnect. We will contact you soon.";
                return RedirectToAction("Advertwithus");
            }
            return View(model);
        }
        public static SelectList PackageList()
        {
            var list = new[] 
                { 
                //new StatesDropDown  { StateId = 1001, StateName = "Priority Listing"  },
                new StatesDropDown  { StateId = 1001, StateName = "VConnect Recommended Business"  },
                new StatesDropDown  { StateId = 1002, StateName = "Display Advertisements"  },
                new StatesDropDown  { StateId = 1003, StateName = "Featured business"  },
                new StatesDropDown  { StateId = 1004, StateName = "Online Leads"  },
                new StatesDropDown  { StateId = 1005, StateName = "KYC Solution"  },
                new StatesDropDown  { StateId = 1006 ,StateName = "Targeted SMS/ Email"}
                };
            return new SelectList(list, "StateId", "StateName");
        }
        #endregion
        #region our product
        public ActionResult Product()
        {
            return View();
        }
        #endregion
        #region Advertize with us IBTC
        public ActionResult Advertwithusibtc()
        {
            AdvertwithusModels model = new AdvertwithusModels();
            model.StateOptions = Utility.GetSelectList();
            model.PackageOptions = PackageList();
            model.SuccessMessage = TempData["SuccessMessage"] as string;
            model.MessageError = TempData["MessageError"] as string;
            return View(model);
        }
        [HttpPost]
        public ActionResult Advertwithusibtc(AdvertwithusModels model)
        {
            string statename = null, packagename = null;
            UserSession objUS = new UserSession();
            model.StateOptions = Utility.GetSelectList();
            SelectListItem SelectListItem = new SelectListItem();
            SelectListItem.Value = model.SelectedStateId.ToString();
            model.PackageOptions = PackageList();
            int result = 1;
            SelectList selectStatet = new SelectList(model.StateOptions, "Id", "Name");
            foreach (SelectListItem item in selectStatet.Items)
            {
                if (item.Value == model.SelectedStateId.ToString())
                {
                    statename = item.Text;
                    break;
                }
            }
            SelectList selecttPackage = new SelectList(model.PackageOptions, "Id", "Name");
            foreach (SelectListItem item in selecttPackage.Items)
            {
                if (item.Value == model.SelectedPackage.ToString())
                {
                    packagename = item.Text;
                    break;
                }
            }
            if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
            {
                result = model.addadvertise(Convert.ToInt32(objUS.SV_VCUserContentID.ToString()), model.txtcompany.ToString(), statename, model.txtname.ToString().Trim(), model.txtcontactno.ToString(), model.txtemail.ToString().Trim(), model.txtdetail.ToString(), packagename, "Stanbic IBTC Bank");
            }
            else
            {
                result = model.addadvertise(0, model.txtcompany.ToString(), statename, model.txtname.ToString().Trim(), model.txtcontactno.ToString(), model.txtemail.ToString().Trim(), model.txtdetail.ToString(), packagename, "Stanbic IBTC Bank");
            }
            if (result == 0)
            {
                TempData["SuccessMessage"] = "Thank you for choosing to advertize on VConnect. We will contact you soon.";
                return RedirectToAction("Advertwithusibtc");
            }
            return View(model);
        }
        #endregion
        #region feedback email
        #region sendemail
        protected void SendFeebackMail(string subject, string feedback)
        {
            string location = string.Empty;
            string Message = "";
            System.Net.Mail.SmtpClient SMPT = new System.Net.Mail.SmtpClient();
            System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage();
            System.IO.FileStream FsContent = new System.IO.FileStream(Server.MapPath("~/resource/email_templates/feedback.html"), System.IO.FileMode.Open, System.IO.FileAccess.Read);
            System.IO.StreamReader SRcontent = new System.IO.StreamReader(FsContent);
            Message = SRcontent.ReadToEnd();
            FsContent.Close();
            SRcontent.Close();
            FsContent.Dispose();
            SRcontent.Dispose();
            Message = Message.Replace("{0}", Utility.ToTitleCase(subject.ToString().Trim()));
            Message = Message.Replace("{1}", Utility.ToTitleCase(feedback.ToString().Trim()));
            Email.Subject = ConfigurationManager.AppSettings["SubjectfeedbackTOAdmin"].ToString();
            Email.From = new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["networkUserIdvcHelpdesk"].ToString(), "VConnect.com");
            Email.IsBodyHtml = true;
            Email.Body = Message;
            Email.To.Add(ConfigurationManager.AppSettings["EmailTofeedbackTOAdmin"].ToString().Trim());
            SendEmail send = new SendEmail();
            try
            {
                send.SendMailToUser(ref Email, ConfigurationManager.AppSettings["networkUserIdvcHelpdesk"].ToString(), ConfigurationManager.AppSettings["networkUserIdPasswordvcHelpdesk"].ToString());
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
        }
        #endregion
        #endregion
        #region subscribe email
        protected void SendSubscribeMail(string email)
        {
            string location = string.Empty;
            string Message = "";
            if (!string.IsNullOrEmpty(email))
            {
                System.Net.Mail.SmtpClient SMPT = new System.Net.Mail.SmtpClient();
                System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage();
                System.IO.FileStream FsContent = new System.IO.FileStream(Server.MapPath("~/resource/email_templates/AckMailToNewsletterSubscriber.html"), System.IO.FileMode.Open, System.IO.FileAccess.Read);
                System.IO.StreamReader SRcontent = new System.IO.StreamReader(FsContent);
                Message = SRcontent.ReadToEnd();
                FsContent.Close();
                SRcontent.Close();
                FsContent.Dispose();
                SRcontent.Dispose();
                Email.Subject = ConfigurationManager.AppSettings["SubjectSubscrivename"].ToString();
                Email.From = new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["networkUserIdvcSubscribe"].ToString(), "VConnect.com");
                Email.IsBodyHtml = true;
                Email.Body = Message;
                Email.To.Add(email.ToString().Trim());
                SendEmail send = new SendEmail();
                try
                {
                    send.SendMailToUser(ref Email, ConfigurationManager.AppSettings["networkUserIdvcSubscribe"].ToString(), ConfigurationManager.AppSettings["networkUserIdPasswordvcSubscribe"].ToString());
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
            }
        }
        #endregion
        #region contact us admin email
        protected void SendContactusAdmMail(string name, string email, string phone, string message)
        {
            string Message = "";
            System.Net.Mail.SmtpClient SMPT = new System.Net.Mail.SmtpClient();
            System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage();
            System.IO.FileStream FsContent = new System.IO.FileStream(Server.MapPath("~/resource/email_templates/contactusadm.html"), System.IO.FileMode.Open, System.IO.FileAccess.Read);
            System.IO.StreamReader SRcontent = new System.IO.StreamReader(FsContent);
            Message = SRcontent.ReadToEnd();
            FsContent.Close();
            SRcontent.Close();
            FsContent.Dispose();
            SRcontent.Dispose();
            Message = Message.Replace("{0}", Utility.ToTitleCase(name.ToString().Trim()));
            Message = Message.Replace("{1}", email.ToString().Trim());
            Message = Message.Replace("{2}", phone.ToString().Trim());
            Message = Message.Replace("{3}", message.ToString().Trim());
            Email.Subject = ConfigurationManager.AppSettings["SubjectcontactusTOAdmin"].ToString();
            Email.From = new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["networkUserId"].ToString(), "VConnect.com");
            Email.IsBodyHtml = true;
            Email.Body = Message;
            Email.To.Add(ConfigurationManager.AppSettings["EmailTofeedbackTOAdmin"].ToString().Trim());
            SendEmail send = new SendEmail();
            try
            {
                send.SendMailToUser(ref Email, ConfigurationManager.AppSettings["networkUserId"].ToString(), ConfigurationManager.AppSettings["networkUserIdpassword"].ToString());
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
        }
        #endregion
        #region contact us user email
        protected void SendContactususerMail(string name, string email)
        {
            string Message = "";
            if (!string.IsNullOrEmpty(email))
            {
                System.IO.FileStream FsContent = new System.IO.FileStream(Server.MapPath("~/resource/email_templates/contactususer.html"), System.IO.FileMode.Open, System.IO.FileAccess.Read);
                System.IO.StreamReader SRcontent = new System.IO.StreamReader(FsContent);
                Message = SRcontent.ReadToEnd();
                FsContent.Close();
                SRcontent.Close();
                FsContent.Dispose();
                SRcontent.Dispose();
                Message = Message.Replace("{0}", Utility.ToTitleCase(name.ToString().Trim()));
                //===================================
                var recipients = new List<Mandrill.Messages.Recipient>();
                recipients.Add(new Mandrill.Messages.Recipient(email.ToString().Trim(), ""));
                var mandrill = new Mandrill.Messages.Message()
                {
                    To = recipients.ToArray(),
                    FromEmail = "info@VConnect.com",
                    Subject = ConfigurationManager.AppSettings["SubjectcontactusTOUser"].ToString(),

                    Html = Message
                };
                //=================================== 
                SendEmail send = new SendEmail();
                try
                {
                    send.VCUsermails(mandrill);
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
            }
        }
        #endregion

        #region advertise with us email
        protected void AdvertiseWithUsMail(string name, string email,string state,string phoneNumber, string company, string package, string breif)
        {
            string Message = "";
            if (!string.IsNullOrEmpty(email))
            {
                System.Net.Mail.SmtpClient SMPT = new System.Net.Mail.SmtpClient();
                System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage();
                System.IO.FileStream FsContent = new System.IO.FileStream(Server.MapPath("~/resource/email_templates/contactususer.html"), System.IO.FileMode.Open, System.IO.FileAccess.Read);
                System.IO.StreamReader SRcontent = new System.IO.StreamReader(FsContent);
                Message = SRcontent.ReadToEnd();
                FsContent.Close();
                SRcontent.Close();
                FsContent.Dispose();
                SRcontent.Dispose();
                Message = Message.Replace("{0}", Utility.ToTitleCase(name.ToString().Trim()));
                Email.Subject = ConfigurationManager.AppSettings["SubjectcontactusTOUser"].ToString();
                Email.From = new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["networkUserIdvcHelpdesk"].ToString(), "VConnect.com");
                Email.IsBodyHtml = true;
                Email.Body = Message;
                Email.To.Add(email.ToString().Trim());
                SendEmail send = new SendEmail();
                try
                {
                    send.SendMailToUser(ref Email, ConfigurationManager.AppSettings["networkUserIdvcHelpdesk"].ToString(), ConfigurationManager.AppSettings["networkUserIdPasswordvcHelpdesk"].ToString());
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
            }
        }
        #endregion

        //#region For Only Admin
        //[HttpGet]
        //public ActionResult ClientsAddbyAdmin(Int32? leadid, string submit, string error)
        //{
        //    clientsadmin reg = new clientsadmin();
        //    reg.SuccessMessage = TempData["SuccessMessage"] as string;
        //    reg.MessageError = TempData["MessageError"] as string;
        //    if (!string.IsNullOrEmpty(error))
        //    {
        //        if (error == "1")
        //            reg.SuccessMessage = "You have successfully enterd data.";
        //        else if (error == "0")
        //            reg.MessageError = "Please field all...";
        //        else
        //        {
        //            TempData["SuccessMessage"] = "";
        //            TempData["MessageError"] = "";
        //        }
        //    }
        //    else
        //    {
        //        reg.SuccessMessage = null;
        //        reg.MessageError = null;
        //    }
        //    if (leadid.HasValue)
        //    {
        //        reg.leadid = leadid.Value;
        //    }
        //    if (!string.IsNullOrEmpty(submit))
        //    {
        //        reg.chkrdo = submit;
        //    }
        //    else
        //    {
        //        reg.chkrdo = "Business";
        //    }
        //    reg = Clientsshowform(reg);
        //    if (leadid.HasValue)
        //    {
        //        foreach (var twt in reg.listlead.Where(m => m.leadid == leadid.Value)) { reg.leadname = twt.leadname; }
        //    }
        //    return View(reg);
        //}

        //public ActionResult ClientsAddbyAdmin1(string drp1, string drpQtype, string txtanswers, string txtqname, string drpcompalsary, int txtlength, string txtplaceholder, string drpcolumnname, string txtvalidation, int drpqorder, string drpuniquetype, string refcolumnname, string submit)
        //{
        //    Int32 userid = 0;
        //    if (string.IsNullOrEmpty(drpuniquetype))
        //        drpuniquetype = "text";
        //    string sms = string.Empty;
        //    if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
        //        userid = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
        //    int Error = 0;
        //    using (var db = new VconnectDBContext29())
        //    {
        //        var cmd = db.Database.Connection.CreateCommand();
        //        cmd.CommandText = "[dbo].[Client_Registraion]";
        //        cmd.CommandTimeout = 600;
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        cmd.Parameters.Add(new SqlParameter("@leadid", int.Parse(drp1)));
        //        //cmd.Parameters.Add(new SqlParameter("@Qid", reg.txtqname));
        //        cmd.Parameters.Add(new SqlParameter("@Qname", txtqname));
        //        cmd.Parameters.Add(new SqlParameter("@Qtype", drpQtype));
        //        cmd.Parameters.Add(new SqlParameter("@compalsary", drpcompalsary));
        //        cmd.Parameters.Add(new SqlParameter("@validations", txtvalidation));
        //        cmd.Parameters.Add(new SqlParameter("@uniquetype", drpuniquetype));
        //        cmd.Parameters.Add(new SqlParameter("@answers", txtanswers));
        //        cmd.Parameters.Add(new SqlParameter("@createdby", userid));
        //        //cmd.Parameters.Add(new SqlParameter("@modifyby", reg.leadid));
        //        cmd.Parameters.Add(new SqlParameter("@qorder", drpqorder));
        //        cmd.Parameters.Add(new SqlParameter("@length", txtlength));
        //        cmd.Parameters.Add(new SqlParameter("@columnname", drpcolumnname));
        //        //cmd.Parameters.Add(new SqlParameter("@uniquename", reg.leadid));
        //        cmd.Parameters.Add(new SqlParameter("@placeholder", txtplaceholder));
        //        //cmd.Parameters.Add(new SqlParameter("@leadname", reg.leadid));
        //        cmd.Parameters.Add(new SqlParameter("@chkrdo", submit.Trim()));
        //        cmd.Parameters.Add(new SqlParameter("@referenceid", refcolumnname));
        //        cmd.Parameters.Add(new SqlParameter("@Error", SqlDbType.Int));
        //        cmd.Parameters["@Error"].Direction = ParameterDirection.Output;
        //        try
        //        {
        //            var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
        //            connection.Open();
        //            cmd.ExecuteNonQuery();
        //            Error = Convert.ToInt32(cmd.Parameters["@Error"].Value.ToString());
        //            if (Error == 0)
        //            {
        //                TempData["SuccessMessage"] = "Registration successful.";
        //                sms = "Registration successful.";
        //            }
        //            else
        //            {
        //                TempData["MessageError"] = "Sorry..";
        //                sms = "Sorry..";
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            db.Database.Connection.Close();
        //        }
        //        finally
        //        {
        //            //db.Database.Connection.Close();
        //        }
        //    }
        //    return Json(sms, JsonRequestBehavior.AllowGet);
        //    //return RedirectPermanent("/Static/ClientsAddbyAdmin?leadid="+drp1);
        //    //return View(reg);
        //}
        //public clientsadmin Clientsshowform(clientsadmin reg)
        //{
        //    List<Listleadid> leaddata = new List<Listleadid>();
        //    using (var db = new VconnectDBContext29())
        //    {
        //        var cmd = db.Database.Connection.CreateCommand();
        //        cmd.CommandText = "[dbo].[Client_Binddata]";
        //        cmd.CommandTimeout = 600;
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        if (reg.leadid != 0 && reg.leadid != null && reg.leadid.ToString() != "")
        //            cmd.Parameters.Add(new SqlParameter("@leadid", reg.leadid));
        //        cmd.Parameters.Add(new SqlParameter("@chkrdo", reg.chkrdo));
        //        try
        //        {
        //            var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
        //            connection.Open();
        //            var reader = cmd.ExecuteReader();
        //            reg.maxorder = ((IObjectContextAdapter)db).ObjectContext.Translate<Questionorder>(reader).FirstOrDefault();

        //            reader.NextResult();
        //            reg.listlead = ((IObjectContextAdapter)db).ObjectContext.Translate<Listleadid>(reader).ToList();

        //            reader.NextResult();
        //            reg.listcolumn = ((IObjectContextAdapter)db).ObjectContext.Translate<ColumnNamelist>(reader).ToList();

        //            reader.NextResult();
        //            reg.listrefcolumn = ((IObjectContextAdapter)db).ObjectContext.Translate<RefColumnNamelist>(reader).ToList();
        //        }
        //        catch (Exception ex)
        //        {
        //        }
        //        finally
        //        {
        //            //db.Database.Connection.Close();
        //        }
        //    }
        //    return reg;
        //}
        //public ActionResult AddColumnsTable(string columnname, string datatype)
        //{
        //    string sms = string.Empty;
        //    int Error = 0;
        //    using (var db = new VconnectDBContext29())
        //    {
        //        var cmd = db.Database.Connection.CreateCommand();
        //        cmd.CommandText = "[dbo].[Client_AddColumns]";
        //        cmd.CommandTimeout = 600;
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        cmd.Parameters.Add(new SqlParameter("@columnname", columnname));
        //        cmd.Parameters.Add(new SqlParameter("@datatype", datatype));

        //        cmd.Parameters.Add(new SqlParameter("@Error", SqlDbType.Int));
        //        cmd.Parameters["@Error"].Direction = ParameterDirection.Output;
        //        try
        //        {
        //            var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
        //            connection.Open();
        //            cmd.ExecuteNonQuery();
        //            Error = Convert.ToInt32(cmd.Parameters["@Error"].Value.ToString());
        //            if (Error == 1)
        //                sms = "Already column name exits in table.";
        //            else
        //                sms = "Thanks";
        //        }
        //        catch (Exception ex)
        //        {
        //            db.Database.Connection.Close();
        //        }
        //        finally
        //        {
        //            //db.Database.Connection.Close();
        //        }
        //    }
        //    return Json(sms, JsonRequestBehavior.AllowGet);
        //}
        //public ActionResult Set_Query(string tablename2, Int32? leadid)
        //{
        //    clientsadmin reg = new clientsadmin();
        //    List<Listtablecolumn> tablecolumn = new List<Listtablecolumn>();
        //    List<Listdropcolumn> dropcolumn = new List<Listdropcolumn>();
        //    string table1 = "";
        //    string table2 = "";
        //    using (var db = new VconnectDBContext29())
        //    {
        //        var cmd = db.Database.Connection.CreateCommand();
        //        cmd.CommandText = "[dbo].[Client_SelectColumns]";
        //        cmd.CommandTimeout = 600;
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        cmd.Parameters.Add(new SqlParameter("@leadid", leadid.Value));
        //        cmd.Parameters.Add(new SqlParameter("@tablename", tablename2));
        //        try
        //        {
        //            var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
        //            connection.Open();
        //            var reader = cmd.ExecuteReader();
        //            tablecolumn = ((IObjectContextAdapter)db).ObjectContext.Translate<Listtablecolumn>(reader).ToList();

        //            reader.NextResult();
        //            dropcolumn = ((IObjectContextAdapter)db).ObjectContext.Translate<Listdropcolumn>(reader).ToList();

        //        }
        //        catch (Exception ex)
        //        {
        //        }
        //        finally
        //        {
        //            //db.Database.Connection.Close();
        //        }
        //        //table1+="<select name='site-rating1' id='columntxtvalueid_1' style='width:48%;' required=''>";
        //        table1 += "<option value=''>Please select</option>";
        //        int i = 1;
        //        foreach (var item in tablecolumn)
        //        {
        //            table1 += "<option value=" + i + ">" + item.tablecolumn + "</option>";
        //            i++;
        //        }
        //        // table1+="</select>";

        //        //table2+="<select name='site-rating1' id='columntxtvalueid_1' style='width:48%;float:right;' required=''>";
        //        table2 += "<option value=''>Please select</option>";
        //        int j = 1;
        //        foreach (var item in dropcolumn)
        //        {
        //            table2 += "<option value=" + j + ">" + item.dropcolumn + "</option>";
        //            j++;
        //        }
        //        //table2+="</select>";
        //        var result = new { table1 = table1, table2 = table2 };
        //        return Json(result, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //[HttpGet]
        //public ActionResult ClientsFinals(Int32? leadid, string submit)
        //{
        //    //regupdate.tablebind = Clients_Update(leadid,"select");
        //    clientsadminUpdate regupdate = new clientsadminUpdate();
        //    clientsadmin reg = new clientsadmin();
        //    if (!string.IsNullOrEmpty(submit))
        //    {
        //        regupdate.chkrdo = submit;
        //        reg.chkrdo = submit;
        //    }
        //    else
        //    {
        //        regupdate.chkrdo = "Business";
        //        reg.chkrdo = "Business";
        //    }
        //    reg = Clientsshowform(reg);
        //    regupdate.listlead = reg.listlead;
        //    return View(regupdate);
        //}
        //public ActionResult Clients_Update(Int32? leadid, string querytype, string submit)
        //{
        //    clientsadminUpdate regupdate = new clientsadminUpdate();
        //    List<ListRowbyId> listrowbyId = new List<ListRowbyId>();
        //    CountRow countrow = new CountRow();
        //    string htmltbl = "";
        //    string countrowtxt = "";
        //    string countrowdrp = "";
        //    List<Listdropcolumn> dropcolumn = new List<Listdropcolumn>();
        //    using (var db = new VconnectDBContext29())
        //    {
        //        var cmd = db.Database.Connection.CreateCommand();
        //        cmd.CommandText = "[dbo].[Client_SelectColumns]";
        //        cmd.CommandTimeout = 600;
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        cmd.Parameters.Add(new SqlParameter("@leadid", leadid.Value));
        //        try
        //        {
        //            var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
        //            connection.Open();
        //            var reader = cmd.ExecuteReader();
        //            //tablecolumn = ((IObjectContextAdapter)db).ObjectContext.Translate<Listtablecolumn>(reader).ToList();

        //            reader.NextResult();
        //            dropcolumn = ((IObjectContextAdapter)db).ObjectContext.Translate<Listdropcolumn>(reader).ToList();

        //        }
        //        catch (Exception ex)
        //        {
        //        }
        //        finally
        //        {
        //            //db.Database.Connection.Close();
        //        }
        //    }
        //    using (var db = new VconnectDBContext29())
        //    {
        //        var cmd = db.Database.Connection.CreateCommand();
        //        cmd.CommandText = "[dbo].[Client_Update_Select]";
        //        cmd.CommandTimeout = 600;
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        cmd.Parameters.Add(new SqlParameter("@leadid", leadid.Value));
        //        cmd.Parameters.Add(new SqlParameter("@querytype", querytype));
        //        cmd.Parameters.Add(new SqlParameter("@chkrdo", submit.Trim()));
        //        try
        //        {
        //            var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
        //            connection.Open();
        //            var reader = cmd.ExecuteReader();
        //            listrowbyId = ((IObjectContextAdapter)db).ObjectContext.Translate<ListRowbyId>(reader).ToList();

        //            reader.NextResult();
        //            countrow = ((IObjectContextAdapter)db).ObjectContext.Translate<CountRow>(reader).FirstOrDefault();
        //        }
        //        catch (Exception ex)
        //        {
        //            //db.Database.Connection.Close();
        //        }
        //        finally
        //        {
        //            //db.Database.Connection.Close();
        //        }
        //        if (listrowbyId != null && listrowbyId.Count > 0)
        //        {
        //            htmltbl += @"<div><label>Select Unique_Name(Optional)</label><select name='site-rating' style='width:50%;' id='txtUnique' onchange='uniquedrop();'>";
        //            foreach (var tbl in listrowbyId)
        //            {
        //                if (!string.IsNullOrEmpty(tbl.uniquename))
        //                {
        //                    htmltbl += @"<option value='1'>" + tbl.uniquename.Trim() + "</option>";
        //                    htmltbl += @"<option value='0'>Select unique name</option>";
        //                }
        //                else
        //                    htmltbl += @"<option value='0'>Select unique name</option>";
        //                int k = 2;
        //                foreach (var clmnm in dropcolumn.Where(m => m.dropcolumn != tbl.uniquename && m.dropcolumn != "source" && m.dropcolumn != "leadid" && m.dropcolumn != "widget" && m.dropcolumn != "clientname"))
        //                {

        //                    htmltbl += @"<option value='" + k + "'>" + clmnm.dropcolumn.Trim() + "</option>";
        //                    k++;
        //                }
        //            }
        //            htmltbl += @"</select></div>";
        //            foreach (var tbl in listrowbyId)
        //            {
        //                if (!string.IsNullOrEmpty(tbl.uniquemessage))
        //                {
        //                    htmltbl += "<input type='text' id='txtMessage' style='width:50%;' placeholder='Enter unique message' onblur='uniquesms();' value='" + tbl.uniquemessage + "'/>";
        //                    htmltbl += "<small class='error' id='txtsms' style='display:none;'>Please enter unique message</small>";
        //                    break;
        //                }
        //                else
        //                {
        //                    htmltbl += "<input type='text' id='txtMessage' style='width:50%;display: none;' placeholder='Enter unique message' onblur='uniquesms();' value='" + tbl.uniquemessage + "'/>";
        //                    htmltbl += "<small class='error' id='txtsms' style='display:none;'>Please enter unique message</small>";
        //                    break;
        //                }
        //            }
        //            htmltbl += @"<table id='tab1' class='table'>";
        //            htmltbl += @"<tr><th>ID</th><th>Q_Order</th><th>Label_Name</th><th>Column_Name</th><th>Compulsory</th><th>Label_Type</th><th>Bind_Data</th><th>Length</th><th>Water_Mark</th><th>Message</th><th>Active</th><th>Update</th><th>Delete</th></tr>";
        //            foreach (var tbl in listrowbyId)
        //            {
        //                countrowtxt += tbl.contentid + ",";
        //                htmltbl += @"<tr id='tr_" + tbl.contentid + "'><td>" + tbl.contentid + "</td>";
        //                htmltbl += @"<td><input type='text' value='" + tbl.qorder.ToString().Trim() + "' id='txt1_" + tbl.contentid + "' onchange='Q_Order(&#39;txt1_" + tbl.contentid + "&#39;);' onkeypress = 'return isNumberKey(event)'></input></td>";
        //                htmltbl += @"<td><input type='text' value='" + tbl.Qname.ToString().Trim() + "' id='txt2_" + tbl.contentid + "'></td>";
        //                htmltbl += @"<td> <select name='site-rating' style='width:150px;' id='txt3_" + tbl.contentid + "' onchange='Column_Name(&#39;txt3_" + tbl.contentid + "&#39;)' onclick='SaveColumnName(&#39;txt3_" + tbl.contentid + "&#39;)'>";
        //                htmltbl += @"<option value='1'>" + tbl.columnname + "</option>";
        //                int j = 2;
        //                foreach (var clmnm in dropcolumn.Where(m => m.dropcolumn != tbl.columnname && m.dropcolumn != "source" && m.dropcolumn != "leadid" && m.dropcolumn != "widget" && m.dropcolumn != "clientname"))
        //                {
        //                    htmltbl += @"<option value='" + j + "'>" + clmnm.dropcolumn.Trim() + "</option>";
        //                    j++;
        //                }
        //                htmltbl += @"</select></td>";

        //                htmltbl += @"<td><select name='site-rating' style='width:100px;' id='txt4_" + tbl.contentid + "'>";
        //                htmltbl += @"<option value='3'>" + tbl.compalsary.Trim() + "</option>";
        //                htmltbl += @"<option value='2'>Required</option>";
        //                htmltbl += @"<option value='1'>Optional</option>";
        //                htmltbl += @"</select></td>";
        //                htmltbl += @"<td> <select name='site-rating' style='width:100px;' id='txt5_" + tbl.contentid + "' onchange='Label_Type(" + tbl.contentid + ")'>";
        //                htmltbl += @"<option value='7'>" + tbl.Qtype.Trim() + "</option>";
        //                htmltbl += @"<option value='6'>textbox</option>";
        //                htmltbl += @"<option value='5'>textarea</option>";
        //                htmltbl += @"<option value='4'>drptxt</option>";
        //                //htmltbl += @"<option value='3'>drpmaster</option>";
        //                htmltbl += @"<option value='2'>radio</option>";
        //                htmltbl += @"<option value='1'>checkbox</option>";
        //                htmltbl += @"</select></td>";
        //                htmltbl += @"</select></td>";
        //                if (tbl.Qtype.Trim() == "drpmaster")
        //                    htmltbl += @"<td><textarea cols='20' maxlength='1700' name='txtanswers' rows='2' id='txt6_" + tbl.contentid + "' readonly>" + tbl.answers.ToString().Trim() + "</textarea></td>";
        //                else if (tbl.Qtype.Trim() == "textbox" || tbl.Qtype.Trim() == "textarea")
        //                    htmltbl += @"<td><textarea cols='20' maxlength='1700' name='txtanswers' rows='2' id='txt6_" + tbl.contentid + "' style='display:none;'>" + tbl.answers.ToString().Trim() + "</textarea></td>";
        //                else
        //                    htmltbl += @"<td><textarea cols='20' maxlength='1700' name='txtanswers' rows='2' id='txt6_" + tbl.contentid + "'>" + tbl.answers.ToString().Trim() + "</textarea></td>";
        //                htmltbl += @"<td><input type='text' value=" + tbl.length + " id='txt7_" + tbl.contentid + "' onkeypress = 'return isNumberKey(event)'></td>";
        //                htmltbl += @"<td><input type='text' value='" + tbl.placeholder.ToString().Trim() + "' id='txt8_" + tbl.contentid + "'></td>";
        //                htmltbl += @"<td><input type='text' value='" + tbl.validations.ToString().Trim() + "' id='txt9_" + tbl.contentid + "'></td>";
        //                if (tbl.active != null && tbl.active.Trim() == "1")
        //                    htmltbl += @"<td><input type='checkbox' value='" + tbl.contentid + "' checked='checked'  id='txt10_" + tbl.contentid + "' onclick='Update(&#39;" + tbl.contentid + "&#39;,&#39;active&#39;);'/></td>";
        //                else
        //                    htmltbl += @"<td><input type='checkbox' value='" + tbl.contentid + "'  id='txt10_" + tbl.contentid + "' onclick='Update(&#39;" + tbl.contentid + "&#39;,&#39;active&#39;);'/></td>";

        //                htmltbl += @"<td><a href='#' style='color:green;' onclick='Update(&#39;" + tbl.contentid + "&#39;,&#39;update&#39;);'><u><b>Update</b></u></a></td>";
        //                //if (tbl.active != null && tbl.active.Trim() == "1")
        //                //    htmltbl += @"<td><input type='checkbox' value='" + tbl.contentid + "' checked='checked' onclick='Update(&#39;" + tbl.contentid + "&#39;);' id='txt10_" + tbl.contentid + "'><a href='#' style='color:green;' onclick='Update(&#39;" + tbl.contentid + "&#39;);'><u><b>Update</b></u></a></td>";
        //                //else
        //                //    htmltbl += @"<td><input type='checkbox' value='" + tbl.contentid + "' onclick='Update(&#39;" + tbl.contentid + "&#39;);' id='txt10_" + tbl.contentid + "'><a href='#' style='color:green;' onclick='Update(&#39;" + tbl.contentid + "&#39;);'><u><b>Update</b></u></a></td>";
        //                htmltbl += @"<td><a href='#' style='color:red;' onclick='Delete(&#39;" + tbl.contentid + "&#39;);'><u><b>Delete</b></u></a></td>";
        //                htmltbl += @"</tr>";
        //            }
        //            htmltbl += @"</table>";
        //        }
        //        else
        //        {
        //            htmltbl += @"<div style='color:red;'>Sorry! No record found.</div>";
        //        }
        //        var result = new { htmltbl = htmltbl, countrow = countrow, countrowtxt = countrowtxt };
        //        return Json(result, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //public ActionResult Clients_Update_Final(Int32? contentid, string txtUnique, string txtMessage, string txt1, string txt2, string txt3, string txt4, string txt5, string txt6, string txt7, string txt8, string txt9, string active)
        //{
        //    clientsadminUpdate regupdate = new clientsadminUpdate();
        //    int Error = 0;
        //    if (contentid.HasValue)
        //    {
        //        using (var db = new VconnectDBContext29())
        //        {
        //            var cmd = db.Database.Connection.CreateCommand();
        //            cmd.CommandText = "[dbo].[Client_Update]";
        //            cmd.CommandTimeout = 600;
        //            cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //            //cmd.Parameters.Add(new SqlParameter("@leadid", leadid.Value));
        //            cmd.Parameters.Add(new SqlParameter("@contentid", contentid.Value));
        //            cmd.Parameters.Add(new SqlParameter("@Qname", txt2));
        //            cmd.Parameters.Add(new SqlParameter("@Qtype", txt5));
        //            cmd.Parameters.Add(new SqlParameter("@compalsary", txt4));
        //            cmd.Parameters.Add(new SqlParameter("@validations", txt9));
        //            // cmd.Parameters.Add(new SqlParameter("@uniquetype", txtUnique));
        //            cmd.Parameters.Add(new SqlParameter("@answers", txt6));
        //            cmd.Parameters.Add(new SqlParameter("@active", active));
        //            cmd.Parameters.Add(new SqlParameter("@qorder", txt1));
        //            cmd.Parameters.Add(new SqlParameter("@length", txt7));
        //            cmd.Parameters.Add(new SqlParameter("@columnname", txt3));
        //            cmd.Parameters.Add(new SqlParameter("@uniquename", txtUnique));
        //            cmd.Parameters.Add(new SqlParameter("@placeholder", txt8));
        //            cmd.Parameters.Add(new SqlParameter("@uniquemessage", txtMessage));
        //            cmd.Parameters.Add(new SqlParameter("@Error", SqlDbType.Int));
        //            cmd.Parameters["@Error"].Direction = ParameterDirection.Output;
        //            //cmd.Parameters.Add(new SqlParameter("@referenceid", leadid.Value));
        //            try
        //            {
        //                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
        //                connection.Open();
        //                cmd.ExecuteNonQuery();
        //                Error = Convert.ToInt32(cmd.Parameters["@Error"].Value.ToString());
        //            }
        //            catch (Exception ex)
        //            {
        //                //db.Database.Connection.Close();
        //            }
        //            finally
        //            {
        //                //db.Database.Connection.Close();
        //            }
        //        }
        //    }
        //    return Json(Error, JsonRequestBehavior.AllowGet);
        //}
        //public ActionResult Clients_Delete(Int32? contentid)
        //{
        //    int Error = 0;
        //    if (contentid.HasValue)
        //    {
        //        using (var db = new VconnectDBContext29())
        //        {
        //            var cmd = db.Database.Connection.CreateCommand();
        //            cmd.CommandText = "[dbo].[Client_Delete]";
        //            cmd.CommandTimeout = 600;
        //            cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //            //cmd.Parameters.Add(new SqlParameter("@leadid", leadid.Value));
        //            cmd.Parameters.Add(new SqlParameter("@contentid", contentid.Value));
        //            cmd.Parameters.Add(new SqlParameter("@Error", SqlDbType.Int));
        //            cmd.Parameters["@Error"].Direction = ParameterDirection.Output;
        //            //cmd.Parameters.Add(new SqlParameter("@referenceid", leadid.Value));
        //            try
        //            {
        //                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
        //                connection.Open();
        //                cmd.ExecuteNonQuery();
        //                Error = Convert.ToInt32(cmd.Parameters["@Error"].Value.ToString());
        //            }
        //            catch (Exception ex)
        //            {
        //                //db.Database.Connection.Close();
        //            }
        //            finally
        //            {
        //                //db.Database.Connection.Close();
        //            }
        //        }
        //    }
        //    return Json(Error, JsonRequestBehavior.AllowGet);
        //}
        //public ActionResult Clients_Update_Columnname(Int32? leadid, string txt1)
        //{
        //    string htmldrp = "";
        //    int error = 0;
        //    List<Listdropcolumn> dropcolumn = new List<Listdropcolumn>();
        //    using (var db = new VconnectDBContext29())
        //    {
        //        var cmd = db.Database.Connection.CreateCommand();
        //        cmd.CommandText = "[dbo].[Client_SelectColumns]";
        //        cmd.CommandTimeout = 600;
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        cmd.Parameters.Add(new SqlParameter("@leadid", leadid.Value));
        //        try
        //        {
        //            var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
        //            connection.Open();
        //            var reader = cmd.ExecuteReader();
        //            //tablecolumn = ((IObjectContextAdapter)db).ObjectContext.Translate<Listtablecolumn>(reader).ToList();

        //            reader.NextResult();
        //            dropcolumn = ((IObjectContextAdapter)db).ObjectContext.Translate<Listdropcolumn>(reader).ToList();

        //        }
        //        catch (Exception ex)
        //        {
        //        }
        //        finally
        //        {
        //            //db.Database.Connection.Close();
        //        }
        //    }
        //    if (dropcolumn != null && dropcolumn.Count > 0)
        //    {
        //        int j = 2;
        //        foreach (var clmnm in dropcolumn.Where(m => m.dropcolumn == txt1.Trim() && m.dropcolumn != "source" && m.dropcolumn != "leadid" && m.dropcolumn != "widget" && m.dropcolumn != "clientname"))
        //        {
        //            if (clmnm.dropcolumn.Trim() == txt1.Trim())
        //            {
        //                htmldrp += @"<option value='1'>" + clmnm.dropcolumn.Trim() + "</option>";
        //                error = 1;
        //            }
        //        }
        //        foreach (var clmnm in dropcolumn)
        //        {
        //            htmldrp += @"<option value='" + j + "'>" + clmnm.dropcolumn.Trim() + "</option>";
        //            j++;
        //        }
        //    }
        //    var result = new { htmldrp = htmldrp, error = error };
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
        //#endregion

        #region For Only Admin
        [HttpGet]
        public ActionResult ClientsAddbyAdmin(Int32? leadid, string submit, string error)
        {
            clientsadmin reg = new clientsadmin();
            reg.SuccessMessage = TempData["SuccessMessage"] as string;
            reg.MessageError = TempData["MessageError"] as string;
            if (!string.IsNullOrEmpty(error))
            {
                if (error == "1")
                    reg.SuccessMessage = "You have successful enter data.";
                else if (error == "0")
                    reg.MessageError = "Please field all...";
                else
                {
                    TempData["SuccessMessage"] = "";
                    TempData["MessageError"] = "";
                }
            }
            else
            {
                reg.SuccessMessage = null;
                reg.MessageError = null;
            }
            if (leadid.HasValue)
            {
                reg.leadid = leadid.Value;
            }
            if (!string.IsNullOrEmpty(submit))
            {
                reg.chkrdo = submit;
            }
            else
            {
                reg.chkrdo = "Business";
            }
            reg = Clientsshowform(reg);
            if (leadid.HasValue)
            {
                foreach (var twt in reg.listlead.Where(m => m.leadid == leadid.Value)) { reg.leadname = twt.leadname; }
            }
            return View(reg);
        }

        public ActionResult ClientsAddbyAdmin1(string drp1, string drpQtype, string txtanswers, string txtqname, string drpcompalsary, int txtlength, string txtplaceholder, string drpcolumnname, string txtvalidation, int drpqorder, string drpuniquetype, string refcolumnname, string submit, string data_typetxt)
        {
            Int32 userid = 0;
            if (string.IsNullOrEmpty(drpuniquetype))
                drpuniquetype = "text";
            string sms = string.Empty;
            if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                userid = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
            int Error = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[Client_Registraion]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@leadid", int.Parse(drp1)));
                //cmd.Parameters.Add(new SqlParameter("@Qid", reg.txtqname));
                cmd.Parameters.Add(new SqlParameter("@Qname", txtqname));
                cmd.Parameters.Add(new SqlParameter("@Qtype", drpQtype));
                cmd.Parameters.Add(new SqlParameter("@compalsary", drpcompalsary));
                cmd.Parameters.Add(new SqlParameter("@validations", txtvalidation));
                cmd.Parameters.Add(new SqlParameter("@uniquetype", drpuniquetype));
                cmd.Parameters.Add(new SqlParameter("@answers", txtanswers));
                cmd.Parameters.Add(new SqlParameter("@createdby", userid));
                //cmd.Parameters.Add(new SqlParameter("@modifyby", reg.leadid));
                cmd.Parameters.Add(new SqlParameter("@qorder", drpqorder));
                cmd.Parameters.Add(new SqlParameter("@length", txtlength));
                cmd.Parameters.Add(new SqlParameter("@columnname", drpcolumnname));
                //cmd.Parameters.Add(new SqlParameter("@uniquename", reg.leadid));
                cmd.Parameters.Add(new SqlParameter("@placeholder", txtplaceholder));
                cmd.Parameters.Add(new SqlParameter("@data_typetxt", data_typetxt.Trim()));
                cmd.Parameters.Add(new SqlParameter("@chkrdo", submit.Trim()));
                cmd.Parameters.Add(new SqlParameter("@referenceid", refcolumnname));
                cmd.Parameters.Add(new SqlParameter("@Error", SqlDbType.Int));
                cmd.Parameters["@Error"].Direction = ParameterDirection.Output;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    Error = Convert.ToInt32(cmd.Parameters["@Error"].Value.ToString());
                    if (Error == 0)
                    {
                        TempData["SuccessMessage"] = "Registration successful.";
                        sms = "Registration successful.";
                    }
                    else
                    {
                        TempData["MessageError"] = "Sorry..";
                        sms = "Sorry..";
                    }
                }
                catch (Exception ex)
                {
                    db.Database.Connection.Close();
                }
                finally
                {
                    //db.Database.Connection.Close();
                }
            }
            return Json(sms, JsonRequestBehavior.AllowGet);
            //return RedirectPermanent("/Static/ClientsAddbyAdmin?leadid="+drp1);
            //return View(reg);
        }
        public clientsadmin Clientsshowform(clientsadmin reg)
        {
            List<Listleadid> leaddata = new List<Listleadid>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[Client_Binddata]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                if (reg.leadid != 0 && reg.leadid != null && reg.leadid.ToString() != "")
                    cmd.Parameters.Add(new SqlParameter("@leadid", reg.leadid));
                cmd.Parameters.Add(new SqlParameter("@chkrdo", reg.chkrdo));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    reg.maxorder = ((IObjectContextAdapter)db).ObjectContext.Translate<Questionorder>(reader).FirstOrDefault();

                    reader.NextResult();
                    reg.listlead = ((IObjectContextAdapter)db).ObjectContext.Translate<Listleadid>(reader).ToList();

                    reader.NextResult();
                    reg.listcolumn = ((IObjectContextAdapter)db).ObjectContext.Translate<ColumnNamelist>(reader).ToList();

                    reader.NextResult();
                    reg.listrefcolumn = ((IObjectContextAdapter)db).ObjectContext.Translate<RefColumnNamelist>(reader).ToList();
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    //db.Database.Connection.Close();
                }
            }
            return reg;
        }
        public ActionResult AddColumnsTable(string columnname, string datatype)
        {
            string sms = string.Empty;
            int Error = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[Client_AddColumns]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@columnname", columnname));
                cmd.Parameters.Add(new SqlParameter("@datatype", datatype));

                cmd.Parameters.Add(new SqlParameter("@Error", SqlDbType.Int));
                cmd.Parameters["@Error"].Direction = ParameterDirection.Output;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    Error = Convert.ToInt32(cmd.Parameters["@Error"].Value.ToString());
                    if (Error == 1)
                        sms = "Exist columns name in table.";
                    else
                        sms = "Thanks";
                }
                catch (Exception ex)
                {
                    db.Database.Connection.Close();
                }
                finally
                {
                    //db.Database.Connection.Close();
                }
            }
            return Json(sms, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Set_Query(string tablename2, Int32? leadid)
        {
            clientsadmin reg = new clientsadmin();
            List<Listtablecolumn> tablecolumn = new List<Listtablecolumn>();
            List<Listdropcolumn> dropcolumn = new List<Listdropcolumn>();
            string table1 = "";
            string table2 = "";
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[Client_SelectColumns]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@leadid", leadid.Value));
                cmd.Parameters.Add(new SqlParameter("@tablename", tablename2));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    tablecolumn = ((IObjectContextAdapter)db).ObjectContext.Translate<Listtablecolumn>(reader).ToList();

                    reader.NextResult();
                    dropcolumn = ((IObjectContextAdapter)db).ObjectContext.Translate<Listdropcolumn>(reader).ToList();

                }
                catch (Exception ex)
                {
                }
                finally
                {
                    //db.Database.Connection.Close();
                }
                //table1+="<select name='site-rating1' id='columntxtvalueid_1' style='width:48%;' required=''>";
                table1 += "<option value=''>Please select</option>";
                int i = 1;
                foreach (var item in tablecolumn)
                {
                    table1 += "<option value=" + i + ">" + item.tablecolumn + "</option>";
                    i++;
                }
                // table1+="</select>";

                //table2+="<select name='site-rating1' id='columntxtvalueid_1' style='width:48%;float:right;' required=''>";
                table2 += "<option value=''>Please select</option>";
                int j = 1;
                foreach (var item in dropcolumn)
                {
                    table2 += "<option value=" + j + ">" + item.dropcolumn + "</option>";
                    j++;
                }
                //table2+="</select>";
                var result = new { table1 = table1, table2 = table2 };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult ClientsFinals(Int32? leadid, string submit)
        {
            //regupdate.tablebind = Clients_Update(leadid,"select");
            clientsadminUpdate regupdate = new clientsadminUpdate();
            clientsadmin reg = new clientsadmin();
            if (!string.IsNullOrEmpty(submit))
            {
                regupdate.chkrdo = submit;
                reg.chkrdo = submit;
            }
            else
            {
                regupdate.chkrdo = "Business";
                reg.chkrdo = "Business";
            }
            reg = Clientsshowform(reg);
            regupdate.listlead = reg.listlead;
            return View(regupdate);
        }
        public ActionResult Clients_Update(Int32? leadid, string querytype, string submit)
        {
            clientsadminUpdate regupdate = new clientsadminUpdate();
            List<ListRowbyId> listrowbyId = new List<ListRowbyId>();
            CountRow countrow = new CountRow();
            string htmltbl = "";
            string countrowtxt = "";
            string countrowdrp = "";
            List<Listdropcolumn> dropcolumn = new List<Listdropcolumn>();
            List<Listdropcolumn> dropcolumn1 = new List<Listdropcolumn>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[Client_SelectColumns]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@leadid", leadid.Value));
                cmd.Parameters.Add(new SqlParameter("@type", submit.Trim()));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    //tablecolumn = ((IObjectContextAdapter)db).ObjectContext.Translate<Listtablecolumn>(reader).ToList();

                    reader.NextResult();
                    dropcolumn = ((IObjectContextAdapter)db).ObjectContext.Translate<Listdropcolumn>(reader).ToList();
                    reader.NextResult();
                    dropcolumn1 = ((IObjectContextAdapter)db).ObjectContext.Translate<Listdropcolumn>(reader).ToList();

                }
                catch (Exception ex)
                {
                }
                finally
                {
                    //db.Database.Connection.Close();
                }
            }
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[Client_Update_Select]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@leadid", leadid.Value));
                cmd.Parameters.Add(new SqlParameter("@querytype", querytype));
                cmd.Parameters.Add(new SqlParameter("@chkrdo", submit.Trim()));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    listrowbyId = ((IObjectContextAdapter)db).ObjectContext.Translate<ListRowbyId>(reader).ToList();

                    reader.NextResult();
                    countrow = ((IObjectContextAdapter)db).ObjectContext.Translate<CountRow>(reader).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    //db.Database.Connection.Close();
                }
                finally
                {
                    //db.Database.Connection.Close();
                }
                if (listrowbyId != null && listrowbyId.Count > 0)
                {
                    htmltbl += @"<div><label>Select Unique_Name(Optional)</label><select name='site-rating' style='width:50%;' id='txtUnique' onchange='uniquedrop();'>";
                    string uniquename = "";
                    foreach (var tbl in listrowbyId)
                    {
                        if (!string.IsNullOrEmpty(tbl.uniquename))
                        {
                            uniquename = tbl.uniquename;
                            break;
                        }
                    }
                    if (!string.IsNullOrEmpty(uniquename.Trim()))
                    {
                        htmltbl += @"<option value='1'>" + uniquename.Trim() + "</option>";
                        htmltbl += @"<option value='0'>Select unique name</option>";
                    }
                    else
                        htmltbl += @"<option value='0'>Select unique name</option>";
                    int k = 2;
                    foreach (var clmnm in dropcolumn1.Where(m => m.dropcolumn != uniquename.Trim() && m.dropcolumn != "source" && m.dropcolumn != "leadid" && m.dropcolumn != "widget" && m.dropcolumn != "clientname"))
                    {

                        htmltbl += @"<option value='" + k + "'>" + clmnm.dropcolumn.Trim() + "</option>";
                        k++;
                    }
                    htmltbl += @"</select></div>";
                    foreach (var tbl in listrowbyId)
                    {
                        if (!string.IsNullOrEmpty(tbl.uniquemessage))
                        {
                            htmltbl += "<div><input type='text' id='txtMessage' style='width:50%;' placeholder='Enter unique message' onblur='uniquesms();' value='" + tbl.uniquemessage + "'/><div style='float:right;margin-top: -41px;padding-left: 10px;width: 50%;'><a id='updateid' href='#' style='color:red;' onclick='Updateunique(" + leadid.Value + ");'><b>Update</b></a></div></div>";
                            htmltbl += "<small class='error' id='txtsms' style='display:none;'>Please enter unique message</small>";
                            break;
                        }
                        else
                        {
                            htmltbl += "<div><input type='text' id='txtMessage' style='width:50%;display: none;' placeholder='Enter unique message' onblur='uniquesms();' value='" + tbl.uniquemessage + "'/><div style='float:right;margin-top: -41px;padding-left: 10px;width: 50%;'><a id='updateid' href='#' style='color:red;' onclick='Updateunique(" + leadid.Value + ");'><b>Update</b></a></div></div>";
                            htmltbl += "<small class='error' id='txtsms' style='display:none;'>Please enter unique message</small>";
                            break;
                        }
                    }
                    htmltbl += @"<table id='tab1' class='table'>";
                    htmltbl += @"<tr><th>ID</th><th>Q_Order</th><th>Label_Name</th><th>Column_Name</th><th>Compulsory</th><th>Label_Type</th><th>Bind_Data</th><th>Length</th><th>Water_Mark</th><th>Message</th><th>Pattern_Type</th><th>Active</th><th>Update</th><th>Delete</th></tr>";
                    int clr = 1;
                    foreach (var tbl in listrowbyId)
                    {
                        countrowtxt += tbl.contentid + ",";
                        if (clr % 2 == 0)
                            htmltbl += @"<tr id='tr_" + tbl.contentid + "'><td>" + tbl.contentid + "</td>";
                        else
                            htmltbl += @"<tr id='tr_" + tbl.contentid + "' style='background-color: cornsilk;'><td>" + tbl.contentid + "</td>";
                        htmltbl += @"<td><input type='text' value='" + tbl.qorder.ToString().Trim() + "' id='txt1_" + tbl.contentid + "' onchange='Q_Order(&#39;txt1_" + tbl.contentid + "&#39;);' onkeypress = 'return isNumberKey(event)'></input></td>";
                        htmltbl += @"<td><input type='text' value='" + tbl.Qname.ToString().Trim() + "' id='txt2_" + tbl.contentid + "' style='width:200px;'></td>";
                        htmltbl += @"<td> <select name='site-rating' style='width:150px;' id='txt3_" + tbl.contentid + "' onchange='Column_Name(&#39;txt3_" + tbl.contentid + "&#39;)' onclick='SaveColumnName(&#39;txt3_" + tbl.contentid + "&#39;)'>";
                        if (!string.IsNullOrEmpty(tbl.data_type))
                            htmltbl += @"<option value='1=" + tbl.data_type.Trim() + "'>" + tbl.columnname.Trim() + "</option>";
                        else
                            htmltbl += @"<option value='1=varchar'>" + tbl.columnname + "</option>";
                        int j = 2;
                        foreach (var clmnm in dropcolumn.Where(m => m.dropcolumn != tbl.columnname && m.dropcolumn != "source" && m.dropcolumn != "leadid" && m.dropcolumn != "widget" && m.dropcolumn != "clientname"))
                        {
                            htmltbl += @"<option value='" + j + "=" + clmnm.data_type + "'>" + clmnm.dropcolumn.Trim() + "</option>";
                            j++;
                        }
                        htmltbl += @"</select></td>";

                        htmltbl += @"<td><select name='site-rating' style='width:100px;' id='txt4_" + tbl.contentid + "'>";
                        htmltbl += @"<option value='3'>" + tbl.compalsary.Trim() + "</option>";
                        htmltbl += @"<option value='2'>Required</option>";
                        htmltbl += @"<option value='1'>Optional</option>";
                        htmltbl += @"</select></td>";
                        htmltbl += @"<td> <select name='site-rating' style='width:100px;' id='txt5_" + tbl.contentid + "' onchange='Label_Type(" + tbl.contentid + ")'>";
                        htmltbl += @"<option value='7'>" + tbl.Qtype.Trim() + "</option>";
                        htmltbl += @"<option value='6'>textbox</option>";
                        htmltbl += @"<option value='5'>textarea</option>";
                        htmltbl += @"<option value='4'>drptxt</option>";
                        //htmltbl += @"<option value='3'>drpmaster</option>";
                        htmltbl += @"<option value='2'>radio</option>";
                        htmltbl += @"<option value='1'>checkbox</option>";
                        htmltbl += @"</select></td>";
                        htmltbl += @"</select></td>";
                        if (tbl.Qtype.Trim() == "drpmaster")
                            htmltbl += @"<td><textarea cols='20' maxlength='1700' name='txtanswers' rows='2' id='txt6_" + tbl.contentid + "' style='width:200px;' readonly>" + tbl.answers.ToString().Trim() + "</textarea></td>";
                        else if (tbl.Qtype.Trim() == "textbox" || tbl.Qtype.Trim() == "textarea")
                            htmltbl += @"<td><textarea cols='20' maxlength='1700' name='txtanswers' rows='2' id='txt6_" + tbl.contentid + "' style='display:none;width:200px;'>" + tbl.answers.ToString().Trim() + "</textarea></td>";
                        else
                            htmltbl += @"<td><textarea cols='20' maxlength='1700' name='txtanswers' rows='2' id='txt6_" + tbl.contentid + "' style='width:200px;'>" + tbl.answers.ToString().Trim() + "</textarea></td>";
                        htmltbl += @"<td><input type='text' value=" + tbl.length + " id='txt7_" + tbl.contentid + "' onkeypress = 'return isNumberKey(event)'></td>";
                        htmltbl += @"<td><input type='text' value='" + tbl.placeholder.ToString().Trim() + "' id='txt8_" + tbl.contentid + "' style='width:200px;'></td>";
                        htmltbl += @"<td><input type='text' value='" + tbl.validations.ToString().Trim() + "' id='txt9_" + tbl.contentid + "' style='width:200px;'></td>";
                        htmltbl += @"<td>";
                        if (tbl.Qtype.Trim() == "textbox")
                        {
                            htmltbl += @"<select name='site-rating' style='width:100px;' id='txt11_" + tbl.contentid + "' onchange='return Check_datatype(" + tbl.contentid + ")'>";
                        }
                        else
                        {
                            htmltbl += @"<select name='site-rating' style='width:100px;' id='txt11_" + tbl.contentid + "' disabled='' onchange='return Check_datatype(" + tbl.contentid + ")'>";
                        }
                        htmltbl += @"<option value='3'>" + tbl.uniquetype.Trim() + "</option>";
                        htmltbl += @"<option value='0'>Please select</option>";
                        htmltbl += @"<option value='1'>text</option>";
                        htmltbl += @"<option value='2'>phone</option>";
                        htmltbl += @"<option value='3'>email</option>";
                        htmltbl += @"<option value='4'>numeric</option>";
                        htmltbl += @"<option value='5'>alphanumeric</option>";
                        htmltbl += @"<option value='6'>website</option>";
                        htmltbl += @"<option value='7'>calender</option>";
                        htmltbl += @"<option value='8'>dob</option>";
                        htmltbl += @"</select></td>";
                        if (tbl.active != null && tbl.active.Trim() == "1")
                            htmltbl += @"<td><input type='checkbox' value='" + tbl.contentid + "' checked='checked'  id='txt10_" + tbl.contentid + "' onclick='Update(&#39;" + tbl.contentid + "&#39;,&#39;active&#39;);'/></td>";
                        else
                            htmltbl += @"<td><input type='checkbox' value='" + tbl.contentid + "'  id='txt10_" + tbl.contentid + "' onclick='Update(&#39;" + tbl.contentid + "&#39;,&#39;active&#39;);'/></td>";

                        htmltbl += @"<td><a href='#' style='color:green;' onclick='Update(&#39;" + tbl.contentid + "&#39;,&#39;update&#39;);'><u><b>Update</b></u></a></td>";
                        //if (tbl.active != null && tbl.active.Trim() == "1")
                        //    htmltbl += @"<td><input type='checkbox' value='" + tbl.contentid + "' checked='checked' onclick='Update(&#39;" + tbl.contentid + "&#39;);' id='txt10_" + tbl.contentid + "'><a href='#' style='color:green;' onclick='Update(&#39;" + tbl.contentid + "&#39;);'><u><b>Update</b></u></a></td>";
                        //else
                        //    htmltbl += @"<td><input type='checkbox' value='" + tbl.contentid + "' onclick='Update(&#39;" + tbl.contentid + "&#39;);' id='txt10_" + tbl.contentid + "'><a href='#' style='color:green;' onclick='Update(&#39;" + tbl.contentid + "&#39;);'><u><b>Update</b></u></a></td>";
                        htmltbl += @"<td><a href='#' style='color:red;' onclick='Delete(&#39;" + tbl.contentid + "&#39;);'><u><b>Delete</b></u></a></td>";
                        htmltbl += @"</tr>";
                        clr++;
                    }
                    htmltbl += @"</table>";
                }
                else
                {
                    htmltbl += @"<div style='color:red;'>Sorry! No record found.</div>";
                }
                int countrow1 = countrow.countrowid;
                int anyactive = countrow.anyactive;
                var result = new { htmltbl = htmltbl, countrow1 = countrow1, countrowtxt = countrowtxt, anyactive = anyactive };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Clients_Update_Final(Int32? contentid, string txt1, string txt2, string txt3, string txt4, string txt5, string txt6, string txt7, string txt8, string txt9, string txt11, string active, string data_type)
        {
            if (txt3.Contains(' '))
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
            clientsadminUpdate regupdate = new clientsadminUpdate();
            int Error = 0;
            if (contentid.HasValue)
            {
                using (var db = new VconnectDBContext29())
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[Client_Update]";
                    cmd.CommandTimeout = 600;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //cmd.Parameters.Add(new SqlParameter("@leadid", leadid.Value));
                    cmd.Parameters.Add(new SqlParameter("@contentid", contentid.Value));
                    cmd.Parameters.Add(new SqlParameter("@Qname", txt2));
                    cmd.Parameters.Add(new SqlParameter("@Qtype", txt5));
                    cmd.Parameters.Add(new SqlParameter("@compalsary", txt4));
                    cmd.Parameters.Add(new SqlParameter("@validations", txt9));
                    cmd.Parameters.Add(new SqlParameter("@uniquetype", txt11));
                    cmd.Parameters.Add(new SqlParameter("@answers", txt6));
                    cmd.Parameters.Add(new SqlParameter("@active", active));
                    cmd.Parameters.Add(new SqlParameter("@qorder", txt1));
                    cmd.Parameters.Add(new SqlParameter("@length", txt7));
                    cmd.Parameters.Add(new SqlParameter("@columnname", txt3));
                    //cmd.Parameters.Add(new SqlParameter("@uniquename", txtUnique));
                    cmd.Parameters.Add(new SqlParameter("@placeholder", txt8));
                    cmd.Parameters.Add(new SqlParameter("@data_type", data_type.Trim()));
                    cmd.Parameters.Add(new SqlParameter("@Error", SqlDbType.Int));
                    cmd.Parameters["@Error"].Direction = ParameterDirection.Output;
                    //cmd.Parameters.Add(new SqlParameter("@referenceid", leadid.Value));
                    try
                    {
                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        cmd.ExecuteNonQuery();
                        Error = Convert.ToInt32(cmd.Parameters["@Error"].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        //db.Database.Connection.Close();
                    }
                    finally
                    {
                        //db.Database.Connection.Close();
                    }
                }
            }
            return Json(Error, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Update_Unique(Int32? leadid, string txtUnique, string txtMessage, string submit, int contentid)
        {
            int Error = 0;
            if (leadid.HasValue)
            {
                using (var db = new VconnectDBContext29())
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[Client_Update_Unique]";
                    cmd.CommandTimeout = 600;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@contentid", contentid));
                    cmd.Parameters.Add(new SqlParameter("@leadid", leadid.Value));
                    cmd.Parameters.Add(new SqlParameter("@uniquename", txtUnique));
                    cmd.Parameters.Add(new SqlParameter("@uniquemessage", txtMessage));
                    cmd.Parameters.Add(new SqlParameter("@chkrdo", submit.Trim()));
                    cmd.Parameters.Add(new SqlParameter("@Error", SqlDbType.Int));
                    cmd.Parameters["@Error"].Direction = ParameterDirection.Output;
                    try
                    {
                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        cmd.ExecuteNonQuery();
                        Error = Convert.ToInt32(cmd.Parameters["@Error"].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        //db.Database.Connection.Close();
                    }
                    finally
                    {
                        //db.Database.Connection.Close();
                    }
                }
            }
            return Json(Error, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Clients_Delete(Int32? contentid)
        {
            int Error = 0;
            if (contentid.HasValue)
            {
                using (var db = new VconnectDBContext29())
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[Client_Delete]";
                    cmd.CommandTimeout = 600;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //cmd.Parameters.Add(new SqlParameter("@leadid", leadid.Value));
                    cmd.Parameters.Add(new SqlParameter("@contentid", contentid.Value));
                    cmd.Parameters.Add(new SqlParameter("@Error", SqlDbType.Int));
                    cmd.Parameters["@Error"].Direction = ParameterDirection.Output;
                    //cmd.Parameters.Add(new SqlParameter("@referenceid", leadid.Value));
                    try
                    {
                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        cmd.ExecuteNonQuery();
                        Error = Convert.ToInt32(cmd.Parameters["@Error"].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        //db.Database.Connection.Close();
                    }
                    finally
                    {
                        //db.Database.Connection.Close();
                    }
                }
            }
            return Json(Error, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Clients_Update_Columnname(Int32? leadid, string txt1)
        {
            string htmldrp = "";
            int error = 0;
            List<Listdropcolumn> dropcolumn = new List<Listdropcolumn>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[Client_SelectColumns]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@leadid", leadid.Value));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    //tablecolumn = ((IObjectContextAdapter)db).ObjectContext.Translate<Listtablecolumn>(reader).ToList();

                    reader.NextResult();
                    dropcolumn = ((IObjectContextAdapter)db).ObjectContext.Translate<Listdropcolumn>(reader).ToList();

                }
                catch (Exception ex)
                {
                }
                finally
                {
                    //db.Database.Connection.Close();
                }
            }
            if (dropcolumn != null && dropcolumn.Count > 0)
            {
                int j = 2;
                foreach (var clmnm in dropcolumn.Where(m => m.dropcolumn == txt1.Trim() && m.dropcolumn != "source" && m.dropcolumn != "leadid" && m.dropcolumn != "widget" && m.dropcolumn != "clientname"))
                {
                    if (clmnm.dropcolumn.Trim() == txt1.Trim())
                    {
                        htmldrp += @"<option value='1=" + clmnm.data_type + "'>" + clmnm.dropcolumn.Trim() + "</option>";
                        error = 1;
                    }
                }
                foreach (var clmnm in dropcolumn)
                {
                    htmldrp += @"<option value='" + j + "=" + clmnm.data_type + "'>" + clmnm.dropcolumn.Trim() + "</option>";
                    j++;
                }
            }
            var result = new { htmldrp = htmldrp, error = error };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ClientsView(Int32? leadid, string submit)
        {
            //regupdate.tablebind = Clients_Update(leadid,"select");
            clientsadminUpdate regupdate = new clientsadminUpdate();
            clientsadmin reg = new clientsadmin();
            if (!string.IsNullOrEmpty(submit))
            {
                regupdate.chkrdo = submit;
                reg.chkrdo = submit;
            }
            else
            {
                regupdate.chkrdo = "Business";
                reg.chkrdo = "Business";
            }
            reg = Clientsshowform(reg);
            regupdate.listlead = reg.listlead;
            return View(regupdate);
        }
        public ActionResult Clients_ViewBind(Int32? leadid, string querytype, string submit)
        {
            clientsadminUpdate regupdate = new clientsadminUpdate();
            List<ListRowbyId> listrowbyId = new List<ListRowbyId>();
            List<ListRowbyId> listrowbyIdAct = new List<ListRowbyId>();
            List<ListRowbyId> listrowbyIdNoAct = new List<ListRowbyId>();
            CountRow countrow = new CountRow();
            string htmltbl = "";
            string countrowtxt = "";
            string countrowdrp = "";
            List<Listdropcolumn> dropcolumn = new List<Listdropcolumn>();
            List<Listdropcolumn> dropcolumn1 = new List<Listdropcolumn>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[Client_SelectColumns]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@leadid", leadid.Value));
                cmd.Parameters.Add(new SqlParameter("@type", submit.Trim()));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    //tablecolumn = ((IObjectContextAdapter)db).ObjectContext.Translate<Listtablecolumn>(reader).ToList();

                    reader.NextResult();
                    dropcolumn = ((IObjectContextAdapter)db).ObjectContext.Translate<Listdropcolumn>(reader).ToList();
                    reader.NextResult();
                    dropcolumn1 = ((IObjectContextAdapter)db).ObjectContext.Translate<Listdropcolumn>(reader).ToList();

                }
                catch (Exception ex)
                {
                }
                finally
                {
                    //db.Database.Connection.Close();
                }
            }
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[Client_Views_Select]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@leadid", leadid.Value));
                cmd.Parameters.Add(new SqlParameter("@querytype", querytype));
                cmd.Parameters.Add(new SqlParameter("@chkrdo", submit.Trim()));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    listrowbyIdAct = ((IObjectContextAdapter)db).ObjectContext.Translate<ListRowbyId>(reader).ToList();

                    reader.NextResult();
                    listrowbyIdNoAct = ((IObjectContextAdapter)db).ObjectContext.Translate<ListRowbyId>(reader).ToList();

                    reader.NextResult();
                    countrow = ((IObjectContextAdapter)db).ObjectContext.Translate<CountRow>(reader).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    //db.Database.Connection.Close();
                }
                finally
                {
                    //db.Database.Connection.Close();
                }
                if ((listrowbyIdAct != null && listrowbyIdAct.Count > 0) || (listrowbyIdNoAct != null && listrowbyIdNoAct.Count > 0))
                {
                    string unq = string.Empty;
                    if (!string.IsNullOrEmpty(listrowbyIdAct.FirstOrDefault().uniquename)) { unq = listrowbyIdAct.FirstOrDefault().uniquename.Trim(); }
                    htmltbl += @"<div>Table:<b>Clientadds</b><br/>Unique Columns Name:<b>" + unq + "</b><br/></div><br/>";
                    htmltbl += @"<table id='tab1' class='table'>";
                    htmltbl += @"<tr><th>Sr.No.</th><th>Q_Order</th><th>Label_Name</th><th>Compulsory</th><th>Error Message</th><th>Label_Type</th><th>Bind_Data</th><th>Length</th><th>Water_Mark</th><th>Pattern_Type</th><th>Data_Type</th><th>Column_Name</th><th>Active</th></tr>";
                    int clr = 1;
                    int srno = 1;
                    foreach (var tbl in listrowbyIdAct)
                    {
                        countrowtxt += tbl.contentid + ",";
                        if (clr % 2 == 0)
                            htmltbl += @"<tr><td>" + srno + "</td>";
                        else
                            htmltbl += @"<tr style='background-color: cornsilk;'><td>" + srno + "</td>";
                        htmltbl += @"<td>" + tbl.qorder.ToString().Trim() + "</td>";
                        htmltbl += @"<td>" + tbl.Qname.ToString().Trim() + "</td>";
                        htmltbl += @"<td>" + tbl.compalsary.Trim() + "</td>";
                        htmltbl += @"<td>" + tbl.validations.ToString().Trim() + "</td>";
                        htmltbl += @"<td> " + tbl.Qtype.Trim() + "</td>";
                        htmltbl += @"<td>" + tbl.answers.ToString().Trim() + "</td>";
                        htmltbl += @"<td>" + tbl.length + "</td>";
                        htmltbl += @"<td>" + tbl.placeholder.ToString().Trim() + "</td>";
                        htmltbl += @"<td>" + tbl.uniquetype.Trim() + "</td>";
                        htmltbl += @"<td>" + tbl.data_type.ToString().Trim() + "</td>";
                        htmltbl += @"<td>" + tbl.columnname.Trim() + "</td>";
                        htmltbl += @"<td><input type='checkbox' value='" + tbl.contentid + "' checked='checked' disabled/></td>";

                        htmltbl += @"</tr>";
                        clr++;
                        srno++;
                    }
                    foreach (var tbl in listrowbyIdNoAct)
                    {
                        countrowtxt += tbl.contentid + ",";
                        if (clr % 2 == 0)
                            htmltbl += @"<tr><td>" + srno + "</td>";
                        else
                            htmltbl += @"<tr style='background-color: cornsilk;'><td>" + srno + "</td>";
                        htmltbl += @"<td>" + tbl.qorder.ToString().Trim() + "</td>";
                        htmltbl += @"<td>" + tbl.Qname.ToString().Trim() + "</td>";
                        htmltbl += @"<td>" + tbl.compalsary.Trim() + "</td>";
                        htmltbl += @"<td>" + tbl.validations.ToString().Trim() + "</td>";
                        htmltbl += @"<td> " + tbl.Qtype.Trim() + "</td>";
                        htmltbl += @"<td>" + tbl.answers.ToString().Trim() + "</td>";
                        htmltbl += @"<td>" + tbl.length + "</td>";
                        htmltbl += @"<td>" + tbl.placeholder.ToString().Trim() + "</td>";
                        htmltbl += @"<td>" + tbl.uniquetype.Trim() + "</td>";
                        htmltbl += @"<td>" + tbl.data_type.ToString().Trim() + "</td>";
                        htmltbl += @"<td>" + tbl.columnname.Trim() + "</td>";
                        htmltbl += @"<td><input type='checkbox' value='" + tbl.contentid + "' disabled/></td>";

                        htmltbl += @"</tr>";
                        clr++;
                        srno++;
                    }
                    htmltbl += @"</table>";
                }
                else
                {
                    htmltbl += @"<div style='color:red;'>Sorry! No record found.</div>";
                }
                int countrow1 = countrow.countrowid;
                int anyactive = countrow.anyactive;
                var result = new { htmltbl = htmltbl, countrow1 = countrow1, countrowtxt = countrowtxt, anyactive = anyactive };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}
