﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using Vconnect.Mapping.CommonDb;
using Vconnect.Models;
using System.Data.Entity.Infrastructure;
using Vconnect.Mapping.ListingWEB;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using Vconnect.Common;
using System.Text;
using System.Configuration;
using System.Drawing;

namespace Vconnect.Controllers
{
    public class BoDashboardWebController : Controller
    {
        UserSession objUS = new UserSession();
        #region Declarations
        public string IsHeadOffice = string.Empty;
        public string webconfigSourceImagesRootPath = ConfigurationManager.AppSettings["SourceImagesRootPath"].ToString();
        public string webconfigDestinationImagesRootPath = ConfigurationManager.AppSettings["DestinationImagesRootPath"].ToString();
        public string webconfigImagesRootPath = ConfigurationManager.AppSettings["ImagesRootPath"].ToString();


        #endregion
        // GET: /BoDashboardWeb/
        
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetCityByState(int catId)
        {
            return Json(Vconnect.Common.Utility.GetSelectListCity(catId), JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetAreaByCity(int catId=0, int catId1=0)
        {
            return Json(Utility.GetSelectListArea(catId1, catId), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetEmployeeStrength(int catId)
        {
            return Json (GetEmployeeStrength(catId), JsonRequestBehavior.AllowGet);
        }
       

        public ActionResult Index(string bid, string bname)
        {

            try
            {
                BoDashboardModel obj = new BoDashboardModel();
                //if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
                if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
                {
                    Session.Add("Successcontroller",null);
                    Session.Add("Successaction",null);

                    Session.Add("Successcontroller",null);
                    Session.Add("Successaction",null);

                    string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
                    if (Request.QueryString["BID"] != null)
                    {

                        int chkuserlogin = CheckUserLogin(SV_VCUserContentID, Request.QueryString["BID"].ToString());
                        if (chkuserlogin == 1)
                        {
                            Session.Add("Successcontroller","BoDashboardWeb");
                            Session.Add("Successaction","Index");
                            Session.Add("Successcontroller","BoDashboardWeb");
                            Session.Add("Successaction","Index");
                            return RedirectToAction("Login", "Account");
                        }
                        obj.BusinessID = Request.QueryString["BID"].ToString();
                        obj.BusinessName = Request.QueryString["bname"].ToString();
                        obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                        ViewBag.BusinessID = obj.BusinessID;
                        ViewBag.BusinessName = obj.BusinessName;
                    }
                    if (obj.BusinessID == null && bid == null)
                    {
                        string[] arrayr;
                        string result = Getdefaultbusinessdata(SV_VCUserContentID);
                        arrayr = result.Split(',');
                        obj.BusinessID = arrayr[0];
                        obj.BusinessName = arrayr[1];
                        obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                        obj.DefaultBusinessID = arrayr[0];
                        obj.DefaultBusinessName = arrayr[1];
                        obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");
                        ViewBag.BusinessID = obj.BusinessID;
                        ViewBag.BusinessName = obj.BusinessName;
                    }
                    else if (bid != null && bname != null)
                    {
                        obj.BusinessID = bid;
                        obj.BusinessName = bname;
                        obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    }
                    else
                    {

                    }

                    if (obj.DefaultBusinessID == null || obj.DefaultBusinessID.ToString().Length <= 0)
                    {
                        string[] arrayr;
                        string result = Getdefaultbusinessdata(SV_VCUserContentID);
                        arrayr = result.Split(',');
                        obj.DefaultBusinessID = arrayr[0];
                        obj.DefaultBusinessName = arrayr[1];
                        obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    }
                    obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
                    if (obj._claimedbusinesslist.Count == 0)
                    {
                        return Redirect("/");
                    }

                    
                    if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && objUS.SV_VCLoginID.ToString().Trim() != "" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59" && obj.BusinessID != null && obj.BusinessID.ToString().Trim() != "")
                    {
                        obj.ProfileCompletionvalue = obj.GetProfileCompletion(obj.BusinessID);
                    }
                    else
                    {
                        return Redirect("mybusiness");   
                    }

                    obj._bodashboardbannerlist = BodashboardbannerResultFunc(3);
                    ViewBag.bodbanner = obj;
                    ViewBag.boroothpath = obj.RootPath;
                    obj.Pagename = "BoDashboardWeb/Index";
                    //obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
                    string[] arraydata;
                    string results = GetleftMenudata(obj.BusinessID);
                    arraydata = results.Split(',');
                    obj.membership = arraydata[0];
                    obj.ProfileImage = arraydata[2];
                    obj.CompanyLogo = arraydata[1];
                    obj.Pagename = "BuisnessProfile";

                    GetBizDetails_BODashBoard(Convert.ToInt32(obj.BusinessID), obj);

                    obj.returncontroller = "BoDashboardWeb";
                    obj.returnaction = "Index";
                    List<SelectListItem> ListStatesNames = new List<SelectListItem>();
                    SearchBarWebModel searchwebmodel = new SearchBarWebModel();
                    searchwebmodel.StateOptions = Utility.GetSelectList();

                    List<SelectListItem> listStatesNames = new List<SelectListItem>();
                    obj.StateOptions = Utility.GetSelectList();

                    List<SelectListItem> listCiyNames = new List<SelectListItem>();
                    obj.SelectedCityId = obj.BusienssLocalGovermentId;
                    obj.CityOptions = Utility.GetSelectListCity(obj.BusinessStateId);
                    obj.SelectedCityId = obj.BusienssLocalGovermentId;
                    obj.SelectedStateId = obj.BusinessStateId;

                    List<SelectListItem> listAreaNames = new List<SelectListItem>();
                    obj.SelectedAreaId = obj.Areaid;
                    obj.AreaOptions = Utility.GetSelectListArea(obj.BusienssLocalGovermentId, obj.BusinessStateId);
                    // List<SelectListItem> listEmpOptions = new List<SelectListItem>();
                    //obj.EmpOptionName = GetSelectEmployee();

                    return View("businessProfile", obj);

                }
                else
                {

                    //Session.Add("Successcontroller","BoDashboardWeb");
                    //Session.Add("Successaction","Index");
                    //  return RedirectToAction("Login", "Account", new { rturl = "BoDashboardWeb/Index/" });
                    return Redirect("/login?rturl=BoDashboardWeb/Index");
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);

                return Redirect("/");
            }
            finally
            {

            }
        }


        public List<Bodashboardbannerlisting> BodashboardbannerResultFunc(int? count)
        {
            List<Bodashboardbannerlisting> _Bodashboardbannerlisting = new List<Bodashboardbannerlisting>();
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_staticpagebanner_opt]";
                cmd.Parameters.Add(new SqlParameter("@count", count));
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                try
                {
                    // Run the sproc
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    _Bodashboardbannerlisting = ((IObjectContextAdapter)db).ObjectContext.Translate<Bodashboardbannerlisting>(reader).ToList();

                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            return _Bodashboardbannerlisting;

        }
        //public List< Vconnect.Models.BoDashboardModel.ClaimedBusinessList> getbusiness(string userid)
        //{
        //    //SqlConnection _con = new SqlConnection(ConfigurationSettings.AppSettings["constr"]);
        //    //SqlCommand _com = new SqlCommand();
        //    //_com.CommandTimeout = 50;
        //    //_com.CommandType = CommandType.StoredProcedure;
        //    //_com.Connection = _con;
        //    //_com.CommandText = "prc_get_multiplebusiness";
        //    //_com.Parameters.AddWithValue("@userid", userid);

        //    //SqlDataAdapter da = new SqlDataAdapter();
        //    //da.SelectCommand = _com;
        //    //DataTable dt = new DataTable();

        //    //_con.Open();
        //    //da.Fill(dt);
        //    //_con.Close();
        //    //_con.Dispose();

        //    Vconnect.Models.BoDashboardModel.ClaimedBusinessList cbl = new Vconnect.Models.BoDashboardModel.ClaimedBusinessList();
        //    List<Vconnect.Models.BoDashboardModel.ClaimedBusinessList> lstClaimBusinessList=new List<BoDashboardModel.ClaimedBusinessList>();
        //    using (var db = new VconnectDBContext29())
        //    {
        //        // Create a SQL command to execute the sproc
        //        var cmd = db.Database.Connection.CreateCommand();
        //        cmd.CommandText = "[dbo].[prc_get_multiplebusiness]";
        //        cmd.Parameters.Add(new SqlParameter("@userid", userid));
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        try
        //        {
        //            // Run the sproc
        //            var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
        //            connection.Open();
        //            var reader = cmd.ExecuteReader();
        //            lstClaimBusinessList = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.BoDashboardModel.ClaimedBusinessList>(reader).ToList();

        //        }
        //        finally
        //        {
        //            db.Database.Connection.Close();
        //        }
        //    }

        //    return lstClaimBusinessList;
        //}
        //protected void bindbusiness(string userid)
        //{
        //    string bname = string.Empty;
        //    ChKBBuisnesslist.Items.Clear();
        //    DataTable dt = objBLBO.getbusiness(userid);
        //    if (dt.Rows.Count > 1)
        //    {
        //        ChKBBuisnesslist.DataSource = dt;

        //        string blink = "";
        //        // ChKBBuisnesslist.Text = blink Selected="True";
        //        ChKBBuisnesslist.DataTextField = "businessname";
        //        ChKBBuisnesslist.DataValueField = "contentid";
        //        //ChKBBuisnesslist.ToolTip = "Select to set this Buisness as Default";
        //        ChKBBuisnesslist.DataBind();
        //        alink.Visible = true;
        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            bname = string.Empty;
        //            bname = Regex.Replace(dt.Rows[i]["businessname"].ToString(), @"([^a-zA-Z0-9_]|^\s)", "-");
        //            blink = "<a href=\"" + webconfigWebsiteRootPath.ToString() + "editbusiness/businessprofile/" + bname + "/" + dt.Rows[i]["contentid"] + "\">" + dt.Rows[i]["businessname"] + "</a>";

        //            ChKBBuisnesslist.Items[i].Text = blink;

        //            if (Convert.ToInt32(dt.Rows[i]["isdefault"]) == 1)
        //            {
        //                ChKBBuisnesslist.Items[i].Selected = true;
        //            }


        //        }
        //    }
        //    else
        //    {
        //        ChKBBuisnesslist.Items.Clear();
        //        ChKBBuisnesslist.Visible = false;
        //        alink.Visible = false;
        //    }


        //}

        public ActionResult iSetdefaultbusiness(int? bid, string returnaction, string retuncontroller)
        {
            string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();

            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_SetDefaultbusiness_opt]";
                cmd.Parameters.Add(new SqlParameter("@userid", SV_VCUserContentID));
                cmd.Parameters.Add(new SqlParameter("@businessid", bid));

                SqlParameter parameter1 = new SqlParameter("@ERR", SqlDbType.Int);
                parameter1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(parameter1);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                try
                {
                    // Run the sproc
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    int result = (int)cmd.Parameters["@ERR"].Value;
                    // lstClaimBusinessList = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.BoDashboardModel.ClaimedBusinessList>(reader).ToList();
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            return RedirectToAction(returnaction, retuncontroller);
        }
        
        public List<Vconnect.Models.BoDashboardModel.ClaimedBusinessList> getbusiness(string userid)
        {

            Vconnect.Models.BoDashboardModel.ClaimedBusinessList cbl = new Vconnect.Models.BoDashboardModel.ClaimedBusinessList();
            List<Vconnect.Models.BoDashboardModel.ClaimedBusinessList> lstClaimBusinessList = new List<BoDashboardModel.ClaimedBusinessList>();
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_multiplebusiness_opt]";
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                try
                {
                    // Run the sproc
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    lstClaimBusinessList = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.BoDashboardModel.ClaimedBusinessList>(reader).ToList();
                   
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            return lstClaimBusinessList;
        }
        public int CheckUserLogin(string userid, string businessid)
        {
            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_checkBusiness_opt]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));

                SqlParameter parameter1 = new SqlParameter("@Err", SqlDbType.Int);
                parameter1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(parameter1);
                connection.Open();
                cmd.ExecuteNonQuery();
                result = Convert.ToInt32(cmd.Parameters["@Err"].Value);

            }
            return result;
        }
        public string Getdefaultbusinessdata(string userid)
        {
            string result;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_getDefaultbuiness_opt_new]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                //SqlParameter bid = new SqlParameter("@businessid", SqlDbType.VarChar, 100);
                //SqlParameter bname = new SqlParameter("@businessname", SqlDbType.VarChar, 512);
                //bid.Direction = ParameterDirection.Output;
                //bname.Direction = ParameterDirection.Output;

                SqlParameter parameter1 = new SqlParameter("@Err", SqlDbType.Int);
                parameter1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(parameter1);
                //cmd.Parameters.Add(bid);
                //cmd.Parameters.Add(bname);

                connection.Open();
                cmd.ExecuteNonQuery();
                var reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    result = (string)reader["businessid"].ToString() + "," + reader["businessname"];
                }
                else
                {
                    result = (string)"0" + "," + "1";
                }

            }


            return result;

        }

        public string GetleftMenudata(string businessid)
        {
            string result;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_Buisnesslogo_details_opt]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                connection.Open();
                var reader = cmd.ExecuteReader();
                reader.Read();
                if (reader.HasRows)
                {
                    result = reader["membership"].ToString();
                    reader.NextResult();
                    reader.Read();

                    result = result + "," + reader["companylogo"].ToString();

                    reader.NextResult();
                    reader.Read();

                    result = result + "," + reader["photo"].ToString();
                    reader.NextResult();
                    reader.Read();
                    result = result + "," + reader["attachments"].ToString();
                }
                else
                {
                    result = "0,0,0,0";
                }
            }


            return result;

        }

        [HttpGet]
        protected void GetBizDetails_BODashBoard(int businessid, BoDashboardModel obj)
        {
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_businessdetails_BODashboard_new_opt]";
                cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    reader.Read();

                    //Rate = reader["Rating"].ToString();
                    obj.BusinessAddress = reader["address"].ToString();
                    obj.Houseno = reader["houseno"].ToString();
                    obj.BusinessLandmark = reader["landmark"].ToString();
                    obj.BusinessPhone = reader["phone"].ToString();
                    obj.BusinessWebsite = reader["website"].ToString();
                    obj.IsRecomended = reader["membership"].ToString();
                    obj.BusinessEmail = reader["email"].ToString();
                   
                    obj.BusinessAlternateEmail = reader["alternateemail"].ToString();
                    obj.GBusinessName = reader["businessname"].ToString();
                    obj.GCompanyLogo = reader["companylogo"].ToString();
                    obj.modeadvertisemnet = reader["preferredadvert"].ToString();
                    obj.BusinessState = reader["state"].ToString();
                    obj.BusinessStateId = Convert.ToInt32(reader["stateid"].ToString());
                    obj.BusienssLocalGovermentId = Convert.ToInt32(reader["cityid"].ToString());
                    if (reader["areaid"] != null)
                    {
                        obj.Areaid = Convert.ToInt32(reader["areaid"].ToString());
                    }
                    obj.LocalGovernment = reader["city"].ToString();
                    obj.Area = reader["area"].ToString();
                    obj.EmployeeStrength = reader["empstrength"].ToString();
                    obj.IsInternet = reader["isinternet"].ToString();
                    obj.alternatephone = reader["alternatephone"].ToString();
                    obj.Businessshortdesc = reader["shortdesc"].ToString();
                    ViewBag.rdo = obj.IsInternet;
                    ViewBag.ddlEmpStrength = new SelectList(obj.EmployeeStrength, "EmpOptionId", "EmpOptionName");


                }
                finally
                {
                    db.Database.Connection.Close();
                }


            }

        }
        [HttpGet]
        public ActionResult updateBizDetailsBoDashBoard()
        {
            return Redirect("/login?rturl=BoDashboardWeb/Index");
        }

        [HttpPost]
        public ActionResult updateBizDetailsBoDashBoard(FormCollection collection, BoDashboardModel obj, HttpPostedFileBase uplPhoto)
        {
            string Userid = objUS.SV_VCUserContentID.ToString();
            string sThumbFileName = string.Empty; string sLargeFileName = string.Empty;
            bool FileSaved = false;
            if (collection["GBusinessName"].Trim().ToString() == "")
            {

                Session.Add("updateE","Enter Buisness Name");
                return Index(obj.BusinessID, obj.GBusinessName);

            }
            if (collection["BusinessPhone"].Trim().ToString() == "")
            {

                Session.Add("updateE","Enter Business Phone");
                return Index(obj.BusinessID, obj.GBusinessName);

            }

            //if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
                Session.Add("Successcontroller",null);
                Session.Add("Successaction",null);
                //string mode_adv = string.Empty;
                //mode_adv = collection["modeadvertisemnet"].ToString();
                var mode_adv = Request.Form["chk"];
                ///////
                if (uplPhoto != null)
                {
                    string extensionadd = System.IO.Path.GetExtension(uplPhoto.FileName).ToLower();

                    int bSize = uplPhoto.ContentLength;
                    // Allow only files less than 10240 bytes (approximately 10 KB) to be uploaded.
                    if (bSize <= 5242880)
                    {
                        Random robj = new Random();
                        int randomno = robj.Next(0, 100000000);
                        if (extensionadd == ".gif" || extensionadd == ".jpg" || extensionadd == ".png" || extensionadd == ".jpeg" || extensionadd == ".bmp")
                        {
                            //string UploadFolder = "~/Resource/uploads/companylogo/";
                            //sThumbFileName = uplPhoto.FileName.ToString().Trim().Replace(" ", "-").Replace("&", "and").Replace("'", "`") + "_orginal_" + randomno + extensionadd;
                            sThumbFileName = collection["BusinessID"] + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + extensionadd;
                            //Vconnect.Common.Utility.ImageHandler newImg = new Vconnect.Common.Utility.ImageHandler(uplPhoto, UploadFolder.ToLower(), sThumbFileName.ToLower(), sThumbFileName.ToLower());
                            //bool uploadLrg = newImg.UploadImage();
                            //string newFilename = newImg.NewFilename;
                            //sThumbFileName = newFilename;
                            FileSaved = true;
                            if (FileSaved)
                            {
                                //sThumbFileName = "uploads/companylogo/" + sThumbFileName.ToString().Trim();
                                //Vconnect.Common.Utility.CopyImage.Sync(webconfigSourceImagesRootPath + "uploads", webconfigDestinationImagesRootPath + "uploads");
                                //Vconnect.Common.Utility.CopyImage.Sync(webconfigSourceImagesRootPath + "companylogo", webconfigDestinationImagesRootPath + "companylogo");
                                sThumbFileName = sThumbFileName.ToString().Trim();
                                //  var fn1 = Server.MapPath("~/Resource/uploads/userphotos/original/" + filename);
                                ImageConverter converter = new ImageConverter();
                                System.Drawing.Image uploadImage = System.Drawing.Image.FromStream(uplPhoto.InputStream);
                                var temp = (byte[])converter.ConvertTo(uploadImage, typeof(byte[]));
                                Utility.ByteCloudUploadImages(temp, sThumbFileName, "vcsites/vcimages/resource/uploads/CompanyLogo/");
                                ViewBag.message = "";
                                Session.Add("updateE","");
                            }
                            else
                            {
                                sThumbFileName = "";
                                Session.Add("updateE","Some Error has occured please upload image again");
                                return Index(obj.BusinessID, obj.GBusinessName);
                            }
                        }
                        else
                        {
                            sThumbFileName = "";
                            Session.Add("updateE","only .gif, .jpg, .png, .jpeg allowed");
                            return Index(obj.BusinessID, obj.GBusinessName);
                        }
                    }
                    else
                    {
                        sThumbFileName = "";
                        Session.Add("updateE","File size of image is too large. Maximum file size permitted is 5 MB");
                        return Index(obj.BusinessID, obj.GBusinessName);
                    }
                }
                /////
                //BoDashboardModel ob = (BoDashboardModel)Session.Add("objBo"];
                int isinternet = 0;
                if (collection["apply_discount"] == "yes")
                {
                    isinternet = 1;
                }
                else
                {
                    isinternet = 0;
                }
                string empstregth = string.Empty;
                empstregth = obj.EmployeeStrength.ToString();
                //if (collection["EmpOptionId"] == "18")
                //{
                //    empstregth = "1-10";
                //}
                //else if (collection["EmpOptionId"] == "19")
                //{
                //    empstregth = "11-25";
                //}
                //else if (collection["EmpOptionId"] == "20")
                //{
                //    empstregth = "26-75";
                //}
                //else if (collection["EmpOptionId"] == "21")
                //{
                //    empstregth = "76-250";
                //}
                //else if (collection["EmpOptionId"] == "22")
                //{
                //    empstregth = "251-1000";
                //}
                //else if (collection["EmpOptionId"] == "23")
                //{
                //    empstregth = "More than 1000";
                //}
                int cityid = Convert.ToInt32(collection["SelectedCityId"].ToString());
                int stateid = Convert.ToInt32(collection["SelectedStateId"].ToString());
                int areaid = 0;
                if (collection["SelectedAreaId"] != null && collection["SelectedAreaId"] != "")
                {
                     areaid = Convert.ToInt32(collection["SelectedAreaId"].ToString());
                }
                else
                {
                    areaid = 0;
                }
               
                //string statename = Convert.ToString(collection["state"]);

                if (collection["BusinessPhone"] != "")
                {
                    int isPhonevalid = Vconnect.Common.Utility.isValidPhone(Convert.ToString(collection["BusinessPhone"]));
                    if (isPhonevalid == 0)
                    {
                        Session.Add("updateE","Business Phone invalid");
                        return Index(obj.BusinessID, obj.GBusinessName);
                    }
                }

                if (collection["alternatephone"] != "")
                {
                    int isAltPhonevalid = Vconnect.Common.Utility.isValidPhone(Convert.ToString(collection["BusinessPhone"]));
                    if (isAltPhonevalid == 0)
                    {
                        Session.Add("updateE","Alternate phone invalid");
                        return Index(obj.BusinessID, obj.GBusinessName);
                    }
                }
                using (var db = new VconnectDBContext29())
                {
                    // Create a SQL command to execute the sproc
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[prc_AddUp_businessdetails_BODashBoard_new_opt]";
                    cmd.Parameters.Add(new SqlParameter("@businessid", Convert.ToInt32(collection["BusinessID"])));
                    cmd.Parameters.Add(new SqlParameter("@businessname", Convert.ToString(collection["GBusinessName"])));
                    cmd.Parameters.Add(new SqlParameter("@phone", Convert.ToString(collection["BusinessPhone"])));
                    cmd.Parameters.Add(new SqlParameter("@alternatephone", Convert.ToString(collection["alternatephone"])));
                    cmd.Parameters.Add(new SqlParameter("@companylogo", "uploads/CompanyLogo/" + sThumbFileName));
                    cmd.Parameters.Add(new SqlParameter("@shortdesc", Convert.ToString(collection["Businessshortdesc"])));
                    //cmd.Parameters.Add(new SqlParameter("@state", Convert.ToString(collection["StateOptions"])));
                    cmd.Parameters.Add(new SqlParameter("@stateid", stateid));
                    //cmd.Parameters.Add(new SqlParameter("@city", Convert.ToString(collection["CityOptions"])));
                    cmd.Parameters.Add(new SqlParameter("@cityid", cityid));
                    cmd.Parameters.Add(new SqlParameter("@areaid", areaid));
                    //cmd.Parameters.Add(new SqlParameter("@area", Convert.ToString(collection["AreaOptions"])));
                    cmd.Parameters.Add(new SqlParameter("@houseno", Convert.ToString(collection["Houseno"])));
                    cmd.Parameters.Add(new SqlParameter("@address1", Convert.ToString(collection["BusinessAddress"])));
                    cmd.Parameters.Add(new SqlParameter("@landmark", Convert.ToString(collection["BusinessLandmark"])));
                    cmd.Parameters.Add(new SqlParameter("@email", Convert.ToString(collection["BusinessEmail"])));
                    cmd.Parameters.Add(new SqlParameter("@altemail", Convert.ToString(collection["BusinessAlternateEmail"])));
                    cmd.Parameters.Add(new SqlParameter("@peferadvert", mode_adv.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@website", Convert.ToString(collection["BusinessWebsite"])));
                    cmd.Parameters.Add(new SqlParameter("@empstrength", empstregth));
                    cmd.Parameters.Add(new SqlParameter("@isinternet", isinternet.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@createdby", Userid));
                    SqlParameter parameter1 = new SqlParameter("@ERR", SqlDbType.Int);
                    parameter1.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(parameter1);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    try
                    {
                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        cmd.ExecuteNonQuery();
                        int result = Convert.ToInt32(cmd.Parameters["@Err"].Value);
                        if (result == 0 && uplPhoto != null)
                        {
                            Session.Add("update","Business info saved successfully. Logo will reflect on website once admin approves it.");
                            return Index(obj.BusinessID, obj.GBusinessName);
                        }
                        else if (result == 0)
                        {
                            Session.Add("update","Business info saved successfully.");
                            return Index(obj.BusinessID, obj.GBusinessName);
                            //return View("businessProfile", obj);
                        }
                        else
                        {
                            Session.Add("updateE","Please try again.");
                            return Index(obj.BusinessID, obj.BusinessName);

                        }
                    }
                    finally
                    {
                        db.Database.Connection.Close();
                    }
                }
            }
            return Index(obj.BusinessID, obj.GBusinessName);
        }

        //public static SelectList GetSelectEmployee()
        //{
        //    BoDashboardModel obj = new BoDashboardModel();
        //    var list = new[] 
        //        { 
        //        new EmpOption  { EmpOptionId = 18, EmpOptionName = "1-10"  },
        //        new EmpOption  { EmpOptionId = 19, EmpOptionName = "11-25"  },
        //        new EmpOption  { EmpOptionId = 20, EmpOptionName = "26-75"  },
        //        new EmpOption  { EmpOptionId = 21, EmpOptionName = "76-250"  },
        //        new EmpOption  { EmpOptionId = 22, EmpOptionName = "251-1000"  },
        //        new EmpOption  { EmpOptionId = 23, EmpOptionName = ">More than 1000"  }
        //        };


        //    return new SelectList(list, "EmpOptionId", "EmpOptionName", Convert.ToInt32(obj.EmpOptionId));
        //}


        //public static SelectList GetContactTitle()
        //{
        //    BoDashboardModel obj = new BoDashboardModel();
        //    var list = new[]
        //    {
        //        new Vconnect.Models.BoDashboardModel.ContactTitle { ContactTitleID=0, ContactTitleName = "Mr."},
        //        new Vconnect.Models.BoDashboardModel.ContactTitle { ContactTitleID=1, ContactTitleName = "Mrs."},
        //        new Vconnect.Models.BoDashboardModel.ContactTitle { ContactTitleID=2, ContactTitleName = "Ms."},
        //        new Vconnect.Models.BoDashboardModel.ContactTitle { ContactTitleID=3, ContactTitleName = "Dr."},
        //        new Vconnect.Models.BoDashboardModel.ContactTitle { ContactTitleID=4, ContactTitleName = "Other"}
        //    };

        //    return new SelectList(list, "ContactTitleID", "ContactTitleName", Convert.ToInt32(obj._getcontactDetails.Single().title));

        //}
    


        public ActionResult BuisnessProducts()
        {

            //if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
                //Session.Add("Successcontroller",null);
                //Session.Add("Successaction",null);
                BoDashboardModel obj = new BoDashboardModel();
                string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
                if (Request.QueryString["BID"] != null)
                {

                    int chkuserlogin = CheckUserLogin(SV_VCUserContentID, Request.QueryString["BID"].ToString());
                    if (chkuserlogin == 1)
                    {
                        //Session.Add("Successcontroller","BoDashboardWeb");
                        //Session.Add("Successaction","BuisnessImagesList");
                        return RedirectToAction("Login", "Account");
                    }
                    obj.BusinessID = Request.QueryString["BID"].ToString();
                    obj.BusinessName = Request.QueryString["businessname"].ToString();
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                if (ViewBag.BusinessID == null)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.BusinessID = arrayr[0];
                    obj.BusinessName = arrayr[1];
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");
                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                else
                {
                    obj.BusinessID = ViewBag.BusinessID;
                    obj.BusinessName = ViewBag.BusinessName;
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }
                if (obj.DefaultBusinessID == null || obj.DefaultBusinessID.ToString().Length <= 0)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }

                obj.ProfileCompletionvalue = obj.GetProfileCompletion(obj.BusinessID);
                obj._bodashboardbannerlist = BodashboardbannerResultFunc(3);
                ViewBag.bodbanner = obj;
                ViewBag.boroothpath = obj.RootPath;
                obj.Pagename = "BoDashboardWeb/BuisnessProducts";
                obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
                string[] arraydata;
                string results = GetleftMenudata(obj.BusinessID);
                arraydata = results.Split(',');
                obj.membership = arraydata[0];
                obj.ProfileImage = arraydata[2];
                obj.CompanyLogo = arraydata[1];
                obj.Pagename = "BusinessProductsAdd";

                //GetBizDetails_BODashBoard(Convert.ToInt32(obj.BusinessID), obj);
                obj.returncontroller = "BoDashboardWeb";
                obj.returnaction = "BusinessProductsAdd";

                return View("BusinessProductsAdd", obj);

            }
            else
            {

                //Session.Add("Successcontroller","BoDashboardWeb");
                //Session.Add("Successaction","Index");
                //  return RedirectToAction("Login", "Account", new { rturl = "BoDashboardWeb/Index/" });
                return Redirect("/login?rturl=BoDashboardWeb/BusinessProducts");
            }
        }



        public ActionResult BusinessUserActivity(int? filter, string bid, string bname)
        {
            var activity = "";
            BoDashboardModel obj = new BoDashboardModel();
            obj.heading = "User Activity Report For:";
            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
                Session.Add("Successcontroller",null);
                Session.Add("Successaction",null);

                string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
                if (Request.QueryString["BID"] != null)
                {

                    int chkuserlogin = CheckUserLogin(SV_VCUserContentID, Request.QueryString["BID"].ToString());
                    if (chkuserlogin == 1)
                    {
                        Session.Add("Successcontroller","BoDashboardWeb");
                        Session.Add("Successaction","Index");
                        return RedirectToAction("Login", "Account");
                    }
                    obj.BusinessID = Request.QueryString["BID"].ToString();
                    obj.BusinessName = Request.QueryString["bname"].ToString();
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                if (obj.BusinessID == null && bid == null)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.BusinessID = arrayr[0];
                    obj.BusinessName = arrayr[1];
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");
                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                else if (bid != null)
                {
                    obj.BusinessID = bid;
                    obj.BusinessName = bname;
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }
                else
                {

                }

                if (obj.DefaultBusinessID == null || obj.DefaultBusinessID.ToString().Length <= 0)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }

                obj.ProfileCompletionvalue = obj.GetProfileCompletion(obj.BusinessID);
                obj._bodashboardbannerlist = BodashboardbannerResultFunc(3);
                ViewBag.bodbanner = obj;
                ViewBag.boroothpath = obj.RootPath;
                obj.returncontroller = "BoDashboardWeb";
                obj.returnaction = "BusinessUserActivity";
                obj.Pagename = "BoDashboardWeb/Index";
                obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
                string[] arraydata;
                string results = GetleftMenudata(obj.BusinessID);
                arraydata = results.Split(',');
                obj.membership = arraydata[0];
                obj.ProfileImage = arraydata[2];
                obj.CompanyLogo = arraydata[1];
                obj.Pagename = "BuisnessProfile";



                using (var db = new VconnectDBContext29())
                {
                    filter = (filter == null) ? 30 : filter;
                    // Create a SQL command to execute the sproc@      @
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[prc_getuseractivityreportbo_opt]";
                    cmd.Parameters.Add(new SqlParameter("@businessid", obj.BusinessID));
                    cmd.Parameters.Add(new SqlParameter("@filter", filter));
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    try
                    {
                        // Run the sproc
                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        reader.Read();
                        activity = reader[0].ToString() + "/" + reader[1].ToString() + "/" + reader[2].ToString() + "/" + reader[3].ToString() + "/" + reader[4].ToString() + "/" + reader[5].ToString() + "/" + filter;
                    }
                    finally
                    {
                        db.Database.Connection.Close();
                    }
                }
                obj.Subscription = new List<string>();
                obj.Subscription.Add(activity);
                return View("BusinessUserActivity", obj);

            }
            else
            {

                //Session.Add("Successcontroller","BoDashboardWeb");
                //Session.Add("Successaction","Index");
                //  return RedirectToAction("Login", "Account", new { rturl = "BoDashboardWeb/Index/" });
                return Redirect("/login?rturl=BoDashboardWeb/BusinessUserActivity");
            }


        }

        public ActionResult BusinessSubscription()
        {
            List<string> subscription = new List<string>();

            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
                Session.Add("Successcontroller",null);
                Session.Add("Successaction",null);
                BoDashboardModel obj = new BoDashboardModel();
                string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
                if (Request.QueryString["BID"] != null)
                {

                    int chkuserlogin = CheckUserLogin(SV_VCUserContentID, Request.QueryString["BID"].ToString());
                    if (chkuserlogin == 1)
                    {
                        Session.Add("Successcontroller","BoDashboardWeb");
                        Session.Add("Successaction","Index");
                        return RedirectToAction("Login", "Account");
                    }
                    obj.BusinessID = Request.QueryString["BID"].ToString();
                    obj.BusinessName = Request.QueryString["bname"].ToString();
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                if (ViewBag.BusinessID == null)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.BusinessID = arrayr[0];
                    obj.BusinessName = arrayr[1];
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");
                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                else
                {
                    obj.BusinessID = ViewBag.BusinessID;
                    obj.BusinessName = ViewBag.BusinessName;
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }
                if (obj.DefaultBusinessID == null || obj.DefaultBusinessID.ToString().Length <= 0)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }

                obj.ProfileCompletionvalue = obj.GetProfileCompletion(obj.BusinessID);
                obj._bodashboardbannerlist = BodashboardbannerResultFunc(3);
                ViewBag.bodbanner = obj;
                ViewBag.boroothpath = obj.RootPath;
                obj.Pagename = "BoDashboardWeb/Index";
                obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
                string[] arraydata;
                string results = GetleftMenudata(obj.BusinessID);
                arraydata = results.Split(',');
                obj.membership = arraydata[0];
                obj.ProfileImage = arraydata[2];
                obj.CompanyLogo = arraydata[1];
                obj.Pagename = "BuisnessProfile";

                GetBizDetails_BODashBoard(Convert.ToInt32(obj.BusinessID), obj);
                obj.returncontroller = "BoDashboardWeb";
                obj.returnaction = "Index";
                obj.Subscription = new List<string>();
                obj.Subscription = subscription;
                using (var db = new VconnectDBContext29())
                {
                    // Create a SQL command to execute the sproc@      @

                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[prc_getSubscriptionData]";
                    cmd.Parameters.Add(new SqlParameter("@userid", SV_VCUserContentID));
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    try
                    {
                        // Run the sproc
                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        var sno = 0;
                        while (reader.Read())
                        {
                            sno++;
                            subscription.Add(sno + "/" + reader[1].ToString() + "/" + reader[2].ToString() + "/" + reader[3].ToString() + "/" + reader[4].ToString() + "/" + reader[5].ToString());

                        }
                    }
                    finally
                    {
                        db.Database.Connection.Close();
                    }
                }
                return View("BusinessSubscription", obj);

            }
            else
            {

                //Session.Add("Successcontroller","BoDashboardWeb");
                //Session.Add("Successaction","Index");
                //  return RedirectToAction("Login", "Account", new { rturl = "BoDashboardWeb/Index/" });
                return Redirect("/login?rturl=BoDashboardWeb/BusinessSubscription");
            }
        }
        public ActionResult BusinessSubscriptionBanner(int? filter)
        {
            List<string> subscription = new List<string>();
            string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
            using (var db = new VconnectDBContext29())
            {
                filter = (filter == null) ? 30 : filter;
                // Create a SQL command to execute the sproc@      @
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_bannerclickimpressionlog]";
                cmd.Parameters.Add(new SqlParameter("@userid", SV_VCUserContentID));
                cmd.Parameters.Add(new SqlParameter("@filter", filter));
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                try
                {
                    // Run the sproc
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    int sno = 0;
                    while (reader.Read())
                    {
                        sno++;
                        subscription.Add("<tr><td>" + sno + "</td><td>" + reader[1].ToString() + "</td><td>" + reader[7].ToString() + "</td><td>" + reader[2].ToString() + "</td><td>" + reader[3].ToString() + "</td><td>" + reader[4].ToString() + "</td><td>" + reader[5].ToString() + "</td><td>" + reader[6].ToString() + "</td></tr>");

                    }
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            return PartialView("BoBanner", subscription);
        }

        public ActionResult BusinessLeads(int? filter)
        {

            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
                Session.Add("Successcontroller",null);
                Session.Add("Successaction",null);
                BoDashboardModel obj = new BoDashboardModel();
                string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
                if (Request.QueryString["BID"] != null)
                {

                    int chkuserlogin = CheckUserLogin(SV_VCUserContentID, Request.QueryString["BID"].ToString());
                    if (chkuserlogin == 1)
                    {
                        Session.Add("Successcontroller","BoDashboardWeb");
                        Session.Add("Successaction","Index");
                        return RedirectToAction("Login", "Account");
                    }
                    obj.BusinessID = Request.QueryString["BID"].ToString();
                    obj.BusinessName = Request.QueryString["businessname"].ToString();
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                if (ViewBag.BusinessID == null)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.BusinessID = arrayr[0];
                    obj.BusinessName = arrayr[1];
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");
                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                else
                {
                    obj.BusinessID = ViewBag.BusinessID;
                    obj.BusinessName = ViewBag.BusinessName;
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }
                if (obj.DefaultBusinessID == null || obj.DefaultBusinessID.ToString().Length <= 0)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }

                obj.ProfileCompletionvalue = obj.GetProfileCompletion(obj.BusinessID);
                obj._bodashboardbannerlist = BodashboardbannerResultFunc(3);
                ViewBag.bodbanner = obj;
                ViewBag.boroothpath = obj.RootPath;
                obj.Pagename = "BoDashboardWeb/Index";
                obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
                string[] arraydata;
                string results = GetleftMenudata(obj.BusinessID);
                arraydata = results.Split(',');
                obj.membership = arraydata[0];
                obj.ProfileImage = arraydata[2];
                obj.CompanyLogo = arraydata[1];
                obj.Pagename = "BuisnessProfile";

                GetBizDetails_BODashBoard(Convert.ToInt32(obj.BusinessID), obj);
                obj.returncontroller = "BoDashboardWeb";
                obj.returnaction = "Index";
                obj.Subscription = new List<string>();

                if (filter == null)
                {
                    filter = 2;
                }
                ViewBag.filter = filter;
                List<string> subscription = new List<string>();

                using (var db = new VconnectDBContext29())
                {
                    // Create a SQL command to execute the sproc@      @
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[prc_leadreportbo]";
                    cmd.Parameters.Add(new SqlParameter("@type", filter));
                    cmd.Parameters.Add(new SqlParameter("@userid", SV_VCUserContentID));
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    try
                    {
                        // Run the sproc
                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        var columns = "";
                        int count = reader.FieldCount;
                        columns = "<tr style='color: White; background-color: #E17100;'>";
                        var sno = 0;
                        if (filter == 1 || filter == 2)
                        {

                            columns = columns + " <th scope='col'> Sno </th><th scope='col'> Date </th> <th scope='col'> User Requirement </th> <th scope='col'> User Details </th> </tr> ";
                            subscription.Add(columns);
                            while (reader.Read())
                            {
                                sno++;
                                var row = " <tr> <td>" + sno + "</td><td>" + reader["receivedate"] + "</td><td>" + reader["requirement"] + "</td><td>" + reader["sendername"] + " </br>" + reader["senderemailid"] + " </br>" + reader["sendercontactnumber"] + "</td></tr>";
                                subscription.Add(row);
                            }
                        }
                        else
                        {
                            columns = columns + " <th scope='col'> Sno </th><th scope='col'> Date </th> <th scope='col'> No.of Leads Send </th></tr>  ";
                            subscription.Add(columns);
                            while (reader.Read())
                            {
                                sno++;
                                var row = " <tr> <td>" + sno + "</td><td>" + reader["receivedate"] + "</td><td>" + reader["leads"] + "</td></tr>";
                                subscription.Add(row);
                            }
                        }



                    }
                    finally
                    {
                        db.Database.Connection.Close();
                    }
                }
                obj.Subscription = subscription;
                return View("BusinessLeads", obj);

            }
            else
            {

                //Session.Add("Successcontroller","BoDashboardWeb");
                //Session.Add("Successaction","Index");
                //  return RedirectToAction("Login", "Account", new { rturl = "BoDashboardWeb/Index/" });
                return Redirect("/login?rturl=BoDashboardWeb/BusinessLeads");
            }
        }
        public ActionResult Download()
        {
            var db = new VconnectDBContext29();
            string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
            var cmd = db.Database.Connection.CreateCommand();
            cmd.CommandText = "[dbo].[prc_leadreportbo_opt]";
            cmd.Parameters.Add(new SqlParameter("@type", 5));
            cmd.Parameters.Add(new SqlParameter("@userid", SV_VCUserContentID));
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
            connection.Open();
            GridView gv = new GridView();
            gv.DataSource = cmd.ExecuteReader();
            gv.DataBind();
            if (gv != null)
            {
                return new DownloadFileActionResult(gv, "leads_data.xls");
            }
            else
            {
                return new JavaScriptResult();
            }
        }

        public ActionResult DownloadExcel()
        {
            var db = new VconnectDBContext29();
            string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
            var cmd = db.Database.Connection.CreateCommand();
            cmd.CommandText = "[dbo].[prc_getSubscriptionData]";
            cmd.Parameters.Add(new SqlParameter("@userid", SV_VCUserContentID));
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
            connection.Open();
            GridView gv = new GridView();
            gv.DataSource = cmd.ExecuteReader();
            gv.DataBind();
            if (gv != null)
            {
                return new DownloadFileActionResult(gv, "subscription_data.xls");
            }
            else
            {
                return new JavaScriptResult();
            }
        }


        public ActionResult BuisnessImagesList(string bid, string bname, string filter)
        {

            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
                //Session.Add("Successcontroller",null);
                //Session.Add("Successaction",null);
                BoDashboardModel obj = new BoDashboardModel();
                string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
                if (Request.QueryString["BID"] != null)
                {

                    int chkuserlogin = CheckUserLogin(SV_VCUserContentID, Request.QueryString["BID"].ToString());
                    if (chkuserlogin == 1)
                    {
                        //Session.Add("Successcontroller","BoDashboardWeb");
                        //Session.Add("Successaction","BuisnessImagesList");
                        return RedirectToAction("Login", "Account");
                    }
                    obj.BusinessID = Request.QueryString["BID"].ToString();
                    obj.BusinessName = Request.QueryString["bname"].ToString();
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                if (obj.BusinessID == null && bid == null)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.BusinessID = arrayr[0];
                    obj.BusinessName = arrayr[1];
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");
                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                else if (bid != null)
                {
                    obj.BusinessID = bid;
                    obj.BusinessName = bname;
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }
                else
                {

                }
                if (obj.DefaultBusinessID == null || obj.DefaultBusinessID.ToString().Length <= 0)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }

                obj.ProfileCompletionvalue = obj.GetProfileCompletion(obj.BusinessID);
                obj._bodashboardbannerlist = BodashboardbannerResultFunc(3);
                ViewBag.bodbanner = obj;
                ViewBag.boroothpath = obj.RootPath;
                obj.Pagename = "BoDashboardWeb/BuisnessImagesList";
                obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
                string[] arraydata;
                string results = GetleftMenudata(obj.BusinessID);
                arraydata = results.Split(',');
                obj.membership = arraydata[0];
                obj.ProfileImage = arraydata[2];
                obj.CompanyLogo = arraydata[1];
                obj.Pagename = "BuisnessImagesList";

                //GetBizDetails_BODashBoard(Convert.ToInt32(obj.BusinessID), obj);
                obj.returncontroller = "BoDashboardWeb";
                obj.returnaction = "BuisnessImagesList";
                int typ = 0;
                if(filter==null)
                {
                    filter = "0";
                }
                if(filter=="0")
                {
                    typ = 1;
                }
                else if (filter == "211")
                {
                    typ = 2;
                }
                else 
                {
                    typ = 3;
                }
                obj.filter = filter;
                obj._ListBusinessProfileImages = GetBusinessProfileImages(Int32.Parse(obj.BusinessID), typ, Convert.ToInt16(filter));
                if (obj._ListBusinessProfileImages.ListBusinessProfileImages.Count <= 0)
                {
                    TempData["alertMsg"] = null;
                    TempData["alertMsgE"] = null;
                    TempData["alertMsgE"] = "There is no image for this business please upload image from here";
                    return AddBusinessImages(obj.BusinessID, obj.BusinessName,obj); 
                }
                else
                {
                   // Session.Add("update"] = "Image Delete Suscessfully";
                return View("BuisnessImagesList", obj);
                }

            }
            else
            {

                //Session.Add("Successcontroller","BoDashboardWeb");
                //Session.Add("Successaction","Index");
                //  return RedirectToAction("Login", "Account", new { rturl = "BoDashboardWeb/Index/" });
                return Redirect("/login?rturl=BoDashboardWeb/BuisnessImagesList");
            }

        }

        private BoDashboardModel.ResultTableBusinessProfileImages GetBusinessProfileImages(int businessId, int typeId, int formatId)
        {
            List<Vconnect.Models.BoDashboardModel.BusinessProfileImages> listBusinessProfileImages = new List<Vconnect.Models.BoDashboardModel.BusinessProfileImages>();
            List<Vconnect.Models.BoDashboardModel.BusinessProfileImagesTotalRecords> listBusinessProfileImagesTotalRecords = new List<Vconnect.Models.BoDashboardModel.BusinessProfileImagesTotalRecords>();
            Vconnect.Models.BoDashboardModel.ResultTableBusinessProfileImages tableResultTableBusinessProfileImages = new Vconnect.Models.BoDashboardModel.ResultTableBusinessProfileImages();


            using (var db = new VconnectDBContext29())
            {

                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_get_BO_images_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@businessid", businessId));
                    cmd.Parameters.Add(new SqlParameter("@type", typeId));
                    cmd.Parameters.Add(new SqlParameter("@formatid", formatId));

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();
                    listBusinessProfileImages = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.BoDashboardModel.BusinessProfileImages>(reader).ToList();
                    reader.NextResult();
                    listBusinessProfileImagesTotalRecords = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.BoDashboardModel.BusinessProfileImagesTotalRecords>(reader).ToList();

                    tableResultTableBusinessProfileImages.ListBusinessProfileImages = listBusinessProfileImages;
                    tableResultTableBusinessProfileImages.ListBusinessProfileImagesTotalRecords = listBusinessProfileImagesTotalRecords;


                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                }
                finally { db.Database.Connection.Close(); }
            }

            return tableResultTableBusinessProfileImages;
        }

        public ActionResult AjaxCallDeleteBusinessProfileImages(string contentId, string iType, string busId,string bname)
        {
            int type=1;
            if (@iType =="Images")
            {
                type = 1;
            }
            else
            {
                type = 0;
            }

            using (var db = new VconnectDBContext29())
            {

                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_update_buisnessimagesattachments_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@type", type));
                    cmd.Parameters.Add(new SqlParameter("@businessid", busId));
                    cmd.Parameters.Add(new SqlParameter("@contentid", contentId));

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    cmd.ExecuteNonQuery();

                    Session.Add("update","Image Deleted Sucessfully");
                }
                catch (Exception ex)
                {
                    Session.Add("updateE", "Image not Deleted!!");
                }
                finally { db.Database.Connection.Close(); }
            }
           
            return RedirectToActionPermanent("BuisnessImagesList", new { bid = busId,bname =bname,filter="0" });
        }

        public ActionResult AjaxCallSetBusinessProfileImage(string contentId, string iType, string busId)
        {
            using (var db = new VconnectDBContext29())
            {

                try
                {
                    UserSession obj = new UserSession();
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_addprofilephotos_BO_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object

                    cmd.Parameters.Add(new SqlParameter("@businessid", busId));
                    cmd.Parameters.Add(new SqlParameter("@userid", obj.SV_VCUserContentID.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@contentid", contentId));
                    cmd.Parameters.Add(new SqlParameter("@businessname", ""));
                    cmd.Parameters.Add(new SqlParameter("@photo", ""));
                    cmd.Parameters.Add(new SqlParameter("@type", iType));
                    cmd.Parameters.Add(new SqlParameter("@pcontentid", contentId));
                    var err = new SqlParameter("@Err", SqlDbType.Int);
                    err.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(err);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    cmd.ExecuteNonQuery();


                }
                catch (Exception ex)
                {

                }
                finally { db.Database.Connection.Close(); }
            }
            return RedirectToActionPermanent("BuisnessImagesList");
        }

        public ActionResult BuisnessImagesListBO(string bid, string bname, string filter)
        {

            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
                //Session.Add("Successcontroller",null);
                //Session.Add("Successaction",null);
                BoDashboardModel obj = new BoDashboardModel();
                string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
                if (Request.QueryString["BID"] != null)
                {

                    int chkuserlogin = CheckUserLogin(SV_VCUserContentID, Request.QueryString["BID"].ToString());
                    if (chkuserlogin == 1)
                    {
                       // Session.Add("Successcontroller","BoDashboardWeb");
                        //Session.Add("Successaction","BuisnessImagesListBO");
                        return RedirectToAction("Login", "Account");
                    }
                    obj.BusinessID = Request.QueryString["BID"].ToString();
                    obj.BusinessName = Request.QueryString["bname"].ToString();
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                if (obj.BusinessID == null && bid == null)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.BusinessID = arrayr[0];
                    obj.BusinessName = arrayr[1];
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");
                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                else if (bid != null)
                {
                    obj.BusinessID = bid;
                    obj.BusinessName = bname;
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }
                else
                {

                }
                if (obj.DefaultBusinessID == null || obj.DefaultBusinessID.ToString().Length <= 0)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }

                obj.ProfileCompletionvalue = obj.GetProfileCompletion(obj.BusinessID);
                obj._bodashboardbannerlist = BodashboardbannerResultFunc(3);
                ViewBag.bodbanner = obj;
                ViewBag.boroothpath = obj.RootPath;
                obj.Pagename = "BoDashboardWeb/Index";
                obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
                string[] arraydata;
                string results = GetleftMenudata(obj.BusinessID);
                arraydata = results.Split(',');
                obj.membership = arraydata[0];
                obj.ProfileImage = arraydata[2];
                obj.CompanyLogo = arraydata[1];
                obj.Pagename = "BuisnessImagesList";



                obj.returncontroller = "BoDashboardWeb";
                obj.returnaction = "BuisnessImagesListBO";
                int typ = 0;
                if (filter == null)
                {
                    filter = "0";
                }
                if (filter == "0" || filter == "211")
                {
                    typ = 1;
                }
                else if (filter == "211")
                {
                    typ = 2;
                }
                else
                {
                    typ = 3;
                }
                obj.filter = filter;

                obj._ListBusinessProfileImages = GetBusinessProfileImagesByBO(typ, Int32.Parse(obj.BusinessID), Convert.ToInt16 (filter), Int32.Parse(objUS.SV_VCUserContentID.ToString()));
                if (obj._ListBusinessProfileImages.ListBusinessProfileImagesByBO.Count <=0)
                {
                    TempData["alertMsg"] = null;
                    TempData["alertMsgE"] = null;
                    TempData["alertMsgE"] = "No images added for this business please add images ";
                    return AddBusinessImages(obj.BusinessID, obj.BusinessName,obj);
                }
                else
                {
                    
                    return View("BusinessImagesAddedbyBO", obj);
                }
            }
            else
            {

               // Session.Add("Successcontroller","BoDashboardWeb");
                //Session.Add("Successaction","BuisnessImagesListBO");
                return RedirectToAction("Login", "Account");
            }
        }

        private BoDashboardModel.ResultTableBusinessProfileImages GetBusinessProfileImagesByBO(int typeId, int businessId, int formatId, int userId)
        {
            List<Vconnect.Models.BoDashboardModel.BusinessProfileImagesByBO> listBusinessProfileImagesByBO = new List<Vconnect.Models.BoDashboardModel.BusinessProfileImagesByBO>();
            List<Vconnect.Models.BoDashboardModel.BusinessProfileImagesTotalRecords> listBusinessProfileImagesTotalRecords = new List<Vconnect.Models.BoDashboardModel.BusinessProfileImagesTotalRecords>();
            Vconnect.Models.BoDashboardModel.ResultTableBusinessProfileImages tableResultTableBusinessProfileImages = new Vconnect.Models.BoDashboardModel.ResultTableBusinessProfileImages();


            using (var db = new VconnectDBContext29())
            {

                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_get_BO_imagesbyBO_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@type", typeId));
                    cmd.Parameters.Add(new SqlParameter("@businessid", businessId));
                    cmd.Parameters.Add(new SqlParameter("@formatid", formatId));
                    cmd.Parameters.Add(new SqlParameter("@userid", userId));

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();
                    listBusinessProfileImagesByBO = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.BoDashboardModel.BusinessProfileImagesByBO>(reader).ToList();
                    reader.NextResult();
                    listBusinessProfileImagesTotalRecords = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.BoDashboardModel.BusinessProfileImagesTotalRecords>(reader).ToList();

                    tableResultTableBusinessProfileImages.ListBusinessProfileImagesByBO = listBusinessProfileImagesByBO;
                    tableResultTableBusinessProfileImages.ListBusinessProfileImagesTotalRecords = listBusinessProfileImagesTotalRecords;


                }
                catch (Exception ex)
                {

                }
                finally { db.Database.Connection.Close(); }
            }

            return tableResultTableBusinessProfileImages;
        }

        [HttpGet]
        public ActionResult AddBusinessImages(string bid, string bname,BoDashboardModel obj)
        {
            //BoDashboardModel obj = new BoDashboardModel();
            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
               // Session.Add("Successcontroller",null);
                //Session.Add("Successaction",null);
                
                string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
                if (Request.QueryString["BID"] != null)
                {

                    int chkuserlogin = CheckUserLogin(SV_VCUserContentID, Request.QueryString["BID"].ToString());
                    if (chkuserlogin == 1)
                    {
                      //  Session.Add("Successcontroller","BoDashboardWeb");
                       // Session.Add("Successaction","AddBusinessImages");
                        return RedirectToAction("Login", "Account");
                    }
                    obj.BusinessID = Request.QueryString["BID"].ToString();
                    obj.BusinessName = Request.QueryString["bname"].ToString();
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                if (obj.BusinessID == null && bid == null)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.BusinessID = arrayr[0];
                    obj.BusinessName = arrayr[1];
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");
                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                else if (bid != null)
                {
                    obj.BusinessID = bid;
                    obj.BusinessName = bname;
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }
                else
                {

                }
                if (obj.DefaultBusinessID == null || obj.DefaultBusinessID.ToString().Length <= 0)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }

                if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59" && obj.BusinessID != null && obj.BusinessID.ToString().Trim() != "")
                {
                    obj.ProfileCompletionvalue = obj.GetProfileCompletion(obj.BusinessID);
                }
                else
                {
                    return Redirect("mybusiness");
                }

                obj._bodashboardbannerlist = BodashboardbannerResultFunc(3);
                ViewBag.bodbanner = obj;
                ViewBag.boroothpath = obj.RootPath;
                obj.Pagename = "BoDashboardWeb/AddBusinessImages";
                obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
                string[] arraydata;
                string results = GetleftMenudata(obj.BusinessID);
                arraydata = results.Split(',');
                obj.membership = arraydata[0];
                obj.ProfileImage = arraydata[2];
                obj.CompanyLogo = arraydata[1];
                obj.Pagename = "BusinessImagesAdd";


                obj.returncontroller = "BoDashboardWeb";
                obj.returnaction = "AddBusinessImages";

                //Session.Add("BoDashboardWeb",obj);

                return View("BusinessImageAdd", obj);

            }
            else
            {

                //Session.Add("Successcontroller","BoDashboardWeb");
                //Session.Add("Successaction","Index");
                //  return RedirectToAction("Login", "Account", new { rturl = "BoDashboardWeb/Index/" });
                return Redirect("/login?rturl=BoDashboardWeb/AddBusinessImages");
            }

        }

        [HttpPost]
        public ActionResult AddBusinessImages(FormCollection collection, HttpPostedFileBase uplPhoto, BoDashboardModel objM)
        {
            string bid = collection["bid"].ToString();
            string banme = collection["bnmae"].ToString(); ;
            string UploadFolder = string.Empty;
            string DdlimagetypeAddText = collection["hid_DdlimagetypeAddText"].ToString();
            string DdlimagetypeAddValue = collection["DdlimagetypeAdd2"].ToString();
            string txtimagecaption = collection["txtimagecaption"].ToString();
            bool FileSaved = false;
            //BoDashboardModel objM =new BoDashboardModel();
            //objM = (BoDashboardModel)Session["BoDashboardWeb"];
            if (objM == null)
            {
                try
                {
                    getBusinessdata(bid, banme, objM);
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);

                    return Redirect("/");
                }
            }
            else
            {
                if (objM.BusinessID == null)
                {
                    getBusinessdata(bid, banme, objM);
                }
            }
            string sThumbFileName = string.Empty; string sLargeFileName = string.Empty;
            try
            {
            if (uplPhoto != null)

            {
                if (txtimagecaption.Trim().Length <= 0)
                {
                    TempData["alertMsg"] = null;
                    TempData["alertMsgE"] = "Please enter Photo Caption";
                    return View("BusinessImageAdd", objM);

                }
                string extensionadd = System.IO.Path.GetExtension(uplPhoto.FileName).ToLower();
               

                int bSize = uplPhoto.ContentLength;
                // Allow only files less than 10240 bytes (approximately 10 KB) to be uploaded.
                if (bSize <= 5242880)
                {
                    Random obj = new Random();
                    int randomno = obj.Next(0, 100000000);
                    if (extensionadd == ".gif" || extensionadd == ".jpg" || extensionadd == ".png" || extensionadd == ".jpeg")
                    {

                        if (DdlimagetypeAddText == "Images")
                        {
                            UploadFolder = "vcsites/vcimages/resource/uploads/photogallery/";
                        }
                        else
                        {
                            UploadFolder = "vcsites/vcimages/resource/uploads/attachments/";
                        }

                        if ( objM == null || objM.BusinessName == null || objM.BusinessName.ToString().Trim() == "")
                        {
                            if (string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() == "0" && objUS.SV_VCLoginID.ToString() == "")
                            {
                                return Redirect("/login?rturl=BoDashboardWeb/AddBusinessImages");
                            }
                            else if (banme == "")
                            {
                                return Redirect("BoDashboardWeb/BuisnessImagesList");
                            }
                            else
                            {
                                objM.BusinessName = banme;
                            }
                        }
                        sThumbFileName = objM.BusinessName.ToString().Trim().Replace(" ", "-").Replace("&", "and").Replace("'", "`") + "_thumb_" + randomno + extensionadd;
                        sLargeFileName = objM.BusinessName.ToString().Trim().ToLower().Replace(" ", "-").Replace("&", "and").Replace("'", "`") + "_large_" + randomno + extensionadd;
                        //Vconnect.Common.Utility.ImageHandler newImg = new Vconnect.Common.Utility.ImageHandler(uplPhoto, UploadFolder.ToLower(), sThumbFileName.ToLower(), sLargeFileName.ToLower());
                        //bool uploadLrg = newImg.UploadImage();
                        //string newFilename = newImg.NewFilename;
                        //sThumbFileName = newFilename;
                        FileSaved = true;
                        if (FileSaved)
                        {
                            if (DdlimagetypeAddText == "Images")
                            {
                                sThumbFileName = sThumbFileName.ToString().Trim();
                                //Vconnect.Common.Utility.CopyImage.Sync(webconfigSourceImagesRootPath + "uploads", webconfigDestinationImagesRootPath + "uploads");
                                //Vconnect.Common.Utility.CopyImage.Sync(webconfigSourceImagesRootPath + "photogallery", webconfigDestinationImagesRootPath + "photogallery");
                                ImageConverter converter = new ImageConverter();
                                System.Drawing.Image uploadImage = System.Drawing.Image.FromStream(uplPhoto.InputStream);
                                var temp = (byte[])converter.ConvertTo(uploadImage, typeof(byte[]));
                                Utility.ByteCloudUploadImages(temp, sThumbFileName, "vcsites/vcimages/resource/uploads/photogallery/");
                            }
                            else
                            {
                                sThumbFileName = sThumbFileName.ToString().Trim();
                                //Vconnect.Common.Utility.CopyImage.Sync(webconfigSourceImagesRootPath + "attachments", webconfigDestinationImagesRootPath + "attachments");
                                ImageConverter converter = new ImageConverter();
                                System.Drawing.Image uploadImage = System.Drawing.Image.FromStream(uplPhoto.InputStream);
                                var temp = (byte[])converter.ConvertTo(uploadImage, typeof(byte[]));
                                Utility.ByteCloudUploadImages(temp, sThumbFileName, "vcsites/vcimages/resource/uploads/attachments/");
                            }
                            int result;
                            if (DdlimagetypeAddText == "Images")
                            {
                                result = AddBusinessPhotosByBO(Convert.ToInt32(objM.BusinessID), 0, Convert.ToInt32(objUS.SV_VCUserContentID.ToString()), objM.BusinessName, "uploads/photogallery/" + sThumbFileName, Convert.ToString(txtimagecaption.Trim()));
                            }
                            else
                            {
                                result = AddBusinessAttachmentsBO(Convert.ToInt32(objM.BusinessID), 0, Convert.ToInt32(objUS.SV_VCUserContentID.ToString()), objM.BusinessName, "uploads/attachments/" + sThumbFileName.ToString().Trim(), Convert.ToString(txtimagecaption.Trim()), Convert.ToInt32(DdlimagetypeAddValue), DdlimagetypeAddText.Trim());
                            }
                            TempData["alertMsgE"] = null;
                            TempData["alertMsg"] = null;
                            string array = string.Empty;
                            if (result == 0)
                            {

                                array = objM.BusinessID;
                                TempData["alertMsgE"] = null;
                                TempData["alertMsg"] = "Photo uploaded successfully. It will display on website once admin approves it.";
                            }
                        }
                        else
                        {
                            sThumbFileName = "";
                            TempData["alertMsg"] = null;
                            TempData["alertMsgE"] = "Some Error has occured please upload image again";
                        }
                    }
                    else
                    {
                        TempData["alertMsg"] = null;
                        TempData["alertMsgE"] = "only .gif, .jpg, .png, .jpeg allowed";
                    }
                }
                else
                {
                    TempData["alertMsg"] = null;
                    TempData["alertMsgE"] = "File size of image is too large. Maximum file size permitted is 5 MB";
                }
            }
            else
            {
                
                TempData["alertMsg"] = null;
                TempData["alertMsgE"] = "Please select the photo to upload.";
                //return View("BusinessImageAdd", objM);

            }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);

                return Redirect("/");
            }
            finally
            {

            }

            return View("BusinessImageAdd", objM);

        }

        public int AddBusinessPhotosByBO(int businessid, int contentid, int userid, string businessname, string photos, string photocaption)
        {

            using (var db = new VconnectDBContext29())
            {

                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_add_PhotosToBusiness_BOProfile_add_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                    cmd.Parameters.Add(new SqlParameter("@userid", userid));
                    cmd.Parameters.Add(new SqlParameter("@contentid", contentid));
                    cmd.Parameters.Add(new SqlParameter("@businessname", businessname));
                    cmd.Parameters.Add(new SqlParameter("@photo", photos));
                    cmd.Parameters.Add(new SqlParameter("@photocaption", photocaption));
                    cmd.Parameters.Add(new SqlParameter("@source", "WebBODashboard"));

                    var err = new SqlParameter("@Err", SqlDbType.Int);
                    err.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(err);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteNonQuery();

                    return (int)cmd.Parameters["@Err"].Value; ;

                }
                catch (Exception ex)
                {
                    return 1;
                }
                finally { db.Database.Connection.Close(); }
            }

        }

        public int AddBusinessAttachmentsBO(int businessid, int contentid, int userid, string businessname, string attachments, string photocaption, int formatid, string format)
        {

            using (var db = new VconnectDBContext29())
            {

                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_add_Businessattachments_BO_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                    cmd.Parameters.Add(new SqlParameter("@userid", userid));
                    cmd.Parameters.Add(new SqlParameter("@contentid", contentid));
                    cmd.Parameters.Add(new SqlParameter("@businessname", businessname));
                    cmd.Parameters.Add(new SqlParameter("@attachments", attachments));
                    cmd.Parameters.Add(new SqlParameter("@photocaption", photocaption));
                    cmd.Parameters.Add(new SqlParameter("formatid", formatid));
                    cmd.Parameters.Add(new SqlParameter("@format", format));
                    cmd.Parameters.Add(new SqlParameter("@source", "WebBODashboard"));
                    var err = new SqlParameter("@Err", SqlDbType.Int);
                    err.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(err);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteNonQuery();

                    return (int)cmd.Parameters["@Err"].Value; ;

                }
                catch (Exception ex)
                {
                    return 0;
                }
                finally { db.Database.Connection.Close(); }
            }

        }

        [HttpGet]
        public ActionResult BusinessReviews(string bid, string bname)
        {

            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
               // Session.Add("Successcontroller",null);
                //Session.Add("Successaction",null);
                BoDashboardModel obj = new BoDashboardModel();
                obj.heading = "Business Reviews For:";
                string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
                if (Request.QueryString["ID"] != null)
                {

                    int chkuserlogin = CheckUserLogin(SV_VCUserContentID, Request.QueryString["ID"].ToString());
                    if (chkuserlogin == 1)
                    {
                       // Session.Add("Successcontroller","BoDashboardWeb");
                      //  Session.Add("Successaction","BusinessReviews");
                        return RedirectToAction("Login", "Account");
                    }
                    obj.BusinessID = Request.QueryString["ID"].ToString();
                    obj.BusinessName = Request.QueryString["bname"].ToString();
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                if (obj.BusinessID == null && bid == null)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.BusinessID = arrayr[0];
                    obj.BusinessName = arrayr[1];
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");
                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                else if (bid != null)
                {
                    obj.BusinessID = bid;
                    obj.BusinessName = bname;
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }
                else
                {

                }
                if (obj.DefaultBusinessID == null || obj.DefaultBusinessID.ToString().Length <= 0)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }

                obj.ProfileCompletionvalue = obj.GetProfileCompletion(obj.BusinessID);
                obj._bodashboardbannerlist = BodashboardbannerResultFunc(3);
                ViewBag.bodbanner = obj;
                ViewBag.boroothpath = obj.RootPath;
                obj.Pagename = "BoDashboardWeb/BusinessReviews";
                obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
                string[] arraydata;
                string results = GetleftMenudata(obj.BusinessID);
                arraydata = results.Split(',');
                obj.membership = arraydata[0];
                obj.ProfileImage = arraydata[2];
                obj.CompanyLogo = arraydata[1];
                obj.Pagename = "BusinessReviews";

                
                obj.returncontroller = "BoDashboardWeb";
                obj.returnaction = "BusinessReviews";

              //  Session.Add("BoDashboardWeb",obj);
                GetReviews(Int32.Parse(obj.BusinessID),obj);
                TempData["Reviews"] = TempData["Reviews"];
                return View("BusinessReviews", obj);

            }
            else
            {

                //Session.Add("Successcontroller","BoDashboardWeb");
                //Session.Add("Successaction","Index");
                //  return RedirectToAction("Login", "Account", new { rturl = "BoDashboardWeb/Index/" });
                return Redirect("/login?rturl=BoDashboardWeb/BusinessReviews");
            }

        }

        protected void GetReviews(int businessid,BoDashboardModel obj)
        {
            //BoDashboardModel obj = (BoDashboardModel)Session["BoDashboardWeb"];

            string reportlink = string.Empty;
            //hid_PageNum.Value = "1";
            //hid_ReviewRecordsEnded.Value = "0";
            //divcompanylogo.InnerHtml = "";
            //dvLoadMessage.InnerHtml = "";

            string hid_PageNum = "1";
            string hid_ReviewRecordsEnded = "0";
            int pageNum = (Convert.ToInt16(hid_PageNum) == 0) ? (1) : (Convert.ToInt16(hid_PageNum));


            int rowsPerPage = 5;

            DataSet ds = GetBusinessReviewsBo(businessid, pageNum, rowsPerPage, 0);

            if (ds.Tables[2].Rows[0]["membership"].ToString() == "1")
            {
                obj.membership = "1";

                // lisubs.Visible = false;
            }
            else
            {
                obj.membership = "0";

                //lisubs.Visible = true;
            }


            if (ds.Tables[2].Rows[0]["companylogo"].ToString() != null && ds.Tables[2].Rows[0]["companylogo"].ToString() != "")
            {
                if (ds.Tables[2].Rows[0]["companylogo"].ToString() == null || ds.Tables[2].Rows[0]["companylogo"].ToString() == "")
                {

                    // divcompanylogo.Visible = false;
                }
                else
                {
                    //   Imgcompanylogo.ImageUrl = webconfigImagesRootPath + ds.Tables[2].Rows[0]["companylogo"].ToString();
                }
            }

            else
            {
                //divcompanylogo.Visible = false;
            }
            //Profile image

            if (ds.Tables[3].Rows.Count > 0)
            {
                // imgCompanyProfile.ImageUrl = webconfigImagesRootPath + ds.Tables[3].Rows[0]["photo"].ToString();
                //divcompanylogo.Visible = false;

            }

            else
            {

                // imgCompanyProfile.ImageUrl = webconfigResourceRootPath + "images/noImageAvailable.jpg";

            }

            if (ds.Tables[7].Rows.Count > 0)
            {
                //imgCompanyProfile.ImageUrl = webconfigImagesRootPath + ds.Tables[7].Rows[0]["attachments"].ToString();
                //divcompanylogo.Visible = false;

            }
            //if (imgCompanyProfile.ImageUrl.Contains(("noImageAvailable.jpg")) && !Imgcompanylogo.ImageUrl.Contains(("noImageAvailable.jpg")))
            //{
            //imgCompanyProfile.Visible = false;
            //}

            else
                //    dvreviewchart.Visible = true;

                if (ds.Tables[6].Rows.Count > 0)
                {

                    DataTable dtreview = ds.Tables[4];
                    {


                        foreach (DataRow drNew in dtreview.Rows)
                        {


                            double j = (Convert.ToDouble(drNew["ratingscount"]) / Convert.ToDouble(ds.Tables[5].Rows[0]["totalratings"])) * 100;
                            j = Math.Round(j, 2);

                            if (Convert.ToString(drNew["rating"]) == "5")
                            {
                                obj.tdbar1 = "";
                                obj.tdbar1 = "<span  class= 'bar bar5' " + "style='height:20px;width:" + j + "px" + "'>" + "</span>" + "<span  class= 'value-rating'>" + j + "%" + "</span>";
                            }
                            if (Convert.ToString(drNew["rating"]) == "4")
                            {
                                obj.tdbar2 = "";
                                obj.tdbar2 = "<span  class= 'bar bar4' " + "style='height:20px;width:" + j + "px" + "'>" + "</span>" + "<span  class= 'value-rating'>" + j + "%" + "</span>";
                            }
                            if (Convert.ToString(drNew["rating"]) == "3")
                            {
                                obj.tdbar3 = "";
                                obj.tdbar3 = "<span  class= 'bar bar3' " + "style='height:20px;width:" + j + "px" + "'>" + "</span>" + "<span  class= 'value-rating'>" + j + "%" + "</span>";
                            }
                            if (Convert.ToString(drNew["rating"]) == "2")
                            {
                                obj.tdbar4 = "";
                                obj.tdbar4 = "<span  class= 'bar bar2' " + "style='height:20px;width:" + j + "px" + "'>" + "</span>" + "<span  class= 'value-rating'>" + j + "%" + "</span>";
                            }
                            if (Convert.ToString(drNew["rating"]) == "1")
                            {
                                obj.tdbar5 = "";
                                obj.tdbar5 = "<span  class= 'bar bar1' " + "style='height:20px;width:" + j + "px" + "'>" + "</span>" + "<span  class= 'value-rating'>" + j + "%" + "</span>";
                            }


                        }
                    }

                }
                else
                {

                }



            //Rating Count & Rating 
            if (ds.Tables[6].Rows.Count > 0)
            {
                obj.Rate = ds.Tables[6].Rows[0]["avgrating"].ToString();

                //if (Rate == "0")
                //{
                //    dvreviewchart.Visible = false;
                //}

            }



            else
            {

                //Rate = "";
                //dvreviewchart.Visible = false;

            }

            if (ds.Tables[5].Rows.Count > 0)
            {

                obj.TotalRate = ds.Tables[5].Rows[0]["totalratings"].ToString();
                //if (tRate == "0")
                //{
                //    dvreviewchart.Visible = false;
                //}

            }
            else
            {
                //tRate = "";
                //dvreviewchart.Visible = false;
            }

            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {

                //seemorevideo.Visible = true;
                //dvpaneladdedbyme.Visible = true;
                //TotalRecords = (Convert.ToInt32(ds.Tables[1].Rows[0]["TotalRecords"]));
                //dvreviewcount.Visible = true;
                //dvreviewcount.InnerHtml = "Total Reviews : " + TotalRecords;
                System.Text.StringBuilder objRightData = new System.Text.StringBuilder();
                obj.totalRv = "Total Reviews : " + (Convert.ToInt32(ds.Tables[1].Rows[0]["TotalRecords"]));
                DataTable dtreview = ds.Tables[0];
                {
                    int i = 0;
                    foreach (DataRow drNew in dtreview.Rows)
                    {
                        objRightData.Append("<div class" + "=review-list-holder>");
                        if (drNew["userphoto"].ToString().Contains("http"))
                        {
                            objRightData.Append("<div class" + "=review-users-holder>" + "<img src=" + drNew["userphoto"] + "><br />");
                        }
                        else
                        {
                            objRightData.Append("<div class" + "=review-users-holder>" + "<img src='" + webconfigImagesRootPath + drNew["userphoto"].ToString() + "'><br />");
                        }

                        //if (Convert.ToString(drNew["username"]).Length > 12)
                        //{
                        objRightData.Append("<div class" + "=review-user-link> " + Convert.ToString(drNew["username"]) + "</div>");
                        //}
                        //else
                        //{
                        //    objRightData.Append("<div class" + "=review-user-link> " + "<a href=" + "#" + ">" + Convert.ToString(drNew["username"]) + "</a></div>");
                        //}
                        objRightData.Append("<div class" + "=clear>" + "</div>");
                        objRightData.Append("</div>");
                        if (Convert.ToString(drNew["source"]) == "WAP")
                        {
                            objRightData.Append("<div class" + "=review-via-mobile>" + "Via Mobile" + "</div>");
                        }

                        objRightData.Append("<div class" + "=review-detail-holder>");
                        objRightData.Append("<div class" + "=review-star-holder>" + "" + drNew["createddate"]);
                        objRightData.Append("</div>");
                        objRightData.Append("<div id=\"divreviewdetail_" + i + "\" class" + "=review-details-text>");
                        objRightData.Append(Convert.ToString(drNew["reviewdetail"]));
                        objRightData.Append("</div>");
                        objRightData.Append("<div class" + "=review-more-holder>");
                        int rtextcount = drNew["reviewdetail"].ToString().Length;
                        if (rtextcount > 186)
                        {
                            objRightData.Append("<div class" + "=review-more-link>" + "<a id=\"a_lessMorelink_" + i + "\" href=javascript:showmore('divreviewdetail_" + i + "');ChangeAnchorText('a_lessMorelink_" + i + "');>More</a></div>");
                        }

                        objRightData.Append("<div class" + "=review-problem-link>" + "<a href=javascript:changeDivHtml('divreviewdetail_" + i + "','txtreview','divReviewid_" + i + "');>Problem with this review ?</a></div>");
                        objRightData.Append("<div  style=\"display: none\" id=\"divReportProblem_" + i + "\"></div></div>");
                        objRightData.Append("</div>");
                        objRightData.Append("<div class" + "=clear>" + "</div>");
                        objRightData.Append("</div>");
                        objRightData.Append("<div id=" + "divShowEmb_" + i + " style=\"display: none\">");
                        objRightData.Append(drNew["reviewdetail"]);
                        objRightData.Append("</div>");
                        objRightData.Append("<div id=" + "divReviewid_" + i + " style=\"display: none\">");
                        objRightData.Append(drNew["contentid"]);
                        objRightData.Append("</div>");
                        i++;


                    }
                    objRightData.Append("<div class" + "=" + "clear" + "></div>");



                }
                TempData["Reviews"] = objRightData.ToString();
                //dvpaneladdedbyme.InnerHtml = objRightData.ToString();

            }
            else
            {

                //lblAlertMsgBusinessReview.Visible = true;
                //lblAlertMsgBusinessReview.InnerText = "No Reviews Found";
                //lblAlertMsgBusinessReview.Attributes.Add("class", "bo-MsgRed");
                //dvreviewcount.Visible = false;
                //seemorevideo.Visible = false;


            }



        }

        protected void GetReviews(int businessid)
        {
            BoDashboardModel obj = (BoDashboardModel)Session["BoDashboardWeb"];

            string reportlink = string.Empty;
            //hid_PageNum.Value = "1";
            //hid_ReviewRecordsEnded.Value = "0";
            //divcompanylogo.InnerHtml = "";
            //dvLoadMessage.InnerHtml = "";

            string hid_PageNum = "1";
            string hid_ReviewRecordsEnded = "0";
            int pageNum = (Convert.ToInt16(hid_PageNum) == 0) ? (1) : (Convert.ToInt16(hid_PageNum));


            int rowsPerPage = 5;

            DataSet ds = GetBusinessReviewsBo(businessid, pageNum, rowsPerPage, 0);

            if (ds.Tables[2].Rows[0]["membership"].ToString() == "1")
            {
                obj.membership = "1";

                // lisubs.Visible = false;
            }
            else
            {
                obj.membership = "0";

                //lisubs.Visible = true;
            }


            if (ds.Tables[2].Rows[0]["companylogo"].ToString() != null && ds.Tables[2].Rows[0]["companylogo"].ToString() != "")
            {
                if (ds.Tables[2].Rows[0]["companylogo"].ToString() == null || ds.Tables[2].Rows[0]["companylogo"].ToString() == "")
                {

                    // divcompanylogo.Visible = false;
                }
                else
                {
                    //   Imgcompanylogo.ImageUrl = webconfigImagesRootPath + ds.Tables[2].Rows[0]["companylogo"].ToString();
                }
            }

            else
            {
                //divcompanylogo.Visible = false;
            }
            //Profile image

            if (ds.Tables[3].Rows.Count > 0)
            {
                // imgCompanyProfile.ImageUrl = webconfigImagesRootPath + ds.Tables[3].Rows[0]["photo"].ToString();
                //divcompanylogo.Visible = false;

            }

            else
            {

                // imgCompanyProfile.ImageUrl = webconfigResourceRootPath + "images/noImageAvailable.jpg";

            }

            if (ds.Tables[7].Rows.Count > 0)
            {
                //imgCompanyProfile.ImageUrl = webconfigImagesRootPath + ds.Tables[7].Rows[0]["attachments"].ToString();
                //divcompanylogo.Visible = false;

            }
            //if (imgCompanyProfile.ImageUrl.Contains(("noImageAvailable.jpg")) && !Imgcompanylogo.ImageUrl.Contains(("noImageAvailable.jpg")))
            //{
            //imgCompanyProfile.Visible = false;
            //}

            else
                //    dvreviewchart.Visible = true;

                if (ds.Tables[6].Rows.Count > 0)
                {

                    DataTable dtreview = ds.Tables[4];
                    {


                        foreach (DataRow drNew in dtreview.Rows)
                        {


                            double j = (Convert.ToDouble(drNew["ratingscount"]) / Convert.ToDouble(ds.Tables[5].Rows[0]["totalratings"])) * 100;
                            j = Math.Round(j, 2);

                            if (Convert.ToString(drNew["rating"]) == "5")
                            {
                                obj.tdbar1 = "";
                                obj.tdbar1 = "<span  class= 'bar bar5' " + "style='height:20px;width:" + j + "px" + "'>" + "</span>" + "<span  class= 'value-rating'>" + j + "%" + "</span>";
                            }
                            if (Convert.ToString(drNew["rating"]) == "4")
                            {
                                obj.tdbar2 = "";
                                obj.tdbar2 = "<span  class= 'bar bar4' " + "style='height:20px;width:" + j + "px" + "'>" + "</span>" + "<span  class= 'value-rating'>" + j + "%" + "</span>";
                            }
                            if (Convert.ToString(drNew["rating"]) == "3")
                            {
                                obj.tdbar3 = "";
                                obj.tdbar3 = "<span  class= 'bar bar3' " + "style='height:20px;width:" + j + "px" + "'>" + "</span>" + "<span  class= 'value-rating'>" + j + "%" + "</span>";
                            }
                            if (Convert.ToString(drNew["rating"]) == "2")
                            {
                                obj.tdbar4 = "";
                                obj.tdbar4 = "<span  class= 'bar bar2' " + "style='height:20px;width:" + j + "px" + "'>" + "</span>" + "<span  class= 'value-rating'>" + j + "%" + "</span>";
                            }
                            if (Convert.ToString(drNew["rating"]) == "1")
                            {
                                obj.tdbar5 = "";
                                obj.tdbar5 = "<span  class= 'bar bar1' " + "style='height:20px;width:" + j + "px" + "'>" + "</span>" + "<span  class= 'value-rating'>" + j + "%" + "</span>";
                            }


                        }
                    }

                }
                else
                {

                }



            //Rating Count & Rating 
            if (ds.Tables[6].Rows.Count > 0)
            {
                obj.Rate = ds.Tables[6].Rows[0]["avgrating"].ToString();

                //if (Rate == "0")
                //{
                //    dvreviewchart.Visible = false;
                //}

            }



            else
            {

                //Rate = "";
                //dvreviewchart.Visible = false;

            }

            if (ds.Tables[5].Rows.Count > 0)
            {

                obj.TotalRate = ds.Tables[5].Rows[0]["totalratings"].ToString();
                //if (tRate == "0")
                //{
                //    dvreviewchart.Visible = false;
                //}

            }
            else
            {
                //tRate = "";
                //dvreviewchart.Visible = false;
            }

            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {

                //seemorevideo.Visible = true;
                //dvpaneladdedbyme.Visible = true;
                //TotalRecords = (Convert.ToInt32(ds.Tables[1].Rows[0]["TotalRecords"]));
                //dvreviewcount.Visible = true;
                //dvreviewcount.InnerHtml = "Total Reviews : " + TotalRecords;
                System.Text.StringBuilder objRightData = new System.Text.StringBuilder();
                obj.totalRv = "Total Reviews : "+(Convert.ToInt32(ds.Tables[1].Rows[0]["TotalRecords"]));
                DataTable dtreview = ds.Tables[0];
                {
                    int i = 0;
                    foreach (DataRow drNew in dtreview.Rows)
                    {
                        objRightData.Append("<div class" + "=review-list-holder>");
                        if (drNew["userphoto"].ToString().Contains("http"))
                        {
                            objRightData.Append("<div class" + "=review-users-holder>" + "<img src=" + drNew["userphoto"] + "><br />");
                        }
                        else
                        {
                            objRightData.Append("<div class" + "=review-users-holder>" + "<img src='" + webconfigImagesRootPath + drNew["userphoto"].ToString() + "'><br />");
                        }

                        //if (Convert.ToString(drNew["username"]).Length > 12)
                        //{
                        objRightData.Append("<div class" + "=review-user-link> " + Convert.ToString(drNew["username"]) + "</div>");
                        //}
                        //else
                        //{
                        //    objRightData.Append("<div class" + "=review-user-link> " + "<a href=" + "#" + ">" + Convert.ToString(drNew["username"]) + "</a></div>");
                        //}
                        objRightData.Append("<div class" + "=clear>" + "</div>");
                        objRightData.Append("</div>");
                        if (Convert.ToString(drNew["source"]) == "WAP")
                        {
                            objRightData.Append("<div class" + "=review-via-mobile>" + "Via Mobile" + "</div>");
                        }

                        objRightData.Append("<div class" + "=review-detail-holder>");
                        objRightData.Append("<div class" + "=review-star-holder>" + "" + drNew["createddate"]);
                        objRightData.Append("</div>");
                        objRightData.Append("<div id=\"divreviewdetail_" + i + "\" class" + "=review-details-text>");
                        objRightData.Append(Convert.ToString(drNew["reviewdetail"]));
                        objRightData.Append("</div>");
                        objRightData.Append("<div class" + "=review-more-holder>");
                        int rtextcount = drNew["reviewdetail"].ToString().Length;
                        if (rtextcount > 186)
                        {
                            objRightData.Append("<div class" + "=review-more-link>" + "<a id=\"a_lessMorelink_" + i + "\" href=javascript:showmore('divreviewdetail_" + i + "');ChangeAnchorText('a_lessMorelink_" + i + "');>More</a></div>");
                        }

                        objRightData.Append("<div class" + "=review-problem-link>" + "<a href=javascript:changeDivHtml('divreviewdetail_" + i + "','txtreview','divReviewid_" + i + "');>Problem with this review ?</a></div>");
                        objRightData.Append("<div  style=\"display: none\" id=\"divReportProblem_" + i + "\"></div></div>");
                        objRightData.Append("</div>");
                        objRightData.Append("<div class" + "=clear>" + "</div>");
                        objRightData.Append("</div>");
                        objRightData.Append("<div id=" + "divShowEmb_" + i + " style=\"display: none\">");
                        objRightData.Append(drNew["reviewdetail"]);
                        objRightData.Append("</div>");
                        objRightData.Append("<div id=" + "divReviewid_" + i + " style=\"display: none\">");
                        objRightData.Append(drNew["contentid"]);
                        objRightData.Append("</div>");
                        i++;


                    }
                    objRightData.Append("<div class" + "=" + "clear" + "></div>");



                }
                TempData["Reviews"] = objRightData.ToString();
                //dvpaneladdedbyme.InnerHtml = objRightData.ToString();

            }
            else
            {

                //lblAlertMsgBusinessReview.Visible = true;
                //lblAlertMsgBusinessReview.InnerText = "No Reviews Found";
                //lblAlertMsgBusinessReview.Attributes.Add("class", "bo-MsgRed");
                //dvreviewcount.Visible = false;
                //seemorevideo.Visible = false;


            }



        }

        public ActionResult Data(string pge, string rowsPerPage, string BusinessID)
        {

            int busid = Int32.Parse(BusinessID);

            DataSet ds = GetBusinessReviewsBo(busid, Int32.Parse(pge), Int32.Parse(rowsPerPage), 0);

            StringBuilder getPostsText = new StringBuilder();

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow myDataRow in ds.Tables[0].Rows)
                {
                    Guid i = Guid.NewGuid();
                    getPostsText.AppendFormat("<div class" + "=review-list-holder>");

                    if (myDataRow["userphoto"].ToString().Contains("http"))
                    {
                        getPostsText.Append("<div class" + "=review-users-holder>" + "<img src=" + myDataRow["userphoto"] + "><br />");
                    }
                    else
                    {
                        getPostsText.Append("<div class" + "=review-users-holder>" + "<img src=" + webconfigImagesRootPath + myDataRow["userphoto"].ToString() + "><br />");
                    }


                    getPostsText.AppendFormat("<div class" + "=review-user-link> " + Convert.ToString(myDataRow["username"]) + "</div>");
                    getPostsText.AppendFormat("<div class" + "=clear>" + "</div>");
                    getPostsText.AppendFormat("</div>");
                    if (Convert.ToString(myDataRow["source"]) == "WAP")
                    {
                        getPostsText.AppendFormat("<div class" + "=review-via-mobile>" + "Via Mobile" + "</div>");
                    }

                    getPostsText.AppendFormat("<div class" + "=review-detail-holder>");
                    getPostsText.AppendFormat("<div class" + "=review-star-holder>" + "" + myDataRow["createddate"]);
                    getPostsText.AppendFormat("</div>");
                    getPostsText.AppendFormat("<div id=\"divreviewdetail_" + i + "\" class" + "=review-details-text>");
                    getPostsText.AppendFormat(Convert.ToString(myDataRow["reviewdetail"]));
                    getPostsText.AppendFormat("</div>");
                    getPostsText.AppendFormat("<div class" + "=review-more-holder>");
                    int rtextcount = myDataRow["reviewdetail"].ToString().Length;
                    if (rtextcount > 186)
                    {
                        getPostsText.AppendFormat("<div class" + "=review-more-link>" + "<a id=\"a_lessMorelink_" + i + "\" href=javascript:showmore('divreviewdetail_" + i + "');ChangeAnchorText('a_lessMorelink_" + i + "');>More</a></div>");
                    }


                    getPostsText.AppendFormat("<div class" + "=review-problem-link>" + "<a href=javascript:changeDivHtml('divreviewdetail_" + i + "','txtreview','divReviewid_" + i + "');>Problem with this review ?</a></div>");

                    getPostsText.AppendFormat("<div  style=\"display: none\" id=\"divReportProblem_" + i + "\"></div></div>");
                    getPostsText.AppendFormat("</div>");
                    getPostsText.AppendFormat("<div class" + "=clear>" + "</div>");
                    getPostsText.AppendFormat("</div>");
                    getPostsText.AppendFormat("<div id=" + "divShowEmb_" + i + " style=\"display: none\">");
                    getPostsText.AppendFormat(myDataRow["reviewdetail"].ToString());
                    getPostsText.AppendFormat("</div>");
                    getPostsText.AppendFormat("<div id=" + "divReviewid_" + i + " runat=server style=\"display: none\">");
                    getPostsText.AppendFormat(myDataRow["contentid"].ToString());
                    getPostsText.AppendFormat("</div>");



                    getPostsText.AppendFormat("<div class" + "=" + "clear" + "></div>");

                }
            }

            return Json(getPostsText.ToString(), JsonRequestBehavior.AllowGet);
        }

        public static DataSet GetBusinessReviewsBo(int businessid, int pageNum, int rowsPerPage, int reviewid)
        {

            DataSet ds = new DataSet();

            using (var db = new VconnectDBContext29())
            {

                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_get_businessreviewsbo_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                    cmd.Parameters.Add(new SqlParameter("@pageNum", pageNum));
                    cmd.Parameters.Add(new SqlParameter("@rowsPerPage", rowsPerPage));
                    cmd.Parameters.Add(new SqlParameter("@contentid", reviewid));
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();

                    DataTable tableBusinessReviews = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.BoDashboardModel.BusinessReviews>(reader).ToDataTable();
                    tableBusinessReviews.TableName = "tableBusinessReviews";
                    reader.NextResult();
                    DataTable tableBusinessReviewsTotalRecords = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.BoDashboardModel.BusinessReviewsTotalRecords>(reader).ToDataTable();
                    tableBusinessReviewsTotalRecords.TableName = "tableBusinessReviewsTotalRecords";
                    reader.NextResult();
                    DataTable tableBusinessProfileCompleteness = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.BoDashboardModel.BusinessProfileCompleteness>(reader).ToDataTable();
                    tableBusinessProfileCompleteness.TableName = "tableBusinessProfileCompleteness";
                    reader.NextResult();
                    DataTable tableBusinessUserPhotoDetails = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.BoDashboardModel.BusinessUserPhotoDetails>(reader).ToDataTable();
                    tableBusinessUserPhotoDetails.TableName = "tableBusinessUserPhotoDetails";
                    reader.NextResult();
                    DataTable tableBusinessRatingsDetails = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.BoDashboardModel.BusinessRatingsDetails>(reader).ToDataTable();
                    tableBusinessRatingsDetails.TableName = "tableBusinessRatingsDetails";
                    reader.NextResult();
                    DataTable tableBusinessProfileTotalRatings = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.BoDashboardModel.BusinessProfileTotalRatings>(reader).ToDataTable();
                    tableBusinessProfileTotalRatings.TableName = "tableBusinessProfileTotalRatings";
                    reader.NextResult();
                    DataTable tableBusinessProfileAverageRatings = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.BoDashboardModel.BusinessProfileAverageRatings>(reader).ToDataTable();
                    tableBusinessProfileAverageRatings.TableName = "tableBusinessProfileAverageRatings";
                    reader.NextResult();
                    DataTable tableBusinessProfileAttachments = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.BoDashboardModel.BusinessProfileAttachments>(reader).ToDataTable();
                    tableBusinessProfileAttachments.TableName = "tableBusinessProfileAttachments";

                    ds.Tables.Add(tableBusinessReviews);
                    ds.Tables.Add(tableBusinessReviewsTotalRecords);
                    ds.Tables.Add(tableBusinessProfileCompleteness);
                    ds.Tables.Add(tableBusinessUserPhotoDetails);
                    ds.Tables.Add(tableBusinessRatingsDetails);
                    ds.Tables.Add(tableBusinessProfileTotalRatings);
                    ds.Tables.Add(tableBusinessProfileAverageRatings);
                    ds.Tables.Add(tableBusinessProfileAttachments);

                }
                catch (Exception ex) { }
                finally { db.Database.Connection.Close(); }
            }


            return ds;
        }
        public ActionResult AjaxPostReviews(string BusinessID, string Txteditor, string hdn)
        {
            string ReviewAlertMsg = "";
            UserSession objUS = new UserSession();
            int resultP = 1;

            string reviewid = hdn;
            if (reviewid.Length > 0)
            {
                resultP = AddInCorrectReview_BO(Convert.ToInt32(BusinessID), Convert.ToInt32(reviewid), Convert.ToInt32(objUS.SV_VCUserContentID), Txteditor.Trim(), "WEBBODashboard");
            }
            if (Convert.ToString(resultP) == "0")
            {
                ReviewAlertMsg = "<p class='bo-MsgGreen'>Issue Posted sucessfully</p>";
            }
            else
            {
                ReviewAlertMsg = "You have aleady Posted issue for this Review";
            }

            return Json(ReviewAlertMsg);
        }
        public int AddInCorrectReview_BO(int businessid, int reviewid, int userid, string comment, string source)
        {

            int result = 0;

            using (var db = new VconnectDBContext29())
            {
                try
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[prc_add_Review_issue_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@businessid", businessid));
                    cmd.Parameters.Add(new SqlParameter("@reviewid", reviewid));
                    cmd.Parameters.Add(new SqlParameter("@userid", userid));
                    cmd.Parameters.Add(new SqlParameter("@comment", comment));
                    cmd.Parameters.Add(new SqlParameter("@source", source));

                    var Err = new SqlParameter("@Err", SqlDbType.Int);
                    Err.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(Err);

                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    cmd.ExecuteNonQuery();

                    result = (int)cmd.Parameters["@Err"].Value;

                }
                catch (Exception ex) { }
                finally { db.Database.Connection.Close(); }
            }

            return result;
        }
        public ActionResult BusinessCallLeads(int? filter)
        {
            BoDashboardModel obj = new BoDashboardModel();
            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
                Session.Add("Successcontroller",null);
                Session.Add("Successaction",null);

                string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
                if (Request.QueryString["BID"] != null)
                {

                    int chkuserlogin = CheckUserLogin(SV_VCUserContentID, Request.QueryString["BID"].ToString());
                    if (chkuserlogin == 1)
                    {
                        Session.Add("Successcontroller","BoDashboardWeb");
                        Session.Add("Successaction","BusinessCallLeads");
                        return RedirectToAction("Login", "Account");
                    }
                    obj.BusinessID = Request.QueryString["BID"].ToString();
                    obj.BusinessName = Request.QueryString["businessname"].ToString();
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                if (ViewBag.BusinessID == null)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.BusinessID = arrayr[0];
                    obj.BusinessName = arrayr[1];
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");
                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                else
                {
                    obj.BusinessID = ViewBag.BusinessID;
                    obj.BusinessName = ViewBag.BusinessName;
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }
                if (obj.DefaultBusinessID == null || obj.DefaultBusinessID.ToString().Length <= 0)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }

                obj.ProfileCompletionvalue = obj.GetProfileCompletion(obj.BusinessID);
                obj._bodashboardbannerlist = BodashboardbannerResultFunc(3);
                ViewBag.bodbanner = obj;
                ViewBag.boroothpath = obj.RootPath;
                obj.Pagename = "BoDashboardWeb/Index";
                obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
                string[] arraydata;
                string results = GetleftMenudata(obj.BusinessID);
                arraydata = results.Split(',');
                obj.membership = arraydata[0];
                obj.ProfileImage = arraydata[2];
                obj.CompanyLogo = arraydata[1];
                obj.Pagename = "BuisnessProfile";

                GetBizDetails_BODashBoard(Convert.ToInt32(obj.BusinessID), obj);
                obj.returncontroller = "BoDashboardWeb";
                obj.returnaction = "BusinessCallLeads";

                obj.Subscription = new List<string>();
                using (var db = new VconnectDBContext29())
                {
                    filter = (filter == null) ? 2 : filter;
                    // Create a SQL command to execute the sproc@      @
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[prc_getcallleadsummary_opt]";
                    cmd.Parameters.Add(new SqlParameter("@userid", SV_VCUserContentID));
                    cmd.Parameters.Add(new SqlParameter("@filter", filter));
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    try
                    {
                        // Run the sproc
                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        int sno = 0;
                        if (filter == 3 || filter == 4)
                        {
                            obj.Subscription.Add("<tr><th scope='col'>SNo</th><th scope='col'>Date</th><th scope='col'>Business Name</th><th scope='col'>No.of  Call Leads Send</th></tr>");

                            while (reader.Read())
                            {
                                sno++;
                                obj.Subscription.Add("<tr><td>" + sno + "</td><td>" + reader["calldate"].ToString() + "</td><td>" + reader["businessname"].ToString() + "</td><td>" + reader["noofcall"].ToString() + "</td></tr>");

                            }
                        }
                        else
                        {
                            obj.Subscription.Add("<th scope='col'>SNo</th><th scope='col'>Date</th><th scope='col'>User Phone</th><th scope='col'>File</th>");

                            while (reader.Read())
                            {
                                sno++;
                                obj.Subscription.Add("<tr><td>" + sno + "</td><td>" + reader["calldate"].ToString() + "</td><td>" + reader["userphone"].ToString() + "</td><td>" + reader["filepath"].ToString() + "</td></tr>");

                            }
                        }
                        ViewBag.filter = filter;
                    }
                    finally
                    {
                        db.Database.Connection.Close();
                    }
                }

            }
            else
            {

                Session.Add("Successcontroller","BoDashboardWeb");
                Session.Add("Successaction","BusinessCallLeads");
                return RedirectToAction("Login", "Account");
            }



            return View("BusinessCallLeads", obj);

        }
        public ActionResult KeywordAutoComplete(string term)
        {
            var data = new List<Vconnect.Models.BoDashboardModel.SearchKeywordsPname>();
            List<Vconnect.Models.BoDashboardModel.SearchKeywordsPname> keywordCollection = new List<Vconnect.Models.BoDashboardModel.SearchKeywordsPname>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "select distinct top 10 productname from productmaster where  isverified=1 and productname like '" + term + "%'  order by productname ";
                cmd.CommandType = CommandType.Text;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    keywordCollection = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.BoDashboardModel.SearchKeywordsPname>(reader).ToList();
                    data = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.BoDashboardModel.SearchKeywordsPname>(reader).ToList();
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            var dataJson = keywordCollection.Select(m => new { label = m.Productname, value = string.Concat(m.Productname.ToString()) });
            return Json(dataJson, JsonRequestBehavior.AllowGet);
        }

        public ActionResult KeywordAutoCompleteS(string term)
        {
            var data = new List<Vconnect.Models.BoDashboardModel.SearchKeywordsSname>();
            List<Vconnect.Models.BoDashboardModel.SearchKeywordsSname> keywordCollection = new List<Vconnect.Models.BoDashboardModel.SearchKeywordsSname>();
            using (var db = new VconnectDBContext29())
            {
                term = term.Replace("'", "''");
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "select distinct top 10 servicename from Servicemaster where  isverified=1 and servicename like '" + term + "%'  order by servicename ";
                cmd.CommandType = CommandType.Text;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    
                    keywordCollection = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.BoDashboardModel.SearchKeywordsSname>(reader).ToList();
                    data = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.BoDashboardModel.SearchKeywordsSname>(reader).ToList();
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            var dataJson = keywordCollection.Select(m => new { label = m.Servicename, value = string.Concat(m.Servicename.ToString()) });
            return Json(dataJson, JsonRequestBehavior.AllowGet);
        }

        public ActionResult KeywordAutoCompleteB(string term)
        {
            var data = new List<Vconnect.Models.BoDashboardModel.SearchKeywordsBname>();
            List<Vconnect.Models.BoDashboardModel.SearchKeywordsBname> keywordCollection = new List<Vconnect.Models.BoDashboardModel.SearchKeywordsBname>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "select distinct top 10 * from brandmaster where  isverified=1 and brandname like '" + term + "%'  order by brandname ";
                cmd.CommandType = CommandType.Text;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    keywordCollection = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.BoDashboardModel.SearchKeywordsBname>(reader).ToList();
                    data = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.BoDashboardModel.SearchKeywordsBname>(reader).ToList();
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            var dataJson = keywordCollection.Select(m => new { label = m.brandname, value = string.Concat(m.brandname.ToString()) });
            return Json(dataJson, JsonRequestBehavior.AllowGet);
        }




        #region vediogalary
        [HttpGet]
        public ActionResult GetVedioGalary(string bid, string bname, string videoid)
        {
            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
                Session.Add("Successcontroller",null);
                Session.Add("Successaction",null);
                BoDashboardModel obj = new BoDashboardModel();
                string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
                if (Request.QueryString["ID"] != null)
                {

                    int chkuserlogin = CheckUserLogin(SV_VCUserContentID, Request.QueryString["ID"].ToString());
                    if (chkuserlogin == 1)
                    {
                        Session.Add("Successcontroller","BoDashboardWeb");
                        Session.Add("Successaction","GetVedioGalary");
                        return RedirectToAction("Login", "Account");
                    }
                    obj.BusinessID = Request.QueryString["ID"].ToString();
                    obj.BusinessName = Request.QueryString["bname"].ToString();
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                if (obj.BusinessID == null && bid == null)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.BusinessID = arrayr[0];
                    obj.BusinessName = arrayr[1];
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");
                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                else if (bid != null)
                {
                    obj.BusinessID = bid;
                    obj.BusinessName = bname;
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }
                else
                {

                }
                if (obj.DefaultBusinessID == null || obj.DefaultBusinessID.ToString().Length <= 0)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }

                obj.ProfileCompletionvalue = obj.GetProfileCompletion(obj.BusinessID);
                obj._bodashboardbannerlist = BodashboardbannerResultFunc(3);
                ViewBag.bodbanner = obj;
                ViewBag.boroothpath = obj.RootPath;
                obj.Pagename = "BoDashboardWeb/GetVedioGalary";
                obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
                string[] arraydata;
                string results = GetleftMenudata(obj.BusinessID);
                arraydata = results.Split(',');
                obj.membership = arraydata[0];
                obj.ProfileImage = arraydata[2];
                obj.CompanyLogo = arraydata[1];
                obj.Pagename = "GetVedioGalary";
                obj.returncontroller = "BoDashboardWeb";
                obj.returnaction = "GetVedioGalary";
                int ContentID = 0;
                int businessid = Convert.ToInt32(obj.BusinessID);
                obj._getVediogalary = obj.getbusinessVediolist(ContentID, businessid, obj);
                if (obj._getVediogalary.Count == 0)
                {
                    GetSampleVideoDetails(442, 1, 4, obj);
                    TempData["message"] = null;
                    TempData["messageE"] = "There is no Video added for this business. Add Videos using the form below";
                    return View("businessvideos", obj);


                }
                else
                {
                    if (videoid != null)
                    {
                        VideoDetails(videoid, obj);
                    }
                    obj.returncontroller = "BoDashboardWeb";
                    obj.returnaction = "GetVedioGalary";
                    return View("businessVideosList", obj);
                }
            }
            else
            {
                //Session.Add("Successcontroller","BoDashboardWeb");
                //Session.Add("Successaction","Index");
                //  return RedirectToAction("Login", "Account", new { rturl = "BoDashboardWeb/Index/" });
                return Redirect("/login?rturl=BoDashboardWeb/GetVedioGalary");
            }
        }


        #endregion
        #region Videos
        public ActionResult BuisnessVideos(string bid, string bname)
        {

            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
                Session.Add("Successcontroller",null);
                Session.Add("Successaction",null);
                BoDashboardModel obj = new BoDashboardModel();
                string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
                if (Request.QueryString["BID"] != null)
                {

                    int chkuserlogin = CheckUserLogin(SV_VCUserContentID, Request.QueryString["BID"].ToString());
                    if (chkuserlogin == 1)
                    {
                        Session.Add("Successcontroller","BoDashboardWeb");
                        Session.Add("Successaction","BuisnessVideos");
                        return RedirectToAction("Login", "Account");
                    }
                    obj.BusinessID = Request.QueryString["BID"].ToString();
                    obj.BusinessName = Request.QueryString["bname"].ToString();
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                if (obj.BusinessID == null && bid == null)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.BusinessID = arrayr[0];
                    obj.BusinessName = arrayr[1];
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");
                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                else if (bid != null)
                {
                    obj.BusinessID = bid;
                    obj.BusinessName = bname;
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }
                else
                {

                }
                if (obj.DefaultBusinessID == null || obj.DefaultBusinessID.ToString().Length <= 0)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }

                obj.ProfileCompletionvalue = obj.GetProfileCompletion(obj.BusinessID);
                obj._bodashboardbannerlist = BodashboardbannerResultFunc(3);
                ViewBag.bodbanner = obj;
                ViewBag.boroothpath = obj.RootPath;
                obj.Pagename = "BoDashboardWeb/BuisnessVideos";
                obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
                string[] arraydata;
                string results = GetleftMenudata(obj.BusinessID);
                arraydata = results.Split(',');
                obj.membership = arraydata[0];
                obj.ProfileImage = arraydata[2];
                obj.CompanyLogo = arraydata[1];
                GetSampleVideoDetails(442, 1, 4, obj);
                obj.returncontroller = "BoDashboardWeb";
                obj.returnaction = "BuisnessVideos";

                int ContentID = 0;
                obj._getVediogalary = obj.getbusinessVediolist(ContentID, Convert.ToInt32(bid), obj);
               // obj._getVediogalary = obj.getbusinessVediolist(ContentID, obj.BusinessID, obj);
                if (obj._getVediogalary.Count == 0)
                {
                    TempData["message"] = null;
                    TempData["messageE"] = "There is no video added for this business. Add videos using the form below";
                    return View("businessVideos", obj);


                }
                else
                {
                    TempData["messageE"] = null;
                    TempData["message"] = null;
                    //obj.returncontroller = "BoDashboardWeb";
                    //obj.returnaction = "BuisnessVideos";
                    return View("businessVideos", obj);
                }

               

            }
            else
            {

                //Session.Add("Successcontroller","BoDashboardWeb");
                //Session.Add("Successaction","Index");
                //  return RedirectToAction("Login", "Account", new { rturl = "BoDashboardWeb/Index/" });
                return Redirect("/login?rturl=BoDashboardWeb/BuisnessVideos");
            }
        }

        public ActionResult playBuisnessVideos(string bid, string bname, int videoid)
        {
            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
                Session.Add("Successcontroller",null);
                Session.Add("Successaction",null);
                BoDashboardModel obj = new BoDashboardModel();
                string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
                if (Request.QueryString["BID"] != null)
                {

                    int chkuserlogin = CheckUserLogin(SV_VCUserContentID, Request.QueryString["BID"].ToString());
                    if (chkuserlogin == 1)
                    {
                        Session.Add("Successcontroller","BoDashboardWeb");
                        Session.Add("Successaction","BuisnessVideos");
                        return RedirectToAction("Login", "Account");
                    }
                    obj.BusinessID = Request.QueryString["BID"].ToString();
                    obj.BusinessName = Request.QueryString["bname"].ToString();
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                if (obj.BusinessID == null && bid == null)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.BusinessID = arrayr[0];
                    obj.BusinessName = arrayr[1];
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");
                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                else if (bid != null)
                {
                    obj.BusinessID = bid;
                    obj.BusinessName = bname;
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }
                else
                {

                }
                if (obj.DefaultBusinessID == null || obj.DefaultBusinessID.ToString().Length <= 0)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }

                obj.ProfileCompletionvalue = obj.GetProfileCompletion(obj.BusinessID);
                obj._bodashboardbannerlist = BodashboardbannerResultFunc(3);
                ViewBag.bodbanner = obj;
                ViewBag.boroothpath = obj.RootPath;
                obj.Pagename = "BoDashboardWeb/BuisnessVideos";
                obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
                string[] arraydata;
                string results = GetleftMenudata(obj.BusinessID);
                arraydata = results.Split(',');
                obj.membership = arraydata[0];
                obj.ProfileImage = arraydata[2];
                obj.CompanyLogo = arraydata[1];
                GetSampleVideoDetails(videoid, 1, 4, obj);
                obj.returncontroller = "BoDashboardWeb";
                obj.returnaction = "BuisnessVideos";

                return View("businessVideos", obj);

            }
            else
            {

                //Session.Add("Successcontroller","BoDashboardWeb");
                //Session.Add("Successaction","Index");
                //  return RedirectToAction("Login", "Account", new { rturl = "BoDashboardWeb/Index/" });
                return Redirect("/login?rturl=BoDashboardWeb/playBuisnessVideos");
            }
        }

        [HttpGet]
        public ActionResult Video()
        {
            return Redirect("/login?rturl=BoDashboardWeb/Index");
        }

        [HttpPost]
        public ActionResult Video(string bid, string bname, BoDashboardModel obj)
        {
            // BoDashboardModel obj = new BoDashboardModel();
            string userid = objUS.SV_VCUserContentID.ToString();
            obj._claimedbusinesslist = getbusiness(userid);
            string[] arraydata;
            string results = GetleftMenudata(bid);
            arraydata = results.Split(',');
            obj.membership = arraydata[0];
            obj.ProfileImage = arraydata[2];
            obj.CompanyLogo = arraydata[1];
            obj.BusinessID = bid;
            obj.BusinessName = bname.ToString();
            string VideoTitle = obj.Video_Title;

            if(VideoTitle==null || VideoTitle=="" )
            {
                TempData["message"] = null;
                TempData["messageE"] = "Enter Video Title.";
                return View("businessVideos", obj);

            }
            string VideoDesc = obj.VideoDescription;
            if (VideoDesc == null || VideoDesc == "")
            {
                TempData["message"] = null;
                TempData["messageE"] = "Enter video code";
                return View("businessVideos", obj);

            }
            if (VideoDesc.Contains("iframe") || VideoDesc.Contains("width") || VideoDesc.Contains("height"))
            {
                string iframe = VideoDesc;
                int iframeSrcStartPoint = iframe.IndexOf("src");
                int closeTagPoint = iframe.IndexOf(">");
                string removeAllStringTillStartPoint = iframe.Substring(iframeSrcStartPoint, closeTagPoint - iframeSrcStartPoint);
                int iframeSrcFirstSpacePointAfterSrcAttribute = removeAllStringTillStartPoint.IndexOf(" ");
                string iframeSrc = removeAllStringTillStartPoint.Substring(0, iframeSrcFirstSpacePointAfterSrcAttribute);

                VideoDesc = "<iframe title='YouTube video player'  width=\"100%\" height=\"250\" " + iframeSrc + " frameborder=\"0\" allowfullscreen></iframe>";
                /////////////////////////// VideoDesc = VideoDesc.Trim().Replace("'", "`");
                VideoDesc = VideoDesc.Trim();
            }
            else
            {
                const string pattern = @"(?:https?:\/\/)?(?:www\.)?(?:(?:(?:youtube.com\/watch\?[^?]*v=|youtu.be\/)([\w\-]+))(?:[^\s?]+)?)";
                const string replacement = "<iframe title='YouTube video player' width='100%' height='250' src='http://www.youtube.com/embed/$1' frameborder='0' allowfullscreen='1'></iframe>";
                var rgx = new Regex(pattern);
                var resultString = rgx.Replace(VideoDesc, replacement);
                ///////////////////VideoDesc = resultString.Trim().Replace("'", "`");
                VideoDesc = resultString.Trim();
            }

           
           
           
            //if (obj.DefaultBusinessID == null || obj.DefaultBusinessID.ToString().Length <= 0)
            //{
            //    string[] arrayr;
            //    string result = Getdefaultbusinessdata(userid);
            //    arrayr = result.Split(',');
            //    obj.DefaultBusinessID = arrayr[0];
            //    obj.DefaultBusinessName = arrayr[1];
            //    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

            //}

            obj.ProfileCompletionvalue = obj.GetProfileCompletion(obj.BusinessID);
            obj._bodashboardbannerlist = BodashboardbannerResultFunc(3);
            ViewBag.bodbanner = obj;
            ViewBag.boroothpath = obj.RootPath;
           

            string result = AddVideos(VideoTitle, VideoDesc, userid, obj.BusinessID);
            if (result == "0")
            {
                //obj.VideoDescription = "";
                //obj.VideoTitle = "";
                TempData["messageE"] = null;
                TempData["message"] = "Video Uploaded Sucessfully. It will reflect on website once admin approves it.";
                return View("businessVideos", obj);
                

            }
            else if (result == "2")
            {

                TempData["message"] = null;
                TempData["messageE"] = "Video with the same title already exist.";
                return View("businessVideos", obj);



            }
            else
            {

                TempData["message"] = null;
                TempData["messageE"] = "Video not submited. please try again aater.";
                return View("businessVideos", obj);


            }
           // return RedirectToAction("BuisnessVideos", "BoDashboardWeb");
            
        }

        public string AddVideos(string VideoTitle, string VideoDesc, string userid, string buisnessid)
        {
            string result;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[Usp_AddUpdVideoBO_opt]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.Parameters.Add(new SqlParameter("@businessid", buisnessid));
                cmd.Parameters.Add(new SqlParameter("@createdBy", userid));
                cmd.Parameters.Add(new SqlParameter("@VideoTitle", VideoTitle));
                cmd.Parameters.Add(new SqlParameter("@VideoEmbeddedPath", VideoDesc));
                cmd.Parameters.Add(new SqlParameter("@source", "WEBBoDashbaord"));
                SqlParameter parameter1 = new SqlParameter("@Err", SqlDbType.Int);
                parameter1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(parameter1);
                connection.Open();
                cmd.ExecuteNonQuery();
                result = (string)cmd.Parameters["@ERR"].Value.ToString();
            }


            return result;

        }


        public ActionResult Videorequest(string BusinessId)
        {
            string Userid = objUS.SV_VCUserContentID.ToString();



            string result = AddVideoRequest(Userid, BusinessId);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private string AddVideoRequest(string Userid, string BusinessId)
        {
            string result;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_videorequest_opt]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                cmd.Parameters.Add(new SqlParameter("@userid", Userid));
                cmd.Parameters.Add(new SqlParameter("@businessid", BusinessId));

                SqlParameter parameter1 = new SqlParameter("@Err", SqlDbType.Int);
                parameter1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(parameter1);


                connection.Open();
                cmd.ExecuteNonQuery();
                result = (string)cmd.Parameters["@ERR"].Value.ToString();
            }


            return result;

        }


        public void GetSampleVideoDetails(int contentid, int pageNum, int rowsPerPage, BoDashboardModel obj)
        {
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[Usp_GetSampleVideoBO_opt]";
                cmd.Parameters.Add(new SqlParameter("@contentid", contentid));
                cmd.Parameters.Add(new SqlParameter("@pageNum", pageNum));
                cmd.Parameters.Add(new SqlParameter("@rowsPerPage", rowsPerPage));
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    reader.Read();

                    //Rate = reader["Rating"].ToString();
                    obj.vbusinessname = reader["businessname"].ToString();
                    obj.vcompanylogo = reader["companylogo"].ToString();
                    obj.vavgrating = reader["avgrating"].ToString();
                    obj.vcategory = reader["category"].ToString();
                    obj.vfulladdress = reader["fulladdress"].ToString();
                    obj.videocode = reader["videocode"].ToString();



                }
                finally
                {
                    db.Database.Connection.Close();
                }


            }

        }

        public void VideoDetails(string videoid, BoDashboardModel obj)
        {
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[Usp_GetVideolistBO_opt]";
                cmd.Parameters.Add(new SqlParameter("@contentid", videoid));
                cmd.Parameters.Add(new SqlParameter("@businessid", 0));

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    reader.Read();

                    //Rate = reader["Rating"].ToString();
                    obj.videocode = reader["VideoEmbeddedPath"].ToString();
                    obj.VideoTitle = reader["VideoTitle"].ToString();
                    obj.contactname = reader["contactname"].ToString();
                    obj.vcreateddate = reader["createddate"].ToString();




                }
                finally
                {
                    db.Database.Connection.Close();
                }


            }

        }
        #endregion

        #region Controller For GetContact,Products And Service
        public ActionResult GetContactDetails(string bid, string bname)
        {

            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
                Session.Add("Successcontroller",null);
                Session.Add("Successaction",null);
                BoDashboardModel obj = new BoDashboardModel();
                string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
                if (Request.QueryString["BID"] != null)
                {

                    int chkuserlogin = CheckUserLogin(SV_VCUserContentID, Request.QueryString["BID"].ToString());
                    if (chkuserlogin == 1)
                    {
                        Session.Add("Successcontroller","BoDashboardWeb");
                        Session.Add("Successaction","GetContactDetails");
                        return RedirectToAction("Login", "Account");
                    }
                    obj.BusinessID = Request.QueryString["BID"].ToString();
                    obj.BusinessName = Request.QueryString["bname"].ToString();
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                if (obj.BusinessID == null && bid == null)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.BusinessID = arrayr[0];
                    obj.BusinessName = arrayr[1];
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");
                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");
                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                else if (bid != null)
                {
                    obj.BusinessID = bid;
                    obj.BusinessName = bname;
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }
                else
                {

                }
                if (obj.DefaultBusinessID == null || obj.DefaultBusinessID.ToString().Length <= 0)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }

                obj.ProfileCompletionvalue = obj.GetProfileCompletion(obj.BusinessID);
                obj._bodashboardbannerlist = BodashboardbannerResultFunc(3);
                ViewBag.bodbanner = obj;
                ViewBag.boroothpath = obj.RootPath;
                obj.Pagename = "BoDashboardWeb/GetContactDetails";
                obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
                string[] arraydata;
                string results = GetleftMenudata(obj.BusinessID);
                arraydata = results.Split(',');
                obj.membership = arraydata[0];
                obj.ProfileImage = arraydata[2];
                obj.CompanyLogo = arraydata[1];
                obj.Pagename = "BuisnessProfile";

                //GetBizDetails_BODashBoard(Convert.ToInt32(obj.BusinessID), obj);

                obj.getcontactDetails = obj.getcntactDetails(obj.BusinessID, obj);

                obj.workinghourlist = Utility.GetWorkingHours();
                obj._getWorkinghours = obj.getWorkinghoursBoDashboard(obj.BusinessID);


                obj.returncontroller = "BoDashboardWeb";
                obj.returnaction = "GetContactDetails";
                //return RedirectToRoute("GetContactDetails", obj);
                return View("BusinessContact", obj);
            }
            else
            {

                //Session.Add("Successcontroller","BoDashboardWeb");
                //Session.Add("Successaction","Index");
                //  return RedirectToAction("Login", "Account", new { rturl = "BoDashboardWeb/Index/" });
                return Redirect("/login?rturl=BoDashboardWeb/GetContactDetails");
            }
        }
        //on submit add contact details and working hours to db and then display
        [HttpGet]
        public ActionResult ADDUPCDetails()
        {
            return Redirect("/login?rturl=BoDashboardWeb/Index");
        }

        [HttpPost]
        public ActionResult ADDUPCDetails(FormCollection collection, BoDashboardModel obj)
        {
            //string mondaystart = obj._getWorkinghours[0].mondaystart;

            string mondaystart = collection["timeIDmons"].ToString();
            string mondayend = collection["timeIDmonE"].ToString();
            string tuesdaystart = collection["timeIDLtuesS"].ToString();
            string tuesdayend = collection["timeIDLtuesE"].ToString();
            string wednesdaystart = collection["timeIDWedS"].ToString();
            string wednesdayend = collection["timeIDWedE"].ToString();
            string thursdaystart = collection["timeIDThursS"].ToString();
            string thursdayend = collection["timeIDThursE"].ToString();
            string fridaystart = collection["timeIDFriS"].ToString();
            string fridayend = collection["timeIDFriE"].ToString();
            string saturdaystart = collection["timeIDSatS"].ToString();
            string saturdayend = collection["timeIDSatE"].ToString();
            string sundaystart = collection["timeIDSunS"].ToString();
            string sundayend = collection["timeIDSunE"].ToString();
            string contactname = "";
            if (collection["contactname"]!=null )
            {
                 contactname = collection["contactname"].ToString();
            }
            string designation = "";
            if (collection["designation"] != null)
            {
                designation = collection["designation"].ToString();
            }
            string phone ="";
            if (collection["phone"] != null && collection["phone"].ToString()!="")
            {
                phone= collection["phone"].ToString();
                int isPhonevalid = Vconnect.Common.Utility.isValidPhone(Convert.ToString(collection["phone"]));
                if(isPhonevalid ==0)
                {
                    TempData["alertMsgE"] = "Phone No. Not Valid";
                    return GetContactDetails(collection["bid"].ToString(), collection["bname"].ToString());
                }
            }

            string email = "";
            string alternatephone = "";

            if (collection["alternatephone"] != null && collection["alternatephone"].ToString() != "")
            {
                 alternatephone = collection["alternatephone"].ToString();
                 int isPhonevalid = Vconnect.Common.Utility.isValidPhone(Convert.ToString(collection["alternatephone"]));
                 if (isPhonevalid == 0)
                 {
                     TempData["alertMsgE"] = "Alternate Phone No. Not Valid";
                     return GetContactDetails(collection["bid"].ToString(), collection["bname"].ToString());
                 }
            }
            if (collection["email"] != null)
            {
                 email = collection["email"].ToString();
            }
            string contentid = collection["contentid"].ToString();
            string bid = collection["bid"].ToString();
            string bname = collection["bname"].ToString();
            string title = obj.getcontactDetails.title.ToString();

            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {


                //obj.BusinessID = collection["businessid"];
                obj.addupbusinesscontactDetails(bid, contentid, contactname, designation, phone, alternatephone, email, title);

                obj.addworkinghours(bid, mondaystart, mondayend, tuesdaystart, tuesdayend, wednesdaystart, wednesdayend,
                    thursdaystart, thursdayend, fridaystart, fridayend, saturdaystart, saturdayend, sundaystart, sundayend);

                UpdateBusinessWorkingHours(bid.ToString());
                TempData["alertMsg"] = "Business Contact info updated successfully";

                //Session.Add("update"] = "Contact Details Updated Sucessfully";
                obj.returncontroller = "BoDashboardWeb";
                obj.returnaction = "GetContactDetails";
                return GetContactDetails(bid, bname);

            }
            else
            {

                Session.Add("Successcontroller","BoDashboardWeb");
                Session.Add("Successaction","GetContactDetails");
                return RedirectToAction("Login", "Account");
            }
        }

        private void UpdateBusinessWorkingHours(string businessid)
        {
            try {
                string a = string.Empty, b = string.Empty, c = string.Empty;
                Int32 d;
                string[] day = { "Mon", "Tue", "Wed", "Thur", "Fri", "Sat", "Sun", "a" };
                DataTable dt = new DataTable();
                string query = "Select businessid ,mondaystart,mondayend,tuesdaystart,tuesdayend,wednesdaystart,wednesdayend,thursdaystart,thursdayend,fridaystart,fridayend,saturdaystart,saturdayend,sundaystart,sundayend from businessworkinghours where businessid is not null and businessid="+businessid;
                using (var db = new VconnectDBContext29())
                {
                    // Create a SQL command to execute the sproc
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = query;
                    cmd.CommandType = System.Data.CommandType.Text;
                    try
                    {
                        // Run the sproc
                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        dt = ((IObjectContextAdapter)db).ObjectContext.Translate<Vconnect.Models.BoDashboardModel.WorkingHoursModel>(reader).ToDataTable();
                        dt.TableName = "dtWorkingHours";
                       //cmd.ExecuteNonQuery();
                       // reader.Close();
                    }
                    finally
                    {
                        db.Database.Connection.Close();
                    }
                }

                DataSet ds = new DataSet();
                ds.Tables.Add(dt);

                for (Int32 k = 0; k < ds.Tables[0].Rows.Count; k++)
                {
                    d = Convert.ToInt32(ds.Tables[0].Rows[k]["businessid"].ToString());
                    string[] daystart = { ds.Tables[0].Rows[k]["mondaystart"].ToString(), ds.Tables[0].Rows[k]["tuesdaystart"].ToString(), ds.Tables[0].Rows[k]["wednesdaystart"].ToString(), ds.Tables[0].Rows[k]["thursdaystart"].ToString(), ds.Tables[0].Rows[k]["fridaystart"].ToString(), ds.Tables[0].Rows[k]["saturdaystart"].ToString(), ds.Tables[0].Rows[k]["sundaystart"].ToString(), "b" };
                    string[] dayend = { ds.Tables[0].Rows[k]["mondayend"].ToString(), ds.Tables[0].Rows[k]["tuesdayend"].ToString(), ds.Tables[0].Rows[k]["wednesdayend"].ToString(), ds.Tables[0].Rows[k]["thursdayend"].ToString(), ds.Tables[0].Rows[k]["fridayend"].ToString(), ds.Tables[0].Rows[k]["saturdayend"].ToString(), ds.Tables[0].Rows[k]["sundayend"].ToString(), "c" };
                    for (int i = 0; i < 7; i++)
                    {
                        a = day[i];
                        for (int j = i; j <= 7; j++)
                        {
                            if (daystart[i] == daystart[j] && dayend[i] == dayend[j])
                            {
                                b = day[j];
                                c = c.ToString();
                            }
                            else
                            {
                                if (a == b && daystart[i] == dayend[i])
                                {
                                    c += a + ':' + "Closed,";
                                }
                                else if (a == b && (daystart[i] == "Closed" || dayend[i] == "Closed"))
                                {
                                    c += a + ':' + "Closed,";
                                }
                                else if (a == b && daystart[i] != dayend[i])
                                {
                                    c += a + ':' + daystart[i] + "-" + dayend[i] + ",";
                                }
                                else if (a != b && (daystart[i] == "Closed" || dayend[i] == "Closed"))
                                {
                                    c += a + '-' + b + ':' + "Closed,";
                                }
                                else if (a != b && daystart[i] == dayend[i])
                                {
                                    c += a + '-' + b + ':' + "Closed,";
                                }
                                else if (a != b && daystart[i] != dayend[i])
                                {
                                    c += a + '-' + b + ':' + daystart[i] + "-" + dayend[i] + ",";
                                }
                                else
                                {
                                    c += a + '-' + b + ':' + daystart[i] + "-" + dayend[i] + ",";
                                }
                                i = j - 1;
                                break;
                            }
                        }
                    }
                    BusinessWorkingHour(d, c);
                    c = string.Empty;

                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
        
        }

        public static void BusinessWorkingHour(Int32 d, string c)
        {
              
            using (var db = new VconnectDBContext29())
            {
                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_web_add_StoreBusinessWorkingHour]";
                cmd.Parameters.Add(new SqlParameter("@bizid", d));
                cmd.Parameters.Add(new SqlParameter("@workinghours",  c.ToString()));
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                try
                {
                    // Run the sproc
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
        }

        //For Adding new Products
        [HttpGet]
        public ActionResult AddBuisnessProducts(string bid, string bname)
        {

            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
                Session.Add("Successcontroller",null);
                Session.Add("Successaction",null);
                BoDashboardModel obj = new BoDashboardModel();
                string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
                if (Request.QueryString["BID"] != null)
                {

                    int chkuserlogin = CheckUserLogin(SV_VCUserContentID, Request.QueryString["BID"].ToString());
                    if (chkuserlogin == 1)
                    {
                        Session.Add("Successcontroller","BoDashboardWeb");
                        Session.Add("Successaction","AddBuisnessProducts");
                        return RedirectToAction("Login", "Account");
                    }
                    obj.BusinessID = Request.QueryString["BID"].ToString();
                    obj.BusinessName = Request.QueryString["bname"].ToString();
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                if (obj.BusinessID == null && bid == null)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.BusinessID = arrayr[0];
                    obj.BusinessName = arrayr[1];
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");
                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                else if (bid != null)
                {
                    obj.BusinessID = bid;
                    obj.BusinessName = bname;
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }
                else
                {

                }
                if (obj.DefaultBusinessID == null || obj.DefaultBusinessID.ToString().Length <= 0)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }

                obj.ProfileCompletionvalue = obj.GetProfileCompletion(obj.BusinessID);
                obj._bodashboardbannerlist = BodashboardbannerResultFunc(3);
                ViewBag.bodbanner = obj;
                ViewBag.boroothpath = obj.RootPath;
                obj.Pagename = "BoDashboardWeb/BuisnessProducts";
                obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
                string[] arraydata;
                string results = GetleftMenudata(obj.BusinessID);
                arraydata = results.Split(',');
                obj.membership = arraydata[0];
                obj.ProfileImage = arraydata[2];
                obj.CompanyLogo = arraydata[1];
                obj.Pagename = "BusinessProductsAdd";

                obj.returncontroller = "BoDashboardWeb";
                obj.returnaction = "AddBuisnessProducts";
                return View("BusinessProductsAdd", obj);

            }
            else
            {

                //Session.Add("Successcontroller","BoDashboardWeb");
                //Session.Add("Successaction","Index");
                //  return RedirectToAction("Login", "Account", new { rturl = "BoDashboardWeb/Index/" });
                return Redirect("/login?rturl=BoDashboardWeb/AddBuisnessProducts");
            }
        }
        [HttpPost]
        public ActionResult AddBuisnessProducts(BoDashboardModel bomodel, HttpPostedFileBase uplPhoto, FormCollection collection)
        {

            Int32 brandcointaintid = 0;
            string productdetails = string.Empty;
            string productmodal = string.Empty;
            string productname = bomodel.businessProduct.productname;
            string productprice = string.Empty;
            string brandname = string.Empty;
            string bid = collection["bid"].ToString();
            string bname = collection["bname"].ToString();
            bool FileSaved = false;
            if (bomodel.brandname != null)
            {
                brandname = bomodel.brandname;
            }
            if (bomodel.businessProduct.details != null)
            {
                productdetails = bomodel.businessProduct.details;
            }
            if (bomodel.businessProduct.modal != null)
            {
                productmodal = bomodel.businessProduct.modal;
            }

            if (bomodel.businessProduct.price != null)
            {
                productprice = bomodel.businessProduct.price;
            }

            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
                Session.Add("Successcontroller",null);
                Session.Add("Successaction",null);

                string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();

                string sThumbFileName = string.Empty; string sLargeFileName = string.Empty;

                if (uplPhoto != null)
                {
                    string extensionadd = System.IO.Path.GetExtension(uplPhoto.FileName).ToLower();

                    int bSize = uplPhoto.ContentLength;
                    // Allow only files less than 10240 bytes (approximately 10 KB) to be uploaded.
                    if (bSize <= 5242880)
                    {
                        Random robj = new Random();
                        int randomno = robj.Next(0, 100000000);
                        if (extensionadd == ".gif" || extensionadd == ".jpg" || extensionadd == ".png" || extensionadd == ".jpeg")
                        {
                            //string UploadFolder = "~/Resource/uploads/businessproduct/";
                            //sThumbFileName = uplPhoto.FileName.ToString().Trim().Replace(" ", "-").Replace("&", "and").Replace("'", "`") + "_orginal_" + randomno + extensionadd;
                            sThumbFileName = bid + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + extensionadd;
                            //Vconnect.Common.Utility.ImageHandler newImg = new Vconnect.Common.Utility.ImageHandler(uplPhoto, UploadFolder.ToLower(), sThumbFileName.ToLower(), sThumbFileName.ToLower());
                           // bool uploadLrg = newImg.UploadImage();
                           // string newFilename = newImg.NewFilename;
                           // sThumbFileName = newFilename;
                            FileSaved = true;
                            if (FileSaved)
                            {
                                sThumbFileName = sThumbFileName.ToString().Trim();
                                //Vconnect.Common.Utility.CopyImage.Sync(webconfigSourceImagesRootPath + "uploads", webconfigDestinationImagesRootPath + "uploads");
                                //Vconnect.Common.Utility.CopyImage.Sync(webconfigSourceImagesRootPath + "businessproduct", webconfigDestinationImagesRootPath + "businessproduct");
                                ImageConverter converter = new ImageConverter();
                                System.Drawing.Image uploadImage = System.Drawing.Image.FromStream(uplPhoto.InputStream);
                                var temp = (byte[])converter.ConvertTo(uploadImage, typeof(byte[]));
                                Utility.ByteCloudUploadImages(temp, sThumbFileName, "vcsites/vcimages/resource/uploads/businessproduct/");
                                TempData["alertMsg1"] = null;
                                TempData["alertMsg"] = null;
                            }
                            else
                            {
                                sThumbFileName = "";
                                TempData["alertMsg1"] = null;
                                TempData["alertMsg"] = "Some Error has occured please upload image again";
                                return AddBuisnessProducts(bid, bname);

                            }
                        }
                        else
                        {
                            sThumbFileName = "";
                            TempData["alertMsg1"] = null;
                            TempData["alertMsg"] = "only .gif, .jpg, .png, .jpeg allowed";
                            return AddBuisnessProducts(bid, bname);
                        }
                    }
                    else
                    {
                        sThumbFileName = "";
                        TempData["alertMsg"] = "File size of image is too large. Maximum file size permitted is 5 MB";
                        return AddBuisnessProducts(bid, bname);
                    }
                }
                /////
                int valiedproduct = bomodel.getvalidProductservice(1, productname);
                if (valiedproduct != 0)
                {
                    if (brandname != null && brandname != "")
                    {
                        brandcointaintid = bomodel.GetBrandName(brandname);
                        if (brandcointaintid != 0)
                        {
                            if (uplPhoto != null)
                            {
                                int result1 = bomodel.addBusinessProductByBizID(bid, productname, productdetails, productmodal, brandcointaintid, productprice, SV_VCUserContentID, "uploads/businessproduct/" + sThumbFileName);
                                if (result1 == 0)
                                {
                                    TempData["alertMsg"] = null;
                                    TempData["alertMsg1"] = "Product added successfully. Product image will be displayed on webite after approval from Administrator";
                                    return BuisnessProductsList(bid, bname);

                                }
                                else if (result1 == 1)
                                {
                                    TempData["alertMsg"] = "Invalid Product Name";
                                    TempData["alertMsg1"] = null;
                                    return AddBuisnessProducts(bid, bname);
                                }
                                else if (result1 == 2)
                                {
                                    TempData["alertMsg"] = "The product name already exists. Try again with other Product name.";
                                    TempData["alertMsg1"] = null;
                                    return AddBuisnessProducts(bid, bname);
                                }
                            }
                            else
                            {
                                int result1 = bomodel.addBusinessProductByBizID(bid, productname, productdetails, productmodal, brandcointaintid, productprice, SV_VCUserContentID, "uploads/businessproduct/" + sThumbFileName);
                                if (result1 == 0)
                                {
                                    TempData["alertMsg"] = null;
                                    TempData["alertMsg1"] = "Product added Successfully";
                                    return BuisnessProductsList(bid, bname);

                                }
                                else if (result1 == 1)
                                {
                                    TempData["alertMsg"] = "Invalid Product Name";
                                    TempData["alertMsg1"] = null;
                                    return AddBuisnessProducts(bid, bname);
                                }
                                else if (result1 == 2)
                                {
                                    TempData["alertMsg"] = "The product name already exists. Try again with other Product name.";
                                    TempData["alertMsg1"] = null;
                                    return AddBuisnessProducts(bid, bname);
                                }
                                else
                                {
                                    TempData["alertMsg"] = "Try Again Some External Error Occurs";
                                    TempData["alertMsg1"] = null;
                                    return AddBuisnessProducts(bid, bname);
                                }
                            }
                           
                           
                        }
                        else
                        {
                            TempData["alertMsg"] = "Invalid Brand Name";
                            TempData["alertMsg1"] = null;
                            return AddBuisnessProducts(bid, bname);
                        }
                    }
                    else
                    {
                        if (uplPhoto != null)
                        {
                            int result1 = bomodel.addBusinessProductByBizID(bid, productname, productdetails, productmodal, brandcointaintid, productprice, SV_VCUserContentID, "uploads/businessproduct/" + sThumbFileName);
                            if (result1 == 0)
                            {
                                TempData["alertMsg"] = null;
                                TempData["alertMsg1"] = "Product added successfully. Product image will be displayed on webite after approval from Administrator";
                                return BuisnessProductsList(bid, bname);

                            }
                            else if (result1 == 1)
                            {
                                TempData["alertMsg"] = "Invalid Product Name";
                                TempData["alertMsg1"] = null;
                                return AddBuisnessProducts(bid, bname);
                            }
                            else if (result1 == 2)
                            {
                                TempData["alertMsg"] = "The product name already exists. Try again with other Product name.";
                                TempData["alertMsg1"] = null;
                                return AddBuisnessProducts(bid, bname);
                            }
                        }
                        else
                        {
                            int result1 = bomodel.addBusinessProductByBizID(bid, productname, productdetails, productmodal, brandcointaintid, productprice, SV_VCUserContentID, "uploads/businessproduct/" + sThumbFileName);
                            if (result1 == 0)
                            {
                                TempData["alertMsg"] = null;
                                TempData["alertMsg1"] = "Product added Successfully";
                                return BuisnessProductsList(bid, bname);

                            }
                            else if (result1 == 1)
                            {
                                TempData["alertMsg"] = "Invalid Product Name";
                                TempData["alertMsg1"] = null;
                                return AddBuisnessProducts(bid, bname);
                            }
                            else if (result1 == 2)
                            {
                                TempData["alertMsg"] = "The product name already exists. Try again with other Product name.";
                                TempData["alertMsg1"] = null;
                                return AddBuisnessProducts(bid, bname);
                            }
                            else
                            {
                                TempData["alertMsg"] = "Try Again Some External Error Occurs";
                                TempData["alertMsg1"] = null;
                                return AddBuisnessProducts(bid, bname);
                            }
                        }
                    }
                }
                else
                {
                    TempData["alertMsg"] = "Select Proper Product Name from List";
                    TempData["alertMsg1"] = null;
                    return AddBuisnessProducts(bid, bname);
                }

                string containtid = "0";
                bomodel._getproductswithimage = bomodel.getAllProducts(bid, containtid);
                return AddBuisnessProducts(bid, bname);
            }
            else
            {
                Session.Add("Successcontroller","BoDashboardWeb");
                Session.Add("Successaction","AddBuisnessProducts");
                return RedirectToAction("Login", "Account");
            }
        }

        //For Getting The List Of Products
        [HttpGet]
        public ActionResult BuisnessProductsList(string bid, string bname)
        {

            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
                Session.Add("Successcontroller",null);
                Session.Add("Successaction",null);
                BoDashboardModel obj = new BoDashboardModel();
                string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
                if (Request.QueryString["BID"] != null)
                {

                    int chkuserlogin = CheckUserLogin(SV_VCUserContentID, Request.QueryString["BID"].ToString());
                    if (chkuserlogin == 1)
                    {
                        Session.Add("Successcontroller","BoDashboardWeb");
                        Session.Add("Successaction","BuisnessProductsList");
                        return RedirectToAction("Login", "Account");
                    }
                    obj.BusinessID = Request.QueryString["BID"].ToString();
                    obj.BusinessName = Request.QueryString["bname"].ToString();
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                if (obj.BusinessID == null && bid == null)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.BusinessID = arrayr[0];
                    obj.BusinessName = arrayr[1];
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");
                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                else if (bid != null)
                {
                    obj.BusinessID = bid;
                    obj.BusinessName = bname;
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }
                else
                {

                }
                if (obj.DefaultBusinessID == null || obj.DefaultBusinessID.ToString().Length <= 0)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }

                obj.ProfileCompletionvalue = obj.GetProfileCompletion(obj.BusinessID);
                obj._bodashboardbannerlist = BodashboardbannerResultFunc(3);
                ViewBag.bodbanner = obj;
                ViewBag.boroothpath = obj.RootPath;
                obj.Pagename = "BoDashboardWeb/BuisnessProductsList";
                obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
                string[] arraydata;
                string results = GetleftMenudata(obj.BusinessID);
                arraydata = results.Split(',');
                obj.membership = arraydata[0];
                obj.ProfileImage = arraydata[2];
                obj.CompanyLogo = arraydata[1];
                obj.Pagename = "BuisnessProductsList";

                obj.returncontroller = "BoDashboardWeb";
                obj.returnaction = "BuisnessProductsList";
                string businessid = obj.BusinessID;
                string containtid = "0";
               
                obj._getproductswithimage = obj.getAllProducts(businessid, containtid);
                if (obj._getproductswithimage.Count > 0)
                {
                    return View("BusinessProductslist", obj);
                }

                else
                {
                    TempData["alertMsg1"] = null;
                    TempData["alertMsg"] = "There is no product added for this business. Add products using the form below";

                    return RedirectToAction("AddBuisnessProducts", "BoDashboardWeb", new { bid = obj.BusinessID, bname = obj.BusinessName });
                } 
               

            }
            else
            {

                //Session.Add("Successcontroller","BoDashboardWeb");
                //Session.Add("Successaction","Index");
                //  return RedirectToAction("Login", "Account", new { rturl = "BoDashboardWeb/Index/" });
                return Redirect("/login?rturl=BoDashboardWeb/BuisnessProductsList");
            }
        }
        //For Deleting the products
        public ActionResult DeleteProducts(string cid,string bid,string bname)
        {
            BoDashboardModel obj = new BoDashboardModel();
            string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
            string businessid = "0";
            obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
            int result = obj.DeleteProducts(businessid, cid, SV_VCUserContentID);
            if (result == 1)
            {
                TempData["alertMsg1"] = "Product Deleted Successfully";
            }
            else
            {
                TempData["alertMsg"] = "Product Not Deleted Successfully";
            }

            return RedirectToAction("BuisnessProductsList", "BoDashboardWeb", new { bid=bid,bname=bname});
        }
        //For Edit Products Get
        [HttpGet]
        public ActionResult EditProducts(string cid, string bid)
        {
            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
                Session.Add("Successcontroller",null);
                Session.Add("Successaction",null);
                BoDashboardModel obj = new BoDashboardModel();
                string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
                if (Request.QueryString["BID"] != null)
                {

                    int chkuserlogin = CheckUserLogin(SV_VCUserContentID, Request.QueryString["BID"].ToString());
                    if (chkuserlogin == 1)
                    {
                        Session.Add("Successcontroller","BoDashboardWeb");
                        Session.Add("Successaction","BuisnessProductsList");
                        return RedirectToAction("Login", "Account");
                    }
                    obj.BusinessID = Request.QueryString["BID"].ToString();
                    obj.BusinessName = Request.QueryString["bname"].ToString();
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                if (ViewBag.BusinessID == null)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.BusinessID = arrayr[0];
                    obj.BusinessName = arrayr[1];
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");
                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                else
                {
                    obj.BusinessID = ViewBag.BusinessID;
                    obj.BusinessName = ViewBag.BusinessName;
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }
                if (obj.DefaultBusinessID == null || obj.DefaultBusinessID.ToString().Length <= 0)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }

                obj.ProfileCompletionvalue = obj.GetProfileCompletion(obj.BusinessID);
                obj._bodashboardbannerlist = BodashboardbannerResultFunc(3);
                ViewBag.bodbanner = obj;
                ViewBag.boroothpath = obj.RootPath;
                obj.Pagename = "BoDashboardWeb/BuisnessProductsList";
                obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
                string[] arraydata;
                string results = GetleftMenudata(obj.BusinessID);
                arraydata = results.Split(',');
                obj.membership = arraydata[0];
                obj.ProfileImage = arraydata[2];
                obj.CompanyLogo = arraydata[1];
                obj.Pagename = "BusinessProductsEdit";

                obj.returncontroller = "BoDashboardWeb";
                obj.returnaction = "BuisnessProductsList";
                string businessid = obj.BusinessID;
                obj._editbusinessProduct = obj.EditProductList(cid, bid);

                // obj._getBrand = obj.GetBrandName(brandname);
                //string brandname1 = obj._getBrand.SingleOrDefault().brandname.ToString();

                return View("BusinessProductsEdit", obj);

            }
            else
            {

                //Session.Add("Successcontroller","BoDashboardWeb");
                //Session.Add("Successaction","Index");
                //  return RedirectToAction("Login", "Account", new { rturl = "BoDashboardWeb/Index/" });
                return Redirect("/login?rturl=BoDashboardWeb/Index");
            }
        }
        //For Editing Products Post
        [HttpPost]
        public ActionResult EditProducts(string cid, HttpPostedFileBase uplPhoto, FormCollection Form)
        {
            BoDashboardModel obj = new BoDashboardModel();
            string productname = string.Empty;
            string productdetails = string.Empty;
            string productmodal = string.Empty;
            string productbrand = string.Empty;
            string price = string.Empty;
            string brandname = string.Empty;
            bool FileSaved = false;
            productname = Form["productname"].ToString();
            if (Form["brandname"].ToString() != null)
            {
                brandname = Form["brandname"].ToString();
            }
            if (Form["details"].ToString() != null)
            {
                productdetails = Form["details"].ToString();
            }
            if (Form["modal"].ToString() != null)
            {
                productmodal = Form["modal"].ToString();
            }

            if (Form["price"].ToString() != null)
            {
                price = Form["price"].ToString();
            }



            //string image = Form["image"].ToString();
            string containtid = Form["contentid"];
            string businessid = Form["businessid"];
            string bid = Form["bid"];
            string bname = Form["bname"];
            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {

                string sThumbFileName = string.Empty; string sLargeFileName = string.Empty;

                if (uplPhoto != null)
                {
                    string extensionadd = System.IO.Path.GetExtension(uplPhoto.FileName).ToLower();

                    int bSize = uplPhoto.ContentLength;
                    // Allow only files less than 10240 bytes (approximately 10 KB) to be uploaded.
                    if (bSize <= 5242880)
                    {
                        Random robj = new Random();
                        int randomno = robj.Next(0, 100000000);
                        if (extensionadd == ".gif" || extensionadd == ".jpg" || extensionadd == ".png" || extensionadd == ".jpeg")
                        {
                            //string UploadFolder = "~/Resource/uploads/businessproduct/";
                            //sThumbFileName = uplPhoto.FileName.ToString().Trim().Replace(" ", "-").Replace("&", "and").Replace("'", "`") + "_orginal_" + randomno + extensionadd;
                            sThumbFileName = businessid + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + extensionadd;
                            //Vconnect.Common.Utility.ImageHandler newImg = new Vconnect.Common.Utility.ImageHandler(uplPhoto, UploadFolder.ToLower(), sThumbFileName.ToLower(), sThumbFileName.ToLower());
                            //bool uploadLrg = newImg.UploadImage();
                            //string newFilename = newImg.NewFilename;
                            //sThumbFileName = newFilename;
                            FileSaved = true;
                            if (FileSaved)
                            {
                                sThumbFileName = sThumbFileName.ToString().Trim();
                                //Vconnect.Common.Utility.CopyImage.Sync(webconfigSourceImagesRootPath + "uploads", webconfigDestinationImagesRootPath + "uploads");
                               // Vconnect.Common.Utility.CopyImage.Sync(webconfigSourceImagesRootPath + "businessproduct", webconfigDestinationImagesRootPath + "businessproduct");
                                ImageConverter converter = new ImageConverter();
                                System.Drawing.Image uploadImage = System.Drawing.Image.FromStream(uplPhoto.InputStream);
                                var temp = (byte[])converter.ConvertTo(uploadImage, typeof(byte[]));
                                Utility.ByteCloudUploadImages(temp, sThumbFileName, "vcsites/vcimages/resource/uploads/businessproduct/");
                                TempData["alertMsg"] = null;
                                TempData["alertMsg1"] = null;
                            }
                            else
                            {
                                sThumbFileName = "";
                                TempData["alertMsg1"] = null;
                                TempData["alertMsg"]  = "Some Error has occured please upload image again";
                                return AddBuisnessProducts(bid, bname);

                            }
                        }
                        else
                        {
                            sThumbFileName = "";
                            TempData["alertMsg1"] = null;
                            TempData["alertMsg"] = "only .gif, .jpg, .png, .jpeg allowed";
                            return AddBuisnessProducts(bid, bname);
                        }
                    }
                    else
                    {
                        sThumbFileName = "";
                        TempData["alertMsg1"] = null;
                        TempData["alertMsg"] = "File size of image is too large. Maximum file size permitted is 5 MB";
                        return AddBuisnessProducts(bid, bname);
                    }
                }
                //check brand is valid or not
                /////
                int valiedproduct = obj.getvalidProductservice(1, productname);
                if (valiedproduct != 0)
                {
                    if (brandname != null && brandname != "")
                    {
                        int brandcointaintid = obj.GetBrandName(brandname);
                        if (brandcointaintid != 0)
                        {
                            if (uplPhoto != null)
                            {
                                int result1 = obj.UpdateProductDetails(containtid, businessid, productname, productdetails, productmodal, productbrand, price, brandcointaintid, "uploads/businessproduct/" + sThumbFileName);
                                if (result1 == 3)
                                {
                                    TempData["alertMsg"] = null;
                                    TempData["alertMsg1"] = "Product updated successfully.Image will appear once admin approves it.";

 
                                    return BuisnessProductsList(bid, bname);

                                }
                                else
                                {
                                    TempData["alertMsg1"] = null;
                                    TempData["alertMsg"] = "Product Not Updated Successfully";
                                }
                            }
                            else
                            {
                                int result1 = obj.UpdateProductDetails(containtid, businessid, productname, productdetails, productmodal, productbrand, price, brandcointaintid, "uploads/businessproduct/" + sThumbFileName);
                                if (result1 == 3)
                                {
                                    TempData["alertMsg"] = null;
                                    TempData["alertMsg1"] = "Product updated successfully.";
                                    return BuisnessProductsList(bid, bname);
                                }
                                else
                                {
                                    TempData["alertMsg1"] = null;
                                    TempData["alertMsg"] = "Product Not Updated Successfully";
                                }
                            }
                        }
                        else
                        {
                            TempData["alertMsg1"] = null;
                            TempData["alertMsg"] = "Invalid BrandName Name";
                            return AddBuisnessProducts(bid, bname);
                        }

                    }
                    else
                    {
                        if (uplPhoto != null )
                        {
                            int result1 = obj.UpdateProductDetails(containtid, businessid, productname, productdetails, productmodal, productbrand, price, 0, "uploads/businessproduct/" + sThumbFileName);
                            if (result1 == 3)
                            {
                                TempData["alertMsg"] = null;
                                TempData["alertMsg1"] = "Product updated successfully.Image will appear once approved by Admin ";
                                return BuisnessProductsList(bid, bname);
                            }
                            else
                            {
                                TempData["alertMsg1"] = null;
                                TempData["alertMsg"] = "Product Not Updated Successfully";
                            }
                        }
                        else
                        {
                            int result1 = obj.UpdateProductDetails(containtid, businessid, productname, productdetails, productmodal, productbrand, price, 0, "uploads/businessproduct/" + sThumbFileName);
                            if (result1 == 3)
                            {
                                TempData["alertMsg"] = null;
                                TempData["alertMsg1"] = "Product updated successfully.";
                                return BuisnessProductsList(bid, bname);
                            }
                            else
                            {
                                TempData["alertMsg1"] = null;
                                TempData["alertMsg"] = "Product Not Updated Successfully";
                            }

                        }
                    }
                }
                else
                {
                    TempData["alertMsg1"] = null;
                    TempData["alertMsg"] = "Invalid Product Name";
                    return AddBuisnessProducts(bid, bname);
                }

                ////////



            }
            else
            {
                Session.Add("Successcontroller","BoDashboardWeb");
                Session.Add("Successaction","BuisnessProductsList");
                return RedirectToAction("Login", "Account");
            }
            return AddBuisnessProducts(bid, bname);

        }
        //For Adding New Services Get
        [HttpGet]
        public ActionResult BuisnessServicesAdd(string bid, string bname)
        {
            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
                Session.Add("Successcontroller",null);
                Session.Add("Successaction",null);
                BoDashboardModel obj = new BoDashboardModel();
                string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
                if (Request.QueryString["BID"] != null)
                {

                    int chkuserlogin = CheckUserLogin(SV_VCUserContentID, Request.QueryString["BID"].ToString());
                    if (chkuserlogin == 1)
                    {
                        Session.Add("Successcontroller","BoDashboardWeb");
                        Session.Add("Successaction","BuisnessImagesList");
                        return RedirectToAction("Login", "Account");
                    }
                    obj.BusinessID = Request.QueryString["BID"].ToString();
                    obj.BusinessName = Request.QueryString["bname"].ToString();
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                if (ViewBag.BusinessID == null)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.BusinessID = arrayr[0];
                    obj.BusinessName = arrayr[1];
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");
                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                else
                {
                    obj.BusinessID = ViewBag.BusinessID;
                    obj.BusinessName = ViewBag.BusinessName;
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }
                if (obj.DefaultBusinessID == null || obj.DefaultBusinessID.ToString().Length <= 0)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }

                obj.ProfileCompletionvalue = obj.GetProfileCompletion(obj.BusinessID);
                obj._bodashboardbannerlist = BodashboardbannerResultFunc(3);
                ViewBag.bodbanner = obj;
                ViewBag.boroothpath = obj.RootPath;
                obj.Pagename = "BoDashboardWeb/BusinessServiceAdd";
                obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
                string[] arraydata;
                string results = GetleftMenudata(obj.BusinessID);
                arraydata = results.Split(',');
                obj.membership = arraydata[0];
                obj.ProfileImage = arraydata[2];
                obj.CompanyLogo = arraydata[1];
                obj.Pagename = "BusinessServiceAdd";

                obj.returncontroller = "BoDashboardWeb";
                obj.returnaction = "BusinessServiceAdd";
                return View("BusinessServiceAdd", obj);
            }
            else
            {
                //Session.Add("Successcontroller","BoDashboardWeb");
                //Session.Add("Successaction","Index");
                //  return RedirectToAction("Login", "Account", new { rturl = "BoDashboardWeb/Index/" });
                return Redirect("/login?rturl=BoDashboardWeb/Index");
            }
        }
        //For Adding New Services Post
        [HttpPost]
        public ActionResult BuisnessServicesAdd(BoDashboardModel bomodel, HttpPostedFileBase uplPhoto, FormCollection collection)
        {
            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
                string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
                string servicename = string.Empty;
                string servicedetails = string.Empty;
                string pricerange = string.Empty;
                string bid = collection["bid"].ToString();
                string bname = collection["bname"].ToString();
                bool FileSaved = false;
                if (bomodel.addservices.servicename != null)
                {
                    servicename = bomodel.addservices.servicename;
                }
                if (bomodel.addservices.details != null)
                {
                    servicedetails = bomodel.addservices.details;
                }
                if (bomodel.addservices.pricerange != null)
                {
                    pricerange = bomodel.addservices.pricerange;
                }

                string serviceimage = bomodel.addservices.image;


                //image upload
                string sThumbFileName = string.Empty; string sLargeFileName = string.Empty;

                if (uplPhoto != null)
                {
                    string extensionadd = System.IO.Path.GetExtension(uplPhoto.FileName).ToLower();

                    int bSize = uplPhoto.ContentLength;
                    // Allow only files less than 10240 bytes (approximately 10 KB) to be uploaded.
                    if (bSize <= 5242880)
                    {
                        Random robj = new Random();
                        int randomno = robj.Next(0, 100000000);
                        if (extensionadd == ".gif" || extensionadd == ".jpg" || extensionadd == ".png" || extensionadd == ".jpeg")
                        {
                            //string UploadFolder = "~/Resource/uploads/businessservice/";

                            //sThumbFileName = uplPhoto.FileName.ToString().Trim().Replace(" ", "-").Replace("&", "and").Replace("'", "`") + "_orginal_" + randomno + extensionadd;
                            sThumbFileName = bid + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + extensionadd;
                            //Vconnect.Common.Utility.ImageHandler newImg = new Vconnect.Common.Utility.ImageHandler(uplPhoto, UploadFolder.ToLower(), sThumbFileName.ToLower(), sThumbFileName.ToLower());
                            //bool uploadLrg = newImg.UploadImage();
                            //string newFilename = newImg.NewFilename;
                            //sThumbFileName = newFilename;
                            FileSaved = true;
                            if (FileSaved)
                            {
                                sThumbFileName = sThumbFileName.ToString().Trim();
                                //Vconnect.Common.Utility.CopyImage.Sync(webconfigSourceImagesRootPath + "uploads", webconfigDestinationImagesRootPath + "uploads");
                                //Vconnect.Common.Utility.CopyImage.Sync(webconfigSourceImagesRootPath + "businessservice", webconfigDestinationImagesRootPath + "businessservice");
                                ImageConverter converter = new ImageConverter();
                                System.Drawing.Image uploadImage = System.Drawing.Image.FromStream(uplPhoto.InputStream);
                                var temp = (byte[])converter.ConvertTo(uploadImage, typeof(byte[]));
                                Utility.ByteCloudUploadImages(temp, sThumbFileName, "vcsites/vcimages/resource/uploads/businessservice/");
                                TempData["alertMsg"] = null;
                                TempData["alertMsg1"] = null;
                            }
                            else
                            {
                                sThumbFileName = "";
                                TempData["alertMsg1"] = null;
                                TempData["alertMsg"] = "Some Error has occured please upload image again";
                                return BuisnessServicesAdd(bid, bname);
                            }
                        }
                        else
                        {
                            sThumbFileName = "";
                            TempData["alertMsg1"] = null;
                            TempData["alertMsg"] = "only .gif, .jpg, .png, .jpeg allowed";
                            return BuisnessServicesAdd(bid, bname);
                        }
                    }
                    else
                    {
                        sThumbFileName = "";
                        TempData["alertMsg1"] = null;
                        TempData["alertMsg"] = "File size of image is too large. Maximum file size permitted is 5 MB";
                        return BuisnessServicesAdd(bid, bname);
                    }
                }
                int valiedproduct = bomodel.getvalidProductservice(2, servicename);
                if (valiedproduct == 0)
                {
                    TempData["alertMsg1"] = null;
                    TempData["alertMsg"] = "Invalid Service Name,Please Select proper Service Name from list!";
                    return BuisnessServicesAdd(bid, bname);
                }
                if (uplPhoto != null)
                {
                    int result1 = bomodel.addnewservices(bid, servicename, servicedetails, pricerange, SV_VCUserContentID, "uploads/businessservice/" + sThumbFileName, bname);
                    if (result1 == 0)
                    {
                        TempData["alertMsg"] = null;
                        TempData["alertMsg1"] = "Services added successfully. Service image will be displayed on webite after approval from Administrator";
                    }
                    if (result1 == 2)
                    {
                        TempData["alertMsg1"] = null;
                        TempData["alertMsg"] = "Service Name Already Exists,Please try with another Name";
                        return BuisnessServicesAdd(bid, bname);
                    }


                    string containtid = "0";
                    bomodel._GetAllServices = bomodel.getAllServicesList(bid, containtid);
                    return GetBuisnessServicesList(bid, bname);
                }
                else
                {
                    int result1 = bomodel.addnewservices(bid, servicename, servicedetails, pricerange, SV_VCUserContentID, "uploads/businessservice/" + sThumbFileName, bname);
                    if (result1 == 0)
                    {
                        TempData["alertMsg"] = null;
                        TempData["alertMsg1"] = "Services added successfully.";
                    }
                    if (result1 == 2)
                    {
                        TempData["alertMsg1"] = null;
                        TempData["alertMsg"] = "Service Name Already Exists,Please try with another Name";
                        return BuisnessServicesAdd(bid, bname);
                    }


                    string containtid = "0";
                    bomodel._GetAllServices = bomodel.getAllServicesList(bid, containtid);
                    return GetBuisnessServicesList(bid, bname);
                }
            }
            else
            {
                Session.Add("Successcontroller","BoDashboardWeb");
                Session.Add("Successaction","BuisnessServicesAdd");
                return RedirectToAction("Login", "Account");
            }
        }
        //For Getting All Services list
        [HttpGet]
        public ActionResult GetBuisnessServicesList(string bid="", string bname="")
        {

            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
                Session.Add("Successcontroller",null);
                Session.Add("Successaction",null);
                BoDashboardModel obj = new BoDashboardModel();
                string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
                if (Request.QueryString["BID"] != null)
                {

                    int chkuserlogin = CheckUserLogin(SV_VCUserContentID, Request.QueryString["BID"].ToString());
                    if (chkuserlogin == 1)
                    {
                        Session.Add("Successcontroller","BoDashboardWeb");
                        Session.Add("Successaction","GetBuisnessServicesList");
                        return RedirectToAction("Login", "Account");
                    }
                    obj.BusinessID = Request.QueryString["BID"].ToString();
                    obj.BusinessName = Request.QueryString["bname"].ToString();
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                if (obj.BusinessID == null && bid == null)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.BusinessID = arrayr[0];
                    obj.BusinessName = arrayr[1];
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");
                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                else if (bid != null)
                {
                    obj.BusinessID = bid;
                    obj.BusinessName = bname;
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }
                else
                {

                }
                if (obj.DefaultBusinessID == null || obj.DefaultBusinessID.ToString().Length <= 0)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }

                obj.ProfileCompletionvalue = obj.GetProfileCompletion(obj.BusinessID);
                obj._bodashboardbannerlist = BodashboardbannerResultFunc(3);
                ViewBag.bodbanner = obj;
                ViewBag.boroothpath = obj.RootPath;
                obj.Pagename = "BoDashboardWeb/BuisnessProductsList";
                obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
                string[] arraydata;
                string results = GetleftMenudata(obj.BusinessID);
                arraydata = results.Split(',');
                obj.membership = arraydata[0];
                obj.ProfileImage = arraydata[2];
                obj.CompanyLogo = arraydata[1];
                obj.Pagename = "BuisnessProductsList";

                obj.returncontroller = "BoDashboardWeb";
                obj.returnaction = "GetBuisnessServicesList";
                string businessid = obj.BusinessID;
                string containtid = "0";
                obj._GetAllServices = obj.getAllServicesList(businessid, containtid);
                if (obj._GetAllServices.Count > 0)
                {
                    return View("BusinessServiceslist", obj);
                }
                else
                {
                    TempData["alertMsg"] = null;
                    TempData["alertMsg"] = "There is no service added for this business. Add Service using the form below";
                   
                    return View("BusinessServiceAdd", obj);
                }


            }
            else
            {

                //Session.Add("Successcontroller","BoDashboardWeb");
                //Session.Add("Successaction","Index");
                //  return RedirectToAction("Login", "Account", new { rturl = "BoDashboardWeb/Index/" });
                return Redirect("/login?rturl=BoDashboardWeb/GetBuisnessServicesList");
            }
        }
        //For Editing A particular Services Get
        [HttpGet]
        public ActionResult EditServices(string cid, string bid, string bname)
        {

            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
                Session.Add("Successcontroller",null);
                Session.Add("Successaction",null);
                BoDashboardModel obj = new BoDashboardModel();
                string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
                if (Request.QueryString["BID"] != null)
                {

                    int chkuserlogin = CheckUserLogin(SV_VCUserContentID, Request.QueryString["BID"].ToString());
                    if (chkuserlogin == 1)
                    {
                        Session.Add("Successcontroller","BoDashboardWeb");
                        Session.Add("Successaction","GetBuisnessServicesList");
                        return RedirectToAction("Login", "Account");
                    }
                    obj.BusinessID = Request.QueryString["BID"].ToString();
                    obj.BusinessName = Request.QueryString["bname"].ToString();
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                if (obj.BusinessID == null && bid == null)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.BusinessID = arrayr[0];
                    obj.BusinessName = arrayr[1];
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");
                    ViewBag.BusinessID = obj.BusinessID;
                    ViewBag.BusinessName = obj.BusinessName;
                }
                else if (bid != null)
                {
                    obj.BusinessID = bid;
                    obj.BusinessName = bname;
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }
                else
                {

                }
                if (obj.DefaultBusinessID == null || obj.DefaultBusinessID.ToString().Length <= 0)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }

                obj.ProfileCompletionvalue = obj.GetProfileCompletion(obj.BusinessID);
                obj._bodashboardbannerlist = BodashboardbannerResultFunc(3);
                ViewBag.bodbanner = obj;
                ViewBag.boroothpath = obj.RootPath;
                obj.Pagename = "BoDashboardWeb/GetBuisnessServicesList";
                obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
                string[] arraydata;
                string results = GetleftMenudata(obj.BusinessID);
                arraydata = results.Split(',');
                obj.membership = arraydata[0];
                obj.ProfileImage = arraydata[2];
                obj.CompanyLogo = arraydata[1];
                obj.Pagename = "GetBuisnessServicesList";

                obj.returncontroller = "BoDashboardWeb";
                obj.returnaction = "GetBuisnessServicesList";
                string businessid = obj.BusinessID;
                string containtid = cid;
                obj._GetAllServices = obj.getSpecificServices(businessid, containtid);
                return View("EditBusinessServices", obj);

            }
            else
            {

                //Session.Add("Successcontroller","BoDashboardWeb");
                //Session.Add("Successaction","Index");
                //  return RedirectToAction("Login", "Account", new { rturl = "BoDashboardWeb/Index/" });
                return Redirect("/login?rturl=BoDashboardWeb/Index");
            }
        }
        //For Editing Editing A partcular Services Post
        [HttpPost]
        public ActionResult EditServices(string cid, HttpPostedFileBase uplPhoto, FormCollection form)
        {
            string servicename = "";
            string servicedetails = "";
            string pricerange = "";
            string containtid = "";
            string businessid = "";
            bool FileSaved = false;

            if (form["servicename"] != null)
                servicename = form["servicename"];
            if (form["details"] != null)
                servicedetails = form["details"];
            if (form["pricerange"] != null)
                pricerange = form["pricerange"];


            containtid = form["contentid"];
            businessid = form["businessid"];
            string bid = form["bid"];
            string bname = form["bname"];
            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString() != "0" && !string.IsNullOrEmpty(objUS.SV_VCUserType) && objUS.SV_VCUserType.ToString() == "59")
            {
                Session.Add("Successcontroller",null);
                Session.Add("Successaction",null);

                //image upload
                string sThumbFileName = string.Empty; string sLargeFileName = string.Empty;

                if (uplPhoto != null)
                {
                    string extensionadd = System.IO.Path.GetExtension(uplPhoto.FileName).ToLower();

                    int bSize = uplPhoto.ContentLength;
                    // Allow only files less than 10240 bytes (approximately 10 KB) to be uploaded.
                    if (bSize <= 5242880)
                    {
                        Random robj = new Random();
                        int randomno = robj.Next(0, 100000000);
                        if (extensionadd == ".gif" || extensionadd == ".jpg" || extensionadd == ".png" || extensionadd == ".jpeg")
                        {
                            //string UploadFolder = "~/Resource/uploads/businessservice/";
                            //sThumbFileName = uplPhoto.FileName.ToString().Trim().Replace(" ", "-").Replace("&", "and").Replace("'", "`") + "_orginal_" + randomno + extensionadd;
                            sThumbFileName = businessid + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + extensionadd;
                            //Vconnect.Common.Utility.ImageHandler newImg = new Vconnect.Common.Utility.ImageHandler(uplPhoto, UploadFolder.ToLower(), sThumbFileName.ToLower(), sThumbFileName.ToLower());
                            //bool uploadLrg = newImg.UploadImage();
                            //string newFilename = newImg.NewFilename;
                            //sThumbFileName = newFilename;

                            FileSaved = true;
                            if (FileSaved)
                            {
                                sThumbFileName = sThumbFileName.ToString().Trim();
                                //Vconnect.Common.Utility.CopyImage.Sync(webconfigSourceImagesRootPath + "uploads", webconfigDestinationImagesRootPath + "uploads");
                                //Vconnect.Common.Utility.CopyImage.Sync(webconfigSourceImagesRootPath + "businessservice", webconfigDestinationImagesRootPath + "businessservice");
                                ImageConverter converter = new ImageConverter();
                                System.Drawing.Image uploadImage = System.Drawing.Image.FromStream(uplPhoto.InputStream);
                                var temp = (byte[])converter.ConvertTo(uploadImage, typeof(byte[]));
                                Utility.ByteCloudUploadImages(temp, sThumbFileName, "vcsites/vcimages/resource/uploads/businessservice/");
                                ViewBag.messageE = "";
                            }
                            else
                            {
                                sThumbFileName = "";
                                ViewBag.message = null;
                                ViewBag.messageE = "Some Error has occured please upload image again";
                                return EditServices(containtid, bid, bname);

                            }



                        }
                        else
                        {
                            sThumbFileName = "";
                            ViewBag.message = null;
                            ViewBag.messageE = "only .gif, .jpg, .png, .jpeg allowed";
                            return EditServices(containtid, bid, bname);

                        }
                    }
                    else
                    {
                        sThumbFileName = "";
                        ViewBag.message = null;
                        ViewBag.messageE = "File size of image is too large. Maximum file size permitted is 5 MB";
                        return EditServices(containtid, bid, bname);

                    }
                }
                BoDashboardModel obj = new BoDashboardModel();
                int validservicename = obj.getvalidProductservice(2, servicename);
                if (validservicename != 0)
                {
                    if (uplPhoto != null)
                    {
                        int sevicenameresult = obj.updateservices(containtid, businessid, servicename, servicedetails, pricerange, "uploads/businessservice/" + sThumbFileName, bname);
                        if (sevicenameresult == 3)
                        {
                            TempData["alertMsg"] = null;
                            TempData["alertMsg1"]  = "Service updated successfully. Service image will be displayed on webite after approval from Administrator";
                            return GetBuisnessServicesList(bid, bname);
                        }
                        if (sevicenameresult == 1)
                        {
                            ViewBag.message = null;
                            ViewBag.messageE = "Some error Occurs";
                        }
                        if (sevicenameresult == 2)
                        {
                            ViewBag.message = null;
                            ViewBag.messageE = "Service Name Already Exists,Please try with another Name";
                        }
                    }
                    else
                    {
                        int sevicenameresult = obj.updateservices(containtid, businessid, servicename, servicedetails, pricerange, "uploads/businessservice/" + sThumbFileName, bname);
                        if (sevicenameresult == 3)
                        {
                            TempData["alertMsg"] = null;
                            TempData["alertMsg1"] = "Service updated successfully";
                            return GetBuisnessServicesList(bid, bname);
                        }
                        if (sevicenameresult == 1)
                        {
                            ViewBag.message = null;
                            ViewBag.messageE = "Some error Occurs";
                        }
                        if (sevicenameresult == 2)
                        {
                            ViewBag.message = null;
                            ViewBag.messageE = "Service Name Already Exists,Please try with another Name";
                        }
                    }
                    obj._GetAllServices = obj.getAllServicesList(businessid, containtid);

                }
                return EditServices(containtid, bid, bname);
            }
            else
            {
                //Session.Add("Successcontroller","BoDashboardWeb");
                //Session.Add("Successaction","Index");
                //  return RedirectToAction("Login", "Account", new { rturl = "BoDashboardWeb/Index/" });
                return Redirect("/login?rturl=BoDashboardWeb/EditServices");
            }
        }
        //For Deleting the services
        public ActionResult DeleteServices(string cid, string bid, string bname)
        {
            BoDashboardModel obj = new BoDashboardModel();
            string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();
            string businessid = "0";
            obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
            int result = obj.DeleteSercices(businessid, cid, SV_VCUserContentID);
            if (result == 1)
            {
                TempData["alertMsg1"] = "Service Deleted Successfully";
            }
            else
            {
                TempData["alertMsg"] = "Service Not Deleted Successfully";
            }
            return RedirectToAction("GetBuisnessServicesList", "BoDashboardWeb", new { bid=bid,bname=bname});
        }




        #endregion


        ////
        public void getBusinessdata(string bid,string bname, BoDashboardModel obj)
        {
        string SV_VCUserContentID = objUS.SV_VCUserContentID.ToString();

        if (obj == null) obj = new BoDashboardModel();

                if (bid != null)
                {
                    obj.BusinessID = bid;
                    obj.BusinessName = bname;
                    obj.BusinessName = Regex.Replace(obj.BusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }
                else
                {

                }
                if (obj.DefaultBusinessID == null || obj.DefaultBusinessID.ToString().Length <= 0)
                {
                    string[] arrayr;
                    string result = Getdefaultbusinessdata(SV_VCUserContentID);
                    arrayr = result.Split(',');
                    obj.DefaultBusinessID = arrayr[0];
                    obj.DefaultBusinessName = arrayr[1];
                    obj.DefaultBusinessName = Regex.Replace(obj.DefaultBusinessName, @"([^a-zA-Z0-9_]|^\s)", "-");

                }

                obj.ProfileCompletionvalue = obj.GetProfileCompletion(obj.BusinessID);
                obj._bodashboardbannerlist = BodashboardbannerResultFunc(3);
                ViewBag.bodbanner = obj;
                ViewBag.boroothpath = obj.RootPath;
                obj.Pagename = "BoDashboardWeb/AddBusinessImages";
                obj._claimedbusinesslist = getbusiness(SV_VCUserContentID);
                string[] arraydata;
                string results = GetleftMenudata(obj.BusinessID);
                arraydata = results.Split(',');
                obj.membership = arraydata[0];
                obj.ProfileImage = arraydata[2];
                obj.CompanyLogo = arraydata[1];
                obj.Pagename = "BusinessImagesAdd";

      }  /////
    }

}
