﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Vconnect.Common;
using Vconnect.Enums;
using Vconnect.Mapping.ListingWEB;
using Vconnect.Models;


//using VConnect.Settings.Enums; 

namespace Vconnect.Controllers
{

    public class SearchListWEBController : Controller
    {
        //
        // GET: /SearchListWAP/
        Int32 ContentId = 0;
        public string previousURL = string.Empty;
        public string sourceURL = string.Empty;
        string AbsoluteURI = string.Empty;
        UserSession objUS = new UserSession();
        public ActionResult Index()
        {
            return View();
        }



        [HttpGet]
        public ActionResult SearchWEB(int? page, int? Sort, string tagname, string loc, int? catid)
        {
            if (!string.IsNullOrEmpty(loc) && loc.Contains(" "))
            {
                Response.Clear(); Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", Request.Url.AbsoluteUri.ToString().ToLower().Replace(" ", "_").Replace(' ', '_').Replace("%20", "_")); Response.End();
                return null;
            }            
           if (Request.Url.AbsoluteUri.ToString() != Request.Url.AbsoluteUri.ToString().ToLower())
            {

                if (Request.Url.AbsoluteUri.ToString().IndexOf("?") != -1)
                {
                 
                    if (Request.Url.AbsoluteUri.ToString().Substring(0, Request.Url.AbsoluteUri.ToString().IndexOf("?") - 1) != Request.Url.AbsoluteUri.ToString().Substring(0, Request.Url.AbsoluteUri.ToString().IndexOf("?") - 1).ToLower())
                    {
                        Response.Clear(); Response.Status = "301 Moved Permanently";
                        Response.AddHeader("Location", Request.Url.AbsoluteUri.ToString().ToLower()); Response.End();
                        return null;
                        //return new HttpStatusCodeResult(301, "301 Moved Permanently");
                    }
                }              
                else
                {
                    Response.Clear(); Response.Status = "301 Moved Permanently";
                    Response.AddHeader("Location", Request.Url.AbsoluteUri.ToString().ToLower()); Response.End();
                    return null;
                    //return new HttpStatusCodeResult(301, "301 Moved Permanently");
                }
                //Response.Clear(); Response.Status = "301 Moved Permanently";
                //Response.AddHeader("Location", Request.Url.AbsoluteUri.ToString().ToLower()); Response.End();
                //return new HttpStatusCodeResult(301, "301 Moved Permanently");
            }
           string vcSelectedLocId = string.Empty;
           string vcSelectedLocation = string.Empty;
            string newUrl = string.Empty;            
            try
            {
                newUrl = urlRedirection();
                if (!string.IsNullOrEmpty(newUrl))
                {
                    Response.Clear(); Response.Status = "301 Moved Permanently";
                    Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();
                    return null;//return new HttpStatusCodeResult(301, "301 Moved Permanently");
                }
                SearchListWebModel searchListWebModel = new SearchListWebModel();
                //string AbsoluteURI = string.Empty;
                if (System.Web.HttpContext.Current.Request.Url.AbsoluteUri == null || System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString().Trim() == "")
                {
                    AbsoluteURI = "-";
                }
                else
                {
                    AbsoluteURI = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString().Replace("~", "");
                }
                AssignHTTPVariables(AbsoluteURI);
                searchListWebModel.contentId = string.IsNullOrEmpty(Convert.ToString(catid)) ? 0 : Convert.ToInt32(catid);
                searchListWebModel.StateOptions = Utility.GetSelectList();
                int Page = 1;
                if (string.IsNullOrEmpty(Request.QueryString["sq"]) && string.IsNullOrEmpty(Request.QueryString["sl"]) && string.IsNullOrEmpty(loc) && string.IsNullOrEmpty(tagname))
                {
                    return RedirectPermanent("/searchnotfound");
                }
                if (Request.QueryString["sq"] == null)
                {
                    searchListWebModel.metaSearchLoc = loc;
                    if (Request.Cookies["vcSearchLocation"] != null && Request.Cookies["vcSearchLocation"].Value.Contains(',') && previousURL.IndexOf("vconnect")!=-1)
                    {
                        string[] strSrchLoc_Id = Request.Cookies["vcSearchLocation"].Value.Split(',');
                        if (strSrchLoc_Id[0].Split('-').Count() > 2)
                        {
                            searchListWebModel.SearchLocation = strSrchLoc_Id[0].Split('-')[2].Replace("_", " ");
                            searchListWebModel.locid = Convert.ToInt32(strSrchLoc_Id[1]);
                        }
                        else if (strSrchLoc_Id[0].Contains('-'))
                        {
                            searchListWebModel.SearchLocation = strSrchLoc_Id[0].Split('-')[1].Replace("_", " ");
                        }
                        else
                        {
                            searchListWebModel.SearchLocation = strSrchLoc_Id[0].Split('-')[0].Replace("_", " ");
                        }
                        
                        searchListWebModel.metaSearchLoc = strSrchLoc_Id[0];
                        
                    }
                    else
                    {
                        searchListWebModel.SearchLocation = loc.Replace("_", " ").Replace("-", ",");
                        searchListWebModel.locid = 0;
                       
                    }
                   //27/05/14 searchListWebModel.metaSearchLoc = loc;
                    searchListWebModel.SearchText = tagname;

                    //if (loc.IndexOf("-") != -1)
                    //{                        
                    //    loc = loc.Substring(loc.IndexOf("-") + 1, loc.Length - loc.IndexOf("-") - 1).Replace("_", " ").Replace("_", " ");
                    //}
                   //27/05/14 searchListWebModel.SearchLocation = loc.Replace("_", " ").Replace("-", ",");
                    searchListWebModel.pageNum = 1;
                    string hostHeader = string.Empty;
                    hostHeader = Request.Url.AbsoluteUri.ToString();
                    if (hostHeader.ToLower().IndexOf("?") != -1)
                    {
                        int sURLstart = hostHeader.ToString().IndexOf("?");
                        hostHeader = (hostHeader.ToString().Substring(0, sURLstart)).ToString().Trim();

                    }
                    
                    if (hostHeader.IndexOf("vendors-search_s") != -1)
                    {
                        searchListWebModel.type = 2;
                    }
                    else if (hostHeader.IndexOf("vendors-search_p") != -1)
                    {
                        searchListWebModel.type = 1;
                    }
                    else if (hostHeader.IndexOf("search_br") != -1)
                    {
                        searchListWebModel.type = 3;
                    }
                    else if (hostHeader.IndexOf("_c") != -1)
                    {
                        searchListWebModel.type = 4;
                    }
                }
                else
                {
                    if (Request.QueryString["sl"] != null)
                    {
                        loc = Request.QueryString["sl"].ToString();
                    }
                    //else if (Request.QueryString["amp;sl"] != null)
                    //{
                    //    loc = Request.QueryString["amp;sl"].ToString();
                    //}
                    else
                    {
                        loc = "lagos";
                    }
                    searchListWebModel.metaSearchLoc = loc;                   
                    searchListWebModel.SearchLocation = loc.Replace("_", " ").Replace("-", ",");
                    //if (Request.QueryString["sid"] != null)
                    //{
                    //    searchListWebModel.locid = Convert.ToInt32(Request.QueryString["sid"].ToString());
                    //}
                    //else if (Request.QueryString["amp;sid"] != null)
                    //{
                    //    searchListWebModel.locid = Convert.ToInt32(Request.QueryString["amp;sid"].ToString());
                    //}
                    //else
                    //{
                    //    searchListWebModel.locid = 0;
                    //}

                    searchListWebModel.locid = string.IsNullOrEmpty(Request.QueryString["sid"]) ? 0 : Convert.ToInt32(Request.QueryString["sid"].ToString());
                    searchListWebModel.SearchText = Request.QueryString["sq"].ToString().Replace("_", " ").Replace("-", " ");
                    searchListWebModel.type = 0;
                    searchListWebModel.pageNum = string.IsNullOrEmpty(Request.QueryString["pg"]) ? 1 : Convert.ToInt16(Request.QueryString["pg"].ToString());
                    Page = string.IsNullOrEmpty(Request.QueryString["pg"]) ? 1 : Convert.ToInt16(Request.QueryString["pg"].ToString());

                }
                try
                {
                    
                    if (Request.Cookies["vcSelectedLocId"] != null) vcSelectedLocId = Request.Cookies["vcSelectedLocId"].Value.ToString();
                    if (Request.Cookies["vcSelectedLocation"] != null) vcSelectedLocation = Request.Cookies["vcSelectedLocation"].Value.ToString();

                    if (loc.IndexOf("-") != -1)
                    {
                        loc = loc.Substring(loc.IndexOf("-") + 1, loc.Length - loc.IndexOf("-") - 1).Replace("_", " ").Replace("_", " ");
                    }

                    Utility.CreateCokkies(vcSelectedLocId, vcSelectedLocation, searchListWebModel.SearchText, searchListWebModel.SearchLocation == loc ? loc : searchListWebModel.SearchLocation);

                }
                catch
                {
                }


                if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))

                    ContentId = Convert.ToInt32(objUS.SV_VCUserContentID);
                else
                    ContentId = 0;
                
                SearchWEB(searchListWebModel, Page, searchListWebModel.type);
                if (Request.Cookies.AllKeys.Contains("vcSearchText") && Request.Cookies["vcSearchText"] != null)
                {
                    Request.Cookies.Remove("vcSearchText");
                }
                HttpCookie vcSearchText = new HttpCookie("vcSearchText");
                vcSearchText.Value = searchListWebModel.SearchText;
                vcSearchText.Expires = DateTime.Now.AddDays(1);
                Response.Cookies.Add(vcSearchText);

                vcSelectedLocId = string.Empty;
                vcSelectedLocation = string.Empty;
                if (Request.Cookies["vcSelectedLocId"] != null) vcSelectedLocId = Request.Cookies["vcSelectedLocId"].Value.ToString();
                if (Request.Cookies["vcSelectedLocation"] != null) vcSelectedLocation = Request.Cookies["vcSelectedLocation"].Value.ToString();

                if (loc.IndexOf("-") != -1)
                {
                    loc = loc.Substring(loc.IndexOf("-") + 1, loc.Length - loc.IndexOf("-") - 1).Replace("_", " ").Replace("_", " ");
                }

                Utility.CreateCokkies(vcSelectedLocId, vcSelectedLocation, searchListWebModel.SearchText, searchListWebModel.SearchLocation == loc ? loc : searchListWebModel.SearchLocation);
                //Utility.CreateCokkies(vcSelectedLocId, vcSelectedLocation, searchListWebModel.SearchText, searchListWebModel.SearchLocation == loc ? loc : searchListWebModel.SearchLocation);
                //if result not found in any category then redirect qsearch
                #region commented for Display 404/serchnotfound and not redirecting to qserch any more
                //////if (searchListWebModel != null && searchListWebModel.listSearchResultWEB.BusinessCount.FirstOrDefault().totalbiz < 1 && searchListWebModel.type > 0)
                //////{
                //////    // searchListWebModel.type = 0;
                //////    SearchWEB(searchListWebModel, Page, 0);



                //////}
                #endregion
                //ona-ara/list-of-design-of-unisex-wears-qsearch(wrong old url dor city)
                //if (searchListWebModel.listSearchResultWEB.BusinessCount.FirstOrDefault().totalbiz == 0 && searchListWebModel.type ==0)
                //{
                //    string URL = System.Web.HttpContext.Current.Request.RawUrl.ToString();
                //    int sURLstart = 0;
                //    if (URL.ToLower().IndexOf("list-of") != -1)
                //    {
                //        sURLstart = URL.ToLower().IndexOf("list-of");
                //    }
                //    URL = (URL.Substring(0, sURLstart)).ToString().Replace("/list-of-", "").Replace("list-of-", "").Trim();
                //    string[] array;
                //    // string newUrl = string.Empty;
                //    array = URL.Split('-');
                //    string loc1 = array[0].Replace("/", "");
                //    string loc2 = array[1].Replace("/", "");
                //    string loc3 = loc1 + "_" + loc2;
                //    searchListWebModel.SearchLocation = loc3;


                //    string vcSelectedLocId = string.Empty;
                //    string vcSelectedLocation = string.Empty;
                //    if (Request.Cookies["vcSelectedLocId"] != null) vcSelectedLocId = Request.Cookies["vcSelectedLocId"].Value.ToString();
                //    if (Request.Cookies["vcSelectedLocation"] != null) vcSelectedLocation = Request.Cookies["vcSelectedLocation"].Value.ToString();

                //    Utility.CreateCokkies(vcSelectedLocId, vcSelectedLocation, searchListWebModel.SearchText, loc3);
                //    SearchWEB(searchListWebModel, Page, 0);
                //}
                //if (searchListWebModel.listSearchResultWEB.BusinessCount.FirstOrDefault().totalcount < 1)
                //{

                //    SearchNotFoundLog(Convert.ToInt32(searchListWebModel.type), searchListWebModel.SearchLocation.ToString(), searchListWebModel.SearchText.ToString());
                //    return Redirect("/searchnotfound");
                //}
                //else
                //{

                //    AddSearchPageLog(searchListWebModel.type, searchListWebModel.SearchText.ToString(), searchListWebModel.SearchLocation.ToString(), searchListWebModel.listSearchResultWEB.BusinessCount.FirstOrDefault().totalbiz.ToString(),Utility.GetIpAddress(),Utility.GetReversedns(Utility.GetIpAddress()), Utility.GetDomain(), Convert.ToInt32(ContentId));

                //    //Thread thrd_AddSearchPageLog = new Thread(delegate() { AddSearchPageLog(searchListWebModel.type, searchListWebModel.SearchText.ToString(), searchListWebModel.SearchLocation.ToString(), searchListWebModel.listSearchResultWEB.BusinessCount.FirstOrDefault().totalbiz.ToString(), MyGlobalVariables.GetIpAddress, MyGlobalVariables.GetReverseDns, MyGlobalVariables.GetDomain, Convert.ToInt32(ContentId)); });
                //    //thrd_AddSearchPageLog.Start();
                //    //thrd_AddSearchPageLog.Join();
                //}
                Response.Clear(); //Response.StatusCode = 404;
                Response.Status = "200 Ok";
                if (searchListWebModel.listSearchResultWEB.BusinessCount.FirstOrDefault().totalcount < 1)
                {
                    SearchNotFoundLog(Convert.ToInt32(searchListWebModel.type), searchListWebModel.SearchLocation.ToString(), searchListWebModel.SearchText.ToString());
                    if (searchListWebModel.type > 0)
                    {
                        if (searchListWebModel.productServiceURL != null && !string.IsNullOrEmpty(searchListWebModel.productServiceURL.url))
                        {
                            Response.Clear(); Response.Status = "301 Moved Permanently";
                            Response.AddHeader("Location","http://www.vconnect.com/" + searchListWebModel.productServiceURL.url); Response.End();
                            return null; //return new HttpStatusCodeResult(301, "301 Moved Permanently");
                         //   return Redirect("/" + searchListWebModel.productServiceURL.url);
                        }
                        else
                        {
                            //Response.Clear(); Response.StatusCode = 404;
                            //Response.Status = "404 NotFound";
                            Response.Clear(); Response.StatusCode = 404;
                            Response.Status = "404 NotFound";
                            return View("../Static/404");
                            //return Redirect("/404");
                        }                        
                    }
                    else
                    {
                        //Response.Clear(); //Response.StatusCode = 404;
                        //Response.Status = "404 NotFound";
                        //return Redirect("/searchnotfound");
                        return View("../Shared/SearchNotFound");
                    }
                }
                else
                {
                    //Response.Clear(); //Response.StatusCode = 404;
                    //Response.Status = "200 Ok";
                    //AddSearchPageLog(searchListWebModel.type, searchListWebModel.SearchText.ToString(), searchListWebModel.SearchLocation.ToString(), searchListWebModel.listSearchResultWEB.BusinessCount.FirstOrDefault().totalbiz.ToString(), Utility.GetIpAddress(), Utility.GetReversedns(Utility.GetIpAddress()), Utility.GetDomain(), Convert.ToInt32(ContentId));
                    
                    //Thread thrd_AddSearchPageLog = new Thread(delegate() { AddSearchPageLog(searchListWebModel.type, searchListWebModel.SearchText.ToString(), searchListWebModel.SearchLocation.ToString(), searchListWebModel.listSearchResultWEB.BusinessCount.FirstOrDefault().totalbiz.ToString(), MyGlobalVariables.GetIpAddress, MyGlobalVariables.GetReverseDns, MyGlobalVariables.GetDomain, Convert.ToInt32(ContentId)); });
                    //thrd_AddSearchPageLog.Start();
                    //thrd_AddSearchPageLog.Join();
                }


                return View("SearchWEB");
            }

            catch (Exception ex)
            {
                log.LogMe(ex);
                //Response.Clear(); //Response.StatusCode = 404;
                //Response.Status = "404 NotFound";
                return RedirectPermanent("/searchnotfound");
                //return View("../Shared/SearchNotFound");
            }
            finally
            {

            }

        }

        #region URL issues and redirection
        public string urlRedirection()
        {
            string strurlRedirection = string.Empty;
            try
            {
              //  string websiteRawPath = "http://vconnect.com";//ConfigurationManager.AppSettings["WebsiteRawPath"].ToString();               
                string websiteRawPath = ConfigurationManager.AppSettings["WebsiteRawPath"].ToString();    
                //added by rashmi on 25-03-2014
                if (System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("/list-of-list-of-") != -1 ||
                    System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("/list-oflist-of-") != -1)
                {
                    strurlRedirection = websiteRawPath + System.Web.HttpContext.Current.Request.RawUrl.ToLower().Replace("/list-of-list-of-", "/list-of-");
                    //return strurlRedirection;

                }

                 //http://localhost:1202/code/lagos/list-of-jewelries_c46174__-page10 TO http://localhost:1202/code/lagos/list-of-jewelries_c46174_-page10
                else if ((System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("__") != -1 ||
                    System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("_-") != -1))
                {
                    string newUrl = System.Web.HttpContext.Current.Request.RawUrl.ToLower();
                    newUrl = websiteRawPath + newUrl.Replace("__", "_");
                    strurlRedirection = newUrl.Replace("_-", "-");
                    //return strurlRedirection;
                }
                //http://localhost:1202/code/pages/abuja-asokoro/list-of-churches_c515
                else if (System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("pages/") != -1 &&
                    System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("list-of-") != -1)
                {
                    string newUrl = System.Web.HttpContext.Current.Request.RawUrl.ToLower();
                    strurlRedirection = websiteRawPath + newUrl.Replace("pages/", "");
                    // return strurlRedirection;

                }
                ////http://localhost:60140/lagos/list-of-legal-practitioners-in-lagos-sortby=reviews_c5495
                //else if (System.Web.HttpContext.Current.Request.RawUrl.ToLower().Substring(System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("/")).IndexOf("=") != -1 &&
                //    System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("?") == -1)
                //{
                //    //string newUrl = System.Web.HttpContext.Current.Request.RawUrl.ToLower().Substring(0, System.Web.HttpContext.Current.Request.RawUrl.ToLower().LastIndexOf("/")) + System.Web.HttpContext.Current.Request.RawUrl.ToLower().Substring(System.Web.HttpContext.Current.Request.RawUrl.ToLower().LastIndexOf("/")).Replace("=", "-");
                //    //strurlRedirection = websiteRawPath + newUrl;
                //    strurlRedirection = websiteRawPath + System.Web.HttpContext.Current.Request.RawUrl.ToLower().Replace("=", "-");

                //}
                else
                {
                    strurlRedirection = string.Empty;
                    //return strurlRedirection;
                }
                return strurlRedirection;
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                return strurlRedirection;
            }
            //return strurlRedirection;

        }
        #endregion

        #region  New Lead Moddule         
        [HttpPost]
        public ActionResult SearchWEB(SearchListWebModel searchListWebModel, int? Page, int? type)
        {           
            List<SearchListBusinessWEB> searchListBuisnes = new List<SearchListBusinessWEB>();
            List<BusinessCountWEB> totalBusinessCount = new List<BusinessCountWEB>();
            List<SearchListPremiumBusinessWEB> searchListPremiumBusiness = new List<SearchListPremiumBusinessWEB>();
            List<ListSearchResultWEB> listResSetTabParent = new List<ListSearchResultWEB>();
            List<LocationFilter> LocationFilter = new List<LocationFilter>();
            List<KeywordFilter> KeywordFilter = new List<KeywordFilter>();
            List<RelatedSearch> RelatedSearch = new List<RelatedSearch>();
            List<LeadQuestionaree> leadQuestionaree = new List<LeadQuestionaree>();
            ListSearchResultWEB listResultSetTableObj = new ListSearchResultWEB();
            ProductServiceURL productServiceURL = new ProductServiceURL();
            PageInfo pageInfo = new PageInfo();
            if (Request.Url.AbsoluteUri.ToString().IndexOf("returnval") != -1)
            {
                if (Request.QueryString["returnval"].ToString() == "save")
                {
                    Int64 bizId = Convert.ToInt64(Request.QueryString["bizID"].ToString());
                    string str = Favourite(bizId);

                }
                else
                {
                    Int64 bizId = Convert.ToInt64(Request.QueryString["bizID"].ToString());
                    string str = Like(bizId);

                }
            }
            searchListWebModel.rowsPerPage = 10;

            if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))

                ContentId = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
            else
                ContentId = 0;
            searchListWebModel.locid = string.IsNullOrEmpty(Request.QueryString["sid"]) ? 0 : Convert.ToInt32(Request.QueryString["sid"].ToString());
            searchListWebModel.sort = searchListWebModel.sort != null && searchListWebModel.sort > 0 ? searchListWebModel.sort : 1;
            using (var db = new VconnectDBContext29())
            {

                var cmd = db.Database.Connection.CreateCommand();
                if (type.HasValue)
                {
                    searchListWebModel.type = type.Value;
                    cmd.CommandText = type.Value < 1 ? "[dbo].[prc_get_search]" : type.Value == 4 ? "[dbo].[getwebsearchinpcat]" : "[dbo].[GetWebSearchInpProSer]";
                    //cmd.CommandText = type.Value < 1 ? "[dbo].[prc_get_search]" : type.Value == 4 ? "[dbo].[prc_get_searchbycategory_beta]" : "[dbo].[prc_get_searchbytype_201309test123]";
                    
                }
                else
                {
                    cmd.CommandText = "[dbo].[prc_get_search]";
                    searchListWebModel.type = 0;
                }
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@keyword", searchListWebModel.SearchText.ToString().Replace("-", " ").Replace("  ", " ")));
                cmd.Parameters.Add(new SqlParameter("@location", searchListWebModel.SearchLocation.ToString()));
                cmd.Parameters.Add(new SqlParameter("@pageNum", searchListWebModel.pageNum));
                cmd.Parameters.Add(new SqlParameter("@rowsPerPage", searchListWebModel.rowsPerPage));
                cmd.Parameters.Add(new SqlParameter("@sort", searchListWebModel.sort));
                cmd.Parameters.Add(new SqlParameter("@type", searchListWebModel.type));
                cmd.Parameters.Add(new SqlParameter("@userid", ContentId));
                cmd.Parameters.Add(new SqlParameter("@locationid", Convert.ToInt32(searchListWebModel.locid)));

                if (type.Value > 0)
                {
                    cmd.Parameters.Add(new SqlParameter("@contentid", Convert.ToInt32(searchListWebModel.contentId)));
                }

                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();
                    searchListBuisnes = ((IObjectContextAdapter)db).ObjectContext.Translate<SearchListBusinessWEB>(reader).ToList();

                    // Move to second result set and read Posts
                    reader.NextResult();

                    totalBusinessCount = ((IObjectContextAdapter)db).ObjectContext.Translate<BusinessCountWEB>(reader).ToList();
                    searchListWebModel.totalPages = (int)Math.Ceiling((double)totalBusinessCount[0].totalcount.Value / searchListWebModel.rowsPerPage);


                    reader.NextResult();

                    searchListPremiumBusiness = ((IObjectContextAdapter)db).ObjectContext.Translate<SearchListPremiumBusinessWEB>(reader).ToList();

                    listResultSetTableObj.SearchListBusiness = searchListBuisnes;
                    listResultSetTableObj.BusinessCount = totalBusinessCount;
                    listResultSetTableObj.SearchListPremiumBusiness = searchListPremiumBusiness;


                    listResSetTabParent.Add(listResultSetTableObj);


                    searchListWebModel.SearchText = searchListWebModel.SearchText.ToString();
                    searchListWebModel.SearchLocation = searchListWebModel.SearchLocation.ToString();
                    searchListWebModel.listSearchResultWEB = listResultSetTableObj;

                    reader.NextResult();
                    searchListWebModel.LocationFilter = ((IObjectContextAdapter)db).ObjectContext.Translate<LocationFilter>(reader).ToList();

                    ViewBag.locationcount = (searchListWebModel.LocationFilter.Count <= 0) ? 0 : searchListWebModel.LocationFilter.Count;
                    reader.NextResult();
                    searchListWebModel.KeywordFilter = ((IObjectContextAdapter)db).ObjectContext.Translate<KeywordFilter>(reader).ToList();
                    reader.NextResult();
                    searchListWebModel.RelatedSearch = ((IObjectContextAdapter)db).ObjectContext.Translate<RelatedSearch>(reader).ToList();
                    reader.NextResult();
                    searchListWebModel.leadQuestionaree = ((IObjectContextAdapter)db).ObjectContext.Translate<LeadQuestionaree>(reader).ToList();
                    //searchListWebModel.leadWord = ((IObjectContextAdapter)db).ObjectContext.Translate<LeadWord>(reader).FirstOrDefault();
                    reader.NextResult();
                    searchListWebModel.productServiceURL = ((IObjectContextAdapter)db).ObjectContext.Translate<ProductServiceURL>(reader).FirstOrDefault();
                   
                    //if (type > 0)
                    //{
                    //    reader.NextResult();
                    //    reader.Read();
                    //    searchListWebModel.SearchText = ((IDataRecord)reader)[0].ToString();
                    //    reader.Close();
                    //}
                    if (type > 0)
                    {
                        reader.NextResult();
                        searchListWebModel.pageInfo = ((IObjectContextAdapter)db).ObjectContext.Translate<PageInfo>(reader).FirstOrDefault();
                        if (searchListWebModel.pageInfo != null && !string.IsNullOrEmpty(searchListWebModel.pageInfo.keyword))
                        { searchListWebModel.SearchText = searchListWebModel.pageInfo.keyword; }
                        if (searchListWebModel.pageInfo != null && !string.IsNullOrEmpty(searchListWebModel.pageInfo.locationtag))
                        {
                            searchListWebModel.metaSearchLoc = searchListWebModel.pageInfo.locationtag; 
                        }
                    }
                    ViewBag.keywordcount = (searchListWebModel.KeywordFilter.Count <= 0) ? 0 : searchListWebModel.KeywordFilter.Count;

                    string bizid = string.Empty;
                    foreach (var item in searchListBuisnes)
                    {
                        bizid = bizid + "," + item.businessid;
                    }
                    foreach (var item in searchListPremiumBusiness)
                    {
                        bizid = bizid + "," + item.businessid;
                    }
                }

                catch (Exception exp)
                {
                    log.LogMe(exp);
                }

                finally
                {
                    db.Database.Connection.Close();
                }

            }

            return View("SearchWEB", searchListWebModel);

        }
        #endregion

        #region Old Lead Module
        //[HttpPost]
        //public ActionResult SearchWEB(SearchListWebModel searchListWebModel, int? Page, int? type)
        //{
        //    List<SearchListBusinessWEB> searchListBuisnes = new List<SearchListBusinessWEB>();
        //    List<BusinessCountWEB> totalBusinessCount = new List<BusinessCountWEB>();
        //    List<SearchListPremiumBusinessWEB> searchListPremiumBusiness = new List<SearchListPremiumBusinessWEB>();
        //    List<ListSearchResultWEB> listResSetTabParent = new List<ListSearchResultWEB>();
        //    List<LocationFilter> LocationFilter = new List<LocationFilter>();
        //    List<KeywordFilter> KeywordFilter = new List<KeywordFilter>();
        //    List<RelatedSearch> RelatedSearch = new List<RelatedSearch>();
        //    ListSearchResultWEB listResultSetTableObj = new ListSearchResultWEB();
        //    ProductServiceURL productServiceURL = new ProductServiceURL();

        //    if (Request.Url.AbsoluteUri.ToString().IndexOf("returnval") != -1)
        //    {
        //        if (Request.QueryString["returnval"].ToString() == "save")
        //        {
        //            Int64 bizId = Convert.ToInt64(Request.QueryString["bizID"].ToString());
        //            string str = Favourite(bizId);

        //        }
        //        else
        //        {
        //            Int64 bizId = Convert.ToInt64(Request.QueryString["bizID"].ToString());
        //            string str = Like(bizId);

        //        }
        //    }
        //    searchListWebModel.rowsPerPage = 10;

        //    if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))

        //        ContentId = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
        //    else
        //        ContentId = 0;
        //    searchListWebModel.locid = string.IsNullOrEmpty(Request.QueryString["sid"]) ? 0 : Convert.ToInt32(Request.QueryString["sid"].ToString());
        //    searchListWebModel.sort = searchListWebModel.sort != null && searchListWebModel.sort > 0 ? searchListWebModel.sort : 1;
        //    using (var db = new VconnectDBContext29())
        //    {

        //        var cmd = db.Database.Connection.CreateCommand();
        //        if (type.HasValue)
        //        {
        //            searchListWebModel.type = type.Value;
        //            cmd.CommandText = type.Value < 1 ? "[dbo].[prc_get_search]" : type.Value == 4 ? "[dbo].[prc_get_searchbycategory_beta]" : "[dbo].[prc_get_searchbytype_201309test123]";
        //        }
        //        else
        //        {
        //            cmd.CommandText = "[dbo].[prc_get_search]";
        //            searchListWebModel.type = 0;
        //        }
        //        cmd.CommandTimeout = 600;
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        //Add parameters to command object
        //        cmd.Parameters.Add(new SqlParameter("@keyword", searchListWebModel.SearchText.ToString().Replace("-", " ").Replace("  ", " ")));
        //        cmd.Parameters.Add(new SqlParameter("@location", searchListWebModel.SearchLocation.ToString()));
        //        cmd.Parameters.Add(new SqlParameter("@pageNum", searchListWebModel.pageNum));
        //        cmd.Parameters.Add(new SqlParameter("@rowsPerPage", searchListWebModel.rowsPerPage));
        //        cmd.Parameters.Add(new SqlParameter("@sort", searchListWebModel.sort));
        //        cmd.Parameters.Add(new SqlParameter("@type", searchListWebModel.type));
        //        cmd.Parameters.Add(new SqlParameter("@userid", ContentId));
        //        cmd.Parameters.Add(new SqlParameter("@locationid", Convert.ToInt32(searchListWebModel.locid)));

        //        if (type.Value > 0)
        //        {
        //            cmd.Parameters.Add(new SqlParameter("@contentid", Convert.ToInt32(searchListWebModel.contentId)));
        //        }

        //        try
        //        {
        //            var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
        //            connection.Open();

        //            var reader = cmd.ExecuteReader();
        //            searchListBuisnes = ((IObjectContextAdapter)db).ObjectContext.Translate<SearchListBusinessWEB>(reader).ToList();

        //            // Move to second result set and read Posts
        //            reader.NextResult();

        //            totalBusinessCount = ((IObjectContextAdapter)db).ObjectContext.Translate<BusinessCountWEB>(reader).ToList();
        //            searchListWebModel.totalPages = (int)Math.Ceiling((double)totalBusinessCount[0].totalcount.Value / searchListWebModel.rowsPerPage);


        //            reader.NextResult();

        //            searchListPremiumBusiness = ((IObjectContextAdapter)db).ObjectContext.Translate<SearchListPremiumBusinessWEB>(reader).ToList();

        //            listResultSetTableObj.SearchListBusiness = searchListBuisnes;
        //            listResultSetTableObj.BusinessCount = totalBusinessCount;
        //            listResultSetTableObj.SearchListPremiumBusiness = searchListPremiumBusiness;


        //            listResSetTabParent.Add(listResultSetTableObj);


        //            searchListWebModel.SearchText = searchListWebModel.SearchText.ToString();
        //            searchListWebModel.SearchLocation = searchListWebModel.SearchLocation.ToString();
        //            searchListWebModel.listSearchResultWEB = listResultSetTableObj;

        //            reader.NextResult();
        //            searchListWebModel.LocationFilter = ((IObjectContextAdapter)db).ObjectContext.Translate<LocationFilter>(reader).ToList();

        //            ViewBag.locationcount = (searchListWebModel.LocationFilter.Count <= 0) ? 0 : searchListWebModel.LocationFilter.Count;
        //            reader.NextResult();
        //            searchListWebModel.KeywordFilter = ((IObjectContextAdapter)db).ObjectContext.Translate<KeywordFilter>(reader).ToList();
        //            reader.NextResult();
        //            searchListWebModel.RelatedSearch = ((IObjectContextAdapter)db).ObjectContext.Translate<RelatedSearch>(reader).ToList();
        //            reader.NextResult();
        //            searchListWebModel.leadWord = ((IObjectContextAdapter)db).ObjectContext.Translate<LeadWord>(reader).FirstOrDefault();
        //            reader.NextResult();
        //            searchListWebModel.productServiceURL = ((IObjectContextAdapter)db).ObjectContext.Translate<ProductServiceURL>(reader).FirstOrDefault();

        //            ViewBag.keywordcount = (searchListWebModel.KeywordFilter.Count <= 0) ? 0 : searchListWebModel.KeywordFilter.Count;

        //            string bizid = string.Empty;
        //            foreach (var item in searchListBuisnes)
        //            {
        //                bizid = bizid + "," + item.businessid;
        //            }
        //            foreach (var item in searchListPremiumBusiness)
        //            {
        //                bizid = bizid + "," + item.businessid;
        //            }
        //        }

        //        catch (Exception exp)
        //        {
        //            log.LogMe(exp);
        //        }

        //        finally
        //        {
        //            db.Database.Connection.Close();
        //        }

        //    }

        //    return View("SearchWEB", searchListWebModel);

        //}

        #endregion
        protected void SearchNotFoundLog(int searchtype, string location, string searchedtext)
        {
            int result = 0;
            string IPAddress = string.Empty;
            string ReverseDNS = string.Empty;
           
            IPAddress = Vconnect.MvcApplication.GetVisitorIPAddress();
            ReverseDNS = !string.IsNullOrEmpty(IPAddress) ? Vconnect.MvcApplication.GetReverseDns(IPAddress.ToString()) : "";
            if (ContentId != null)
            {
                result = log.addsearchnotfoundlog(searchedtext, location, IPAddress, searchtype.ToString(), Convert.ToInt32(ContentId), ReverseDNS);
            }
            else
            {
                result = log.addsearchnotfoundlog(searchedtext, location, IPAddress, searchtype.ToString(), 0, ReverseDNS);

            }
        }
        protected void AssignHTTPVariables(string AbsoluteURI)
        {

            if (Request == null || Request.UrlReferrer == null || Request.UrlReferrer.ToString().Trim() == "")
            {
                previousURL = "-";
            }
            else
            {
                previousURL = Request.UrlReferrer.ToString().ToLower().Trim();//.Replace("//", "~~").Replace("/", "~").Replace(".html", ".indexpage").Replace("_", "~").Replace("-", "~").Trim();
            }
            sourceURL = AbsoluteURI.ToString().ToLower().Trim();//.Replace("//", "~~").Replace("/", "~").Replace(".html", ".indexpage").Replace("_", "~").Replace("-", "~").Trim();
        }
        private List<ListSearchResultWEB> GetSearchDataWEB(int? Page, int? Type, int? RowsPerPage, Int32 catid, string SearchText, string SearchLocation, Int32 locid)
        {

            List<SearchListBusinessWEB> searchListBuisnes = new List<SearchListBusinessWEB>();
            List<BusinessCountWEB> totalBusinessCount = new List<BusinessCountWEB>();
            List<SearchListPremiumBusinessWEB> searchListPremiumBusiness = new List<SearchListPremiumBusinessWEB>();
            List<ListSearchResultWEB> listResSetTabParent = new List<ListSearchResultWEB>();
            List<LocationFilter> LocationFilter = new List<LocationFilter>();
            List<KeywordFilter> KeywordFilter = new List<KeywordFilter>();
            List<RelatedSearch> RelatedSearch = new List<RelatedSearch>();
            ListSearchResultWEB listResultSetTableObj = new ListSearchResultWEB();


            if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))

                ContentId = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
            else
                ContentId = 0;


            int currentpage = Page.HasValue ? Page.Value : 1; //Page ?? 1;
            int currenttype = Type ?? 1;
            int CurrentRowsPerPage = RowsPerPage ?? 10;

            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = Type < 1 ? "[dbo].[prc_get_search]" :Type == 4 ? "[dbo].[prc_get_searchbycategory_beta]" : "[dbo].[prc_get_searchbytype_201309test123]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@keyword", SearchText.ToString().Replace("-", " ").Replace("  ", "")));
                cmd.Parameters.Add(new SqlParameter("@location", SearchLocation.ToString()));
                cmd.Parameters.Add(new SqlParameter("@pageNum", currentpage));
                cmd.Parameters.Add(new SqlParameter("@rowsPerPage", RowsPerPage));
                cmd.Parameters.Add(new SqlParameter("@sort", 1));
                cmd.Parameters.Add(new SqlParameter("@type", currenttype));
                cmd.Parameters.Add(new SqlParameter("@userid", ContentId));
                cmd.Parameters.Add(new SqlParameter("@locationid", locid));
                if (Type > 0)
                {
                    cmd.Parameters.Add(new SqlParameter("@contentid", catid));
                }

                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();
                    searchListBuisnes = ((IObjectContextAdapter)db).ObjectContext.Translate<SearchListBusinessWEB>(reader).ToList();

                    // Move to second result set and read Posts
                    reader.NextResult();

                    totalBusinessCount = ((IObjectContextAdapter)db).ObjectContext.Translate<BusinessCountWEB>(reader).ToList();
                    reader.NextResult();


                    searchListPremiumBusiness = ((IObjectContextAdapter)db).ObjectContext.Translate<SearchListPremiumBusinessWEB>(reader).ToList();
                    reader.NextResult();

                    listResultSetTableObj.SearchListBusiness = searchListBuisnes;
                    listResultSetTableObj.BusinessCount = totalBusinessCount;
                    listResultSetTableObj.SearchListPremiumBusiness = searchListPremiumBusiness;



                    string bizid = string.Empty;
                    foreach (var item in searchListBuisnes)
                    {
                        bizid = bizid + "," + item.businessid;
                    }
                    foreach (var item in searchListPremiumBusiness)
                    {
                        bizid = bizid + "," + item.businessid;
                    }

                    listResSetTabParent.Add(listResultSetTableObj);


                }
                catch (Exception exp)
                {
                    log.LogMe(exp);
                }
                finally { db.Database.Connection.Close(); }
            }
            return listResSetTabParent;
        }
        public ActionResult BannerImpressionLog(Int64? bannerid, int? businessid, string businessname, string sourceurl, string previousurl)
        {
            try
            {
                if (bannerid.HasValue && businessid.HasValue)
                {
                    Int64 userId = 0;
                    if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                    {
                        userId = Convert.ToInt64(objUS.SV_VCUserContentID.ToString());
                    }

                    string IPAddress = string.Empty;
                    string ReverseDNS = string.Empty;

                    IPAddress = Vconnect.MvcApplication.GetVisitorIPAddress();
                    ReverseDNS = !string.IsNullOrEmpty(IPAddress) ? Vconnect.MvcApplication.GetReverseDns(IPAddress.ToString()) : "";
                    log.addbannerimpressionlog(Convert.ToString(bannerid.Value), businessid.Value, businessname, 0, IPAddress, System.Web.HttpUtility.HtmlDecode(sourceurl), System.Web.HttpUtility.HtmlDecode(previousurl), ReverseDNS);
                    return Json("logged", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSearchWEBData(SearchListWebModel searchListWebModel, int? count, int? pageNum, int? Sort, int? Type, int? RowsPerPage, string SearchText, string SearchLocation, Int32? typeid, Int32? locid)
        {
            if (Request.UrlReferrer !=null &&  Request.UrlReferrer.ToString().Contains(ConfigurationManager.AppSettings["WebsiteRawPath"].ToString()))
            {
                string viewName = "";
                viewName = "_ListingViewMoreWeb";
                JsonModel jsonModel = new JsonModel();
                searchListWebModel.pageNum = pageNum.HasValue ? (pageNum.Value + 1) : 1;
                int currentpage = pageNum.HasValue ? (pageNum.Value + 1) : 1; //Page ?? 1;
                int nextpage = count.HasValue ? (count.Value + 1) : 1;
                int currentsort = Sort ?? 1;
                int currenttype = Type ?? 1;
                int CurrentRowsPerPage = RowsPerPage ?? 10;
                //  searchListWebModel.contentId = string.IsNullOrEmpty(Convert.ToString(searchListWebModel.contentId)) ? 0 : Convert.ToInt32(searchListWebModel.contentId);
                //  searchListWebModel.locid = string.IsNullOrEmpty(Request.QueryString["sid"]) ? 0 : Convert.ToInt32(Request.QueryString["sid"].ToString());
                ListSearchResultWEB listSearchResultWEB = new ListSearchResultWEB();
                if ((typeid.HasValue == false && string.IsNullOrEmpty(SearchText) && string.IsNullOrEmpty(SearchLocation) && locid.HasValue == false) || (!string.IsNullOrEmpty(SearchText) && string.IsNullOrEmpty(SearchLocation)))
                {
                    return Json(null,
                   JsonRequestBehavior.AllowGet);
                }
                listSearchResultWEB = GetSearchDataWEB(nextpage, currenttype, CurrentRowsPerPage, typeid.Value, SearchText, SearchLocation, locid.Value).FirstOrDefault();
                if (listSearchResultWEB == null)
                {
                    return Json(null,
                   JsonRequestBehavior.AllowGet);
                }
                searchListWebModel.listSearchResultWEB = listSearchResultWEB;

                jsonModel.HTMLString = RenderPartialViewToString(viewName, searchListWebModel);
                return Json(jsonModel,
                   JsonRequestBehavior.AllowGet);
            }
            return Json(null,
                   JsonRequestBehavior.AllowGet);
        }
        protected string RenderPartialViewToString(string viewName, object model)
        {

            try
            {
                if (string.IsNullOrWhiteSpace(viewName))
                    viewName = ControllerContext.RouteData.GetRequiredString("action");
                ViewData.Model = model;
                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                    ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                    viewResult.View.Render(viewContext, sw);

                    return sw.GetStringBuilder().ToString();
                }
            }
            catch (Exception e)
            {

                ViewData["IsError"] = "y";
                return "";
            }
        }

        public List<BannerWeb> GetBanners(string keyword, string location, string bizid)
        {
            string tempBizid = string.Empty;
            List<BannerWeb> bannerWEB = new List<BannerWeb>();

            if (!string.IsNullOrEmpty(bizid))
            {
                tempBizid = bizid.Trim().Substring(1);

                using (var db = new VconnectDBContext29())
                {

                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[prc_get_banneradvert_2014_test]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@businessid", tempBizid));
                    cmd.Parameters.Add(new SqlParameter("@keyword", keyword.Replace("-", " ").Replace("  ", "")));
                    cmd.Parameters.Add(new SqlParameter("@location", location));


                    try
                    {
                        // Run the sproc
                        //db.Database.Connection.Open();

                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();

                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            bannerWEB = ((IObjectContextAdapter)db).ObjectContext.Translate<BannerWeb>(reader).ToList();
                            reader.NextResult();

                        }

                    }
                    catch (Exception exp)
                    {
                        log.LogMe(exp);
                    }
                    finally
                    {
                        db.Database.Connection.Close();
                    }
                }

            }

            return bannerWEB;

        }

        //[HttpGet]
        //public JsonResult GetBannerHTML(string keyword, string location, string bizid,string sourceurl,string previousurl)
        //{

           
        //    if (string.IsNullOrEmpty(MyGlobalVariables.GetIpAddress))
        //    {
        //        MyGlobalVariables.GetIpAddress = Utility.GetIpAddress();
        //    }
        //    if (string.IsNullOrEmpty(MyGlobalVariables.GetReverseDns))
        //    {
        //        MyGlobalVariables.GetReverseDns = Utility.GetReversedns(MyGlobalVariables.GetIpAddress);
        //    }

        //    List<BannerWeb> bannerWEB = new List<BannerWeb>();
        //    if (!string.IsNullOrEmpty(bizid))
        //    {
        //        using (var db = new VconnectDBContext29())
        //        {

        //            var cmd = db.Database.Connection.CreateCommand();
        //            cmd.CommandText = "[dbo].[prc_get_banneradvert_2014_test_bannerlog]";
        //            cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //            //Add parameters to command object
        //            cmd.Parameters.Add(new SqlParameter("@businessid", bizid));
        //            cmd.Parameters.Add(new SqlParameter("@keyword", keyword.Replace("-", " ").Replace("  ", "")));
        //            cmd.Parameters.Add(new SqlParameter("@location", location));
        //            cmd.Parameters.Add(new SqlParameter("@sourceurl", sourceURL));
        //            cmd.Parameters.Add(new SqlParameter("@previousurl", previousURL));
        //            cmd.Parameters.Add(new SqlParameter("@ipaddress", MyGlobalVariables.GetIpAddress));
        //            cmd.Parameters.Add(new SqlParameter("@reversedns", MyGlobalVariables.GetReverseDns));

        //            if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
        //            {
        //                ContentId = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
        //                cmd.Parameters.Add(new SqlParameter("@userid", ContentId));
        //            }
        //            else
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@userid", 0));
        //            }




        //            try
        //            {

        //                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
        //                connection.Open();

        //                var reader = cmd.ExecuteReader();
        //                if (reader.HasRows)
        //                {
        //                    bannerWEB = ((IObjectContextAdapter)db).ObjectContext.Translate<BannerWeb>(reader).ToList();                           
        //                }
        //                if (bannerWEB != null)
        //                {
        //                    foreach (var item in bannerWEB)
        //                    {
        //                       string WebsiteRawPath = ConfigurationManager.AppSettings["WebsiteRawPath"].ToString();
        //                        var div = '<div><a href='"+WebsiteRawPath+"'/linkredirect?id=' + item.bannerid + '&url=@curl&link=' + item.url + '&sourceurl=' + sourceurl + '&previousurl=' + previousurl + '" target="_blank" rel="nofollow"><img  src="@MyGlobalVariables.ImagesRootPath' + item.banner + '" /></a></a></div>';
        //                        if(item.membership == 50){}
        //                            else if(item.membership == 40){}
        //                        else if (item.membership == 30) { }
        //                    }
        //                }
                                                

        //            }
        //            catch (Exception exp)
        //            {
        //                log.LogMe(exp);

        //            }
        //            finally
        //            {
        //                db.Database.Connection.Close();
        //            }
        //        }

        //    }
        //    return Json(bannerWEB,
        //       JsonRequestBehavior.AllowGet);
        //}


        [HttpGet]
        public JsonResult GetBanner(string keyword, string location, string bizid)
        {

            //   StringBuilder sb = new StringBuilder();
            if (System.Web.HttpContext.Current.Request.Url == null || System.Web.HttpContext.Current.Request.Url.ToString().Trim() == "")
            {
                AbsoluteURI = "-";
            }
            else
            {
                AbsoluteURI = System.Web.HttpContext.Current.Request.Url.ToString().Replace("~", "");
            }
            AssignHTTPVariables(AbsoluteURI);
            if (string.IsNullOrEmpty(MyGlobalVariables.GetIpAddress))
            {
                MyGlobalVariables.GetIpAddress = Utility.GetIpAddress();
            }
            if (string.IsNullOrEmpty(MyGlobalVariables.GetReverseDns))
            {
               MyGlobalVariables.GetReverseDns = Utility.GetReversedns(MyGlobalVariables.GetIpAddress);
            }

            List<BannerWeb> bannerWEB = new List<BannerWeb>();
            if (!string.IsNullOrEmpty(bizid))
            {
                using (var db = new VconnectDBContext29())
                {

                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[prc_get_banneradvert_2014_test_bannerlog]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object
                    cmd.Parameters.Add(new SqlParameter("@businessid", bizid));
                    cmd.Parameters.Add(new SqlParameter("@keyword", keyword.Replace("-", " ").Replace("  ", "")));
                    cmd.Parameters.Add(new SqlParameter("@location", location));
                    cmd.Parameters.Add(new SqlParameter("@sourceurl", sourceURL));
                    cmd.Parameters.Add(new SqlParameter("@previousurl", previousURL));
                    cmd.Parameters.Add(new SqlParameter("@ipaddress", MyGlobalVariables.GetIpAddress));
                    cmd.Parameters.Add(new SqlParameter("@reversedns", MyGlobalVariables.GetReverseDns));

                    if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                    {
                        ContentId = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                        cmd.Parameters.Add(new SqlParameter("@userid", ContentId));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@userid", 0));
                    }



                 
                    try
                    {

                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();

                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            bannerWEB = ((IObjectContextAdapter)db).ObjectContext.Translate<BannerWeb>(reader).ToList();
                            //string bannerid = string.Empty;
                            //foreach (var i in bannerWEB)
                            //{
                            //    bannerid = i.bannerid.ToString();

                            //    sb.Append(bannerid);
                            //    sb.Append(",");


                            //}
                        }

                        //if (System.Web.HttpContext.Current.Request.Url.AbsoluteUri == null || System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString().Trim() == "")
                        //{
                        //    AbsoluteURI = "-";
                        //}
                        //else
                        //{
                        //    AbsoluteURI = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString().Replace("~", "");
                        //}
                        //AssignHTTPVariables(MyGlobalVariables.GetIpAddress, AbsoluteURI);
                        // int result = 0;

                        //if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                        //{
                        //    ContentId = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                        //    result = log.addbannerimpressionlog(sb.ToString(), 0, keyword, ContentId, MyGlobalVariables.GetIpAddress, sourceURL, previousURL, MyGlobalVariables.GetReverseDns.ToString());
                        //}
                        ////    //ContentId = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                        //else
                        //{
                        //    ContentId = 0;
                        //    result = log.addbannerimpressionlog(sb.ToString(), 0, keyword, ContentId, MyGlobalVariables.GetIpAddress, sourceURL, previousURL, MyGlobalVariables.GetReverseDns.ToString());
                        //}


                    }
                    catch (Exception exp)
                    {
                        log.LogMe(exp);

                    }
                    finally
                    {
                        db.Database.Connection.Close();
                    }
                }

            }
            return Json(bannerWEB,
               JsonRequestBehavior.AllowGet);
        }

        public void BannerClickLogwithSource(int? bid, string sourceurl, string previousurl)
        {
            if (bid.HasValue)
            {
                int result = 0;
                //if (System.Web.HttpContext.Current.Request.Url.AbsoluteUri == null || System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString().Trim() == "")
                //{
                //    AbsoluteURI = "-";
                //}
                //else
                //{
                //    AbsoluteURI = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString().Replace("~", "");
                //}
                //AssignHTTPVariables(AbsoluteURI);

                string IPAddress = string.Empty;
                string ReverseDNS = string.Empty;
                IPAddress = Vconnect.MvcApplication.GetVisitorIPAddress();
                ReverseDNS = !string.IsNullOrEmpty(IPAddress) ? Vconnect.MvcApplication.GetReverseDns(IPAddress.ToString()) : "";
                if (Request != null && !string.IsNullOrEmpty(objUS.SV_VCLoginID))
                {
                    ContentId = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                    result = log.addbannerclicklog(bid.Value, 0, "", ContentId, IPAddress, sourceurl, previousurl);
                }
                else if (System.Web.HttpContext.Current.Request != null && !string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                {
                    ContentId = Convert.ToInt32(objUS.SV_VCUserContentID);
                    result = log.addbannerclicklog(bid.Value, 0, "", ContentId, IPAddress, sourceurl, previousurl);
                }
                //    //ContentId = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                else
                {

                    result = log.addbannerclicklog(bid.Value, 0, "", 0, IPAddress, sourceurl, previousurl);
                }
            }
        }

        //for bannerclicklog
        public void BannerClickLog(int? bid, string sourceurl, string previousurl)
        {
            //string sourceurl = string.Empty;
            //string previousurl = string.Empty;

            ////sourceurl = TempData["source"] != null ? TempData["source"].ToString() : "";
            ////previousurl = TempData["previous"] != null ? TempData["previous"].ToString() : "";

           
            //sourceurl = TempData.Peek("source") != null ? TempData.Peek("source").ToString() : "";
            //previousurl = TempData.Peek("previous") != null ? TempData.Peek("previous").ToString() : "";

            //TempData.Keep("source");
            //TempData.Keep("previous");


            if (bid.HasValue)
            {
                int result = 0;
                string IPAddress = string.Empty;
                string ReverseDNS = string.Empty;
                
                IPAddress = Vconnect.MvcApplication.GetVisitorIPAddress();
                ReverseDNS = !string.IsNullOrEmpty(IPAddress) ? Vconnect.MvcApplication.GetReverseDns(IPAddress.ToString()) : "";
                //if (System.Web.HttpContext.Current.Request.Url.AbsoluteUri == null || System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString().Trim() == "")
                //{
                //    AbsoluteURI = "-";
                //}
                //else
                //{
                //    AbsoluteURI = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString().Replace("~", "");
                //}
                //AssignHTTPVariables(AbsoluteURI);


                if (Request != null && !string.IsNullOrEmpty(objUS.SV_VCLoginID))
                {
                    ContentId = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                    result = log.addbannerclicklog(bid.Value, 0, "", ContentId, IPAddress, System.Web.HttpUtility.HtmlDecode(sourceurl), System.Web.HttpUtility.HtmlDecode(previousurl));
                }
                else if (System.Web.HttpContext.Current.Request != null && !string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                {
                    ContentId = Convert.ToInt32(objUS.SV_VCUserContentID);
                    result = log.addbannerclicklog(bid.Value, 0, "", ContentId, IPAddress, System.Web.HttpUtility.HtmlDecode(sourceurl), System.Web.HttpUtility.HtmlDecode(previousurl));
                }
                //    //ContentId = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                else
                {

                    result = log.addbannerclicklog(bid.Value, 0, "", 0, IPAddress, System.Web.HttpUtility.HtmlDecode(sourceurl), System.Web.HttpUtility.HtmlDecode(previousurl));
                }
            }
        }

        public JsonResult BannerClickLogged(int? bid)
        {
            if (bid.HasValue)
            {
                int result = 0;
                if (System.Web.HttpContext.Current.Request.Url.AbsoluteUri == null || System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString().Trim() == "")
                {
                    AbsoluteURI = "-";
                }
                else
                {
                    AbsoluteURI = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString().Replace("~", "");
                }
                AssignHTTPVariables(AbsoluteURI);


                if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                {
                    ContentId = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                    result = log.addbannerclicklog(bid.Value, 0, "", ContentId, Utility.GetIpAddress(), sourceURL, previousURL);
                }
                //    //ContentId = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                else
                {

                    result = log.addbannerclicklog(bid.Value, 0, "", 0, Utility.GetIpAddress(), sourceURL, previousURL);
                }
            }
            return Json("bannerclicklogged", JsonRequestBehavior.AllowGet);
        }

        public ActionResult prioritybizlog(int businessid, string businessname, int prioityspot, string searchkeyword, string searchlocation,string sourceurl, string previousurl)
        {

            try
            {

                int userId = 0;
                if (!string.IsNullOrEmpty(objUS.SV_VCLoginID))
                {
                    userId = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                }
                int result = 0;

                string IPAddress = string.Empty;
                string ReverseDNS = string.Empty;
                IPAddress = Vconnect.MvcApplication.GetVisitorIPAddress();
                ReverseDNS = !string.IsNullOrEmpty(IPAddress) ? Vconnect.MvcApplication.GetReverseDns(IPAddress.ToString()) : "";
                result = log.prioritybizlog(businessid, businessname, prioityspot, searchkeyword, searchlocation, IPAddress, ReverseDNS, userId, System.Web.HttpUtility.HtmlDecode(sourceurl), System.Web.HttpUtility.HtmlDecode(previousurl));
            }
            catch (Exception ex)
            { log.LogMe(ex); }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LogSearchPage(string SearchKeywordType, string SearchKeyword, string SearchLocation, string TotalRecords, string contentid, int? islead, string sourceurl, string previousurl)
        {
            //string sourceurl = string.Empty;
            //string previousurl = string.Empty;           
            ////sourceurl = TempData["sourcelisting"] != null ? TempData["sourcelisting"].ToString() : "";
            ////previousurl = TempData["previouslisting"] != null ? TempData["previouslisting"].ToString() : "";
            //sourceurl = TempData.Peek("sourcelisting") != null ? TempData.Peek("sourcelisting").ToString() : "";
            //previousurl = TempData.Peek("previouslisting") != null ? TempData.Peek("previouslisting").ToString() : "";
            //TempData.Keep("sourcelisting");
            //TempData.Keep("previouslisting");
            if (string.IsNullOrEmpty(SearchKeywordType) && string.IsNullOrEmpty(SearchKeyword) && string.IsNullOrEmpty(SearchLocation) && string.IsNullOrEmpty(TotalRecords) && string.IsNullOrEmpty(contentid) && islead.HasValue == false && string.IsNullOrEmpty(sourceurl) && string.IsNullOrEmpty(previousurl))
            {
            }
            else
            {
                string result = "0";
                string warning = string.Empty;
                string outipaddress = string.Empty;
                string outsearchlocation = string.Empty;
                string outsourcename = string.Empty;
                string outIPLocation = string.Empty;
                string Iplocation = string.Empty;
                //if (System.Web.HttpContext.Current.Request.Url.AbsoluteUri == null || System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString().Trim() == "")
                //{
                //    AbsoluteURI = "-";
                //}
                //else
                //{
                //    AbsoluteURI = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString();//.Replace("~", "");
                //}
                //AssignHTTPVariables(AbsoluteURI);
                //if ((Request.QueryString["sl"] != null && Request.QueryString["sq"] != null) || (Request.QueryString["SearchKeyword"] != null && Request.QueryString["SearchLocation"] != null))
                {
                    //Session["IPADDRESS"] = MyGlobalVariables.GetIpAddress;
                    //Session["REVERSEDNS"] = MyGlobalVariables.GetReverseDns;
                    //if (MyGlobalVariables.GetIpAddress == "0")
                    //{
                    //    try
                    //    {
                    //        Session["IPADDRESS"] = Vconnect.MvcApplication.GetVisitorIPAddress();
                    //    }
                    //    catch
                    //    { }
                    //}
                    //if (MyGlobalVariables.GetReverseDns == "0")
                    //{
                    //    try
                    //    {
                    //        Session["REVERSEDNS"] = Vconnect.MvcApplication.GetReverseDns(Session["IPADDRESS"].ToString());
                    //    }
                    //    catch
                    //    { }
                    //}
                    string IPAddress = string.Empty;
                    string ReverseDNS = string.Empty;
                    IPAddress = Vconnect.MvcApplication.GetVisitorIPAddress();
                    ReverseDNS = !string.IsNullOrEmpty(IPAddress) ? Vconnect.MvcApplication.GetReverseDns(IPAddress.ToString()) : "";
                    result = log.addsearchpagelog(SearchKeyword.ToString().Trim(), SearchLocation, 0, "", Convert.ToInt32(contentid), IPAddress, System.Web.HttpUtility.HtmlDecode(sourceurl), System.Web.HttpUtility.HtmlDecode(previousurl), SearchKeywordType.ToString().Trim(), "", Utility.GetDomain(), Sitetype.websearch.GetHashCode().ToString(), Iplocation, TotalRecords, "Web Search", ReverseDNS, 0, islead.HasValue ? islead.Value : 0);

                }
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
        
        //for search Page log
        public ActionResult SearchPageLog(string SearchKeywordType, string SearchKeyword, string SearchLocation, string TotalRecords, string contentid, int? islead)
        {

            string result = "0";
            string warning = string.Empty;
            string outipaddress = string.Empty;
            string outsearchlocation = string.Empty;
            string outsourcename = string.Empty;
            string outIPLocation = string.Empty;
            string Iplocation = string.Empty;
            if (System.Web.HttpContext.Current.Request.Url.AbsoluteUri == null || System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString().Trim() == "")
            {
                AbsoluteURI = "-";
            }
            else
            {
                AbsoluteURI = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString();//.Replace("~", "");
            }
            AssignHTTPVariables(AbsoluteURI);
            if ((Request.QueryString["sl"] != null && Request.QueryString["sq"] != null) || (Request.QueryString["SearchKeyword"] != null && Request.QueryString["SearchLocation"] != null))          
            {
                string IPAddress = string.Empty;
                string ReverseDNS = string.Empty;
                IPAddress = Vconnect.MvcApplication.GetVisitorIPAddress();
                ReverseDNS = !string.IsNullOrEmpty(IPAddress) ? Vconnect.MvcApplication.GetReverseDns(IPAddress.ToString()) : "";
                result = log.addsearchpagelog(SearchKeyword.ToString().Trim(), SearchLocation, 0, "", Convert.ToInt32(contentid), IPAddress, sourceURL, previousURL, SearchKeywordType.ToString().Trim(), "", Utility.GetDomain(), Sitetype.websearch.GetHashCode().ToString(), Iplocation, TotalRecords, "Web Search", ReverseDNS, 0, islead.HasValue ? islead.Value : 0);

            }

            return Json(null, JsonRequestBehavior.AllowGet);
        }
        
        protected void AddSearchPageLog(int SearchKeywordType, string SearchKeyword, string SearchLocation, string TotalRecords, string IPAddress, string reversedns, string Domain, Int32 contentid)
        {

            string result = "0";
            string warning = string.Empty;
            string outipaddress = string.Empty;
            string outsearchlocation = string.Empty;
            string outsourcename = string.Empty;
            string outIPLocation = string.Empty;
            string Iplocation = string.Empty;
            if (Request.QueryString["sl"] != null && Request.QueryString["sq"] != null)
            {
                //string IPAddress = string.Empty;
                string ReverseDNS = string.Empty;
                IPAddress = Vconnect.MvcApplication.GetVisitorIPAddress();
                ReverseDNS = !string.IsNullOrEmpty(IPAddress) ? Vconnect.MvcApplication.GetReverseDns(IPAddress.ToString()) : "";
                result = log.addsearchpagelog(SearchKeyword.ToString().Trim(), SearchLocation, 0, "", contentid, IPAddress, sourceURL, previousURL, SearchKeywordType.ToString().Trim(), "", Domain, Sitetype.websearch.GetHashCode().ToString(), Iplocation, TotalRecords, "Web Search", ReverseDNS, 0, 0);

            }

            return;
        }
        #region old lead code
        //public ActionResult LeadRequest(string userName, string Phone, string keyword, string location)
        //{
        //    int vaildphone = 0;
        //    string sResult = string.Empty;
        //    if (Phone != null)
        //    {
        //        vaildphone = Vconnect.Common.Utility.isValidPhone("0" + Phone.ToString());
        //        if (vaildphone == 0)
        //        {
        //            sResult = "Invalid";
        //        }
        //        else
        //        {

        //            string Source = string.Empty;
        //            int USERID = 0;
        //            string sRandomCode = Utility.GetNumericRandomNumber().ToString().Trim();
        //            string result = AddLeadRequest(userName, Phone, keyword, location, sRandomCode);
        //            string[] array = result.Split(',');
        //            int OutLeadID = Convert.ToInt32(array[0]);
        //            int Err = Convert.ToInt32(array[1]);
        //            if (Err == 0)
        //            {
        //                SendSMS objSendSMS = new SendSMS();
        //                //string SMSResponse = objSendSMS.Sendmessage("Dear Customer, Your OTP for verifying mobile number is: " +sRandomCode + ".", homeWebModel.Phone.ToString().Trim());
        //             //   string SMSResponse = objSendSMS.Sendmessage("Dear Customer, Your OTP for verifying mobile number is: " + sRandomCode.Trim() + ".", Phone);
                       
                     
        //                //Maintain the SMS EMAIL LOG
        //                //if (MyGlobalVariables.IsWapWeb == "WAP")
        //                //{
        //                //    Utility.SaveSMSEmailLog_lead(Convert.ToInt32(USERID), Usertype.OtherUser.GetHashCode(), "", "0" + Phone, userName, MessageType.SMS_UserReg.GetHashCode(), "Lead phone verification code", "Dear Customer, Your OTP for verifying mobile number is:" + sRandomCode.ToString().Trim() + ".", 0, "", "", SMSResponse, "", 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), "", "WAP", OutLeadID);
        //                //}
        //                //else
        //                //{
        //                //   Utility.SaveSMSEmailLog_lead(Convert.ToInt32(USERID), Usertype.OtherUser.GetHashCode(), "", "0" + Phone, userName, MessageType.SMS_UserReg.GetHashCode(), "Lead phone verification code", "Dear Customer, Your OTP for verifying mobile number is:" + sRandomCode.ToString().Trim() + ".", 0, "", "", SMSResponse, "", 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), "", "WEB", OutLeadID);
        //                //}


        //                //ViewData["sendvalidationcode"] = "validationcodesent";
        //                sResult = "code";
        //            }
                   
        //            else if (Err == 3)
        //            {
        //                sResult = "success";
        //            }
        //            else
        //            {
        //                sResult = "Error";
        //            }

        //        }

        //    }
        //    return Json(sResult, JsonRequestBehavior.AllowGet);
        //}
        //public string AddLeadRequest(string userName, string Phone, string SearchText, string SearchLocation, string code)
        //{
        //    int err = 0;
        //    int lead = 0;
        //    string result = string.Empty;
        //    try
        //    {
        //        using (var db = new VconnectDBContext29())
        //        {
        //            var cmd = db.Database.Connection.CreateCommand();
        //            var _con = ((IObjectContextAdapter)db).ObjectContext.Connection;
        //            cmd.CommandTimeout = 75;
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.CommandText = "prc_add_leadrequest_beta_otp";
        //            cmd.Parameters.Add(new SqlParameter("@userloginid", 0));
        //            cmd.Parameters.Add(new SqlParameter("@sendercontactnumber", "0"+ Phone));
        //            cmd.Parameters.Add(new SqlParameter("@requirement", SearchText));
        //            cmd.Parameters.Add(new SqlParameter("@sendername", userName));
        //            cmd.Parameters.Add(new SqlParameter("@LFLocation", SearchLocation));
        //            if (MyGlobalVariables.IsWapWeb == "WAP")
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@leadsourceid", LeadSourceidType.PYR));
        //                cmd.Parameters.Add(new SqlParameter("@leadsourcetext", "WAPPYR"));
        //            }
        //            else
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@leadsourceid", LeadSourceidType.ListingPageAutoLead));
        //                cmd.Parameters.Add(new SqlParameter("@leadsourcetext", "ListingPageAutoLead"));
        //            }


        //            cmd.Parameters.Add(new SqlParameter("@ipaddress", Utility.GetIpAddress()));
        //            cmd.Parameters.Add(new SqlParameter("@verificationcode", code));
        //            SqlParameter Err = new SqlParameter("@Err", SqlDbType.Int);
        //            Err.Direction = ParameterDirection.Output;
        //            SqlParameter leadid = new SqlParameter("@leadid", SqlDbType.Int);
        //            leadid.Direction = ParameterDirection.Output;
        //            cmd.Parameters.Add(Err);
        //            cmd.Parameters.Add(leadid);
        //            _con.Open(); cmd.ExecuteNonQuery();
        //            err = (int)cmd.Parameters["@Err"].Value;
        //            lead = (int)cmd.Parameters["@leadid"].Value;

        //            result = cmd.Parameters["@leadid"].Value + "," + cmd.Parameters["@Err"].Value;
        //            _con.Close(); _con.Dispose();


        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.LogMe(ex);
        //    }
        //    return result;

        //}

        //public ActionResult GetLead(string validationCode, string keyword, string location)
        //{

        //    //check if number verified
        //    string result = string.Empty;

        //    if (string.IsNullOrEmpty(validationCode) && string.IsNullOrEmpty(keyword) && string.IsNullOrEmpty(location))
        //    {
        //        result = "Error";
        //    }
        //    else
        //    {
        //        int isValidCode = VerifyCode(validationCode, keyword, location);

        //        if (isValidCode == 1) //Error in procedure
        //        {
        //            result = "Error";

        //        }
        //        else if (isValidCode == 3)//Code not valid
        //        {
        //            result = "InvalidCode";

        //        }
        //        else if (isValidCode == 0)//Manual lead
        //        {
        //            result = "ManualLead";

        //        }
        //        else
        //        {
        //            result = "Success";

        //        }
        //    }

        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
        ////To verify verification code sent on mobile number
        //public int VerifyCode(string validationCode, string keyword, string location)
        //{
        //    int result = 0;
        //    try
        //    {
        //        using (var db = new VconnectDBContext29())
        //        {
        //            var cmd = db.Database.Connection.CreateCommand();
        //            var _con = ((IObjectContextAdapter)db).ObjectContext.Connection;
        //            cmd.CommandTimeout = 75;
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.CommandText = "prc_update_leadrequest";
        //            cmd.Parameters.Add(new SqlParameter("@requirement", keyword));
        //            cmd.Parameters.Add(new SqlParameter("@LFLocation", location));
        //            cmd.Parameters.Add(new SqlParameter("@verificationcode", validationCode));
        //            SqlParameter Err = new SqlParameter("@Err", SqlDbType.Int);
        //            Err.Direction = ParameterDirection.Output;
        //            cmd.Parameters.Add(Err);
        //            _con.Open();
        //            cmd.ExecuteNonQuery();

        //            result = (int)cmd.Parameters["@Err"].Value;
        //            _con.Close(); _con.Dispose();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.LogMe(ex);

        //    }
        //    return result;

        //}
        ////To inseret lead request and verificationcode in lead table before verification
        #endregion

        #region new lead code

        public ActionResult LeadRequestSpecification(string userName, string Phone, string keyword, string location, string questionAnswer, int contenttype, int contenttypeid, string sourceurl, string previousurl)
        {
            
            int vaildphone = 0;
            string sResult = string.Empty;
            SearchListWebModel searchList = new SearchListWebModel();
            int resultFinal = 0;
            if (Phone != null)
            {
                vaildphone = Vconnect.Common.Utility.isValidPhonemobile("0" + Phone.ToString());
                if (vaildphone == 0)
                {
                    sResult = "Invalid";
                }
                else
                {                   
                    string Source = string.Empty;
                    int USERID = 0;
                    string sRandomCode = Utility.GetNumericRandomNumber().ToString().Trim();
                    string result = AddLeadRequest(userName, Phone, keyword, location, sRandomCode, questionAnswer, contenttype, contenttypeid, System.Web.HttpUtility.HtmlDecode(sourceurl), System.Web.HttpUtility.HtmlDecode(previousurl));
                    string[] array = result.Split(',');
                    int OutLeadID = Convert.ToInt32(array[0]);
                    int Err = Convert.ToInt32(array[1]);

                    resultFinal = searchList.leadResponse(keyword, location, OutLeadID);
                    if (resultFinal == 1)
                        sResult = "success," + OutLeadID;
                    else
                        sResult = "Error";
                    //if (resultFinal == 0)
                    //{
                    //    sResult = "code," + OutLeadID;
                    //}
                    //else if (resultFinal == 3)
                    //{
                    //    sResult = "success," + OutLeadID;
                    //}
                    //else 
                    //{
                    //    sResult = "Error";
                    //}

                    //if (Err == 0)
                    //{
                    //    SendSMS objSendSMS = new SendSMS();
                    //    //string SMSResponse = objSendSMS.Sendmessage("Dear Customer, Your OTP for verifying mobile number is: " +sRandomCode + ".", homeWebModel.Phone.ToString().Trim());
                    //    //   string SMSResponse = objSendSMS.Sendmessage("Dear Customer, Your OTP for verifying mobile number is: " + sRandomCode.Trim() + ".", Phone);


                    //    //Maintain the SMS EMAIL LOG
                    //    //if (MyGlobalVariables.IsWapWeb == "WAP")
                    //    //{
                    //    //    Utility.SaveSMSEmailLog_lead(Convert.ToInt32(USERID), Usertype.OtherUser.GetHashCode(), "", "0" + Phone, userName, MessageType.SMS_UserReg.GetHashCode(), "Lead phone verification code", "Dear Customer, Your OTP for verifying mobile number is:" + sRandomCode.ToString().Trim() + ".", 0, "", "", SMSResponse, "", 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), "", "WAP", OutLeadID);
                    //    //}
                    //    //else
                    //    //{
                    //    //   Utility.SaveSMSEmailLog_lead(Convert.ToInt32(USERID), Usertype.OtherUser.GetHashCode(), "", "0" + Phone, userName, MessageType.SMS_UserReg.GetHashCode(), "Lead phone verification code", "Dear Customer, Your OTP for verifying mobile number is:" + sRandomCode.ToString().Trim() + ".", 0, "", "", SMSResponse, "", 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), "", "WEB", OutLeadID);
                    //    //}


                    //    //ViewData["sendvalidationcode"] = "validationcodesent";
                    //    sResult = "code," + OutLeadID;
                    //}

                    //else if (Err == 3)
                    //{
                    //    sResult = "success," + OutLeadID;
                    //}
                    //else
                    //{
                    //    sResult = "Error";
                    //}

                }

            }
            return Json(sResult, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LeadRequest(string userName, string Phone, string keyword, string location)
        {
            int vaildphone = 0;
            string sResult = string.Empty;
            if (Phone != null)
            {
                vaildphone = Vconnect.Common.Utility.isValidPhone("0" + Phone.ToString());
                if (vaildphone == 0)
                {
                    sResult = "Invalid";
                }
                else
                {

                    string Source = string.Empty;
                    int USERID = 0;
                    string sRandomCode = Utility.GetNumericRandomNumber().ToString().Trim();
                    string result = AddLeadRequest(userName, Phone, keyword, location, sRandomCode);
                    string[] array = result.Split(',');
                    int OutLeadID = Convert.ToInt32(array[0]);
                    int Err = Convert.ToInt32(array[1]);
                    if (Err == 0)
                    {
                        SendSMS objSendSMS = new SendSMS();
                        //string SMSResponse = objSendSMS.Sendmessage("Dear Customer, Your OTP for verifying mobile number is: " +sRandomCode + ".", homeWebModel.Phone.ToString().Trim());
                        //   string SMSResponse = objSendSMS.Sendmessage("Dear Customer, Your OTP for verifying mobile number is: " + sRandomCode.Trim() + ".", Phone);


                        //Maintain the SMS EMAIL LOG
                        //if (MyGlobalVariables.IsWapWeb == "WAP")
                        //{
                        //    Utility.SaveSMSEmailLog_lead(Convert.ToInt32(USERID), Usertype.OtherUser.GetHashCode(), "", "0" + Phone, userName, MessageType.SMS_UserReg.GetHashCode(), "Lead phone verification code", "Dear Customer, Your OTP for verifying mobile number is:" + sRandomCode.ToString().Trim() + ".", 0, "", "", SMSResponse, "", 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), "", "WAP", OutLeadID);
                        //}
                        //else
                        //{
                        //   Utility.SaveSMSEmailLog_lead(Convert.ToInt32(USERID), Usertype.OtherUser.GetHashCode(), "", "0" + Phone, userName, MessageType.SMS_UserReg.GetHashCode(), "Lead phone verification code", "Dear Customer, Your OTP for verifying mobile number is:" + sRandomCode.ToString().Trim() + ".", 0, "", "", SMSResponse, "", 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), "", "WEB", OutLeadID);
                        //}


                        //ViewData["sendvalidationcode"] = "validationcodesent";
                        sResult = "code";
                    }

                    else if (Err == 3)
                    {
                        sResult = "success";
                    }
                    else
                    {
                        sResult = "Error";
                    }

                }

            }
            return Json(sResult, JsonRequestBehavior.AllowGet);
        }
        public string AddLeadRequest(string userName, string Phone, string SearchText, string SearchLocation, string code, string questionAnswer, Int32 contenttype, Int32 contenttypeid, string sourceurl, string previousurl)
        {
            int err = 0;
            int lead = 0;
            string result = string.Empty;
            try
            {
                using (var db = new VconnectDBContext29())
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    var _con = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    cmd.CommandTimeout = 75;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "prc_add_leadrequest_beta_otp";
                    cmd.Parameters.Add(new SqlParameter("@userloginid", 0));
                    cmd.Parameters.Add(new SqlParameter("@sendercontactnumber", "0" + Phone));
                    cmd.Parameters.Add(new SqlParameter("@requirement", SearchText));
                    cmd.Parameters.Add(new SqlParameter("@sendername", userName));
                    cmd.Parameters.Add(new SqlParameter("@LFLocation", SearchLocation));
                    cmd.Parameters.Add(new SqlParameter("@sourceurl", sourceurl));
                    cmd.Parameters.Add(new SqlParameter("@previousurl", previousurl));
                    //if (MyGlobalVariables.IsWapWeb == "WAP")
                    //{
                    //    cmd.Parameters.Add(new SqlParameter("@leadsourceid", LeadSourceidType.PYR));
                    //    cmd.Parameters.Add(new SqlParameter("@leadsourcetext", "WAPPYR"));
                    //}
                    //else
                    //{
                    cmd.Parameters.Add(new SqlParameter("@leadsourceid", LeadSourceidType.ListingPageAutoLead));
                    cmd.Parameters.Add(new SqlParameter("@leadsourcetext", "ListingPageAutoLead"));
                    //}


                    cmd.Parameters.Add(new SqlParameter("@ipaddress", Utility.GetIpAddress()));
                    cmd.Parameters.Add(new SqlParameter("@verificationcode", code));
                    SqlParameter Err = new SqlParameter("@Err", SqlDbType.Int);
                    Err.Direction = ParameterDirection.Output;
                    SqlParameter leadid = new SqlParameter("@leadid", SqlDbType.Int);
                    leadid.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(Err);
                    cmd.Parameters.Add(leadid);
                    _con.Open(); cmd.ExecuteNonQuery();
                    err = (int)cmd.Parameters["@Err"].Value;
                    lead = (int)cmd.Parameters["@leadid"].Value;

                    ViewBag.lead = lead;
                    SearchListWebModel searchListWebModal = new SearchListWebModel();
                    if (!string.IsNullOrEmpty(questionAnswer))
                    {
                        if (questionAnswer.Contains(","))
                        {
                            string[] questions = questionAnswer.Split(',');
                            foreach (var item in questions)
                            {
                                if (!item.Contains("undefined"))
                                {
                                    string[] question = item.Split('#');
                                    searchListWebModal.leadQuestion(Convert.ToInt32(question[0]), Convert.ToInt32(question[1]), Convert.ToInt32(question[2]), lead, contenttype, contenttypeid);
                                }
                            }
                        }
                        else if (questionAnswer != "")
                        {                                                        
                            string[] question = questionAnswer.Split('#');
                            searchListWebModal.leadQuestion(Convert.ToInt32(question[0]), Convert.ToInt32(question[1]), Convert.ToInt32(question[2]), lead, contenttype, contenttypeid);                              
                        }
                    }
                    result = cmd.Parameters["@leadid"].Value + "," + cmd.Parameters["@Err"].Value;
                    _con.Close(); _con.Dispose();
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            return result;

        }


        public string leadQuestionRequest(Int32 lead, string questionAnswer, Int32 contenttype, Int32 contenttypeid)
        {
            SearchListWebModel searchListWebModal = new SearchListWebModel();
            if (!string.IsNullOrEmpty(questionAnswer))
            {
                if (questionAnswer.Contains(","))
                {
                    string[] questions = questionAnswer.Split(',');
                    foreach (var item in questions)
                    {
                        if (!item.Contains("undefined"))
                        {
                            string[] question = item.Split('#');
                            searchListWebModal.leadQuestion(Convert.ToInt32(question[0]), Convert.ToInt32(question[1]), Convert.ToInt32(question[2]), lead, contenttype, contenttypeid);
                        }
                    }
                }
            }
            return "code," + lead;
        }

        public string AddLeadRequest(string userName, string Phone, string SearchText, string SearchLocation, string code)
        {
            int err = 0;
            int lead = 0;
            string result = string.Empty;
            try
            {
                using (var db = new VconnectDBContext29())
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    var _con = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    cmd.CommandTimeout = 75;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "prc_add_leadrequest_beta_otp";
                    cmd.Parameters.Add(new SqlParameter("@userloginid", 0));
                    cmd.Parameters.Add(new SqlParameter("@sendercontactnumber", "0" + Phone));
                    cmd.Parameters.Add(new SqlParameter("@requirement", SearchText));
                    cmd.Parameters.Add(new SqlParameter("@sendername", userName));
                    cmd.Parameters.Add(new SqlParameter("@LFLocation", SearchLocation));
                    if (MyGlobalVariables.IsWapWeb == "WAP")
                    {
                        cmd.Parameters.Add(new SqlParameter("@leadsourceid", LeadSourceidType.PYR));
                        cmd.Parameters.Add(new SqlParameter("@leadsourcetext", "WAPPYR"));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@leadsourceid", LeadSourceidType.ListingPageAutoLead));
                        cmd.Parameters.Add(new SqlParameter("@leadsourcetext", "ListingPageAutoLead"));
                    }


                    cmd.Parameters.Add(new SqlParameter("@ipaddress", Utility.GetIpAddress()));
                    cmd.Parameters.Add(new SqlParameter("@verificationcode", code));
                    SqlParameter Err = new SqlParameter("@Err", SqlDbType.Int);
                    Err.Direction = ParameterDirection.Output;
                    SqlParameter leadid = new SqlParameter("@leadid", SqlDbType.Int);
                    leadid.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(Err);
                    cmd.Parameters.Add(leadid);
                    _con.Open(); cmd.ExecuteNonQuery();
                    err = (int)cmd.Parameters["@Err"].Value;
                    lead = (int)cmd.Parameters["@leadid"].Value;

                    result = cmd.Parameters["@leadid"].Value + "," + cmd.Parameters["@Err"].Value;
                    _con.Close(); _con.Dispose();


                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            return result;

        }

        public ActionResult GetLead(string validationCode, string keyword, string location)
        {

            //check if number verified
            string result = string.Empty;

            if (string.IsNullOrEmpty(validationCode) && string.IsNullOrEmpty(keyword) && string.IsNullOrEmpty(location))
            {
                result = "Error";
            }
            else
            {
                int isValidCode = VerifyCode(validationCode, keyword, location);

                if (isValidCode == 1) //Error in procedure
                {
                    result = "Error";

                }
                else if (isValidCode == 3)//Code not valid
                {
                    result = "InvalidCode";

                }
                else if (isValidCode == 0)//Manual lead
                {
                    result = "ManualLead";

                }
                else
                {
                    result = "Success";

                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //To verify verification code sent on mobile number
        public int VerifyCode(string validationCode, string keyword, string location)
        {
            int result = 0;
            try
            {
                using (var db = new VconnectDBContext29())
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    var _con = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    cmd.CommandTimeout = 75;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "prc_update_leadrequest";
                    cmd.Parameters.Add(new SqlParameter("@requirement", keyword));
                    cmd.Parameters.Add(new SqlParameter("@LFLocation", location));
                    cmd.Parameters.Add(new SqlParameter("@verificationcode", validationCode));
                    SqlParameter Err = new SqlParameter("@Err", SqlDbType.Int);
                    Err.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(Err);
                    _con.Open();
                    cmd.ExecuteNonQuery();

                    result = (int)cmd.Parameters["@Err"].Value;
                    _con.Close(); _con.Dispose();
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);

            }
            return result;

        }
        #endregion 
        public ActionResult SearchNotFound()
        {
            //Response.Clear(); //Response.StatusCode = 404;
            //Response.Status = "404 NotFound";
            return View("SearchNotFound");
        }

        protected void LoadMeta(string Pagetype)
        {
            // int CurrentPageNo = Convert.ToInt32(ViewState["pgNo"]);
            //string sSearchKeyword = (ViewState["SearchKeyword"].ToString().ToLower()).Replace("providers-of", " ").Replace("dealers-of", " ").Replace("-", " ").Replace("_", " ").ToLower();
            string sSearchLocation = string.Empty;
            string sState = string.Empty;
            string sStateBreadCrumb = string.Empty;
            string sStateBoilerText = string.Empty;
        }
        //for  save a business(favourite business)
        public string Favourite(Int64? businessId)
        {
            Int64 userId = 0;
            string userName = string.Empty;
            string message = string.Empty;

            //  userId = Convert.ToInt64(objUS.SV_VCUserContentID.ToString());
            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && businessId.HasValue && businessId.Value >0)
            {
                userId = Convert.ToInt64(objUS.SV_VCUserContentID.ToString());
                userName = objUS.SV_VCUserName.ToString();

                SearchListWebModel listModel = new SearchListWebModel();
                int result = listModel.UserWatchlist(userId, businessId.Value);
                if (result == 2)
                {

                    message = "Business already in your list.";
                }
                else
                {
                    message = "Saved";
                    BizDetailWEB bizDetailWeb = new BizDetailWEB();

                    string name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(userName.ToLower());
                    var bizdetail = bizDetailWeb.GetBizDetails(businessId.Value).BizMaster.FirstOrDefault();
                    string IPAddress = string.Empty;
                    string ReverseDNS = string.Empty;
                    IPAddress = Vconnect.MvcApplication.GetVisitorIPAddress();
                    ReverseDNS = !string.IsNullOrEmpty(IPAddress) ? Vconnect.MvcApplication.GetReverseDns(IPAddress.ToString()) : "";
                    if (bizdetail.email.ToString() != "")
                    {
                        SendFavioriteBizCred(bizdetail.contactperson.ToString(), bizdetail.email.ToString(), userName, bizdetail.businessname.ToString(), bizdetail.city.ToString(), bizdetail.state.ToString());
                        //Utility.SaveSMSEmailLog(bizdetail.businessid, Convert.ToInt32(Usertype.B2CbusinessOwner), bizdetail.email.ToString(), bizdetail.phone.ToString(), bizdetail.businessname.ToString(), MessageType.Email_ToBizOwner_MarkedFavourite.GetHashCode(), ConfigurationManager.AppSettings["BizAddFavorite"].ToString(), "Busiess added as favorite by the user: " + userName.ToString().Trim(), Convert.ToInt32(bizdetail.businessid), null, null, null,Utility.GetIpAddress(), 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(),Utility.GetReversedns(Utility.GetIpAddress()), "WEB");
                        Utility.SaveSMSEmailLog(Convert.ToInt32(userId), Convert.ToInt32(Usertype.B2CbusinessOwner), bizdetail.email.ToString(), bizdetail.phone.ToString(), bizdetail.businessname.ToString(), MessageType.Email_ToBizOwner_MarkedFavourite.GetHashCode(), ConfigurationManager.AppSettings["BizAddFavorite"].ToString(), "Busiess added as favorite by the user: " + userName.ToString().Trim(), Convert.ToInt32(bizdetail.businessid), null, null, null, IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), ReverseDNS, "WEB");
                    }
                }
            }

            return message;
        }
        //for Like a business
        public string Like(Int64? businessId)
        {
            Int64 userId = 0;
            string message = string.Empty;
            string userName = string.Empty;
            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && businessId.HasValue && businessId.Value >0)
            {
                userId = Convert.ToInt64(objUS.SV_VCUserContentID.ToString());
                userName = objUS.SV_VCUserName.ToString();

                SearchListWebModel listModel = new SearchListWebModel();
                int result = listModel.BusinessLike(userId, businessId.Value);
                if (result == 2)
                {
                    message = "Business already in your list.";
                }
                else
                {
                    message = "Liked";
                }
            }
            return message;
        }

        //for making business liked/saved
        public ActionResult SaveLike(string bizID, Int64? userID, string val)
        {
            string res = string.Empty;
            if (!string.IsNullOrEmpty(val) && !string.IsNullOrEmpty(bizID))
            {
                if (val.ToUpper() == "LIKE" || val == "1")
                {

                    res = Like(Convert.ToInt64(bizID));
                }
                else
                {
                    res = Favourite(Convert.ToInt64(bizID));
                }
            }
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        //Mail for fav business
        protected void SendFavioriteBizCred(string ContactPerson, string EmailID, string UserName, string BusinessName, string Area, string State)
        {
            string location = string.Empty;
            string vcusername = UserName;
            string bizName = BusinessName.ToString().Trim();
            string Message = "";
            System.Net.Mail.SmtpClient SMPT = new System.Net.Mail.SmtpClient();
            System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage();
            System.IO.FileStream FsContent = new System.IO.FileStream(Server.MapPath("~/resource/email_templates/FavoriteBussiness.html"), System.IO.FileMode.Open, System.IO.FileAccess.Read);
            System.IO.StreamReader SRcontent = new System.IO.StreamReader(FsContent);
            Message = SRcontent.ReadToEnd();
            FsContent.Close();
            SRcontent.Close();
            FsContent.Dispose();
            SRcontent.Dispose();
            Message = Message.Replace("{0}", ContactPerson);
            Message = Message.Replace("{1}", UserName);
            Message = Message.Replace("{2}", BusinessName);
            if (Area != "")
            { location = ", " + Area; }
            if (State != "")
            { location = location + ", " + State; }
            Message = Message.Replace("{3}", location);
            Message = Message.Replace("{20}", "www.vconnect.com");
            //Email.Subject = ConfigurationManager.AppSettings["BizAddFavorite"].ToString();
            Email.Subject = bizName.ToString() + " is one of " + vcusername.ToString() + "'s Favorite";
            Email.From = new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["networkUserIdSupport"].ToString(), "VConnect.com");
            Email.IsBodyHtml = true;
            Email.Body = Message;
            Email.To.Add(EmailID);
            //Email.To.Add("connect2divya@gmail.com");
            SendEmail send = new SendEmail();
            try
            {
                send.SendMailToUser(ref Email, ConfigurationManager.AppSettings["networkUserIdSupport"].ToString(), ConfigurationManager.AppSettings["networkUserIdPasswordSupport"].ToString());
            }
            catch (Exception exp)
            {
                log.LogMe(exp);
            }
        }
    }
}