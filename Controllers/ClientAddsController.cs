﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Vconnect.Common;
using Vconnect.Models;

namespace Vconnect.Controllers
{
    public class ClientAddsController : Controller
    {
        UserSession objUS = new UserSession();
        //
        // GET: /ClientAdds/

        public ActionResult naijafoodfiesta()
        {
            ClientAdds clients = new ClientAdds();
            clients.SuccessMessage = TempData["SuccessMessage"] as string;
            return View(clients);
        }
        [HttpPost]
        public ActionResult naijafoodfiesta(ClientAdds clients)
        {
            string clientName ="Naija Food Fiesta", url=string.Empty;
            int exeResult = 0;
            bool a = true;
            if (clients.emailId != null)
            {
                Regex regEmail = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
                a = regEmail.IsMatch(clients.emailId);
            }
            int result = 10;
            if (clients.phoneNumber != null)
            {
                result = Vconnect.Common.Utility.isValidPhone(clients.phoneNumber);
            }
            if (clients.userName == null)
            {
                ViewBag.Msg = "Enter your name";
            }
            else if (result == 0)
            {
                ViewBag.Msg = "Invalid contact number";
            }
            else if (a == false)
            {
                ViewBag.Msg = "Invalid Email-Id";
            }
            else
            {
                try
                {
                    exeResult = clients.AddUserReg(clientName, clients.userName, !string.IsNullOrEmpty(clients.businessName) ? clients.businessName : string.Empty, clients.emailId, clients.phoneNumber, !string.IsNullOrEmpty(clients.userQuery) ? clients.userQuery : string.Empty);
                    if (exeResult > 0)
                    {
                        ViewBag.Msg = "Some Error Occured";
                    }
                    else
                    {
                        clients.SuccessMessage = "You have successfully Registered";
                        TempData["SuccessMessage"] = "You have successfully Registered";
                        url = RoutingUtility.GetClientAddURL("ss");
                        return Redirect(url);
                    }
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
            }
            return View(clients);
        }

    }
}
