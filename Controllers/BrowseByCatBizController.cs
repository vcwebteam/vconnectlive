﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vconnect.Models;
using Vconnect.Common;
using Vconnect.Mapping.ListingWEB;
using Vconnect.Mapping;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity.Infrastructure;
using System.IO;
using Vconnect.Mapping.CommonDb;
using System.Configuration;

namespace Vconnect.Controllers
{
    public class BrowseByCatBizController : Controller
    {
        //
        // GET: /BrowseByCatBiz/
        UserSession objUS = new UserSession();
        [HttpGet]
        public ActionResult Index(int? page, string tagname, string loc)
        {

            string newUrl = urlRedirectionCatList();
            if (newUrl != "")
            {
                Response.Clear(); Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();
            }
            BrowseByCatBizModel browseByCatBizModel = new BrowseByCatBizModel();
            if (tagname == null)
               return  Redirect("/404");

            string hostHeader = Request.Url.AbsoluteUri.ToString();
            browseByCatBizModel.SearchText = tagname.ToString();
            browseByCatBizModel.SearchLocation = loc;
            if (hostHeader.IndexOf("categories") != -1)
                browseByCatBizModel.type = 2;
            else if (hostHeader.IndexOf("products") != -1)
                browseByCatBizModel.type = 6;
            else if (hostHeader.IndexOf("services") != -1)
                browseByCatBizModel.type = 7;
            else if (hostHeader.IndexOf("brands") != -1)
                browseByCatBizModel.type = 3;
            else
                browseByCatBizModel.type = 1;

            browseByCatBizModel.rowsPerPage = 20;
            if (page == null)
                browseByCatBizModel.pageNum = 1;
            else
                browseByCatBizModel.pageNum = Convert.ToInt16(page);

            browseByCatBizModel.getResult();



            ViewBag.Pager = Pager.Items(Int32.Parse(browseByCatBizModel.totalRes.ToString())).PerPage(Int32.Parse(browseByCatBizModel.rowsPerPage.ToString()) * 2).Move(page ?? 1).Segment(5).Center();


            return View(browseByCatBizModel);


        }
        public string urlRedirectionCatList()
        {
            string websiteRawPath = ConfigurationManager.AppSettings["WebsiteRawPath"].ToString();
            string wapRawPath = ConfigurationManager.AppSettings["wapRawPath"].ToString();
            if (Request.Url.AbsoluteUri.ToString() != Request.Url.AbsoluteUri.ToString().ToLower())
            {
                if (Request.Url.AbsoluteUri.ToString().IndexOf("?") != -1)
                {
                    if (Request.Url.AbsoluteUri.ToString().Substring(0, Request.Url.AbsoluteUri.ToString().IndexOf("?") - 1) != Request.Url.AbsoluteUri.ToString().Substring(0, Request.Url.AbsoluteUri.ToString().IndexOf("?") - 1).ToLower())
                    {
                        //Response.Clear(); Response.Status = "301 Moved Permanently";
                        //Response.AddHeader("Location", Request.Url.AbsoluteUri.ToString().ToLower()); Response.End();                        
                        //return null;
                         string newUrl = System.Web.HttpContext.Current.Request.RawUrl.ToLower();
                         return newUrl = websiteRawPath + newUrl;
                    }
                }
                else
                {
                    string newUrl = System.Web.HttpContext.Current.Request.RawUrl.ToLower();
                    return newUrl = websiteRawPath + newUrl;
                    //Response.Clear(); Response.Status = "301 Moved Permanently";
                    //Response.AddHeader("Location", Request.Url.AbsoluteUri.ToString().ToLower()); Response.End();                    
                    //return null;
                }
            }
            if ((System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString().ToLower().IndexOf("_-page") != -1)
                    || (System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString().ToLower().IndexOf("-_page") != -1))
            {

                string newUrl = System.Web.HttpContext.Current.Request.RawUrl.ToLower();
                return newUrl = websiteRawPath + newUrl.Replace("_-page", "-page").Replace("-_page", "-page");

            }
            //SEO ISSUE Duplicacy issue      --http://localhost:1202/code/nigeria/businesses-from-ameritech-international-surulere-lagos~b57497
            if (System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("businesses-from-") != -1 && (System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("~b") != -1))
            {
                int istartLength = 0;
                string newUrl = (System.Web.HttpContext.Current.Request.RawUrl.ToLower());
                istartLength = newUrl.ToLower().IndexOf("businesses-from-");
                newUrl = (newUrl.Substring(istartLength).ToString());
                return websiteRawPath + newUrl.Replace("businesses-from-", "/").Replace("~b", "_b");

            }    //www.vconnect.com/rivers-ogu_~bolo/categories-from-p
            else if (System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("_~") != -1
                    && System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("responseajaxdisplay.aspx") == -1)
            {
                string newUrl = System.Web.HttpContext.Current.Request.RawUrl.ToLower();
                return newUrl = websiteRawPath + newUrl.Replace("_~", "_");

            }
            //SEO ISSUE Duplicacy issue
            else if (Request.QueryString["state"] != null)
            {
                //http://localhost:1202/code/pages/lagos-ikeja/businesses-from-a
                if (Request.QueryString["state"].ToString().ToLower().IndexOf("pages/") != -1)
                {
                    string newUrl = System.Web.HttpContext.Current.Request.RawUrl.ToLower();

                    return newUrl = websiteRawPath + newUrl.Replace("pages/", "");

                }
                //http://localhost:1202/code/lagos/list-browsebycategory.aspx_searchlocation=nigeria/businesses-from-z
                else if (System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("/list-browsebycategory.aspx_searchlocation") != -1)
                {
                    string newUrl = System.Web.HttpContext.Current.Request.RawUrl.ToLower();

                    return newUrl = websiteRawPath + newUrl.Replace("/list-browsebycategory.aspx_searchlocation-nigeria", "");


                }
                //http://localhost:1202/code/lagos-lagos/brands-from-a
                else if (System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString().ToLower().IndexOf("state=") != -1
                    && System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString().ToLower().IndexOf("searchlocation=") != -1
                    && (Request.QueryString["searchlocation"].ToString().ToLower() == Request.QueryString["state"].ToString().ToLower())
                    && System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("responseajaxdisplay.aspx") == -1)
                {
                    string newUrl = System.Web.HttpContext.Current.Request.RawUrl.ToLower();
                    string[] array1;
                    array1 = newUrl.Split('/');
                    return newUrl = websiteRawPath + newUrl.Replace(array1[1], Request.QueryString["state"].ToString());

                }
                else
                {
                    return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }
        }
        #region wap
        public ActionResult BrowseByBussiness()
        {
            return View();
        }
        public ActionResult BrowseByCategories()
        {
            return View();
        }
        public ActionResult BrowseByProducts()
        {
            return View();
        }
        public ActionResult BrowseByServices()
        {
            return View();
        }
        #endregion
    }
}
