﻿using DotNetOpenAuth.AspNet;
using Facebook;
using Microsoft.Web.WebPages.OAuth;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Globalization;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Vconnect.Common;
using Vconnect.Enums;
using Vconnect.Models;
using Google.GData.Apps;
using Google.GData.Extensions.Apps;
using Google.GData.Client;
using Google.Contacts;
using Google.GData.Apps.Groups;
using System.IO;
using Google.GData.Contacts;
using System.Runtime.Serialization.Json;
using System.Security.Cryptography;
using MailChimp.Types;

namespace Vconnect.Controllers
{
    //Created by Nishant 
    public class AccountController : Controller
    {

        #region Declarations
        // oauth application keys twitter
        string oauth_consumer_key = "96ZV9fuJQnQLdrRSSWJdVQ";
        string oauth_consumer_secret = "5QOT5c2GcgC2jGtpBwVUlDWAZIZNFSjxDOSO3svzY";
        string oauth_token = "";
        string oauth_token_secret = "";
        // oauth application keys twitter

        private UserSession objUS = new UserSession();
        private string webconfigWebsiteRootPath = ConfigurationManager.AppSettings["WebsiteRootPath"].ToString();
        public string reversedns = string.Empty;
        private string DefaultloginControllerName = string.Empty;
        private string DefaultloginActionName = string.Empty;
        private string source ="Web";
        private string rturl = string.Empty;
        private HttpCookie VCverifycode;
        private int flagFBEmailIsVerified = 1;
        private long resIsInt = 0;
        private int sourceCodeVal = 0;
        private string srcReturnVal=string.Empty;
        private string url;
        private GoogleId gId;
        /*For local host*/
        //private const string clientID = "87532421247-3fmtmsgupk4ot11qv91mt8ld4sn1nveo.apps.googleusercontent.com";
        //private const string clientSecret = "LEPfcsa1gj2EAfM2DeZ5hRjC";
        //private const string redirectURIs = "http://localhost:31784/Account/oauth2callback";
        /*For local host*/

        /*For beta*/
        //private const string clientID = "87532421247-8b98h5t3v8ghfpc9j4uibgk6altmvdka.apps.googleusercontent.com";
        //private const string clientSecret = "38_EjbTQqQvFrRI_PWULqVzr";
        //private const string redirectURIs = "http://b.vconnect.co/Account/oauth2callback";
        /*For beta*/


        /*For vconnect*/
        //private const string clientID = "358707076312-h6dfit1802g4f03fcsnlrobki4tknutl.apps.googleusercontent.com";
        //private const string clientSecret = "yxHwsdbfjvqEbrcFJ5avHOed";
        //private const string redirectURIs = "http://www.vconnect.com/Account/oauth2callback";
        /*For vconnect*/
        private string clientID = ConfigurationManager.AppSettings["gmailclientid"].ToString().Trim();
        private string clientSecret = ConfigurationManager.AppSettings["gmailclientsecret"].ToString().Trim();
        private string redirectURIs = ConfigurationManager.AppSettings["gmailredirecturl"].ToString().Trim();




        //for twitter
        public string strTwiterFollowers { get; set; }
        public string strTwiterFriends { get; set; }
        public string authHeader = string.Empty;
        public string resource_url = string.Empty;
        //for twitter
        #endregion

        [HttpGet]
        [AllowAnonymous]
        [ValidateInput(false)]
        public ActionResult Login()
        {

            try
            {
                //if (System.Web.HttpContext.Current.Request.Cookies["VCLoginID"] != null && System.Web.HttpContext.Current.Request.Cookies["VCLoginID"].ToString().Trim() != "")
                if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString().Trim() != "")
                    return RedirectPermanent("/");

                if (System.Web.HttpContext.Current.Session["loginerror"] != null)
                {
                    if (System.Web.HttpContext.Current.Session["loginerror"].ToString().Trim() != "")
                    {
                        ViewBag.UserMsg = System.Web.HttpContext.Current.Session["loginerror"];
                        System.Web.HttpContext.Current.Session["loginerror"] = "";
                    }
                }

                System.Web.HttpContext.Current.Session["rturl"] = null;
                if (Request.QueryString["rturl"] != null && Request.QueryString["rturl"].ToString().Trim() != "")
                {
                    System.Web.HttpContext.Current.Session["rturl"] = Request.QueryString["rturl"].ToString().Trim().Replace("$", "&");
                    if (Request.QueryString["provider"] != null && Request.QueryString["provider"].ToString().Trim() != "")
                    {
                        return new ExternalLoginResult(Request.QueryString["provider"].ToString().Trim(), Url.Action("ExternalLoginCallback", new { Provider = Request.QueryString["provider"].ToString().Trim(), rturl = Request.QueryString["rturl"].ToString().Trim() }));
                    }
                }
                else if (Request.QueryString["src"] != null && Request.QueryString["src"].ToString().Trim() != "")
                {
                    System.Web.HttpContext.Current.Session["rturl"] = CatchRTUrl(Request.QueryString["src"].ToString().Trim());
                    if (Request.QueryString["provider"] != null && Request.QueryString["provider"].ToString().Trim() != "")
                    {
                        return new ExternalLoginResult(Request.QueryString["provider"].ToString().Trim(), Url.Action("ExternalLoginCallback", new { Provider = Request.QueryString["provider"].ToString().Trim(), rturl = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim() }));
                    }
                }
                else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
                {
                    if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"] != null
                        && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"].ToString().Trim() != "")
                    {
                        System.Web.HttpContext.Current.Session["rturl"] = CatchRTUrl(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"].ToString().Trim());
                        //if (Request.QueryString["provider"] != null && Request.QueryString["provider"].ToString().Trim() != "")
                        //{
                        //    return new ExternalLoginResult(Request.QueryString["provider"].ToString().Trim(), Url.Action("ExternalLoginCallback", new { Provider = Request.QueryString["provider"].ToString().Trim(), rturl = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim() }));
                        //}
                    }
                }

                LoginModel model = new LoginModel();
                if (!string.IsNullOrWhiteSpace(Request.QueryString["br"]) && !string.IsNullOrEmpty(Request.QueryString["br"]))
                {
                    model.BR = !string.IsNullOrWhiteSpace(Request.QueryString["br"].ToString().Trim()) ? Request.QueryString["br"].ToString().Trim() : "";
                    model.BID = !string.IsNullOrWhiteSpace(Request.QueryString["bid"].ToString().Trim()) ? Int32.Parse(Request.QueryString["bid"].ToString().Trim()) : 0;
                }

                if (!string.IsNullOrWhiteSpace(Request.QueryString["email"]) && !string.IsNullOrEmpty(Request.QueryString["email"]))
                {
                    model.QueryParameterEmail = !string.IsNullOrWhiteSpace(Request.QueryString["email"].ToString().Trim()) ? Request.QueryString["email"].ToString().Trim() : "";
                    model.QueryParameterEmailVerificationCode = !string.IsNullOrWhiteSpace(Request.QueryString["verifyCode"].ToString().Trim()) ? Request.QueryString["verifyCode"].ToString().Trim() : "";
                }

                if (System.Web.HttpContext.Current.Session["model"] != null)
                {
                    Type t = System.Web.HttpContext.Current.Session["model"].GetType();
                    if (t == typeof(LoginModel))
                        model = (LoginModel)System.Web.HttpContext.Current.Session["model"];
                }
                else
                {
                    model.IsDuplicate = "0";
                    System.Web.HttpContext.Current.Session["model"] = model;
                }

                return View(model);
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                if (System.Web.HttpContext.Current.Session["rturl"] != null)
                    return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Replace("&", "$"));
                else
                    return RedirectPermanent("/login");
            }
        }

        public ActionResult oauth2callback(string state, string code, string error)
        {
            if (error != null)
            {
                ViewBag.error = error;
                return RedirectPermanent("/login");
            }

            int indx = state.IndexOf("$");
            if (indx != -1)
            {
                System.Web.HttpContext.Current.Session["rturl"] = state.Substring((indx + 7), (state.Length - (indx + 7)));
                state = state.Substring(0, indx);

                if (System.Web.HttpContext.Current.Session["rturl"] != null && System.Web.HttpContext.Current.Session["rturl"].ToString().Contains("userconnect"))
                {
                    var gop = new GoogleOauthParameters
                    {
                        Code = code,
                        ClientId = clientID,
                        ClientSecret = clientSecret,
                        RedirectURIs = redirectURIs,
                        Rturl = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()
                    };

                    List<GoogleOauthParameters> gopList = new List<GoogleOauthParameters>();
                    gopList.Add(gop);

                    TempData["gopList"] = gopList;
                    TempData.Keep("gopList");
                    return RedirectPermanent("/UserDashboard/oauth2callback");

                }
                else
                {
                    System.Web.HttpContext.Current.Session["rturl"] = CatchRTUrl(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim());
                    rturl = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim();
                }
            }

           
            ViewBag.Message = "Using OAuth 2.0 for Web Server Applications (Experimental)";
            GoogleResponse responseFromServer = new GoogleResponse();
            try
            {
                HttpWebRequest requestToken = (HttpWebRequest)WebRequest.Create("https://accounts.google.com/o/oauth2/token");
                requestToken.Method = "POST";
                string postData = string.Format(
                    "code={0}&client_id={1}&client_secret={2}&redirect_uri={3}&grant_type=authorization_code",
                    code, clientID, clientSecret, redirectURIs
                );
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                requestToken.ContentType = "application/x-www-form-urlencoded";
                requestToken.ContentLength = byteArray.Length;
                using (Stream dataStream = requestToken.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();
                    WebResponse response = requestToken.GetResponse();
                    if (((HttpWebResponse)response).StatusCode == HttpStatusCode.OK)
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            responseFromServer = Deserialise<GoogleResponse>(reader.ReadToEnd());
                            reader.Close();
                            dataStream.Close();
                            response.Close();
                        }
                    }
                    else
                    {
                        dataStream.Close();
                        response.Close();
                        ViewBag.error = ((HttpWebResponse)response).StatusDescription;
                        return RedirectPermanent("/login");
                    }
                }
                if (responseFromServer != null && !string.IsNullOrEmpty(responseFromServer.access_token))
                {
                    url = string.Format("https://www.googleapis.com/oauth2/v1/userinfo?access_token={0}", responseFromServer.access_token);

                    HttpWebRequest requestUserInfo = (HttpWebRequest)WebRequest.Create(url);
                    HttpWebResponse responseUserInfo = (HttpWebResponse)requestUserInfo.GetResponse();
                    if (((HttpWebResponse)responseUserInfo).StatusCode == HttpStatusCode.OK)
                    {
                        using (Stream receiveStream = responseUserInfo.GetResponseStream())
                        {
                            Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                            using (StreamReader readStream = new StreamReader(receiveStream, encode))
                            {
                                gId = Deserialise<GoogleId>(readStream.ReadToEnd());
                                responseUserInfo.Close();
                                readStream.Close();

                                var parameters = new OAuth2Parameters
                                {
                                    /*For local host*/
                                    //ClientId = @"87532421247-3fmtmsgupk4ot11qv91mt8ld4sn1nveo.apps.googleusercontent.com",
                                    //ClientSecret = @"LEPfcsa1gj2EAfM2DeZ5hRjC",
                                    //RedirectUri = @"http://localhost:31784/Account/oauth2callback",
                                    /*For local host*/

                                    /*For beta server*/
                                    //ClientId = "87532421247-8b98h5t3v8ghfpc9j4uibgk6altmvdka.apps.googleusercontent.com",
                                    //ClientSecret = "38_EjbTQqQvFrRI_PWULqVzr",
                                    //RedirectUri = @"http://b.vconnect.co/Account/oauth2callback",
                                    /*For beta server*/

                                    /*For vconnect server*/
                                    //ClientId = "358707076312-h6dfit1802g4f03fcsnlrobki4tknutl.apps.googleusercontent.com",
                                    //ClientSecret = "yxHwsdbfjvqEbrcFJ5avHOed",
                                    //RedirectUri = @"http://www.vconnect.com/Account/oauth2callback",
                                    /*For vconnect server*/

                                    ClientId = ConfigurationManager.AppSettings["gmailclientid"].ToString().Trim(),
                                    ClientSecret = ConfigurationManager.AppSettings["gmailclientsecret"].ToString().Trim(),
                                    RedirectUri = @ConfigurationManager.AppSettings["gmailredirecturl"].ToString().Trim(),

                                    Scope = @"http://www.google.com/m8/feeds",
                                    AccessCode = code,
                                    AccessToken = responseFromServer.access_token,  /* use the value returned from the old call to GetAccessToken here */
                                    RefreshToken = responseFromServer.access_token,

                                };

                                try
                                {
                                    List<FetchContactsModel> contactDetails = new List<FetchContactsModel>();
                                    string App_Name = "MyNetwork Web Application!";
                                    ContactsQuery query = new ContactsQuery(ContactsQuery.CreateContactsUri("default"));
                                    RequestSettings rs = new RequestSettings(App_Name, parameters);
                                    rs.AutoPaging = true;
                                    ContactsRequest cr = new ContactsRequest(rs);
                                    Feed<Contact> f = cr.GetContacts();

                                    int count = 0;
                                    if (f.Entries != null)
                                    {
                                        Feed<Contact> feed = cr.GetContacts();
                                        foreach (Contact entry in feed.Entries)
                                        {
                                            FetchContactsModel contact = new FetchContactsModel
                                            {
                                                Name = !string.IsNullOrEmpty(entry.Name.FullName) ? entry.Name.FullName : "",
                                                EmailId = entry.Emails.Count >= 1 ? entry.Emails[0].Address : "",
                                                SecondaryEmail = entry.Emails.Count >= 2 ? entry.Emails[1].Address : "",
                                                Phonenumber = entry.Phonenumbers.Count >= 1 ? entry.Phonenumbers[0].Value : "",
                                                Source = "Website",
                                            };

                                            contactDetails.Add(contact);
                                        }
                                    }

                                    TempData["gousers"] = contactDetails;
                                }
                                catch(Exception ex){}

                                Dictionary<string, string> userData = new Dictionary<string, string>();
                                userData.Add("id", gId.id);
                                userData.Add("username", !string.IsNullOrEmpty(gId.name) ? gId.name.Trim() : "");
                                userData.Add("email", !string.IsNullOrEmpty(gId.email) ? gId.email.Trim() : "");

                                return RegisterGoogleUser(userData);

                            }
                        }
                    }
                }




            }
            catch (System.Net.WebException ex)
            {
                log.LogMe(ex);
                //ViewBag.error = ex.Message;
                return RedirectPermanent("/login");
            }
            //ViewBag.error = "Something went wrong.";
            return RedirectPermanent("/login");
        }

        public static T Deserialise<T>(string json)
        {
            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(json)))
            {
                var serialiser = new DataContractJsonSerializer(typeof(T));
                return (T)serialiser.ReadObject(ms);
            }
        }


     
        public static object CatchRTUrl(string src)

        {
            string rturl = "";
            switch (src)
            {
                case "smsc":
                    rturl = "http://vconnectors.vconnect.com/send.aspx";
                    break;

                case "events":
                    rturl = "http://events.vconnect.com/";
                    break;

                case "movies":
                    rturl = "http://movies.vconnect.com/";
                    break;


                default:
                    rturl = "/";
                    break;
            }
            System.Web.HttpContext.Current.Session["rturl"] = rturl;

            return rturl;
        }

        #region For Vconnect-WebLogin

        [HttpPost]
        [AllowAnonymous]
        [ValidateInput(false)]
        public ActionResult Login(LoginModel myModel, string email)
        {
            if (Request.QueryString["rturl"] != null && Request.QueryString["rturl"].ToString().Trim() != "" && System.Web.HttpContext.Current.Session["rturl"] == null)
                System.Web.HttpContext.Current.Session["rturl"] = Request.QueryString["rturl"].ToString().Trim();
            else if (Request.QueryString["src"] != null && Request.QueryString["src"].ToString().Trim() != "" && System.Web.HttpContext.Current.Session["rturl"] == null)
            {
                System.Web.HttpContext.Current.Session["rturl"] = CatchRTUrl(Request.QueryString["src"].ToString().Trim());
            }

            if (string.IsNullOrWhiteSpace(myModel.Login) || string.IsNullOrEmpty(myModel.Login) || myModel.Login.Trim() == "email address")
            {
                System.Web.HttpContext.Current.Session["loginerror"] = "<p style=\"color:red;\">please enter a valid email/mobile no</p>";
                if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                    return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                else
                    return RedirectPermanent("/login");
            }
            if (string.IsNullOrEmpty(myModel.Password) || string.IsNullOrWhiteSpace(myModel.Password) || myModel.Password.Trim() == "password")
            {
                System.Web.HttpContext.Current.Session["loginerror"] = "<p style=\"color:red;\">please enter a password</p>";
                if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                    return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                else
                    return RedirectPermanent("/login");
            }


            try
            {
                if ((System.Web.HttpContext.Current.Session["rturl"] == null || System.Web.HttpContext.Current.Session["rturl"].ToString().Trim() == "") && (Request.QueryString["rturl"] != null && Request.QueryString["rturl"].ToString().Trim() != ""))
                {
                    System.Web.HttpContext.Current.Session["rturl"] = Request.QueryString["rturl"].ToString().Trim().Replace("$", "&");
                    if (Request.QueryString["provider"] != null && Request.QueryString["provider"].ToString().Trim() != "")
                    {
                        return new ExternalLoginResult(Request.QueryString["provider"].ToString().Trim(), Url.Action("externallogincallback", new { provider = Request.QueryString["provider"].ToString().Trim(), rturl = Request.QueryString["rturl"].ToString().Trim() }));
                    }
                }
                else if ((System.Web.HttpContext.Current.Session["rturl"] == null || System.Web.HttpContext.Current.Session["rturl"].ToString().Trim() == "") && (Request.QueryString["src"] != null && Request.QueryString["src"].ToString().Trim() != ""))
                {
                    System.Web.HttpContext.Current.Session["rturl"] = CatchRTUrl(Request.QueryString["src"].ToString().Trim());
                    if (Request.QueryString["provider"] != null && Request.QueryString["provider"].ToString().Trim() != "")
                    {
                        return new ExternalLoginResult(Request.QueryString["provider"].ToString().Trim(), Url.Action("externallogincallback", new { provider = Request.QueryString["provider"].ToString().Trim(), rturl = Request.QueryString["rturl"].ToString().Trim() }));
                    }
                }

                LoginModel model = new LoginModel();
                if (System.Web.HttpContext.Current.Session["model"] != null)
                {
                    Type t = System.Web.HttpContext.Current.Session["model"].GetType();
                    if (t == typeof(LoginModel) && model.Login != null && model.Login.Trim() != "" && model.Password != null && model.Password.Trim() != "")
                    {
                        model = (LoginModel)System.Web.HttpContext.Current.Session["model"];
                        model.Login = (model.Login) == null ? "" : model.Login.Trim();
                        model.Password = (model.Password) == null ? "" : model.Password.Trim();
                    }
                    else
                    {
                        model = myModel;
                        model.Password = (myModel.Password) == null ? "" : myModel.Password.Trim();
                        model.Login = (myModel.Login) == null ? "" : myModel.Login.Trim();
                    }
                }
                else
                {
                    model = myModel;
                    model.Password = (myModel.Password) == null ? "" : myModel.Password.Trim();
                    model.Login = (myModel.Login) == null ? "" : myModel.Login.Trim();
                }

                //if (system.web.httpcontext.current.request.cookies["vcloginid"] != null && model.Password != null && model.Login != null && system.web.httpcontext.current.objUS.SV_VCLoginID.ToString().Trim() != "" && system.web.httpcontext.current.objUS.SV_VCLoginID.ToString().Trim() != "0" && system.web.httpcontext.current.objUS.SV_VCLoginID.ToString().Trim() == model.Login.ToString().Trim() && system.web.httpcontext.current.request.cookies["vcpassword"].value.ToString().Trim() == model.Password.ToString().Trim())
                //{
                //    if (system.web.httpcontext.current.request.cookies["vcloginid"] != null && system.web.httpcontext.current.objUS.SV_VCLoginID.ToString().Trim() != "" && system.web.httpcontext.current.objUS.SV_VCLoginID.ToString() != "0" && system.web.httpcontext.current.objUS.SV_VCLoginID.ToString().Trim() == model.Login.Trim() && system.web.httpcontext.current.request.cookies["vcpassword"].value.ToString().Trim() == model.Password.Trim())
                //    {
                //        if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                //        {
                //            string rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim();
                //            return RedirectPermanent(rturl);
                //        }
                //        else
                //            return RedirectPermanent("/");
                //    }
                //    else
                //    {
                //        System.Web.HttpContext.Current.Session["model"] = model;
                //        return redirecttoroute("wblogin");
                //    }
                //}
                //else
                //{
                //    System.Web.HttpContext.Current.Session["model"] = model;
                //    return redirecttoroute("wblogin");
                //    return wblogin();
                //}
                if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && model.Password != null && model.Login != null && objUS.SV_VCLoginID.ToString().Trim() != "" && objUS.SV_VCLoginID.ToString().Trim() != "0" && objUS.SV_VCLoginID.ToString().Trim() == model.Login.ToString().Trim() && System.Web.HttpContext.Current.Request.Cookies["VCPassword"].Value.ToString().Trim() == model.Password.ToString().Trim())
                {
                    if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString().Trim() != "" && objUS.SV_VCLoginID.ToString() != "0" && objUS.SV_VCLoginID.ToString().Trim() == model.Login.Trim() && System.Web.HttpContext.Current.Request.Cookies["VCPassword"].Value.ToString().Trim() == model.Password.Trim())
                    {
                        if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                        {
                            string rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim();
                            return RedirectPermanent(rturl);
                        }
                        else
                            return RedirectPermanent("/");
                    }
                    else
                    {
                        System.Web.HttpContext.Current.Session["model"] = model;
                        return RedirectToRoute("wblogin");
                    }
                }
                else
                {
                    System.Web.HttpContext.Current.Session["model"] = model;
                    //return RedirectToRoute("wblogin");
                    return wblogin();
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                    return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                else
                    return RedirectPermanent("/login");
            }
        }

        [ValidateInput(false)]
        public ActionResult wblogin()
        {
            try
            {
                string rturl = "";
                if (System.Web.HttpContext.Current.Session["model"] == null)
                {
                    if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                        return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                    else
                        return RedirectPermanent("/login");
                }
                LoginModel model = (LoginModel)System.Web.HttpContext.Current.Session["model"];
                string VCLoginPassword = string.Empty;
                string VCLoginName = string.Empty;
                string VCUserContentID = string.Empty;
                bool VCIsMobileVerified = false;
                string VCUserBusinessID = string.Empty;
                string VCUserType = string.Empty;
                string VCUserEmail = string.Empty;
                string VCUserPhone = string.Empty;
                string VCUserStateid = string.Empty;
                string VCUserCityid = string.Empty;
                string VCUserState = string.Empty;
                string VCUserCity = string.Empty;
                string VCisagent = string.Empty;
                string retrnValFlagEmail = string.Empty;
                string VCUserPhoto = string.Empty;
                string VCUserPhotoMobile = string.Empty;               
                string VCUserUrl = string.Empty;


                reversedns = Utility.GetReversedns(Utility.GetIpAddress());


                System.Web.HttpContext.Current.Session["lgnProvider"] = "vc";
                System.Web.HttpContext.Current.Session["loginError"] = string.Empty;

                //Login Function
                DataSet ds;
                ds = LoginUser(model);

                if (ds == null || ds.Tables.Count <= 0)
                {
                    if (System.Web.HttpContext.Current.Session["loginError"] == null || string.IsNullOrWhiteSpace(System.Web.HttpContext.Current.Session["loginError"].ToString()))
                          System.Web.HttpContext.Current.Session["loginError"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Incorrect Login ID/Password.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";

                    if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                        return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                    else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
                    {

                        if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"].ToString().Trim() != "")
                        {
                            rturl = System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"].ToString().Trim();
                            return RedirectPermanent("/login?rturl=" + rturl.Replace("&", "$"));
                        }
                        else
                        {
                            return RedirectPermanent("/");
                        }
                    }
                    else
                        return RedirectPermanent("/login"); 
                }

                if (ds.Tables["tableUserDetails"] != null)
                {
                    if (ds.Tables["tableUserDetails"].Rows.Count > 0)
                    {
                        model.UID = !string.IsNullOrWhiteSpace(ds.Tables["tableUserDetails"].Rows[0]["contentid"].ToString()) ? Int32.Parse(ds.Tables["tableUserDetails"].Rows[0]["contentid"].ToString().Trim()) : 0;
                        VCLoginPassword = !string.IsNullOrWhiteSpace(ds.Tables["tableUserDetails"].Rows[0]["password"].ToString()) ? ds.Tables["tableUserDetails"].Rows[0]["password"].ToString().Trim() : "";
                    }
                }

                if (model.Password != null && VCLoginPassword!=null)
                {
                    if (VCLoginPassword != model.Password.Trim())
                    {
                        if (System.Web.HttpContext.Current.Session["loginError"] == null || System.Web.HttpContext.Current.Session["loginError"].ToString().Trim() == "")
                        {
                            System.Web.HttpContext.Current.Session["loginError"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Incorrect Login ID/Password.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                        }
                            if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                                return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                        else
                                return RedirectPermanent("/login");
                    }
                }
                else
                {
                    System.Web.HttpContext.Current.Session["loginError"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Incorrect Login ID/Password.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                    if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                        return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                    else
                        return RedirectPermanent("/login");
                }


                if (VCLoginPassword != null)
                {
                    if (ds.Tables["tableUserDetails"].Rows.Count > 0)
                    {
                        if (new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,6}$").IsMatch(model.Login.Trim()))
                        {

                            if ((retrnValFlagEmail != "1") && (string.IsNullOrWhiteSpace(ds.Tables["tableUserDetails"].Rows[0]["isemailverified"].ToString()) || ds.Tables["tableUserDetails"].Rows[0]["isemailverified"].ToString().Trim() == "0" || ds.Tables["tableUserDetails"].Rows[0]["isemailverified"].ToString().Trim().ToLower() == "false"))
                            {
                                model.CheckEmailVerification = 1;
                                model.CheckMobileVerification = 0;
                                System.Web.HttpContext.Current.Session["model"] = model;
                                return RedirectToRoute("verification");
                            }


                            if ((!string.IsNullOrWhiteSpace(ds.Tables["tableUserDetails"].Rows[0]["phone"].ToString()) && ds.Tables["tableUserDetails"].Rows[0]["phone"].ToString().Trim() != "0") && (string.IsNullOrWhiteSpace(ds.Tables["tableUserDetails"].Rows[0]["ismobileverified"].ToString()) || ds.Tables["tableUserDetails"].Rows[0]["ismobileverified"].ToString().Trim() == "0" || ds.Tables["tableUserDetails"].Rows[0]["ismobileverified"].ToString().Trim().ToLower() == "false"))
                            {
                                model.CheckEmailVerification = 0;
                                model.CheckMobileVerification = 1;
                                model.PhoneNo = ds.Tables["tableUserDetails"].Rows[0]["phone"].ToString().Trim();
                                System.Web.HttpContext.Current.Session["model"] = model;
                                return RedirectToRoute("verification");
                            }

                        }
                        else
                        {
                            if ((!string.IsNullOrWhiteSpace(ds.Tables["tableUserDetails"].Rows[0]["phone"].ToString()) && ds.Tables["tableUserDetails"].Rows[0]["phone"].ToString().Trim() != "0") && (string.IsNullOrWhiteSpace(ds.Tables["tableUserDetails"].Rows[0]["ismobileverified"].ToString()) || ds.Tables["tableUserDetails"].Rows[0]["ismobileverified"].ToString().ToLower() == "0" || ds.Tables["tableUserDetails"].Rows[0]["ismobileverified"].ToString().Trim().ToLower() == "false"))
                            {
                                model.CheckEmailVerification = 0;
                                model.CheckMobileVerification = 1;
                                model.PhoneNo = ds.Tables["tableUserDetails"].Rows[0]["phone"].ToString().Trim();
                                System.Web.HttpContext.Current.Session["model"] = model;
                                return RedirectToRoute("verification");
                            }

                            else if ((retrnValFlagEmail != "1") && (ds.Tables["tableUserDetails"].Rows[0]["email"].ToString().Trim() == "0" || ds.Tables["tableUserDetails"].Rows[0]["email"].ToString().Trim() == ""))
                            {
                                System.Web.HttpContext.Current.Session["model"] = model;
                                return RedirectToRoute("updateuseremail");
                            }

                            else if ((retrnValFlagEmail != "1") && (string.IsNullOrWhiteSpace(ds.Tables["tableUserDetails"].Rows[0]["isemailverified"].ToString()) || ds.Tables["tableUserDetails"].Rows[0]["isemailverified"].ToString().Trim() == "0" || ds.Tables["tableUserDetails"].Rows[0]["isemailverified"].ToString().Trim().ToLower() == "false"))
                            {
                                model.CheckEmailVerification = 1;
                                model.CheckMobileVerification = 0;
                                System.Web.HttpContext.Current.Session["model"] = model;
                                return RedirectToRoute("verification");
                            }

                        }


                        //model.ListResultTableAccountBusinessMappingModel = WEBLoginDatabaseCall.DBGetAccountMappingDetails(model.Login);
                        //if (model.ListResultTableAccountBusinessMappingModel.UserAccountMappingModel.Count > 1)
                        //{
                        //    model.IsDuplicate = "1";
                        //    System.Web.HttpContext.Current.Session["model"] = model;

                        //    if (MyGlobalVariables.IsWapWeb != "WAP")
                        //    {
                        //        StringBuilder sbUserAccountMappingPopup = new StringBuilder();
                        //        sbUserAccountMappingPopup.AppendLine(" <script> ");
                        //        sbUserAccountMappingPopup.AppendLine(" if (document.getElementById('change-phone').style.display == 'none') { ");
                        //        sbUserAccountMappingPopup.AppendLine(" document.getElementById('change-phone').style.display = 'block'; ");
                        //        sbUserAccountMappingPopup.AppendLine(" document.getElementById('fade').style.display = 'block' ");
                        //        sbUserAccountMappingPopup.AppendLine(" } ");
                        //        sbUserAccountMappingPopup.AppendLine(" document.getElementById('change-phone').style.display = 'block'; ");
                        //        sbUserAccountMappingPopup.AppendLine(" document.getElementById('fade').style.display = 'block' ");
                        //        sbUserAccountMappingPopup.AppendLine(" </script> ");

                        //        System.Web.HttpContext.Current.Session["loginError"] = sbUserAccountMappingPopup.ToString();

                        //        //ViewBag.returnUrlActionName = returnUrlActionName;
                        //        //ViewBag.returnUrlControllerName = returnUrlControllerName;
                        //        if (System.Web.HttpContext.Current.Session["rturl"] == null)
                        //            return Redirect("/login");
                        //        //return RedirectToAction("Login",model);
                        //        else
                        //            return Redirect("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Replace("&", "$"));
                        //        //return RedirectToAction(defaultaction, defaultcontroller, new { returnUrlActionName, returnUrlControllerName });
                        //    }
                        //    else
                        //    {
                        //        System.Web.HttpContext.Current.Session["loginError"] = string.Empty;
                        //        return RedirectToActionPermanent("UserAccountMapping");
                        //    }


                        //}
                        VCUserBusinessID = "0";
                        VCLoginName = string.IsNullOrWhiteSpace(ds.Tables["tableUserDetails"].Rows[0]["contactname"].ToString()) ? "" : ds.Tables["tableUserDetails"].Rows[0]["contactname"].ToString();
                        //VCLoginName += string.IsNullOrWhiteSpace(ds.Tables["tableUserDetails"].Rows[0]["lastname"].ToString()) ? "" : ds.Tables["tableUserDetails"].Rows[0]["lastname"].ToString();
                        VCUserContentID = string.IsNullOrWhiteSpace(ds.Tables["tableUserDetails"].Rows[0]["contentid"].ToString()) ? "0" : ds.Tables["tableUserDetails"].Rows[0]["contentid"].ToString();
                        VCUserType = string.IsNullOrWhiteSpace(ds.Tables["tableUserDetails"].Rows[0]["usertype"].ToString()) ? "" : ds.Tables["tableUserDetails"].Rows[0]["usertype"].ToString();
                        VCUserEmail = string.IsNullOrWhiteSpace(ds.Tables["tableUserDetails"].Rows[0]["email"].ToString()) ? "" : ds.Tables["tableUserDetails"].Rows[0]["email"].ToString();
                        VCUserPhone = string.IsNullOrWhiteSpace(ds.Tables["tableUserDetails"].Rows[0]["phone"].ToString()) ? "" : ds.Tables["tableUserDetails"].Rows[0]["phone"].ToString();
                        VCisagent = string.IsNullOrWhiteSpace(ds.Tables["tableUserDetails"].Rows[0]["isagent"].ToString()) ? "" : ds.Tables["tableUserDetails"].Rows[0]["isagent"].ToString();
                        VCUserPhoto = !string.IsNullOrWhiteSpace(ds.Tables["tableUserDetails"].Rows[0]["photo"].ToString().Trim()) ? ds.Tables["tableUserDetails"].Rows[0]["photo"].ToString().Trim() : "";
                        VCUserPhotoMobile = !string.IsNullOrWhiteSpace(ds.Tables["tableUserDetails"].Rows[0]["photomobile"].ToString().Trim()) ? ds.Tables["tableUserDetails"].Rows[0]["photomobile"].ToString().Trim() : "";
                        VCUserUrl = !string.IsNullOrWhiteSpace(ds.Tables["tableUserDetails"].Rows[0]["userUrl"].ToString().Trim()) ? ds.Tables["tableUserDetails"].Rows[0]["userUrl"].ToString().Trim() : "";

                        if (string.IsNullOrWhiteSpace(ds.Tables["tableUserDetails"].Rows[0]["ismobileverified"].ToString()))
                            VCIsMobileVerified = false;
                        else
                            VCIsMobileVerified = Convert.ToBoolean(ds.Tables["tableUserDetails"].Rows[0]["ismobileverified"].ToString());

                       // ViewBag.UserMsg = "Logged In successfully.";
                        int ischecked = 0;

                        if (model.RememberMe)
                        {
                            ischecked = 1;
                        }
                        
                        //if (UserSession.GenerateSession(source, model.Login.Trim(), VCLoginName.Trim(), VCUserContentID.Trim(), VCIsMobileVerified, VCUserType.Trim(),
                        //    VCUserPhone.Trim(), VCUserEmail.Trim(), VCUserStateid.Trim(), VCUserCityid.Trim(), 0, "", "", 0, 0, 0, VCUserPhoto.Trim(), VCUserPhotoMobile.Trim(), VCUserUrl.Trim()))
                        if (UserSession.GenerateSession(source, model.Login.Trim(), VCLoginName.Trim(), VCUserContentID.Trim(), VCUserType.Trim(), "", VCUserPhoto.Trim(), VCUserUrl, VCUserEmail, VCUserPhone))
                        {
                            if (long.TryParse(VCUserType, out resIsInt) && long.TryParse(VCUserContentID, out resIsInt) && long.TryParse(VCUserBusinessID, out resIsInt))
                            {
                                if (System.Web.HttpContext.Current.Session["rturl"]!=null && System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()!="")
                                {
                                    sourceCodeVal =  ReturnURIValue(System.Web.HttpContext.Current.Session["rturl"].ToString());
                                    if (sourceCodeVal.ToString().Trim() != "" && sourceCodeVal >=0)
                                    {
                                        WEBLoginDatabaseCall.AddLoginUserLog(source, VCLoginName, Convert.ToInt32(VCUserType), model.Login.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), 0, 1, reversedns,sourceCodeVal);
                                    }
                                    else
                                    {
                                        WEBLoginDatabaseCall.AddLoginUserLog(source, VCLoginName, Convert.ToInt32(VCUserType), model.Login.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), 0, 1, reversedns,0);
                                    }
                                }
                                else
                                {
                                    WEBLoginDatabaseCall.AddLoginUserLog(source, VCLoginName, Convert.ToInt32(VCUserType), model.Login.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), 0, 1, reversedns, 0);
                                }

                            }
                                  System.Web.HttpContext.Current.Session["loginError"] = string.Empty;

                            
                            
                                 
                            if (!string.IsNullOrEmpty(objUS.SV_VCUserName) && objUS.SV_VCUserName.ToString().Trim() != "")
                            {
                                MyGlobalVariables._checkLoggedin = 1;
                                Session.Add("_checkLoggedin", "1");
                            }
                            if (!long.TryParse(VCUserContentID, out resIsInt))
                            {
                                VCUserContentID = "0";
                            }


                            if (System.Web.HttpContext.Current.Session["model"] != null)
                            {
                                LoginModel myModel = new LoginModel();
                                Type t = System.Web.HttpContext.Current.Session["model"].GetType();
                                if (t == typeof(LoginModel))
                                {
                                    myModel = (LoginModel)System.Web.HttpContext.Current.Session["model"];
                                    if (!string.IsNullOrWhiteSpace(myModel.BR))
                                    {
                                        myModel.UID = Int32.Parse(VCUserContentID);
                                        if (long.TryParse(myModel.BID.ToString().Trim(), out resIsInt))
                                        {
                                            WEBLoginDatabaseCall.BusinessUpdate(myModel.BID, myModel.UID, 60);
                                        }
                                    }
                                    else
                                    {
                                        if (Request.QueryString["bid"] != null && Request.QueryString["bid"].ToString().Trim() != "" && Request.QueryString["br"] != null && Request.QueryString["bid"].ToString().Trim() != "")
                                        {
                                            if (long.TryParse(Request.QueryString["bid"].ToString().Trim(), out resIsInt))
                                            {
                                                WEBLoginDatabaseCall.BusinessUpdate(long.Parse(Request.QueryString["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                                            }
                                        }
                                        else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
                                        {
                                            if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim() != "" && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"].ToString().Trim() != "")
                                            {
                                                if (long.TryParse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim(), out resIsInt))
                                                {
                                                    WEBLoginDatabaseCall.BusinessUpdate(long.Parse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (Request.QueryString["bid"] != null && Request.QueryString["bid"].ToString().Trim() != "" && Request.QueryString["br"] != null && Request.QueryString["bid"].ToString().Trim() != "")
                                    {
                                        if (long.TryParse(Request.QueryString["bid"].ToString().Trim(), out resIsInt))
                                        {
                                            WEBLoginDatabaseCall.BusinessUpdate(long.Parse(Request.QueryString["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                                        }
                                    }
                                    else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
                                    {
                                        if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim() != "" && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"].ToString().Trim() != "")
                                        {
                                            if (long.TryParse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim(), out resIsInt))
                                            {
                                                WEBLoginDatabaseCall.BusinessUpdate(long.Parse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (Request.QueryString["bid"] != null && Request.QueryString["bid"].ToString().Trim() != "" && Request.QueryString["br"] != null && Request.QueryString["bid"].ToString().Trim() != "")
                                {
                                    if (long.TryParse(Request.QueryString["bid"].ToString().Trim(), out resIsInt))
                                    {
                                        WEBLoginDatabaseCall.BusinessUpdate(long.Parse(Request.QueryString["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                                    }
                                }
                                else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
                                {
                                    if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim() != "" && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"].ToString().Trim() != "")
                                    {
                                        if (long.TryParse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim(), out resIsInt))
                                        {
                                            WEBLoginDatabaseCall.BusinessUpdate(long.Parse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                                        }
                                    }
                                }
                            }

                            if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                            {
                                int indx = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().IndexOf("&");
                                if (indx > 0)
                                    rturl = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Substring(0, indx);

                                rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("#_=_", "");

                                if (rturl.Contains("http://"))
                                {
                                    indx = rturl.IndexOf("http");
                                    if (indx > 0)
                                        rturl = rturl.Trim().Substring(indx, rturl.Length - 1);

                                    if (CheckUrlBeforeProcessing(rturl))
                                        return Redirect(rturl);
                                    else
                                        return Redirect("/");
                                }
                                else
                                {
                                    if (System.Web.HttpContext.Current.Session["rturl"] != null && System.Web.HttpContext.Current.Session["rturl"].ToString().Trim() != "")
                                        rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim();
                                    else
                                        rturl = (rturl != null && rturl.Trim() != "") ? "/" + rturl : "/";

                                    return RedirectPermanent(rturl);

                                    //rturl ="/"+ System.Web.HttpContext.Current.Session["rturl"].ToString().Trim();
                                    //return Redirect(rturl);
                                }

                            }
                            else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
                            {

                                if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"].ToString().Trim() != "")
                                {
                                    rturl = System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"].ToString().Trim();
                                    return RedirectPermanent(rturl);
                                }
                                else
                                {
                                    return RedirectPermanent("/");
                                }
                            }
                           
                            else
                            {
                                return RedirectPermanent("/");
                            }

                        }

                    }

                }

                

                if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                    return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                else
                    return RedirectPermanent("/login"); 
               
            }
            catch(Exception ex)
            {
                log.LogMe(ex);
                if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                    return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                else
                    return RedirectPermanent("/login"); 
            }
        }

        private int ReturnURIValue(string rturl)
        {
            try
            {
                if(rturl.Contains("http://"))
                    rturl= rturl.Trim();
                else
                    rturl = "http://" + rturl.Trim();

                Uri myUri = new Uri(rturl);
                srcReturnVal = System.Web.HttpUtility.ParseQueryString(myUri.Query).Get("returnval");
                return CatchSourceCode(srcReturnVal);               
            }
            catch (Exception ex)
            {
                //log.LogMe(ex);
                return 0;
            }
        }

        private int CatchSourceCode(string scode)
        {
            int cod;
            try
            {
                switch (scode)
                { 
                    case "1":
                        cod = 1;
                        break;

                    case "2":
                        cod = 2;
                        break;

                    case "3":
                        cod = 3;
                        break;

                    case "4":
                        cod = 4;
                        break;

                    case "5":
                        cod = 5;
                        break;

                    case "6":
                        cod = 6;
                        break;

                    case "7":
                        cod = 7;
                        break;

                    case "8":
                        cod = 8;
                        break;

                    default:
                        cod = 0;
                        break;

                }

                return cod;
            }
            catch (Exception ex)
            {
                cod = 0;
                return cod;
            }
            finally { }
        }

        [ValidateInput(false)]
        public ActionResult UpdateUserEmail(LoginModel myModel)
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                return View();
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateUserEmail(FormCollection collection)
        {
            try
            {
                if (collection["emailid"] == null || collection["emailid"].ToString().Trim() == "" || collection["emailid"].ToString().Trim() == "Enter email address")
                {
                    TempData["error"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Please enter a valid email address.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                    return View();
                }

                LoginModel model = new LoginModel();
                if (System.Web.HttpContext.Current.Session["model"] != null)
                {
                    Type t = System.Web.HttpContext.Current.Session["model"].GetType();
                    if (t == typeof(LoginModel))
                        model = (LoginModel)System.Web.HttpContext.Current.Session["model"];
                }

                string userEmail = string.IsNullOrWhiteSpace(collection["emailid"]) ? "" : collection["emailid"].ToString().Trim();

                if (model.Login != null && model.Login.Trim() != "" && model.Password != null && model.Password.Trim() != "")
                {
                    int res = WEBLoginDatabaseCall.DBUpdateUserPhoneEmail(model.Login, model.Password, userEmail, 1);

                    if (res == 0)
                    {
                        return RedirectToRoute("wblogin");
                    }
                    else if (res == 1)
                    {
                        TempData["error"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Email address already exists.Please use that email address to login.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                        return View();
                    }
                    else
                    {
                        TempData["error"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Some error occured please try later.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                        return View();
                    }
                }
                else
                {
                    TempData["error"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Some error occured please try later.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                    return View();
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                return View();
            }
        }

        [ValidateInput(false)]
        public ActionResult generateloginidverification(LoginModel myModel, string opt)
        {
            if (!string.IsNullOrWhiteSpace(opt) && opt == "1")
            {
                if (System.Web.HttpContext.Current.Session["model"] != null && myModel.EmailId == null)
                {
                    Type t = System.Web.HttpContext.Current.Session["model"].GetType();
                    if (t == typeof(LoginModel))
                    {
                        myModel = (LoginModel)System.Web.HttpContext.Current.Session["model"];
                    }

                }
                return View("verification", myModel);
            }
            else
            {
                LoginModel model = new LoginModel();

                try
                {

                    if (myModel.Login != null)
                    {
                        if (myModel.IsResendCode == 1)
                        {
                            myModel.MobileVerificationCode = "";
                            TempData.Remove("VerifyCode");
                            TempData["loginError"] = "";
                        }

                        System.Web.HttpContext.Current.Session["model"] = myModel;
                        model = myModel;
                    }
                    else
                    {
                        if (System.Web.HttpContext.Current.Session["model"] != null)
                        {
                            Type t = System.Web.HttpContext.Current.Session["model"].GetType();
                            if (t == typeof(LoginModel))
                            {
                                model = (LoginModel)System.Web.HttpContext.Current.Session["model"];
                                if (model.IsResendCode == 1)
                                {
                                    model.MobileVerificationCode = "";
                                    TempData["loginError"] = "";
                                    TempData.Remove("VerifyCode");
                                    System.Web.HttpContext.Current.Session["model"] = model;
                                }
                            }
                        }
                    }

                    System.Web.HttpContext.Current.Session["lgnProvider"] = "vc";
                    if (System.Web.HttpContext.Current.Session["lgnProvider"] == null || System.Web.HttpContext.Current.Session["model"] == null)
                    {
                        ViewBag.UserMsg = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Your session got expired.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                        if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                            return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                        else
                            return RedirectPermanent("/login");

                    }


                    if (!string.IsNullOrWhiteSpace(model.Login))
                    {
                        string randCode = WEBLoginDatabaseCall.Generate(4);

                        DataTable dt = WEBLoginDatabaseCall.GetUserInfo(model.Login);

                        if (dt.Rows.Count > 0)
                        {
                            string IPAddress = Utility.GetIpAddress();

                            if (model.CheckEmailVerification == 1)
                            {
                                WEBLoginDatabaseCall.UpdateNewVerificationCode(model.Login.Trim(), randCode);

                                StringBuilder sbEmailContent = new StringBuilder();
                                sbEmailContent.Append("<a href=" + ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + "verify?uid=" + (!string.IsNullOrWhiteSpace(dt.Rows[0]["userid"].ToString()) ? dt.Rows[0]["userid"].ToString().Trim() : "0") + "&code=" + randCode + "&p=" + System.Web.HttpContext.Current.Session["lgnProvider"].ToString().Trim() + ">Click Here</a>");
                                string msgUrl = ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + "verify?uid=" + (!string.IsNullOrWhiteSpace(dt.Rows[0]["userid"].ToString()) ? dt.Rows[0]["userid"].ToString().Trim() : "0") + "&code=" + randCode + "&p=" + System.Web.HttpContext.Current.Session["lgnProvider"].ToString().Trim();

                                long result;
                                string passwrd = (dt.Rows[0]["password"] != null && !string.IsNullOrEmpty(dt.Rows[0]["password"].ToString().Trim())) ? dt.Rows[0]["password"].ToString().Trim() : "";

                                if (long.TryParse(model.Login, out result))
                                {
                                    model.EmailId = (dt.Rows[0]["loginemail"].ToString() != null && dt.Rows[0]["loginemail"].ToString().Trim() != "") ? dt.Rows[0]["loginemail"].ToString().Trim() : "";
                                    SendEmail(dt.Rows[0]["screenname"].ToString().Trim(), sbEmailContent.ToString(), msgUrl, "verification.htm", 1, model.EmailId.Trim(), passwrd, 0);
                                    //SendEmail(dt.Rows[0]["screenname"].ToString().Trim(), sbEmailContent.ToString(), msgUrl, "verification.htm", 1, model.EmailId.Trim(), passwrd, 0);// SendEmail((!string.IsNullOrWhiteSpace(dt.Rows[0]["screenname"].ToString()) ? dt.Rows[0]["screenname"].ToString().Trim() : ""), sbEmailContent.ToString(), msgUrl, "verification.htm", 1, model.EmailId.Trim(), passwrd);
                                }
                                else
                                {
                                    SendEmail(dt.Rows[0]["screenname"].ToString().Trim(), sbEmailContent.ToString(), msgUrl, "verification.htm", 1, model.EmailId.Trim(), passwrd, 0);

                                    //SendEmail((!string.IsNullOrWhiteSpace(dt.Rows[0]["screenname"].ToString()) ? dt.Rows[0]["screenname"].ToString().Trim() : ""), sbEmailContent.ToString(), msgUrl, "verification.htm", 1, model.Login.Trim(), passwrd);
                                }


                                //Maintain the SMS EMAIL LOG
                                if (dt.Rows[0]["userid"] != null && dt.Rows[0]["userid"].ToString().Trim() != "")
                                {

                                    Utility.SaveSMSEmailLog(Convert.ToInt32(dt.Rows[0]["userid"].ToString().Trim()), Usertype.OtherUser.GetHashCode(), model.Login.Trim(), "", "", MessageType.Email_Forgot.GetHashCode(), ConfigurationManager.AppSettings["Subjectforgotpass"].ToString(), sbEmailContent.ToString(), 0, "", "", "", IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.Email.GetHashCode(), reversedns, source);

                                }
                                else
                                {

                                    Utility.SaveSMSEmailLog(0, Usertype.OtherUser.GetHashCode(), model.Login.Trim(), "", "", MessageType.Email_Forgot.GetHashCode(), ConfigurationManager.AppSettings["Subjectforgotpass"].ToString(), sbEmailContent.ToString(), 0, "", "", "", IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.Email.GetHashCode(), reversedns, source);

                                }

                                TempData["loginMsg"] = "<p>A verification link has been sent to your email address. You need to verify your account via that link to gain access.</p><p>Didn't get the email? <a href=\"javascript:void(0);\" onclick=\"resendEmail();showSpinner();\" >Click here</a> to resend it.</p>";
                            }

                            long resOut;
                            if (model.CheckMobileVerification == 1)
                            {
                                if (long.TryParse(model.Login.Trim(), out resOut))
                                {
                                    model.isMobile = 1;
                                }

                                if (long.TryParse(model.PhoneNo.Trim(), out resOut))
                                {
                                    int vaildphone = Utility.isValidPhone(model.PhoneNo.Trim());
                                    if (vaildphone == 0)
                                    {
                                        TempData["loginError"] = "Phone no (" + model.PhoneNo.Trim() + ") is not valid.";
                                        model.IsPhonenoNotValid = 1;
                                        model.CheckMobileVerification = 0;
                                        System.Web.HttpContext.Current.Session["model"] = model;
                                        return RedirectToRoute("verification", new { opt = "1" });

                                    }

                                }

                                StringBuilder sbMobileContent = new StringBuilder();

                                SendSMS objSendSMS = new SendSMS();

                                string SMSResponse = string.Empty;

                                if (string.IsNullOrWhiteSpace(model.MobileVerificationCode))
                                {
                                    TempData["VerifyCode"] = randCode;
                                    TempData.Keep("VerifyCode");
                                    model.CheckMobileVerification = 1;

                                    WEBLoginDatabaseCall.UpdateNewVerificationCode(model.Login.Trim(), randCode);

                                    sbMobileContent.Append("Your verification code " + randCode);

                                    SMSResponse = objSendSMS.Sendmessage(sbMobileContent.ToString() + ".", model.PhoneNo.Trim());

                                    //Maintain the SMS EMAIL LOG
                                    if (dt.Rows[0]["userid"] != null && dt.Rows[0]["userid"].ToString().Trim() != "")
                                    {

                                        Utility.SaveSMSEmailLog(Convert.ToInt32(dt.Rows[0]["userid"].ToString().Trim()), Usertype.OtherUser.GetHashCode(), "", model.PhoneNo.Trim(), "", MessageType.SMS_Forgot.GetHashCode(), "Your Verification Code", sbMobileContent.ToString(), 0, "", "", SMSResponse.ToString(), IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), reversedns, source);
                                    }
                                    else
                                    {
                                        Utility.SaveSMSEmailLog(0, Usertype.OtherUser.GetHashCode(), "", model.PhoneNo.Trim(), "", MessageType.SMS_Forgot.GetHashCode(), "Your Verification Code", sbMobileContent.ToString(), 0, "", "", SMSResponse.ToString(), IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), reversedns, source);
                                    }
                                    model.IsResendCode = 0;
                                }
                                else
                                {
                                    if (TempData["VerifyCode"] != null)
                                        model.ConfirmMobileVerificationCode = !string.IsNullOrEmpty(TempData.Peek("VerifyCode").ToString().Trim()) ? TempData.Peek("VerifyCode").ToString().Trim() : "";
                                    else if (System.Web.HttpContext.Current.Request.Cookies["VCverifycode"] != null)
                                        model.ConfirmMobileVerificationCode = !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Cookies["VCverifycode"].Value.Trim()) ? System.Web.HttpContext.Current.Request.Cookies["VCverifycode"].Value.Trim() : "";
                                    else
                                        model.ConfirmMobileVerificationCode = "";

                                    if (model.ConfirmMobileVerificationCode.Trim().Equals(model.MobileVerificationCode.Trim()))
                                    {
                                        model.CheckEmailVerification = 0;

                                        string retrnVal = WEBLoginDatabaseCall.UpdateIsMobileEmailVerified(dt.Rows[0]["userid"].ToString().Trim(), 1);

                                        if (retrnVal == "1")
                                        {
                                            model.IsResendCode = 0;
                                            model.CheckMobileVerification = 0;
                                            model.IsPhoneRedirect = 1;

                                            System.Web.HttpContext.Current.Session["model"] = model;
                                            TempData["loginError"] = "";
                                            TempData.Remove("VerifyCode");

                                            return RedirectToRoute("wblogin");
                                        }
                                        else
                                        {

                                            TempData["loginError"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">" + retrnVal + "<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                                        }
                                    }
                                    else
                                    {
                                        model.MobileVerificationCode = "";
                                        model.CheckMobileVerification = 1;
                                        TempData["loginError"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Verification code you entered is incorrect.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";

                                    }

                                }

                            }

                        }

                    }
                    else
                    {
                        TempData["loginError"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Some error occured please try later.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";

                    }

                    System.Web.HttpContext.Current.Session["model"] = model;
                    if (model.isRedirect == 1)
                    {
                        System.Web.HttpContext.Current.Session["view"] = "Verification";
                    }

                    return View("Verification", model);
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                    if (model.isRedirect == 1)
                    {
                        System.Web.HttpContext.Current.Session["view"] = "Verification";
                    }

                    return View("Verification", model);
                }
            }
        }
        //public ActionResult generateloginidverification(LoginModel myModel, string opt)
        //{
        //    if (!string.IsNullOrWhiteSpace(opt) && opt == "1")
        //    {
        //        if (System.Web.HttpContext.Current.Session["model"] != null && myModel.EmailId == null)
        //        {
        //            Type t = System.Web.HttpContext.Current.Session["model"].GetType();
        //            if (t == typeof(LoginModel))
        //            {
        //                myModel = (LoginModel)System.Web.HttpContext.Current.Session["model"];
        //            }

        //        }
        //        return View("verification", myModel);
        //    }
        //    else
        //    {
        //        LoginModel model = new LoginModel();
               
        //        try
        //        {

        //            if (myModel.Login != null)
        //            {
        //                if (myModel.IsResendCode == 1)
        //                {
        //                    myModel.MobileVerificationCode = "";
        //                    TempData.Remove("VerifyCode");
        //                    TempData["loginError"] = "";                                         
        //                }

        //                System.Web.HttpContext.Current.Session["model"] = myModel;
        //                model = myModel;
        //            }
        //            else
        //            {
        //                if (System.Web.HttpContext.Current.Session["model"] != null)
        //                {
        //                    Type t = System.Web.HttpContext.Current.Session["model"].GetType();
        //                    if (t == typeof(LoginModel))
        //                    {
        //                        model = (LoginModel)System.Web.HttpContext.Current.Session["model"];
        //                        if (model.IsResendCode == 1)
        //                        {
        //                            model.MobileVerificationCode = "";
        //                            TempData["loginError"] = "";
        //                            TempData.Remove("VerifyCode");
        //                            System.Web.HttpContext.Current.Session["model"] = model;
        //                        }
        //                    }
        //                }
        //            }
                    
        //            System.Web.HttpContext.Current.Session["lgnProvider"] = "vc";
        //            if (System.Web.HttpContext.Current.Session["lgnProvider"] == null || System.Web.HttpContext.Current.Session["model"] == null)
        //            {
        //                ViewBag.UserMsg = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Your session got expired.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
        //                if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
        //                    return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
        //                else
        //                    return RedirectPermanent("/login");

        //            }


        //            if (!string.IsNullOrWhiteSpace(model.Login))
        //            {
        //                string randCode = WEBLoginDatabaseCall.Generate(4);

        //                DataTable dt = WEBLoginDatabaseCall.GetUserInfo(model.Login);

        //                if (dt.Rows.Count > 0)
        //                {
        //                    string IPAddress = Utility.GetIpAddress();

        //                    if (model.CheckEmailVerification == 1)
        //                    {
        //                        WEBLoginDatabaseCall.UpdateNewVerificationCode(model.Login.Trim(), randCode);

        //                        StringBuilder sbEmailContent = new StringBuilder();
        //                        sbEmailContent.Append("<a href=" + ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + "verify?uid=" + (!string.IsNullOrWhiteSpace(dt.Rows[0]["userid"].ToString()) ? dt.Rows[0]["userid"].ToString().Trim():"0") + "&code=" + randCode + "&p=" + System.Web.HttpContext.Current.Session["lgnProvider"].ToString().Trim() + ">Click Here</a>");
        //                        string msgUrl = ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + "verify?uid=" + (!string.IsNullOrWhiteSpace(dt.Rows[0]["userid"].ToString()) ? dt.Rows[0]["userid"].ToString().Trim() : "0") + "&code=" + randCode + "&p=" + System.Web.HttpContext.Current.Session["lgnProvider"].ToString().Trim();

        //                        long result;
        //                        string passwrd = (dt.Rows[0]["password"] != null && !string.IsNullOrEmpty(dt.Rows[0]["password"].ToString().Trim())) ? dt.Rows[0]["password"].ToString().Trim() : "";

        //                        if (long.TryParse(model.Login, out result))
        //                        {
        //                            model.EmailId = (dt.Rows[0]["loginemail"].ToString() != null && dt.Rows[0]["loginemail"].ToString().Trim() != "") ? dt.Rows[0]["loginemail"].ToString().Trim() : "";
        //                            SendEmail((!string.IsNullOrWhiteSpace(dt.Rows[0]["screenname"].ToString()) ? dt.Rows[0]["screenname"].ToString().Trim():""), sbEmailContent.ToString(), msgUrl, "verification.htm", 1, model.EmailId.Trim(), passwrd);
        //                        }
        //                        else
        //                        {
        //                            SendEmail((!string.IsNullOrWhiteSpace(dt.Rows[0]["screenname"].ToString()) ? dt.Rows[0]["screenname"].ToString().Trim() : ""), sbEmailContent.ToString(), msgUrl, "verification.htm", 1, model.Login.Trim(), passwrd);
        //                        }


        //                        //Maintain the SMS EMAIL LOG
        //                        if (dt.Rows[0]["userid"] != null && dt.Rows[0]["userid"].ToString().Trim() != "")
        //                        {

        //                            Utility.SaveSMSEmailLog(Convert.ToInt32(dt.Rows[0]["userid"].ToString().Trim()), Usertype.OtherUser.GetHashCode(), model.Login.Trim(), "", "", MessageType.Email_Forgot.GetHashCode(), ConfigurationManager.AppSettings["Subjectforgotpass"].ToString(), sbEmailContent.ToString(), 0, "", "", "", IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.Email.GetHashCode(), reversedns, source);

        //                        }
        //                        else
        //                        {

        //                            Utility.SaveSMSEmailLog(0, Usertype.OtherUser.GetHashCode(), model.Login.Trim(), "", "", MessageType.Email_Forgot.GetHashCode(), ConfigurationManager.AppSettings["Subjectforgotpass"].ToString(), sbEmailContent.ToString(), 0, "", "", "", IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.Email.GetHashCode(), reversedns, source);

        //                        }

        //                        TempData["loginMsg"] = "<p>A verification link has been sent to your email address. You need to verify your account via that link to gain access.</p><p>Didn't get the email? <a href=\"javascript:void(0);\" onclick=\"resendEmail();showSpinner();\" >Click here</a> to resend it.</p>";
        //                    }

        //                    long resOut;
        //                    if (model.CheckMobileVerification == 1)
        //                    {
        //                        if (long.TryParse(model.Login.Trim(), out resOut))
        //                        {
        //                            model.isMobile = 1;
        //                        }

        //                        if (long.TryParse(model.PhoneNo.Trim(), out resOut))
        //                        {
        //                            int vaildphone = Utility.isValidPhone(model.PhoneNo.Trim());
        //                            if (vaildphone == 0)
        //                            {
        //                                TempData["loginError"] = "Phone no ("+model.PhoneNo.Trim()+") is not valid.";
        //                                model.IsPhonenoNotValid = 1;
        //                                model.CheckMobileVerification = 0;
        //                                System.Web.HttpContext.Current.Session["model"] = model;
        //                                return RedirectToRoute("verification", new { opt = "1" });

        //                            }

        //                        }

        //                        StringBuilder sbMobileContent = new StringBuilder();

        //                        SendSMS objSendSMS = new SendSMS();

        //                        string SMSResponse = string.Empty;

        //                        if (string.IsNullOrWhiteSpace(model.MobileVerificationCode))
        //                        {
        //                            TempData["VerifyCode"] = randCode;
        //                            TempData.Keep("VerifyCode");
        //                            model.CheckMobileVerification = 1;

        //                            WEBLoginDatabaseCall.UpdateNewVerificationCode(model.Login.Trim(), randCode);

        //                            sbMobileContent.Append("Your verification code " + randCode);

        //                            SMSResponse = objSendSMS.Sendmessage(sbMobileContent.ToString() + ".", model.PhoneNo.Trim());

        //                            //Maintain the SMS EMAIL LOG
        //                            if (dt.Rows[0]["userid"] != null && dt.Rows[0]["userid"].ToString().Trim() != "")
        //                            {

        //                                Utility.SaveSMSEmailLog(Convert.ToInt32(dt.Rows[0]["userid"].ToString().Trim()), Usertype.OtherUser.GetHashCode(), "", model.PhoneNo.Trim(), "", MessageType.SMS_Forgot.GetHashCode(), "Your Verification Code", sbMobileContent.ToString(), 0, "", "", SMSResponse.ToString(), IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), reversedns, source);
        //                            }
        //                            else
        //                            {
        //                                Utility.SaveSMSEmailLog(0, Usertype.OtherUser.GetHashCode(), "", model.PhoneNo.Trim(), "", MessageType.SMS_Forgot.GetHashCode(), "Your Verification Code", sbMobileContent.ToString(), 0, "", "", SMSResponse.ToString(), IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), reversedns, source);
        //                            }
        //                            model.IsResendCode = 0;
        //                        }
        //                        else
        //                        {
        //                            if (TempData["VerifyCode"] != null)
        //                                model.ConfirmMobileVerificationCode = !string.IsNullOrEmpty(TempData.Peek("VerifyCode").ToString().Trim()) ? TempData.Peek("VerifyCode").ToString().Trim() : "";
        //                            else if (System.Web.HttpContext.Current.Request.Cookies["VCverifycode"] != null)
        //                                model.ConfirmMobileVerificationCode = !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Cookies["VCverifycode"].Value.Trim()) ? System.Web.HttpContext.Current.Request.Cookies["VCverifycode"].Value.Trim() : "";
        //                            else
        //                                model.ConfirmMobileVerificationCode = "";

        //                            if (model.ConfirmMobileVerificationCode.Trim().Equals(model.MobileVerificationCode.Trim()))
        //                            {
        //                                model.CheckEmailVerification = 0;

        //                                string retrnVal = WEBLoginDatabaseCall.UpdateIsMobileEmailVerified(dt.Rows[0]["userid"].ToString().Trim(), 1);

        //                                if (retrnVal == "1")
        //                                {
        //                                    model.IsResendCode = 0;
        //                                    model.CheckMobileVerification = 0;
        //                                    model.IsPhoneRedirect = 1;

        //                                    System.Web.HttpContext.Current.Session["model"] = model;
        //                                    TempData["loginError"] = "";
        //                                    TempData.Remove("VerifyCode");
                                            
        //                                    return RedirectToRoute("wblogin");
        //                                }
        //                                else
        //                                {

        //                                    TempData["loginError"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">" + retrnVal + "<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
        //                                }
        //                            }
        //                            else
        //                            {
        //                                model.MobileVerificationCode = "";
        //                                model.CheckMobileVerification = 1;
        //                                TempData["loginError"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Verification code you entered is incorrect.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";

        //                            }

        //                        }

        //                    }

        //                }

        //            }
        //            else
        //            {
        //                TempData["loginError"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Some error occured please try later.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";

        //            }

        //            System.Web.HttpContext.Current.Session["model"] = model;
        //            if (model.isRedirect == 1)
        //            {
        //                System.Web.HttpContext.Current.Session["view"] = "Verification";
        //            }

        //            return View("Verification", model);
        //        }
        //        catch (Exception ex)
        //        {
        //            log.LogMe(ex);
        //            if (model.isRedirect == 1)
        //            {
        //                System.Web.HttpContext.Current.Session["view"] = "Verification";
        //            }

        //            return View("Verification", model);
        //        }
        //    }
        //}

        [ValidateInput(false)]
        protected void SendEmail(string screenName, string msgLink, string msgUrl, string template, int flagEmail, string emailid, string password, int userid)
        {
            bool isEmail = Regex.IsMatch(emailid, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z");
            if (isEmail)
            {

                string Message = "";
                //System.Net.Mail.SmtpClient SMPT = new System.Net.Mail.SmtpClient();
                //System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage();
                System.IO.FileStream FsContent = new System.IO.FileStream(Server.MapPath("~/resource/email_templates/" + template), System.IO.FileMode.Open, System.IO.FileAccess.Read);
                System.IO.StreamReader SRcontent = new System.IO.StreamReader(FsContent);
                Message = SRcontent.ReadToEnd();
                FsContent.Close();
                SRcontent.Close();
                FsContent.Dispose();
                SRcontent.Dispose();
                TextInfo convertCase = new CultureInfo("en-US", false).TextInfo;
                if (flagEmail == 1)
                {
                    Message = Message.Replace("{1}", msgUrl);
                    Message = Message.Replace("{20}", ConfigurationManager.AppSettings["WebsiteRootPath"].ToString());
                    //=================================== 
                    var recipients = new List<Mandrill.Messages.Recipient>();
                    recipients.Add(new Mandrill.Messages.Recipient(emailid, screenName));
                    var mandrill = new Mandrill.Messages.Message()
                    {
                        To = recipients.ToArray(),
                        FromEmail = "info@VConnect.com",
                        Subject = "VConnect Registration: Email Confirmation Required",

                        Html = Message
                    };
                    //=================================== 
                    SendEmail send = new SendEmail();
                    try
                    {
                        send.VCUsermails(mandrill);
                    }
                    catch (Exception ex)
                    {
                        log.LogMe(ex);
                    }

                }
                else if (flagEmail == 2)
                {

                    Message = Message.Replace("{2}", screenName);
                    Message = Message.Replace("{0}", msgLink);
                    Message = Message.Replace("{3}", ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + "following/");

                    //===================================
                    var recipients = new List<Mandrill.Messages.Recipient>();
                    recipients.Add(new Mandrill.Messages.Recipient(emailid, ""));
                    var mandrill = new Mandrill.Messages.Message()
                    {
                        To = recipients.ToArray(),
                        FromEmail = "info@VConnect.com",
                        Subject = "Reset Your VConnect Password.",

                        Html = Message
                    };
                    //=================================== 
                    SendEmail send = new SendEmail();
                    try
                    {
                        send.VCUsermails(mandrill);
                    }
                    catch (Exception ex)
                    {
                        log.LogMe(ex);
                    }

                }
                else if (flagEmail == 3)
                {
                    string userurl = Utility.userdetail(userid, "userurl");
                    Message = Message.Replace("{1}", ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + "following/" + userurl.Trim());
                    Message = Message.Replace("{3}", ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + "following/");
                    //===================================
                    var recipients = new List<Mandrill.Messages.Recipient>();
                    recipients.Add(new Mandrill.Messages.Recipient(emailid, screenName));
                    var mandrill = new Mandrill.Messages.Message()
                    {
                        To = recipients.ToArray(),
                        FromEmail = "info@VConnect.com",
                        Subject = "Welcome to Vconnect!!",

                        Html = Message
                    };
                    //=================================== 
                    SendEmail send = new SendEmail();
                    try
                    {
                        send.VCUsermails(mandrill);
                    }
                    catch (Exception ex)
                    {
                        log.LogMe(ex);
                    }
                }

            }
        }
        [HttpGet]
        [ValidateInput(false)]
        public ActionResult UserAccountMapping(LoginModel myModel)
        {
            LoginModel model = new LoginModel();
                
            try
            {
                model = (LoginModel)System.Web.HttpContext.Current.Session["model"];
                if (string.IsNullOrWhiteSpace(model.Login))
                    model = myModel;

                model.ListResultTableAccountBusinessMappingModel = WEBLoginDatabaseCall.DBGetAccountMappingDetails(model.Login);
                System.Web.HttpContext.Current.Session["model"] = model;
                return View("Login", model);
               
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                return View("Login", model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UserAccountMappingPost(LoginModel myModel, FormCollection collection)
        {
            LoginModel model = new LoginModel();
            try
            {
               
                if (System.Web.HttpContext.Current.Session["model"] != null)
                    model = (LoginModel)System.Web.HttpContext.Current.Session["model"];
                else
                    model = myModel;

                string[] arrSelectedValue = collection["ContentId"].ToString().Split(',');
                int contentID = Int32.Parse(arrSelectedValue[0].ToString());
                string phone = arrSelectedValue[1].ToString();
                string UserLoginId = arrSelectedValue[2].ToString();

                string ResValue = WEBLoginDatabaseCall.DBMapUserAccountWithBusiness(contentID, phone, UserLoginId);
                string[] arrResValue = ResValue.Split(',');

                model.Password = arrResValue[1].ToString();
                model.Login = UserLoginId;

                if (ResValue[0].ToString() == "1")
                {
                    Session.Remove("model");
                    System.Web.HttpContext.Current.Session["model"] = model;
                    return RedirectToRoute("wblogin");
                }
                else
                {
                    Session.Remove("model");
                    System.Web.HttpContext.Current.Session["model"] = model;
                    TempData["loginError"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Some error occured please try later.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                    return View("UserAccountMapping", model);
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                return View("UserAccountMapping", model);
            }
        }

        public DataSet LoginUser(LoginModel model)
        {
            
            DataSet dsLoginResultsWeb = new DataSet();
            List<UserDetails> listUserDetails = new List<UserDetails>();

            AccountLoginResultWEB listResultSetTableObj = new AccountLoginResultWEB();

            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[prc_get_cuserLogin_new_opt]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object
                cmd.Parameters.Add(new SqlParameter("@password", model.Password));
                cmd.Parameters.Add(new SqlParameter("@userloginid", model.Login));
                //cmd.Parameters.Add(new SqlParameter("@encryptedpassword", Utility.Encrypt.encryptPassword(model.Password)));
                cmd.Parameters.Add(new SqlParameter("@encryptedpassword", ""));
                cmd.Parameters.Add(new SqlParameter("@flag", 1));
                var err=new SqlParameter("@ERR",SqlDbType.Int);
                err.Direction=ParameterDirection.Output;
                cmd.Parameters.Add(err);
                var userid = new SqlParameter("@userid", SqlDbType.Int);
                userid.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(userid);

                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();

                    DataTable tableUserDetails = ((IObjectContextAdapter)db).ObjectContext.Translate<UserDetails>(reader).ToDataTable();
                    tableUserDetails.TableName = "tableUserDetails";
                    dsLoginResultsWeb.Tables.Add(tableUserDetails);

                    reader.Close();
                    if (cmd.Parameters["@ERR"] != null)
                    {
                        if (cmd.Parameters["@ERR"].Value.ToString() == "3")
                        {
                            if (new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,6}$").IsMatch(model.Login.Trim()))
                            {

                                System.Web.HttpContext.Current.Session["loginError"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Login ID is not verified. Click on verification link we sent to verify your Login ID. To get a new link, click on <a href=/sendverification?userid=" + cmd.Parameters["@userid"].Value.ToString().Trim() + ">Resend</a>.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                            }
                            else
                            {
                                System.Web.HttpContext.Current.Session["loginError"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Login ID not verified or does not exist.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                    return dsLoginResultsWeb;
                }
                finally { db.Database.Connection.Close(); }

            }
            return dsLoginResultsWeb;
        }

        [ValidateInput(false)]
        public ActionResult sendverification(string userid)
        {
            try
            {
                System.Web.HttpContext.Current.Session["lgnProvider"] = "vc";
                if (Request.QueryString["rturl"] != null && Request.QueryString["rturl"].ToString().Trim() != "")
                {
                    System.Web.HttpContext.Current.Session["rturl"] = null;
                    System.Web.HttpContext.Current.Session["rturl"] = Request.QueryString["rturl"].ToString().Trim();
                }
                else if (Request.QueryString["src"] != null && Request.QueryString["src"].ToString().Trim() != "")
                {
                    System.Web.HttpContext.Current.Session["rturl"] = CatchRTUrl(Request.QueryString["src"].ToString().Trim());
                }

                DataTable dt;
                string randCode = WEBLoginDatabaseCall.Generate(4);

                if (userid != null && userid.Trim() != "")
                {
                    dt = WEBLoginDatabaseCall.GetUserInfo("", userid.Trim(), "","");
                    string email = string.Empty;

                    if (dt.Rows.Count > 0)
                    {
                        email = !string.IsNullOrWhiteSpace(dt.Rows[0]["loginid"].ToString()) ? dt.Rows[0]["loginid"].ToString().Trim() : "";
                    }
                    else
                    {
                        email = "";
                    }


                    if (email != null && email.Trim() != "")
                    {
                        dt = WEBLoginDatabaseCall.GetUserInfo(email.Trim());

                        if (dt.Rows.Count > 0)
                        {
                            string IPAddress = Utility.GetIpAddress();


                            WEBLoginDatabaseCall.UpdateNewVerificationCode(email, randCode);

                            StringBuilder sbEmailContent = new StringBuilder();
                            sbEmailContent.Append("<a href=" + ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + "verify?uid=" + (!string.IsNullOrWhiteSpace(dt.Rows[0]["userid"].ToString()) ? dt.Rows[0]["userid"].ToString().Trim() : "0") + "&code=" + randCode + "&p=" + System.Web.HttpContext.Current.Session["lgnProvider"].ToString().Trim() + ">Click Here</a>");
                            string msgUrl = ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + "verify?uid=" + (!string.IsNullOrWhiteSpace(dt.Rows[0]["userid"].ToString()) ? dt.Rows[0]["userid"].ToString().Trim() : "0") + "&code=" + randCode + "&p=" + System.Web.HttpContext.Current.Session["lgnProvider"].ToString().Trim();

                            string passwrd = (dt.Rows[0]["password"] != null && !string.IsNullOrEmpty(dt.Rows[0]["password"].ToString().Trim())) ? dt.Rows[0]["password"].ToString().Trim() : "";
                            SendEmail(dt.Rows[0]["screenname"].ToString().Trim(), sbEmailContent.ToString(), msgUrl, "verification.htm", 1, email.Trim(), passwrd, 0);
                            //SendEmail((!string.IsNullOrWhiteSpace(dt.Rows[0]["screenname"].ToString()) ? dt.Rows[0]["screenname"].ToString().Trim() : ""), sbEmailContent.ToString(), msgUrl, "verification.htm", 1, email.Trim(), passwrd);

                            //Maintain the SMS EMAIL LOG
                            if (dt.Rows[0]["userid"] != null && dt.Rows[0]["userid"].ToString().Trim() != "")
                            {

                                Utility.SaveSMSEmailLog(Convert.ToInt32(dt.Rows[0]["userid"].ToString().Trim()), Usertype.OtherUser.GetHashCode(), email.Trim(), "", "", MessageType.Email_Forgot.GetHashCode(), ConfigurationManager.AppSettings["Subjectforgotpass"].ToString(), sbEmailContent.ToString(), 0, "", "", "", IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.Email.GetHashCode(), reversedns, source);

                            }
                            else
                            {

                                Utility.SaveSMSEmailLog(0, Usertype.OtherUser.GetHashCode(), email.Trim(), "", "", MessageType.Email_Forgot.GetHashCode(), ConfigurationManager.AppSettings["Subjectforgotpass"].ToString(), sbEmailContent.ToString(), 0, "", "", "", IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.Email.GetHashCode(), reversedns, source);

                            }
                            TempData["isSucceed"] = 1;
                            System.Web.HttpContext.Current.Session["loginError"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Verification link has been successfully sent. Did not receive the link? <a href=/sendverification?userid=" + userid.Trim() + " >click here</a> to resend the link.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";

                        }
                        else
                        {
                            TempData["loginError"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Some error occured please try later.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                        }
                    }
                    else
                    {
                        TempData["loginError"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Some error occured please try later.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                    }
                }
                else
                {
                    TempData["loginError"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Some error occured please try later.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                }

                if (Request.UrlReferrer != null && Request.UrlReferrer.ToString().Trim().ToLower().Contains("login"))
                {
                    return RedirectPermanent(Request.UrlReferrer.ToString().Trim());
                }
                else if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                {
                    return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                }
                else
                    return RedirectPermanent("/login");
            }
            catch (Exception ex)
            {
                if (Request.UrlReferrer != null && Request.UrlReferrer.ToString().Trim().ToLower().Contains("login"))
                {
                    return RedirectPermanent(Request.UrlReferrer.ToString().Trim());
                }
                else if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                {
                    return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                }
                else
                    return RedirectPermanent("/login");
            }
        }

        [ValidateInput(false)]
        public ActionResult RemovePhoneNumber(LoginModel model)
        {
            try
            {
                if (model.Login != null && !string.IsNullOrEmpty(model.Login.ToString().Trim()))
                {
                    int ResValue = WEBLoginDatabaseCall.DBUpdateUserPhoneEmail(model.Login.Trim(), model.Password.Trim(), "", 2);

                    if (ResValue == 1)
                        TempData["loginError"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Phone number removed successfully.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                    else
                        TempData["loginError"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Some error occured please try later.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";

                    System.Web.HttpContext.Current.Session["model"] = model;
                    wblogin();
                    return View("Verification", model);
                }
                else
                {
                    System.Web.HttpContext.Current.Session["model"] = model;
                    return View("Verification", model);
                }

            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                return View("Verification", model);
            }
        }

        #endregion

        #region For Facebook/Google/Twitter

        private Uri RedirectUri
        {

            get
            {

                System.Web.HttpContext.Current.Session["rturl"] = null;
                if (Request.QueryString["rturl"] != null)
                {
                    System.Web.HttpContext.Current.Session["rturl"] = Request.QueryString["rturl"].Replace("$", "&");
                    if (Request.QueryString["provider"] != null)
                    {
                        rturl = Request.QueryString["rturl"].ToString().Trim().Replace("$", "&");
                    }
                }
                else if ((Request.QueryString["src"] != null && Request.QueryString["src"].ToString().Trim() != ""))
                {
                    System.Web.HttpContext.Current.Session["rturl"] = CatchRTUrl(Request.QueryString["src"].ToString().Trim());
                    if (Request.QueryString["provider"] != null && Request.QueryString["provider"].ToString().Trim() != "")
                    {
                        rturl = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim();
                    }
                }
                else if (System.Web.HttpContext.Current.Request.UrlReferrer != null && System.Web.HttpContext.Current.Request.UrlReferrer.ToString().Trim() != "")
                {
                    if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["src"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["src"].ToString().Trim() != "")
                    {
                        rturl = System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["src"].ToString().Trim();
                        System.Web.HttpContext.Current.Session["rturl"] = CatchRTUrl(rturl);
                        rturl = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim();
                    }
                }

                var uriBuilder = new UriBuilder(Request.Url);
                try
                {
                    var query = System.Web.HttpUtility.ParseQueryString(uriBuilder.Query);
                   // query["rturl"] = (rturl == null || rturl.ToString().Trim() == "") ? System.Web.HttpContext.Current.Session["rturl"].ToString().Trim() : rturl;
                    query["rturl"] = (rturl == null || rturl.ToString().Trim() == "") ? ((System.Web.HttpContext.Current.Session["rturl"] != null && System.Web.HttpContext.Current.Session["rturl"].ToString().Trim() != "") ? System.Web.HttpContext.Current.Session["rturl"].ToString().Trim() : "") : rturl;
                    uriBuilder.Query = query.ToString();
                    uriBuilder.Fragment = null;

                    uriBuilder.Path = Url.Action("FacebookCallback");

                    return uriBuilder.Uri;
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                    if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                        uriBuilder = new UriBuilder("/login?" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                    else
                        uriBuilder = new UriBuilder("/login");

                    return uriBuilder.Uri;
                }
            }
        }


        [ValidateInput(false)]
        public ActionResult Facebook()
        {
            try
            {
                var fb = new FacebookClient();
                var loginUrl = fb.GetLoginUrl(new
                {
                    client_id = ConfigurationManager.AppSettings["facebookAppID"].ToString(),
                    client_secret = ConfigurationManager.AppSettings["facebookAppSecret"].ToString(),
                    redirect_uri = RedirectUri.AbsoluteUri,
                    response_type = "code",
                    scope = "email,user_friends" // Add other permissions as needed
                });

                return RedirectPermanent(loginUrl.AbsoluteUri);
            }
            catch(Exception ex)
            {
               // log.LogMe(ex);
                if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                    return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                else
                    return RedirectPermanent("/login"); 
                   
            }
        }

        [ValidateInput(false)]
        public ActionResult FacebookCallback(string code)
        {
            try
            {
                var fb = new FacebookClient();
                dynamic result = fb.Post("oauth/access_token", new
                {
                    client_id = ConfigurationManager.AppSettings["facebookAppID"].ToString(),
                    client_secret = ConfigurationManager.AppSettings["facebookAppSecret"].ToString(),
                    redirect_uri = RedirectUri.AbsoluteUri,
                    code = code,
                    scope ="read_friendlists"   
                });

                var accessToken = result.access_token;

                // Store the access token in the session
                System.Web.HttpContext.Current.Session["AccessToken"] = accessToken;

                // update the facebook client with the access token so 
                // we can make requests on behalf of the user
                fb.AccessToken = accessToken;

                try
                {
                    // Get the user's information
                    dynamic me = fb.Get("me?fields=name,first_name,last_name,id,email,birthday,gender,link,picture");
                    dynamic frns=fb.Get("me/friends?fields=name,first_name,last_name,id,gender,link"); //string email = me.email;

                    // Set the auth cookie
                    // FormsAuthentication.SetAuthCookie(email, false);
                    //return RedirectToAction("Index", "Home");

                    var userDataFromProvider = result.ExtraData;
                    Dictionary<string, string> userData = new Dictionary<string, string>();

                    userData.Add("id", me.id);
                    userData.Add("name", me.name != "" ? me.name : "");
                    userData.Add("email", me.email != "" ? me.email : "");
                    userData.Add("link", me.link != "" ? me.link : "");
                    userData.Add("gender", me.gender != "" ? me.gender : "");
                    string email = me.email != "" ? me.email : "";
                    string mobileNo = "";
                    string username = me.name != "" ? me.name : "";
                    string gender = me.gender != "" ? me.gender : "";

                    List<FbUserFriendsList> fbUsers = new List<FbUserFriendsList>();
                    try
                    {
                        JObject friendListJson = JObject.Parse(frns.ToString());

                      
                        foreach (var friend in friendListJson["data"].Children())
                        {
                            FbUserFriendsList fbUser = new FbUserFriendsList();

                            fbUser.Id = (friend["id"] != null && friend["id"].ToString().Trim() != "" ? friend["id"].ToString().Trim().Replace("\"", "") : "");
                            fbUser.Name = (friend["name"] != null && friend["name"].ToString().Trim() != "" ? friend["name"].ToString().Trim().Replace("\"", "") : "");
                            fbUser.FirstName = (friend["first_name"] != null && friend["first_name"].ToString().Trim() != "" ? friend["first_name"].ToString().Trim().Replace("\"", "") : "");
                            fbUser.LastName = (friend["last_name"] != null && friend["last_name"].ToString().Trim() != "" ? friend["last_name"].ToString().Trim().Replace("\"", "") : "");
                            fbUser.Gender = (friend["gender"] != null && friend["gender"].ToString().Trim() != "" ? friend["gender"].ToString().Trim().Replace("\"", "") : "");
                            fbUser.Link = (friend["link"] != null && friend["link"].ToString().Trim() != "" ? friend["link"].ToString().Trim().Replace("\"", "") : "");
                            fbUser.Source = "Website";
                            //fbUser.Picture = (friend["picture"] != null && friend["picture"].ToString().Trim() != "" ? friend["picture"].ToString().Trim().Replace("\"", "") : "");
                            //fbUser.Picture = "http://graph.facebook.com/"+fbUser.Id+"/pictures";

                            fbUsers.Add(fbUser);
                        }

                    }
                    catch { }

                    TempData["fbusers"] = fbUsers;
                    TempData.Keep("fbusers");
                    return RegisterFBUser(new RegisterExternalLoginModel { Email = email, Mobile = mobileNo, ProviderId = me.id, Provider = "Facebook", UserName = username, userData = userData, Gender = gender });

                }
                catch (Exception ex)
                {
                    if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                        return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                    else
                        return RedirectPermanent("/login"); 
                }
               
                if (System.Web.HttpContext.Current.Session["view"] != null)
                {
                    string calView = System.Web.HttpContext.Current.Session["view"].ToString();
                    Session.Remove("view");
                    return View(calView);
                }
                else if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString()))
                {
                    rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString();
                    return RedirectPermanent(rturl);
                }
                else
                {
                    return RedirectPermanent("/");

                }

            }
            catch(Exception ex)
            {
                
                if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                    return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                else
                    return RedirectPermanent("/login"); 
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateInput(false)]
        public ActionResult ExternalLogin(string provider="",string rturl="")
        {
            try
            {
                System.Web.HttpContext.Current.Session["rturl"] = null;
                if (Request.QueryString["rturl"] != null)
                {
                    System.Web.HttpContext.Current.Session["rturl"] = Request.QueryString["rturl"].Trim().Replace("$", "&");
                }
                else if (Request.QueryString["src"] != null && Request.QueryString["src"].ToString().Trim() != "")
                {
                    System.Web.HttpContext.Current.Session["rturl"] = CatchRTUrl(Request.QueryString["src"].ToString().Trim());
                }
                if (provider != null && provider.Trim() != "")
                {
                    return new ExternalLoginResult(provider, Url.Action("ExternalLoginCallback", new { Provider = provider, rturl = rturl }));
                }
                else
                {
                    if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                        return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                    else
                        return RedirectPermanent("/login"); 
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                    return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                else
                    return RedirectPermanent("/login"); 
            }
        }

        [AllowAnonymous]
        [ValidateInput(false)]
        public ActionResult ExternalLoginCallback(string provider, string rturl)
        {
            try
            {
                if (Request.QueryString["rturl"] != null)
                {
                    System.Web.HttpContext.Current.Session["rturl"] = Request.QueryString["rturl"].Trim().Replace("$", "&");
                    if (Request.QueryString["provider"] != null)
                    {
                        rturl = Request.QueryString["rturl"].ToString();
                    }
                }
                else if (Request.QueryString["src"] != null && Request.QueryString["src"].ToString().Trim() != "")
                {
                    System.Web.HttpContext.Current.Session["rturl"] = CatchRTUrl(Request.QueryString["src"].ToString().Trim().Replace("$", "&"));
                    if (Request.QueryString["provider"] != null && Request.QueryString["provider"].ToString().Trim() != "")
                    {
                        rturl = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim();
                    }
                }
                else if (System.Web.HttpContext.Current.Request.UrlReferrer != null && System.Web.HttpContext.Current.Request.UrlReferrer.ToString().Trim() != "")
                {
                    if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["src"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["src"].ToString().Trim() != "")
                    {
                        rturl = System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["src"].ToString().Trim();
                        System.Web.HttpContext.Current.Session["rturl"] = CatchRTUrl(rturl);
                        rturl = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim();
                    }
                }
                // Rewrite request before it gets passed on to the OAuth Web Security classes
                //if (provider == null)
                // GooglePlusClient.RewriteRequest();

                AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback"));

                if (!result.IsSuccessful)
                {
                    if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                        return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                    else
                        return RedirectPermanent("/login");
                }


                var userDataFromProvider = result.ExtraData;
                Dictionary<string, string> userData = new Dictionary<string, string>();
                if (result.Provider.ToLower() == "twitter")
                {
                    System.Web.HttpContext.Current.Session["TWuserid"] = result.ProviderUserId.ToString();
                    System.Web.HttpContext.Current.Session["TWscreenname"] = result.UserName.ToString();

                    oauth_token = userDataFromProvider["accesstoken"].ToString().Trim();
                    oauth_token_secret = userDataFromProvider["accesssecret"].ToString().Trim();

                    userData.Add("id", result.ProviderUserId);
                    userData.Add("name", result.UserName.ToString());

                    //================twitter followers/followings=================//

                    List<TwiterFollowers> listFollowers = new List<TwiterFollowers>();
                    try
                    {
                        resource_url = "https://api.twitter.com/1.1/followers/list.json";

                        authHeader = GenerateAuthHeader(resource_url);
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(resource_url);
                        request.Headers.Add("Authorization", authHeader);
                        request.Method = "GET";
                        request.ContentType = "application/x-www-form-urlencoded";
                        //using (Stream stream = request.GetRequestStream())
                        //{
                        //    byte[] content = ASCIIEncoding.ASCII.GetBytes(postBody);
                        //    stream.Write(content, 0, content.Length);
                        //}
                        WebResponse response = request.GetResponse();
                        string resTwt = new StreamReader(response.GetResponseStream()).ReadToEnd();
                        JObject j = JObject.Parse(resTwt);
                        JArray data = (JArray)j["users"];
                        int totalFollowers = (data).Count;
                        //TempData["lblTotalFollowers"] = "Nishant has " + (data).Count + " Followers";

                        if (data != null)
                        {

                            foreach (var item in data)
                            {
                                TwiterFollowers objTwiterFollowers = new TwiterFollowers();
                                objTwiterFollowers.ScreenName = item["screen_name"].ToString().Replace("\"", "");
                                objTwiterFollowers.ProfileImage = item["profile_image_url"].ToString().Replace("\"", "");
                                objTwiterFollowers.TwitterId = item["id"].ToString().Replace("\"", "");
                                objTwiterFollowers.Source = "Website";
                                listFollowers.Add(objTwiterFollowers);
                            }


                            //Random objRnd = new Random();
                            //List<TwiterFollowers> rendomFollowers = listFollowers.OrderBy(item => objRnd.Next()).ToList<TwiterFollowers>();
                            //for (int i = 0; i <= rendomFollowers.Count - 1; i++)
                            //{
                            //    strTwiterFollowers = strTwiterFollowers + "<li><a target='_blank' title='" + rendomFollowers[i].ScreenName + "' href=https://twitter.com/" + rendomFollowers[i].ScreenName + "><img src='" + rendomFollowers[i].ProfileImage + "'/><span>" + rendomFollowers[i].ScreenName + "</span></a></li>";

                            //}

                            /////////////////////TempData["strTwiterFollowers"] = strTwiterFollowers;

                        }

                        //==========================twitter friends=========================//
                        resource_url = "https://api.twitter.com/1.1/friends/list.json";
                        authHeader = GenerateAuthHeader(resource_url);

                        request = (HttpWebRequest)WebRequest.Create(resource_url);
                        request.Headers.Add("Authorization", authHeader);
                        request.Method = "GET";
                        request.ContentType = "application/x-www-form-urlencoded";

                        response = request.GetResponse();
                        resTwt = new StreamReader(response.GetResponseStream()).ReadToEnd();
                        j = JObject.Parse(resTwt);
                        data = (JArray)j["users"];
                        int totalFriends = (data).Count;
                        //TempData["lblTotalFriends"] = "Nishant has " + (data).Count + " Friends";

                        if (data != null)
                        {
                            //List<TwiterFriends> listFriends = new List<TwiterFriends>();
                            foreach (var item in data)
                            {
                                TwiterFollowers objTwiterFriends = new TwiterFollowers();
                                objTwiterFriends.ScreenName = item["screen_name"].ToString().Replace("\"", "");
                                objTwiterFriends.ProfileImage = item["profile_image_url"].ToString().Replace("\"", "");
                                objTwiterFriends.TwitterId = item["id"].ToString().Replace("\"", "");
                                objTwiterFriends.Source = "Website";
                                listFollowers.Add(objTwiterFriends);
                            }


                            //Random objRnd = new Random();
                            //List<TwiterFriends> rendomFriends = listFriends.OrderBy(item => objRnd.Next()).ToList<TwiterFriends>();
                            //for (int i = 0; i <= rendomFriends.Count - 1; i++)
                            //{
                            //    strTwiterFriends = strTwiterFriends + "<li><a target='_blank' title='" + rendomFriends[i].ScreenName + "' href=https://twitter.com/" + rendomFriends[i].ScreenName + "><img src='" + rendomFriends[i].ProfileImage + "'/><span>" + rendomFriends[i].ScreenName + "</span></a></li>";

                            //}

                            //    TempData["strTwiterFriends"] = strTwiterFriends;

                        }

                    }
                    catch (Exception ex) { }
                    //==========================twitter friends=========================//

                    if (listFollowers.Count > 0)
                    {
                        TempData["strTwiterFollowers"] = listFollowers;
                        TempData.Keep("strTwiterFollowers");
                    }
                    //================twitter followers/followings=================//


                    return RegisterTwitterUser(new RegisterExternalLoginModel { ProviderId = result.ProviderUserId, Provider = result.Provider, UserName = result.UserName, userData = userData });
                }
                else if (result.Provider.ToLower() == "google")
                {
                    Uri myUri = new Uri(result.ProviderUserId);
                    string id = System.Web.HttpUtility.ParseQueryString(myUri.Query).Get("id");

                    userData.Add("id", id);
                    userData.Add("username", result.UserName.ToString() != "" ? result.UserName : "");
                    userData.Add("email", result.UserName.ToString() != "" ? result.UserName : "");

                    return RegisterGoogleUser(userData);
                }


                if (!string.IsNullOrEmpty(objUS.SV_VCUserName) && objUS.SV_VCUserName.ToString().Trim() != "")
                {
                    MyGlobalVariables._checkLoggedin = 1;
                }

                if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                {
                    rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString();
                    return RedirectPermanent(rturl);
                }
                else
                {
                    return RedirectPermanent("/");
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                    return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                else
                    return RedirectPermanent("/login");
            }
        }
        //public ActionResult ExternalLoginCallback(string provider,string rturl)
        //{
        //    try
        //    {
        //       if (Request.QueryString["rturl"] != null)
        //        {
        //            System.Web.HttpContext.Current.Session["rturl"] = Request.QueryString["rturl"].Trim().Replace("$", "&");
        //            if (Request.QueryString["provider"] != null)
        //            {
        //                rturl = Request.QueryString["rturl"].ToString();
        //            }
        //        }
        //       else if (Request.QueryString["src"] != null && Request.QueryString["src"].ToString().Trim() != "")
        //       {
        //           System.Web.HttpContext.Current.Session["rturl"] = CatchRTUrl(Request.QueryString["src"].ToString().Trim().Replace("$","&"));
        //           if (Request.QueryString["provider"] != null && Request.QueryString["provider"].ToString().Trim() != "")
        //           {
        //               rturl = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim();  
        //           }
        //       }
        //       else if (System.Web.HttpContext.Current.Request.UrlReferrer != null && System.Web.HttpContext.Current.Request.UrlReferrer.ToString().Trim() != "")
        //       {
        //           if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["src"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["src"].ToString().Trim() != "")
        //           {
        //               rturl = System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["src"].ToString().Trim();
        //               System.Web.HttpContext.Current.Session["rturl"] = CatchRTUrl(rturl);
        //               rturl = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim();
        //           }
        //       }
        //        // Rewrite request before it gets passed on to the OAuth Web Security classes
        //        //if (provider == null)
        //            // GooglePlusClient.RewriteRequest();

        //        AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback"));

        //        if (!result.IsSuccessful)
        //        {
        //            if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
        //                return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
        //            else
        //                return RedirectPermanent("/login"); 
        //        }


        //        var userDataFromProvider = result.ExtraData;
        //        Dictionary<string, string> userData = new Dictionary<string, string>();
        //        if (result.Provider.ToLower() == "twitter")
        //        {
        //            System.Web.HttpContext.Current.Session["TWuserid"] = result.ProviderUserId.ToString();
        //            System.Web.HttpContext.Current.Session["TWscreenname"] = result.UserName.ToString();

        //            oauth_token = userDataFromProvider["accesstoken"].ToString().Trim();
        //            oauth_token_secret = userDataFromProvider["accesssecret"].ToString().Trim();

        //            userData.Add("id", result.ProviderUserId);
        //            userData.Add("name", result.UserName.ToString());

        //            //================twitter followers/followings=================//

        //            List<TwiterFollowers> listFollowers = new List<TwiterFollowers>();
        //            try
        //            {
        //                resource_url = "https://api.twitter.com/1.1/followers/list.json";

        //                authHeader = GenerateAuthHeader(resource_url);
        //                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(resource_url);
        //                request.Headers.Add("Authorization", authHeader);
        //                request.Method = "GET";
        //                request.ContentType = "application/x-www-form-urlencoded";
        //                //using (Stream stream = request.GetRequestStream())
        //                //{
        //                //    byte[] content = ASCIIEncoding.ASCII.GetBytes(postBody);
        //                //    stream.Write(content, 0, content.Length);
        //                //}
        //                WebResponse response = request.GetResponse();
        //                string resTwt = new StreamReader(response.GetResponseStream()).ReadToEnd();
        //                JObject j = JObject.Parse(resTwt);
        //                JArray data = (JArray)j["users"];
        //                int totalFollowers = (data).Count;
        //                //TempData["lblTotalFollowers"] = "Nishant has " + (data).Count + " Followers";

        //                if (data != null)
        //                {

        //                    foreach (var item in data)
        //                    {
        //                        TwiterFollowers objTwiterFollowers = new TwiterFollowers();
        //                        objTwiterFollowers.ScreenName = item["screen_name"].ToString().Replace("\"", "");
        //                        objTwiterFollowers.ProfileImage = item["profile_image_url"].ToString().Replace("\"", "");
        //                        objTwiterFollowers.TwitterId = item["id"].ToString().Replace("\"", "");
        //                        objTwiterFollowers.Source = "Website";
        //                        listFollowers.Add(objTwiterFollowers);
        //                    }


        //                    //Random objRnd = new Random();
        //                    //List<TwiterFollowers> rendomFollowers = listFollowers.OrderBy(item => objRnd.Next()).ToList<TwiterFollowers>();
        //                    //for (int i = 0; i <= rendomFollowers.Count - 1; i++)
        //                    //{
        //                    //    strTwiterFollowers = strTwiterFollowers + "<li><a target='_blank' title='" + rendomFollowers[i].ScreenName + "' href=https://twitter.com/" + rendomFollowers[i].ScreenName + "><img src='" + rendomFollowers[i].ProfileImage + "'/><span>" + rendomFollowers[i].ScreenName + "</span></a></li>";

        //                    //}

        //                    /////////////////////TempData["strTwiterFollowers"] = strTwiterFollowers;

        //                }

        //                //==========================twitter friends=========================//
        //                resource_url = "https://api.twitter.com/1.1/friends/list.json";
        //                authHeader = GenerateAuthHeader(resource_url);

        //                request = (HttpWebRequest)WebRequest.Create(resource_url);
        //                request.Headers.Add("Authorization", authHeader);
        //                request.Method = "GET";
        //                request.ContentType = "application/x-www-form-urlencoded";

        //                response = request.GetResponse();
        //                resTwt = new StreamReader(response.GetResponseStream()).ReadToEnd();
        //                j = JObject.Parse(resTwt);
        //                data = (JArray)j["users"];
        //                int totalFriends = (data).Count;
        //                //TempData["lblTotalFriends"] = "Nishant has " + (data).Count + " Friends";

        //                if (data != null)
        //                {
        //                    //List<TwiterFriends> listFriends = new List<TwiterFriends>();
        //                    foreach (var item in data)
        //                    {
        //                        TwiterFollowers objTwiterFriends = new TwiterFollowers();
        //                        objTwiterFriends.ScreenName = item["screen_name"].ToString().Replace("\"", "");
        //                        objTwiterFriends.ProfileImage = item["profile_image_url"].ToString().Replace("\"", "");
        //                        objTwiterFriends.TwitterId = item["id"].ToString().Replace("\"", "");
        //                        objTwiterFriends.Source = "Website";
        //                        listFollowers.Add(objTwiterFriends);
        //                    }


        //                    //Random objRnd = new Random();
        //                    //List<TwiterFriends> rendomFriends = listFriends.OrderBy(item => objRnd.Next()).ToList<TwiterFriends>();
        //                    //for (int i = 0; i <= rendomFriends.Count - 1; i++)
        //                    //{
        //                    //    strTwiterFriends = strTwiterFriends + "<li><a target='_blank' title='" + rendomFriends[i].ScreenName + "' href=https://twitter.com/" + rendomFriends[i].ScreenName + "><img src='" + rendomFriends[i].ProfileImage + "'/><span>" + rendomFriends[i].ScreenName + "</span></a></li>";

        //                    //}

        //                    //    TempData["strTwiterFriends"] = strTwiterFriends;

        //                }

        //            }
        //            catch (Exception ex) { }             
        //            //==========================twitter friends=========================//

        //            if (listFollowers.Count > 0)
        //            {
        //                TempData["strTwiterFollowers"] = listFollowers;
        //                TempData.Keep("strTwiterFollowers");
        //            }
        //            //================twitter followers/followings=================//


        //            return RegisterTwitterUser(new RegisterExternalLoginModel { ProviderId = result.ProviderUserId, Provider = result.Provider, UserName = result.UserName, userData = userData });
        //        }
        //        else if (result.Provider.ToLower() == "google")
        //        {
        //            Uri myUri = new Uri(result.ProviderUserId);
        //            string id = System.Web.HttpUtility.ParseQueryString(myUri.Query).Get("id");

        //            userData.Add("id", id);
        //            userData.Add("username", result.UserName.ToString() != "" ? result.UserName : "");
        //            userData.Add("email", result.UserName.ToString() != "" ? result.UserName : "");
                    
        //            return RegisterGoogleUser(userData);
        //        }

              
        //        if (System.Web.HttpContext.Current.Request.Cookies["-VCUserName-"] != null && System.Web.HttpContext.Current.Request.Cookies["-VCUserName-"].ToString().Trim() != "")
        //        {
        //            MyGlobalVariables._checkLoggedin = 1;
        //        }

        //        if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
        //        {
        //            rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString();
        //            return RedirectPermanent(rturl);
        //        }
        //        else
        //        {
        //            return RedirectPermanent("/");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.LogMe(ex);
        //        if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
        //            return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
        //        else
        //            return RedirectPermanent("/login"); 
        //    }
        //}

        protected string GenerateAuthHeader(string resorcUrl)
        {
            // oauth implementation details
            var oauth_version = "1.0";
            var oauth_signature_method = "HMAC-SHA1";

            // unique request details
            var oauth_nonce = Convert.ToBase64String(
                new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
            var timeSpan = DateTime.UtcNow
                - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var oauth_timestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();

            // message api details
            //var status = "Updating status via REST API 1.1";
            resource_url = resorcUrl;

            // create oauth signature
            var baseFormat = "oauth_consumer_key={0}&oauth_nonce={1}&oauth_signature_method={2}" +
                            "&oauth_timestamp={3}&oauth_token={4}&oauth_version={5}";

            var baseString = string.Format(baseFormat,
                                        oauth_consumer_key,
                                        oauth_nonce,
                                        oauth_signature_method,
                                        oauth_timestamp,
                                        oauth_token,
                                        oauth_version
                //,Uri.EscapeDataString(status)
                                        );

            baseString = string.Concat("GET&", Uri.EscapeDataString(resource_url), "&", Uri.EscapeDataString(baseString));

            var compositeKey = string.Concat(Uri.EscapeDataString(oauth_consumer_secret),
                                    "&", Uri.EscapeDataString(oauth_token_secret));

            string oauth_signature;
            using (HMACSHA1 hasher = new HMACSHA1(ASCIIEncoding.ASCII.GetBytes(compositeKey)))
            {
                oauth_signature = Convert.ToBase64String(
                    hasher.ComputeHash(ASCIIEncoding.ASCII.GetBytes(baseString)));
            }

            // create the request header
            var headerFormat = "OAuth oauth_nonce=\"{0}\", oauth_signature_method=\"{1}\", " +
                               "oauth_timestamp=\"{2}\", oauth_consumer_key=\"{3}\", " +
                               "oauth_token=\"{4}\", oauth_signature=\"{5}\", " +
                               "oauth_version=\"{6}\"";

            var authHeader = string.Format(headerFormat,
                                    Uri.EscapeDataString(oauth_nonce),
                                    Uri.EscapeDataString(oauth_signature_method),
                                    Uri.EscapeDataString(oauth_timestamp),
                                    Uri.EscapeDataString(oauth_consumer_key),
                                    Uri.EscapeDataString(oauth_token),
                                    Uri.EscapeDataString(oauth_signature),
                                    Uri.EscapeDataString(oauth_version)
                            );


            // make the request
            // var postBody = "status=" + Uri.EscapeDataString(status);

            ServicePointManager.Expect100Continue = false;

            return authHeader;
        }


        [HttpGet]
        [AllowAnonymous]
        [ValidateInput(false)]
        public ActionResult ExternalLoginConfirmation()
        {
            TempData.Keep("fbusers");

            RegisterExternalLoginModel model = new RegisterExternalLoginModel();
            
            if (Request.QueryString["UserName"] != null && Request.QueryString["Email"] != null && Request.QueryString["IsEmailNull"] != null && Request.QueryString["UserName"].ToString() != "" && Request.QueryString["Email"].ToString() != "" && Request.QueryString["IsEmailNull"].ToString() != "")
            {
                model.UserName = Request.QueryString["UserName"].ToString().Trim();
                model.Email = Request.QueryString["Email"].ToString().Trim();
                model.IsEmailNull = Convert.ToInt32( Request.QueryString["IsEmailNull"].ToString().Trim());
                model.Provider = Request.QueryString["Provider"] != null ? Request.QueryString["Provider"] : "";
                model.ProviderId = Request.QueryString["ProviderId"] != null ? Request.QueryString["ProviderId"] : "";
                System.Web.HttpContext.Current.Session["ELmodel"] = model;
            }

            if (model.ProviderId == null || model.ProviderId.Trim() == "")
            {
                if (System.Web.HttpContext.Current.Session["ELmodel"] != null)
                {
                    Type t = System.Web.HttpContext.Current.Session["ELmodel"].GetType();
                    if (t == typeof(RegisterExternalLoginModel))
                    {
                        model = (RegisterExternalLoginModel)System.Web.HttpContext.Current.Session["ELmodel"];
                    }
                }

            }
            return View(model);
        }
         
        [HttpPost]
        [AllowAnonymous]
        [ValidateInput(false)]
        public ActionResult ExternalLoginConfirmation(string Email)
        {
            TempData.Keep("fbusers");

            RegisterExternalLoginModel model = new RegisterExternalLoginModel();
            if (System.Web.HttpContext.Current.Session["ELmodel"] != null)
            {
                Type t = System.Web.HttpContext.Current.Session["ELmodel"].GetType();
                if (t == typeof(RegisterExternalLoginModel))
                {
                    model = (RegisterExternalLoginModel)System.Web.HttpContext.Current.Session["ELmodel"];
                    model.Email = Email;
                    Session.Remove("ELmodel");
                }
            }

            if (string.IsNullOrEmpty(model.Email) || string.IsNullOrWhiteSpace(model.Email) || model.Email.Trim() == "Your email address")
            {
                ViewBag.UserAlert = "<p style=\"color:red;\">Enter your valid email address</p>";
                System.Web.HttpContext.Current.Session["ELmodel"] = model;
                return RedirectToRoute("externalloginconfirmation");
            }
            try
            {
                if (!string.IsNullOrWhiteSpace(model.Mobile) && !string.IsNullOrEmpty(model.Mobile.Trim()))
                {
                    long result;
                    if (long.TryParse(model.Mobile.Trim(), out result))
                    {
                        int vaildphone = Utility.isValidPhone(model.Mobile.Trim());
                        if (vaildphone == 0)
                        {
                            ViewBag.UserAlert = "phone no is not valid.";
                            System.Web.HttpContext.Current.Session["ELmodel"] = model;
                            return RedirectToRoute("externalloginconfirmation");
                        }
                        else
                            ViewBag.UserAlert = "";
                    }
                    else
                    {
                        ViewBag.UserAlert = "phone no is not valid.";
                        System.Web.HttpContext.Current.Session["ELmodel"] = model;
                        return RedirectToRoute("externalloginconfirmation");
                    }
                }
                else { model.Mobile = ""; }

                if (model.Provider != null)
                {
                    switch (model.Provider.ToLower())
                    {
                        case "facebook":
                            RegisterFBUser(model);
                            break;
                        case "twitter":
                            RegisterTwitterUser(model);
                            break;
                    }
                }

                if (!string.IsNullOrEmpty(objUS.SV_VCUserName) && objUS.SV_VCUserName.ToString().Trim() != "")
                {
                    MyGlobalVariables._checkLoggedin = 1;
                    Session.Add("_checkLoggedin", "1");
                }
                if (!string.IsNullOrEmpty(objUS.SV_VCUserName))
                {
                    var cookie = objUS.SV_VCUserName;
                    ViewBag.MyCookie = cookie;
                }
                if (System.Web.HttpContext.Current.Session["view"] != null && System.Web.HttpContext.Current.Session["view"].ToString().Trim() != "")
                {
                    string calView = System.Web.HttpContext.Current.Session["view"].ToString().Trim();
                    Session.Remove("view");
                    return RedirectToRoutePermanent(calView, new { opt = "1" });
                }
                else if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString()))
                {
                    string rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim();
                    return RedirectPermanent(rturl);
                }
                else
                    return RedirectPermanent("/");
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                System.Web.HttpContext.Current.Session["ELmodel"] = model;
                return RedirectToRoute("externalloginconfirmation");
            }
        }
        //public ActionResult ExternalLoginConfirmation(string Email)
        //{
        //    TempData.Keep("fbusers");

        //    RegisterExternalLoginModel model = new RegisterExternalLoginModel();
        //    if (System.Web.HttpContext.Current.Session["ELmodel"] != null)
        //    {
        //        Type t = System.Web.HttpContext.Current.Session["ELmodel"].GetType();
        //        if (t == typeof(RegisterExternalLoginModel))
        //        {
        //            model = (RegisterExternalLoginModel)System.Web.HttpContext.Current.Session["ELmodel"];
        //            model.Email = Email;
        //            Session.Remove("ELmodel");
        //        }
        //    }

        //    if (string.IsNullOrEmpty(model.Email) || string.IsNullOrWhiteSpace(model.Email) || model.Email.Trim() == "Your email address")
        //    {
        //        ViewBag.UserAlert = "<p style=\"color:red;\">Enter your valid email address</p>";
        //        System.Web.HttpContext.Current.Session["ELmodel"] = model;
        //        return RedirectToRoute("externalloginconfirmation");
        //    }
        //    try
        //    {
        //        if (!string.IsNullOrWhiteSpace(model.Mobile) && !string.IsNullOrEmpty(model.Mobile.Trim()))
        //        {
        //            long result;
        //            if (long.TryParse(model.Mobile.Trim(), out result))
        //            {
        //                int vaildphone = Utility.isValidPhone(model.Mobile.Trim());
        //                if (vaildphone == 0)
        //                {
        //                    ViewBag.UserAlert = "phone no is not valid.";
        //                    System.Web.HttpContext.Current.Session["ELmodel"] = model;
        //                    return RedirectToRoute("externalloginconfirmation");
        //                }
        //                else
        //                    ViewBag.UserAlert = "";
        //            }
        //            else
        //            {
        //                ViewBag.UserAlert = "phone no is not valid.";
        //                System.Web.HttpContext.Current.Session["ELmodel"] = model;
        //                return RedirectToRoute("externalloginconfirmation");
        //            }
        //        }
        //        else { model.Mobile = ""; }

        //        if (model.Provider != null)
        //        {
        //            switch (model.Provider.ToLower())
        //            {
        //                case "facebook":
        //                    RegisterFBUser(model);
        //                    break;
        //                case "twitter":
        //                    RegisterTwitterUser(model);
        //                    break;
        //            }
        //        }

        //        if (System.Web.HttpContext.Current.Request.Cookies["-VCUserName-"] != null && System.Web.HttpContext.Current.Request.Cookies["-VCUserName-"].ToString().Trim() != "")
        //            MyGlobalVariables._checkLoggedin = 1;

        //        if (System.Web.HttpContext.Current.Request.Cookies["-VCUserName-"] != null)
        //        {
        //            var cookie = System.Web.HttpContext.Current.Request.Cookies["-VCUserName-"];
        //            ViewBag.MyCookie = cookie.Value;
        //        }
        //        if (System.Web.HttpContext.Current.Session["view"] != null && System.Web.HttpContext.Current.Session["view"].ToString().Trim() != "")
        //        {
        //            string calView = System.Web.HttpContext.Current.Session["view"].ToString().Trim();
        //            Session.Remove("view");
        //            return RedirectToRoutePermanent(calView, new { opt = "1" });
        //        }
        //        else if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString()))
        //        {
        //            string rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim();
        //            return RedirectPermanent(rturl);
        //        }
        //        else
        //            return RedirectPermanent("/");
        //    }
        //    catch (Exception ex)
        //    {
        //        log.LogMe(ex);
        //        System.Web.HttpContext.Current.Session["ELmodel"] = model;
        //        return RedirectToRoute("externalloginconfirmation");
        //    }
        //}

        [AllowAnonymous]
       
        [ValidateInput(false)]
        public ActionResult ExternalLoginsList()
        {
            try
            {
                return PartialView("_ExternalLoginsListPartial", OAuthWebSecurity.RegisteredClientData);
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                    return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                else
                    return RedirectPermanent("/login"); 
                   
            }
        }

        [ValidateInput(false)]
        public ActionResult RegisterTwitterUser(RegisterExternalLoginModel model)
        {
            try
            {

                TempData["UserAlert"] = "";
                string rturl = "";
                List<TwiterFollowers> twFollowers = new List<TwiterFollowers>();
                if (TempData["strTwiterFollowers"] != null)
                {
                    twFollowers = (List<TwiterFollowers>)TempData.Peek("strTwiterFollowers");
                    TempData.Keep("strTwiterFollowers");
                }


                string VCUserPassword = string.Empty;
                string VCVerificationCode = string.Empty;
                string VCLoginPassword = string.Empty;
                string VCLoginName = string.Empty;
                string VCUserContentID = string.Empty;
                bool VCIsMobileVerified = false;
                string VCUserBusinessID = string.Empty;
                string VCUserType = string.Empty;
                string VCUserEmail = string.Empty;
                string VCUserPhone = string.Empty;
                string VCUserStateid = string.Empty;
                string VCUserCityid = string.Empty;
                string VCUserState = string.Empty;
                string VCUserCity = string.Empty;
                string VCUserPhoto = string.Empty;
                string VCUserPhotoMobile = string.Empty;
                string VCUserUrl = string.Empty;
                reversedns = Utility.GetReversedns(Utility.GetIpAddress());

                if (model.rurl == null)
                {
                    System.Web.HttpContext.Current.Session["lgnProvider"] = "tw";
                }
                string source = "twitter";
                string TWUID = !string.IsNullOrWhiteSpace(model.ProviderId) ? model.ProviderId.ToString().Trim() : "";
                string screenname = !string.IsNullOrWhiteSpace(model.UserName) ? model.UserName.ToString().Trim() : "";

                source = "twitter";

                string resTWID = WEBLoginDatabaseCall.CheckTwiiterUserID(TWUID, "twitter");
                string[] arrResTWID = resTWID.Split(',');
                if (arrResTWID.Length > 0 && arrResTWID[0].ToString() == "1")
                {
                    DataTable userdt = WEBLoginDatabaseCall.GetUserInfo(arrResTWID[1].ToString().Trim());
                    if (userdt.Rows.Count > 0 && !string.IsNullOrWhiteSpace(userdt.Rows[0]["isemailverified"].ToString()) && userdt.Rows[0]["isemailverified"].ToString().Trim() != "0" && userdt.Rows[0]["isemailverified"].ToString().Trim().ToLower() != "false" && !string.IsNullOrWhiteSpace(userdt.Rows[0]["isverified"].ToString()) && userdt.Rows[0]["isverified"].ToString().Trim() != "0" && userdt.Rows[0]["isverified"].ToString().Trim().ToLower() != "false")
                    {
                        VCLoginPassword = "";
                        VCLoginName = !string.IsNullOrWhiteSpace(userdt.Rows[0]["screenname"].ToString()) ? userdt.Rows[0]["screenname"].ToString() : "";
                        VCUserContentID = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userid"].ToString()) ? userdt.Rows[0]["userid"].ToString() : "0";

                        if (string.IsNullOrWhiteSpace(userdt.Rows[0]["ismobileverified"].ToString()))
                            VCIsMobileVerified = false;
                        else
                            VCIsMobileVerified = Convert.ToBoolean(userdt.Rows[0]["ismobileverified"].ToString());


                        VCUserBusinessID = "0";
                        VCUserType = !string.IsNullOrWhiteSpace(userdt.Rows[0]["usertype"].ToString()) ? userdt.Rows[0]["usertype"].ToString() : "0";
                        VCUserEmail = !string.IsNullOrWhiteSpace(userdt.Rows[0]["loginemail"].ToString()) ? userdt.Rows[0]["loginemail"].ToString() : "";
                        VCUserPhone = !string.IsNullOrWhiteSpace(userdt.Rows[0]["mobile"].ToString()) ? userdt.Rows[0]["mobile"].ToString() : "";
                        VCUserStateid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["stateid"].ToString()) ? userdt.Rows[0]["stateid"].ToString() : "";
                        VCUserCityid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityid"].ToString()) ? userdt.Rows[0]["cityid"].ToString() : "";
                        VCUserState = !string.IsNullOrWhiteSpace(userdt.Rows[0]["statename"].ToString()) ? userdt.Rows[0]["statename"].ToString() : "";
                        VCUserCity = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityname"].ToString()) ? userdt.Rows[0]["cityname"].ToString() : "";
                        VCUserPhoto = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photo"].ToString().Trim()) ? userdt.Rows[0]["photo"].ToString().Trim() : "";
                        VCUserPhotoMobile = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photomobile"].ToString().Trim()) ? userdt.Rows[0]["photomobile"].ToString().Trim() : "";
                        VCUserUrl = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userUrl"].ToString().Trim()) ? userdt.Rows[0]["userUrl"].ToString().Trim() : "";

                        UserSession.GenerateSession("twitter web", (!string.IsNullOrWhiteSpace(userdt.Rows[0]["loginemail"].ToString()) ? userdt.Rows[0]["loginemail"].ToString().Trim() : ""), VCLoginName.Trim(), VCUserContentID.Trim(), VCUserType.Trim(), model.ProviderId, VCUserPhoto, VCUserUrl, VCUserEmail, VCUserPhone);
                        userdt.Dispose();
                        if (long.TryParse(VCUserType, out resIsInt) && long.TryParse(VCUserContentID, out resIsInt) && long.TryParse(VCUserBusinessID, out resIsInt))
                        {

                            if (System.Web.HttpContext.Current.Session["rturl"] != null && System.Web.HttpContext.Current.Session["rturl"].ToString().Trim() != "")
                            {
                                sourceCodeVal = ReturnURIValue(System.Web.HttpContext.Current.Session["rturl"].ToString());
                                if (sourceCodeVal.ToString().Trim() != "" && sourceCodeVal >= 0)
                                {
                                    WEBLoginDatabaseCall.AddLoginUserLog("Twitter Web", VCLoginName, Convert.ToInt32(VCUserType), objUS.SV_VCLoginID.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), Convert.ToInt32(VCUserBusinessID), 1, reversedns, sourceCodeVal);
                                }
                                else
                                {
                                    WEBLoginDatabaseCall.AddLoginUserLog("Twitter Web", VCLoginName, Convert.ToInt32(VCUserType), objUS.SV_VCLoginID.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), Convert.ToInt32(VCUserBusinessID), 1, reversedns, 0);

                                }
                            }
                            else
                            {
                                WEBLoginDatabaseCall.AddLoginUserLog("Twitter Web", VCLoginName, Convert.ToInt32(VCUserType), objUS.SV_VCLoginID.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), Convert.ToInt32(VCUserBusinessID), 1, reversedns, 0);
                            }
                        }


                        if (!long.TryParse(VCUserContentID, out resIsInt))
                        {
                            VCUserContentID = "0";
                        }

                        //twfollowers list 
                        if (twFollowers != null && twFollowers.Count > 0)
                        {
                            WEBLoginDatabaseCall.DBSaveTWUserFollowersList(twFollowers, Convert.ToInt32(VCUserContentID));
                            TempData["strTwiterFollowers"] = null;
                        }

                        if (model.rurl != null)
                        {
                            rturl = "/" + model.rurl.ToString();
                            return RedirectPermanent(rturl);
                        }

                        if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                        {
                            int indx = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().IndexOf("&");
                            if (indx > 0)
                                rturl = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Substring(0, indx);

                            rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("#_=_", "");

                            if (rturl.Contains("http://"))
                            {
                                indx = rturl.IndexOf("http");
                                if (indx > 0)
                                    rturl = rturl.Trim().Substring(indx, rturl.Length - 1);

                                if (CheckUrlBeforeProcessing(rturl))
                                    return RedirectPermanent(rturl);
                                else
                                    return RedirectPermanent("/");
                            }
                            else
                            {
                                rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim();
                                return RedirectPermanent(rturl);
                            }

                        }
                        else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
                        {

                            if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"].ToString().Trim() != "")
                            {
                                rturl = System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"].ToString().Trim();
                                return RedirectPermanent(rturl);
                            }
                            else
                            {
                                return RedirectPermanent("/");
                            }
                        }
                        else
                        {
                            return RedirectPermanent("/");
                        }


                        //if (model.rurl != null)
                        //{
                        //    rturl = "/" + model.rurl.ToString();
                        //    return Redirect(rturl);
                        //}

                        //if (System.Web.HttpContext.Current.Session["rturl"] != null)
                        //{
                        //    rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString();
                        //    return Redirect(rturl);
                        //}
                        //else
                        //{
                        //    return Redirect("/");
                        //}
                    }
                    else
                    {

                        LoginModel lgModel = new LoginModel();
                        lgModel.Login = arrResTWID[1].ToString().Trim();
                        lgModel.CheckEmailVerification = 1;
                        lgModel.isRedirect = 1;
                        System.Web.HttpContext.Current.Session["model"] = null;

                        generateloginidverification(lgModel, "");
                        return View("Verification");
                    }
                }

                if ((string.IsNullOrEmpty(model.Email)) || (string.IsNullOrEmpty(TWUID)))
                {
                    ViewBag.Provider = "Twitter";
                    model.UserName = screenname;
                    System.Web.HttpContext.Current.Session["ELmodel"] = model;
                    return RedirectToRoute("externalloginconfirmation");
                }
                else
                {

                    if (WEBLoginDatabaseCall.CheckEmailID(model.Email.ToString().Trim())) // save new entry
                    {
                        string password = WEBLoginDatabaseCall.Generate(6);
                        int uid;

                        string result = WEBLoginDatabaseCall.NewUser(TWUID.ToString(), password.ToString(), screenname, model.Mobile, model.Email, "NewUser".ToString(), source, "Website", out uid);
                        if (result.Trim() == "Success" && !(Int32.Parse(VCUserContentID) == 0))
                        {
                            SendEmail("", "", "", "WelcomeAboard.html", 3, VCUserEmail.Trim(), ""
                                   , Int32.Parse(VCUserContentID));
                        }
                        if (result.Trim() == Result.Success.ToString().Trim() || result.Trim() == "Already Exists")
                        {

                            DataTable userdt = WEBLoginDatabaseCall.GetUserInfo(model.Email.Trim(), TWUID, "twitter", "");
                            if (userdt.Rows.Count > 0)
                            {
                                if (string.IsNullOrWhiteSpace(userdt.Rows[0]["isemailverified"].ToString()) || userdt.Rows[0]["isemailverified"].ToString().Trim() == "0" || userdt.Rows[0]["isemailverified"].ToString().Trim().ToLower() == "false")
                                {
                                    LoginModel lgModel = new LoginModel();
                                    lgModel.Login = model.Email.Trim();
                                    lgModel.CheckEmailVerification = 1;
                                    lgModel.isRedirect = 1;
                                    System.Web.HttpContext.Current.Session["lgnProvider"] = "tw";
                                    System.Web.HttpContext.Current.Session["model"] = null;
                                    generateloginidverification(lgModel, "");
                                    if (System.Web.HttpContext.Current.Session["view"] != null)
                                    {
                                        return RedirectToRoutePermanent(System.Web.HttpContext.Current.Session["view"].ToString().Trim());
                                    }
                                }

                                VCLoginPassword = "";
                                VCLoginName = !string.IsNullOrWhiteSpace(userdt.Rows[0]["screenname"].ToString()) ? userdt.Rows[0]["screenname"].ToString() : "";
                                VCUserContentID = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userid"].ToString()) ? userdt.Rows[0]["userid"].ToString() : "0";

                                if (string.IsNullOrWhiteSpace(userdt.Rows[0]["ismobileverified"].ToString()))
                                    VCIsMobileVerified = false;
                                else
                                    VCIsMobileVerified = Convert.ToBoolean(userdt.Rows[0]["ismobileverified"].ToString());

                                VCUserBusinessID = "0";
                                VCUserType = !string.IsNullOrWhiteSpace(userdt.Rows[0]["usertype"].ToString()) ? userdt.Rows[0]["usertype"].ToString() : "0";
                                VCUserEmail = !string.IsNullOrWhiteSpace(userdt.Rows[0]["loginemail"].ToString()) ? userdt.Rows[0]["loginemail"].ToString() : "";
                                VCUserPhone = !string.IsNullOrWhiteSpace(userdt.Rows[0]["mobile"].ToString()) ? userdt.Rows[0]["mobile"].ToString() : "";
                                VCUserStateid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["stateid"].ToString()) ? userdt.Rows[0]["stateid"].ToString() : "";
                                VCUserCityid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityid"].ToString()) ? userdt.Rows[0]["cityid"].ToString() : "";
                                VCUserState = !string.IsNullOrWhiteSpace(userdt.Rows[0]["statename"].ToString()) ? userdt.Rows[0]["statename"].ToString() : "";
                                VCUserCity = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityname"].ToString()) ? userdt.Rows[0]["cityname"].ToString() : "";
                                VCUserPhoto = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photo"].ToString().Trim()) ? userdt.Rows[0]["photo"].ToString().Trim() : "";
                                VCUserPhotoMobile = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photomobile"].ToString().Trim()) ? userdt.Rows[0]["photomobile"].ToString().Trim() : "";
                                VCUserUrl = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userUrl"].ToString().Trim()) ? userdt.Rows[0]["userUrl"].ToString().Trim() : "";

                                UserSession.GenerateSession("twitter web", model.Email.Trim(), VCLoginName.Trim(), VCUserContentID.Trim(), VCUserType.Trim(), model.ProviderId, VCUserPhoto, VCUserUrl, VCUserEmail, VCUserPhone);
                                userdt.Dispose();

                                ViewBag.UserMsg = "Login ID already registered but email not verified. Please verify email by clicking on the link already been sent to (" + VCUserEmail.Trim() + ") a moment ago.";
                            }
                        }
                        else if (result.Trim() == "Already Exists with other ID")
                        {
                            ViewBag.Provider = "Twitter";
                            model.UserName = screenname;
                            System.Web.HttpContext.Current.Session["view"] = "ExternalLoginConfirmation";
                            System.Web.HttpContext.Current.Session["ELmodel"] = model;
                            TempData["UserAlert"] = "<p style='color:red;'>This email address already associated with another twitter account.</p>";
                            TempData.Keep("UserAlert");
                            return RedirectToRoute("externalloginconfirmation");
                        }
                        var cookie = objUS.SV_VCUserName;
                        ViewBag.MyCookie = cookie;

                    }
                    else  // check if there Twitter ID Exists on that Email ID
                    {
                        if (WEBLoginDatabaseCall.IsEmailAssociatedWithOtherId(model.Email.Trim()))
                        {
                            ViewBag.Provider = "Twitter";
                            model.UserName = screenname;
                            System.Web.HttpContext.Current.Session["view"] = "ExternalLoginConfirmation";
                            System.Web.HttpContext.Current.Session["ELmodel"] = model;
                            TempData["UserAlert"] = "<p style='color:red;'>This email address already associated with another twitter account.</p>";
                            TempData.Keep("UserAlert");
                            return RedirectToRoute("externalloginconfirmation");
                        }
                        else
                        {
                            DataTable userdt = WEBLoginDatabaseCall.GetUserInfo(model.Email.Trim());
                            if (userdt.Rows.Count > 0)
                            {
                                if (string.IsNullOrWhiteSpace(userdt.Rows[0]["isemailverified"].ToString()) || userdt.Rows[0]["isemailverified"].ToString().Trim() == "0" || userdt.Rows[0]["isemailverified"].ToString().Trim().ToLower() == "false")
                                {
                                    LoginModel lgModel = new LoginModel();
                                    lgModel.Login = model.Email.Trim();
                                    lgModel.CheckEmailVerification = 1;
                                    lgModel.isRedirect = 1;
                                    System.Web.HttpContext.Current.Session["model"] = null;
                                    System.Web.HttpContext.Current.Session["lgnProvider"] = "tw";
                                    generateloginidverification(lgModel, "");
                                    if (System.Web.HttpContext.Current.Session["view"] != null)
                                    {
                                        return RedirectToRoutePermanent(System.Web.HttpContext.Current.Session["view"].ToString().Trim());
                                    }
                                }
                            }
                            if (userdt.Rows.Count > 0)
                            {
                                // Update record and set twitter UID 
                                if (WEBLoginDatabaseCall.CheckTwitterID(model.Email.Trim(), model.Mobile.ToString().Trim(), TWUID)) // Record Updated successfully
                                {
                                    int uid = 0;
                                    string result = WEBLoginDatabaseCall.NewUser(TWUID.ToString(), "", screenname, model.Mobile, model.Email, "NewUser".ToString(), source, "Website", out uid);

                                    if (result.Trim() == Result.Success.ToString().Trim() || result.Trim() == "Already Exists" || result.Trim() == "Already Exists with other ID")
                                    {

                                        VCLoginName = !(string.IsNullOrEmpty(VCLoginName = userdt.Rows[0]["screenname"].ToString())) ? VCLoginName = userdt.Rows[0]["screenname"].ToString() : screenname;
                                        VCUserContentID = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userid"].ToString()) ? userdt.Rows[0]["userid"].ToString() : "0";

                                        if (string.IsNullOrWhiteSpace(userdt.Rows[0]["ismobileverified"].ToString()))
                                            VCIsMobileVerified = false;
                                        else
                                            VCIsMobileVerified = Convert.ToBoolean(userdt.Rows[0]["ismobileverified"].ToString());


                                        VCUserBusinessID = "0";
                                        VCUserType = !string.IsNullOrWhiteSpace(userdt.Rows[0]["usertype"].ToString()) ? userdt.Rows[0]["usertype"].ToString() : "0";
                                        VCUserEmail = !string.IsNullOrWhiteSpace(userdt.Rows[0]["loginemail"].ToString()) ? userdt.Rows[0]["loginemail"].ToString() : "";
                                        VCUserPhone = !string.IsNullOrWhiteSpace(userdt.Rows[0]["mobile"].ToString()) ? userdt.Rows[0]["mobile"].ToString() : "";
                                        VCUserStateid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["stateid"].ToString()) ? userdt.Rows[0]["stateid"].ToString() : "";
                                        VCUserCityid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityid"].ToString()) ? userdt.Rows[0]["cityid"].ToString() : "";
                                        VCUserState = !string.IsNullOrWhiteSpace(userdt.Rows[0]["statename"].ToString()) ? userdt.Rows[0]["statename"].ToString() : "";
                                        VCUserCity = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityname"].ToString()) ? userdt.Rows[0]["cityname"].ToString() : "";
                                        VCUserPhoto = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photo"].ToString().Trim()) ? userdt.Rows[0]["photo"].ToString().Trim() : "";
                                        VCUserPhotoMobile = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photomobile"].ToString().Trim()) ? userdt.Rows[0]["photomobile"].ToString().Trim() : "";
                                        VCUserUrl = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userUrl"].ToString().Trim()) ? userdt.Rows[0]["userUrl"].ToString().Trim() : "";

                                        UserSession.GenerateSession("twitter web", model.Email.Trim(), VCLoginName.Trim(), VCUserContentID.Trim(), VCUserType.Trim(), model.ProviderId, VCUserPhoto, VCUserUrl, VCUserEmail, VCUserPhone);

                                        userdt.Dispose();
                                        if (long.TryParse(VCUserType, out resIsInt) && long.TryParse(VCUserContentID, out resIsInt) && long.TryParse(VCUserBusinessID, out resIsInt))
                                        {
                                            if (System.Web.HttpContext.Current.Session["rturl"] != null && System.Web.HttpContext.Current.Session["rturl"].ToString().Trim() != "")
                                            {
                                                sourceCodeVal = ReturnURIValue(System.Web.HttpContext.Current.Session["rturl"].ToString());
                                                if (sourceCodeVal.ToString().Trim() != "" && sourceCodeVal >= 0)
                                                {
                                                    WEBLoginDatabaseCall.AddLoginUserLog("Twitter Web", VCLoginName, Convert.ToInt32(VCUserType), objUS.SV_VCLoginID.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), Convert.ToInt32(VCUserBusinessID), 1, reversedns, sourceCodeVal);
                                                }
                                                else
                                                {
                                                   
                                                    WEBLoginDatabaseCall.AddLoginUserLog("Twitter Web", VCLoginName, Convert.ToInt32(VCUserType), objUS.SV_VCLoginID.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), Convert.ToInt32(VCUserBusinessID), 1, reversedns, 0);

                                                }
                                            }
                                            else
                                            {
                                                WEBLoginDatabaseCall.AddLoginUserLog("Twitter Web", VCLoginName, Convert.ToInt32(VCUserType), objUS.SV_VCLoginID.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), Convert.ToInt32(VCUserBusinessID), 1, reversedns, 0);

                                            }
                                        }
                                    }
                                }

                            }

                        }
                        // var cookie = System.Web.HttpContext.Current.Request.Cookies["-VCUserName-"];
                        //ViewBag.MyCookie = cookie.Value;

                    }

                }

                if (!long.TryParse(VCUserContentID, out resIsInt))
                {
                    VCUserContentID = "0";
                }

                //twfollowers list 
                if (twFollowers != null && twFollowers.Count > 0)
                {
                    WEBLoginDatabaseCall.DBSaveTWUserFollowersList(twFollowers, Convert.ToInt32(VCUserContentID));
                    TempData["strTwiterFollowers"] = null;
                }

                if (model.rurl == null)
                {


                    if (System.Web.HttpContext.Current.Session["model"] != null)
                    {
                        LoginModel myModel = new LoginModel();
                        Type t = System.Web.HttpContext.Current.Session["model"].GetType();
                        if (t == typeof(LoginModel))
                        {
                            myModel = (LoginModel)System.Web.HttpContext.Current.Session["model"];
                            if (!string.IsNullOrWhiteSpace(myModel.BR))
                            {
                                myModel.UID = Int32.Parse(VCUserContentID);
                                if (long.TryParse(myModel.BID.ToString().Trim(), out resIsInt))
                                {
                                    WEBLoginDatabaseCall.BusinessUpdate(myModel.BID, myModel.UID, 60);
                                }
                            }
                            else
                            {
                                if (Request.QueryString["bid"] != null && Request.QueryString["bid"].ToString().Trim() != "" && Request.QueryString["br"] != null && Request.QueryString["bid"].ToString().Trim() != "")
                                {
                                    if (long.TryParse(Request.QueryString["bid"].ToString().Trim(), out resIsInt))
                                    {
                                        WEBLoginDatabaseCall.BusinessUpdate(long.Parse(Request.QueryString["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                                    }
                                }
                                else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
                                {
                                    if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim() != "" && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"].ToString().Trim() != "")
                                    {
                                        if (long.TryParse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim(), out resIsInt))
                                        {
                                            WEBLoginDatabaseCall.BusinessUpdate(long.Parse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (Request.QueryString["bid"] != null && Request.QueryString["bid"].ToString().Trim() != "" && Request.QueryString["br"] != null && Request.QueryString["bid"].ToString().Trim() != "")
                            {
                                if (long.TryParse(Request.QueryString["bid"].ToString().Trim(), out resIsInt))
                                {
                                    WEBLoginDatabaseCall.BusinessUpdate(long.Parse(Request.QueryString["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                                }
                            }
                            else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
                            {
                                if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim() != "" && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"].ToString().Trim() != "")
                                {
                                    if (long.TryParse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim(), out resIsInt))
                                    {
                                        WEBLoginDatabaseCall.BusinessUpdate(long.Parse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Request.QueryString["bid"] != null && Request.QueryString["bid"].ToString().Trim() != "" && Request.QueryString["br"] != null && Request.QueryString["bid"].ToString().Trim() != "")
                        {
                            if (long.TryParse(Request.QueryString["bid"].ToString().Trim(), out resIsInt))
                            {
                                WEBLoginDatabaseCall.BusinessUpdate(long.Parse(Request.QueryString["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                            }
                        }
                        else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
                        {
                            if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim() != "" && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"].ToString().Trim() != "")
                            {
                                if (long.TryParse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim(), out resIsInt))
                                {
                                    WEBLoginDatabaseCall.BusinessUpdate(long.Parse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                                }
                            }
                        }
                    }
                }

                if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                {
                    int indx = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().IndexOf("&");
                    if (indx > 0)
                        rturl = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Substring(0, indx);

                    rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("#_=_", "");

                    if (rturl.Contains("http://"))
                    {
                        indx = rturl.IndexOf("http");
                        if (indx > 0)
                            rturl = rturl.Trim().Substring(indx, rturl.Length - 1);

                        if (CheckUrlBeforeProcessing(rturl))
                            return RedirectPermanent(rturl);
                        else
                            return RedirectPermanent("/");
                    }
                    else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
                    {

                        if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"].ToString().Trim() != "")
                        {
                            rturl = System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"].ToString().Trim();
                            return RedirectPermanent(rturl);
                        }
                        else
                        {
                            return RedirectPermanent("/");
                        }
                    }
                    else
                    {
                        rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim();
                        return RedirectPermanent(rturl);
                    }

                }
                else
                {
                    return RedirectPermanent("/");
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                    return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                else
                    return RedirectPermanent("/login");
            }
        }
        //public ActionResult RegisterTwitterUser(RegisterExternalLoginModel model)
        //{
        //    try
        //    {

        //        TempData["UserAlert"] = "";
        //        string rturl = "";
        //        List<TwiterFollowers> twFollowers = new List<TwiterFollowers>();
        //        if (TempData["strTwiterFollowers"] != null)
        //        {
        //            twFollowers = (List<TwiterFollowers>)TempData.Peek("strTwiterFollowers");
        //            TempData.Keep("strTwiterFollowers");
        //        }

        //        string VCUserUrl = string.Empty;
        //        string VCUserPassword = string.Empty;
        //        string VCVerificationCode = string.Empty;
        //        string VCLoginPassword = string.Empty;
        //        string VCLoginName = string.Empty;
        //        string VCUserContentID = string.Empty;
        //        bool VCIsMobileVerified = false;
        //        string VCUserBusinessID = string.Empty;
        //        string VCUserType = string.Empty;
        //        string VCUserEmail = string.Empty;
        //        string VCUserPhone = string.Empty;
        //        string VCUserStateid = string.Empty;
        //        string VCUserCityid = string.Empty;
        //        string VCUserState = string.Empty;
        //        string VCUserCity = string.Empty;
        //        string VCUserPhoto = string.Empty;
        //        string VCUserPhotoMobile = string.Empty;
        //        reversedns = Utility.GetReversedns(Utility.GetIpAddress());
                 
        //        if (model.rurl == null)
        //        {
        //            System.Web.HttpContext.Current.Session["lgnProvider"] = "tw";
        //        }
        //        string source = "twitter";
        //        string TWUID = !string.IsNullOrWhiteSpace(model.ProviderId) ? model.ProviderId.ToString().Trim() : "";
        //        string screenname = !string.IsNullOrWhiteSpace(model.UserName.ToString()) ? model.UserName.ToString().Trim() : "";

        //        source = "twitter";

        //        string resTWID = WEBLoginDatabaseCall.CheckTwiiterUserID(TWUID, "twitter");
        //        string[] arrResTWID = resTWID.Split(',');
        //        if (arrResTWID[0].ToString() == "1")
        //        {
        //            DataTable userdt = WEBLoginDatabaseCall.GetUserInfo(arrResTWID[1].ToString().Trim());
        //            if (userdt.Rows.Count > 0 && !string.IsNullOrWhiteSpace(userdt.Rows[0]["isemailverified"].ToString()) && userdt.Rows[0]["isemailverified"].ToString().Trim() != "0" && userdt.Rows[0]["isemailverified"].ToString().Trim().ToLower() != "false" && !string.IsNullOrWhiteSpace(userdt.Rows[0]["isverified"].ToString()) && userdt.Rows[0]["isverified"].ToString().Trim() != "0" && userdt.Rows[0]["isverified"].ToString().Trim().ToLower() != "false")
        //            {
        //                VCLoginPassword = "";
        //                VCLoginName = !string.IsNullOrWhiteSpace(userdt.Rows[0]["screenname"].ToString()) ? userdt.Rows[0]["screenname"].ToString() : "";
        //                VCUserContentID = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userid"].ToString()) ? userdt.Rows[0]["userid"].ToString() : "0";

        //                if (string.IsNullOrWhiteSpace(userdt.Rows[0]["ismobileverified"].ToString()))
        //                    VCIsMobileVerified = false;
        //                else
        //                    VCIsMobileVerified = Convert.ToBoolean(userdt.Rows[0]["ismobileverified"].ToString());


        //                VCUserBusinessID = "0";
        //                VCUserType = !string.IsNullOrWhiteSpace(userdt.Rows[0]["usertype"].ToString()) ? userdt.Rows[0]["usertype"].ToString() : "0";
        //                VCUserEmail = !string.IsNullOrWhiteSpace(userdt.Rows[0]["loginemail"].ToString()) ? userdt.Rows[0]["loginemail"].ToString() : "";
        //                VCUserPhone = !string.IsNullOrWhiteSpace(userdt.Rows[0]["mobile"].ToString()) ? userdt.Rows[0]["mobile"].ToString() : "";
        //                VCUserStateid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["stateid"].ToString()) ? userdt.Rows[0]["stateid"].ToString() : "";
        //                VCUserCityid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityid"].ToString()) ? userdt.Rows[0]["cityid"].ToString() : "";
        //                VCUserState = !string.IsNullOrWhiteSpace(userdt.Rows[0]["statename"].ToString()) ? userdt.Rows[0]["statename"].ToString() : "";
        //                VCUserCity = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityname"].ToString()) ? userdt.Rows[0]["cityname"].ToString() : "";
        //                VCUserPhoto = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photo"].ToString().Trim()) ? userdt.Rows[0]["photo"].ToString().Trim() : "";
        //                VCUserPhotoMobile = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photomobile"].ToString().Trim()) ? userdt.Rows[0]["photomobile"].ToString().Trim() : "";
        //                VCUserUrl = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userUrl"].ToString().Trim()) ? userdt.Rows[0]["userUrl"].ToString().Trim() : "";
        //                //UserSession.GenerateSession("twitter", (!string.IsNullOrWhiteSpace(userdt.Rows[0]["loginemail"].ToString()) ? userdt.Rows[0]["loginemail"].ToString().Trim() : ""), VCLoginName.Trim(), VCUserContentID.Trim(), VCIsMobileVerified, VCUserType.Trim(), VCUserPhone.Trim(), VCUserEmail.Trim(), VCUserStateid.Trim(), VCUserCityid.Trim(), 0, model.ProviderId, "", 0, 0, 0, VCUserPhoto, VCUserPhotoMobile, VCUserUrl);
        //                UserSession.GenerateSession("twitter web", (!string.IsNullOrWhiteSpace(userdt.Rows[0]["loginemail"].ToString()) ? userdt.Rows[0]["loginemail"].ToString().Trim() : ""), VCLoginName.Trim(), VCUserContentID.Trim(), VCUserType.Trim(), model.ProviderId, VCUserPhoto, VCUserUrl, VCUserEmail, VCUserPhone);
        //                userdt.Dispose();
        //                if (long.TryParse(VCUserType, out resIsInt) && long.TryParse(VCUserContentID, out resIsInt) && long.TryParse(VCUserBusinessID, out resIsInt))
        //                {

        //                    if (System.Web.HttpContext.Current.Session["rturl"] != null && System.Web.HttpContext.Current.Session["rturl"].ToString().Trim() != "")
        //                    {
        //                        sourceCodeVal = ReturnURIValue(System.Web.HttpContext.Current.Session["rturl"].ToString());
        //                        if (sourceCodeVal.ToString().Trim() != "" && sourceCodeVal >= 0)
        //                        {  
        //                            WEBLoginDatabaseCall.AddLoginUserLog("Twitter Web", VCLoginName, Convert.ToInt32(VCUserType), objUS.SV_VCLoginID.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), Convert.ToInt32(VCUserBusinessID), 1, reversedns, sourceCodeVal);
        //                        }
        //                        else
        //                        {
        //                            WEBLoginDatabaseCall.AddLoginUserLog("Twitter Web", VCLoginName, Convert.ToInt32(VCUserType), objUS.SV_VCLoginID.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), Convert.ToInt32(VCUserBusinessID), 1, reversedns, 0);

        //                        }
        //                    }
        //                    else
        //                    {
        //                        WEBLoginDatabaseCall.AddLoginUserLog("Twitter Web", VCLoginName, Convert.ToInt32(VCUserType), objUS.SV_VCLoginID.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), Convert.ToInt32(VCUserBusinessID), 1, reversedns, 0);
        //                    }
        //                }


        //                if (!long.TryParse(VCUserContentID, out resIsInt))
        //                {
        //                    VCUserContentID = "0";
        //                }

        //                //twfollowers list 
        //                if (twFollowers != null && twFollowers.Count > 0)
        //                {
        //                    WEBLoginDatabaseCall.DBSaveTWUserFollowersList(twFollowers, Convert.ToInt32(VCUserContentID));
        //                    TempData["strTwiterFollowers"] = null;
        //                }

        //                if (model.rurl != null)
        //                {
        //                    rturl = "/" + model.rurl.ToString();
        //                    return RedirectPermanent(rturl);
        //                }

        //                if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
        //                {
        //                    int indx = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().IndexOf("&");
        //                    if (indx > 0)
        //                        rturl = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Substring(0, indx);

        //                    rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("#_=_", "");

        //                    if (rturl.Contains("http://"))
        //                    {
        //                        indx = rturl.IndexOf("http");
        //                        if (indx > 0)
        //                            rturl = rturl.Trim().Substring(indx, rturl.Length - 1);

        //                        if (CheckUrlBeforeProcessing(rturl))
        //                            return RedirectPermanent(rturl);
        //                        else
        //                            return RedirectPermanent("/");
        //                    }
        //                    else
        //                    {
        //                        rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim();
        //                        return RedirectPermanent(rturl);
        //                    }

        //                }
        //                else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
        //                {

        //                    if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"].ToString().Trim() != "")
        //                    {
        //                        rturl = System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"].ToString().Trim();
        //                        return RedirectPermanent(rturl);
        //                    }
        //                    else
        //                    {
        //                        return RedirectPermanent("/");
        //                    }
        //                }
                        
        //                else
        //                {
        //                    return RedirectPermanent("/");
        //                }


        //                //if (model.rurl != null)
        //                //{
        //                //    rturl = "/" + model.rurl.ToString();
        //                //    return Redirect(rturl);
        //                //}

        //                //if (System.Web.HttpContext.Current.Session["rturl"] != null)
        //                //{
        //                //    rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString();
        //                //    return Redirect(rturl);
        //                //}
        //                //else
        //                //{
        //                //    return Redirect("/");
        //                //}
        //            }
        //            else
        //            {

        //                LoginModel lgModel = new LoginModel();
        //                lgModel.Login = arrResTWID[1].ToString().Trim();
        //                lgModel.CheckEmailVerification = 1;
        //                lgModel.isRedirect = 1;
        //                System.Web.HttpContext.Current.Session["model"] = null;

        //                generateloginidverification(lgModel, "");
        //                return View("Verification");
        //            }
        //        }

        //        if ((string.IsNullOrEmpty(model.Email)) || (string.IsNullOrEmpty(TWUID)))
        //        {
        //            ViewBag.Provider = "Twitter";
        //            model.UserName = screenname;
        //            System.Web.HttpContext.Current.Session["ELmodel"] = model;
        //            return RedirectToRoute("externalloginconfirmation");
        //        }
        //        else
        //        {

        //            if (WEBLoginDatabaseCall.CheckEmailID(model.Email.ToString().Trim())) // save new entry
        //            {
        //                string password = WEBLoginDatabaseCall.Generate(6);
        //                int uid;

        //                string result = WEBLoginDatabaseCall.NewUser(TWUID.ToString(), password.ToString(), screenname, model.Mobile, model.Email, "NewUser".ToString(), source, "Website", out uid);

        //                if (result.Trim() == Result.Success.ToString().Trim() || result.Trim() == "Already Exists")                               
        //                {

        //                    DataTable userdt = WEBLoginDatabaseCall.GetUserInfo(model.Email.Trim(), TWUID, "twitter","");
        //                    if (userdt.Rows.Count > 0)
        //                    {
        //                        if (string.IsNullOrWhiteSpace(userdt.Rows[0]["isemailverified"].ToString()) || userdt.Rows[0]["isemailverified"].ToString().Trim() == "0" || userdt.Rows[0]["isemailverified"].ToString().Trim().ToLower() == "false")
        //                        {
        //                            LoginModel lgModel = new LoginModel();
        //                            lgModel.Login = model.Email.Trim();
        //                            lgModel.CheckEmailVerification = 1;
        //                            lgModel.isRedirect = 1;
        //                            System.Web.HttpContext.Current.Session["lgnProvider"] = "tw";
        //                            System.Web.HttpContext.Current.Session["model"] = null;
        //                            generateloginidverification(lgModel, "");
        //                            if (System.Web.HttpContext.Current.Session["view"] != null)
        //                            {
        //                                return RedirectToRoutePermanent(System.Web.HttpContext.Current.Session["view"].ToString().Trim());
        //                            }
        //                        }

        //                        VCLoginPassword = "";
        //                        VCLoginName = !string.IsNullOrWhiteSpace(userdt.Rows[0]["screenname"].ToString()) ? userdt.Rows[0]["screenname"].ToString() : "";
        //                        VCUserContentID = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userid"].ToString()) ? userdt.Rows[0]["userid"].ToString() : "0";

        //                        if (string.IsNullOrWhiteSpace(userdt.Rows[0]["ismobileverified"].ToString()))
        //                            VCIsMobileVerified = false;
        //                        else
        //                            VCIsMobileVerified = Convert.ToBoolean(userdt.Rows[0]["ismobileverified"].ToString());

        //                        VCUserBusinessID = "0";
        //                        VCUserType = !string.IsNullOrWhiteSpace(userdt.Rows[0]["usertype"].ToString()) ? userdt.Rows[0]["usertype"].ToString() : "0";
        //                        VCUserEmail = !string.IsNullOrWhiteSpace(userdt.Rows[0]["loginemail"].ToString()) ? userdt.Rows[0]["loginemail"].ToString() : "";
        //                        VCUserPhone = !string.IsNullOrWhiteSpace(userdt.Rows[0]["mobile"].ToString()) ? userdt.Rows[0]["mobile"].ToString() : "";
        //                        VCUserStateid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["stateid"].ToString()) ? userdt.Rows[0]["stateid"].ToString() : "";
        //                        VCUserCityid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityid"].ToString()) ? userdt.Rows[0]["cityid"].ToString() : "";
        //                        VCUserState = !string.IsNullOrWhiteSpace(userdt.Rows[0]["statename"].ToString()) ? userdt.Rows[0]["statename"].ToString() : "";
        //                        VCUserCity = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityname"].ToString()) ? userdt.Rows[0]["cityname"].ToString() : "";
        //                        VCUserPhoto = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photo"].ToString().Trim()) ? userdt.Rows[0]["photo"].ToString().Trim() : "";
        //                        VCUserPhotoMobile = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photomobile"].ToString().Trim()) ? userdt.Rows[0]["photomobile"].ToString().Trim() : "";
        //                        VCUserUrl = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userUrl"].ToString().Trim()) ? userdt.Rows[0]["userUrl"].ToString().Trim() : "";
        //                        //UserSession.GenerateSession("twitter", model.Email.Trim(), VCLoginName.Trim(), VCUserContentID.Trim(), VCIsMobileVerified, VCUserType.Trim(), VCUserPhone.Trim(), VCUserEmail.Trim(), VCUserStateid.Trim(), VCUserCityid.Trim(), 0, model.ProviderId, "", 0, 0, 0, VCUserPhoto, VCUserPhotoMobile, VCUserUrl);
        //                        UserSession.GenerateSession("twitter web", model.Email.Trim(), VCLoginName.Trim(), VCUserContentID.Trim(), VCUserType.Trim(), model.ProviderId, VCUserPhoto, VCUserUrl, VCUserEmail, VCUserPhone);
        //                        userdt.Dispose();

        //                        ViewBag.UserMsg = "Login ID already registered but email not verified. Please verify email by clicking on the link already been sent to (" + VCUserEmail.Trim() + ") a moment ago.";
        //                    }
        //                }
        //                else if (result.Trim() == "Already Exists with other ID")
        //                {
        //                    ViewBag.Provider = "Twitter";
        //                    model.UserName = screenname;
        //                    System.Web.HttpContext.Current.Session["view"] = "ExternalLoginConfirmation";
        //                    System.Web.HttpContext.Current.Session["ELmodel"] = model;
        //                    TempData["UserAlert"] = "<p style='color:red;'>This email address already associated with another twitter account.</p>";
        //                    TempData.Keep("UserAlert");
        //                    return RedirectToRoute("externalloginconfirmation");
        //                }
        //                var cookie = System.Web.HttpContext.Current.Request.Cookies["-VCUserName-"];
        //                ViewBag.MyCookie = cookie.Value;

        //            }
        //            else  // check if there Twitter ID Exists on that Email ID
        //            {
        //                if (WEBLoginDatabaseCall.IsEmailAssociatedWithOtherId(model.Email.Trim()))
        //                {
        //                    ViewBag.Provider = "Twitter";
        //                    model.UserName = screenname;
        //                    System.Web.HttpContext.Current.Session["view"] = "ExternalLoginConfirmation";
        //                    System.Web.HttpContext.Current.Session["ELmodel"] = model;
        //                    TempData["UserAlert"] = "<p style='color:red;'>This email address already associated with another twitter account.</p>";
        //                    TempData.Keep("UserAlert");
        //                    return RedirectToRoute("externalloginconfirmation");
        //                }
        //                else
        //                {
        //                    DataTable userdt = WEBLoginDatabaseCall.GetUserInfo(model.Email.Trim());
        //                    if (userdt.Rows.Count > 0)
        //                    {
        //                        if (string.IsNullOrWhiteSpace(userdt.Rows[0]["isemailverified"].ToString()) || userdt.Rows[0]["isemailverified"].ToString().Trim() == "0" || userdt.Rows[0]["isemailverified"].ToString().Trim().ToLower() == "false")
        //                        {
        //                            LoginModel lgModel = new LoginModel();
        //                            lgModel.Login = model.Email.Trim();
        //                            lgModel.CheckEmailVerification = 1;
        //                            lgModel.isRedirect = 1;
        //                            System.Web.HttpContext.Current.Session["model"] = null;
        //                            System.Web.HttpContext.Current.Session["lgnProvider"] = "tw";
        //                            generateloginidverification(lgModel, "");
        //                            if (System.Web.HttpContext.Current.Session["view"] != null)
        //                            {
        //                                return RedirectToRoutePermanent(System.Web.HttpContext.Current.Session["view"].ToString().Trim());
        //                            }
        //                        }
        //                    }
        //                    if (userdt.Rows.Count > 0)
        //                    {
        //                        // Update record and set twitter UID 
        //                        if (WEBLoginDatabaseCall.CheckTwitterID(model.Email.Trim(), model.Mobile.ToString().Trim(), TWUID)) // Record Updated successfully
        //                        {
        //                            int uid = 0;
        //                            string result = WEBLoginDatabaseCall.NewUser(TWUID.ToString(), "", screenname, model.Mobile, model.Email, "NewUser".ToString(), source, "Website", out uid);

        //                            if (result.Trim() == Result.Success.ToString().Trim() || result.Trim() == "Already Exists")
        //                            {

        //                                VCLoginName = !(string.IsNullOrEmpty(VCLoginName = userdt.Rows[0]["screenname"].ToString())) ? VCLoginName = userdt.Rows[0]["screenname"].ToString() : screenname;
        //                                VCUserContentID = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userid"].ToString()) ? userdt.Rows[0]["userid"].ToString() : "0";

        //                                if (string.IsNullOrWhiteSpace(userdt.Rows[0]["ismobileverified"].ToString()))
        //                                    VCIsMobileVerified = false;
        //                                else
        //                                    VCIsMobileVerified = Convert.ToBoolean(userdt.Rows[0]["ismobileverified"].ToString());


        //                                VCUserBusinessID = "0";
        //                                VCUserType = !string.IsNullOrWhiteSpace(userdt.Rows[0]["usertype"].ToString()) ? userdt.Rows[0]["usertype"].ToString() : "0";
        //                                VCUserEmail = !string.IsNullOrWhiteSpace(userdt.Rows[0]["loginemail"].ToString()) ? userdt.Rows[0]["loginemail"].ToString() : "";
        //                                VCUserPhone = !string.IsNullOrWhiteSpace(userdt.Rows[0]["mobile"].ToString()) ? userdt.Rows[0]["mobile"].ToString() : "";
        //                                VCUserStateid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["stateid"].ToString()) ? userdt.Rows[0]["stateid"].ToString() : "";
        //                                VCUserCityid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityid"].ToString()) ? userdt.Rows[0]["cityid"].ToString() : "";
        //                                VCUserState = !string.IsNullOrWhiteSpace(userdt.Rows[0]["statename"].ToString()) ? userdt.Rows[0]["statename"].ToString() : "";
        //                                VCUserCity = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityname"].ToString()) ? userdt.Rows[0]["cityname"].ToString() : "";
        //                                VCUserPhoto = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photo"].ToString().Trim()) ? userdt.Rows[0]["photo"].ToString().Trim() : "";
        //                                VCUserPhotoMobile = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photomobile"].ToString().Trim()) ? userdt.Rows[0]["photomobile"].ToString().Trim() : "";
        //                                VCUserUrl = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userUrl"].ToString().Trim()) ? userdt.Rows[0]["userUrl"].ToString().Trim() : "";
        //                                //UserSession.GenerateSession("twitter", model.Email.Trim(), VCLoginName.Trim(), VCUserContentID.Trim(), VCIsMobileVerified, VCUserType.Trim(), VCUserPhone.Trim(), VCUserEmail.Trim(), VCUserStateid.Trim(), VCUserCityid.Trim(), 0, model.ProviderId, "", 0, 0, 0, VCUserPhoto, VCUserPhotoMobile, VCUserUrl);
        //                                UserSession.GenerateSession("twitter web", model.Email.Trim(), VCLoginName.Trim(), VCUserContentID.Trim(), VCUserType.Trim(), model.ProviderId, VCUserPhoto, VCUserUrl, VCUserEmail, VCUserPhone);
        //                                userdt.Dispose();
        //                                if (long.TryParse(VCUserType, out resIsInt) && long.TryParse(VCUserContentID, out resIsInt) && long.TryParse(VCUserBusinessID, out resIsInt))
        //                                {
        //                                    if (System.Web.HttpContext.Current.Session["rturl"] != null && System.Web.HttpContext.Current.Session["rturl"].ToString().Trim() != "")
        //                                    {
        //                                        sourceCodeVal = ReturnURIValue(System.Web.HttpContext.Current.Session["rturl"].ToString());
        //                                        if (sourceCodeVal.ToString().Trim() != "" && sourceCodeVal >= 0)
        //                                        {
        //                                            WEBLoginDatabaseCall.AddLoginUserLog("Twitter Web", VCLoginName, Convert.ToInt32(VCUserType), objUS.SV_VCLoginID.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), Convert.ToInt32(VCUserBusinessID), 1, reversedns, sourceCodeVal);
        //                                        }
        //                                        else
        //                                        {
        //                                            WEBLoginDatabaseCall.AddLoginUserLog("Twitter Web", VCLoginName, Convert.ToInt32(VCUserType), objUS.SV_VCLoginID.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), Convert.ToInt32(VCUserBusinessID), 1, reversedns, 0);

        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        WEBLoginDatabaseCall.AddLoginUserLog("Twitter Web", VCLoginName, Convert.ToInt32(VCUserType), objUS.SV_VCLoginID.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), Convert.ToInt32(VCUserBusinessID), 1, reversedns, 0);

        //                                    }
        //                                }
        //                            }
        //                        }

        //                    }

        //                }
        //                // var cookie = System.Web.HttpContext.Current.Request.Cookies["-VCUserName-"];
        //                //ViewBag.MyCookie = cookie.Value;

        //            }

        //        }

        //        if (!long.TryParse(VCUserContentID, out resIsInt))
        //        {
        //            VCUserContentID = "0";
        //        }

        //        //twfollowers list 
        //        if (twFollowers != null && twFollowers.Count > 0)
        //        {
        //            WEBLoginDatabaseCall.DBSaveTWUserFollowersList(twFollowers, Convert.ToInt32(VCUserContentID));
        //            TempData["strTwiterFollowers"] = null;
        //        }

        //        if (model.rurl == null)
        //        {


        //            if (System.Web.HttpContext.Current.Session["model"] != null)
        //            {
        //                LoginModel myModel = new LoginModel();
        //                Type t = System.Web.HttpContext.Current.Session["model"].GetType();
        //                if (t == typeof(LoginModel))
        //                {
        //                    myModel = (LoginModel)System.Web.HttpContext.Current.Session["model"];
        //                    if (!string.IsNullOrWhiteSpace(myModel.BR))
        //                    {
        //                        myModel.UID = Int32.Parse(VCUserContentID);
        //                        if (long.TryParse(myModel.BID.ToString().Trim(), out resIsInt))
        //                        {
        //                            WEBLoginDatabaseCall.BusinessUpdate(myModel.BID, myModel.UID, 60);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (Request.QueryString["bid"] != null && Request.QueryString["bid"].ToString().Trim() != "" && Request.QueryString["br"] != null && Request.QueryString["bid"].ToString().Trim() != "")
        //                        {
        //                            if (long.TryParse(Request.QueryString["bid"].ToString().Trim(), out resIsInt))
        //                            {
        //                                WEBLoginDatabaseCall.BusinessUpdate(long.Parse(Request.QueryString["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
        //                            }
        //                        }
        //                        else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
        //                        {
        //                            if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim() != "" && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"].ToString().Trim() != "")
        //                            {
        //                                if (long.TryParse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim(), out resIsInt))
        //                                {
        //                                    WEBLoginDatabaseCall.BusinessUpdate(long.Parse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    if (Request.QueryString["bid"] != null && Request.QueryString["bid"].ToString().Trim() != "" && Request.QueryString["br"] != null && Request.QueryString["bid"].ToString().Trim() != "")
        //                    {
        //                        if (long.TryParse(Request.QueryString["bid"].ToString().Trim(), out resIsInt))
        //                        {
        //                            WEBLoginDatabaseCall.BusinessUpdate(long.Parse(Request.QueryString["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
        //                        }
        //                    }
        //                    else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
        //                    {
        //                        if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim() != "" && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"].ToString().Trim() != "")
        //                        {
        //                            if (long.TryParse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim(), out resIsInt))
        //                            {
        //                                WEBLoginDatabaseCall.BusinessUpdate(long.Parse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                if (Request.QueryString["bid"] != null && Request.QueryString["bid"].ToString().Trim() != "" && Request.QueryString["br"] != null && Request.QueryString["bid"].ToString().Trim() != "")
        //                {
        //                    if (long.TryParse(Request.QueryString["bid"].ToString().Trim(), out resIsInt))
        //                    {
        //                        WEBLoginDatabaseCall.BusinessUpdate(long.Parse(Request.QueryString["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
        //                    }
        //                }
        //                else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
        //                {
        //                    if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim() != "" && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"].ToString().Trim() != "")
        //                    {
        //                        if (long.TryParse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim(), out resIsInt))
        //                        {
        //                            WEBLoginDatabaseCall.BusinessUpdate(long.Parse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
        //                        }
        //                    }
        //                }
        //            }
        //        }

        //        if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
        //        {
        //            int indx = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().IndexOf("&");
        //            if (indx > 0)
        //                rturl = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Substring(0, indx);

        //            rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("#_=_", "");

        //            if (rturl.Contains("http://"))
        //            {
        //                indx = rturl.IndexOf("http");
        //                if (indx > 0)
        //                    rturl = rturl.Trim().Substring(indx, rturl.Length - 1);

        //                if (CheckUrlBeforeProcessing(rturl))
        //                    return RedirectPermanent(rturl);
        //                else
        //                    return RedirectPermanent("/");
        //            }
        //            else
        //            {
        //                rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim();
        //                return RedirectPermanent(rturl);
        //            }

        //        }
        //        else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
        //        {

        //            if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"].ToString().Trim() != "")
        //            {
        //                rturl = System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"].ToString().Trim();
        //                return RedirectPermanent(rturl);
        //            }
        //            else
        //            {
        //                return RedirectPermanent("/");
        //            }
        //        }
                        
        //        else
        //        {
        //            return RedirectPermanent("/");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.LogMe(ex);
        //        if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
        //            return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
        //        else
        //            return RedirectPermanent("/login"); 
        //    }
        //}

        [ValidateInput(false)]
        public ActionResult RegisterGoogleUser(Dictionary<string, string> userData)
        {
            try
            {
                string rturl = "";
                List<FetchContactsModel> googleUsers = new List<FetchContactsModel>();
                if (TempData["gousers"] != null)
                {
                    googleUsers = (List<FetchContactsModel>)TempData.Peek("gousers");
                    TempData.Keep("gousers");
                }

                string googleEmailAddress = string.Empty;
                string googlefullname = string.Empty;
                string googleID = string.Empty;
                string VCLoginPassword = string.Empty;
                string VCLoginName = string.Empty;
                string VCUserContentID = string.Empty;
                bool VCIsMobileVerified = false;
                string VCUserBusinessID = string.Empty;
                string VCUserType = string.Empty;
                string VCUserEmail = string.Empty;
                string VCUserPhone = string.Empty;
                string VCUserStateid = string.Empty;
                string VCUserCityid = string.Empty;
                string VCUserState = string.Empty;
                string VCUserCity = string.Empty;
                string VCUserPhoto = string.Empty;
                string VCUserPhotoMobile = string.Empty;
                string VCUserUrl = string.Empty;
                reversedns = Utility.GetReversedns(Utility.GetIpAddress());

                if (!userData.ContainsKey("rurl"))
                {
                    System.Web.HttpContext.Current.Session["lgnProvider"] = "go";
                }

                googleEmailAddress = (!(userData["email"] == null) && userData["email"].ToString() != "") ? userData["email"].ToString().Trim().Replace(" ", "+") : "";
                googlefullname = (!(userData["username"] == null) && userData["username"].ToString() != "") ? userData["username"].ToString() : "";
                int index = googlefullname.IndexOf("@");
                if (index > 0)
                    googlefullname = googlefullname.Substring(0, index);

                googleID = userData["id"].ToString();

                int flagResult = 0;
                if ((string.IsNullOrEmpty(googleEmailAddress)) || (string.IsNullOrEmpty(googleID)))
                {
                    flagResult = 0;
                    if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                        return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                    else
                        return RedirectPermanent("/login");
                }

                if (CheckGoogleID(googleEmailAddress, googleID) == Result.Success.ToString())
                {

                    DataTable userdt = WEBLoginDatabaseCall.GetUserInfo(googleEmailAddress, googleID, "google","");
                    if (userdt.Rows.Count > 0)
                    {
                        VCLoginPassword = !string.IsNullOrWhiteSpace(userdt.Rows[0]["password"].ToString()) ? userdt.Rows[0]["password"].ToString().Trim() : "";
                        VCLoginName = !string.IsNullOrWhiteSpace(userdt.Rows[0]["screenname"].ToString()) ? userdt.Rows[0]["screenname"].ToString().Trim() : "";
                        VCUserContentID = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userid"].ToString()) ? userdt.Rows[0]["userid"].ToString().Trim() : "";

                        if (string.IsNullOrWhiteSpace(userdt.Rows[0]["ismobileverified"].ToString()))
                            VCIsMobileVerified = false;
                        else
                            VCIsMobileVerified = Convert.ToBoolean(userdt.Rows[0]["ismobileverified"].ToString());

                        VCUserBusinessID = "0";
                        VCUserType = !string.IsNullOrWhiteSpace(userdt.Rows[0]["usertype"].ToString()) ? userdt.Rows[0]["usertype"].ToString() : "0";
                        VCUserEmail = !string.IsNullOrWhiteSpace(userdt.Rows[0]["loginemail"].ToString()) ? userdt.Rows[0]["loginemail"].ToString() : "";
                        VCUserPhone = !string.IsNullOrWhiteSpace(userdt.Rows[0]["mobile"].ToString()) ? userdt.Rows[0]["mobile"].ToString() : "";
                        VCUserStateid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["stateid"].ToString()) ? userdt.Rows[0]["stateid"].ToString() : "";
                        VCUserCityid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityid"].ToString()) ? userdt.Rows[0]["cityid"].ToString() : "";
                        VCUserState = !string.IsNullOrWhiteSpace(userdt.Rows[0]["statename"].ToString()) ? userdt.Rows[0]["statename"].ToString() : "";
                        VCUserCity = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityname"].ToString()) ? userdt.Rows[0]["cityname"].ToString() : "";
                        VCUserPhoto = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photo"].ToString().Trim()) ? userdt.Rows[0]["photo"].ToString().Trim() : "";
                        VCUserPhotoMobile = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photomobile"].ToString().Trim()) ? userdt.Rows[0]["photomobile"].ToString().Trim() : "";
                        VCUserUrl = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userUrl"].ToString().Trim()) ? userdt.Rows[0]["userUrl"].ToString().Trim() : "";
                        //UserSession.GenerateSession("Google Web", googleEmailAddress.Trim(), VCLoginName.Trim(), VCUserContentID.Trim(), VCIsMobileVerified, VCUserType.Trim(), VCUserPhone.Trim(), VCUserEmail.Trim(), VCUserStateid.Trim(), VCUserCityid.Trim(), 0, googleID, "", 0, 0, 0, VCUserPhoto, VCUserPhotoMobile, VCUserUrl);
                        UserSession.GenerateSession("Google Web", googleEmailAddress.Trim(), VCLoginName.Trim(), VCUserContentID.Trim(), VCUserType.Trim(), googleID, VCUserPhoto, VCUserUrl, VCUserEmail, VCUserPhone);
                        userdt.Dispose();
                        flagResult = 1;
                    }
                }
                else if ((string.IsNullOrEmpty(googleEmailAddress)) || (string.IsNullOrEmpty(googleID)))
                {
                    flagResult = 0;
                    if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                        return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                    else
                        return RedirectPermanent("/login"); 
                }
                else
                {
                    string password = WEBLoginDatabaseCall.Generate(6);
                    string result = WEBLoginDatabaseCall.NewGoogleUser(googleEmailAddress, googlefullname, googleID, VCUserCity, VCUserState, "", "", "", "", "", "", "", "", "Website");
                    if (result.Trim() == "Success" && !(Int32.Parse(VCUserContentID) == 0))
                    {
                        SendEmail("", "", "", "WelcomeAboard.html", 3, VCUserEmail.Trim(), "",
                                 Int32.Parse(VCUserContentID));
                    }

                    if (result.Trim() == Result.Success.ToString().Trim() || result.Trim() == "Already Exists")                               
                    {

                        DataTable userdt = WEBLoginDatabaseCall.GetUserInfo(googleEmailAddress);
                        if (userdt.Rows.Count > 0)
                        {
                            VCLoginPassword = !string.IsNullOrWhiteSpace(userdt.Rows[0]["password"].ToString()) ? userdt.Rows[0]["password"].ToString().Trim() : "";
                            VCLoginName = !string.IsNullOrWhiteSpace(userdt.Rows[0]["screenname"].ToString()) ? userdt.Rows[0]["screenname"].ToString().Trim() : "";
                            VCUserContentID = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userid"].ToString()) ? userdt.Rows[0]["userid"].ToString().Trim() : "";

                            if (string.IsNullOrWhiteSpace(userdt.Rows[0]["ismobileverified"].ToString()))
                                VCIsMobileVerified = false;
                            else
                                VCIsMobileVerified = Convert.ToBoolean(userdt.Rows[0]["ismobileverified"].ToString());

                            VCUserBusinessID = "0";
                            VCUserType = !string.IsNullOrWhiteSpace(userdt.Rows[0]["usertype"].ToString()) ? userdt.Rows[0]["usertype"].ToString() : "0";
                            VCUserEmail = !string.IsNullOrWhiteSpace(userdt.Rows[0]["loginemail"].ToString()) ? userdt.Rows[0]["loginemail"].ToString() : "";
                            VCUserPhone = !string.IsNullOrWhiteSpace(userdt.Rows[0]["mobile"].ToString()) ? userdt.Rows[0]["mobile"].ToString() : "";
                            VCUserStateid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["stateid"].ToString()) ? userdt.Rows[0]["stateid"].ToString() : "";
                            VCUserCityid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityid"].ToString()) ? userdt.Rows[0]["cityid"].ToString() : "";
                            VCUserState = !string.IsNullOrWhiteSpace(userdt.Rows[0]["statename"].ToString()) ? userdt.Rows[0]["statename"].ToString() : "";
                            VCUserCity = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityname"].ToString()) ? userdt.Rows[0]["cityname"].ToString() : "";
                            VCUserPhoto = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photo"].ToString().Trim()) ? userdt.Rows[0]["photo"].ToString().Trim() : "";
                            VCUserPhotoMobile = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photomobile"].ToString().Trim()) ? userdt.Rows[0]["photomobile"].ToString().Trim() : "";
                            VCUserUrl = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userUrl"].ToString().Trim()) ? userdt.Rows[0]["userUrl"].ToString().Trim() : "";
                            //UserSession.GenerateSession("Google Web", googleEmailAddress.Trim(), VCLoginName.Trim(), VCUserContentID.Trim(), VCIsMobileVerified, VCUserType.Trim(), VCUserPhone.Trim(), VCUserEmail.Trim(), VCUserStateid.Trim(), VCUserCityid.Trim(), 0, googleID, "", 0, 0, 0, VCUserPhoto, VCUserPhotoMobile, VCUserUrl);
                            UserSession.GenerateSession("Google Web", googleEmailAddress.Trim(), VCLoginName.Trim(), VCUserContentID.Trim(), VCUserType.Trim(), googleID, VCUserPhoto, VCUserUrl, VCUserEmail, VCUserPhone);
                            userdt.Dispose();
                            flagResult = 1;
                        }
                    }
                    else
                    {
                        flagResult = 0;
                        if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                            return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                        else
                            return RedirectPermanent("/login"); 
                    }
                }

                //googleContacts list 
                if (googleUsers != null && googleUsers.Count > 0)
                {
                    WEBLoginDatabaseCall.DBSaveGoogleContactsList(googleUsers, Convert.ToInt32(VCUserContentID));
                    TempData["gousers"] = null;
                }

                //Set user location to cookies

                if (long.TryParse(VCUserType, out resIsInt) && long.TryParse(VCUserContentID, out resIsInt) && long.TryParse(VCUserBusinessID, out resIsInt))
                {
                    if ( System.Web.HttpContext.Current.Session["rturl"] != null && System.Web.HttpContext.Current.Session["rturl"].ToString().Trim() != "")
                    {
                        sourceCodeVal = ReturnURIValue(System.Web.HttpContext.Current.Session["rturl"].ToString());
                        if (sourceCodeVal.ToString().Trim() != "" && sourceCodeVal >= 0)
                        {
                            WEBLoginDatabaseCall.AddLoginUserLog("Google Web", VCLoginName, Convert.ToInt32(VCUserType), objUS.SV_VCLoginID.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), Convert.ToInt32(VCUserBusinessID), 1, reversedns,sourceCodeVal);
                        }
                        else
                        {
                            WEBLoginDatabaseCall.AddLoginUserLog("Google Web", VCLoginName, Convert.ToInt32(VCUserType), objUS.SV_VCLoginID.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), Convert.ToInt32(VCUserBusinessID), 1, reversedns,0);
                        
                        }
                    }
                    else
                    {
                        WEBLoginDatabaseCall.AddLoginUserLog("Google Web", VCLoginName, Convert.ToInt32(VCUserType), objUS.SV_VCLoginID.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), Convert.ToInt32(VCUserBusinessID), 1, reversedns, 0);

                    }
                }
                //ViewBag.UserMsg = "Thank you ,You have successfully logged in...!";

                if (!long.TryParse(VCUserContentID, out resIsInt))
                {
                    VCUserContentID = "0";
                }

                if (!userData.ContainsKey("rurl"))
                {

                    if (System.Web.HttpContext.Current.Session["model"] != null)
                    {
                        LoginModel myModel = new LoginModel();
                        Type t = System.Web.HttpContext.Current.Session["model"].GetType();
                        if (t == typeof(LoginModel))
                        {
                            myModel = (LoginModel)System.Web.HttpContext.Current.Session["model"];
                            if (!string.IsNullOrWhiteSpace(myModel.BR))
                            {
                                myModel.UID = Int32.Parse(VCUserContentID);
                                if (long.TryParse(myModel.BID.ToString().Trim(), out resIsInt))
                                {
                                    WEBLoginDatabaseCall.BusinessUpdate(myModel.BID, myModel.UID, 60);
                                }
                            }
                            else
                            {
                                if (Request.QueryString["bid"] != null && Request.QueryString["bid"].ToString().Trim() != "" && Request.QueryString["br"] != null && Request.QueryString["bid"].ToString().Trim() != "")
                                {
                                    if (long.TryParse(Request.QueryString["bid"].ToString().Trim(), out resIsInt))
                                    {
                                        WEBLoginDatabaseCall.BusinessUpdate(long.Parse(Request.QueryString["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                                    }
                                }
                                else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
                                {
                                    if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim() != "" && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"].ToString().Trim() != "")
                                    {
                                        if (long.TryParse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim(), out resIsInt))
                                        {
                                            WEBLoginDatabaseCall.BusinessUpdate(long.Parse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (Request.QueryString["bid"] != null && Request.QueryString["bid"].ToString().Trim() != "" && Request.QueryString["br"] != null && Request.QueryString["bid"].ToString().Trim() != "")
                            {
                                if (long.TryParse(Request.QueryString["bid"].ToString().Trim(), out resIsInt))
                                {
                                    WEBLoginDatabaseCall.BusinessUpdate(long.Parse(Request.QueryString["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                                }
                            }
                            else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
                            {
                                if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim() != "" && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"].ToString().Trim() != "")
                                {
                                    if (long.TryParse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim(), out resIsInt))
                                    {
                                        WEBLoginDatabaseCall.BusinessUpdate(long.Parse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Request.QueryString["bid"] != null && Request.QueryString["bid"].ToString().Trim() != "" && Request.QueryString["br"] != null && Request.QueryString["bid"].ToString().Trim() != "")
                        {
                            if (long.TryParse(Request.QueryString["bid"].ToString().Trim(), out resIsInt))
                            {
                                WEBLoginDatabaseCall.BusinessUpdate(long.Parse(Request.QueryString["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                            }
                        }
                        else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
                        {
                            if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim() != "" && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"].ToString().Trim() != "")
                            {
                                if (long.TryParse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim(), out resIsInt))
                                {
                                    WEBLoginDatabaseCall.BusinessUpdate(long.Parse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                                }
                            }
                        }
                    }
                }


                
                if (userData.ContainsKey("rurl"))
                {

                    if (userData["rurl"] != null)
                    {
                        rturl = "/" + userData["rurl"].ToString();
                        return RedirectPermanent(rturl);
                    }
                }


                if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                {
                    int indx = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().IndexOf("&");
                    if (indx > 0)
                        rturl = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Substring(0, indx);

                    rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("#_=_", "");

                    if (rturl.Contains("http://"))
                    {
                        indx = rturl.IndexOf("http");
                        if (indx > 0)
                            rturl = rturl.Trim().Substring(indx, rturl.Length - 1);

                        if (CheckUrlBeforeProcessing(rturl))
                            return RedirectPermanent(rturl);
                        else
                            return RedirectPermanent("/");
                    }
                    else
                    {
                        rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim();
                        return RedirectPermanent(rturl);
                    }

                }
                else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
                {

                    if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"].ToString().Trim() != "")
                    {
                        rturl = System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"].ToString().Trim();
                        return RedirectPermanent(rturl);
                    }
                    else
                    {
                        return RedirectPermanent("/");
                    }
                }
                        
                else
                {
                    return RedirectPermanent("/");

                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                

                if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                    return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                else
                    return RedirectPermanent("/login"); 
            }
        }

        private string CheckGoogleID(string Emailid, string UID)
        {
            try
            {
                if (WEBLoginDatabaseCall.CheckGoogleID(UID)==true)
                {
                    return Result.Success.ToString();
                }
                else
                {
                    if (!string.IsNullOrEmpty(Emailid))
                    {
                        if (WEBLoginDatabaseCall.CheckGoogleID(Emailid))
                        {
                            return Result.Success.ToString();
                        }
                        else
                            return Result.Failure.ToString();
                    }
                    else
                        return Result.Failure.ToString();
                }
                
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                return "";
            }
        }

        [ValidateInput(false)]
        public ActionResult RegisterFBUser(RegisterExternalLoginModel model)
        {
            try
            {
                int resParse = 0;
                string rturl = "";
                List<FbUserFriendsList> fbusers = new List<FbUserFriendsList>();
                if (TempData["fbusers"] != null)
                {
                    fbusers = (List<FbUserFriendsList>)TempData.Peek("fbusers");
                    TempData.Keep("fbusers");
                }

                string FBemailAddress = string.Empty;
                string FBfullName = string.Empty;
                string FBID = string.Empty;
                string VCLoginPassword = string.Empty;
                string VCLoginName = string.Empty;
                string VCUserContentID = string.Empty;
                bool VCIsMobileVerified = false;
                string VCUserBusinessID = string.Empty;
                string VCUserType = string.Empty;
                string VCUserEmail = string.Empty;
                string VCUserPhone = string.Empty;
                string VCUserStateid = string.Empty;
                string VCUserCityid = string.Empty;
                string VCUserState = string.Empty;
                string VCUserCity = string.Empty;
                string VCUserPhoto = string.Empty;
                string VCUserPhotoMobile = string.Empty;
                string VCUserUrl = string.Empty;
                reversedns = Utility.GetReversedns(Utility.GetIpAddress());

                if (model.rurl == null)
                {
                    System.Web.HttpContext.Current.Session["lgnProvider"] = "fb";
                }

                if (model.Email != null && !string.IsNullOrEmpty(model.Email))
                {
                    FBemailAddress = model.Email.Replace(" ", "+");
                }
                else
                {
                    FBemailAddress = model.Email;
                }

                int flagResult = 0;


                FBfullName = model.UserName;
                FBID = model.ProviderId;

                if (CheckFacebookID(FBemailAddress, FBID) == Result.Success.ToString())
                {
                    DataTable userdt = new DataTable();
                    if (FBemailAddress != null && FBemailAddress.ToString().Trim() != "" && model.IsEmailNull == 0)
                    {
                        userdt = WEBLoginDatabaseCall.GetUserInfo(FBemailAddress.Trim(), FBID.ToString().Trim(), "facebook", model.Gender);
                    }
                    else
                    {
                        FBemailAddress = (string.IsNullOrWhiteSpace(FBemailAddress)) ? FBID : FBemailAddress;
                        userdt = WEBLoginDatabaseCall.GetUserInfoByProviderId(FBemailAddress.Trim(), FBID.ToString().Trim(), "facebook", model.Gender);
                        if (string.IsNullOrWhiteSpace(userdt.Rows[0]["isemailverified"].ToString()) || userdt.Rows[0]["isemailverified"].ToString().Trim() == "0" || userdt.Rows[0]["isemailverified"].ToString().Trim().ToLower() == "false")
                        {
                            LoginModel lgModel = new LoginModel();
                            lgModel.Login = userdt.Rows[0]["LoginEmail"].ToString().Trim();
                            lgModel.CheckEmailVerification = 1;
                            lgModel.isRedirect = 1;
                            System.Web.HttpContext.Current.Session["model"] = null;
                            System.Web.HttpContext.Current.Session["lgnProvider"] = "fb";
                            generateloginidverification(lgModel, "");
                            if (System.Web.HttpContext.Current.Session["view"] != null)
                            {
                                return View(System.Web.HttpContext.Current.Session["view"].ToString().Trim());
                            }
                        }

                    }

                    //if (string.IsNullOrWhiteSpace(userdt.Rows[0]["isemailverified"].ToString()) || userdt.Rows[0]["isemailverified"].ToString() == "0" || userdt.Rows[0]["isemailverified"].ToString().ToLower() == "false")
                    //{
                    //    LoginModel lgModel = new LoginModel();
                    //    lgModel.Login = model.Email.Trim();
                    //    lgModel.CheckEmailVerification = 1;
                    //    lgModel.isRedirect = 1;
                    //    System.Web.HttpContext.Current.Session["model"] = null;
                    //    generateloginidverification(lgModel);
                    //    if (System.Web.HttpContext.Current.Session["view"] != null)
                    //    {
                    //        return View(System.Web.HttpContext.Current.Session["view"].ToString());
                    //    }
                    //}

                    if (userdt.Rows.Count > 0)
                    {
                        string loginEmail = (userdt.Rows[0]["loginemail"] != null && !string.IsNullOrEmpty(userdt.Rows[0]["loginemail"].ToString().Trim())) ? userdt.Rows[0]["loginemail"].ToString() : "";
                        VCLoginPassword = (userdt.Rows[0]["password"] != null && !string.IsNullOrEmpty(userdt.Rows[0]["password"].ToString().Trim())) ? userdt.Rows[0]["password"].ToString() : "";
                        VCLoginName = !string.IsNullOrWhiteSpace(userdt.Rows[0]["screenname"].ToString()) ? userdt.Rows[0]["screenname"].ToString() : "";
                        VCUserContentID = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userid"].ToString()) ? userdt.Rows[0]["userid"].ToString() : "0";

                        if (string.IsNullOrWhiteSpace(userdt.Rows[0]["ismobileverified"].ToString()))
                            VCIsMobileVerified = false;
                        else
                            VCIsMobileVerified = Convert.ToBoolean(userdt.Rows[0]["ismobileverified"].ToString());

                        VCUserBusinessID = "0";
                        VCUserType = !string.IsNullOrWhiteSpace(userdt.Rows[0]["usertype"].ToString()) ? userdt.Rows[0]["usertype"].ToString() : "0";
                        VCUserEmail = !string.IsNullOrWhiteSpace(userdt.Rows[0]["loginemail"].ToString()) ? userdt.Rows[0]["loginemail"].ToString() : "";
                        VCUserPhone = !string.IsNullOrWhiteSpace(userdt.Rows[0]["mobile"].ToString()) ? userdt.Rows[0]["mobile"].ToString() : "";
                        VCUserStateid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["stateid"].ToString()) ? userdt.Rows[0]["stateid"].ToString() : "";
                        VCUserCityid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityid"].ToString()) ? userdt.Rows[0]["cityid"].ToString() : "";
                        VCUserState = !string.IsNullOrWhiteSpace(userdt.Rows[0]["statename"].ToString()) ? userdt.Rows[0]["statename"].ToString() : "";
                        VCUserCity = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityname"].ToString()) ? userdt.Rows[0]["cityname"].ToString() : "";
                        VCUserPhoto = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photo"].ToString().Trim()) ? userdt.Rows[0]["photo"].ToString().Trim() : "";
                        VCUserPhotoMobile = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photomobile"].ToString().Trim()) ? userdt.Rows[0]["photomobile"].ToString().Trim() : "";
                        VCUserUrl = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userUrl"].ToString().Trim()) ? userdt.Rows[0]["userUrl"].ToString().Trim() : "";

                        UserSession.GenerateSession("FB Web", loginEmail.Trim(), VCLoginName.Trim(), VCUserContentID.Trim(), VCUserType.Trim(), model.ProviderId, VCUserPhoto, VCUserUrl, VCUserEmail, VCUserPhone);
                        userdt.Dispose();
                        flagResult = 1;
                    }
                }
                else if ((string.IsNullOrEmpty(model.Email)) || (string.IsNullOrEmpty(FBID)))
                {
                    flagFBEmailIsVerified = 0;
                    ViewBag.Provider = "Facebook";
                    model.IsEmailNull = 1;
                    System.Web.HttpContext.Current.Session["ELmodel"] = model;
                    TempData["fbusers"] = fbusers;
                    TempData.Keep("fbusers");
                    return RedirectToRoute("externalloginconfirmation");
                }
                else
                {
                    string password = WEBLoginDatabaseCall.Generate(6);
                    if (model.IsEmailNull == 0)
                    {
                        flagFBEmailIsVerified = 1;
                    }
                    else
                    {
                        flagFBEmailIsVerified = 0;
                    }

                    string result = WEBLoginDatabaseCall.NewFacebookUser(FBemailAddress, FBfullName, FBID, VCUserCity, VCUserState, "", "", "", model.Gender, "", "", "", "", source, flagFBEmailIsVerified);
                    if (result.Trim() == "Success" && Int32.TryParse(VCUserContentID,out resParse))
                    {
                        if (Int32.TryParse(VCUserContentID, out resParse))
                        {
                            SendEmail("", "", "", "WelcomeAboard.html", 3, VCUserEmail, "",
                                     Int32.Parse(VCUserContentID));
                        }
                    }

                    if (result.Trim() == "Success" || result.Trim() == "Already Exists")
                    {
                        DataTable userdt = new DataTable();
                        if (flagFBEmailIsVerified == 0 || model.IsEmailNull == 1)
                        {
                            userdt = WEBLoginDatabaseCall.CheckEmailVerified(FBemailAddress);
                        }
                        else
                        {
                            userdt = WEBLoginDatabaseCall.GetUserInfo(FBemailAddress);
                        }
                        if (userdt.Rows.Count > 0)
                        {

                            if (string.IsNullOrWhiteSpace(userdt.Rows[0]["isemailverified"].ToString()) || userdt.Rows[0]["isemailverified"].ToString().Trim() == "0" || userdt.Rows[0]["isemailverified"].ToString().Trim().ToLower() == "false")
                            {
                                LoginModel lgModel = new LoginModel();
                                lgModel.Login = model.Email.Trim();
                                lgModel.CheckEmailVerification = 1;
                                lgModel.isRedirect = 1;
                                System.Web.HttpContext.Current.Session["model"] = null;
                                System.Web.HttpContext.Current.Session["lgnProvider"] = "fb";
                                generateloginidverification(lgModel, "");
                                if (System.Web.HttpContext.Current.Session["view"] != null)
                                {
                                    return View(System.Web.HttpContext.Current.Session["view"].ToString().Trim());
                                }
                            }


                            VCLoginPassword = (userdt.Rows[0]["password"] != null && !string.IsNullOrEmpty(userdt.Rows[0]["password"].ToString().Trim())) ? userdt.Rows[0]["password"].ToString() : "";
                            VCLoginName = !string.IsNullOrWhiteSpace(userdt.Rows[0]["screenname"].ToString()) ? userdt.Rows[0]["screenname"].ToString() : "";
                            VCUserContentID = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userid"].ToString()) ? userdt.Rows[0]["userid"].ToString() : "0";

                            if (string.IsNullOrWhiteSpace(userdt.Rows[0]["ismobileverified"].ToString()))
                                VCIsMobileVerified = false;
                            else
                                VCIsMobileVerified = Convert.ToBoolean(userdt.Rows[0]["ismobileverified"].ToString());

                            VCUserBusinessID = "0";
                            VCUserType = !string.IsNullOrWhiteSpace(userdt.Rows[0]["usertype"].ToString()) ? userdt.Rows[0]["usertype"].ToString() : "0";
                            VCUserEmail = !string.IsNullOrWhiteSpace(userdt.Rows[0]["loginemail"].ToString()) ? userdt.Rows[0]["loginemail"].ToString() : "";
                            VCUserPhone = !string.IsNullOrWhiteSpace(userdt.Rows[0]["mobile"].ToString()) ? userdt.Rows[0]["mobile"].ToString() : "";
                            VCUserStateid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["stateid"].ToString()) ? userdt.Rows[0]["stateid"].ToString() : "";
                            VCUserCityid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityid"].ToString()) ? userdt.Rows[0]["cityid"].ToString() : "";
                            VCUserState = !string.IsNullOrWhiteSpace(userdt.Rows[0]["statename"].ToString()) ? userdt.Rows[0]["statename"].ToString() : "";
                            VCUserCity = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityname"].ToString()) ? userdt.Rows[0]["cityname"].ToString() : "";
                            VCUserPhoto = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photo"].ToString().Trim()) ? userdt.Rows[0]["photo"].ToString().Trim() : "";
                            VCUserPhotoMobile = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photomobile"].ToString().Trim()) ? userdt.Rows[0]["photomobile"].ToString().Trim() : "";
                            VCUserUrl = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userUrl"].ToString().Trim()) ? userdt.Rows[0]["userUrl"].ToString().Trim() : "";

                            UserSession.GenerateSession("FB Web", FBemailAddress, VCLoginName, VCUserContentID, VCUserType, model.ProviderId, VCUserPhoto, VCUserUrl, VCUserEmail, VCUserPhone);
                            userdt.Dispose();
                            flagResult = 1;
                        }
                    }
                    else
                    {
                        flagResult = 0;
                        if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                            return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                        else
                            return RedirectPermanent("/login");
                    }
                }

                if (!long.TryParse(VCUserContentID, out resIsInt))
                {
                    VCUserContentID = "0";
                }

                //facebookfriends list 
                if (fbusers != null && fbusers.Count > 0)
                {
                    if(Int32.TryParse(VCUserContentID,out resParse))
                    {
                    WEBLoginDatabaseCall.DBSaveFBUserFriendList(fbusers, Convert.ToInt32(VCUserContentID));
                    TempData["fbusers"] = null;
                    }
                }

                if (long.TryParse(VCUserType, out resIsInt) && long.TryParse(VCUserContentID, out resIsInt) && long.TryParse(VCUserBusinessID, out resIsInt))
                {

                    if (System.Web.HttpContext.Current.Session["rturl"] != null && System.Web.HttpContext.Current.Session["rturl"].ToString().Trim() != "")
                    {
                        sourceCodeVal = ReturnURIValue(System.Web.HttpContext.Current.Session["rturl"].ToString());
                        if (sourceCodeVal.ToString().Trim() != "" && sourceCodeVal >= 0)
                        {
                            if (Int32.TryParse(VCUserContentID, out resParse) && Int32.TryParse(VCUserType, out resParse) && Int32.TryParse(VCUserBusinessID, out resParse))
                            {
                                WEBLoginDatabaseCall.AddLoginUserLog("FB Web", VCLoginName, Convert.ToInt32(VCUserType), objUS.SV_VCLoginID.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), Convert.ToInt32(VCUserBusinessID), 1, reversedns, sourceCodeVal);
                            }
                        }
                        else
                        {
                            if (Int32.TryParse(VCUserContentID, out resParse) && Int32.TryParse(VCUserType, out resParse) && Int32.TryParse(VCUserBusinessID, out resParse))
                            {
                                WEBLoginDatabaseCall.AddLoginUserLog("FB Web", VCLoginName, Convert.ToInt32(VCUserType), objUS.SV_VCLoginID.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), Convert.ToInt32(VCUserBusinessID), 1, reversedns, 0);
                            }
                        }
                    }
                    else
                    {
                        if (Int32.TryParse(VCUserContentID, out resParse) && Int32.TryParse(VCUserType, out resParse) && Int32.TryParse(VCUserBusinessID, out resParse))
                        {
                            WEBLoginDatabaseCall.AddLoginUserLog("FB Web", VCLoginName, Convert.ToInt32(VCUserType), objUS.SV_VCLoginID.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), Convert.ToInt32(VCUserBusinessID), 1, reversedns, 0);
                        }
                    }
                }


                if (model.rurl == null)
                {

                    if (System.Web.HttpContext.Current.Session["model"] != null)
                    {
                        LoginModel myModel = new LoginModel();
                        Type t = System.Web.HttpContext.Current.Session["model"].GetType();
                        if (t == typeof(LoginModel))
                        {
                            myModel = (LoginModel)System.Web.HttpContext.Current.Session["model"];
                            if (!string.IsNullOrWhiteSpace(myModel.BR))
                            {
                                if (Int32.TryParse(VCUserContentID, out resParse))
                                {
                                    myModel.UID = Int32.Parse(VCUserContentID);
                                    if (long.TryParse(myModel.BID.ToString().Trim(), out resIsInt))
                                    {
                                        WEBLoginDatabaseCall.BusinessUpdate(myModel.BID, myModel.UID, 60);
                                    }
                                }
                            }
                            else
                            {
                                if (Request.QueryString["bid"] != null && Request.QueryString["bid"].ToString().Trim() != "" && Request.QueryString["br"] != null && Request.QueryString["bid"].ToString().Trim() != "")
                                {
                                    if (long.TryParse(Request.QueryString["bid"].ToString().Trim(), out resIsInt))
                                    {
                                        WEBLoginDatabaseCall.BusinessUpdate(long.Parse(Request.QueryString["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                                    }
                                }
                                else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
                                {
                                    if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim() != "" && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"].ToString().Trim() != "")
                                    {
                                        if (long.TryParse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim(), out resIsInt))
                                        {
                                            WEBLoginDatabaseCall.BusinessUpdate(long.Parse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (Request.QueryString["bid"] != null && Request.QueryString["bid"].ToString().Trim() != "" && Request.QueryString["br"] != null && Request.QueryString["bid"].ToString().Trim() != "")
                            {
                                if (long.TryParse(Request.QueryString["bid"].ToString().Trim(), out resIsInt))
                                {
                                    WEBLoginDatabaseCall.BusinessUpdate(long.Parse(Request.QueryString["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                                }
                            }
                            else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
                            {
                                if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim() != "" && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"].ToString().Trim() != "")
                                {
                                    if (long.TryParse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim(), out resIsInt))
                                    {
                                        WEBLoginDatabaseCall.BusinessUpdate(long.Parse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Request.QueryString["bid"] != null && Request.QueryString["bid"].ToString().Trim() != "" && Request.QueryString["br"] != null && Request.QueryString["bid"].ToString().Trim() != "")
                        {
                            if (long.TryParse(Request.QueryString["bid"].ToString().Trim(), out resIsInt))
                            {
                                WEBLoginDatabaseCall.BusinessUpdate(long.Parse(Request.QueryString["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                            }
                        }
                        else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
                        {
                            if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim() != "" && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"].ToString().Trim() != "")
                            {
                                if (long.TryParse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim(), out resIsInt))
                                {
                                    WEBLoginDatabaseCall.BusinessUpdate(long.Parse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
                                }
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(objUS.SV_VCUserName) && objUS.SV_VCUserName.ToString().Trim() != "")
                {
                    MyGlobalVariables._checkLoggedin = 1;
                    Session.Add("_checkLoggedin", "1");
                }

                if (model.rurl != null)
                {
                    rturl = "/" + model.rurl.ToString();
                    return RedirectPermanent(rturl);
                }

                if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                {
                    int indx = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().IndexOf("&");
                    if (indx > 0)
                        rturl = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Substring(0, indx);

                    rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("#_=_", "");

                    if (rturl.Contains("http://"))
                    {
                        indx = rturl.IndexOf("http");
                        if (indx > 0)
                            rturl = rturl.Trim().Substring(indx, rturl.Length - 1);

                        if (CheckUrlBeforeProcessing(rturl))
                            return RedirectPermanent(rturl);
                        else
                            return RedirectPermanent("/");
                    }
                    else
                    {
                        rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim();
                        return RedirectPermanent(rturl);
                    }

                }
                else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
                {

                    if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"].ToString().Trim() != "")
                    {
                        rturl = System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"].ToString().Trim();
                        return RedirectPermanent(rturl);
                    }
                    else
                    {
                        return RedirectPermanent("/");
                    }
                }
                else
                {
                    return RedirectPermanent("/");
                }


            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                    return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
                else
                    return RedirectPermanent("/login");
            }

        }
        //public ActionResult RegisterFBUser(RegisterExternalLoginModel model)
        //{
        //    try
        //    {
        //        string rturl = "";
        //        List<FbUserFriendsList> fbusers=new List<FbUserFriendsList>();
        //        if (TempData["fbusers"] != null)
        //        {   
        //            fbusers = (List<FbUserFriendsList>)TempData.Peek("fbusers");
        //            TempData.Keep("fbusers");
        //        }
        //        string VCUserUrl = string.Empty;
        //        string FBemailAddress = string.Empty;
        //        string FBfullName = string.Empty;
        //        string FBID = string.Empty;
        //        string VCLoginPassword = string.Empty;
        //        string VCLoginName = string.Empty;
        //        string VCUserContentID = string.Empty;
        //        bool VCIsMobileVerified = false;
        //        string VCUserBusinessID = string.Empty;
        //        string VCUserType = string.Empty;
        //        string VCUserEmail = string.Empty;
        //        string VCUserPhone = string.Empty;
        //        string VCUserStateid = string.Empty;
        //        string VCUserCityid = string.Empty;
        //        string VCUserState = string.Empty;
        //        string VCUserCity = string.Empty;
        //        string VCUserPhoto = string.Empty;
        //        string VCUserPhotoMobile = string.Empty;
        //        reversedns = Utility.GetReversedns(Utility.GetIpAddress());

        //        if (model.rurl == null)
        //        {
        //            System.Web.HttpContext.Current.Session["lgnProvider"] = "fb";
        //        }

        //        if (model.Email != null && !string.IsNullOrEmpty(model.Email.ToString().Trim()))
        //        {
        //            FBemailAddress = model.Email.ToString().Trim().Replace(" ", "+");
        //        }
        //        else
        //        {
        //            FBemailAddress = model.Email;
        //        }

        //        int flagResult = 0;

               
        //        FBfullName = model.UserName;
        //        FBID = model.ProviderId;

        //        if (CheckFacebookID(FBemailAddress, FBID) == Result.Success.ToString())
        //        {
        //            DataTable userdt = new DataTable();
        //            if (FBemailAddress != null && FBemailAddress.ToString().Trim() != "" && model.IsEmailNull == 0)
        //            {
        //                userdt = WEBLoginDatabaseCall.GetUserInfo(FBemailAddress.Trim(), FBID.ToString().Trim(), "facebook", model.Gender);
        //            }
        //            else
        //            {
        //                FBemailAddress = (string.IsNullOrWhiteSpace(FBemailAddress)) ? FBID : FBemailAddress;
        //                userdt = WEBLoginDatabaseCall.GetUserInfoByProviderId(FBemailAddress.Trim(), FBID.ToString().Trim(), "facebook", model.Gender);
        //                if (string.IsNullOrWhiteSpace(userdt.Rows[0]["isemailverified"].ToString()) || userdt.Rows[0]["isemailverified"].ToString().Trim() == "0" || userdt.Rows[0]["isemailverified"].ToString().Trim().ToLower() == "false")
        //                {
        //                    LoginModel lgModel = new LoginModel();
        //                    lgModel.Login = userdt.Rows[0]["LoginEmail"].ToString().Trim();
        //                    lgModel.CheckEmailVerification = 1;
        //                    lgModel.isRedirect = 1;
        //                    System.Web.HttpContext.Current.Session["model"] = null;
        //                    System.Web.HttpContext.Current.Session["lgnProvider"] = "fb";
        //                    generateloginidverification(lgModel, "");
        //                    if (System.Web.HttpContext.Current.Session["view"] != null)
        //                    {
        //                        return View(System.Web.HttpContext.Current.Session["view"].ToString().Trim());
        //                    }
        //                }

        //            }

        //            //if (string.IsNullOrWhiteSpace(userdt.Rows[0]["isemailverified"].ToString()) || userdt.Rows[0]["isemailverified"].ToString() == "0" || userdt.Rows[0]["isemailverified"].ToString().ToLower() == "false")
        //            //{
        //            //    LoginModel lgModel = new LoginModel();
        //            //    lgModel.Login = model.Email.Trim();
        //            //    lgModel.CheckEmailVerification = 1;
        //            //    lgModel.isRedirect = 1;
        //            //    System.Web.HttpContext.Current.Session["model"] = null;
        //            //    generateloginidverification(lgModel);
        //            //    if (System.Web.HttpContext.Current.Session["view"] != null)
        //            //    {
        //            //        return View(System.Web.HttpContext.Current.Session["view"].ToString());
        //            //    }
        //            //}

        //            if (userdt.Rows.Count > 0)
        //            {
        //                string loginEmail = (userdt.Rows[0]["loginemail"] != null && !string.IsNullOrEmpty(userdt.Rows[0]["loginemail"].ToString().Trim())) ? userdt.Rows[0]["loginemail"].ToString() : "";
        //                VCLoginPassword = (userdt.Rows[0]["password"] != null && !string.IsNullOrEmpty(userdt.Rows[0]["password"].ToString().Trim())) ? userdt.Rows[0]["password"].ToString() : "";
        //                VCLoginName = !string.IsNullOrWhiteSpace(userdt.Rows[0]["screenname"].ToString()) ? userdt.Rows[0]["screenname"].ToString() : "";
        //                VCUserContentID = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userid"].ToString()) ? userdt.Rows[0]["userid"].ToString() : "0";

        //                if (string.IsNullOrWhiteSpace(userdt.Rows[0]["ismobileverified"].ToString()))
        //                    VCIsMobileVerified = false;
        //                else
        //                    VCIsMobileVerified = Convert.ToBoolean(userdt.Rows[0]["ismobileverified"].ToString());

        //                VCUserBusinessID = "0";
        //                VCUserType = !string.IsNullOrWhiteSpace(userdt.Rows[0]["usertype"].ToString()) ? userdt.Rows[0]["usertype"].ToString() : "0";
        //                VCUserEmail = !string.IsNullOrWhiteSpace(userdt.Rows[0]["loginemail"].ToString()) ? userdt.Rows[0]["loginemail"].ToString() : "";
        //                VCUserPhone = !string.IsNullOrWhiteSpace(userdt.Rows[0]["mobile"].ToString()) ? userdt.Rows[0]["mobile"].ToString() : "";
        //                VCUserStateid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["stateid"].ToString()) ? userdt.Rows[0]["stateid"].ToString() : "";
        //                VCUserCityid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityid"].ToString()) ? userdt.Rows[0]["cityid"].ToString() : "";
        //                VCUserState = !string.IsNullOrWhiteSpace(userdt.Rows[0]["statename"].ToString()) ? userdt.Rows[0]["statename"].ToString() : "";
        //                VCUserCity = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityname"].ToString()) ? userdt.Rows[0]["cityname"].ToString() : "";
        //                VCUserPhoto = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photo"].ToString().Trim()) ? userdt.Rows[0]["photo"].ToString().Trim() : "";
        //                VCUserPhotoMobile = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photomobile"].ToString().Trim()) ? userdt.Rows[0]["photomobile"].ToString().Trim() : "";
        //                VCUserUrl = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userUrl"].ToString().Trim()) ? userdt.Rows[0]["userUrl"].ToString().Trim() : "";
        //                //UserSession.GenerateSession("FB Web", loginEmail.Trim(), VCLoginName.Trim(), VCUserContentID.Trim(), VCIsMobileVerified, VCUserType.Trim(), VCUserPhone.Trim(), VCUserEmail.Trim(), VCUserStateid.Trim(), VCUserCityid.Trim(), 0, model.ProviderId, "", 0, 0, 0, VCUserPhoto, VCUserPhotoMobile, VCUserUrl);
        //                UserSession.GenerateSession("FB Web", loginEmail.Trim(), VCLoginName.Trim(), VCUserContentID.Trim(), VCUserType.Trim(), model.ProviderId, VCUserPhoto, VCUserUrl, VCUserEmail, VCUserPhone);
        //                userdt.Dispose();
        //                flagResult = 1;
        //            }
        //        }
        //        else if ((string.IsNullOrEmpty(model.Email)) || (string.IsNullOrEmpty(FBID)))
        //        {
        //            flagFBEmailIsVerified = 0;
        //            ViewBag.Provider = "Facebook";
        //            model.IsEmailNull = 1;
        //            System.Web.HttpContext.Current.Session["ELmodel"] = model;
        //            TempData["fbusers"] = fbusers;
        //            TempData.Keep("fbusers");
        //            return RedirectToRoute("externalloginconfirmation");
        //        }
        //        else
        //        {
        //            string password = WEBLoginDatabaseCall.Generate(6);
        //            if (model.IsEmailNull == 0)
        //            {
        //                flagFBEmailIsVerified = 1;
        //            }
        //            else
        //            {
        //                flagFBEmailIsVerified = 0;
        //            }

        //            string result = WEBLoginDatabaseCall.NewFacebookUser(FBemailAddress, FBfullName, FBID, VCUserCity, VCUserState, "", "", "", model.Gender, "", "", "", "", source, flagFBEmailIsVerified);
        //            if (result.Trim() == "Success" || result.Trim() == "Already Exists")
        //            {
        //                DataTable userdt = new DataTable();
        //                if (flagFBEmailIsVerified == 0 || model.IsEmailNull == 1)
        //                {
        //                    userdt = WEBLoginDatabaseCall.CheckEmailVerified(FBemailAddress);
        //                }
        //                else
        //                {
        //                    userdt = WEBLoginDatabaseCall.GetUserInfo(FBemailAddress);
        //                }
        //                if (userdt.Rows.Count > 0)
        //                {

        //                    if (string.IsNullOrWhiteSpace(userdt.Rows[0]["isemailverified"].ToString()) || userdt.Rows[0]["isemailverified"].ToString().Trim() == "0" || userdt.Rows[0]["isemailverified"].ToString().Trim().ToLower() == "false")
        //                    {
        //                        LoginModel lgModel = new LoginModel();
        //                        lgModel.Login = model.Email.Trim();
        //                        lgModel.CheckEmailVerification = 1;
        //                        lgModel.isRedirect = 1;
        //                        System.Web.HttpContext.Current.Session["model"] = null;
        //                        System.Web.HttpContext.Current.Session["lgnProvider"] = "fb";
        //                        generateloginidverification(lgModel, "");
        //                        if (System.Web.HttpContext.Current.Session["view"] != null)
        //                        {
        //                            return View(System.Web.HttpContext.Current.Session["view"].ToString().Trim());
        //                        }
        //                    }


        //                    VCLoginPassword = (userdt.Rows[0]["password"] != null && !string.IsNullOrEmpty(userdt.Rows[0]["password"].ToString().Trim())) ? userdt.Rows[0]["password"].ToString() : "";
        //                    VCLoginName = !string.IsNullOrWhiteSpace(userdt.Rows[0]["screenname"].ToString()) ? userdt.Rows[0]["screenname"].ToString() : "";
        //                    VCUserContentID = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userid"].ToString()) ? userdt.Rows[0]["userid"].ToString() : "0";

        //                    if (string.IsNullOrWhiteSpace(userdt.Rows[0]["ismobileverified"].ToString()))
        //                        VCIsMobileVerified = false;
        //                    else
        //                        VCIsMobileVerified = Convert.ToBoolean(userdt.Rows[0]["ismobileverified"].ToString());

        //                    VCUserBusinessID = "0";
        //                    VCUserType = !string.IsNullOrWhiteSpace(userdt.Rows[0]["usertype"].ToString()) ? userdt.Rows[0]["usertype"].ToString() : "0";
        //                    VCUserEmail = !string.IsNullOrWhiteSpace(userdt.Rows[0]["loginemail"].ToString()) ? userdt.Rows[0]["loginemail"].ToString() : "";
        //                    VCUserPhone = !string.IsNullOrWhiteSpace(userdt.Rows[0]["mobile"].ToString()) ? userdt.Rows[0]["mobile"].ToString() : "";
        //                    VCUserStateid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["stateid"].ToString()) ? userdt.Rows[0]["stateid"].ToString() : "";
        //                    VCUserCityid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityid"].ToString()) ? userdt.Rows[0]["cityid"].ToString() : "";
        //                    VCUserState = !string.IsNullOrWhiteSpace(userdt.Rows[0]["statename"].ToString()) ? userdt.Rows[0]["statename"].ToString() : "";
        //                    VCUserCity = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityname"].ToString()) ? userdt.Rows[0]["cityname"].ToString() : "";
        //                    VCUserPhoto = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photo"].ToString().Trim()) ? userdt.Rows[0]["photo"].ToString().Trim() : "";
        //                    VCUserPhotoMobile = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photomobile"].ToString().Trim()) ? userdt.Rows[0]["photomobile"].ToString().Trim() : "";
        //                    VCUserUrl = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userUrl"].ToString().Trim()) ? userdt.Rows[0]["userUrl"].ToString().Trim() : "";
        //                    //UserSession.GenerateSession("FB Web", FBemailAddress.Trim(), VCLoginName.Trim(), VCUserContentID.Trim(), VCIsMobileVerified, VCUserType.Trim(), VCUserPhone.Trim(), VCUserEmail.Trim(), VCUserStateid.Trim(), VCUserCityid.Trim(), 0, model.ProviderId, "", 0, 0, 0, VCUserPhoto, VCUserPhotoMobile, VCUserUrl);
        //                    UserSession.GenerateSession("FB Web", FBemailAddress.Trim(), VCLoginName.Trim(), VCUserContentID.Trim(), VCUserType.Trim(), model.ProviderId, VCUserPhoto, VCUserUrl, VCUserEmail, VCUserPhone);
        //                    userdt.Dispose();
        //                    flagResult = 1;
        //                }
        //            }
        //            else
        //            {
        //                flagResult = 0;
        //                if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
        //                    return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
        //                else
        //                    return RedirectPermanent("/login");
        //            }
        //        }

        //        if (!long.TryParse(VCUserContentID, out resIsInt))
        //        {
        //            VCUserContentID = "0";
        //        }

        //        //facebookfriends list 
        //        if (fbusers != null && fbusers.Count > 0)
        //        {
        //            WEBLoginDatabaseCall.DBSaveFBUserFriendList(fbusers, Convert.ToInt32(VCUserContentID));
        //            TempData["fbusers"]=null;
        //        }
           
        //        if (long.TryParse(VCUserType, out resIsInt) && long.TryParse(VCUserContentID, out resIsInt) && long.TryParse(VCUserBusinessID, out resIsInt))
        //        {

        //            if (System.Web.HttpContext.Current.Session["rturl"] != null && System.Web.HttpContext.Current.Session["rturl"].ToString().Trim() != "")
        //            {
        //                sourceCodeVal = ReturnURIValue(System.Web.HttpContext.Current.Session["rturl"].ToString());
        //                if (sourceCodeVal.ToString().Trim() != "" && sourceCodeVal >= 0)
        //                {   
        //                    WEBLoginDatabaseCall.AddLoginUserLog("FB Web", VCLoginName, Convert.ToInt32(VCUserType), objUS.SV_VCLoginID.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), Convert.ToInt32(VCUserBusinessID), 1, reversedns, sourceCodeVal);
        //                }
        //                else
        //                {
        //                    WEBLoginDatabaseCall.AddLoginUserLog("FB Web", VCLoginName, Convert.ToInt32(VCUserType), objUS.SV_VCLoginID.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), Convert.ToInt32(VCUserBusinessID), 1, reversedns, 0);

        //                }
        //            }
        //            else
        //            {
        //                WEBLoginDatabaseCall.AddLoginUserLog("FB Web", VCLoginName, Convert.ToInt32(VCUserType), objUS.SV_VCLoginID.ToString().Trim(), "", Convert.ToInt32(VCUserContentID), Convert.ToInt32(VCUserBusinessID), 1, reversedns, 0);

        //            }
        //        }

               
        //            if (model.rurl == null)
        //            {

        //                if (System.Web.HttpContext.Current.Session["model"] != null)
        //                {
        //                    LoginModel myModel = new LoginModel();
        //                    Type t = System.Web.HttpContext.Current.Session["model"].GetType();
        //                    if (t == typeof(LoginModel))
        //                    {
        //                        myModel = (LoginModel)System.Web.HttpContext.Current.Session["model"];
        //                        if (!string.IsNullOrWhiteSpace(myModel.BR))
        //                        {
        //                            myModel.UID = Int32.Parse(VCUserContentID);
        //                            if (long.TryParse(myModel.BID.ToString().Trim(), out resIsInt))
        //                            {
        //                                WEBLoginDatabaseCall.BusinessUpdate(myModel.BID, myModel.UID, 60);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            if (Request.QueryString["bid"] != null && Request.QueryString["bid"].ToString().Trim() != "" && Request.QueryString["br"] != null && Request.QueryString["bid"].ToString().Trim() != "")
        //                            {
        //                                if (long.TryParse(Request.QueryString["bid"].ToString().Trim(), out resIsInt))
        //                                {
        //                                    WEBLoginDatabaseCall.BusinessUpdate(long.Parse(Request.QueryString["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
        //                                }
        //                            }
        //                            else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
        //                            {
        //                                if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim() != "" && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"].ToString().Trim() != "")
        //                                {
        //                                    if (long.TryParse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim(), out resIsInt))
        //                                    {
        //                                        WEBLoginDatabaseCall.BusinessUpdate(long.Parse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (Request.QueryString["bid"] != null && Request.QueryString["bid"].ToString().Trim() != "" && Request.QueryString["br"] != null && Request.QueryString["bid"].ToString().Trim() != "")
        //                        {
        //                            if (long.TryParse(Request.QueryString["bid"].ToString().Trim(), out resIsInt))
        //                            {
        //                                WEBLoginDatabaseCall.BusinessUpdate(long.Parse(Request.QueryString["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
        //                            }
        //                        }
        //                        else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
        //                        {
        //                            if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim() != "" && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"].ToString().Trim() != "")
        //                            {
        //                                if (long.TryParse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim(), out resIsInt))
        //                                {
        //                                    WEBLoginDatabaseCall.BusinessUpdate(long.Parse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    if (Request.QueryString["bid"] != null && Request.QueryString["bid"].ToString().Trim() != "" && Request.QueryString["br"] != null && Request.QueryString["bid"].ToString().Trim() != "")
        //                    {
        //                        if (long.TryParse(Request.QueryString["bid"].ToString().Trim(), out resIsInt))
        //                        {
        //                            WEBLoginDatabaseCall.BusinessUpdate(long.Parse(Request.QueryString["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
        //                        }
        //                    }
        //                    else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
        //                    {
        //                        if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim() != "" && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["br"].ToString().Trim() != "")
        //                        {
        //                            if (long.TryParse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim(), out resIsInt))
        //                            {
        //                                WEBLoginDatabaseCall.BusinessUpdate(long.Parse(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["bid"].ToString().Trim()), long.Parse(VCUserContentID), 60);
        //                            }
        //                        }
        //                    }
        //                }
        //            }

        //            if (System.Web.HttpContext.Current.Request.Cookies["-VCUserName-"] != null && System.Web.HttpContext.Current.Request.Cookies["-VCUserName-"].ToString().Trim() != "")
        //            {
        //                MyGlobalVariables._checkLoggedin = 1;
        //            }

        //            if (model.rurl != null)
        //            {
        //                rturl = "/" + model.rurl.ToString();
        //                return RedirectPermanent(rturl);
        //            }

        //            if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
        //            {
        //                int indx = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().IndexOf("&");
        //                if (indx > 0)
        //                    rturl = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Substring(0, indx);

        //                rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("#_=_", "");

        //                if (rturl.Contains("http://"))
        //                {
        //                    indx = rturl.IndexOf("http");
        //                    if (indx > 0)
        //                        rturl = rturl.Trim().Substring(indx, rturl.Length - 1);

        //                    if (CheckUrlBeforeProcessing(rturl))
        //                        return RedirectPermanent(rturl);
        //                    else
        //                        return RedirectPermanent("/");
        //                }
        //                else
        //                {
        //                    rturl ="/"+ System.Web.HttpContext.Current.Session["rturl"].ToString().Trim();
        //                    return RedirectPermanent(rturl);
        //                }

        //            }
        //            else if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
        //            {

        //                if (System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"] != null && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"].ToString().Trim() != "")
        //                {
        //                    rturl = System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"].ToString().Trim();
        //                    return RedirectPermanent(rturl);
        //                }
        //                else
        //                {
        //                    return RedirectPermanent("/");
        //                }
        //            }
                        
        //            else
        //            {
        //                return RedirectPermanent("/");
        //            }
                    

        //    }
        //    catch (Exception ex)
        //    {
        //        log.LogMe(ex);
        //        if (System.Web.HttpContext.Current.Session["rturl"] != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
        //            return RedirectPermanent("/login?rturl=" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("&", "$"));
        //        else
        //            return RedirectPermanent("/login"); 
        //    }

        //}

        //public static bool CheckUrlBeforeProcessing(string rturl)
        //{
        //    Match match = Regex.Match(rturl, @"^http(s)?://([\w-]+.)+[\w-]+(/[\w- ./?%&=])?$", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //    if (match.Success)
        //        return true;
        //    else
        //        return false;
        //}
        public static bool CheckUrlBeforeProcessing(string rturl)
        {
            Match match = Regex.Match(rturl, @"(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.]*(\?\S+)?)?)?)", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            if (match.Success)
                return true;
            else
                return false;
        }

        public static string CheckFacebookID(string Emailid, string UID)
        {
            try
            {
                if (WEBLoginDatabaseCall.CheckFacebookID(UID) == true)
                {
                    return Result.Success.ToString();
                }
                else
                {
                    if (!string.IsNullOrEmpty(Emailid))
                    {
                        if (WEBLoginDatabaseCall.CheckFacebookID(Emailid))
                        {
                            return Result.Success.ToString();
                        }
                        else
                        {
                            return Result.Failure.ToString();
                        }
                    }
                    else
                        return Result.Failure.ToString();
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                return "";
            }
        }

        #endregion

        #region CommonFunctions/Actions

        private void AutoLogin(string NewUserID, string FirstName, string email, string Type)// To Auto login user after registration
        {
            try
            {
                if (Request.QueryString["LWREDIRECT"] != null)
                {
                    WEBLoginDatabaseCall.AddLeadWordRequest(objUS.SV_VCUserName, objUS.SV_VCLoginID, objUS.SV_VCUserContentID, webconfigWebsiteRootPath);

                    Response.Redirect(webconfigWebsiteRootPath + "thankyou/sendleadword/" + System.Web.HttpContext.Current.Session["OpenIDSearchKeyword"].ToString().Trim() + "/" + System.Web.HttpContext.Current.Session["OpenIDSearchLocation"].ToString().Trim() + "/Y", true);
                }
                else if (Request.QueryString["return"] != null)
                {
                    if (Request.QueryString["return"] == "smsc")
                    {
                        Response.RedirectPermanent("http://vconnectors.vconnect.com/send.aspx", false);
                    }
                    else if (Request.QueryString["return"] == "events")
                    {
                        Response.RedirectPermanent("http://events.vconnect.com", false);
                    }
                    else if (Request.QueryString["return"] == "offers")
                    {
                        Response.RedirectPermanent("http://offers.vconnect.com", false);
                    }

                    else
                    {
                        //Response.Redirect(Request.QueryString["return"].ToString().Trim().ToLower().Replace("~claimyourbusiness_xx", "/claimyourbusiness_xx").Replace("~viewallreviews", "/viewallreviews").Replace("~b", "_b").Replace("^", "/").Replace("_postreview", "_fbpostreview"), true);
                        Response.RedirectPermanent(Request.QueryString["return"].ToString().Trim().ToLower().Replace("~claimyourbusiness_xx", "/claimyourbusiness_xx").Replace("~viewallreviews", "/viewallreviews").Replace("~b", "_b").Replace("^", "/").Replace("_postreview", "_fbpostreview"), true);
                    }
                }
                else if (Request.QueryString["BizRatereturn"] != null)
                {
                    System.Web.HttpContext.Current.Session["RateBizReturnSuccess"] = "Y";
                    Response.RedirectPermanent(Request.QueryString["BizRatereturn"].ToString().Trim().ToLower().Replace("~b", "_b").Replace("^", "/").Replace("_postreview", "_fbpostreview"), true);
                }
                else
                {
                    //if (objUS.SV_VCUserbusinessID != null && objUS.SV_VCUserbusinessID != "" && objUS.SV_VCUserbusinessID != "0")
                    //{
                    //    Response.Redirect(webconfigWebsiteRootPath.ToString() + "myaccount", true);
                    //}
                    //else
                    //{
                    //    Response.Redirect(webconfigWebsiteRootPath.ToString() + "users/myaccount", true);
                    //}
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                
            }
        }

        //[ValidateInput(false)]
        public ActionResult LogOff()
        {
            try
            {
                System.Web.HttpContext.Current.Session["loginError"] = "";
                string userid = string.Empty;

                System.Web.HttpContext.Current.Session["rturl"] = null;
                if (!(Request.QueryString["rturl"] == null) && !string.IsNullOrEmpty(Request.QueryString["rturl"].ToString().Trim()))
                {
                    System.Web.HttpContext.Current.Session["rturl"] = Request.QueryString["rturl"].Trim().Replace("$", "&");
                }
                else if (!(Request.QueryString["src"] == null) && Request.QueryString["src"].ToString().Trim() != "")
                {
                    System.Web.HttpContext.Current.Session["rturl"] = CatchRTUrl(Request.QueryString["src"].ToString().Trim());
                }
               

                if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString().Trim() != "" && objUS.SV_VCLoginID.ToString().Trim() != "0")
                {
                    userid = objUS.SV_VCLoginID;
                    AddLogoutTime(userid, 2);
                    Utility.ClearCookies();
                    Utility.ClearProperties();
                }
                MyGlobalVariables._checkLoggedin = 0;
                Session.Add("_checkLoggedin", "0");
                System.Web.HttpContext.Current.Session["UserDoneLogout"] = "Y";

                if (!(System.Web.HttpContext.Current.Session["rturl"] == null) && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["rturl"].ToString().Trim()))
                {
                    int indx = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().IndexOf("&");
                    if (indx > 0)
                        rturl = System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Substring(0, indx);

                    rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim().Replace("#_=_", "");

                    if (rturl.Contains("http://"))
                    {
                        indx = rturl.IndexOf("http");
                        if (indx > 0)
                            rturl = rturl.Trim().Substring(indx, rturl.Length - 1);

                        if (CheckUrlBeforeProcessing(rturl))
                            return Redirect(rturl);
                        else
                            return Redirect("/");
                    }
                    else
                    {
                        if (!(System.Web.HttpContext.Current.Session["rturl"] == null) && System.Web.HttpContext.Current.Session["rturl"].ToString().Trim() != "")
                            rturl = "/" + System.Web.HttpContext.Current.Session["rturl"].ToString().Trim();
                        else
                            rturl = (rturl != null && rturl.Trim() != "") ? "/" + rturl : "/";

                        return RedirectPermanent(rturl);

                        //rturl ="/"+ System.Web.HttpContext.Current.Session["rturl"].ToString().Trim();
                        //return Redirect(rturl);
                    }

                }
                else if (!(System.Web.HttpContext.Current.Request.UrlReferrer == null) && Request.UrlReferrer.AbsoluteUri.ToString().Trim() != "")
                {

                    if (!(System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"] == null) && System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"].ToString().Trim() != "")
                    {
                        rturl = System.Web.HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["rturl"].ToString().Trim();
                        return RedirectPermanent(rturl);
                    }
                    else
                    {
                        return RedirectPermanent("/");
                    }
                }

                else
                {
                    return RedirectPermanent("/");
                }


                //if (!(System.Web.HttpContext.Current.Request.UrlReferrer == null) && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.UrlReferrer.ToString().Trim()))
                //{
                //    return RedirectPermanent(System.Web.HttpContext.Current.Request.UrlReferrer.ToString());
                //}
                //else
                //{
                //    return RedirectPermanent("/");
                //}

            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                if (System.Web.HttpContext.Current.Request.UrlReferrer != null && !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.UrlReferrer.ToString().Trim()))
                {
                    return RedirectPermanent(System.Web.HttpContext.Current.Request.UrlReferrer.ToString());
                }
                else
                {
                    return RedirectPermanent("/");
                }
            }
        }

        public void AddLogoutTime(string userid, int flag)
        {
            try
            {
                int result = WEBLoginDatabaseCall.DBAddLogoutTime(userid, flag);
            }
            catch (Exception ex) { }
        }

        #endregion

        #region ForgotPasswordActions

        [HttpGet]
        [ValidateInput(false)]
        public ActionResult ForgotPassword(ForgotPasswordModel model)
        {
            if (model.EmailId == null || model.EmailId.Trim() == "")
            {
                if (System.Web.HttpContext.Current.Session["FPmodel"] != null)
                {
                    Type t = System.Web.HttpContext.Current.Session["FPmodel"].GetType();
                    if (t == typeof(ForgotPasswordModel))
                    {
                        model = (ForgotPasswordModel)System.Web.HttpContext.Current.Session["FPmodel"];
                        Session.Remove("FPmodel");
                    }
                }

            }

            if (model.IsNotMobileVerified == null)
            {
                model.IsNotMobileVerified = 0;
            }

            if (TempData["msg"] != null || Session["msg"] != null)
            {
                TempData["msg"] = TempData["msg"].ToString();
                Session["msg"] = Session["msg"].ToString();
            }
            model.message = model.message;
            //if (string.IsNullOrEmpty(model.EmailVerificationCode))
            //    model.message = "test";
            //else
            //    model.message = "test1";
            return View(model);
            
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ForgotPassword(ForgotPasswordModel model, string optionalParam)
        {
            if (string.IsNullOrWhiteSpace(model.EmailId) || string.IsNullOrEmpty(model.EmailId.Trim()))
            {
                ViewBag.UserMsg = "<p style=\"color:red;\">Enter your login id</p>";
                System.Web.HttpContext.Current.Session["FPmodel"] = model;
                return RedirectToRoute("forgotpassword");
            }
            
            try
            {
                long result;
                int isMobileLogin=0;
                if(model.IsSucceed==null)
                    model.IsSucceed = 0;

                if (long.TryParse(model.EmailId.Trim(), out result))
                {
                    isMobileLogin = 1;
                    int vaildphone = Utility.isValidPhone(model.EmailId.Trim());
                    if (vaildphone == 0)
                    {
                        TempData["msg"] = "<p>Phone no is not valid.</p>";
                        Session["msg"] = "<p>Phone no is not valid.</p>";
                        System.Web.HttpContext.Current.Session["FPmodel"] = model;
                        return RedirectToRoute("forgotpassword");
                    }
                    else
                    {
                        ViewBag.UserMsg = "";
                    }
                }

                /***************************************************/

                 //WEBLoginDatabaseCall.DBGetActiveUserRecordsWithCount(model.EmailId.Trim());
                    

                /***************************************************/
                if (string.IsNullOrWhiteSpace(model.MobileVerificationCode))
                {
                    string randpass = WEBLoginDatabaseCall.Generate(6);
                    string vcode = WEBLoginDatabaseCall.Generate(4);
                    string Password1 = string.Empty;
                    string Name = string.Empty;
                    string isVerify = string.Empty;
                    //string res = WEBLoginDatabaseCall.DBForgotPass(model.EmailId.Trim(), randpass, WEBLoginDatabaseCall.encryptPassword(randpass), vcode);
                    string res = WEBLoginDatabaseCall.DBForgotPass(model.EmailId.Trim(), randpass, randpass, vcode);
                    string[] array = res.Split(',');
                    if (array.Length <= 0 || array.Length < 2)
                    {
                        System.Web.HttpContext.Current.Session["FPmodel"] = model;
                        return RedirectToRoute("forgotpassword");
                    }

                    Password1 = array[0];
                    Name = array[1];
                    isVerify = array[3];

                    if (Password1 == "-1")
                    {
                        TempData["loginError"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Login ID doesnot exists in our records.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                        model.status = "7th";
                    }
                    else if (isVerify == "0" && isMobileLogin == 1)
                    {
                        model.EmailVerificationCode = vcode;
                        model.status = "1st";
                        System.Web.HttpContext.Current.Session["FPmodel"] = model;
                        return RedirectToAction("generatenewemaillink",model);
                        //return RedirectToRoute("generatenewemaillink");
                    }
                    else if (isVerify == "0")
                    {
                        System.Web.HttpContext.Current.Session["lgnProvider"] = "vc";

                        DataTable dt = WEBLoginDatabaseCall.DBFetchUnverifiedUserDetials(model.EmailId);
                        if (dt.Rows.Count > 0)
                        {
                            string IPAddress = Utility.GetIpAddress();
                            StringBuilder sbEmailContent = new StringBuilder();
                            sbEmailContent.Append("<a href=" + ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + "verify?uid=" + (!string.IsNullOrWhiteSpace(dt.Rows[0]["userid"].ToString()) ? dt.Rows[0]["userid"].ToString().Trim() : "0") + "&code=" + vcode + "&p=" + System.Web.HttpContext.Current.Session["lgnProvider"].ToString().Trim() + ">Click Here</a>");
                            
                            string msgUrl = ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + "verify?uid=" + (!string.IsNullOrWhiteSpace(dt.Rows[0]["userid"].ToString())? dt.Rows[0]["userid"].ToString().Trim():"0") + "&code=" + vcode + "&p=" + System.Web.HttpContext.Current.Session["lgnProvider"].ToString().Trim();
                            string passwrd = (Password1 != null && !string.IsNullOrEmpty(Password1.Trim())) ? Password1 : "";
                            SendEmail(dt.Rows[0]["screenname"].ToString().Trim(), sbEmailContent.ToString(), msgUrl, "verification.htm", 1, model.EmailId.Trim(), passwrd, 0);
                           // SendEmail(dt.Rows[0]["screenname"].ToString().Trim(), sbEmailContent.ToString(), msgUrl, "verification.htm", 1, model.EmailId.Trim(), passwrd);

                            model.status = "2nd";
                            //Maintain the SMS EMAIL LOG
                            if (dt.Rows[0]["userid"] != null && dt.Rows[0]["userid"].ToString().Trim() != "")
                            {

                                Utility.SaveSMSEmailLog(Convert.ToInt32(dt.Rows[0]["userid"].ToString().Trim()), Usertype.OtherUser.GetHashCode(), model.EmailId.Trim(), "", "", MessageType.Email_Forgot.GetHashCode(), ConfigurationManager.AppSettings["Subjectforgotpass"].ToString(), sbEmailContent.ToString(), 0, "", "", "", IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.Email.GetHashCode(), reversedns, source);

                            }
                            else
                            {

                                Utility.SaveSMSEmailLog(0, Usertype.OtherUser.GetHashCode(), model.EmailId.Trim(), "", "", MessageType.Email_Forgot.GetHashCode(), ConfigurationManager.AppSettings["Subjectforgotpass"].ToString(), sbEmailContent.ToString(), 0, "", "", "", IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.Email.GetHashCode(), reversedns, source);

                            }

                            model.IsSucceed = 1;
                            model.message = "<p>A verification link has been sent to your email address. You need to verify your account via that link to gain access.</p><p>Didn't get the email? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" >Click here</a> to resend it.</p>";
                            TempData["msg"] = "<p>A verification link has been sent to your email address. You need to verify your account via that link to gain access.</p><p>Didn't get the email? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" >Click here</a> to resend it.</p>";
                            Session["msg"] = "<p>A verification link has been sent to your email address. You need to verify your account via that link to gain access.</p><p>Didn't get the email? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" >Click here</a> to resend it.</p>";
                            return View(model);
                        }
                    }
                    else if (Password1 == "0")
                    {
                        model.status = "3rd";
                        model.EmailVerificationCode = vcode;
                        System.Web.HttpContext.Current.Session["FPmodel"] = model;
                        //return RedirectToRoute("generatenewemaillink");
                        return RedirectToAction("generatenewemaillink", model);
                    }
                    else
                    {
                        model.status = "4th";
                        model.EmailVerificationCode = vcode;
                        System.Web.HttpContext.Current.Session["FPmodel"] = model;
                        //return RedirectToRoute("generatenewemaillink");
                        return RedirectToAction("generatenewemaillink", model);
                    }

                }
                else
                {
                    model.status = "5th";
                    System.Web.HttpContext.Current.Session["FPmodel"] = model;
                    //return RedirectToRoute("generatenewemaillink");
                    return RedirectToAction("generatenewemaillink", model);

                }

                model.status = "6th";
                System.Web.HttpContext.Current.Session["FPmodel"] = model;
                //return RedirectToRoute("forgotpassword");
                return RedirectToAction("forgotpassword", model);
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                System.Web.HttpContext.Current.Session["FPmodel"] = model;
                return RedirectToRoute("forgotpassword");
            }
        }

        [HttpGet]
        [ValidateInput(false)]
        public ActionResult Verify()
        {
            try
            {
                string VCUserUrl = string.Empty;
                string VCLoginPassword = string.Empty;
                string VCLoginName = string.Empty;
                string VCUserContentID = string.Empty;
                bool VCIsMobileVerified = false;
                string VCUserBusinessID = string.Empty;
                string VCUserType = string.Empty;
                string VCUserEmail = string.Empty;
                string VCUserPhone = string.Empty;
                string VCUserStateid = string.Empty;
                string VCUserCityid = string.Empty;
                string VCUserState = string.Empty;
                string VCUserCity = string.Empty;
                string VCisagent = string.Empty;
                string retrnValFlagEmail = string.Empty;
                string src = string.Empty;
                string VCUserPhoto = string.Empty;
                string VCUserPhotoMobile = string.Empty;


                VerifyModel model = new VerifyModel();

                if (Request.QueryString["code"]!=null && !string.IsNullOrWhiteSpace(Request.QueryString["code"].ToString()))
                {
                    model.code = !string.IsNullOrWhiteSpace(Request.QueryString["code"].ToString().Trim()) ? Request.QueryString["code"].ToString().Trim() : "";
                    model.userid = !string.IsNullOrWhiteSpace(Request.QueryString["uid"].ToString().Trim()) ? Int32.Parse(Request.QueryString["uid"].ToString().Trim()) : 0;
                    model.provider = !string.IsNullOrWhiteSpace(Request.QueryString["p"]) ? Request.QueryString["p"].ToString().Trim() : "vc";
                }

                string providerId = "";

                if (model.provider != null && model.provider.ToString().Trim() != "")
                {
                    if (model.provider.Trim().ToLower() == "tw")
                        src = "twitter";
                    else if (model.provider.Trim().ToLower() == "fb")
                        src = "facebook";
                    else if (model.provider.Trim().ToLower() == "go")
                        src = "google";
                    else
                        src = "";
                }
                else
                {
                    TempData["isSucceed"] = 0;
                    return View("verify");
                }


                if (model.userid.ToString().Trim()=="")
                {
                    TempData["isSucceed"] = 0;
                    return View("verify");
                }
                else
                {

                    string result = WEBLoginDatabaseCall.DBVerifyUser(model.userid, model.code);

                    if (result == "0")
                    {
                        DataTable userdt = WEBLoginDatabaseCall.GetUserInfo("", model.userid.ToString().Trim(), src,"");

                        if (model.provider != null && model.provider.ToString().Trim() != "")
                        {
                            if (model.provider.Trim().ToLower() == "t")
                                providerId = userdt.Rows[0]["twitteruid"].ToString();
                            else if (model.provider.Trim().ToLower() == "fb")
                                providerId = userdt.Rows[0]["googleid"].ToString();
                            else if (model.provider.Trim().ToLower() == "go")
                                providerId = userdt.Rows[0]["facebookuid"].ToString();
                            else
                                providerId = "";
                        }
                        else
                        {
                            TempData["isSucceed"] = 0;
                            return View("verify");
                        }

                        if (userdt.Rows.Count > 0)
                        {
                            VCLoginPassword = "";
                            VCLoginName = !string.IsNullOrWhiteSpace(userdt.Rows[0]["screenname"].ToString()) ? userdt.Rows[0]["screenname"].ToString() : "";
                            VCUserContentID = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userid"].ToString()) ? userdt.Rows[0]["userid"].ToString() : "0";

                            if (string.IsNullOrWhiteSpace(userdt.Rows[0]["ismobileverified"].ToString()))
                                VCIsMobileVerified = false;
                            else
                                VCIsMobileVerified = Convert.ToBoolean(userdt.Rows[0]["ismobileverified"].ToString());

                            VCUserBusinessID = "0";
                            VCUserType = !string.IsNullOrWhiteSpace(userdt.Rows[0]["usertype"].ToString()) ? userdt.Rows[0]["usertype"].ToString() : "0";
                            VCUserEmail = !string.IsNullOrWhiteSpace(userdt.Rows[0]["loginemail"].ToString()) ? userdt.Rows[0]["loginemail"].ToString() : "";
                            VCUserPhone = !string.IsNullOrWhiteSpace(userdt.Rows[0]["mobile"].ToString()) ? userdt.Rows[0]["mobile"].ToString() : "";
                            VCUserStateid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["stateid"].ToString()) ? userdt.Rows[0]["stateid"].ToString() : "";
                            VCUserCityid = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityid"].ToString()) ? userdt.Rows[0]["cityid"].ToString() : "";
                            VCUserState = !string.IsNullOrWhiteSpace(userdt.Rows[0]["statename"].ToString()) ? userdt.Rows[0]["statename"].ToString() : "";
                            VCUserCity = !string.IsNullOrWhiteSpace(userdt.Rows[0]["cityname"].ToString()) ? userdt.Rows[0]["cityname"].ToString() : "";
                            VCUserPhoto = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photo"].ToString().Trim()) ? userdt.Rows[0]["photo"].ToString().Trim() : "";
                            VCUserPhotoMobile = !string.IsNullOrWhiteSpace(userdt.Rows[0]["photomobile"].ToString().Trim()) ? userdt.Rows[0]["photomobile"].ToString().Trim() : "";
                            VCUserUrl = !string.IsNullOrWhiteSpace(userdt.Rows[0]["userUrl"].ToString().Trim()) ? userdt.Rows[0]["userUrl"].ToString().Trim() : "";
                            //UserSession.GenerateSession(source, VCUserEmail.Trim(), VCLoginName.Trim(), VCUserContentID.Trim(), VCIsMobileVerified, VCUserType.Trim(), VCUserPhone.Trim(), VCUserEmail.Trim(), VCUserStateid.Trim(), VCUserCityid.Trim(), 0, providerId, "", 0, 0, 0, VCUserPhoto, VCUserPhotoMobile, VCUserUrl);
                            UserSession.GenerateSession(source, VCUserEmail.Trim(), VCLoginName.Trim(), VCUserContentID.Trim(), VCUserType.Trim(), providerId, VCUserPhoto, VCUserUrl, VCUserEmail, VCUserPhone);
                            userdt.Dispose();
                            return RedirectPermanent("/");
                        }
                        else
                        {

                            TempData["userid"] = model.userid.ToString().Trim();
                            if (TempData["isSucceed"] == null)
                            {
                                TempData["isSucceed"] = 0;
                            }
                            return View("verify");
                        }

                    }
                    else
                    { 
                        if (TempData["isSucceed"] == null)
                        {
                            TempData["isSucceed"] = 0;
                        }
                        TempData["userid"] = model.userid.ToString().Trim();
                        return View("verify");
                    }
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                return View("verify");
            }
        }

        [ValidateInput(false)]
        public ActionResult NewPassword(string uid, string code, int cnt = 0)
        {
            NewPasswordModel model = new NewPasswordModel();
            try
            {

                if (!string.IsNullOrWhiteSpace(uid) && !string.IsNullOrWhiteSpace(code))
                {
                    model.userId = uid;
                    model.VerificationCode = code;
                    System.Web.HttpContext.Current.Session["NPmodel"] = model;
                }
                else
                {
                    if (System.Web.HttpContext.Current.Session["NPmodel"] != null)
                    {
                        model = (NewPasswordModel)System.Web.HttpContext.Current.Session["NPmodel"];
                    }
                }

                if (model.userId != null && model.userId.ToString().Trim()!="" && model.VerificationCode != null && model.VerificationCode.ToString().Trim()!="")
                {
                    int retVal = WEBLoginDatabaseCall.DBCheckValidVerificationLink(model.userId, model.VerificationCode);
                    if (retVal.ToString() == "1" || model.IsSucceed == 1)
                        TempData["IsValidLink"] = 1;
                    else
                    {
                        TempData["IsValidLink"] = 0;
                    }
                }
                else
                {
                    TempData["IsValidLink"] = 0;
                }
                return View(model);
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                return View(model);

            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult NewPassword(NewPasswordModel model)
        {
            if (string.IsNullOrEmpty(model.NewPassword) || string.IsNullOrWhiteSpace(model.NewPassword) || model.NewPassword.Trim() == "Enter your new password")
            {
                TempData["UserMsg"] = "<p style=\"color:red;\">Enter your new password</p>";
                System.Web.HttpContext.Current.Session["NPmodel"] = model;
                return RedirectToRoute("newpassword");
            }
            if (string.IsNullOrEmpty(model.ConfirmPassword) || string.IsNullOrWhiteSpace(model.ConfirmPassword) || model.ConfirmPassword.Trim() == "Confirm new password")
            {
                TempData["UserMsg"] = "<p style=\"color:red;\">Enter Confirm password</p>";
                System.Web.HttpContext.Current.Session["NPmodel"] = model;
                return RedirectToRoute("newpassword");
            }
            if (model.ConfirmPassword != model.ConfirmPassword)
            {
                TempData["UserMsg"] = "<p style=\"color:red;\">Password did not match</p>";
                System.Web.HttpContext.Current.Session["NPmodel"] = model;
                return RedirectToRoute("newpassword");
            }
            if (model.NewPassword.Length < 6 || model.ConfirmPassword.Length < 6)
            {
                TempData["UserMsg"] = "<p style=\"color:red;\">Password should be at least 6 characters in length</p>";
                System.Web.HttpContext.Current.Session["NPmodel"] = model;
                return RedirectToRoute("newpassword");
            }

            try
            {
                string vcode = WEBLoginDatabaseCall.Generate(4);
                if (model.userId != null && model.userId.ToString().Trim() != "" && model.NewPassword != null && model.NewPassword.ToString().Trim() != "")
                {
                    string result = WEBLoginDatabaseCall.UpdatePassword(model.userId, model.NewPassword, vcode);
                    TempData["IsValidLink"] = 1;

                    if (result == "Success")
                    {
                        TempData["UserMsg"] = "<p>Your password has been successfully reset. Go to the <a href=\"/login\">login page</a>.</p>";
                        model.IsSucceed = 1;
                        System.Web.HttpContext.Current.Session["NPmodel"] = model;
                        return RedirectToRoute("newpassword");

                    }
                    else
                    {
                        TempData["UserMsg"] = result;
                        model.IsSucceed = 0;
                    }
                }

                System.Web.HttpContext.Current.Session["NPmodel"] = model;
                return RedirectToRoute("newpassword");
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                System.Web.HttpContext.Current.Session["NPmodel"] = model;
                return RedirectToRoute("newpassword");
            }
        }

        [ValidateInput(false)]
        public ActionResult generatenewemaillink(ForgotPasswordModel model)
        {
            try
            {
                if (model.EmailId == null || model.EmailId.Trim() == "")
                {
                    if (System.Web.HttpContext.Current.Session["FPmodel"] != null)
                    {
                        Type t = System.Web.HttpContext.Current.Session["FPmodel"].GetType();
                        if (t == typeof(ForgotPasswordModel))
                        {
                            model = (ForgotPasswordModel)System.Web.HttpContext.Current.Session["FPmodel"];
                        }
                    }
                }

                if (Request.QueryString["cnt"] != null && Request.QueryString["cnt"].ToString() != "")
                {
                    model.IsSucceed = Int32.Parse(Request.QueryString["cnt"].ToString().Trim());
                }

                model.IsResend = 0;
                DataTable dt;
                string randCode = WEBLoginDatabaseCall.Generate(4);
                if (model.EmailId == null || model.EmailId.ToString() == "")
                {
                    model.status = model.status +"8th";
                    if (Request.QueryString["uid"] != null && Request.QueryString["uid"].ToString() != "")
                    {
                        model.IsResend = 1;
                        model.EmailVerificationCode = Request.QueryString["code"] != null ? Request.QueryString["code"].ToString() : "";
                        model.userId = Request.QueryString["uid"] != null ? Request.QueryString["uid"].ToString() : "0";
                    }

                    if (model.userId != null && !string.IsNullOrEmpty(model.userId.ToString().Trim()))
                    {
                        dt = WEBLoginDatabaseCall.GetUserInfo("", model.userId.Trim(), "","");
                    }
                    else
                    {
                        if (Request.QueryString["code"] != null)
                        {
                            model.EmailVerificationCode = Request.QueryString["code"] != null ? Request.QueryString["code"].ToString() : "";
                            return RedirectToRoute("newpassword", new { uid = model.userId, code = model.EmailVerificationCode, cnt = model.IsSucceed });
                        }
                        else
                        {
                            System.Web.HttpContext.Current.Session["FPmodel"] = model;
                            return RedirectToRoute("forgotpassword");
                        }

                    }


                    if (dt.Rows.Count > 0)
                    {
                        model.EmailId =((dt.Rows[0]["loginid"]!=null) && !string.IsNullOrEmpty(dt.Rows[0]["loginid"].ToString().Trim())) ? dt.Rows[0]["loginid"].ToString().Trim():"";
                    }
                    else
                    {
                        model.EmailId = "";
                    }
                }
                else
                {
                    model.status = model.status + "9th";
                    dt = WEBLoginDatabaseCall.GetUserInfo(model.EmailId.Trim());
                }
                
                if (dt.Rows.Count > 0)
                {
                    model.status = model.status + "10th";
                    if (model.EmailId == null || string.IsNullOrEmpty(model.EmailId.ToString().Trim()))
                    {
                        if (Request.QueryString["code"] != null)
                        {
                            model.EmailVerificationCode = Request.QueryString["code"] != null ? Request.QueryString["code"].ToString() : "";
                            return RedirectToRoute("newpassword", new { uid = model.userId, code = model.EmailVerificationCode, cnt = model.IsSucceed });
                        }
                        else
                        {
                            System.Web.HttpContext.Current.Session["FPmodel"] = model;
                            return RedirectToRoute("forgotpassword");
                        }
                    }

                    string IPAddress = Utility.GetIpAddress();

                    //Validate if user entered EMAIL as LoginID, then send Email else SMS
                    if (new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,6}$").IsMatch(model.EmailId.Trim()))
                    {
                        model.status = model.status + "11th";
                        if (model.IsResend == 1)
                            WEBLoginDatabaseCall.UpdateNewVerificationCode(model.EmailId.Trim(), randCode);
                        else
                            WEBLoginDatabaseCall.UpdateNewVerificationCode(model.EmailId.Trim(), model.EmailVerificationCode.Trim());

                        model.IsNotMobileVerified = 0;

                        StringBuilder sbEmailContent = new StringBuilder();

                        if (model.IsResend == 1)
                            //sbEmailContent.Append("Thank you for visiting VConnect. Please click on the link below to reset your password. Password Reset Link: <a href=" + webconfigWebsiteRootPath + "newpassword?uid=" + dt.Rows[0]["ContentID"].ToString().Trim() + "&code=" + randCode + ">Click Here</a>");
                            sbEmailContent.Append( webconfigWebsiteRootPath + "newpassword?uid=" + dt.Rows[0]["ContentID"].ToString().Trim() + "&code=" + randCode);
                        else
                            //sbEmailContent.Append("Thank you for visiting VConnect. Please click on the link below to reset your password. Password Reset Link: <a href=" + webconfigWebsiteRootPath + "newpassword?uid=" + dt.Rows[0]["ContentID"].ToString().Trim() + "&code=" + model.EmailVerificationCode + ">Click Here</a>");
                            sbEmailContent.Append( webconfigWebsiteRootPath + "newpassword?uid=" + dt.Rows[0]["ContentID"].ToString().Trim() + "&code=" + model.EmailVerificationCode);
                        SendEmail((!string.IsNullOrWhiteSpace(dt.Rows[0]["screenname"].ToString()) ? dt.Rows[0]["screenname"].ToString().Trim() : ""), sbEmailContent.ToString(), "", "forgotpass.html", 2, model.EmailId.Trim(), "", 0);

                      //  SendEmail((!string.IsNullOrWhiteSpace(dt.Rows[0]["screenname"].ToString())?dt.Rows[0]["screenname"].ToString().Trim():""), sbEmailContent.ToString(),"", "forgotpass.html", 2, model.EmailId.Trim(), "");

                        model.status = model.status + "12th";
                        //Maintain the SMS EMAIL LOG
                        if (model.IsSucceed == 0)
                        {
                            model.status = model.status + "13th";
                            model.message = "A password reset link has been sent to <a href=\"mailto:" + model.EmailId.Trim() + "\">" + model.EmailId.Trim() + "</a>.Please check your mail and follow the link to complete the process. </br> Did not receive the link? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend the link.";
                            TempData["msg"] = "A password reset link has been sent to <a href=\"mailto:" + model.EmailId.Trim() + "\">" + model.EmailId.Trim() + "</a>.Please check your mail and follow the link to complete the process. </br> Did not receive the link? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend the link.";
                            Session["msg"] = "A password reset link has been sent to <a href=\"mailto:" + model.EmailId.Trim() + "\">" + model.EmailId.Trim() + "</a>.Please check your mail and follow the link to complete the process. </br> Did not receive the link? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend the link.";
                        }
                        else
                        {
                            model.status = model.status + "14th";
                            model.message = "Reset link has been successfully sent to <a href=\"mailto:" + model.EmailId.Trim() + "\">" + model.EmailId.Trim() + "</a>. Did not receive the link? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend the link.";
                            TempData["msg"] = "Reset link has been successfully sent to <a href=\"mailto:" + model.EmailId.Trim() + "\">" + model.EmailId.Trim() + "</a>. Did not receive the link? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend the link.";
                            Session["msg"] = "Reset link has been successfully sent to <a href=\"mailto:" + model.EmailId.Trim() + "\">" + model.EmailId.Trim() + "</a>. Did not receive the link? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend the link.";
                        }

                        model.IsSucceed = model.IsSucceed + 1;

                        if (dt.Rows[0]["userid"] != null && dt.Rows[0]["userid"].ToString().Trim() != "")
                        {

                            model.status = model.status + "30th";
                            if (!string.IsNullOrEmpty(dt.Rows[0]["userid"].ToString().Trim())) {
                                //model.status = model.status + dt.Rows[0]["userid"].ToString().Trim();
                                //model.status = model.status + Usertype.OtherUser.GetHashCode().ToString();
                                //model.status = model.status + model.EmailId.Trim();
                                //model.status = model.status + MessageType.Email_Forgot.GetHashCode().ToString();                                
                                //model.status = model.status + IPAddress;
                                //model.status = model.status + Request.Url.AbsoluteUri.ToString().Trim();
                                //model.status = model.status + ActionType.Email.GetHashCode().ToString();
                                //model.status = model.status + reversedns;
                                //model.status = model.status + source;
                               // model.status = model.status + ConfigurationManager.AppSettings["Subjectforgotpass"].ToString();
                            }
                            
                            //Utility.SaveSMSEmailLog(Convert.ToInt32(dt.Rows[0]["userid"].ToString().Trim()), Usertype.OtherUser.GetHashCode(), model.EmailId.Trim(), "", "", MessageType.Email_Forgot.GetHashCode(), ConfigurationManager.AppSettings["Subjectforgotpass"].ToString(), sbEmailContent.ToString(), 0, "", "", "", IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.Email.GetHashCode(), reversedns, source);
                            Utility.SaveSMSEmailLog(Convert.ToInt32(dt.Rows[0]["userid"].ToString().Trim()), Usertype.OtherUser.GetHashCode(), model.EmailId.Trim(), "", "", MessageType.Email_Forgot.GetHashCode(), "Your VConnect credentials.", sbEmailContent.ToString(), 0, "", "", "", IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.Email.GetHashCode(), reversedns, source);
                        }
                        else
                        {
                            model.status = model.status + "31st";
                           // Utility.SaveSMSEmailLog(0, Usertype.OtherUser.GetHashCode(), model.EmailId.Trim(), "", "", MessageType.Email_Forgot.GetHashCode(), ConfigurationManager.AppSettings["Subjectforgotpass"].ToString(), sbEmailContent.ToString(), 0, "", "", "", IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.Email.GetHashCode(), reversedns, source);
                            Utility.SaveSMSEmailLog(0, Usertype.OtherUser.GetHashCode(), model.EmailId.Trim(), "", "", MessageType.Email_Forgot.GetHashCode(), "Your VConnect credentials.", sbEmailContent.ToString(), 0, "", "", "", IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.Email.GetHashCode(), reversedns, source);
                        }

                       
                    }
                    else
                    {
                        model.status = model.status + "15th";
                        long result = 0;
                        if (long.TryParse(model.EmailId.Trim(), out result))
                        {
                            int vaildphone = Utility.isValidPhone(model.EmailId.Trim());
                            if (vaildphone == 0)
                            {
                                TempData["msg"] = "<p>Phone no is not valid.</p>";
                                Session["msg"] = "<p>Phone no is not valid.</p>";
                                model.message = "<p>Phone no is not valid.</p>";
                                System.Web.HttpContext.Current.Session["FPmodel"] = model;
                                return RedirectToRoute("forgotpassword");
                            }

                        }
                        StringBuilder sbMobileContent = new StringBuilder();
                        SendSMS objSendSMS = new SendSMS();

                        string SMSResponse = string.Empty;

                        if ((dt.Rows[0]["ismobileverified"].ToString().Trim() == "1" || dt.Rows[0]["ismobileverified"].ToString().Trim().ToLower() == "true") && (dt.Rows[0]["isverified"].ToString().Trim() == "1" || dt.Rows[0]["isverified"].ToString().Trim() == "1") && (dt.Rows[0]["isactive"].ToString().Trim() == "1" || dt.Rows[0]["isactive"].ToString().Trim() == "1"))
                        {
                            sbMobileContent.Append("Your Password is " + (!string.IsNullOrWhiteSpace(dt.Rows[0]["password"].ToString())? dt.Rows[0]["password"].ToString().Trim():""));
                            model.IsSucceed = model.IsSucceed + 1;

                            if (model.IsSucceed == 1)
                            {
                                model.status = model.status + "16th";
                                model.message = "<p>Your credentials has been sent to your mobile no " + model.EmailId.Trim() + ". Did not receive the credentials? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend.</p>";
                                TempData["msg"] = "<p>Your credentials has been sent to your mobile no "+model.EmailId.Trim()+". Did not receive the credentials? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend.</p>";
                                Session["msg"] = "<p>Your credentials has been sent to your mobile no " + model.EmailId.Trim() + ". Did not receive the credentials? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend.</p>";
                            }
                            else
                            {
                                model.status = model.status + "17th";
                                model.message = "<p>Credentials successfully sent. Did not receive the credentials? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend.</p>";
                                TempData["msg"] = "<p>Credentials successfully sent. Did not receive the credentials? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend.</p>";
                                Session["msg"] = "<p>Credentials successfully sent. Did not receive the credentials? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend.</p>";
                            }
                         
                            SMSResponse = objSendSMS.Sendmessage(sbMobileContent.ToString() + ".", model.EmailId.Trim());


                            //Maintain the SMS EMAIL LOG
                            if (dt.Rows[0]["userid"] != null && dt.Rows[0]["userid"].ToString().Trim() != "")
                            {
                                Utility.SaveSMSEmailLog(Convert.ToInt32(dt.Rows[0]["userid"].ToString().Trim()), Usertype.OtherUser.GetHashCode(), "", model.EmailId.Trim(), "", MessageType.SMS_Forgot.GetHashCode(), ConfigurationManager.AppSettings["Subjectforgotpass"].ToString(), sbMobileContent.ToString(), 0, "", "", SMSResponse.ToString(), IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), reversedns, source);
                            }
                            else
                            {
                                Utility.SaveSMSEmailLog(0, Usertype.OtherUser.GetHashCode(), "", model.EmailId.Trim(), "", MessageType.SMS_Forgot.GetHashCode(), ConfigurationManager.AppSettings["Subjectforgotpass"].ToString(), sbMobileContent.ToString(), 0, "", "", SMSResponse.ToString(), IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), reversedns, source);
                            }
                            System.Web.HttpContext.Current.Session["FPmodel"] = model;
                            //return RedirectToRoute("forgotpassword");
                            return RedirectToAction("forgotpassword", model);
                        }
                        else
                        {
                            model.status = model.status + "18th";
                            if (string.IsNullOrWhiteSpace(model.MobileVerificationCode))
                            {
                                TempData["VerifyCode"] = randCode;
                                TempData.Keep("VerifyCode");

                            
                                model.IsNotMobileVerified = 1;

                              
                                WEBLoginDatabaseCall.UpdateNewVerificationCode(model.EmailId.Trim(), randCode);

                                sbMobileContent.Append("Your verification code " + randCode);
                                if (model.isVerifiedResendCount == 0)
                                {
                                    model.status = model.status + "19th";
                                    model.message = "<p>Please enter the verification code sent to your mobile no " + model.EmailId.Trim() + ". Did not receive the code? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend.</p>";
                                    TempData["msg"] = "<p>Please enter the verification code sent to your mobile no "+model.EmailId.Trim()+". Did not receive the code? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend.</p>";
                                    Session["msg"] = "<p>Please enter the verification code sent to your mobile no " + model.EmailId.Trim() + ". Did not receive the code? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend.</p>";
                                }
                                else
                                {
                                    model.status = model.status + "20th";
                                    model.message = "<p>Verification code successfully sent. Did not receive the code? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend.</p>";
                                    TempData["msg"] = "<p>Verification code successfully sent. Did not receive the code? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend.</p>";
                                    Session["msg"] = "<p>Verification code successfully sent. Did not receive the code? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend.</p>";
                                }

                                model.isVerifiedResendCount = model.isVerifiedResendCount + 1;

                                SMSResponse = objSendSMS.Sendmessage(sbMobileContent.ToString() + ".", model.EmailId.Trim());

                                //Maintain the SMS EMAIL LOG
                                if (dt.Rows[0]["userid"] != null && dt.Rows[0]["userid"].ToString().Trim() != "")
                                {

                                    Utility.SaveSMSEmailLog(Convert.ToInt32(dt.Rows[0]["userid"].ToString().Trim()), Usertype.OtherUser.GetHashCode(), "", model.EmailId.Trim(), "", MessageType.SMS_Forgot.GetHashCode(), "Your Verification Code", sbMobileContent.ToString(), 0, "", "", SMSResponse.ToString(), IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), reversedns, source);
                                }
                                else
                                {
                                    Utility.SaveSMSEmailLog(0, Usertype.OtherUser.GetHashCode(), "", model.EmailId.Trim(), "", MessageType.SMS_Forgot.GetHashCode(), "Your Verification Code", sbMobileContent.ToString(), 0, "", "", SMSResponse.ToString(), IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), reversedns, source);
                                }
                                System.Web.HttpContext.Current.Session["FPmodel"] = model;
                                return RedirectToAction("forgotpassword", model);
                                //return RedirectToRoute("forgotpassword");
                            }
                            else
                            {
                                model.ConfirmMobileVerificationCode = !string.IsNullOrEmpty(TempData.Peek("VerifyCode").ToString().Trim()) ? TempData.Peek("VerifyCode").ToString().Trim() : "";
                                model.status = model.status + "21st";
                                if (model.ConfirmMobileVerificationCode.Trim().Equals(model.MobileVerificationCode.Trim()))
                                {
                                   
                                    model.IsNotMobileVerified = 0;
                                    model.IsSucceed = model.IsSucceed + 1;

                                    //changed today
                                    sbMobileContent.Append("Your Password is " + (!string.IsNullOrWhiteSpace(dt.Rows[0]["password"].ToString()) ? dt.Rows[0]["password"].ToString().Trim() : ""));
                          
           //                         sbMobileContent.Append("Your credentials has been sent to your mobile no."); // + model.EmailId.Trim());
                                    if (model.IsSucceed == 0)
                                    {
                                        model.status = model.status + "22nd";
                                         model.message = "<p>Your credentials has been sent to your mobile no "+model.EmailId.Trim()+". Did not receive the credentials? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend.</p>";
                                        TempData["msg"] = "<p>Your credentials has been sent to your mobile no "+model.EmailId.Trim()+". Did not receive the credentials? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend.</p>";
                                        Session["msg"] = "<p>Your credentials has been sent to your mobile no " + model.EmailId.Trim() + ". Did not receive the credentials? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend.</p>";
                                    }
                                    else
                                    {
                                        model.status = model.status + "23rd";
                                        model.message = "<p>Credentials successfully sent. Did not receive the credentials? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend.</p>";
                                        TempData["msg"] = "<p>Credentials successfully sent. Did not receive the credentials? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend.</p>";
                                        Session["msg"] = "<p>Credentials successfully sent. Did not receive the credentials? <a href=\"javascript:void(0);\" onclick=\"document.getElementById('form1').submit();\" class=\"red-link underline\">click here</a> to resend.</p>";
                                    }

                                    model.IsSucceed = model.IsSucceed + 1;
                                    if (dt.Rows[0]["userid"] != null && dt.Rows[0]["userid"].ToString().Trim() != "")
                                    {
                                        string retrnVal = WEBLoginDatabaseCall.UpdateIsMobileEmailVerified(dt.Rows[0]["userid"].ToString().Trim(), 1);

                                        if (retrnVal == "1")
                                        {
                                            model.status = model.status + "24th";
                                            SMSResponse = objSendSMS.Sendmessage(sbMobileContent.ToString() + ".", model.EmailId.Trim());

                                            if (dt.Rows[0]["userid"] != null && dt.Rows[0]["userid"].ToString().Trim() != "")
                                            {
                                                Utility.SaveSMSEmailLog(Convert.ToInt32(dt.Rows[0]["userid"].ToString().Trim()), Usertype.OtherUser.GetHashCode(), "", model.EmailId.Trim(), "", MessageType.SMS_Forgot.GetHashCode(), ConfigurationManager.AppSettings["Subjectforgotpass"].ToString(), sbMobileContent.ToString(), 0, "", "", SMSResponse.ToString(), IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), reversedns, source);
                                            }
                                            else
                                            {
                                                Utility.SaveSMSEmailLog(0, Usertype.OtherUser.GetHashCode(), "", model.EmailId.Trim(), "", MessageType.SMS_Forgot.GetHashCode(), ConfigurationManager.AppSettings["Subjectforgotpass"].ToString(), sbMobileContent.ToString(), 0, "", "", SMSResponse.ToString(), IPAddress, 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), reversedns, source);
                                            }
                                            TempData.Remove("VerifyCode");
                                        }
                                        else
                                        {

                                            model.status = model.status + "25th";
                                            TempData["msg"] = retrnVal;
                                            Session["msg"] = retrnVal;
                                            model.message = retrnVal;
                                        }
                                    }
                                    else
                                    {
                                        model.status = model.status + "26th";
                                        model.message = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Some error occurred.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>"; 
                                        TempData["msg"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Some error occurred.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>"; 
                                        Session["msg"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Some error occurred.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>"; 
                                    }
                                }
                                else
                                {
                                    model.status = model.status + "27th";
                                    model.IsNotMobileVerified = 1;
                                    TempData["loginError"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Verification code you entered is incorrect.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                                   // Session["msg"] = TempData["msg"].ToString();
                                }

                            }

                        }

                    }

                }
                else
                {
                    TempData["loginError"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Email address does not exists.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                }


                if (Request.QueryString["code"] != null)
                {
                    model.status = model.status + "28th";
                    model.EmailVerificationCode = Request.QueryString["code"] != null ? Request.QueryString["code"].ToString() : "";
                    return RedirectToRoute("newpassword",new {uid =  model.userId ,code= model.EmailVerificationCode , cnt= model.IsSucceed});
                }
                else
                {
                    model.status = model.status + "29th";
                    System.Web.HttpContext.Current.Session["FPmodel"] = model;
                  //  return RedirectToRoute("forgotpassword");
                    //return RedirectToRoute("forgotpassword");
                    return RedirectToAction("forgotpassword", model);
                }

            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                if (Request.QueryString["code"] != null)
                {
                    model.EmailVerificationCode = Request.QueryString["code"] != null ? Request.QueryString["code"].ToString() : "";
                    return RedirectToRoute("newpassword", new { uid = model.userId, code = model.EmailVerificationCode, cnt = model.IsSucceed });
                }
                else
                {
                    System.Web.HttpContext.Current.Session["FPmodel"] = model;
                    return RedirectToRoute("forgotpassword");
                }
            }
        }

        #endregion
        

        #region Helpers
       
        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider="", string returnUrl="")
            {
                string rturl = "";
                int indxSrc = returnUrl.IndexOf("rturl=");

                if (indxSrc > 0)
                {  
                   rturl = returnUrl.Substring((indxSrc + 6), ((returnUrl.Length - 6) - (indxSrc)));
                   if (rturl != null && rturl.ToString().Trim() != "" && !rturl.Contains("http"))
                   {
                        object rt = CatchRTUrl(rturl);
                        rturl = rt.ToString().Trim();

                        rturl = returnUrl.Substring(0, indxSrc);
                        rturl = rturl + "rturl=" + rt.ToString();
                        ReturnUrl = rturl;
                    }
                    else
                    {
                        ReturnUrl = returnUrl;
                    }
                }
                else
                {
                    ReturnUrl = returnUrl;
                }

                Provider = provider;
                
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                
                try
              
                {
                    if (Provider != null && Provider.Trim() != "")
                    {
                        
                        OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
                    }
                    else
                    {
                        if (context.HttpContext.Session["rturl"] != null && !string.IsNullOrEmpty(context.HttpContext.Session["rturl"].ToString().Trim()))
                            context.HttpContext.Response.RedirectPermanent("/login?rturl=" + context.HttpContext.Session["rturl"].ToString().Trim().Replace("&", "$"));
                        else
                            context.HttpContext.Response.RedirectPermanent("/login");
                    }
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                    return;
                }
            }
        }

        #endregion



    }

}
