﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vconnect.Models;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text.RegularExpressions;
using Vconnect.Common;
using System.Configuration;
using System.IO;
using System.Web.Routing;
using Vconnect.Enums;
using System.Net.Mail;
using MailChimp.Types;

namespace Vconnect.Controllers
{
    public class BizRegController : Controller
    {
        UserSession objUS = new UserSession();
        #region drop for State/LGA
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetProductByCategory(int? catId)
        {
            if (catId.HasValue && (!Request.AcceptTypes.Contains("text/html")))
            {
               return Json(Vconnect.Common.Utility.GetSelectListCity(catId.Value), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region phoneNumberCheck
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult phonenumbercheckalt(string altphonenumber, string phonenumber,int? userid)
        {
            if ((!Request.AcceptTypes.Contains("text/html")))
            {
                int d = 4;
                if (!string.IsNullOrEmpty(altphonenumber))
                {
                    int result = Vconnect.Common.Utility.isValidPhone(altphonenumber);
                    int result1 = 0;
                    if (userid.HasValue)
                    {
                        result1 = Vconnect.Common.Utility.businessphonechk(altphonenumber, userid.Value);
                    }
                    string sContactNumber = altphonenumber;
                    string subSection = sContactNumber.Substring(0, 1);

                    if (subSection != "0")
                    {
                        d = 5;
                    }
                    else if (result == 0)
                    {
                        d = 2;
                    }
                    else if (result1 == 1)
                    {
                        d = 1;
                    }
                    else
                    {
                        d = 0;
                        if (!string.IsNullOrEmpty(phonenumber) && !string.IsNullOrEmpty(altphonenumber))
                        {
                            if (altphonenumber == phonenumber)
                                d = 6;
                        }
                    }
                }
                else
                {
                    d = 4;
                }
                return Json(d, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult phonenumbercheck(string phonenumber, int? userid)
        {
          if ((!Request.AcceptTypes.Contains("text/html")))
            {
            int d = 4;
            if (!string.IsNullOrEmpty(phonenumber))
            {
                int result1 = 0;
                int result = Vconnect.Common.Utility.isValidPhone(phonenumber);
                if(userid.HasValue)
                result1 = Vconnect.Common.Utility.businessphonechk(phonenumber, userid.Value);
                string sContactNumber = phonenumber;
                string subSection = sContactNumber.Substring(0, 1);
                if (subSection != "0")
                {
                    d = 5;
                }
                else if (result == 0)
                {
                    d = 2;
                }
                else if (result1 == 1)
                {
                    d = 1;
                }
                else
                {
                    d = 0;
                }
            }
            else
            {
                d = 4;
            }

            return Json(d, JsonRequestBehavior.AllowGet);
            }
             else
             {
                 return Json(null, JsonRequestBehavior.AllowGet);
             }
        }
        #endregion 
        #region bizregweb
        [HttpGet]
        public ActionResult BizReg()
        {
                BizRegModels homeWapModel = new BizRegModels();
                homeWapModel.SearchLocation = string.IsNullOrEmpty(homeWapModel.SearchLocation) ? "Lagos" : homeWapModel.SearchLocation;
                homeWapModel.ConditionaValue = true;
                List<SelectListItem> ListStatesNames = new List<SelectListItem>();
                SearchBarWebModel searchwebmodel = new SearchBarWebModel();
                searchwebmodel.StateOptions = Utility.GetSelectList();
                List<SelectListItem> listStatesNames = new List<SelectListItem>();
                homeWapModel.StateOptions = Utility.GetSelectList();
                List<SelectListItem> listCiyNames = new List<SelectListItem>();
                homeWapModel.CityOptions = Utility.GetSelectListCity(125);
                UserSession objUS = new UserSession();
                if (objUS.IsSessionAlive())
                {
                    homeWapModel.CAPTCHA = objUS.SV_VCUserContentID;
                }
                else
                {
                    homeWapModel.CAPTCHA="0";
                }
                homeWapModel.SuccessMessage = TempData["SuccessMessage"] as string;
                return View(homeWapModel);
        }
        [HttpPost]
        public ActionResult BizReg(BizRegModels homeWapModel,FormCollection frm)
        {
             string hidevalue = frm["hidvaluetext"];
             if (string.IsNullOrEmpty(hidevalue))
             {
                 bool a = true;
                 UserSession objUS = new UserSession();
                 if (objUS.IsSessionAlive())
                 {
                     homeWapModel.CAPTCHA = objUS.SV_VCUserContentID;
                 }
                 else
                 {
                     homeWapModel.CAPTCHA = "0";
                 }
                 if (!string.IsNullOrEmpty(homeWapModel.txtemail))
                 {
                     Regex regEmail = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
                     a = regEmail.IsMatch(homeWapModel.txtemail);
                     if (Regex.Matches(homeWapModel.txtemail, "@").Count > 1 || (homeWapModel.txtemail.IndexOfAny(new char[] { '/', '%', '!', '#', '$', '^', '&', '*', '(', ')', '+', '=', '|', '{', '}', '[', ']', ':', ';', ',', '>', '<', '?', '~', '`', '"', '-' }) > 0) || Regex.Matches(homeWapModel.txtemail, "'").Count > 0)
                         a = false;

                 }
                 List<SelectListItem> listStatesNames = new List<SelectListItem>();
                 homeWapModel.StateOptions = Vconnect.Common.Utility.GetSelectList();
                 List<SelectListItem> listCiyNames = new List<SelectListItem>();
                 homeWapModel.CityOptions = Vconnect.Common.Utility.GetSelectListCity(homeWapModel.SelectedStateId);
                 int result = 1, result1 = 0, result2 = 1, result3 = 0;
                 int useridupdate;
                 if (objUS.IsSessionAlive())
                 {
                     useridupdate = Convert.ToInt32(objUS.SV_VCUserContentID);
                 }
                 else
                 {
                     useridupdate = 0;
                 }
                 if (homeWapModel.txtContactPersonPhone != null)
                 {
                     result = Vconnect.Common.Utility.isValidPhone(homeWapModel.txtContactPersonPhone);
                     result1 = Vconnect.Common.Utility.businessphonechk(homeWapModel.txtContactPersonPhone, useridupdate);
                 }
                 if (homeWapModel.txtAltPhone != null)
                 {
                     result2 = Vconnect.Common.Utility.isValidPhone(homeWapModel.txtAltPhone);
                     result3 = Vconnect.Common.Utility.businessphonechk(homeWapModel.txtAltPhone, useridupdate);
                 }
                 else
                     homeWapModel.txtAltPhone = "";
                 if (homeWapModel.txtCompanyName == null)
                     ViewBag.Msg = "Enter the company name";
                 else if (homeWapModel.txtContactPersonPhone == null)
                 {
                     ViewBag.Msg = "Enter company contact number";
                 }
                 else if (result == 0)
                 {
                     ViewBag.Msg = "Invalid contact number";
                 }
                 else if (result1 == 1)
                 {
                     ViewBag.Msg = "Sorry, a business is already registered with this contact number. Please provide another valid phone number to continue registration";
                 }
                 else if (result2 == 0)
                 {
                     ViewBag.Msg = "Invalid alternate phone number";
                 }
                 else if (result3 == 1)
                 {
                     ViewBag.Msg = "Alternate phone no. already exits";
                 }
                 else if (homeWapModel.txtContactPersonPhone == homeWapModel.txtAltPhone)
                 {
                     ViewBag.Msg = "Alternate phone match with company phone number";
                 }
                 else if (a == false)
                 {
                     ViewBag.Msg = "Invalid Email-Id";
                 }
                 else if (homeWapModel.txtAddress == null)
                 {
                     ViewBag.Msg = "Enter the address of company";
                 }
                 else if (homeWapModel.SelectedStateId == 0)
                     ViewBag.Msg = "Select the state of the company";
                 else if (homeWapModel.SelectedCityId == 0)
                     ViewBag.Msg = "Select the LGA of the company";
                 else if (homeWapModel.txtProduct == null)
                 {
                     ViewBag.Msg = "Enter a product/service ";
                 }
                 //else if (string.IsNullOrEmpty(homeWapModel.txtemail))
                 //{
                 //    ViewBag.Msg = "Enter email-id ";
                 //}
                 else if (homeWapModel.ConditionaValue == false)
                     ViewBag.Msg = "Please accept the terms and conditions.";
                 else
                 {
                     if (string.IsNullOrEmpty(homeWapModel.txtemail))
                     {
                         homeWapModel.txtemail = "";
                     }
                     if (string.IsNullOrEmpty(homeWapModel.txtwebsite))
                     {
                         homeWapModel.txtwebsite = "";
                     }
                     using (var db = new VconnectDBContext29())
                     {
                         int userid;
                         Int64 bid = 0;
                         userid = 0;
                         int agentid = 0;
                         int utype = 0;
                         if (objUS.IsSessionAlive())
                         {
                             userid = Convert.ToInt32(objUS.SV_VCUserContentID);
                             utype = 1;
                             agentid = 0;

                         }
                         else if (objUS.SV_VCisagent != "" && Convert.ToInt32(objUS.SV_VCisagent) == 1)
                         {
                             agentid = Convert.ToInt32(objUS.SV_VCUserContentID);
                             utype = 3;
                             userid = 0;
                         }
                         else
                         {
                             agentid = 0;
                             utype = 1;
                             userid = 0;
                         }
                         var cmd = db.Database.Connection.CreateCommand();
                         cmd.CommandText = "[dbo].[prc_web_add_newbusinessregistration_opt]";
                         cmd.CommandType = System.Data.CommandType.StoredProcedure;
                         cmd.Parameters.Add(new SqlParameter("@UserID", userid));
                         cmd.Parameters.Add(new SqlParameter("@usertype", utype));
                         cmd.Parameters.Add(new SqlParameter("@CompanyName", homeWapModel.txtCompanyName.ToString()));
                         cmd.Parameters.Add(new SqlParameter("@Status", "0"));
                         cmd.Parameters.Add(new SqlParameter("@phone", homeWapModel.txtContactPersonPhone.ToString()));
                         cmd.Parameters.Add(new SqlParameter("@alterphone", homeWapModel.txtAltPhone.ToString()));
                         cmd.Parameters.Add(new SqlParameter("@address", homeWapModel.txtAddress.ToString()));
                         cmd.Parameters.Add(new SqlParameter("@source", "WEB"));
                         cmd.Parameters.Add(new SqlParameter("@stateid", homeWapModel.SelectedStateId.ToString()));
                         cmd.Parameters.Add(new SqlParameter("@cityid", homeWapModel.SelectedCityId.ToString()));
                         cmd.Parameters.Add(new SqlParameter("@contactperson", homeWapModel.txtcontactname));
                         cmd.Parameters.Add(new SqlParameter("@createdby", userid));
                         cmd.Parameters.Add(new SqlParameter("@website", homeWapModel.txtwebsite));
                         cmd.Parameters.Add(new SqlParameter("@productname", homeWapModel.txtProduct));
                         cmd.Parameters.Add(new SqlParameter("@email", homeWapModel.txtemail.ToString()));
                         //SqlParameter parameter1 = new SqlParameter("@errorcode", SqlDbType.VarChar, 100);
                         //cmd.Parameters.Add(parameter1);
                         cmd.Parameters.Add(new SqlParameter("@errorcode", SqlDbType.VarChar, 100));
                         cmd.Parameters["@errorcode"].Direction = ParameterDirection.Output;
                         try
                         {
                             var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                             connection.Open();
                             var reader = cmd.ExecuteReader();
                             while (reader.Read())
                                 try
                                 {
                                     bid = int.Parse(reader[0].ToString().Trim());
                                 }
                                 catch (Exception ex)
                                 {
                                     log.LogMe(ex);
                                     bid = 0;
                                 }
                             if (bid != 0)
                             {
                                 if (userid != 0)
                                 {
                                     SendFavioriteBizCred(homeWapModel.txtContactPersonPhone.ToString().Trim(), homeWapModel.txtemail.ToString().Trim(), homeWapModel.txtcontactname.ToString().Trim(), homeWapModel.txtCompanyName.ToString().Trim(), homeWapModel.SelectedStateId.ToString().Trim());
                                     return RedirectToAction("BusinessComf", "BizReg", TempData["SessionVariable"]);
                                     //ViewBag.Msg1 = "Thank you for your contribution. The business has been added successfully. It will start appearing on the website once verified by our executives.";
                                 }
                                 else
                                 {
                                     string IPAddress = MyGlobalVariables.GetIpAddress;
                                     SendSMS send = new SendSMS();
                                     string SMSResponse = send.Sendmessage("Thanks for business registration", homeWapModel.txtContactPersonPhone.ToString().Trim());

                                     string sms1 = "Dear Business Owner, Congratulations! Your Business was successfully added on VConnect. VConnect team will call you to complete your online profile.";
                                     //string sms2 = "VConnect is Nigeria's largest Business database. To know how VConnect can help you grow your business, Flash on 01-247-2255 or mail us at business@vconnect.com.";
                                     string sms2 = "VConnect is Nigeria's largest Business database. To know how VConnect can help you grow your business, mail us at business@vconnect.com.";
                                     string msg1 = send.Sendmessage(sms1.ToString(), homeWapModel.txtContactPersonPhone.ToString());
                                     string msg2 = send.Sendmessage(sms2.ToString(), homeWapModel.txtContactPersonPhone.ToString());
                                     if (!string.IsNullOrEmpty(homeWapModel.txtemail))
                                     {
                                         SendFavioriteBizCred(homeWapModel.txtContactPersonPhone.ToString().Trim(), homeWapModel.txtemail.ToString().Trim(), homeWapModel.txtcontactname.ToString().Trim(), homeWapModel.txtCompanyName.ToString().Trim(), homeWapModel.SelectedStateId.ToString().Trim());
                                         //Maintain the SMS EMAIL LOG
                                         if (objUS.SV_VCUserContentID != null && objUS.SV_VCUserContentID != "")
                                         {
                                             Utility.SaveSMSEmailLog(Convert.ToInt32(objUS.SV_VCUserContentID), Usertype.B2CbusinessOwner.GetHashCode(), homeWapModel.txtemail.ToString().Trim(), homeWapModel.txtContactPersonPhone, homeWapModel.txtCompanyName.ToString().Trim(), MessageType.Email_Registration.GetHashCode(), "Business registration", "Thanks for business registration", 0, "", "", "", IPAddress, 0, 0, MyGlobalVariables._sourceUrl, ActionType.Email.GetHashCode(), MyGlobalVariables.GetReverseDns, "WEB");
                                         }
                                         else
                                         {
                                             Utility.SaveSMSEmailLog(0, Usertype.B2CbusinessOwner.GetHashCode(), homeWapModel.txtemail.ToString().Trim(), homeWapModel.txtContactPersonPhone, homeWapModel.txtCompanyName.ToString().Trim(), MessageType.Email_Registration.GetHashCode(), "Business registration", "Thanks for business registration", 0, "", "", "", IPAddress, 0, 0, MyGlobalVariables._sourceUrl, ActionType.Email.GetHashCode(), MyGlobalVariables.GetReverseDns, "WEB");
                                         }
                                     }
                                     //Maintain the SMS EMAIL LOG
                                     if (objUS.SV_VCUserContentID != null && objUS.SV_VCUserContentID != "")
                                     {
                                         Utility.SaveSMSEmailLog(Convert.ToInt32(objUS.SV_VCUserContentID), Usertype.B2CbusinessOwner.GetHashCode(), homeWapModel.txtemail.ToString().Trim(), homeWapModel.txtContactPersonPhone, homeWapModel.txtCompanyName.ToString().Trim(), MessageType.SMS_BizReg.GetHashCode(), "Business registration", sms1.ToString(), 0, "", "", SMSResponse, IPAddress, 0, 0, MyGlobalVariables._sourceUrl, ActionType.SMS.GetHashCode(), MyGlobalVariables.GetReverseDns, "WEB");
                                         Utility.SaveSMSEmailLog(Convert.ToInt32(objUS.SV_VCUserContentID), Usertype.B2CbusinessOwner.GetHashCode(), homeWapModel.txtemail.ToString().Trim(), homeWapModel.txtContactPersonPhone, homeWapModel.txtCompanyName.ToString().Trim(), MessageType.SMS_BizReg.GetHashCode(), "Business registration", sms2.ToString(), 0, "", "", SMSResponse, IPAddress, 0, 0, MyGlobalVariables._sourceUrl, ActionType.SMS.GetHashCode(), MyGlobalVariables.GetReverseDns, "WEB");
                                     }
                                     else
                                     {
                                         Utility.SaveSMSEmailLog(0, Usertype.B2CbusinessOwner.GetHashCode(), homeWapModel.txtemail.ToString().Trim(), homeWapModel.txtContactPersonPhone, homeWapModel.txtCompanyName.ToString().Trim(), MessageType.SMS_BizReg.GetHashCode(), "Business registration", sms1.ToString(), 0, "", "", SMSResponse, IPAddress, 0, 0, MyGlobalVariables._sourceUrl, ActionType.SMS.GetHashCode(), MyGlobalVariables.GetReverseDns, "WEB");
                                         Utility.SaveSMSEmailLog(0, Usertype.B2CbusinessOwner.GetHashCode(), homeWapModel.txtemail.ToString().Trim(), homeWapModel.txtContactPersonPhone, homeWapModel.txtCompanyName.ToString().Trim(), MessageType.SMS_BizReg.GetHashCode(), "Business registration", sms2.ToString(), 0, "", "", SMSResponse, IPAddress, 0, 0, MyGlobalVariables._sourceUrl, ActionType.SMS.GetHashCode(), MyGlobalVariables.GetReverseDns, "WEB");
                                     }
                                     Session["BID"] = bid.ToString();
                                     //TempData["rturl"] = "BizReg / BusinessComf";
                                     return RedirectToAction("Login", "Account", new { cname = Session["BID"].ToString(), rturl = "BizReg/BusinessComf", bid = bid.ToString(), br = "br" });
                                 }
                             }
                             else
                             {
                                 ViewBag.Msg = "Some Error has ocurred!";
                             }
                         }
                         catch (Exception ex)
                         {
                             log.LogMe(ex);
                         }
                         finally
                         {
                             db.Database.Connection.Close();

                         }
                     }
                 }
             }
                return View(homeWapModel);
        }
        #endregion
        #region confirmation
        public ActionResult BusinessComf()
        {
            return View();
        }
        #endregion
        #region sendemail
        protected void SendFavioriteBizCred(string ContactPerson, string EmailID, string UserName, string BusinessName, string address)
        {
            try
            {
                string location = string.Empty;
                string vcusername = UserName;
                string bizName = BusinessName.ToString().Trim();
                string Message = "";
                if (!string.IsNullOrEmpty(EmailID))
                {
                    System.IO.FileStream FsContent = new System.IO.FileStream(Server.MapPath("~/resource/email_templates/businessuserregistration.html"), System.IO.FileMode.Open, System.IO.FileAccess.Read);
                    System.IO.StreamReader SRcontent = new System.IO.StreamReader(FsContent);
                    Message = SRcontent.ReadToEnd();

                    FsContent.Close();
                    SRcontent.Close();
                    FsContent.Dispose();
                    SRcontent.Dispose();
                    Message = Message.Replace("{0}", ContactPerson);
                    Message = Message.Replace("{1}", Utility.ToTitleCase(UserName));
                    Message = Message.Replace("{2}", BusinessName);

                    Message = Message.Replace("{3}", address);
                    Message = Message.Replace("{20}", "www.vconnect.com");
                    var recipients = new List<Mandrill.Messages.Recipient>();
                    recipients.Add(new Mandrill.Messages.Recipient(EmailID, UserName));
                    var mandrill = new Mandrill.Messages.Message()
                    {
                        To = recipients.ToArray(),
                        FromEmail = "business@vconnect.com",
                        Subject = "Your Business is Registered on VConnect",

                        Html = Message
                    };
                    //===================================


                    SendEmail send = new SendEmail();
                    try
                    {
                        send.BOMails(mandrill);
                    }
                    catch (Exception ex)
                    {
                        log.LogMe(ex);
                    }
                    // Email.Dispose();
                }
            }
            catch
            { }


        }
        #endregion 
        #region IBTC bank
        public ActionResult ibtcbank()
        {
            ibtcModels ibtc = new ibtcModels();
            ibtc.SuccessMessage = TempData["SuccessMessage"] as string;
            return View(ibtc);
        }
        [HttpPost]
        public ActionResult ibtcbank(ibtcModels ibtc)
        {
            bool a = true;
            if (ibtc.txtemail != null)
            {
                Regex regEmail = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
                a = regEmail.IsMatch(ibtc.txtemail);
                if (Regex.Matches(ibtc.txtemail, "@").Count > 1 || (ibtc.txtemail.IndexOfAny(new char[] { '/', '%', '!', '#', '$', '^', '&', '*', '(', ')', '+', '=', '|', '{', '}', '[', ']', ':', ';', ',', '>', '<', '?', '~', '`', '"', '-' }) > 0) || Regex.Matches(ibtc.txtemail, "'").Count > 0)
                    a = false;
            }
            int result = 1, result1 = 0;
            if (ibtc.txtphone != null)
            {
                result = Vconnect.Common.Utility.isValidPhone(ibtc.txtphone);
                // result1 = ibtcphonechk(ibtc.txtphone);
            }
            if (ibtc.txtname == null)
                ViewBag.Msg = "Enter your name";
            else if (ibtc.txtdesignation == null)
            {
                ViewBag.Msg = "Enter your designation";
            }
            else if (result == 0)
            {
                ViewBag.Msg = "Invalid contact number";
            }
            //else if (result1 == 1)
            //{
            //    ViewBag.Msg = "Sorry, a business is already registered with this contact number. Please provide another valid phone number to continue registration";
            //}           
            else if (ibtc.txtbizname == null)
            {
                ViewBag.Msg = "Enter your business name";
            }
            else if (ibtc.txtbizcategory == null)
                ViewBag.Msg = "Enter your business category";
            else if (a == false)
            {
                ViewBag.Msg = "Invalid Email-Id";
            }
            else
            {
                using (var db = new VconnectDBContext29())
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[prc_add_clientadds]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@clientname", "Stanbic IBTC Bank"));
                    cmd.Parameters.Add(new SqlParameter("@username", ibtc.txtname.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@designation", ibtc.txtdesignation.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@phoneno", ibtc.txtphone.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@bizname", ibtc.txtbizname.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@bizcategory", ibtc.txtbizcategory.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@emailid", ibtc.txtemail.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@source", "WEB"));
                    cmd.Parameters.Add(new SqlParameter("@Err", SqlDbType.VarChar, 100));
                    cmd.Parameters["@Err"].Direction = ParameterDirection.Output;
                    try
                    {
                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        cmd.ExecuteNonQuery();
                        TempData["SuccessMessage"] = "You have successfuly registered.";

                        return RedirectToAction("ibtcbank", "BizReg");
                    }
                    catch (Exception ex)
                    {
                        log.LogMe(ex);
                    }
                    finally
                    {
                        db.Database.Connection.Close();

                    }
                }
            }
            return View(ibtc);
        }
        public static int ibtcphonechk(string phone)
        {
            int result = 0;

            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_add_clientadds]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandTimeout = 300;
                cmd.Parameters.Add(new SqlParameter("@phoneno", phone));
                cmd.Parameters.Add(new SqlParameter("@Err", SqlDbType.VarChar, 10));
                cmd.Parameters["@Err"].Direction = ParameterDirection.Output;
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();
                db.Database.Connection.Close();
                result = Convert.ToInt32(cmd.Parameters["@errorcode"].Value.ToString());
            }
            return result;
        }
        #endregion
        #region NIIT Scholarship
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult niitcityselect(int? catId)
        {
            if (catId.HasValue && (!Request.AcceptTypes.Contains("text/html")))
            {
                string center = string.Empty;
                niitModals niitsch = new niitModals();
                SelectList selecttCity = new SelectList(niitsch.CityOptions=CityList(), "Id", "Name");
                foreach (SelectListItem item in selecttCity.Items)
                {
                    if (item.Value == catId.ToString())
                    {
                        center = "NIIT " + item.Text; ;
                        break;
                    }
                }
                return Json(center, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult niitscholarship()
        {
            niitModals niitsch = new niitModals();
            niitsch.CityOptions = CityList();
            niitsch.CenterOptions = CenterList("Choose center",0);
            niitsch.CourseOptions = CourseList();
            niitsch.StatusOptions = StatusList();
            niitsch.TimeSlotOptions = TimeSlotList();
            niitsch.SuccessMessage = TempData["SuccessMessage"] as string;
            return View(niitsch);
        }
        [HttpPost]
        public ActionResult niitscholarship(niitModals niitsch)
        {
            string city = string.Empty, center = string.Empty, status = string.Empty, timeslot = string.Empty,course=string.Empty;
            bool a = true;
            niitsch.CityOptions = CityList();
            niitsch.CenterOptions = CenterList("Choose center",0);
            niitsch.CourseOptions = CourseList();
            niitsch.StatusOptions = StatusList();
            niitsch.TimeSlotOptions = TimeSlotList();
            SelectList selecttCity = new SelectList(niitsch.CityOptions, "Id", "Name");
            foreach (SelectListItem item in selecttCity.Items)
            {
                if (item.Value == niitsch.SelectedCityId.ToString())
                {
                    city = item.Text;
                    center = "NIIT " + city;
                    niitsch.CenterID = 1;
                    niitsch.CenterOptions = CenterList(center,1);
                    break;
                }
            }
            SelectList selecttStatus = new SelectList(niitsch.StatusOptions, "Id", "Name");
            foreach (SelectListItem item in selecttStatus.Items)
            {
                if (item.Value == niitsch.SelectedStatusId.ToString())
                {
                    status = item.Text;
                    break;
                }
            }
            SelectList selecttCourse = new SelectList(niitsch.CourseOptions, "Id", "Name");
            foreach (SelectListItem item in selecttCourse.Items)
            {
                if (item.Value == niitsch.SelectedCourseId.ToString())
                {
                    course = item.Text;
                    break;
                }
            }
            SelectList selecttTimeslot = new SelectList(niitsch.TimeSlotOptions, "Id", "Name");
            foreach (SelectListItem item in selecttTimeslot.Items)
            {
                if (item.Value == niitsch.SelectedTimeSlotId.ToString())
                {
                    timeslot = item.Text;
                    break;
                }
            }
            if (niitsch.txtemail != null)
            {
                Regex regEmail = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
                a = regEmail.IsMatch(niitsch.txtemail);
                if (Regex.Matches(niitsch.txtemail, "@").Count > 1 || (niitsch.txtemail.IndexOfAny(new char[] { '/', '%', '!', '#', '$', '^', '&', '*', '(', ')', '+', '=', '|', '{', '}', '[', ']', ':', ';', ',', '>', '<', '?', '~', '`', '"', '-' }) > 0) || Regex.Matches(niitsch.txtemail, "'").Count > 0)
                    a = false;
            }
            int result = 1, result1 = 0;
            if (!string.IsNullOrEmpty(niitsch.txtphone))
            {
                result = Vconnect.Common.Utility.isValidPhone(niitsch.txtphone);
                // result1 = ibtcphonechk(niitsch.txtphone);
            }
            if (string.IsNullOrEmpty(niitsch.txtname))
                ViewBag.Msg = "Enter your name";
            else if (result == 0)
            {
                ViewBag.Msg = "Invalid contact number";
            }
            //else if (result1 == 1)
            //{
            //    ViewBag.Msg = "Sorry, a business is already registered with this contact number. Please provide another valid phone number to continue registration";
            //}  
            else if (a == false)
            {
                ViewBag.Msg = "Invalid Email-Id";
            }
            else if (city == string.Empty)
                ViewBag.Msg = "Please  Choose City";
            else if (center == string.Empty)
                ViewBag.Msg = "Please  Choose centre ";
            else if (status == string.Empty)
                ViewBag.Msg = "Select Current Status";
            else if (course == string.Empty)
                ViewBag.Msg = "Select course";
            else if (timeslot == string.Empty)
                ViewBag.Msg = "Please  Choose Time";
            else
            {

                using (var db = new VconnectDBContext29())
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[prc_add_clientadds]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@clientname", "NIIT Scholarship"));
                    cmd.Parameters.Add(new SqlParameter("@username", niitsch.txtname.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@phoneno", niitsch.txtphone.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@emailid", niitsch.txtemail.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@city", city));
                    cmd.Parameters.Add(new SqlParameter("@center", center));
                    cmd.Parameters.Add(new SqlParameter("@statuscurrent", status));
                    cmd.Parameters.Add(new SqlParameter("@course", course));
                    cmd.Parameters.Add(new SqlParameter("@timeslot", timeslot));
                    cmd.Parameters.Add(new SqlParameter("@source", "WEB"));
                    cmd.Parameters.Add(new SqlParameter("@Err", SqlDbType.VarChar, 100));
                    cmd.Parameters["@Err"].Direction = ParameterDirection.Output;
                    try
                    {
                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        cmd.ExecuteNonQuery();
                        TempData["SuccessMessage"] = "Thank You  We will Get Back to you Soon";
                        return RedirectToAction("niitscholarship", "BizReg");
                    }
                    catch (Exception ex)
                    {
                        log.LogMe(ex);
                    }
                    finally
                    {
                        db.Database.Connection.Close();
                    }
                }
            }

            return View(niitsch);
        }
        public static SelectList CityList()
        {
            var list = new[] 
                {
                     new StatesDropDown  { StateId = 1 ,StateName = "Abuja"},
                     new StatesDropDown  { StateId = 2 ,StateName = "Ajah"},
                     new StatesDropDown  { StateId = 3 ,StateName = "Asaba"},
                     new StatesDropDown  { StateId = 4 ,StateName = "Benin"},
                     new StatesDropDown  { StateId = 5 ,StateName = "Eket"},
                     new StatesDropDown  { StateId = 6 ,StateName = "Festac"},
                     new StatesDropDown  { StateId = 7 ,StateName = "Ibadan"},
                     new StatesDropDown  { StateId = 8 ,StateName = "Ikeja"},
                     new StatesDropDown  { StateId = 9 ,StateName = "Kaduna"},
                     new StatesDropDown  { StateId = 10 ,StateName = "Lokoja"},
                     new StatesDropDown  { StateId = 11 ,StateName = "Makurdi"},
                     new StatesDropDown  { StateId = 12 ,StateName = "Maraba"},
                     new StatesDropDown  { StateId = 13 ,StateName = "Offa"},
                     new StatesDropDown  { StateId = 14 ,StateName = "Onitsha"},
                     new StatesDropDown  { StateId = 15 ,StateName = "Otta"},
                     new StatesDropDown  { StateId = 16 ,StateName = "Owerri"},
					 new StatesDropDown  { StateId = 17 ,StateName = "Port Harcourt"},
					 new StatesDropDown  { StateId = 18 ,StateName = "Surulere"},
                     new StatesDropDown  { StateId = 19 ,StateName = "Warri"}
                };
            return new SelectList(list, "StateId", "StateName");
        }
        public static SelectList CenterList(string course,int id)
        {
            var list = new[] 
                {
                    new StatesDropDown  { StateId = id ,StateName = course}
                };
            return new SelectList(list, "StateId", "StateName");
        }
        public static SelectList StatusList()
        {
            var list = new[] 
                {
                    new StatesDropDown  { StateId = 1 ,StateName = "School"},
                    new StatesDropDown  { StateId = 2 ,StateName = "'O'LEVEL"},
                    new StatesDropDown  { StateId = 3 ,StateName = "'A'LEVEL"},
                    new StatesDropDown  { StateId = 4 ,StateName = "COLLEGE-YEAR1"},
                    new StatesDropDown  { StateId = 5 ,StateName = "COLLEGE-YEAR2"},
                    new StatesDropDown  { StateId = 6 ,StateName = "COLLEGE-YEAR3"},
                    new StatesDropDown  { StateId = 7 ,StateName = "COLLEGE-YEAR4"},
                    new StatesDropDown  { StateId = 8 ,StateName = "Polytechnic Employed"},
                    new StatesDropDown  { StateId = 9 ,StateName = "Polytechnic Un-Employed"},
                    new StatesDropDown  { StateId = 10 ,StateName = "IT Graduate Employed"},
                    new StatesDropDown  { StateId = 11 ,StateName = "IT Graduate Un-Employed"},
                    new StatesDropDown  { StateId = 12 ,StateName = "Non-IT Graduate Employed"},
                    new StatesDropDown  { StateId = 13 ,StateName = "Non-IT Graduate Un-Employed"},
                    new StatesDropDown  { StateId = 14 ,StateName = "Post Graduate Employed"},
                    new StatesDropDown  { StateId = 15 ,StateName = "Post Graduate Un-Employed"},
                    new StatesDropDown  { StateId = 16 ,StateName = "Others"}
                };
            return new SelectList(list, "StateId", "StateName");
        }
         public static SelectList CourseList()
        {
            var list = new[] 
                {
                    new StatesDropDown  { StateId = 1 ,StateName = "Mind Series (MMS)"},
                    new StatesDropDown  { StateId = 2 ,StateName = "The GNIIT  Cloud Centric Curriculum"},
                    new StatesDropDown  { StateId = 3 ,StateName = "Oracle 11g / 12C"},
                    new StatesDropDown  { StateId = 4 ,StateName = "CCNA"},
                    new StatesDropDown  { StateId = 5 ,StateName = "Diploma in Hardware and Networking"},
                    new StatesDropDown  { StateId = 6 ,StateName = "Diploma in .NET Technologies"},
                    new StatesDropDown  { StateId = 7 ,StateName = "Diploma in Java Technologies"},
                    new StatesDropDown  { StateId = 8 ,StateName = "Program for School leavers –  Diplomas for Rapid Employability"},
                    new StatesDropDown  { StateId = 9 ,StateName = "Program on Developing Windows Store Apps in HTML5 and JavaScript"},
                    new StatesDropDown  { StateId = 10 ,StateName = "Program on Mobile Apps Development for the Android Platform"},
                    new StatesDropDown  { StateId = 11 ,StateName = "Introduction to Windows Store App Development in HTML5 and JavaScript"},
                    new StatesDropDown  { StateId = 12 ,StateName = "Introduction to Windows Store App Development in C#"},
                    new StatesDropDown  { StateId = 13 ,StateName = "HTML5 Programming"},
                    new StatesDropDown  { StateId = 14 ,StateName = "Certificate Program on .NET Technologies"},
                    new StatesDropDown  { StateId = 15 ,StateName = "Developing Mobile Apps for the Android Platform"},
                    new StatesDropDown  { StateId = 16 ,StateName = "Web Design & Development"},
                    new StatesDropDown  { StateId = 17 ,StateName = "Diploma in Infrastructure Management"},
                    new StatesDropDown  { StateId = 18 ,StateName = "Certificate Program in Multimedia Skills"},
                    new StatesDropDown  { StateId = 19 ,StateName = "Certificate Program in MIS Skills"},
                    new StatesDropDown  { StateId = 20 ,StateName = "Certificate Program in Software Testing and Quality Assurance"},
                    new StatesDropDown  { StateId = 21 ,StateName = "Certificate Program in IT Infrastructure Library (ITIL®) Foundation Certification v3.0"},
                    new StatesDropDown  { StateId = 22 ,StateName = "Certificate Program in Certified Information Systems Auditor (CISA®)"},
                    new StatesDropDown  { StateId = 23 ,StateName = "Certificate Program in Project Management Professional (PMP®)"},
                    new StatesDropDown  { StateId = 24 ,StateName = "Certificate Program in Storage Technology"}
                };
            return new SelectList(list, "StateId", "StateName");
        }
        public static SelectList TimeSlotList()
        {
            var list = new[] 
                {
                    new StatesDropDown  { StateId = 1 ,StateName = "9 AM - 11 AM"},
                    new StatesDropDown  { StateId = 2 ,StateName = "12 PM - 2 PM"},
                    new StatesDropDown  { StateId = 3 ,StateName = "3 PM - 5 PM"}
                };
            return new SelectList(list, "StateId", "StateName");
        }
        #endregion
        #region interswitchng.com
        public ActionResult interswitchng()
        {
            switchngModels switchng = new switchngModels();
            switchng.BusinessTypeOptions = BusinessTypeList();
            switchng.PaymentTypeOptions = PaymentoptionList();
            switchng.InventoryStockValue = false;
            switchng.SuccessMessage = TempData["SuccessMessage"] as string;
            return View(switchng);
        }
        [HttpPost]
        public ActionResult interswitchng(switchngModels switchng)
        {
            string BusinessType = string.Empty, PaymentType = string.Empty, inventorystock = string.Empty;
            switchng.BusinessTypeOptions = BusinessTypeList();
            switchng.PaymentTypeOptions = PaymentoptionList();
            if (switchng.InventoryStockValue == true)
                inventorystock = "Yes";
            else
                inventorystock = "No";
            SelectList selecttStatus = new SelectList(switchng.BusinessTypeOptions, "Id", "Name");
            foreach (SelectListItem item in selecttStatus.Items)
            {
                if (item.Value == switchng.SelectedBusinessTypeId.ToString())
                {
                    BusinessType = item.Text;
                    break;
                }
            }
            SelectList selecttPayment = new SelectList(switchng.PaymentTypeOptions, "Id", "Name");
            foreach (SelectListItem item in selecttPayment.Items)
            {
                if (item.Value == switchng.SelectedBusinessTypeId.ToString())
                {
                    PaymentType = item.Text;
                    break;
                }
            }
            bool a = true;
            if (!string.IsNullOrEmpty(switchng.txtEmail))
            {
                Regex regEmail = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
                a = regEmail.IsMatch(switchng.txtEmail);
                if (Regex.Matches(switchng.txtEmail, "@").Count > 1 || (switchng.txtEmail.IndexOfAny(new char[] { '/', '%', '!', '#', '$', '^', '&', '*', '(', ')', '+', '=', '|', '{', '}', '[', ']', ':', ';', ',', '>', '<', '?', '~', '`', '"', '-' }) > 0) || Regex.Matches(switchng.txtEmail, "'").Count > 0)
                    a = false;
            }
            int result = 1, result1 = 0;
            if (!string.IsNullOrEmpty(switchng.txtPhone))
            {
                result = Vconnect.Common.Utility.isValidPhone(switchng.txtPhone);
                // result1 = ibtcphonechk(ibtc.txtphone);
            }
            if (string.IsNullOrEmpty(switchng.txtName))
            {
                ViewBag.Msg = "Enter your name";
            }
            else if (string.IsNullOrEmpty(switchng.txtPhone))
            {
                ViewBag.Msg = "Enter your phone number";
            }
            else if (result == 0)
            {
                ViewBag.Msg = "Invalid phone number";
            }
            //else if (result1 == 1)
            //{
            //    ViewBag.Msg = "Sorry, a business is already registered with this contact number. Please provide another valid phone number to continue registration";
            //}  
            else if (string.IsNullOrEmpty(switchng.txtEmail))
            {
                ViewBag.Msg = "Enter email-id";
            }
            else if (a == false)
            {
                ViewBag.Msg = "Invalid Email-Id";
            }
            else if (BusinessType == string.Empty)
            {
                ViewBag.Msg = "Select your business type and field";
            }
            else if (PaymentType == string.Empty)
            {
                ViewBag.Msg = "Select payment option";
            }
            else if (string.IsNullOrEmpty(switchng.txtInquiry))
            {
                ViewBag.Msg = "Enter your inquiry";
            }
            else
            {
                using (var db = new VconnectDBContext29())
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[prc_add_clientadds]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@clientname", "Interswitch Limited"));
                    cmd.Parameters.Add(new SqlParameter("@username", switchng.txtName.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@phoneno", switchng.txtPhone.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@emailid", switchng.txtEmail.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@query", switchng.txtInquiry.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@bizcategory", BusinessType));
                    cmd.Parameters.Add(new SqlParameter("@course", PaymentType));
                    cmd.Parameters.Add(new SqlParameter("@statuscurrent", inventorystock));
                    cmd.Parameters.Add(new SqlParameter("@source", "WEB"));
                    cmd.Parameters.Add(new SqlParameter("@Err", SqlDbType.VarChar, 100));
                    cmd.Parameters["@Err"].Direction = ParameterDirection.Output;
                    try
                    {
                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        cmd.ExecuteNonQuery();
                        TempData["SuccessMessage"] = "Your Inquiry submitted successfully!";

                        return RedirectToAction("interswitchng", "BizReg");
                    }
                    catch (Exception ex)
                    {
                        log.LogMe(ex);
                    }
                    finally
                    {
                        db.Database.Connection.Close();

                    }
                }
            }
            return View(switchng);
        }
        public static SelectList BusinessTypeList()
        {
            var list = new[] 
                {
                    new StatesDropDown  { StateId = 1 ,StateName = "Hotels "},
                    new StatesDropDown  { StateId = 2 ,StateName = "Supermarkets "},
                    new StatesDropDown  { StateId = 3 ,StateName = "Travel Agents"},
                    new StatesDropDown  { StateId = 4 ,StateName = "Resorts and recreation centers"},
                    new StatesDropDown  { StateId = 5 ,StateName = "Security companies"},
                    new StatesDropDown  { StateId = 6 ,StateName = "Restaurants/ Clubs / Lounges "},
                    new StatesDropDown  { StateId = 7 ,StateName = "All Retails"},
                    new StatesDropDown  { StateId = 8 ,StateName = "Laundry"},
                    new StatesDropDown  { StateId = 9 ,StateName = "Event Management/ shows"},
                    new StatesDropDown  { StateId = 10 ,StateName = "Malls"},
                    new StatesDropDown  { StateId = 11 ,StateName = "Hospitality companies"}
                };
            return new SelectList(list, "StateId", "StateName");
        }
        public static SelectList PaymentoptionList()
        {
            var list = new[] 
                {
                    new StatesDropDown  { StateId = 1 ,StateName = "Web"},
                    new StatesDropDown  { StateId = 2 ,StateName = "ATM"},
                    new StatesDropDown  { StateId = 3 ,StateName = "Mobile Phone"},
                    new StatesDropDown  { StateId = 4 ,StateName = "None"}
                };
            return new SelectList(list, "StateId", "StateName");
        }
        #endregion
    }
}
