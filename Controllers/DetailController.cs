﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vconnect.Models;
using Vconnect.Common;
using System.Configuration;
using System.Text.RegularExpressions;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Net;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using MailChimp.Types;
using System.Net.Mail;
using Vconnect.Enums;
using System.Text;
using System.Threading.Tasks;

namespace Vconnect.Controllers
{
    public class DetailController : Controller
    {
        UserSession objUS = new UserSession();
        public string webconfigSourceImagesRootPath = ConfigurationManager.AppSettings["SourceImagesRootPath"].ToString();
        public string webconfigDestinationImagesRootPath = ConfigurationManager.AppSettings["DestinationImagesRootPath"].ToString();
        public static Int32 BusinessId = 0;
        #region Business Details Page
        [ValidateInput(false)]
        public void Detailattachment(Int32 BID, string businessname)
        {

            BusinessId = BID;
            try
            {
                //long bizID;


                string newUrl = urlRedirectionBizDetail();
                if (newUrl != "")
                {
                    Response.Clear(); Response.Status = "301 Moved Permanently";
                    Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();
                }
                string returnVal = string.Empty;
                DetailWEBBiz bizDetailWEB = new DetailWEBBiz();
                string str = string.Empty;
                Int64 userID = 0;
                if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                {
                    userID = Convert.ToInt64(objUS.SV_VCUserContentID.ToString());
                }
                Detail bizDetails = bizDetailWEB.GetBizDetails(BID, userID);

                if (bizDetails == null || bizDetails.BizMaster == null || bizDetails.BizMaster.Count == 0)
                {
                    Response.Redirect("/");
                }
                else if (businessname != null && (businessname.ToString().Trim().Contains("viewattachments") || businessname.ToString().Trim().Contains("videogallery") || businessname.ToString().Trim().Contains("viewallreviews")))
                {

                    businessname = bizDetails.BizMaster.FirstOrDefault().businessname.Replace(" ", "-");
                    businessname = businessname + "-" + bizDetails.BizMaster.FirstOrDefault().city.Replace(" ", "_");
                    businessname = businessname + "-" + bizDetails.BizMaster.FirstOrDefault().state.Replace(" ", "-") + "_b" + BID;
                    //Response.Clear(); Response.Status = "301 Moved Permanently";
                    //Response.AddHeader("Location", businessname.Trim().ToLower()); Response.End();
                    Response.RedirectPermanent("~/" + businessname);

                }

            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }

        }


        [ValidateInput(false)]
        public void Detailrv(Int32 bizID, string businessname)
        {

            BusinessId = bizID;


            try
            {
                //long bizID;


                string newUrl = urlRedirectionBizDetail();
                if (newUrl != "")
                {
                    Response.Clear(); Response.Status = "301 Moved Permanently";
                    Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();
                }
                string returnVal = string.Empty;
                DetailWEBBiz bizDetailWEB = new DetailWEBBiz();
                string str = string.Empty;
                Int64 userID = 0;
                if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                {
                    userID = Convert.ToInt64(objUS.SV_VCUserContentID.ToString());
                }
                 Detail bizDetails = bizDetailWEB.GetBizDetails(bizID, userID);

                if (bizDetails == null || bizDetails.BizMaster == null || bizDetails.BizMaster.Count == 0)
                {
                    Response.Redirect("/");
                }
                else if (businessname != null && (businessname.ToString().Trim().Contains("viewattachments") || businessname.ToString().Trim().Contains("videogallery") || businessname.ToString().Trim().Contains("viewallreviews")))
                {

                    businessname = bizDetails.BizMaster.FirstOrDefault().businessname.Replace(" ", "-");
                    businessname = businessname + "-" + bizDetails.BizMaster.FirstOrDefault().city.Replace(" ", "_");
                    businessname = businessname + "-" + bizDetails.BizMaster.FirstOrDefault().state.Replace(" ", "-") + "_b" + bizID;
                    //Response.Clear(); Response.Status = "301 Moved Permanently";
                    //Response.AddHeader("Location", businessname.Trim().ToLower()); Response.End();
                    Response.RedirectPermanent("~/" + businessname);

                }

            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }

        }

        [ValidateInput(false)]
        public ActionResult getbusinessdetail(Int32? bizID, string businessname, string method)
        {
            if (bizID.HasValue)
            {
                BusinessId = bizID.Value;
                try
                {
                    //long bizID;
                    if (Request.Url.AbsoluteUri.ToString() != Request.Url.AbsoluteUri.ToString().ToLower())
                    {
                        Response.Clear(); Response.Status = "301 Moved Permanently";
                        Response.AddHeader("Location", Request.Url.AbsoluteUri.ToString().ToLower()); Response.End();
                        return new HttpStatusCodeResult(301, "301 Moved Permanently");
                    }

                    string newUrl = urlRedirectionBizDetail();
                    if (newUrl != "")
                    {
                        Response.Clear(); Response.Status = "301 Moved Permanently";
                        Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();
                    }
                    string returnVal = string.Empty;
                    DetailWEBBiz bizDetailWEB = new DetailWEBBiz();
                    string str = string.Empty;
                    Int32 userid = 0;
                    //if (Request.Url.AbsoluteUri.ToString().IndexOf("returnval") != -1 && Request.Url.AbsoluteUri.ToString().IndexOf("bizid") != -1)
                    if (Request.Url.AbsoluteUri.ToString().IndexOf("returnval") != -1 || Request.QueryString["returnVal"] != null)
                    {
                        //Int64 bizId = Convert.ToInt64(Request.QueryString["bizid"].ToString());
                        SearchListWEBController searchListWEBController = new SearchListWEBController();
                        // string bname = Request.QueryString["bname"].ToString();
                        if (Request.QueryString["returnval"].ToString().ToLower() == "save" || Request.QueryString["returnval"].ToString().ToLower() == "2")
                        {
                            //str = searchListWEBController.Favourite(bizId);
                            userid = (!string.IsNullOrEmpty(objUS.SV_VCUserContentID)) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                            ViewBag.returnVal = "save";

                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "like" || Request.QueryString["returnval"].ToString().ToLower() == "1")
                        {
                            //str = searchListWEBController.Like(bizId);
                            userid = (!string.IsNullOrEmpty(objUS.SV_VCUserContentID)) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                            ViewBag.returnVal = "like";
                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "claim" || Request.QueryString["returnval"].ToString().ToLower() == "4")
                        {
                            ViewBag.returnVal = "claim";
                            return Redirect("claimbusiness?bid=" + bizID);
                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "review" || Request.QueryString["returnval"].ToString().ToLower() == "5")
                        {
                            ViewBag.returnVal = "review";
                            userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "ishelpfull" || Request.QueryString["returnval"].ToString().ToLower() == "7" || Request.QueryString["returnval"].ToString().ToLower() == "8")
                        {
                            ViewBag.returnVal = "ishelpfull";
                            userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "IsReviewAbusive" || Request.QueryString["returnval"].ToString().ToLower() == "10")
                        {
                            ViewBag.returnVal = "IsReviewAbusive";
                            userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "follow" || Request.QueryString["returnval"].ToString().ToLower() == "6")
                        {
                            ViewBag.returnVal = "follow";
                            userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                            ViewBag.follow = "follow";
                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "photo" || Request.QueryString["returnval"].ToString().ToLower() == "9")
                        {
                            ViewBag.returnVal = "photo";
                            userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                            ViewBag.follow = "photo";
                        }
                        else if (Request.QueryString["returnval"] != null && Request.QueryString["contentidin"] != null && Request.QueryString["returnval"].ToString().ToLower() == "11")
                        {
                            ViewBag.returnVal = "businessimprove";
                            userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                            ViewBag.follow = "businessimprove";
                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "phone" || Request.QueryString["returnval"].ToString().ToLower() == "12")
                        {
                            ViewBag.returnVal = "phone";
                            userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                            ViewBag.follow = "phone";
                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "alternatephone" || Request.QueryString["returnval"].ToString().ToLower() == "13")
                        {
                            ViewBag.returnVal = "alternatephone";
                            userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                            ViewBag.follow = "alternatephone";
                        }
                    }
                    Int64 userID = 0;
                    if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                    {
                        userID = Convert.ToInt64(objUS.SV_VCUserContentID.ToString());
                    }
                    Detail bizDetails = bizDetailWEB.GetBizDetails(bizID.Value, userID);

                    if (bizDetails == null || bizDetails.BizMaster == null || bizDetails.BizMaster.Count == 0)
                    {
                        if (bizDetails.BusinessURL != null && !string.IsNullOrEmpty(bizDetails.BusinessURL.url))
                        {
                            return Redirect(ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + bizDetails.BusinessURL.url);
                        }
                        Response.Clear(); Response.StatusCode = 404;
                        Response.Status = "404 NotFound";
                        return View("../Static/404");
                    }


                    bizDetails.SearchLocation = "Lagos";
                    bizDetails.StateOptions = Utility.GetSelectList();
                    bizDetails.BizMaster.FirstOrDefault().CityOptions = Utility.GetSelectListCity(bizDetails.BizMaster.FirstOrDefault().stateid.Value);
                    bizDetails.workinghourlist = Utility.GetWorkingHours();
                    // AddBusinessLog(bizID.Value, businessname);
                    //if (bizDetails.BizMaster.FirstOrDefault().isstore == 1)
                    //{
                    //    return View("detail#st-section", bizDetails);
                    //}
                    //else
                    //{
                    return Json(bizDetails, JsonRequestBehavior.AllowGet);
                   // return View("detail", bizDetails);
                    //}
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }



        [ValidateInput(false)]
        public ActionResult detail(Int32? bizID, string businessname, string method)
        {
            if (bizID.HasValue)
            {
                BusinessId = bizID.Value;
                try
                {
                    //long bizID;
                    if (Request.Url.AbsoluteUri.ToString() != Request.Url.AbsoluteUri.ToString().ToLower())
                    {
                        Response.Clear(); Response.Status = "301 Moved Permanently";
                        Response.AddHeader("Location", Request.Url.AbsoluteUri.ToString().ToLower()); Response.End();
                        return new HttpStatusCodeResult(301, "301 Moved Permanently");
                    }

                    string newUrl = urlRedirectionBizDetail();
                    if (newUrl != "")
                    {
                        Response.Clear(); Response.Status = "301 Moved Permanently";
                        Response.AddHeader("Location", newUrl.Trim().ToLower()); Response.End();
                    }
                    string returnVal = string.Empty;
                    DetailWEBBiz bizDetailWEB = new DetailWEBBiz();
                    string str = string.Empty;
                    Int32 userid = 0;
                    //if (Request.Url.AbsoluteUri.ToString().IndexOf("returnval") != -1 && Request.Url.AbsoluteUri.ToString().IndexOf("bizid") != -1)
                    if (Request.Url.AbsoluteUri.ToString().IndexOf("returnval") != -1 || Request.QueryString["returnVal"] != null)
                    {
                        //Int64 bizId = Convert.ToInt64(Request.QueryString["bizid"].ToString());
                        SearchListWEBController searchListWEBController = new SearchListWEBController();
                        // string bname = Request.QueryString["bname"].ToString();
                        if (Request.QueryString["returnval"].ToString().ToLower() == "save" || Request.QueryString["returnval"].ToString().ToLower() == "2")
                        {
                            //str = searchListWEBController.Favourite(bizId);
                            userid = (!string.IsNullOrEmpty(objUS.SV_VCUserContentID)) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                            ViewBag.returnVal = "save";

                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "like" || Request.QueryString["returnval"].ToString().ToLower() == "1")
                        {
                            //str = searchListWEBController.Like(bizId);
                            userid = (!string.IsNullOrEmpty(objUS.SV_VCUserContentID)) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                            ViewBag.returnVal = "like";
                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "claim" || Request.QueryString["returnval"].ToString().ToLower() == "4")
                        {
                            ViewBag.returnVal = "claim";
                            return Redirect("claimbusiness?bid=" + bizID);
                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "review" || Request.QueryString["returnval"].ToString().ToLower() == "5")
                        {
                            ViewBag.returnVal = "review";
                            userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "ishelpfull" || Request.QueryString["returnval"].ToString().ToLower() == "7" || Request.QueryString["returnval"].ToString().ToLower() == "8")
                        {
                            ViewBag.returnVal = "ishelpfull";
                            userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "IsReviewAbusive" || Request.QueryString["returnval"].ToString().ToLower() == "10")
                        {
                            ViewBag.returnVal = "IsReviewAbusive";
                            userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "follow" || Request.QueryString["returnval"].ToString().ToLower() == "6")
                        {
                            ViewBag.returnVal = "follow";
                            userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                            ViewBag.follow = "follow";
                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "photo" || Request.QueryString["returnval"].ToString().ToLower() == "9")
                        {
                            ViewBag.returnVal = "photo";
                            userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                            ViewBag.follow = "photo";
                        }
                        else if (Request.QueryString["returnval"] != null && Request.QueryString["contentidin"] != null && Request.QueryString["returnval"].ToString().ToLower() == "11")
                        {
                            ViewBag.returnVal = "businessimprove";
                            userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                            ViewBag.follow = "businessimprove";
                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "phone" || Request.QueryString["returnval"].ToString().ToLower() == "12")
                        {
                            ViewBag.returnVal = "phone";
                            userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                            ViewBag.follow = "phone";
                        }
                        else if (Request.QueryString["returnval"].ToString().ToLower() == "alternatephone" || Request.QueryString["returnval"].ToString().ToLower() == "13")
                        {
                            ViewBag.returnVal = "alternatephone";
                            userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                            if (userid == 0)
                            {
                                return Redirect(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("?")));
                            }
                            ViewBag.follow = "alternatephone";
                        }
                    }
                    Int64 userID = 0;
                    if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                    {
                        userID = Convert.ToInt64(objUS.SV_VCUserContentID.ToString());
                    }
                    Detail bizDetails = bizDetailWEB.GetBizDetails(bizID.Value, userID);

                    if (bizDetails == null || bizDetails.BizMaster == null || bizDetails.BizMaster.Count == 0)
                    {
                        if (bizDetails.BusinessURL != null && !string.IsNullOrEmpty(bizDetails.BusinessURL.url))
                        {
                            return Redirect(ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + bizDetails.BusinessURL.url);
                        }
                        Response.Clear(); Response.StatusCode = 404;
                        Response.Status = "404 NotFound";
                        return View("../Static/404");
                    }
                    
                    
                    bizDetails.SearchLocation = "Lagos";
                    bizDetails.StateOptions = Utility.GetSelectList();
                    bizDetails.BizMaster.FirstOrDefault().CityOptions = Utility.GetSelectListCity(bizDetails.BizMaster.FirstOrDefault().stateid.Value);
                    bizDetails.workinghourlist = Utility.GetWorkingHours();
                   // AddBusinessLog(bizID.Value, businessname);
                    //if (bizDetails.BizMaster.FirstOrDefault().isstore == 1)
                    //{
                    //    return View("detail#st-section", bizDetails);
                    //}
                    //else
                    //{
                    
                        return View("detail", bizDetails);
                    //}
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }
        #endregion
        public string urlRedirectionBizDetail()
        {
            string WebsiteRawPath = ConfigurationManager.AppSettings["WebsiteRawPath"].ToString();
            //SEO ISSUE Duplicacy issue      --http://localhost:1202/code/nigeria/businesses-from-power-up-limited-ikeja-lagos_b250
            if (System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("businesses-from-") != -1 && System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("_b") != -1)
            {
                int istartLength = 0;
                string newUrl = System.Web.HttpContext.Current.Request.RawUrl.ToLower();
                istartLength = newUrl.ToLower().IndexOf("businesses-from-");
                newUrl = (newUrl.Substring(istartLength).ToString());
                return WebsiteRawPath + newUrl.Replace("businesses-from-", "/");


            }
            //www.vconnect.com/business/united-bank-for-africa-plc-lagos_island-lagos_b146757
            else if (System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("business/") != -1 && System.Web.HttpContext.Current.Request.RawUrl.ToLower().IndexOf("_b") != -1)
            {
                string newUrl = System.Web.HttpContext.Current.Request.RawUrl.ToLower();
                return WebsiteRawPath + newUrl.Replace("business/", "");


            }
            else
            {
                return string.Empty;
            }
        }
        #region Similar Businesses
        public ActionResult RelatedBiz(int? bizid)
        {
            //if (bizid.HasValue)// && Request.ContentType.IndexOf("json") > 0)
            if (bizid.HasValue) // && (!Request.AcceptTypes.Contains("text/html"))  // && Request.ContentType.IndexOf("json") > 0)
            {  
                DetailWEBBiz bizDetailWEB = new DetailWEBBiz();
                var similar = bizDetailWEB.GetRelatedBiz(bizid);
                var result = similar.Select(m => new { bizname = m.businessname, bizid = m.businessid, rate = m.avgrating, businessurl = m.businessurl, businessLogo = m.companylogo });

               string  Html = "<div class='similar-businesses'>";
                    Html += "   <h5 class='green'>Similar Businesses</h5>";
                    int recordCount = 0, i=0;
                    recordCount = result.Count();
                    if (recordCount > 3) { recordCount = 3; }

                    foreach (var item in result) {

                        Html += "   <div class='sbusiness'>";

                        if (i == 0) {
                            if (item.businessLogo == "") {
                                Html += "<img class='left' src='"+ Vconnect.Common.MyGlobalVariables.Staticimage + "lab-small.png' alt='"+item.bizname+"'/>";
                            }
                            else {
                                Html += "<img class='left' src='" + Vconnect.Common.MyGlobalVariables.ImagesRootPath + item.businessLogo + "' alt='" + item.bizname + "' width='50' height='55'/>";
                            }
                        }
                        else if (i == 1) {
                            if (item.businessLogo == "") {
                                Html += "<img class='left' src='" + Vconnect.Common.MyGlobalVariables.Staticimage + "lab-small2.png' alt='" + item.bizname + "'/>";
                            }
                            else {
                                Html += "<img class='left' src='" + Vconnect.Common.MyGlobalVariables.ImagesRootPath + item.businessLogo + "' alt='" + item.bizname + "' width='50' height='55'/>";
                            }
                        }
                        else if (i == 2) {
                            if (item.businessLogo == "") {
                                Html += "<img class='left' src='" + Vconnect.Common.MyGlobalVariables.Staticimage + "lab-small3.png' alt='" + item.bizname + "'/>";
                            }
                            else {
                                Html += "<img class='left' src='" + Vconnect.Common.MyGlobalVariables.ImagesRootPath + item.businessLogo + "' alt='" + item.bizname + "' width='50' height='55'/>";
                            }
                        }
                        else {
                            Html += "<img class='left' src='" + Vconnect.Common.MyGlobalVariables.Staticimage + "lab-small3.png' alt='@Model.BizMaster.FirstOrDefault().businessname'/>";
                        }
                Html += "<div class='business-name'>";
                        //Html += '        <strong><a href="../BizDetail/BizDetail?bizID=' + result[i].bizid + ''>' + result[i].bizname + '</a></strong><br>'
                Html += "<strong><a href='" + item.businessurl + "'  onClick='ga('send', 'event', 'BizDInteraction', 'IconClick', 'SimilarBusiness');'>" + item.bizname + "</a></strong><br>";
                        //
                if (item.rate == null) {
                    Html += "<span class='star-rating'>" + Vconnect.Common.Utility.RateMe(0) + "</span>";
                }
                else {
                    Html += "<span class='star-rating'>" + Vconnect.Common.Utility.RateMe(item.rate) + "</span>";
                }
                Html += "</div>";
                Html += "</div>";
                        i++;
            }
            Html += "</div>";

            return Json(Html, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region Report Incorect
        public ActionResult RepotIncorrect(string remarks, string whatswrong, string detail, string email, int? bizId)
        {
            //if ((!Request.AcceptTypes.Contains("text/html")))
            //{
                try
                {

                    int userid = 0;
                    userid = (!string.IsNullOrEmpty(objUS.SV_VCUserContentID)) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                    if (bizId.HasValue)// && Request.ContentType.IndexOf("json") > 0)
                    {
                        DetailWEBBiz bizDetailWEB = new DetailWEBBiz();
                        DetailInfoBiz bizDetailInfo = new DetailInfoBiz();
                        bizDetailInfo = bizDetailWEB.GetBizDetailInfo(bizId.Value);
                        int result = bizDetailWEB.ReportIncorrect(remarks, whatswrong, detail, email, bizId.Value, userid);
                        if (result != 0)
                        {
                            return Json("error", JsonRequestBehavior.AllowGet);
                        }
                        EmailTOAdminForIncorrectReport(bizDetailInfo.businessname, email, detail, email);
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
                return Json("Informatsion is added", JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            //    return Json(null,JsonRequestBehavior.AllowGet);
            //}
        }
        public ActionResult ImproveBizInfo(Dictionary<string, string> ImproveBiz, string business, string url, bool flag)
        {
            //if ((!Request.AcceptTypes.Contains("text/html")))
            //{
                List<ImproveBiz> ImproveBizset = new List<ImproveBiz>();
                List<WorkingHourBiz> WorkingHourBiz = new List<WorkingHourBiz>();
                ImproveBizset.Add(new ImproveBiz()
                {
                    userid = (!string.IsNullOrEmpty(objUS.SV_VCUserContentID)) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0,
                    businessid = ImproveBiz["businessid"],
                    bname = ImproveBiz["bname"],
                    bemail = ImproveBiz["bemail"],
                    bwebsite = ImproveBiz["bwebsite"],
                    bphone = ImproveBiz["bphone"],
                    baphone = ImproveBiz["baphone"],
                    baddress = ImproveBiz["baddress"],
                    bdescription = ImproveBiz["bdescription"],
                    bproducts = ImproveBiz["bproducts"],
                    yemailadd = ImproveBiz["yemailadd"],
                    isybiz = ImproveBiz["isybiz"],

                    blga = ImproveBiz["blga"],
                    blgaid = (!string.IsNullOrEmpty(ImproveBiz["isybiz"])) ? int.Parse(ImproveBiz["blgaid"].ToString()) : 0,
                    bstate = ImproveBiz["bstate"],
                    bstateid = (!string.IsNullOrEmpty(ImproveBiz["bstateid"])) ? int.Parse(ImproveBiz["bstateid"].ToString()) : 0,
                    contentidin = int.Parse(ImproveBiz["contentidin"])
                });
                WorkingHourBiz.Add(new WorkingHourBiz()
                {
                    mondaystart = ImproveBiz["Mon_Start"],
                    mondayend = ImproveBiz["Mon_End"],
                    tuesdaystart = ImproveBiz["Tues_Start"],
                    tuesdayend = ImproveBiz["Tues_End"],
                    wednesdaystart = ImproveBiz["Wed_Start"],
                    wednesdayend = ImproveBiz["Wed_End"],
                    thursdaystart = ImproveBiz["Thur_Start"],
                    thursdayend = ImproveBiz["Thur_End"],
                    fridaystart = ImproveBiz["Fri_Start"],
                    fridayend = ImproveBiz["Fri_End"],
                    saturdaystart = ImproveBiz["Sat_Start"],
                    saturdayend = ImproveBiz["Sat_End"],
                    sundaystart = ImproveBiz["Sun_Start"],
                    sundayend = ImproveBiz["Sun_End"]
                });
                DetailWEBBiz bizDetailWEB = new DetailWEBBiz();
                ImproveBizInfoAddGet detailimprove = new ImproveBizInfoAddGet();
                detailimprove.ImproveBizset = ImproveBizset;
                detailimprove.WorkingHourBiz = WorkingHourBiz;
                List<WorkingHourBiz> bizwrkinghrstemp = (List<WorkingHourBiz>)TempData.Peek("workinghours");
                List<ProductList> ProductListtemp = (List<ProductList>)TempData.Peek("selectproduct");
                int a1 = 1, a2 = 1, a3 = 1;
                if (flag == false)
                {
                    if (ProductListtemp != null)
                    {
                        var bproducts = (ImproveBiz["bproducts"]).Split(',');
                        var list1 = bproducts.ToList();
                        var list2 = (from m in ProductListtemp select m.productid).ToList();
                        var list3 = list1.Except(list2).ToList();
                        var list4 = list2.Except(list1).ToList();
                        if (list1.Count == list2.Count && (list3.Count == 0 && list4.Count == 0))
                        {
                            flag = false;
                            a1 = 0;
                        }
                        else if (list3.Count <= 0 && list4.Count <= 0)
                        {
                            flag = false;
                            a1 = 0;
                        }
                    }
                    if (bizwrkinghrstemp != null && bizwrkinghrstemp.Count>0)
                    {
                        if (bizwrkinghrstemp.FirstOrDefault().mondaystart == ImproveBiz["Mon_Start"] && bizwrkinghrstemp.FirstOrDefault().mondayend == ImproveBiz["Mon_End"]
                        && bizwrkinghrstemp.FirstOrDefault().tuesdaystart == ImproveBiz["Tues_Start"] && bizwrkinghrstemp.FirstOrDefault().tuesdayend == ImproveBiz["Tues_End"]
                        && bizwrkinghrstemp.FirstOrDefault().wednesdaystart == ImproveBiz["Wed_Start"] && bizwrkinghrstemp.FirstOrDefault().wednesdayend == ImproveBiz["Wed_End"]
                        && bizwrkinghrstemp.FirstOrDefault().thursdaystart == ImproveBiz["Thur_Start"] && bizwrkinghrstemp.FirstOrDefault().thursdayend == ImproveBiz["Thur_End"]
                        && bizwrkinghrstemp.FirstOrDefault().fridaystart == ImproveBiz["Fri_Start"] && bizwrkinghrstemp.FirstOrDefault().fridayend == ImproveBiz["Fri_End"]
                        && bizwrkinghrstemp.FirstOrDefault().saturdaystart == ImproveBiz["Sat_Start"] && bizwrkinghrstemp.FirstOrDefault().saturdayend == ImproveBiz["Sat_End"]
                        && bizwrkinghrstemp.FirstOrDefault().sundaystart == ImproveBiz["Sun_Start"] && bizwrkinghrstemp.FirstOrDefault().sundayend == ImproveBiz["Sun_End"])
                        {
                            flag = false;
                            a2 = 0;
                        }
                    }
                    if (ImproveBizset.FirstOrDefault().isybiz == "0")
                    {
                        flag = false;
                        a3 = 0;
                    }
                }
                int claimed1 = 0;
                int datareturn = 0;
                if (a1 == 1 || a2 == 1 || a3 == 1)
                    flag = true;
                if (flag == true)
                {
                    int result = bizDetailWEB.ImproveBizInfoADDget(detailimprove, out claimed1);
                    if (result > 0 && ImproveBizset.FirstOrDefault().userid == 0)
                    {
                        datareturn = result;
                    }
                    else
                    {
                        datareturn = 1;
                        if (claimed1 == 0 && !string.IsNullOrEmpty(ImproveBizset.FirstOrDefault().yemailadd) && ImproveBizset.FirstOrDefault().isybiz == "1")
                        {
                            datareturn = 2;
                        }
                    }
                    //if (claimed1 == 0 && !string.IsNullOrEmpty(ImproveBizset.FirstOrDefault().yemailadd) && ImproveBizset.FirstOrDefault().isybiz=="1")
                    //{
                    //    SendEmailToUserBizInfo(ImproveBizset.FirstOrDefault().yemailadd, int.Parse(ImproveBizset.FirstOrDefault().businessid), business, url);
                    //}
                    return Json(datareturn, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(4, JsonRequestBehavior.AllowGet);
                }

            //}
            //else
            //{
            //    return Json(null, JsonRequestBehavior.AllowGet);
            //}
        }
        #endregion
        #region Rate and Review
        [ValidateInput(false)]
        public ActionResult AddRating_Review(string userid, string bizid, string review, string businessname, Int16? rate, int share=0)
        {
            //if ((!Request.AcceptTypes.Contains("text/html")))
            //{
                string OUTBusinessReviewContentID = string.Empty;
                string result = string.Empty;
                if (Session["rturl"] != null && Session["rturl"] != "" && Session["rturl"].ToString().ToLower().IndexOf("reviewtext") != -1)
                {
                    Session["rturl"] = "";
                }
                try
                {
                    if (rate == null || rate == 0)
                        OUTBusinessReviewContentID = "-1";
                    else if (review != null && review != "" && review != " ")
                    {
                        DetailWEBBiz bizDetailWEB = new DetailWEBBiz();
                        OUTBusinessReviewContentID = bizDetailWEB.addbusinessreviews(Convert.ToInt64(bizid), Convert.ToInt64(userid), review, businessname, share);
                        result = bizDetailWEB.addbusinessRate(Convert.ToInt32(bizid), Convert.ToInt32(userid), rate, Convert.ToInt32(OUTBusinessReviewContentID));
                    }

                    else
                        OUTBusinessReviewContentID = "0";
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
                return Json(OUTBusinessReviewContentID, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            //    return Json(null, JsonRequestBehavior.AllowGet);
            //}
        }
        
        public ActionResult BussinesReview(long? bizID, string tab, int page=1, int sort = 0)
        {
            Int64 userID = 0;
            if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
            {
                userID = Convert.ToInt64(objUS.SV_VCUserContentID.ToString());
            }
            if (bizID.HasValue) // && (!Request.AcceptTypes.Contains("text/html"))  // && Request.ContentType.IndexOf("json") > 0)
            {
                DetailWEBBiz bizDetailWEB = new DetailWEBBiz();
                var rowsPerPage = 5;
                if (tab == "overview-section")
                {
                    rowsPerPage = 5;
                }
                else
                {
                    rowsPerPage = 10;
                }
                var data = bizDetailWEB.GetBizReviewDetails(bizID.Value, userID, page, rowsPerPage, sort);
                ////RedirectToAction("BusinessDetail", "BizDetailWEB"); 
                //string htmlcode = "";
                //htmlcode += "";
                //int i = 1;
                //foreach (var dataslt in data)
                //{
                //    htmlcode += @"<li>";
                //    htmlcode += @"<div class='review-item'>";
                //    htmlcode += @"<div class='review-heading clearfix'>";
                //    htmlcode += @"<div class='user-photo-wrapper'>";
                //    htmlcode += @"<img src='" + Vconnect.Common.MyGlobalVariables.ImagesRootPath + "" + dataslt.UserPhoto + "' alt='" + dataslt.UserName + "'>";
                //    htmlcode += @"</div>";
                //    htmlcode += @"<div class='heading-actions right'>";
                //    if (dataslt.isfollowed != 0 && dataslt.isfollowed != null)
                //    {
                //        htmlcode += @"<a id='Follow_" + dataslt.userid + "_" + i + "' href='#' class='button unfollow-btn' onclick='Follow(" + dataslt.userid + "," + userID + "," + dataslt.isfollowed + ");'><i class='icon-ok'></i>Following</a>";
                //    }
                //    else
                //    {
                //        htmlcode += @"<a id='Follow_" + dataslt.userid + "_" + i + "' href='#' class='btn' onclick='Follow(" + dataslt.userid + "," + userID + ",0);'><i class='icon-user-add'></i>Follow</a>";
                //    }
                //    htmlcode += @"</div>";
                //    htmlcode += @"<h4 class='username'><a href='/user/" + dataslt.userurl + "'>" + dataslt.UserName + "</a></h4>";
                //    htmlcode += @"<div class='user-reviews-count'>" + dataslt.followedby + " followers &middot; " + dataslt.reviewCount + " reviews</div>";
                //    htmlcode += @"</div>";
                //    htmlcode += @"<div class='review-body'>";
                //    htmlcode += @"<div class='star-rating'>";
                //    for (int i1 = 1; i1 <= dataslt.rating; i1++)
                //    {
                //        htmlcode += @"<i class='icon-star'></i>";
                //    }
                //    for (int j = 1; j <= (5 - dataslt.rating); j++)
                //    {
                //        htmlcode += @"<i class='icon-star-empty'></i>";
                //    }
                //    //htmlcode += @" &middot; 24 minutes ago</div>";
                //    htmlcode += @" &middot; " + dataslt.createddate + "</div>";
                //    htmlcode += @"<p>" + dataslt.reviewdetail + "</p>";
                //    htmlcode += @"</div>";
                //    htmlcode += @"<div class='review-footer'>";
                //    htmlcode += @"<div class='actions'><a href='#'><i class='icon-heart'></i></a> <a href='#'><i class='icon-share'></i></a> &middot; " + dataslt.likes + " likes</div>";
                //    htmlcode += @"</div>";
                //    htmlcode += @"</div>";
                //    htmlcode += @"</li>";
                //    if (data.Count > 3 && i == 3 && tab == "overview-section")
                //    {
                //        break;
                //    }
                //    if (i == 10)
                //        break;
                //    i++;
                //}
                //int count = 0;
                //count = data.Count;
                //var result = new { htmlcode = htmlcode, count = count };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult wasthishelpful(string whichnumber, string status, Int64? bizid)
        {
            if (bizid.HasValue)
            {
                Int64 userid = 0;
                DetailWEBBiz bizDetailWEB = new DetailWEBBiz();
                if (Request.Cookies != null && !string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                {
                    userid = Convert.ToInt64(objUS.SV_VCUserContentID);
                }
                int result = bizDetailWEB.wasthishelpful(whichnumber, status, bizid.Value, userid);
                if (result == 2)
                {
                    return Json(2, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult IsHelpFull(Int64 bizId, Int64 reviewId, int? userId, int? ishelp)
        {
            //if ((!Request.AcceptTypes.Contains("text/html")))
            //{
                DetailWEBBiz bizDetailWEB = new DetailWEBBiz();
                userId = Convert.ToInt32(objUS.SV_VCUserContentID);
                bizDetailWEB.IsHelpFull(bizId, reviewId, userId, ishelp);
                if (ishelp == 1)
                {
                    return Json("HelpFull", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("InAccurate", JsonRequestBehavior.AllowGet);
                }
            //}
            //else
            //{
            //    return Json(null, JsonRequestBehavior.AllowGet);
            //}
        }
        public ActionResult IsReviewAbusive(Int64 bizId, Int64 reviewId, int? userId, int? isabusive)
        {
            //if ((!Request.AcceptTypes.Contains("text/html")))
            //{
                DetailWEBBiz bizDetailWEB = new DetailWEBBiz();
                userId = Convert.ToInt32(objUS.SV_VCUserContentID);
                bizDetailWEB.IsReviewAbusive(bizId, reviewId, userId, isabusive);
                return Json("Abusive", JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            //    return Json(null, JsonRequestBehavior.AllowGet);
            //}
        }
        #endregion
        #region GetDirection
        [HttpGet]
        public ActionResult GetDirection(long? bizid)
        {
            if (bizid.HasValue)
            {
                DetailWEBBiz bizDetailWEB = new DetailWEBBiz();
                Detail bizDetails = bizDetailWEB.GetBizDetails(bizid.Value);
                if (bizDetails.BizMaster != null && bizDetails.BizMaster.Count > 0)
                {
                    //return RedirectPermanent("getdirection?bizid=" + bizID);
                    //var Address = bizDetails.BizMaster.FirstOrDefault();
                    //ViewBag.BizAddress = Address.address1 + " , " + Address.area + " , " + Address.city + " , " + Address.state;
                    return View(bizDetails.BizMaster.FirstOrDefault());
                }
                else
                {
                    //return Redirect("/"); //bizID not present in business Master
                    Response.Clear(); Response.StatusCode = 404;
                    Response.Status = "404 NotFound";
                    return View("../Static/404");
                }
            }
            else
            {
                //return Redirect("/"); //bizID is null
                Response.Clear(); Response.StatusCode = 404;
                Response.Status = "404 NotFound";
                return View("../Static/404");
            }
        }
        #endregion
        #region Photo-Gallery
        public ActionResult BussinesImage(long? bizID, string tab, int page = 1)
        {
            if (bizID.HasValue) // && (!Request.AcceptTypes.Contains("text/html"))//  && Request.ContentType.IndexOf("json") > 0)
            {
                DetailWEBBiz bizDetailWEB = new DetailWEBBiz();
                var rowsPerPage = 5;
                if (tab == "overview-section")
                {
                    rowsPerPage = 10;
                }
                else
                {
                    rowsPerPage = 20;
                }
                var data = bizDetailWEB.GetPhotoGallery(bizID.Value, page, rowsPerPage);
                //string htmlcode = "";
                //if (tab == "overview-section")
                //{
                //    int k = 0;
                //    foreach (var photo in data)
                //    {
                //        if (k == 7)
                //            break;
                //        htmlcode += @"<li>";
                //        htmlcode += @"<div class='thumb'>";
                //        htmlcode += @"<a class='th' data-lightbox='biz-photos' href='" + Vconnect.Common.MyGlobalVariables.ImagesRootPath + photo.photosmax + "'><img src='" + Vconnect.Common.MyGlobalVariables.ImagesRootPath + photo.photothumbs + "' alt='" + photo.title + "'></a>";
                //        htmlcode += @"</div>";
                //        htmlcode += @"</li>";
                //        k++;
                //    }
                //}
                //else
                //{
                //    int k2 = 1;
                //    foreach (var photo in data)
                //    {
                //        htmlcode += @"<li>";
                //        htmlcode += @"<div class='thumb'>";
                //        string path = Vconnect.Common.MyGlobalVariables.ImagesRootPath + photo.photothumbs;
                //       // HttpWebRequest request = WebRequest.Create(path) as HttpWebRequest;
                //        //Image imageCircle = Image.FromFile(path);
                //        //if (Directory.Exists(path) || System.IO.File.Exists(path))
                //        //{
                //            htmlcode += @"<a class='th' data-lightbox='biz-photos' href='" + Vconnect.Common.MyGlobalVariables.ImagesRootPath + photo.photosmax + "'><img src='" + Vconnect.Common.MyGlobalVariables.ImagesRootPath + photo.photothumbs + "' alt='" + photo.title + "'></a>";
                //        //}
                //        //else
                //        //{
                //        //    htmlcode += @"<a class='th' data-lightbox='biz-photos' href='http://static.vconnect.co/img/profile-photo.jpg'><img src='http://static.vconnect.co/img/profile-photo.jpg' alt='" + photo.title + "'></a>";
                //        //}
                //        htmlcode += @"</div>";
                //        htmlcode += @"</li>";
                //        if (k2 == 20)
                //            break;
                //        k2++;
                //    }
                //}
                //int count = 0;
                //count = data.Count;
                //var result = new { htmlcode = htmlcode, count = count};
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        //public ActionResult GetWorking_hours(long? bizID)
        //{
        //    if (bizID.HasValue && (!Request.AcceptTypes.Contains("text/html")))//  && Request.ContentType.IndexOf("json") > 0)
        //    {
        //        DetailWEBBiz bizDetailWEB = new DetailWEBBiz();
        //        var data = bizDetailWEB.GetWorking_hours(bizID.Value);
        //        return Json(data, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        return Json(null, JsonRequestBehavior.AllowGet);
        //    }
        //}
        public ActionResult GetWorking_hours(long? bizID, string from)
        {
            //from=0 means select 3 table i.e-working hours,product list and service list ...bind in overview section
            //from=1 means select 2 table i.e-working hours and product list ...bind in improve business
            //from=2 means select 1 table working hours ....i.e-checked working hours any change when user approve data

            if (bizID.HasValue) // && (!Request.AcceptTypes.Contains("text/html"))  //  && Request.ContentType.IndexOf("json") > 0)
            {
                DetailWEBBiz bizDetailWEB = new DetailWEBBiz();
                ImproveBizInfoData ImproveBizInfoData = new ImproveBizInfoData();
                ImproveBizInfoData = bizDetailWEB.GetProductServiceWork(bizID.Value, from);
                var worklist = ImproveBizInfoData.WorkingHourBiz.ToList();
                TempData["workinghours"] = ImproveBizInfoData.WorkingHourBiz.ToList();
                TempData.Keep("workinghours");
                //var productlist = (from m in ImproveBizInfoData.ProductList where m.active == "0" select m).ToList();
                if (from == "1")
                {
                    var productlist = (from m in ImproveBizInfoData.ProductList select m).ToList();
                    var selectproduct = (from m in ImproveBizInfoData.ProductList where m.active == "1" select m.productid).ToArray();
                    var result = new { worklist = worklist, productlist = productlist, selectproduct = selectproduct };
                    TempData["selectproduct"] = (from m in ImproveBizInfoData.ProductList where m.active == "1" select m).ToList();
                    TempData.Keep("selectproduct");
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else if (from == "2")
                {
                    var result = new { worklist = worklist };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var servicelist = (from m in ImproveBizInfoData.ServiceBind select m).ToList();
                    var productlist = (from m in ImproveBizInfoData.ProductBind select m).ToList();
                    var result = new { worklist = worklist, servicelist = servicelist, productlist = productlist };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveUploadedFile(int bizid)
        {
            //if ((!Request.AcceptTypes.Contains("text/html")))
            //{
                bool isSavedSuccessfully = true;
                string imgName = string.Empty;
                try
                {
                    foreach (string fileName in Request.Files)
                    {
                        HttpPostedFileBase file = Request.Files[fileName];
                        //Save file content goes here
                        //int id = biz.id;
                        //string image = biz.image;
                        DetailWEBBiz bizDetailWEB = new DetailWEBBiz();
                        //BusinessId = Convert.ToInt32(Request.QueryString["bizid"]);
                        if (bizid != null && bizid > 0)
                        {
                            BusinessId = bizid;
                        }
                        else
                        {
                            BusinessId = Convert.ToInt32(Request.Url.AbsoluteUri.ToString().Substring(Request.Url.AbsoluteUri.ToString().LastIndexOf('=') + 1));
                        }
                        Detail bizDetails = bizDetailWEB.GetBizDetails(BusinessId);
                        string businessname = bizDetails.BizMaster.FirstOrDefault().businessname;
                        int contentid = bizDetails.BizMaster.FirstOrDefault().contentid;
                        Int32 userid = 0;
                        if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                            userid = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                        int MaxContentLength = 1024 * 1024 * 5; //3 MB
                        int imagelength = int.Parse(file.ContentLength.ToString());
                        string[] AllowedFileExtensions = new string[] { ".jpeg", ".jpg", ".gif", ".png", ".bmp" };
                        if (!AllowedFileExtensions.Contains((file.FileName.Substring(file.FileName.LastIndexOf('.')).ToLower())))
                        {
                            //ModelState.AddModelError("File", "Please file of type: " + string.Join(", ", AllowedFileExtensions));
                            return Json(new { Message = "Invaild file extension" });
                        }
                        else if (imagelength > MaxContentLength)
                        {
                            //ModelState.AddModelError("File", "Your file is too large, maximum allowed size is: " + MaxContentLength + " MB");
                            return Json(new { Message = "Your file is too large, maximum allowed size is: " + MaxContentLength + " MB" });
                        }
                        else
                        {
                            //string finalfilename = renameUploadFile(file, 0);
                            //TO:DO
                            //uploads/photogallery/first-bank-of-nigeria-plc_thumb_55013122.jpg
                            imgName = (DateTime.Now.ToString("dd-mm-yyyy-hh-mm-ss-tt") + file.FileName.Replace(" ", "_").Replace("#", "_").Replace("%", "_").Replace("^", "_").Replace("'", "_")).ToString();
                            var path = "vcsites/vcimages/resource/uploads/photogallery/";
                           
                            //public string webconfigSourceImagesRootPath = ConfigurationManager.AppSettings["SourceImagesRootPath"].ToString();
                            //public string webconfigDestinationImagesRootPath = ConfigurationManager.AppSettings["DestinationImagesRootPath"].ToString();
                            //CopyImage.Sync(path, @"C:\vcsites\vcimages\Resource\uploads\photogallery");
                            Utility.createCookieBizPhoto(Convert.ToInt32(BusinessId), imgName);
                            bizDetailWEB.addbusinessphotos(BusinessId, contentid, userid, businessname, ("uploads/photogallery/" + imgName).ToString().Trim());
                            ImageConverter converter = new ImageConverter();
                            System.Drawing.Image uploadImage = System.Drawing.Image.FromStream(file.InputStream);
                            var temp = (byte[])converter.ConvertTo(uploadImage, typeof(byte[]));
                            Utility.ByteCloudUploadImages(temp, imgName, path);

                            //Utility.cloudUploadImages(file, imgName, path);
                            // Vconnect.Common.Utility.CopyImage.Sync(webconfigSourceImagesRootPath + "photogallery", webconfigDestinationImagesRootPath + "photogallery");
                            TempData["SuccessMessage"] = "File uploaded successfully";
                        }
                        
                        //}
                        // BusinessId
                    }
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                    imgName = "ERROR";
                    return Json(imgName, JsonRequestBehavior.AllowGet);
                }
                if (isSavedSuccessfully)
                {
                    return Json(imgName, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    imgName = "ERROR";
                    return Json(imgName, JsonRequestBehavior.AllowGet);
                }
            //}
            //else
            //{
            //    return Json(null, JsonRequestBehavior.AllowGet);
            //}
        }
        public ActionResult EditPhotoTitleDescription(string bizid, string name, string title, string description)
        {
            string status = string.Empty;
            if (!string.IsNullOrEmpty(bizid) && !string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(title) && !string.IsNullOrEmpty(description))  // && (!Request.AcceptTypes.Contains("text/html"))
            {
                string photopath = "uploads/photogallery/" + name;
                Utility.createCookieBizPhoto(Convert.ToInt32(bizid), name);
                updateBusinessPhoto(bizid, photopath, title, description, 0);
                return Json("SUCCESS", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult UpdateBizPhoto()
        {
            List<ImageObjbiz> bizImgObj = new List<ImageObjbiz>();
            if (Request.Cookies["vcBizPhoto"] != null)  // && (!Request.AcceptTypes.Contains("text/html"))
            {
                HttpCookie cookieBizList = Request.Cookies["vcBizPhoto"];
                if (cookieBizList.HasKeys)
                {
                    //string[] imagename = cookieBizList.Value.ToString().Split('=');
                    //string bizid = imagename[0].ToString();
                    //Int32 useridnew = int.Parse(bizid.ToString());
                    //foreach (string selectimage in imagename)
                    //{
                    //    Int32 createdby = 0;
                    //    if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                    //        createdby = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                    //    string newimagename = "uploads/photogallery/" + selectimage.Replace("&" + bizid.ToString(), "").ToString();
                    //    updateBusinessPhoto("", newimagename, "", "", createdby);
                    //}
                    //return Json("SUCCESS", JsonRequestBehavior.AllowGet);


                    string[] imagename = cookieBizList.Value.ToString().Split('&');
                    //string bizid = imagename[0].ToString();
                    //Int32 useridnew = int.Parse(bizid.ToString());
                    foreach (string selectimage in imagename)
                    {
                        //string[] arr = selectimage.ToString().Split('=');
                        string[] imgdetails = selectimage.ToString().Split('=');
                        Int32 createdby = 0;
                        if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                            createdby = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                        //string newimagename = "uploads/photogallery/" + selectimage.Replace("&" + imgdetails[0].ToString(), "").ToString();
                        updateBusinessPhoto("", "uploads/photogallery/" + imgdetails[1].ToString(), "", "", createdby);
                    }
                    return Json("SUCCESS", JsonRequestBehavior.AllowGet);
                }
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
        public int updateBusinessPhoto(string bizid, string photoName, string photoTitle, string photoDescription, Int32? createdby)
        {

            int result = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_edit_PhotosToBusiness_live]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object

                cmd.Parameters.Add(new SqlParameter("@photo", photoName.ToString()));
                cmd.Parameters.Add(new SqlParameter("@title", photoTitle.ToString()));
                cmd.Parameters.Add(new SqlParameter("@description", photoDescription.ToString()));
                if (createdby.HasValue)
                {
                    cmd.Parameters.Add(new SqlParameter("@userid", createdby.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@userid", 0));
                }
                cmd.Parameters.Add(new SqlParameter("@Err", ParameterDirection.Output));
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();
                result = (int)cmd.Parameters["@Err"].Value;
            }
            return result;
        }
        #endregion
        #region Claim business
        public ActionResult BusinessClaim(long? bizID)
        {
            if (bizID.HasValue) // && (!Request.AcceptTypes.Contains("text/html"))  //  && Request.ContentType.IndexOf("json") > 0)
            {
                DetailWEBBiz bizDetailWEB = new DetailWEBBiz();
                var data = bizDetailWEB.GetBusinessClaim(bizID.Value);
                //RedirectToAction("BusinessDetail", "BizDetailWEB"); 
                if (data == null) { return Json("0", JsonRequestBehavior.AllowGet); }
                else
                {
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region Email
        protected void EmailTOAdminForIncorrectReport(string businessname, string username, string reportdetail, string email)
        {
            string Message = "";
            System.Net.Mail.SmtpClient SMPT = new System.Net.Mail.SmtpClient();
            System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage();
            System.IO.FileStream FsContent = new System.IO.FileStream(Server.MapPath("~/resource/email_templates/reportincorrect.html"), System.IO.FileMode.Open, System.IO.FileAccess.Read);
            System.IO.StreamReader SRcontent = new System.IO.StreamReader(FsContent);
            Message = SRcontent.ReadToEnd();
            FsContent.Close();
            SRcontent.Close();
            FsContent.Dispose();
            SRcontent.Dispose();
            Message = Message.Replace("{0}", username);
            Message = Message.Replace("{1}", businessname);
            Message = Message.Replace("{2}", reportdetail);
            Message = Message.Replace("{3}", email);

            Message = Message.Replace("{20}", ConfigurationManager.AppSettings["WebsiteRootPath"].ToString());
            Email.Subject = ConfigurationManager.AppSettings["SubjectreportincorrectTOAdmin"].ToString() + " - " + businessname;
            Email.From = new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["networkUserIdvcHelpdesk"].ToString(), "VConnect.com");
            Email.IsBodyHtml = true;
            Email.Body = Message;
            Email.To.Add(ConfigurationManager.AppSettings["EmailToreportincorrectTOAdmin"].ToString());
            //Email.To.Add("devinder.kaur@aardeesoft.com");
            //BLLEmail.EmailSending(ref Email, ConfigurationManager.AppSettings["OthersUserId"].ToString().Trim(), ConfigurationManager.AppSettings["OthersUserPassword"].ToString().Trim());
            SendEmail send = new SendEmail();
            try
            {
                send.SendMailToUser(ref Email, ConfigurationManager.AppSettings["networkUserIdvcHelpdesk"].ToString(), ConfigurationManager.AppSettings["networkUserIdPasswordvcHelpdesk"].ToString());
            }
            catch (Exception e)
            {
                log.LogMe(e);
            }

        }
        [HttpPost]
        public ActionResult SendEmailToBusiness(string name, string email, string message, int bizID, int userid)   //(string[] mystr, string another)
        {
            string bname, bemail;
            DetailWEBBiz detailwebbiz = new DetailWEBBiz();
            detailwebbiz.getBusinessEmail(bizID, out bname, out bemail);
            if (bemail.Contains("takashiyamauchi") == false)
            {
                //message = "You have received a message from Vconnect user.<br/>" + "Name : " + name + "<br/>Email : " + email + "<br/>Message : <br/>" + message;
                string path = Server.MapPath("\\Resource\\email_templates\\") + "BOMail.html";
                string body;
                using (var sr = new StreamReader(path))
                {
                    body = sr.ReadToEnd();
                }
                body = body.Replace("{1}", name)
                     .Replace("{3}", email)
                    .Replace("{2}", message);
                //detailwebbiz.SendEmail("VC user query", email, name, message);   //Testing
                detailwebbiz.SendEmail("VC user query", bemail, bname, body);   //Actual            
                detailwebbiz.AddEmailLog(bizID, bname, email, message, name, userid);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SendEmailToUserBizInfo(string EmailID, Int64? bizid, string business, string url)
        {
            string location = string.Empty;
            string Message = "";
            if (!string.IsNullOrEmpty(EmailID))
            {
                System.Net.Mail.SmtpClient SMPT = new System.Net.Mail.SmtpClient();
                System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage();
                System.IO.FileStream FsContent = new System.IO.FileStream(Server.MapPath("~/resource/email_templates/Claim_Business.html"), System.IO.FileMode.Open, System.IO.FileAccess.Read);
                System.IO.StreamReader SRcontent = new System.IO.StreamReader(FsContent);
                Message = SRcontent.ReadToEnd();

                FsContent.Close();
                SRcontent.Close();
                FsContent.Dispose();
                SRcontent.Dispose();
                Message = Message.Replace("{1}", business.Trim());
                //Message = Message.Replace("{2}", ((ConfigurationManager.AppSettings["WebsiteRootPath"]).ToString() + url).Trim() + "?email=claimed");
                Message = Message.Replace("{2}", "http://www.vconnect.com/CYBRepotIncorrect?bid=" + bizid.ToString());

                Email.Subject = "You are missing out! Claim your business for free today.";
                Email.From = new System.Net.Mail.MailAddress((ConfigurationManager.AppSettings["networkUserIdSupport"]).ToString(), "VConnect.com");
                Email.IsBodyHtml = true;
                Email.Body = Message;
                Email.To.Add(new MailAddress(EmailID));
                SendEmail send = new SendEmail();
                try
                {
                    send.SendMailToUser(ref Email, (ConfigurationManager.AppSettings["networkUserIdSupport"]).ToString(), (ConfigurationManager.AppSettings["networkUserIdPasswordSupport"]).ToString());
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
                Email.Dispose();
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion
       
        public ActionResult BusinessLog(Int32? bizid, string bizName, string sourceurl, string previousurl)
        {          
            try
            {               
                string IPAddress = string.Empty;
                string ReverseDNS = string.Empty;
                if (bizid.HasValue && bizid.Value > 0)
                {
                   
                    int result = 0;

                    if (bizName == null)
                    {
                        bizName = string.Empty;
                    }
                    IPAddress = Vconnect.MvcApplication.GetVisitorIPAddress();
                    ReverseDNS = !string.IsNullOrEmpty(IPAddress) ? Vconnect.MvcApplication.GetReverseDns(IPAddress.ToString()) : "";
                    
                    if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                    {
                        result = log.addbusinessvisitorlog(bizid.Value, bizName, Convert.ToInt32(objUS.SV_VCUserContentID.ToString()), IPAddress, System.Web.HttpUtility.HtmlDecode(sourceurl), System.Web.HttpUtility.HtmlDecode(previousurl), 6, ReverseDNS);
                    }
                    else
                    {
                        result = log.addbusinessvisitorlog(bizid.Value, bizName, 0, IPAddress, System.Web.HttpUtility.HtmlDecode(sourceurl), System.Web.HttpUtility.HtmlDecode(previousurl), 6, ReverseDNS);

                    }
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
            
        }

        public ActionResult BusinessPhoneNumbers(long? bizID, string whichNumber, string sourceurl, string previousurl)
        {
            AddPhoneClickLog(bizID, whichNumber, sourceurl, previousurl);

            DetailWEBBiz detailwebbiz = new DetailWEBBiz();
            int userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;

            if (bizID.HasValue)//  && Request.ContentType.IndexOf("json") > 0)
            {
                if (userid != 0 || detailwebbiz.IPallowed(Utility.GetIpAddress()) == true)  //when we implement 11 click limit for logged in users, remove the userid != 0 condition here
                {
                    var data = detailwebbiz.GetPhone(bizID.Value);

                    if (whichNumber.CompareTo("phone") == 0)
                    {
                        data.phone = !string.IsNullOrEmpty(data.phone) ? GetPhoneinBinary(data.phone, "phone") : "";
                        data.alternatephone = null;
                    }
                    else if (whichNumber.CompareTo("alternatephone") == 0)
                    {
                        data.phone = null;
                        data.alternatephone = !string.IsNullOrEmpty(data.alternatephone) ? GetPhoneinBinary(data.alternatephone, "alternatephone") : "";
                    }
                    //RedirectToAction("BusinessDetail", "BizDetailWEB"); 
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddPhoneClickLog(Int64? bizId, string whichNumber, string sourceurl, string previousurl)
        {
            //string sourceurl = string.Empty;
            //string previousurl = string.Empty;

            //sourceurl = TempData["sourcedetail"] != null ? TempData["sourcedetail"].ToString() : "";
            //previousurl = TempData["previousdetail"] != null ? TempData["previousdetail"].ToString() : "";


            //sourceurl = TempData.Peek("sourcedetail") != null ? TempData.Peek("sourcedetail").ToString() : "";
            //previousurl = TempData.Peek("previousdetail") != null ? TempData.Peek("previousdetail").ToString() : "";

            //TempData.Keep("sourcedetail");
            //TempData.Keep("previousdetail");


            if (bizId.HasValue)
            {
                DetailWEBBiz detailwebbiz = new DetailWEBBiz();
                DetailPhone PhoneNumbers = new DetailPhone();
                PhoneNumbers = detailwebbiz.GetPhone(bizId.Value);

                JsonModel jsonModel = new JsonModel();
                string phoneno = string.Empty;
                int result = 0;

                //FileContentResult img = null;
                int userid = (!string.IsNullOrEmpty(objUS.SV_VCUserContentID)) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;

                //if (whichNumber == "phone")
                //    phoneno = "phone";   //bizPhoneNumbers.phone;

                //else if (whichNumber == "alternatephone")
                //    phoneno = "alternatephone"; //bizPhoneNumbers.alternatephone;

                //else if (whichNumber == "address")
                //    phoneno = "address";
                //else
                //    phoneno = "phone";

                phoneno = whichNumber == "alternatephone" ? "alternatephone" : whichNumber == "address" ? "address" : "phone";
                    //phoneno = PhoneNumbers.phone;

                string ipaddress = Utility.GetIpAddress();
                result = log.addphoneclicklog(phoneno, bizId.ToString(), userid, ipaddress, sourceurl, previousurl);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetPhoneinBinary(string phoneno, string phoneAlternatePhone)
        {
            FileContentResult img = null;
            JsonModel jsonModel = new JsonModel();
            string phone = string.Empty;
            string alternatephone = string.Empty;
            string phoneNumbers = string.Empty;

            if (phoneAlternatePhone == "phone" && !string.IsNullOrEmpty(phoneno))
            {
                img = StringImage(phoneno, "phone");
                phone = "<img src='data:image/png;base64," + Convert.ToBase64String(img.FileContents) + "' alt='Primary Phone' style='border:solid 0px white' />";
                return phone;
            }
            else if (phoneAlternatePhone == "alternatephone" && !string.IsNullOrEmpty(phoneno))
            {
                img = StringImage(phoneno, "alternatephone");
                alternatephone = "<img src='data:image/png;base64," + Convert.ToBase64String(img.FileContents) + "' alt='Secondary Phone' style='border:solid 0px white'/>";
                return alternatephone;

            }
            return string.Empty;
        }

        public FileContentResult StringImage(string str, string whichNumber)
        {
            var rand = new Random((int)DateTime.Now.Ticks);

            //generate new question
            int a = rand.Next(10, 99);
            int b = rand.Next(0, 9);
            var captcha = str;//string.Format("{0} + {1} = ?", a, b);

            //store answer
            //Session["Captcha" + prefix] = a + b;


            //image stream
            FileContentResult img = null;
            Color fontcolor = Color.FromArgb(89, 89, 89);   //website uses #595959 which in RGB format is 89,89,89
            SolidBrush mybrush = new SolidBrush(fontcolor);

            using (var mem = new MemoryStream())
            using (var bmp = new Bitmap(91, 21))
            using (var gfx = Graphics.FromImage((Image)bmp))
            {
                gfx.SmoothingMode = SmoothingMode.AntiAlias;
                gfx.Clear(Color.Transparent);
                gfx.TextRenderingHint = TextRenderingHint.AntiAlias;

                gfx.FillRectangle(Brushes.Transparent, 0, 0, bmp.Width, bmp.Height);

                gfx.DrawString(captcha, new Font("Tahoma", 10), mybrush, 0, 0);

                //render as Jpeg
                bmp.Save(mem, System.Drawing.Imaging.ImageFormat.Png);
                img = this.File(mem.GetBuffer(), "image/png");

            }
            img.FileDownloadName = whichNumber;
            return img;
        }

        public ActionResult BusinessAddress(int? bizID=0 ,string sourceurl ="",string previousurl="")  //get business address
        {
            if (bizID.HasValue && bizID.Value > 0)
            {
                AddPhoneClickLog(bizID, "address", sourceurl, previousurl);

                DetailWEBBiz detailwebbiz = new DetailWEBBiz();
                string houseno, address1, area;

                int userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;

                if (bizID != 0)
                {
                    if (userid != 0 || detailwebbiz.IPallowed(Utility.GetIpAddress()) == true)  //when we implement 11 click limit for logged in users, remove the userid != 0 condition here
                    {
                        detailwebbiz.getBusinessAddress(bizID.Value, out houseno, out address1, out area);
                        string[] data = new string[3] { houseno, address1, area };
                        return Json(data, JsonRequestBehavior.AllowGet);
                    }
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetStore(int? supplierid, int productid = 0, string brandid = null, int minprice = 0, int maxprice = 0, int pageNum = 1, int rowsPerPage = 12, int sort = 1, string attributeid = null)
        {
            if (supplierid.HasValue && supplierid.Value >0)
            {
                DetailWEBBiz detailwebbiz = new DetailWEBBiz();

                int userid = !string.IsNullOrEmpty(objUS.SV_VCUserContentID) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;

                if (supplierid != 0)
                {
                    Store myStore = detailwebbiz.GetStore(supplierid.Value, productid, brandid, minprice, maxprice, pageNum, rowsPerPage, sort, attributeid);
                    return Json(myStore, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetQuote(string sourceurl, string previousurl)
        {            
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetQuote(int? itemid, string name, string phone, string whenToBuy, string message,string sourceurl,string previousurl)
        {
            Dictionary<string, int> response = new Dictionary<string, int>();
            if (itemid.HasValue)
            {
                DetailWEBBiz detailwebbiz = new DetailWEBBiz();
                Dictionary<string, string> bizInfo = new Dictionary<string, string>();
                int vaildphone = 0;
                
                int when = !string.IsNullOrEmpty(whenToBuy) ? Convert.ToInt32(whenToBuy) : 0;
                
                if (phone != null)
                {
                    if (phone.ToString().IndexOf('0') == 0)
                    {
                        vaildphone = Vconnect.Common.Utility.isValidPhonemobile(phone.ToString());
                    }
                    else
                    {
                        vaildphone = Vconnect.Common.Utility.isValidPhonemobile("0" + phone.ToString());
                    }
                    if (vaildphone == 0)
                    {
                        
                        response.Add("status", 0);
                        return Json(response, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        bizInfo = detailwebbiz.addQuoteRequestToDB(itemid.Value, name, phone, when, message, sourceurl, previousurl);

                        GetQuoteSmsSending(bizInfo["bizphone"], phone, bizInfo["productname"], when, Convert.ToInt32(bizInfo["ismasked"]));
                        GetQuoteEmailSending(bizInfo["bizemail"], name, phone, bizInfo["productname"], bizInfo["imagepath"], when, bizInfo["description"], Convert.ToInt32(bizInfo["ismasked"]));

                        
                        response.Add("status", 1);

                        return Json(response, JsonRequestBehavior.AllowGet);
                    }                    
                }
               
                response.Add("status", 0);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            else
            {
                response.Add("status", 0);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        public bool GetQuoteEmailSending(string businessemail, string username, string userphone, string productname, string productimage, int whenToBuy, string description, int ismasked)
        {
            DetailWEBBiz detailwebbiz = new DetailWEBBiz();
            StringBuilder message = new StringBuilder();
            string body, path;
            string[] when = new string[4] { "No Fixed Timeline", "Now", "Next Week", "No Fixed Timeline" };

            if (ismasked == 0)
                path = Server.MapPath("\\Resource\\email_templates\\") + "vetenquirymailtobusiness.html";
            else
                path = Server.MapPath("\\Resource\\email_templates\\") + "vetenquirymailtobusinessexpired.html";

            using (var sr = new StreamReader(path))
            {
                body = sr.ReadToEnd();
            }

            body = body.Replace("{{productname}}", productname)
                        .Replace("{{username}}", username)
                        .Replace("{{when}}", when[whenToBuy])
                        .Replace("{{userphone}}", userphone)
                        .Replace("{{productimage}}", productimage);

            detailwebbiz.SendEmail("Product Inquiry", businessemail, "", body); //This takes time
            int bizID = 0;
            string bname = "";
            detailwebbiz.AddEmailLog(bizID, bname, businessemail, body, username, Convert.ToInt32(objUS.SV_VCUserContentID));

            return true;
        }

        public bool GetQuoteSmsSending(string bizphone, string userphone, string itemname, int whenToBuy, int ismasked)
        {
            DetailWEBBiz detailwebbiz = new DetailWEBBiz();

            //message content variety.
            string[] when = new string[4] { "", " Now", " Next Week", "" };
            string[] leadsleft = new string[2];
            leadsleft[0] = "Dear Business Owner, VConnect user requires {{itemname}} for purchase{{when}}. Call {{userphone}} with your best quotes";
            leadsleft[1] = "Dear Business Owner, VConnect user with phone number 080-xxxx-xxxx requires {{itemname}} for purchase{{when}}. " +
                           "Call 070-0000-8888 to Subscribe for Leads Package and get full user details.";

            //find appropriate message content.
            string message;
            message = leadsleft[ismasked].Replace("{{itemname}}", itemname)
                        .Replace("{{when}}", when[whenToBuy])
                        .Replace("{{userphone}}", userphone);

            //send sms
            string SMSResponse = detailwebbiz.Sendmessage(message, bizphone);    //Actual : commented in testing
            //string SMSResponse = "";    //testing
            detailwebbiz.addsmsemaillog(Convert.ToInt32(objUS.SV_VCUserContentID), 126, "", bizphone.Trim(), "", 1117 + ismasked, "Your request submitted successfully.", message, 0, "", "", SMSResponse.ToString(), "", 0, 0, "", 1, "");
            return true;
        }

        protected void SendEmailToBusinessInformation(string EmailID, Int64? bizid)
        {
            string location = string.Empty;
            string Message = "";
            if (!string.IsNullOrEmpty(EmailID))
            {
                System.Net.Mail.SmtpClient SMPT = new System.Net.Mail.SmtpClient();
                System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage();
                System.IO.FileStream FsContent = new System.IO.FileStream(Server.MapPath("~/resource/email_templates/Businessuserregistration.html"), System.IO.FileMode.Open, System.IO.FileAccess.Read);
                System.IO.StreamReader SRcontent = new System.IO.StreamReader(FsContent);
                Message = SRcontent.ReadToEnd();

                FsContent.Close();
                SRcontent.Close();
                FsContent.Dispose();
                SRcontent.Dispose();
                //Message = Message.Replace("{0}", ContactPerson);
                //Message = Message.Replace("{1}", Utility.ToTitleCase(UserName));

                Email.Subject = "Business contact details.";
                Email.From = new System.Net.Mail.MailAddress((ConfigurationManager.AppSettings["networkbusinessid"]).ToString(), "VConnect.com");
                Email.IsBodyHtml = true;
                Email.Body = Message;
                Email.To.Add(new MailAddress(EmailID));
                SendEmail send = new SendEmail();
                try
                {
                    send.SendMailToUser2(ref Email, (ConfigurationManager.AppSettings["networkbusinessid"]).ToString(), (ConfigurationManager.AppSettings["networkbusinessidPassword"]).ToString());
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
                Email.Dispose();
            }
        }
        #region phoneNumberCheck
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult phonenumbercheckalt(string altphonenumber, string phonenumber, int? userid, string baltphone)
        {
            //if ((!Request.AcceptTypes.Contains("text/html")))
            //{
                int d = 4;
                if (!string.IsNullOrEmpty(altphonenumber))
                {
                    int result = Vconnect.Common.Utility.isValidPhone(altphonenumber);
                    int result1 = 0;

                    if (userid.HasValue)
                    {
                        result1 = Vconnect.Common.Utility.businessphonechk(altphonenumber, userid.Value);
                    }
                    string sContactNumber = altphonenumber;
                    string subSection = sContactNumber.Substring(0, 1);

                    if (subSection != "0")
                    {
                        d = 5;
                    }
                    else if (result == 0)
                    {
                        d = 2;
                    }
                    else if (result1 == 1)
                    {
                        if (!string.IsNullOrEmpty(phonenumber) && !string.IsNullOrEmpty(altphonenumber))
                        {
                            if (altphonenumber == phonenumber || !string.IsNullOrEmpty(altphonenumber) && baltphone == altphonenumber)
                                d = 6;
                        }
                        else
                        {
                            d = 1;
                        }
                    }
                    else
                    {
                        d = 0;
                        if (!string.IsNullOrEmpty(phonenumber) && !string.IsNullOrEmpty(altphonenumber))
                        {
                            if (altphonenumber == phonenumber)
                                d = 6;
                        }
                    }
                }
                else
                {
                    d = 4;
                }
                return Json(d, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            //    return Json(null, JsonRequestBehavior.AllowGet);
            //}
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult phonenumbercheck(string phonenumber, int? userid, string bphone)
        {
            //if ((!Request.AcceptTypes.Contains("text/html")))
            //{
                int d = 4;
                if (!string.IsNullOrEmpty(phonenumber))
                {
                    int result1 = 0;
                    int result = Vconnect.Common.Utility.isValidPhone(phonenumber);
                    if (!string.IsNullOrEmpty(phonenumber) && bphone == phonenumber)
                    {
                        d = 0;
                        return Json(d, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        if (userid.HasValue)
                        {
                            result1 = Vconnect.Common.Utility.businessphonechk(phonenumber, userid.Value);
                        }
                    }
                    string sContactNumber = phonenumber;
                    string subSection = sContactNumber.Substring(0, 1);
                    if (subSection != "0")
                    {
                        d = 5;
                    }
                    else if (result == 0)
                    {
                        d = 2;
                    }
                    else if (result1 == 1)
                    {
                        d = 1;
                    }
                    else
                    {
                        d = 0;
                    }
                }
                else
                {
                    d = 4;
                }

                return Json(d, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            //    return Json(null, JsonRequestBehavior.AllowGet);
            //}
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult Duplicatebusiness(int businessid, int textid, string textvalue)
        {
            //if ((!Request.AcceptTypes.Contains("text/html")))
            //{
                int result = 0;
                int userid = (!string.IsNullOrEmpty(objUS.SV_VCUserContentID)) ? Convert.ToInt32(objUS.SV_VCUserContentID) : 0;
                DetailWEBBiz bizDetailWEB = new DetailWEBBiz();
                result = bizDetailWEB.DuplicatebusinessCheck(businessid, textid, textvalue, userid);
                return Json(result, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            //    return Json(null, JsonRequestBehavior.AllowGet);
            //}
        }
        public ActionResult emailphonesend(Int64? bizid, string emailphone)
        {
            string msg = string.Empty;
            var txtvalue = emailphone;
            if (bizid.HasValue) // && (!Request.AcceptTypes.Contains("text/html"))
            {
                long IDurl = 0;
                if (long.TryParse(txtvalue, out IDurl))
                {
                    IDurl = 1;
                }
                else
                {
                    IDurl = 0;
                }
                if (IDurl == 0 && txtvalue.Contains("@") == false)
                {
                    msg = "Please enter valid email-id.";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
                if (txtvalue.Contains("@") && IDurl == 0)
                {
                    var atpos = txtvalue.IndexOf("@");
                    var dotpos = txtvalue.LastIndexOf(".");
                    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= txtvalue.Length)
                    {
                        msg = "invalid Email-Id";
                        return Json(msg, JsonRequestBehavior.AllowGet);
                    }
                    // SendEmailToBusinessInformation(emailphone, bizid.Value);
                }
                else
                {
                    if (txtvalue.IndexOf("0") != 0)
                    {
                        msg = "Please start contact number with zero";
                        return Json(msg, JsonRequestBehavior.AllowGet);
                    }
                    if (txtvalue.Length != 11)
                    {
                        msg = "Please enter valid mobile number.";
                        return Json(msg, JsonRequestBehavior.AllowGet);
                    }

                    try
                    {
                        int vaildphone = 0;
                        vaildphone = Vconnect.Common.Utility.isValidPhone(emailphone.ToString());
                        if (vaildphone == 0)
                        {
                            msg = "Phone number is not valid.";
                            return Json(msg, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            SendSMS send = new SendSMS();
                            BizDetailInfo bizDetailInfo = new BizDetailInfo();
                            BizDetailWEB bizDetailWEB = new BizDetailWEB();
                            bizDetailInfo = bizDetailWEB.GetBizDetailInfo(bizid.Value);
                            string[] contactNos = bizDetailInfo.contactno.Split(',');
                            string allContactNo = string.Empty;
                            foreach (var item in contactNos)
                            {
                                if (!string.IsNullOrEmpty(item))
                                {
                                    if (string.IsNullOrEmpty(allContactNo))
                                    {
                                        allContactNo = "0" + Convert.ToInt64(item).ToString("##-####-####");
                                    }
                                    else
                                    {
                                        allContactNo = allContactNo + ", " + "0" + Convert.ToInt64(item).ToString("##-####-####");
                                    }
                                }
                            }
                            //msg = bizDetailInfo.businessname + ", " + bizDetailInfo.address + ", " + allContactNo;
                            msg = bizDetailInfo.businessname + ", " + bizDetailInfo.fulladdress + ", " + allContactNo;
                            string SMSResponse = send.Sendmessage(msg, emailphone);
                            int usertype = !string.IsNullOrEmpty(objUS.SV_VCUserType) ? Convert.ToInt16(objUS.SV_VCUserType) : 0;
                            string source = "WEB";
                            if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID) && Convert.ToInt32(objUS.SV_VCUserContentID) != 0)
                            {
                                Utility.SaveSMSEmailLog(Convert.ToInt32(objUS.SV_VCUserContentID), usertype, "", emailphone, "", MessageType.SMS_UserReq.GetHashCode(), ConfigurationManager.AppSettings["SubjectBusinessDetail"].ToString(), "SMSed details send for: " + msg, Convert.ToInt32(bizid.Value), "", "", SMSResponse, Utility.GetIpAddressNew(), 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.Email.GetHashCode(), Utility.GetReversedns(Utility.GetIpAddressNew()), source);
                            }
                            else
                            {
                                Utility.SaveSMSEmailLog(0, usertype, "", emailphone, "", MessageType.SMS_UserReq.GetHashCode(), ConfigurationManager.AppSettings["SubjectBusinessDetail"].ToString(), "SMSed details send for: " + msg, Convert.ToInt32(bizid.Value), "", "", SMSResponse, Utility.GetIpAddressNew(), 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.Email.GetHashCode(), Utility.GetReversedns(Utility.GetIpAddressNew()), source);
                            }
                            msg = "";
                        }
                    }
                    catch (Exception ex)
                    {
                        msg = "invalid data";
                        log.LogMe(ex);
                    }
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult bizWebsite(Int32? bizid)
        {
            DetailWEBBiz detailwebbiz = new DetailWEBBiz();
            string website = detailwebbiz.GetBizWebsite(bizid);
            if (website != null && website.Contains("http") == false)
                website = "http://" + website;
            return RedirectPermanent(website.ToString().Replace("$", "&"));
        }

        #region Widget
        #region Widget
        public ActionResult BizDetailWidget(long? bizID)
        {
            BizDetailWEB bizDetailWEB = new BizDetailWEB();
            BizDetails bizDetails = new BizDetails();
            //BusinessLead businesslead = new BusinessLead();
            if (bizID.HasValue)
            {
                int leadid = 0;
                if (bizDetailWEB.GetBizWidget(bizID.Value) != null)
                    leadid = bizDetailWEB.GetBizWidget(Convert.ToInt32(bizID.Value)).leadid;
                if (leadid != 0)
                {
                    bizDetails = bizDetailWEB.clentdesign(bizID, "business");
                    //bizDetails.businesslead = businesslead;
                    if (bizDetails.businesslead != null && bizDetails.openaccount != null && bizDetails.openaccount.Count > 0)
                    {
                        return View("bizdetailwidget", bizDetails);
                    }
                    else
                    {
                        return RedirectPermanent("/");
                    }
                }
            }
            return RedirectPermanent("/");
        }
        public ActionResult catWidget(string widgeturl)
        {
            BizDetailWEB bizDetailWEB = new BizDetailWEB();
            BizDetails bizDetails = new BizDetails();
            CategoryWidget categoryWidget = new CategoryWidget();
            try
            {
                categoryWidget = bizDetailWEB.getWidgetContentid(widgeturl);
                if (categoryWidget.contentid > 0)
                {
                    bizDetails = bizDetailWEB.clentdesign(categoryWidget.contentid, "category");
                    if (bizDetails.businesslead != null && bizDetails.openaccount != null && bizDetails.openaccount.Count > 0)
                    {
                        return View("CategoryWidget", bizDetails);
                    }
                    else
                    {
                        return RedirectPermanent("/");
                    }
                }
                else
                {
                    return RedirectPermanent("/");
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            return RedirectPermanent("/");
            //return RedirectToActionPermanent("CategoryWidget", new { catID = categoryWidget.contentid });
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult phonenumberWidget(string phonenumber, int? userid)
        {
            //if ((!Request.AcceptTypes.Contains("text/html")))
            //{
                int d = 4;
                if (!string.IsNullOrEmpty(phonenumber))
                {
                    int result1 = 0, result = 0;
                    result = Vconnect.Common.Utility.isValidPhone(phonenumber);
                    //if (userid.HasValue)
                    //    result1 = Vconnect.Common.Utility.businessphonechk(phonenumber, userid.Value);
                    string sContactNumber = phonenumber;
                    string subSection = sContactNumber.Substring(0, 1);
                    if (subSection != "0")
                    {
                        d = 5;
                    }
                    else if (result == 0)
                    {
                        d = 2;
                    }
                    //else if (result1 == 1)
                    //{
                    //    d = 1;
                    //}
                    else
                    {
                        d = 0;
                    }
                }
                else
                {
                    d = 4;
                }

                return Json(d, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            //    return Json(null, JsonRequestBehavior.AllowGet);
            //}
        }

        public ActionResult CategoryWidget(long? catID)
        {
            BizDetailWEB bizDetailWEB = new BizDetailWEB();
            BizDetails bizDetails = new BizDetails();
            if (catID.HasValue)
            {
                bizDetails = bizDetailWEB.clentdesign(catID.Value, "category");
                if (bizDetails.businesslead != null && bizDetails.openaccount != null && bizDetails.openaccount.Count > 0)
                {
                    return View("CategoryWidget", bizDetails);
                }
                else
                {
                    return RedirectPermanent("/");
                }
            }
            return RedirectPermanent("/");
        }
        #endregion

        #region For Dynamic Design Submit
        public ActionResult SaveLeadData(string listid, string listvalue, string leadid, string listmessage, string listRequired, string uniqueid, string submit, string widgetname, string listpattern, string sussesssms)
        {
            if (string.IsNullOrEmpty(listid) && string.IsNullOrEmpty(listvalue) && string.IsNullOrEmpty(leadid) && string.IsNullOrEmpty(listmessage) && string.IsNullOrEmpty(listRequired) && string.IsNullOrEmpty(uniqueid) && string.IsNullOrEmpty(submit) && string.IsNullOrEmpty(widgetname) && string.IsNullOrEmpty(listpattern))
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string widget = "";
                if (string.IsNullOrEmpty(widgetname))
                    widgetname = "Widget";
                if (!string.IsNullOrEmpty(submit) && submit == "category")
                    widget = "cw_" + leadid;
                else
                    widget = "bw_" + leadid;
                List<RegisterUserClients> registerclients = new List<RegisterUserClients>();
                int Error = 0;
                string uniquevalue = string.Empty;
                string result = string.Empty;
                var columnname = listid.Split(';').ToList();
                var columnvalue = listvalue.Split(';').ToList();
                var patterntype = listpattern.Split(';').ToList();
                var columnRequired = listRequired.Split(';').ToList();
                var columnmessage = listmessage.Split(';').ToList();

                for (int j = 0; j < columnRequired.Count - 1; j++)
                {
                    if (columnRequired[j].Trim() == "Required" && columnRequired[j].Trim() != "," && (string.IsNullOrEmpty(columnvalue[j].Trim()) || columnvalue[j].Trim() == "," || columnvalue[j].Trim() == "undefined" || columnvalue[j].Trim() == "Please select"))
                    {
                        var resultdata1 = new { Error = columnmessage[j].Trim() + "    ", sussesssms = sussesssms };
                        return Json(resultdata1, JsonRequestBehavior.AllowGet);
                    }
                }
                string query = string.Empty;
                var queryvalue = "GETDATE()," + "$$" + leadid + "$$,$$WEB$$," + "$$" + widget + "$$," + "$$" + widgetname + "$$,";
                //Pass query string
                var querycolumn = "createddate,leadid,source,widget,clientname,";
                for (int k = 0; k < columnvalue.Count - 1; k++)
                {
                    if (!string.IsNullOrEmpty(uniqueid) && columnname[k] == uniqueid)
                    {
                        uniquevalue = columnvalue[k];
                    }
                    if (!string.IsNullOrEmpty(columnvalue[k].Trim()))
                    {
                        //for values of query
                        if (columnvalue[k].Substring(0, 1) == ",")
                        {
                            if (k == 0)
                            {
                                if ((patterntype[k].Trim() == "calender" || patterntype[k].Trim() == "dob") && !string.IsNullOrEmpty(columnvalue[k].Trim()))
                                    queryvalue += "$$" + Convert.ToDateTime(columnvalue[k].Remove(0, 1).Trim()) + "$$";
                                else
                                    queryvalue += "$$" + columnvalue[k].Remove(0, 1).Trim() + "$$";
                            }
                            else
                            {
                                if ((patterntype[k].Trim() == "calender" || patterntype[k].Trim() == "dob") && !string.IsNullOrEmpty(columnvalue[k].Trim()))
                                    queryvalue += ",$$" + Convert.ToDateTime(columnvalue[k].Remove(0, 1).Trim()) + "$$";
                                else
                                    queryvalue += ",$$" + columnvalue[k].Remove(0, 1).Trim() + "$$";
                            }
                        }
                        else
                        {
                            if (k == 0)
                            {
                                if ((patterntype[k].Trim() == "calender" || patterntype[k].Trim() == "dob") && !string.IsNullOrEmpty(columnvalue[k].Trim()))
                                    queryvalue += "$$" + Convert.ToDateTime(columnvalue[k].Trim()) + "$$";
                                else
                                    queryvalue += "$$" + columnvalue[k].Trim() + "$$";
                            }
                            else
                            {
                                if ((patterntype[k].Trim() == "calender" || patterntype[k].Trim() == "dob") && !string.IsNullOrEmpty(columnvalue[k].Trim()))
                                    queryvalue += ",$$" + Convert.ToDateTime(columnvalue[k].Trim()) + "$$";
                                else
                                    queryvalue += ",$$" + columnvalue[k].Trim() + "$$";
                            }
                        }
                        //for column name of query
                        if (columnname[k].Substring(0, 1) == ",")
                        {
                            if (k == 0)
                                querycolumn += columnname[k].Remove(0, 1).Trim();
                            else
                                querycolumn += "," + columnname[k].Remove(0, 1).Trim();
                        }
                        else
                        {
                            if (k == 0)
                                querycolumn += columnname[k].Trim();
                            else
                                querycolumn += "," + columnname[k].Trim();
                        }
                    }
                }
                query = "insert into clientadds (" + querycolumn.Replace(",,", ",").Replace(" ", "_") + ") values(" + queryvalue.Replace(",,", ",") + ")";
                using (var db = new VconnectDBContext29())
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[ClientUser_Registraion]";
                    cmd.CommandTimeout = 600;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@leadid", leadid.ToString().Trim()));
                    cmd.Parameters.Add(new SqlParameter("@query", query.ToString().Trim()));
                    cmd.Parameters.Add(new SqlParameter("@Column", uniqueid.ToString().Trim()));
                    cmd.Parameters.Add(new SqlParameter("@Value", uniquevalue.ToString().Trim()));
                    cmd.Parameters.Add(new SqlParameter("@widget", widget.Trim()));
                    cmd.Parameters.Add(new SqlParameter("@Error", SqlDbType.Int));
                    cmd.Parameters["@Error"].Direction = ParameterDirection.Output;
                    try
                    {
                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        cmd.ExecuteNonQuery();
                        Error = Convert.ToInt32(cmd.Parameters["@Error"].Value.ToString());
                        if (Error == 4 && leadid == "21" && submit == "category")
                        {
                            if (!string.IsNullOrEmpty(sussesssms))
                                TempData["SuccessMessage"] = sussesssms;
                            else
                                TempData["SuccessMessage"] = "Thank You";
                            sussesssms = "1";
                        }
                    }
                    catch (Exception ex)
                    {
                        log.LogMe(ex);
                    }
                    finally
                    {
                        //db.Database.Connection.Close();
                    }
                }
                var resultdata = new { Error = Error, sussesssms = sussesssms };
                return Json(resultdata, JsonRequestBehavior.AllowGet);
                // return Redirect("/");
            }
        }
        public ActionResult AutoBinddatadrop(string query)
        {
            List<Listdropbind> dropbind = new List<Listdropbind>();
            string drop1 = "";
            int drop2 = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[Client_Binddatadrop]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@query", query));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    dropbind = ((IObjectContextAdapter)db).ObjectContext.Translate<Listdropbind>(reader).ToList();
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    //db.Database.Connection.Close();
                }
                drop1 += "<option value=''>Please select</option>";
                int i = 1;
                foreach (var item in dropbind.OrderBy(m => m.drptext))
                {
                    drop1 += "<option value=" + item.drpvalue + ">" + item.drptext + "</option>";
                    i++;
                }
                drop2 = dropbind.Count;
                var result = new { drop1 = drop1, drop2 = drop2 };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #endregion
    }
}
