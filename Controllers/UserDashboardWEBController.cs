﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vconnect.Models;
using System.Data.SqlClient;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Web.Helpers;
using Vconnect.Common;
using System.Text.RegularExpressions;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Net;
using System.Net.Mail;
using System.Configuration;
using Facebook;
using Newtonsoft.Json.Linq;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using adamprescott.net.TweetConsole;
using System.Text;
using System.Security.Cryptography;
using Google.GData.Client;
using Google.GData.Contacts;
using Google.Contacts;
using System.Runtime.Serialization.Json;
using DotNetOpenAuth.Messaging;
using SD = System.Drawing;
using MailChimp.Types;
namespace Vconnect.Controllers
{
    public class UserDashboardWebController : Controller
    {
        private static string locState = string.Empty, locSearchLocation = string.Empty;
        private string rturl = string.Empty;
        int UserId = 0;
        string oauth_consumer_key = ConfigurationManager.AppSettings["twitterAppID"].ToString().Trim(); // "96ZV9fuJQnQLdrRSSWJdVQ";
        string oauth_consumer_secret = ConfigurationManager.AppSettings["twitterAppSecret"].ToString().Trim();
        string oauth_token = "";
        string oauth_token_secret = "";
        private TwitterConsumer _twitter;
        private string clientID = ConfigurationManager.AppSettings["gmailclientid"].ToString().Trim();
        private string clientSecret = ConfigurationManager.AppSettings["gmailclientsecret"].ToString().Trim();
        private string redirectURIs = ConfigurationManager.AppSettings["gmailredirecturl"].ToString().Trim();
        public string strTwiterFollowers { get; set; }
        public string strTwiterFriends { get; set; }
        public string authHeader = string.Empty;
        public string resource_url = string.Empty;
        private string url;
        private GoogleId gId;
        UserSession objUS = new UserSession();
        //
        // GET: /UserDashboardWEB/
        #region image path
        public string webconfigSourceImagesRootPath = ConfigurationManager.AppSettings["SourceImagesRootPath"].ToString();
        public string webconfigDestinationImagesRootPath = ConfigurationManager.AppSettings["DestinationImagesRootPath"].ToString();
        #endregion
        public ActionResult Index()
        {
            return View();
        }
        

        #region change password

        [HttpGet]
        public ActionResult WebChangePassword()
        {
            UserDashboardWebModel UserDbWEBModel = new UserDashboardWebModel();
            Int32 userid = 0;
            if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
            {
                userid = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                UserDbWEBModel.UserId = userid;
            }
            else
            {
                return RedirectToAction("Login", "Account", new { rturl = "UserDashboardWeb/UserDetailsWeb" });
            }
            return View();
        }

        [HttpPost]
        public ActionResult WebChangePassword(UserDashboardWebModel UserDbWEBModel, FormCollection form)
        {

            Int32 userid = 0;
            string msg = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                {
                    userid = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                    UserDbWEBModel.UserId = userid;
                }
                int result = 0;

                string pwd = form["pwd"].ToString();
                string confirmpwd = form["confirmpwd"].ToString();
                // UserDashboardWebModel UserDbWEBModel = new UserDashboardWebModel();
                if (UserDbWEBModel.changePassword.OldPassword == null || UserDbWEBModel.changePassword.OldPassword == "")
                {
                    msg = "Please enter old password";
                    ViewBag.msg = "Please enter old password";
                }

                else
                    if (pwd == null || pwd == "")
                    {
                        msg = "Please enter new  password";
                        ViewBag.msg = "Please enter new  password";
                    }
                    else
                        if (confirmpwd == null || confirmpwd == "")
                        {
                            msg = "Plesae enter confirm password";
                            ViewBag.msg = "Plesae enter confirm password";
                        }
                if (pwd != null && UserDbWEBModel.changePassword.OldPassword != null && confirmpwd != null)
                {
                    if (pwd != confirmpwd)
                    {
                        msg = "New Password And Confirm Password must be equal";

                    }
                    else
                    {
                        result = UserDbWEBModel.WebChangePassword(UserDbWEBModel.changePassword, pwd);
                        if (result == 0)
                        {
                            msg = "Password Updated Successfully..Thankyou";
                        }
                        else if (result == 2)
                        {
                            msg = "Old Password does not match! try again.";
                        }
                        else
                        {
                            msg = "An error occurred. Please try again.";
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            return Content(msg);
        }

        #endregion

        #region validaion
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetProductByCategory(int? Stateid)
        {
            //return Json(Vconnect.Common.Utility.GetSelectListCity(Stateid.Value), JsonRequestBehavior.AllowGet);
            if (Stateid.HasValue)
            {
                return Json(Vconnect.Common.Utility.GetSelectListCity(Stateid.Value), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        //for client side phone and alternate phone number check
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult phonenumbercheck(string phonenumber)
        {
            int d = 4;
            if (!string.IsNullOrEmpty(phonenumber.Trim().TrimEnd().TrimStart()) && !string.IsNullOrWhiteSpace(phonenumber.Trim().TrimEnd().TrimStart()))
            {
                int result = Vconnect.Common.Utility.isValidPhone(phonenumber);

                string sContactNumber = phonenumber;
                string subSection = sContactNumber.Substring(0, 1);

                //if (subSection != "0")
                //{
                //    d = 3;
                //}
                //else if (result == 0)
                //{
                //    d = 2;
                //}
                if (result == 0)
                {
                    d = 2;
                }
                else
                {
                    d = 4;
                }
            }
            else
            {
                d = 4;
            }

            return Json(d, JsonRequestBehavior.AllowGet);
        }


        //for client side email and alternate email check
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult emailcheck(string email)
        {
            int d = 5;
            Regex rgx = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
            if (!string.IsNullOrEmpty(email.Trim().TrimEnd().TrimStart()) && !string.IsNullOrWhiteSpace(email.Trim().TrimEnd().TrimStart()))
            {
                if (!rgx.IsMatch(email))
                {
                    d = 1;
                }
            }
            else
            {
                d = 5;
            }
            return Json(d, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Get all the Users Infomation
        public ActionResult UserDetailsWeb()
        {
            Int32 userid = 0;
            UserDashboardWebModel UserDbWEBModel = new UserDashboardWebModel();
            if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
            {
                userid = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                UserDbWEBModel.UserId = userid;
                return RedirectToAction("getUserDetails", new { userid = userid });
            }
            else
            {
                return RedirectToAction("Login", "Account", new { rturl = "UserDashboardWeb/UserDetailsWeb" });
            }
            // return View(UserDbWEBModel);
        }
        //public ActionResult getUserDetails(Int64 userid)
        //{
        //    UserDashboardWebModel UserDbWEBModel = new UserDashboardWebModel();
        //    UserInfo userInfo = new UserInfo();
        //    string logintype = "";
        //    if (!string.IsNullOrEmpty(objUS.SV_VCLoginType))
        //        logintype = objUS.SV_VCLoginType.ToString();
        //    if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
        //        userInfo.loginid = int.Parse(objUS.SV_VCUserContentID.ToString());
        //    else
        //        userInfo.loginid = 0;
        //    userInfo = UserDbWEBModel.getUserInfo(userid, 1, logintype, userInfo.loginid);
        //    userInfo.userid = userid;
        //    if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
        //        userInfo.loginid = int.Parse(objUS.SV_VCUserContentID.ToString());
        //    else
        //        userInfo.loginid = 0;
        //    int i = 0, p = 0;
        //    if (userInfo.userDetailPercentage.Count >= 1)
        //    {
        //        if (userInfo.userDetailPercentage.FirstOrDefault().pwd != null) i++;
        //        if (userInfo.userDetailPercentage.FirstOrDefault().lastname != null) i++;
        //        if (userInfo.userDetailPercentage.FirstOrDefault().description != null) i++;
        //        if (userInfo.userDetailPercentage.FirstOrDefault().contactname != null) i++;
        //        if (userInfo.userDetailPercentage.FirstOrDefault().cityid != null) i++;
        //        if (userInfo.userDetailPercentage.FirstOrDefault().stateid != null) i++;
        //        if (userInfo.userDetailPercentage.FirstOrDefault().phone != null) i++;
        //        if (userInfo.userDetailPercentage.FirstOrDefault().email != null) i++;
        //        if (userInfo.userDetailPercentage.FirstOrDefault().sex != null) i++;
        //        if (userInfo.userDetailPercentage.FirstOrDefault().photo != null && userInfo.userDetailPercentage.FirstOrDefault().photo != "images/no-images.jpg") i++;
        //        if (userInfo.userDetailPercentage.FirstOrDefault().dateofbirth != null) i++;
        //        if (userInfo.userDetailPercentage.FirstOrDefault().isemailverified != null) i++;
        //        if (userInfo.userDetailPercentage.FirstOrDefault().ismobileverified != null) i++;
        //        if (userInfo.userDetailPercentage.FirstOrDefault().isverified != null) i++;
        //        double l = Convert.ToDouble(i);
        //        double k = (l / 14) * 100;
        //        p = Convert.ToInt16(k);
        //    }
        //    userInfo.percentage = p;
        //    Session["Description"] = userInfo.userDetail.description;
        //    return View("UserProfile", userInfo);
        //}
        //menu update when apply for active
        public ActionResult usersinfo(string userid)
        {


            return null;
        }
        public ActionResult getUserDetails(string userid)
        {
            UserDashboardWebModel UserDbWEBModel = new UserDashboardWebModel();
            UserInfo userInfo = new UserInfo();
            int IDurl = 0;
            if (int.TryParse(userid, out IDurl))
            {
                userid = IDurl.ToString().ToLower();
                Response.Clear(); Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", Request.Url.AbsoluteUri.ToString().ToLower().Remove(Request.Url.AbsoluteUri.ToString().ToLower().IndexOf("user")) + "user/" + UserDbWEBModel.getUserContentid(userid)); Response.End();
                return null; //return new HttpStatusCodeResult(301, "301 Moved Permanently");
            }
            else
            {
                userid = (UserDbWEBModel.getUserContentid(userid).ToString().ToLower());
            }
            if (!string.IsNullOrEmpty(userid) && userid != "0")
            {
                if (Request.QueryString["ref"] != null && string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                {
                    return RedirectToAction("Login", "Account", new { rturl = "UserDashboardWeb/getUserDetails?userid=" + userid + "$" + userid + "_gmail_follow" });
                }
               
                string logintype = "";
                if (!string.IsNullOrEmpty(objUS.SV_VCLoginType))
                    logintype = objUS.SV_VCLoginType.ToString();
                if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                    userInfo.loginid = int.Parse(objUS.SV_VCUserContentID.ToString());
                else
                    userInfo.loginid = 0;
                //userInfo = UserDbWEBModel.getUserInfo(userInfo.loginid, 1, logintype, userInfo.loginid);
                userInfo = UserDbWEBModel.getUserInfo(int.Parse(userid), 1, logintype, userInfo.loginid);
                userInfo.userid = int.Parse(userid);
                if (!string.IsNullOrEmpty(objUS.SV_VCUserName))
                    userInfo.loginname = objUS.SV_VCUserName.ToString();
                else
                    userInfo.loginname = "";
                //userInfo.userid = userInfo.loginid;
                if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                    userInfo.loginid = int.Parse(objUS.SV_VCUserContentID.ToString());
                else
                    userInfo.loginid = 0;
                int i = 0, p = 0;
                if (userInfo.userDetailPercentage != null && userInfo.loginid != 0 && userInfo.loginid == userInfo.userid && userInfo.userDetailPercentage.Count >= 1)
                {
                    if (!string.IsNullOrEmpty(userInfo.userDetailPercentage.FirstOrDefault().pwd)) i++;
                    if (!string.IsNullOrEmpty(userInfo.userDetailPercentage.FirstOrDefault().lastname)) i++;
                    if (!string.IsNullOrEmpty(userInfo.userDetailPercentage.FirstOrDefault().description)) i++;
                    if (!string.IsNullOrEmpty(userInfo.userDetailPercentage.FirstOrDefault().contactname)) i++;
                    if (!string.IsNullOrEmpty(userInfo.userDetailPercentage.FirstOrDefault().cityid.ToString()) && (userInfo.userDetailPercentage.FirstOrDefault().cityid != 0)) i++;
                    if (!string.IsNullOrEmpty(userInfo.userDetailPercentage.FirstOrDefault().stateid.ToString()) && (userInfo.userDetailPercentage.FirstOrDefault().stateid != 0)) i++;
                    if (!string.IsNullOrEmpty(userInfo.userDetailPercentage.FirstOrDefault().phone)) i++;
                    if (!string.IsNullOrEmpty(userInfo.userDetailPercentage.FirstOrDefault().email)) i++;
                    if (!string.IsNullOrEmpty(userInfo.userDetailPercentage.FirstOrDefault().sex)) i++;
                    //if (userInfo.userDetailPercentage.FirstOrDefault().photo != null) i++;
                    if (!string.IsNullOrEmpty(userInfo.userDetailPercentage.FirstOrDefault().photo) && userInfo.userDetailPercentage.FirstOrDefault().photo != "images/no-images.jpg") i++;
                    if (userInfo.userDetailPercentage.FirstOrDefault().dateofbirth != null) i++;
                    if (!string.IsNullOrEmpty(userInfo.userDetailPercentage.FirstOrDefault().isemailverified.ToString()) && (userInfo.userDetailPercentage.FirstOrDefault().isemailverified != false)) i++;
                    if (!string.IsNullOrEmpty(userInfo.userDetailPercentage.FirstOrDefault().ismobileverified.ToString()) && userInfo.userDetailPercentage.FirstOrDefault().ismobileverified != false) i++;
                    if (!string.IsNullOrEmpty(userInfo.userDetailPercentage.FirstOrDefault().isverified.ToString()) && userInfo.userDetailPercentage.FirstOrDefault().isverified != 0) i++;
                    double l = Convert.ToDouble(i);
                    double k = (l / 14) * 100;
                    p = Convert.ToInt16(k);
                }
                userInfo.percentage = p;
                if (userInfo.userFollowid != null && (userInfo.userFollowid.err == 0 || userInfo.userFollowid.err == 1))
                    Session["chkbtn"] = userInfo.userFollowid.err;
                return View("UserProfile", userInfo);
            }
            return RedirectPermanent("/");
        }

        public JsonResult Headermenucount(string item, string userid)
        {
            UserDashboardWebModel UserDbWEBModel = new UserDashboardWebModel();
            UserInfo userInfo = new UserInfo();
            int IDurl = 0;
            if (int.TryParse(userid, out IDurl))
            {
                userid = IDurl.ToString().ToLower();
            }
            else
            {
                userid = (UserDbWEBModel.getUserContentid(userid).ToString().ToLower());
            }
            string itemid = "";
            if (!string.IsNullOrEmpty(item))
                itemid = item;
            string htmlcodelist = "";
            int loginid = 0;
            if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                loginid = int.Parse(objUS.SV_VCUserContentID.ToString());
            if (itemid != "")
            {
                List<menucount> usermenucount = new List<menucount>();
                using (var db = new VconnectDBContext29())
                {
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[prc_get_menucount]";
                    cmd.CommandTimeout = 600;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@userid", userid));
                    try
                    {
                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        usermenucount = ((IObjectContextAdapter)db).ObjectContext.Translate<menucount>(reader).ToList();
                        userInfo.usermenucount = usermenucount;

                        if (itemid == "Activity" && usermenucount.FirstOrDefault().activitycount > 0)
                        {
                            htmlcodelist = "headermenu1";
                        }
                        else if (itemid == "Likes" && usermenucount.FirstOrDefault().likecount > 0)
                        {
                            htmlcodelist = "headermenu2";
                        }
                        else if (itemid == "Reviews" && usermenucount.FirstOrDefault().reviewcount > 0)
                        {
                            htmlcodelist = "headermenu3";
                        }
                        else if (itemid == "Followers" && usermenucount.FirstOrDefault().followercount > 0)
                        {
                            htmlcodelist = "headermenu4";
                        }
                        else if (itemid == "Following" && usermenucount.FirstOrDefault().followingcount > 0)
                        {
                            htmlcodelist = "headermenu5";
                        }
                        else if (itemid == "Saved" && usermenucount.FirstOrDefault().savedcount > 0)
                        {
                            htmlcodelist = "headermenu6";
                        }
                        else if (itemid == "Photos" && usermenucount.FirstOrDefault().photocount > 0)
                        {
                            htmlcodelist = "headermenu7";
                        }
                        else
                        {
                            htmlcodelist = "";
                        }
                    }
                    catch (Exception exp)
                    {
                        log.LogMe(exp);
                    }
                    finally { db.Database.Connection.Close(); }
                }
            }
            return Json(htmlcodelist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult menucount(string item, string userid, string claimed)
        {
            UserDashboardWebModel UserDbWEBModel = new UserDashboardWebModel();
            UserInfo userInfo = new UserInfo();
            int IDurl = 0;
            if (int.TryParse(userid, out IDurl))
            {
                userid = IDurl.ToString().ToLower();
            }
            else
            {
                userid = (UserDbWEBModel.getUserContentid(userid).ToString().ToLower());
            }
            string itemid = "Activity";
            if (!string.IsNullOrEmpty(item))
                itemid = item;
            string htmlcodelist = "";
            int loginid = 0;
            if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                loginid = int.Parse(objUS.SV_VCUserContentID.ToString());
            List<menucount> usermenucount = new List<menucount>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_menucount]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    usermenucount = ((IObjectContextAdapter)db).ObjectContext.Translate<menucount>(reader).ToList();
                    userInfo.usermenucount = usermenucount;
                    if (itemid == "Activity")
                        htmlcodelist += @"<li class='active' id='Activity' style='line-height: 2.1875rem;'><a  onclick='Display(&#39;Activity&#39;);'>";
                    else
                        htmlcodelist += @"<li class='' id='Activity' style='line-height: 2.1875rem;'><a  onclick='Display(&#39;Activity&#39;);'>";
                    htmlcodelist += @"<i class='icon-flag'></i> Activity</a></li>";

                    if (itemid == "Likes")
                        htmlcodelist += @"<li id='Likes' class='active'><a onclick='Display(&#39;Likes&#39;);'>";
                    else
                        htmlcodelist += @"<li id='Likes' class=''><a onclick='Display(&#39;Likes&#39;);'>";
                    htmlcodelist += @"<span class='counter'>" + usermenucount.FirstOrDefault().likecount + "</span><br>Likes</a></li>";

                    if (itemid == "Reviews")
                        htmlcodelist += @"<li class='active' id='Reviews'><a  onclick='Display(&#39;Reviews&#39;)'>";
                    else
                        htmlcodelist += @"<li class='' id='Reviews'><a  onclick='Display(&#39;Reviews&#39;)'>";
                    htmlcodelist += @"<span class='counter'>" + usermenucount.FirstOrDefault().reviewcount + "</span><br>Reviews</a></li>";

                    if (itemid == "Followers")
                        htmlcodelist += @"<li id='Followers' class='active'><a onclick='Display(&#39;Followers&#39;)'>";
                    else
                        htmlcodelist += @"<li id='Followers' class=''><a onclick='Display(&#39;Followers&#39;)'>";
                    htmlcodelist += @"<span class='counter' id='Followerscnt'>" + usermenucount.FirstOrDefault().followercount + "</span><br>Followers</a></li>";

                    //if (userid != 0 && loginid == userid)
                    //{
                    if (itemid == "Following")
                        htmlcodelist += @"<li id='Following' class='active'><a onclick='Display(&#39;Following&#39;)'>";
                    else
                        htmlcodelist += @"<li id='Following' class=''><a onclick='Display(&#39;Following&#39;)'>";
                    htmlcodelist += @"<span class='counter'>" + usermenucount.FirstOrDefault().followingcount + "</span><br>Following</a></li>";

                    //}
                    //else
                    //{
                    //    if (itemid == "Following")
                    //        htmlcodelist += @"<li id='Following' class='active' style='display:none;'></li>";
                    //    else
                    //        htmlcodelist += @"<li id='Following' class='' style='display:none;'></li>";
                    //}
                    if (loginid != 0)
                    {
                        if (loginid == int.Parse(userid))
                        {
                            if (itemid == "Saved")
                                htmlcodelist += @"<li id='Saved' class='active'><a onclick='Display(&#39;Saved&#39;)'>";
                            else
                                htmlcodelist += @"<li id='Saved' class=''><a onclick='Display(&#39;Saved&#39;)'>";
                            htmlcodelist += @"<span class='counter'>" + usermenucount.FirstOrDefault().savedcount + "</span><br>Saved</a></li>";

                        }
                        else
                        {
                            if (itemid == "Saved")
                                htmlcodelist += @"<li id='Saved' class='active' style='display:none;'></li>";
                            else
                                htmlcodelist += @"<li id='Saved' class='' style='display:none;'></li>";
                        }
                    }
                    else
                    {
                        if (itemid == "Saved")
                            htmlcodelist += @"<li id='Saved' class='active' style='display:none;'></li>";
                        else
                            htmlcodelist += @"<li id='Saved' class='' style='display:none;'></li>";
                    }
                    htmlcodelist += @"<li id='Badges' class='' style='display:none;'><a onclick='Display(&#39;Badges&#39;)'>";
                    htmlcodelist += @"<span class='counter'>11</span><br>Badges</a></li>";

                    if (itemid == "Photos")
                        htmlcodelist += @"<li id='Photos' class='active'><a onclick='Display(&#39;Photos&#39;)'>";
                    else
                        htmlcodelist += @"<li id='Photos' class=''><a onclick='Display(&#39;Photos&#39;)'>";
                    htmlcodelist += @"<span class='counter'>" + usermenucount.FirstOrDefault().photocount + "</span><br>Photos</a></li>";
                    //if (loginid != 0)
                    //{
                    if (!string.IsNullOrEmpty(claimed) && claimed != "0" && userInfo.usermenucount.FirstOrDefault().usertype1 == 59)
                        htmlcodelist += @"<li id='Businesses' class='single-line right'><a data-dropdown='dropBiz'><i class='icon-briefcase'></i> My Businesses</a></li>";
                    //}
                    //else {
                    //    if (!string.IsNullOrEmpty(claimed) && claimed != "0")
                    //        htmlcodelist += @"<li class='single-line right'><a ><i class='icon-briefcase'></i> My Businesses</a></li>";
                    //}

                }
                catch (Exception exp)
                {
                    log.LogMe(exp);
                }
                finally { db.Database.Connection.Close(); }
            }
            return Json(htmlcodelist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListDataWEB(int? Page, int? Type, string userid)
        {
            UserDashboardWebModel UserDbWEBModel = new UserDashboardWebModel();
            UserInfo userInfo = new UserInfo();
            int IDurl = 0;
            if (int.TryParse(userid, out IDurl))
            {
                userid = IDurl.ToString().ToLower();
            }
            else
            {
                userid = (UserDbWEBModel.getUserContentid(userid).ToString().ToLower());
            }
            int loginid = 0;
            if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                loginid = int.Parse(objUS.SV_VCUserContentID.ToString());
            string htmlcodelist = "";
            int viewcnt = 0;
            int menucnt = 0;
            string logintype = "";
            string parameter = "";
            //if (!string.IsNullOrEmpty(objUS.SV_VCLoginType))
            //    logintype = objUS.SV_VCLoginType.ToString();
            if (!string.IsNullOrEmpty(objUS.SV_VCLoginType))
                logintype = objUS.SV_VCLoginType.ToString();
            //List<UserInfo> userInfo = new List<UserInfo>();
            List<menucount> usermenucount = new List<menucount>();
            List<UserInfo> userInfoParent = new List<UserInfo>();
            List<UserBusiness> userBusiness = new List<UserBusiness>();
            List<UserReviewBusiness> userReviewBusiness = new List<UserReviewBusiness>();
            List<UserLikeBusiness> userLikeBusiness = new List<UserLikeBusiness>();
            List<UserSaveBusiness> userSaveBusiness = new List<UserSaveBusiness>();
            List<UserActivity> userActivity = new List<UserActivity>();
            List<Userfollowers> userfollowers = new List<Userfollowers>();
            List<Userfollowersuggestion> userfollowersuggestion = new List<Userfollowersuggestion>();
            List<Userfolloweing> userfolloweing = new List<Userfolloweing>();
            List<UserPhotoList> userPhotoList = new List<UserPhotoList>();
            List<UserClaimedBusiness> userClaimedBusiness = new List<UserClaimedBusiness>();
            List<UserDetailPercentage> userDetailPercentage = new List<UserDetailPercentage>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_get_menucount]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@userid", userid));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    usermenucount = ((IObjectContextAdapter)db).ObjectContext.Translate<menucount>(reader).ToList();
                    userInfo.usermenucount = usermenucount;
                }
                catch (Exception exp)
                {
                    log.LogMe(exp);
                }
                finally { db.Database.Connection.Close(); }
            }
            //int currenttype = Type ?? 5;
            int followercount = int.Parse(usermenucount.FirstOrDefault().followercount.ToString());
            int followingcount = int.Parse(usermenucount.FirstOrDefault().followingcount.ToString());
            if (Type == 8 && followingcount == 0)
            {
                Type = 7;
            }
            if (Type == 7 && followingcount != 0)
            {
                Type = 8;
            }
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                //cmd.CommandText = Type == 1 ? "[dbo].[prc_get_user_like_suggestion_details]" : Type == 2 ? "[dbo].[prc_get_user_review_suggestion_details]" : Type == 3 ? "[dbo].[prc_get_user_saved_suggestion_details]" : Type == 4 ? "[dbo].[prc_get_user_photo_suggestion_details]" : Type == 5 ? "[dbo].[prc_get_user_activtyWeb_details_test]" : Type == 6 ? "[dbo].[prc_listfollowers]" : Type == 7 ? "[dbo].[prc_followsuggestionsNew]" : "[dbo].[prc_listfollowings]";
                //cmd.CommandText = Type == 1 ? "[dbo].[prc_get_user_like_suggestion_details]" : Type == 2 ? "[dbo].[prc_get_user_review_suggestion_details]" : Type == 3 ? "[dbo].[prc_get_user_saved_suggestion_details]" : Type == 4 ? "[dbo].[prc_get_user_photo_suggestion_details]" : Type == 5 ? "[dbo].[prc_get_user_activtyWeb_details_test]" : Type == 6 ? "[dbo].[prc_listfollowers]" : Type == 7 ? "[dbo].[prc_followsuggestions]" : "[dbo].[prc_listfollowings]";
                cmd.CommandText = Type == 1 ? "[dbo].[prc_get_user_like_suggestion_details]" : Type == 2 ? "[dbo].[prc_get_user_review_suggestion_details]" : Type == 3 ? "[dbo].[prc_get_user_saved_suggestion_details]" : Type == 4 ? "[dbo].[prc_get_user_photo_suggestion_details]" : Type == 5 ? "[dbo].[prc_get_user_activtyWeb_details_test]" : Type == 6 ? "[dbo].[prc_listfollowers_url]" : Type == 7 ? "[dbo].[prc_followsuggestions_url]" : "[dbo].[prc_listfollowings_url]";
                cmd.CommandTimeout = 600;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                if (Type != 6)
                    cmd.Parameters.Add(new SqlParameter("@userid", userid));
                if (Type == 7)
                {
                    cmd.Parameters.Add(new SqlParameter("@logintype", logintype));
                    cmd.Parameters.Add(new SqlParameter("@PageSize", 10));
                }
                if (Type == 8)
                {
                    cmd.Parameters.Add(new SqlParameter("@curUser", loginid));
                    cmd.Parameters.Add(new SqlParameter("@PageSize", 10));
                }
                if (Type == 5)
                {
                    cmd.Parameters.Add(new SqlParameter("@loginid", loginid));
                }
                if (Type == 6)
                {
                    cmd.Parameters.Add(new SqlParameter("@userid", loginid));
                    cmd.Parameters.Add(new SqlParameter("@curUser", userid));
                }
                cmd.Parameters.Add(new SqlParameter("@Page", Page));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();

                    var reader = cmd.ExecuteReader();
                    if (Type == 1)
                    {
                        userLikeBusiness = ((IObjectContextAdapter)db).ObjectContext.Translate<UserLikeBusiness>(reader).ToList();
                        userInfo.userLikeBusiness = userLikeBusiness;
                        viewcnt = userLikeBusiness.Count;
                        menucnt = int.Parse(usermenucount.FirstOrDefault().likecount.ToString());
                        foreach (var unlinking in userLikeBusiness)
                        {
                            if (unlinking.status == 1)
                            {
                                parameter = "&#39;like&#39;," + unlinking.userid + "," + unlinking.businessid + ",&#39;" + unlinking.businessurl + "&#39;";
                                htmlcodelist += @"<li class='like-item'><span class='like-date'>" + Convert.ToDateTime(unlinking.createddate).ToLongDateString() + "</span>";
                                if (unlinking.photo != null)
                                {
                                    htmlcodelist += @"<p><a href='" + ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + unlinking.businessurl + "'><img class='logo' src='" + ConfigurationManager.AppSettings["ImagesRootPath"].ToString() + unlinking.photo + "' alt='logo'> " + @unlinking.businessname + "</a></p></li>";
                                }
                                else
                                {
                                    htmlcodelist += @"<p><a href='" + ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + unlinking.businessurl + "'><img class='logo' src='http://image.vconnect.bz/static/WEB/img/default-biz-logo.jpg' alt='logo'> " + @unlinking.businessname + "</a></p></li>";
                                }
                            }
                            else
                            {
                                htmlcodelist += @"<li><div class='img-container'>";
                                if (unlinking.photo != null)
                                {
                                    htmlcodelist += @"<a href='#'><img src='" + ConfigurationManager.AppSettings["ImagesRootPath"].ToString() + unlinking.photo + "' alt='logo'></a>";
                                }
                                else
                                {
                                    htmlcodelist += @"<a href='#'><img src='http://image.vconnect.bz/static/WEB/img/default-biz-logo.jpg' alt='logo'></a>";
                                }
                                htmlcodelist += @"</div><div class='details-container'><div class='bname'>";
                                htmlcodelist += @"<a href='" + ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + unlinking.businessurl + "'>" + unlinking.businessname + "</a></div><div class='ff'>";
                                htmlcodelist += @"<a href='#'>" + unlinking.city + "</a>, <a href='#'>" + unlinking.state + "</a></div>";
                                parameter = "&#39;like&#39;," + unlinking.userid + "," + unlinking.businessid + ",&#39;" + unlinking.businessurl + "&#39;";
                                if (userid != null && int.Parse(userid) != 0 && unlinking.status == 0)
                                {
                                    htmlcodelist += @"<a id='like1" + unlinking.businessid + "' onclick='fnGetLoginPartial(" + parameter + ");' class='button button-red action-btn like-btn radius' href='javascript:void(0)'><i class='icon-heart'></i>Like</a>";
                                }
                                else
                                {
                                    htmlcodelist += @"<a class='button button-red action-btn like-btn active radius'><i class='icon-ok'></i> Liked</a>";
                                }
                                htmlcodelist += @"</div></li>";
                            }
                        }
                    }

                    else if (Type == 2)
                    {
                        userReviewBusiness = ((IObjectContextAdapter)db).ObjectContext.Translate<UserReviewBusiness>(reader).ToList();
                        userInfo.userReviewBusiness = userReviewBusiness;
                        viewcnt = userReviewBusiness.Count;
                        menucnt = int.Parse(usermenucount.FirstOrDefault().reviewcount.ToString());
                        foreach (var review in userReviewBusiness)
                        {
                            if (review.status == 1)
                            {
                                parameter = "&#39;Review&#39;," + review.userid + "," + review.businessid + ",&#39;" + review.businessurl + "&#39;";
                                htmlcodelist += @"<li class='review-item'><div class='review-header'>";
                                htmlcodelist += @"<span class='bname'><a href='" + ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + review.businessurl + "'>" + review.businessname + "</a></span><span class='rating'>";
                                for (int i1 = 1; i1 <= review.rate; i1++)
                                {
                                    htmlcodelist += @"<i class='icon-star'></i>";
                                }
                                for (int i2 = 1; i2 <= (5 - review.rate); i2++)
                                {
                                    htmlcodelist += @"<i class='icon-star-empty'></i>";
                                }
                                htmlcodelist += @"</span><span class='review-date'>" + Convert.ToDateTime(review.createddate).ToLongDateString() + "</span><br><span class='view-link'><a href='" + ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + review.businessurl + "'><i class='icon-share-alt'></i> View in business page</a></span></div>";
                                if (!string.IsNullOrEmpty(review.details))
                                    htmlcodelist += @"<div class='review-body'><p>" + review.details + ".</p></div>";
                                htmlcodelist += @"</li>";
                            }
                            else
                            {
                                htmlcodelist += @"<li><div class='img-container'>";
                                if (review.photo != null)
                                {
                                    htmlcodelist += @"<a href='#'><img src='" + ConfigurationManager.AppSettings["ImagesRootPath"].ToString() + review.photo + "' alt='logo'></a>";
                                }
                                else
                                {
                                    htmlcodelist += @"<a href='#'><img src='http://image.vconnect.bz/static/WEB/img/category/vc.png' alt='logo'></a>";
                                }
                                htmlcodelist += @"</div><div class='details-container'><div class='bname'>";
                                htmlcodelist += @"<a href='" + ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + review.businessurl + "'>" + review.businessname + "</a></div><div class='ff'>";
                                htmlcodelist += @"<a href='#'>" + review.city + "</a>, <a href='#'>" + review.state + "</a></div>";
                                parameter = "&#39;Review&#39;," + review.userid + "," + review.businessid + ",&#39;" + review.businessurl + "&#39;";
                                //if (userid != null && userid != 0 && review.status == 0)
                                //{
                                //    htmlcodelist += @"<a id='Review1" + review.businessid + "' class='button button-red action-btn like-btn radius' href='javascript:void(o)' onclick='fnGetLoginPartial(" + parameter + ");'><i class='icon-heart'></i>Review</a>";
                                //}
                                //else
                                //{
                                //    htmlcodelist += @"<a class='button button-red action-btn like-btn active radius'><i class='icon-ok'></i> Review</a>";
                                //}
                                if (review.status == 0)
                                {
                                    htmlcodelist += @"<a id='Review1" + review.businessid + "' class='button button-red action-btn like-btn radius' href='" + ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + review.businessurl + "?rturl=" + ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + "UserDashboardWEB/getUserDetails?userid=" + review.userid + "'><i class='icon-heart'></i>Review</a>";
                                }
                                else
                                {
                                    htmlcodelist += @"<a class='button button-red action-btn like-btn active radius' href='" + ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + review.businessurl + "?rturl=" + ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + "UserDashboardWEB/getUserDetails?userid=" + review.userid + "'><i class='icon-ok'></i> Review</a>";
                                }
                                htmlcodelist += @"</div></li>";
                            }
                        }
                    }

                    else if (Type == 3)
                    {
                        userSaveBusiness = ((IObjectContextAdapter)db).ObjectContext.Translate<UserSaveBusiness>(reader).ToList();
                        userInfo.userSaveBusiness = userSaveBusiness;
                        viewcnt = userSaveBusiness.Count;
                        menucnt = int.Parse(usermenucount.FirstOrDefault().savedcount.ToString());
                        foreach (var saved in userSaveBusiness)
                        {
                            if (saved.status == 1)
                            {
                                parameter = "&#39;save&#39;," + saved.userid + "," + saved.businessid + ",&#39;" + saved.businessurl + "&#39;";
                                htmlcodelist += @"<li class='saved-item'>";
                                htmlcodelist += @"<span class='saved-date'>" + Convert.ToDateTime(saved.createddate).ToLongDateString() + "</span>";
                                if (saved.photo != null)
                                {
                                    htmlcodelist += @"<a href='#'><img src='" + ConfigurationManager.AppSettings["ImagesRootPath"].ToString() + saved.photo + "' alt='logo'></a>";
                                }
                                else
                                {
                                    htmlcodelist += @"<a href='#'><img src='http://image.vconnect.bz/static/WEB/img/category/vc.png' alt='logo'></a>";
                                }
                                htmlcodelist += @"<p class='details'><a href='" + ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + saved.businessurl + "'>" + saved.businessname + "</a><br>";
                                htmlcodelist += @"<i class='icon-address'></i> " + saved.address1 + ".<br>";
                                if (!string.IsNullOrEmpty(saved.email))
                                    htmlcodelist += @"<i class='icon-mail'></i> " + saved.email + "<br>";
                                if (!string.IsNullOrEmpty(saved.phone))
                                    htmlcodelist += @"<i class='icon-phone'></i> " + saved.phone + "</p></li>";
                            }
                            else
                            {
                                htmlcodelist += @"<li><div class='img-container'>";
                                if (saved.photo != null)
                                {
                                    htmlcodelist += @"<a href='#'><img src='" + ConfigurationManager.AppSettings["ImagesRootPath"].ToString() + saved.photo + "' alt='logo'></a>";
                                }
                                else
                                {
                                    htmlcodelist += @"<a href='#'><img src='http://image.vconnect.bz/static/WEB/img/category/vc.png' alt='logo'></a>";
                                }
                                htmlcodelist += @"</div><div class='details-container'><div class='bname'>";
                                htmlcodelist += @"<a href='" + ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + saved.businessurl + "'>" + saved.businessname + "</a></div><div class='ff'>";
                                htmlcodelist += @"<a href='#'>" + saved.city + "</a>, <a href='#'>" + saved.state + "</a></div>";
                                parameter = "&#39;save&#39;," + saved.userid + "," + saved.businessid + ",&#39;" + saved.businessurl + "&#39;";
                                if (userid != null && int.Parse(userid) != 0 && saved.status == 0)
                                {
                                    //htmlcodelist += @"<a class='button button-red action-btn save-btn radius' href='javascript:void(0)' onclick='fnGetLoginPartial('save')' id='save1'><i class='icon-bookmark'></i>Save</a>";
                                    htmlcodelist += @"<a id='save1" + saved.businessid + "' onclick='fnGetLoginPartial(" + parameter + ");' class='button button-red action-btn save-btn radius' href='javascript:void(0)'><i class='icon-bookmark'></i>Save</a>";
                                }
                                else
                                {
                                    htmlcodelist += @"<a class='button button-red action-btn save-btn active radius'><i class='icon-ok'></i> Saved</a>";
                                }
                                htmlcodelist += @"</div></li>";
                            }
                        }
                    }
                    else if (Type == 4)
                    {
                        userPhotoList = ((IObjectContextAdapter)db).ObjectContext.Translate<UserPhotoList>(reader).ToList();
                        userInfo.userPhotoList = userPhotoList;
                        viewcnt = userPhotoList.Count;
                        menucnt = int.Parse(usermenucount.FirstOrDefault().photocount.ToString());
                        foreach (var photoview in userPhotoList)
                        {
                            if (photoview.photosmax != null)
                            {
                                htmlcodelist += @"<li><a class='th' data-lightbox='biz-photos' href='" + ConfigurationManager.AppSettings["ImagesRootPath"].ToString() + photoview.photosmax + "' title=''>";
                                htmlcodelist += @"<img src='" + ConfigurationManager.AppSettings["ImagesRootPath"].ToString() + photoview.photosmax + "' alt='" + photoview.businessname + "'></a></li>";
                            }
                        }
                    }
                    else if (Type == 5)
                    {
                        userActivity = ((IObjectContextAdapter)db).ObjectContext.Translate<UserActivity>(reader).ToList();
                        userInfo.userActivity = userActivity;
                        viewcnt = userActivity.Count;
                        menucnt = int.Parse(usermenucount.FirstOrDefault().activitycount.ToString());
                        var displaytest = loginid != 0 && int.Parse(userid) == loginid ? "You" : usermenucount.FirstOrDefault().contactname;

                        foreach (var activityview in userActivity)
                        {
                            if (activityview.activity == 1)
                            {
                                htmlcodelist += @"<li class='activity-item'><span class='activity-date'>" + Convert.ToDateTime(activityview.createddate).ToLongDateString() + "</span><p class='activity'>" + displaytest + " liked <a href='" + ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + activityview.businessurl + "'>" + activityview.businessname + "</a></p></li>";
                            }
                            else if (activityview.activity == 5 && loginid != 0 && loginid == int.Parse(userid))
                            {
                                htmlcodelist += @"<li class='activity-item'><span class='activity-date'>" + Convert.ToDateTime(activityview.createddate).ToLongDateString() + "</span><p class='activity'>" + displaytest + " saved <a href='" + ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + activityview.businessurl + "'>" + activityview.businessname + "</a></p></li>";
                            }
                            else if (activityview.activity == 3)
                            {
                                htmlcodelist += @"<li class='activity-item'><span class='activity-date'>" + Convert.ToDateTime(activityview.createddate).ToLongDateString() + "</span><p class='activity'>" + displaytest + " rated and reviewed <a href='" + ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + activityview.businessurl + "'>" + activityview.businessname + "</a></p></li>";
                            }
                            //else if (activityview.activity == 4)
                            //{
                            //    htmlcodelist += @"<li class='activity-item'><span class='activity-date'>" + Convert.ToDateTime(activityview.createddate).ToLongDateString() + "</span><p class='activity'>" + displaytest + " rated <a href='" + ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + activityview.businessurl + "'>" + activityview.businessname + "</a></p></li>";
                            //}
                            else if (activityview.activity == 2)
                            {
                                htmlcodelist += @"<li class='activity-item'><span class='activity-date'>" + Convert.ToDateTime(activityview.createddate).ToLongDateString() + "</span><p class='activity'>" + displaytest + " added photo <a href='" + ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + activityview.businessurl + "'>" + activityview.businessname + "</a></p></li>";
                            }
                            else if (activityview.activity == 6)
                            {
                                htmlcodelist += @"<li class='activity-item'><span class='activity-date'>" + Convert.ToDateTime(activityview.createddate).ToLongDateString() + "</span>";
                                htmlcodelist += @"<p class='activity'>" + displaytest + " followed <a href='" + ConfigurationManager.AppSettings["ProfilePath"].ToString() + activityview.businessurl + "'>" + activityview.businessname + "</a></p></li>";
                            }
                            else
                            {
                                //htmlcodelist += @"<li class='activity-item'><span class='activity-date'>" + Convert.ToDateTime(activityview.createddate).ToLongDateString() + "</span><p class='activity'><a href='" + ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + activityview.businessurl + "'>" + activityview.businessname + "</a></p></li>";
                            }
                        }
                    }
                    else if (Type == 6)
                    {
                        userfollowers = ((IObjectContextAdapter)db).ObjectContext.Translate<Userfollowers>(reader).ToList();
                        userInfo.userfollowers = userfollowers;
                        viewcnt = userfollowers.Count;
                        menucnt = int.Parse(usermenucount.FirstOrDefault().followercount.ToString());
                        foreach (var followers in userfollowers)
                        {
                            int denf1 = 2;
                            if (denf1 == 1)
                            {
                                htmlcodelist += @"<li><div class='img-container'>";
                                if (followers.photo != null)
                                {
                                    if (followers.photo == "~/App_Themes/Mobile/images/review-img.png")
                                    {
                                        htmlcodelist += @"<a href='"+ ConfigurationManager.AppSettings["ProfilePath"].ToString() + followers.userurl.ToLower() + "'><img src='http://image.vconnect.bz/static/WEB/img/category/vc.png' alt='logo'></a>";
                                    }
                                    else
                                    {
                                        htmlcodelist += @"<a href='"+ ConfigurationManager.AppSettings["ProfilePath"].ToString() + followers.userurl.ToLower() + "'><img src='" + followers.photo + "' alt='logo'></a>";
                                    }
                                }
                                else
                                {
                                    htmlcodelist += @"<a href='"+ ConfigurationManager.AppSettings["ProfilePath"].ToString() + followers.userurl.ToLower() + "'><img src='http://image.vconnect.bz/static/WEB/img/category/vc.png' alt='logo'></a>";
                                }
                                htmlcodelist += @"</div><div class='details-container'><div class='uname'><a href='"+ ConfigurationManager.AppSettings["ProfilePath"].ToString() + followers.userurl.ToLower() + "'>" + followers.name + "</a></div><div class='ff'>";
                                //htmlcodelist += @"<a href='#'>John Doe</a>, <a href='#'>Someone else</a>, and 3 others.</div>";
                                htmlcodelist += @"<a href='#'>" + followers.reviewscnt + " Reviews " + followers.followerscnt + " Followers</div>";
                                parameter = "&#39;follow&#39;," + followers.contentid + "," + loginid + ",&#39;follow_" + followers.contentid + "&#39;";
                                if (followers.isBoth == 1)
                                {
                                    htmlcodelist += @"<a id='follow_" + followers.contentid + "' href='#' class='button follow-button' onclick='fnGetLoginPartial(" + parameter + ");' style='display:none;'><i class='icon-user-add'></i> Follow</a></div></li>";
                                }
                                else
                                {
                                    if (int.Parse(userid) == loginid)
                                        htmlcodelist += @"<a id='follow_" + followers.contentid + "' href='#' class='button follow-button' onclick='fnGetLoginPartial(" + parameter + ");'><i class='icon-user-add'></i> Follow</a></div></li>";
                                    else
                                        htmlcodelist += @"<a id='follow_" + followers.contentid + "' href='#' class='button follow-button'><i class='icon-user-add'></i> Follow</a></div></li>";

                                    // htmlcodelist += @"<button class='unfollow-btn button center' onclick='fnGetLoginPartial(" + parameter + ");'><i class='icon-ok'></i>Following</button></div></li>";
                                    //htmlcodelist += @"<a href='#' class='button follow-button'><i class='icon-ok'> Following</a></div></li>";
                                }
                            }
                            else
                            {
                                if (followers.contentid == int.Parse(userid) && followers.contentid == loginid)
                                {
                                    htmlcodelist += "";
                                }
                                else
                                {
                                    parameter = "&#39;follow&#39;," + followers.contentid + "," + loginid + ",&#39;follow_" + followers.contentid + "&#39;";
                                    string parameter1 = "";
                                    parameter1 = "&#39;follow&#39;," + followers.contentid + "," + loginid + ",&#39;following_" + followers.contentid + "&#39;";
                                    //if (followers.isBoth == 1)
                                    //{
                                    //    if (followers.contentid == loginid)
                                    //        htmlcodelist += @"<li class='follower-item' style='padding-top:1.93rem'><a id='follow_" + followers.contentid + "' href='#' class='button follow-btn' onclick='fnGetLoginPartial(" + parameter + ");' style='display:none;'><i class='icon-user-add'></i> Follow</a>";
                                    //    else
                                    //        htmlcodelist += @"<li class='follower-item' style='padding-top:1.93rem'><a id='following_" + followers.contentid + "' href='#' class='unfollow-btn button center' onclick='fnGetLoginPartial(" + parameter1 + ");'><i class='icon-ok'></i> Following</a>";
                                    //}
                                    //else
                                    //{
                                    //    if (followers.contentid == loginid)
                                    //        htmlcodelist += @"<li class='follower-item' style='padding-top:1.93rem'><a id='follow_" + followers.contentid + "' href='#' class='button follow-btn' onclick='fnGetLoginPartial(" + parameter + ");' style='display:none;'><i class='icon-user-add'></i> Follow</a>";
                                    //    else
                                    //        htmlcodelist += @"<li class='follower-item' style='padding-top:1.93rem'><a id='follow_" + followers.contentid + "' href='#' class='button follow-btn' onclick='fnGetLoginPartial(" + parameter + ");'><i class='icon-user-add'></i> Follow</a>";
                                    //}
                                    if (followers.isBoth == 2 && followers.contentid != loginid)
                                        htmlcodelist += @"<li class='follower-item' style='padding-top:1.93rem'><a id='following_" + followers.contentid + "' href='#' class='unfollow-btn button center' onclick='fnGetLoginPartial(" + parameter1 + ");'><i class='icon-ok'></i> Following</a>";
                                    else if (int.Parse(userid) != 0 && int.Parse(userid) != loginid && followers.contentid != loginid && followers.isBoth != 1 && followers.isCurFollow != 1)
                                        htmlcodelist += @"<li class='follower-item' style='padding-top:1.93rem'><a id='follow_" + followers.contentid + "' href='#' class='button follow-btn' onclick='fnGetLoginPartial(" + parameter + ");'><i class='icon-user-add'></i> Follow</a>";
                                    else if (followers.isBoth == 4)
                                        htmlcodelist += @"<li class='follower-item' style='padding-top:1.93rem'><a id='follow_" + followers.contentid + "' href='#' class='button follow-btn' onclick='fnGetLoginPartial(" + parameter + ");'><i class='icon-user-add'></i> Follow</a>";
                                    else if (followers.contentid == loginid)
                                        htmlcodelist += @"<li class='follower-item' style='padding-top:1.93rem'><a id='follow_" + followers.contentid + "' href='#' class='button follow-btn' onclick='fnGetLoginPartial(" + parameter + ");' style='display:none;'><i class='icon-user-add'></i> Follow</a>";
                                    else
                                        htmlcodelist += @"<li class='follower-item' style='padding-top:1.93rem'><a id='following_" + followers.contentid + "' href='#' class='unfollow-btn button center' onclick='fnGetLoginPartial(" + parameter1 + ");'><i class='icon-ok'></i> Following</a>";
                                    //if (userid == loginid)
                                    //{
                                    //    if (followers.isBoth == 1)
                                    //        htmlcodelist += @"<li class='follower-item' style='padding-top:1.93rem'><a id='follow_" + followers.contentid + "' href='#' class='button follow-btn' onclick='fnGetLoginPartial(" + parameter + ");' style='display:none;'><i class='icon-user-add'></i> Follow</a>";
                                    //    else
                                    //        htmlcodelist += @"<li class='follower-item' style='padding-top:1.93rem'><a id='follow_" + followers.contentid + "' href='#' class='button follow-btn' onclick='fnGetLoginPartial(" + parameter + ");'><i class='icon-user-add'></i> Follow</a>";
                                    //}
                                    //else
                                    //{
                                    //    if (followers.isCurFollow == 1)
                                    //        htmlcodelist += @"<li class='follower-item' style='padding-top:1.93rem'><a id='following_" + followers.contentid + "' href='#' class='unfollow-btn button center' onclick='fnGetLoginPartial(" + parameter1 + ");'><i class='icon-ok'></i> Following</a>";
                                    //    else if (followers.contentid == loginid || followers.isBoth == 1)
                                    //        htmlcodelist += @"<li class='follower-item' style='padding-top:1.93rem'><a id='follow_" + followers.contentid + "' href='#' class='button follow-btn' onclick='fnGetLoginPartial(" + parameter + ");' style='display:none;'><i class='icon-user-add'></i> Follow</a>";
                                    //    else
                                    //        htmlcodelist += @"<li class='follower-item' style='padding-top:1.93rem'><a id='follow_" + followers.contentid + "' href='#' class='button follow-btn' onclick='fnGetLoginPartial(" + parameter + ");' ><i class='icon-user-add'></i> Follow</a>";
                                    //}
                                    if (followers.photo != null)
                                    {
                                        if (followers.photo == "~/App_Themes/Mobile/images/review-img.png")
                                        {
                                            htmlcodelist += @"<p><a href='"+ ConfigurationManager.AppSettings["ProfilePath"].ToString() + followers.userurl.ToLower() + "'><img class='userpic' src='http://image.vconnect.bz/static/WEB/img/category/vc.png' alt='user'> " + @followers.name + "</a></p>";
                                        }
                                        else
                                        {
                                            htmlcodelist += @"<p><a href='"+ ConfigurationManager.AppSettings["ProfilePath"].ToString() + followers.userurl.ToLower() + "'><img class='userpic' src='" + followers.photo + "' alt='user'> " + @followers.name + "</a></p>";
                                        }
                                    }
                                    else
                                    {
                                        htmlcodelist += @"<p><a href='"+ ConfigurationManager.AppSettings["ProfilePath"].ToString() + followers.userurl.ToLower() + "'><img class='userpic' src='http://image.vconnect.bz/static/WEB/img/category/vc.png' alt='user'> " + @followers.name + "</a></p>";
                                    }
                                }
                            }
                        }
                    }
                    else if (Type == 7)
                    {
                        userfollowersuggestion = ((IObjectContextAdapter)db).ObjectContext.Translate<Userfollowersuggestion>(reader).ToList();
                        userInfo.userfollowersuggestion = userfollowersuggestion;
                        viewcnt = userfollowersuggestion.Count;
                        foreach (var followersug in userfollowersuggestion)
                        {
                            htmlcodelist += @"<li style='padding-top:1.93rem'><div class='img-container'>";
                            if (followersug.photo != null)
                            {
                                if (followersug.photo == "~/App_Themes/Mobile/images/review-img.png" || followersug.photo.ToString().Contains("user_icon.jpg"))
                                {
                                    htmlcodelist += @"<a href='"+ ConfigurationManager.AppSettings["ProfilePath"].ToString() + followersug.userurl.ToLower() + "'><img src='http://static.vconnect.co/img/profile-photo.jpg' alt='logo'></a>";
                                }
                                else
                                {
                                    htmlcodelist += @"<a href='"+ ConfigurationManager.AppSettings["ProfilePath"].ToString() + followersug.userurl.ToLower() + "'><img src='" + followersug.photo + "' alt='logo'></a>";
                                }
                            }
                            else
                            {
                                htmlcodelist += @"<a href='"+ ConfigurationManager.AppSettings["ProfilePath"].ToString() + followersug.userurl.ToLower() + "'><img src='http://static.vconnect.co/img/profile-photo.jpg' alt='logo'></a>";
                            }
                            parameter = "&#39;follow&#39;," + followersug.userid + "," + loginid + ",&#39;follow_" + followersug.userid + "&#39;";
                            htmlcodelist += @"</div><div class='details-container'><div class='uname'><a href='"+ ConfigurationManager.AppSettings["ProfilePath"].ToString() + followersug.userurl.ToLower() + "'>" + followersug.name + "</a></div><div class='ff'>";
                            //htmlcodelist += @"<a href='#'>John Doe</a>, <a href='#'>Someone else</a>, and 3 others.</div>";
                            htmlcodelist += @"<a href='#'>" + followersug.reviewscnt + " Reviews " + followersug.followerscnt + " Followers</div>";
                            if (loginid != 0 && followersug.userid == loginid)
                                htmlcodelist += @"<a id='follow_" + followersug.userid + "' href='#' class='button follow-button' onclick='fnGetLoginPartial(" + parameter + ");' style='display:none;'><i class='icon-user-add'></i> Follow</a></div></li>";
                            else
                                htmlcodelist += @"<a id='follow_" + followersug.userid + "' href='#' class='button follow-button' onclick='fnGetLoginPartial(" + parameter + ");'><i class='icon-user-add'></i> Follow</a></div></li>";
                        }
                    }
                    else
                    {
                        userfolloweing = ((IObjectContextAdapter)db).ObjectContext.Translate<Userfolloweing>(reader).ToList();
                        userInfo.userfolloweing = userfolloweing;
                        viewcnt = userfolloweing.Count;
                        menucnt = int.Parse(usermenucount.FirstOrDefault().followingcount.ToString());
                        foreach (var followingsug in userfolloweing)
                        {
                            int denf = 2;
                            if (denf == 1)
                            {
                                htmlcodelist += @"<li><div class='img-container'>";
                                if (followingsug.photo != null)
                                {
                                    if (followingsug.photo == "~/App_Themes/Mobile/images/review-img.png")
                                    {
                                        htmlcodelist += @"<a href='"+ ConfigurationManager.AppSettings["ProfilePath"].ToString() + followingsug.userurl.ToLower() + "'><img src='http://image.vconnect.bz/static/WEB/img/category/vc.png' alt='logo'></a>";
                                    }
                                    else
                                    {
                                        htmlcodelist += @"<a href='"+ ConfigurationManager.AppSettings["ProfilePath"].ToString() + followingsug.userurl.ToLower() + "'><img src='" + followingsug.photo + "' alt='logo'></a>";
                                    }
                                }
                                else
                                {
                                    htmlcodelist += @"<a href='"+ ConfigurationManager.AppSettings["ProfilePath"].ToString() + followingsug.userurl.ToLower() + "'><img src='http://image.vconnect.bz/static/WEB/img/category/vc.png' alt='logo'></a>";
                                }
                                if (loginid != 0 && loginid == int.Parse(userid))
                                {
                                    parameter = "&#39;follow&#39;," + followingsug.contentid + "," + loginid + ",&#39;following_" + followingsug.contentid + "&#39;";
                                }
                                else
                                {
                                    parameter = "&#39;follow&#39;," + followingsug.contentid + "," + loginid + ",&#39;follow_" + followingsug.contentid + "&#39;";
                                }
                                htmlcodelist += @"</div><div class='details-container'><div class='uname'><a href='"+ ConfigurationManager.AppSettings["ProfilePath"].ToString() + followingsug.userurl.ToLower() + "'>" + followingsug.name + "</a></div><div class='ff'>";
                                //htmlcodelist += @"<a href='#'>John Doe</a>, <a href='#'>Someone else</a>, and 3 others.</div>";
                                htmlcodelist += @"<a href='#'>" + followingsug.reviewscnt + " Reviews " + followingsug.followerscnt + " Followers</div>";
                                if (followingsug.isBoth == 1)
                                {
                                    if (loginid != 0 && loginid == int.Parse(userid))
                                    {
                                        htmlcodelist += @"<a id='following_" + followingsug.contentid + "' class='unfollow-btn button center' onclick='fnGetLoginPartial(" + parameter + ");' style='display:none;'><i class='icon-ok'></i>Following</a></div></li>";
                                    }
                                    else
                                    {
                                        htmlcodelist += @"<a id='follow_" + followingsug.contentid + "' href='#' class='button follow-btn' onclick='fnGetLoginPartial(" + parameter + ");' style='display:none;'><i class='icon-user-add'></i> Follow</a></div></li>";
                                    }
                                    //htmlcodelist += @"<a id='following_" + followingsug.contentid + "' class='unfollow-btn button center' onclick='fnGetLoginPartial(" + parameter + ");' style='display:none;'><i class='icon-ok'></i>Following</a></div></li>";
                                    //htmlcodelist += @"<a href='#' class='button follow-button' style='display:none;'><i class='icon-user-add'></i> Follow</a>";
                                }
                                else
                                {
                                    if (loginid != 0 && loginid == int.Parse(userid))
                                    {
                                        if (followingsug.contentid == loginid)
                                            htmlcodelist += @"<a id='following_" + followingsug.contentid + "' class='unfollow-btn button center' onclick='fnGetLoginPartial(" + parameter + ");' style='display:none;'><i class='icon-ok'></i>Following</a></div></li>";
                                        else
                                            htmlcodelist += @"<a id='following_" + followingsug.contentid + "' class='unfollow-btn button center' onclick='fnGetLoginPartial(" + parameter + ");'><i class='icon-ok'></i>Following</a></div></li>";
                                    }
                                    else
                                    {
                                        if (followingsug.contentid == loginid)
                                            htmlcodelist += @"<a id='follow_" + followingsug.contentid + "' href='#' class='button follow-btn' onclick='fnGetLoginPartial(" + parameter + ");' style='display:none;'><i class='icon-user-add'></i> Follow</a></div></li>";
                                        else
                                            htmlcodelist += @"<a id='follow_" + followingsug.contentid + "' href='#' class='button follow-btn' onclick='fnGetLoginPartial(" + parameter + ");'><i class='icon-user-add'></i> Follow</a></div></li>";
                                    }
                                    //htmlcodelist += @"<a id='following_" + followingsug.contentid + "' class='unfollow-btn button center' onclick='fnGetLoginPartial(" + parameter + ");'><i class='icon-ok'></i>Following</a></div></li>";
                                    //htmlcodelist += @"<a id='follow" + followingsug.contentid + "' href='#' class='button follow-button' onclick='fnGetLoginPartial(" + parameter + ");'><i class='icon-user-add'></i> Follow</a>";
                                }
                                htmlcodelist += @"</div></li>";
                            }
                            else
                            {
                                if (followingsug.contentid == int.Parse(userid) && followingsug.contentid == loginid)
                                {
                                    htmlcodelist += "";
                                }
                                else
                                {
                                    if (loginid != 0 && loginid == int.Parse(userid))
                                    {
                                        parameter = "&#39;follow&#39;," + followingsug.contentid + "," + loginid + ",&#39;following_" + followingsug.contentid + "&#39;";
                                    }
                                    else
                                    {
                                        parameter = "&#39;follow&#39;," + followingsug.contentid + "," + loginid + ",&#39;follow_" + followingsug.contentid + "&#39;";
                                    }
                                    //if (followingsug.isBoth == 1)
                                    //{
                                    ////if (loginid != 0 && loginid == userid && followingsug.contentid == loginid)
                                    ////{
                                    ////    htmlcodelist += @"<li class='following-item' style='padding-top:1.93rem'><a id='following_" + followingsug.contentid + "' href='#' class='button unfollow-btn' onclick='fnGetLoginPartial(" + parameter + ");' style='display:none;'><i class='icon-ok'></i>Following</a>";
                                    ////}
                                    ////else if (loginid != 0 && loginid == userid && followingsug.contentid != loginid)
                                    ////{
                                    ////    htmlcodelist += @"<li class='following-item' style='padding-top:1.93rem'><a id='following_" + followingsug.contentid + "' href='#' class='button unfollow-btn' onclick='fnGetLoginPartial(" + parameter + ");' ><i class='icon-ok'></i>Following</a>";
                                    ////}
                                    ////else if (loginid != 0 && loginid != userid && followingsug.contentid == loginid)
                                    ////{
                                    ////    htmlcodelist += @"<li class='follower-item' style='padding-top:1.93rem'><a id='follow_" + followingsug.contentid + "' href='#' class='button follow-btn' onclick='fnGetLoginPartial(" + parameter + ");' style='display:none;'><i class='icon-user-add'></i>Follow</a>";
                                    ////}
                                    ////else if (loginid != 0 && loginid != userid && followingsug.contentid != loginid)
                                    ////{
                                    ////    htmlcodelist += @"<li class='follower-item' style='padding-top:1.93rem'><a id='follow_" + followingsug.contentid + "' href='#' class='button follow-btn' onclick='fnGetLoginPartial(" + parameter + ");' ><i class='icon-user-add'></i>Follow</a>";
                                    ////}
                                    ////else
                                    ////{
                                    ////    //htmlcodelist += @"<li class='follower-item'><a id='follow_" + followingsug.contentid + "' href='#' class='button follow-btn' onclick='fnGetLoginPartial(" + parameter + ");'><i class='icon-user-add'></i>Follow</a>";
                                    ////}
                                    if (loginid != 0 && loginid != int.Parse(userid) && followingsug.contentid != loginid && followingsug.isBoth != 1 && followingsug.isCurFollow != 1)
                                        htmlcodelist += @"<li class='follower-item' style='padding-top:1.93rem'><a id='follow_" + followingsug.contentid + "' href='#' class='button follow-btn' onclick='fnGetLoginPartial(" + parameter + ");'><i class='icon-user-add'></i>Follow</a>";
                                    else if (followingsug.isBoth != 1 && followingsug.contentid != loginid)
                                        htmlcodelist += @"<li class='following-item' style='padding-top:1.93rem'><a id='following_" + followingsug.contentid + "' href='#' class='button unfollow-btn' onclick='fnGetLoginPartial(" + parameter + ");'><i class='icon-ok'></i>Following</a>";
                                    else if (followingsug.contentid == loginid)
                                        htmlcodelist += @"<li class='follower-item' style='padding-top:1.93rem'><a id='follow_" + followingsug.contentid + "' href='#' class='button follow-btn' onclick='fnGetLoginPartial(" + parameter + ");' style='display:none;'><i class='icon-user-add'></i>Follow</a>";
                                    else
                                        htmlcodelist += @"<li class='following-item' style='padding-top:1.93rem'><a id='following_" + followingsug.contentid + "' href='#' class='button unfollow-btn' onclick='fnGetLoginPartial(" + parameter + ");'><i class='icon-ok'></i>Following</a>";
                                    //if (userid == loginid && followingsug.contentid != loginid)
                                    //    htmlcodelist += @"<li class='following-item' style='padding-top:1.93rem'><a id='following_" + followingsug.contentid + "' href='#' class='button unfollow-btn' onclick='fnGetLoginPartial(" + parameter + ");'><i class='icon-ok'></i>Following</a>";
                                    //else if (userid != loginid)
                                    //{
                                    //    if (followingsug.contentid == loginid)
                                    //        htmlcodelist += @"<li class='follower-item' style='padding-top:1.93rem'><a id='follow_" + followingsug.contentid + "' href='#' class='button follow-btn' onclick='fnGetLoginPartial(" + parameter + ");' style='display:none;'><i class='icon-user-add'></i>Follow</a>";
                                    //    else if (followingsug.isCurFollow == 1)
                                    //        htmlcodelist += @"<li class='following-item' style='padding-top:1.93rem'><a id='following_" + followingsug.contentid + "' href='#' class='button unfollow-btn' onclick='fnGetLoginPartial(" + parameter + ");'><i class='icon-ok'></i>Following</a>";
                                    //    else
                                    //        htmlcodelist += @"<li class='follower-item' style='padding-top:1.93rem'><a id='follow_" + followingsug.contentid + "' href='#' class='button follow-btn' onclick='fnGetLoginPartial(" + parameter + ");'><i class='icon-user-add'></i>Follow</a>";
                                    //}
                                    //}
                                    //else
                                    //{
                                    //    if (loginid != 0 && loginid == userid)
                                    //    {
                                    //        if (followingsug.contentid == loginid)
                                    //            htmlcodelist += @"<li class='following-item'><a id='following_" + followingsug.contentid + "' href='#' class='button unfollow-btn' onclick='fnGetLoginPartial(" + parameter + ");' style='display:none;'><i class='icon-ok'></i>Following</a>";
                                    //        else
                                    //            htmlcodelist += @"<li class='following-item'><a id='following_" + followingsug.contentid + "' href='#' class='button unfollow-btn' onclick='fnGetLoginPartial(" + parameter + ");'><i class='icon-ok'></i>Following</a>";
                                    //    }
                                    //    else
                                    //    {
                                    //        if (followingsug.contentid == loginid)
                                    //            htmlcodelist += @"<li class='follower-item'><a id='follow_" + followingsug.contentid + "' href='#' class='button follow-btn' onclick='fnGetLoginPartial(" + parameter + ");' style='display:none;'><i class='icon-user-add'></i>Follow</a>";
                                    //        else
                                    //            htmlcodelist += @"<li class='follower-item'><a id='follow_" + followingsug.contentid + "' href='#' class='button follow-btn' onclick='fnGetLoginPartial(" + parameter + ");'><i class='icon-user-add'></i>Follow</a>";
                                    //    }
                                    //}
                                    if (followingsug.photo != null)
                                    {
                                        if (followingsug.photo == "~/App_Themes/Mobile/images/review-img.png")
                                        {
                                            htmlcodelist += @"<p><a href='"+ ConfigurationManager.AppSettings["ProfilePath"].ToString() + followingsug.userurl.ToLower() + "'><img class='userpic' src='http://image.vconnect.bz/static/WEB/img/category/vc.png' alt='user'> " + @followingsug.name + "</a></p>";
                                        }
                                        else
                                        {
                                            htmlcodelist += @"<p><a href='"+ ConfigurationManager.AppSettings["ProfilePath"].ToString() + followingsug.userurl.ToLower() + "'><img class='userpic' src='" + followingsug.photo + "' alt='user'> " + @followingsug.name + "</a></p>";
                                        }
                                    }
                                    else
                                    {
                                        htmlcodelist += @"<p><a href='"+ ConfigurationManager.AppSettings["ProfilePath"].ToString() + followingsug.userurl.ToLower() + "'><img class='userpic' src='http://image.vconnect.bz/static/WEB/img/category/vc.png' alt='user'> " + @followingsug.name + "</a></p>";
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception exp)
                {
                    log.LogMe(exp);
                }
                finally { db.Database.Connection.Close(); }
            }
            var result = new { htmlcodelist = htmlcodelist, viewcnt = viewcnt, menucnt = menucnt };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //for making Followed
        public ActionResult Follow(int? customerid, int? followedby=0, int? status=1, string source="WEB")
        {
            int result = 0;
            if (status == null)
                status = 1;
            if (followedby.Value == 0)
            {
                if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                    followedby = int.Parse(objUS.SV_VCUserContentID.ToString());
            }
            BizDetailWEB bizDetailWEB = new BizDetailWEB();
            UserInfo userInfo = new UserInfo();
            List<UserfollowDisplay> userfollowDisplay = new List<UserfollowDisplay>();
            UserDashboardWebModel UserDbWEBModel = new UserDashboardWebModel();
            if (Session["rturl"] != null && Session["rturl"] != "" && (Session["rturl"].ToString().ToLower().IndexOf("follow") != -1 || Session["rturl"].ToString().ToLower().IndexOf("returnval=6") != -1) && customerid.HasValue && followedby.HasValue)
            {
                Session["rturl"] = "";
                if (customerid.Value == followedby.Value)
                {
                    //where result =6 represent can't foolow user to itself on profile page
                    result = 6;
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                if (Session["chkbtn"] != null && Session["chkbtn"] != "" && Session["chkbtn"].ToString() == "1")
                {
                    string chkbtn = Session["chkbtn"].ToString();
                    if (!string.IsNullOrEmpty(chkbtn.ToString()) && chkbtn.ToString() == "1")
                    {
                        //where chkbtn =1 represent already foolow user on profile page
                        Session["chkbtn"] = "";
                        result = customerid.Value;
                    }
                }
                else if (status.Value == 9)
                {
                    //where status =9 represent already foolow user on biz details page
                    int result10 = UserDbWEBModel.FollowCheck(customerid.Value, followedby.Value);
                    if (result10 == 0)
                    {
                        result = UserDbWEBModel.Follow(customerid.Value, followedby.Value, 1, "Web");
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                    result = customerid.Value;
                }
                else
                {
                    result = UserDbWEBModel.Follow(customerid.Value, followedby.Value, 1, "Web");
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (followedby.HasValue)
                {
                    if (customerid.HasValue && followedby.HasValue)
                    {
                        if (status.Value != 4)
                        {
                            result = UserDbWEBModel.Follow(customerid.Value, followedby.Value, status.Value, source);
                            return Json(result, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            ReviewerDetails reviewerDetails = new ReviewerDetails();
                            reviewerDetails = bizDetailWEB.getReviewerDetails(customerid.Value, followedby.Value, 1);
                            if (result == 0 && !string.IsNullOrEmpty(reviewerDetails.followedid))
                                SendFollowReviewer(followedby.Value, reviewerDetails.contactname);
                            if (!string.IsNullOrEmpty(reviewerDetails.email) && reviewerDetails.emailsetting.Value == 1 && result == 0)
                            {
                                SendFavioritefollwer(customerid.Value);
                            }
                        }
                    }
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //for making business Reviewed
        public ActionResult SaveReview(string bizID, Int64 userID, string val)
        {
            string res = string.Empty;
            if (val.ToUpper() == "Review" || val == "5")
            {
                res = ReviewFunction(Convert.ToInt64(bizID));
            }
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public string ReviewFunction(Int64 businessId)
        {
            Int64 userId = 0;
            string message = string.Empty;
            string userName = string.Empty;
            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID))
            {
                userId = Convert.ToInt64(objUS.SV_VCUserContentID.ToString());
                userName = objUS.SV_VCUserName.ToString();
                UserDashboardWebModel listModel = new UserDashboardWebModel();
                int result = listModel.BusinessReview(userId, businessId);
                if (result == 2)
                {
                    message = "Business already in your list.";
                }
                else
                {
                    message = "Reviewed";
                }
            }
            return message;
        }
        //send mail to user when follow according to setting-notificatio

        protected void SendFavioritefollwer(int userid)
        {
            UserSession objUS = new UserSession();
            if (objUS.SV_VCUserContentID != null)
            {
                string location = string.Empty;
                string follower = Utility.userdetail(userid, "contactname");
                int currentuser = Convert.ToInt32(objUS.SV_VCUserContentID);
                string username = Utility.userdetail(currentuser, "contactname");
                string email = Utility.userdetail(userid, "email");

                string photo = Utility.userdetail(currentuser, "(CASE WHEN userdetails.photo is null or  userdetails.photo ='' THEN 'http://image.vconnect.bz/vcsites/vcimages/resource/images/user_icon.jpg' ELSE  'http://image.vconnect.bz/vcsites/vcimages/resource/' + (userdetails.photo)END) as photo ");
                
                string Message = "";
                System.IO.FileStream FsContent = new System.IO.FileStream(Server.MapPath("~/resource/email_templates/ToFollower.html"), System.IO.FileMode.Open, System.IO.FileAccess.Read);
                System.IO.StreamReader SRcontent = new System.IO.StreamReader(FsContent);
                Message = SRcontent.ReadToEnd();
                FsContent.Close();
                SRcontent.Close();
                FsContent.Dispose();
                SRcontent.Dispose();
                Message = Message.Replace("{0}", follower);
                Message = Message.Replace("{1}", username);
                Message = Message.Replace("{2}", ConfigurationManager.AppSettings["WebsiteRootPath"].ToString());
                Message = Message.Replace("{3}", objUS.SV_VCUserContentID);
                Message = Message.Replace("{4}", photo);
                Message = Message.Replace("{5}", ConfigurationManager.AppSettings["WebsiteRootPath"].ToString());
                Message = Message.Replace("{6}", objUS.SV_VCUserUrl);

                //===================================
                var recipients = new List<Mandrill.Messages.Recipient>();
                recipients.Add(new Mandrill.Messages.Recipient(email, follower));
                var mandrill = new Mandrill.Messages.Message()
                {
                    To = recipients.ToArray(),
                    FromEmail = "info@VConnect.com",
                    Subject = "Congratulations! You have a new Follower on VConnect",

                    Html = Message
                };
                //=================================== 
                SendEmail send = new SendEmail();
                try
                {
                    send.VCUsermails(mandrill);
                }
                catch (Exception exp)
                {
                    log.LogMe(exp);
                }

            }
        }
        protected void SendFollowReviewer(int userid, string follower)
        {
            UserSession objUS = new UserSession();
            if (objUS.SV_VCUserContentID != null)
            {
                string location = string.Empty;
                //string follower = Utility.userdetail(userid, "contactname");
                int currentuser = Convert.ToInt32(objUS.SV_VCUserContentID);
                string username = Utility.userdetail(currentuser, "contactname");
                string email = Utility.userdetail(currentuser, "email");
                //string bizName = BusinessName.ToString().Trim();
                ReviewerDetails reviewerDetails = new ReviewerDetails();
                var suggestion = reviewerDetails.DBlistfollowersug(userid);

                // following ====
                string fMessage = "";
                //System.Net.Mail.SmtpClient fSMPT = new System.Net.Mail.SmtpClient();
                //System.Net.Mail.MailMessage fEmail = new System.Net.Mail.MailMessage();
                System.IO.FileStream fFsContent = new System.IO.FileStream(Server.MapPath("~/resource/email_templates/ToFollowing.html"), System.IO.FileMode.Open, System.IO.FileAccess.Read);
                System.IO.StreamReader fSRcontent = new System.IO.StreamReader(fFsContent);
                fMessage = fSRcontent.ReadToEnd();
                fFsContent.Close();
                fSRcontent.Close();
                fFsContent.Dispose();
                fSRcontent.Dispose();
                fMessage = fMessage.Replace("{0}", username);
                fMessage = fMessage.Replace("{1}", follower);
                fMessage = fMessage.Replace("{21}", objUS.SV_VCUserUrl);
                 var i = 0; 
                foreach (var item in suggestion)
                {
                    var userDesc = 11;
                    var followings = 14;
                    var followers = 17;
                    var profile = 5;
                    var photo = 8;
                    var t = 2;
                    t = t + i;
                    profile = profile + i;
                    photo = photo + i;
                    userDesc = userDesc + i;
                    followings = followings + i;
                    followers = followers + i;

                    var url = ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + "user/"+ item.userurl+"&ref=gmail";
                    fMessage = fMessage.Replace("{" + t + "}", item.name);
                    fMessage = fMessage.Replace("{" + profile + "}", url);
                    fMessage = fMessage.Replace("{" + photo + "}", item.photo);
                  //  fMessage = fMessage.Replace("{" + userDesc + "}", item.userdesc);
                    fMessage = fMessage.Replace("{" + followings + "}", item.followingscnt.ToString());
                    fMessage = fMessage.Replace("{" + followers + "}", item.followerscnt.ToString());

                    i++;
                }
                //===================================
                var recipients = new List<Mandrill.Messages.Recipient>();
                recipients.Add(new Mandrill.Messages.Recipient(email, username));
                var mandrill = new Mandrill.Messages.Message()
                {
                    To = recipients.ToArray(),
                    FromEmail = "info@VConnect.com",
                    Subject = "You are now following " + follower + " on VConnect",

                    Html = fMessage
                };
                //=================================== 
                SendEmail fsend = new SendEmail();
                try
                {
                    fsend.VCUsermails(mandrill);
                }
                catch (Exception exp)
                {
                    log.LogMe(exp);
                }  
            }
        }
   
        #region Social activity and notification
        public ActionResult notifications()
        {
            int loginid = 0;
            if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                loginid = int.Parse(objUS.SV_VCUserContentID.ToString());
            if (loginid != 0)
            {
                UserDashboardWebModel social = new UserDashboardWebModel();
                UserInfo userInfo = new UserInfo();
                userInfo = social.notificationsReturn(loginid, 1);
                return View("notifications", userInfo);
            }
            else
                return Redirect("/");
        }
        public ActionResult socialactivity(string loc)
        {
            int loginid = 0;
            if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                loginid = int.Parse(objUS.SV_VCUserContentID.ToString());
            if (loginid != 0)
            {
                List<Vconnect.Models.BoDashboardModel.ClaimedBusinessList> claimedbusinesslist = new List<Vconnect.Models.BoDashboardModel.ClaimedBusinessList>();
                Vconnect.Controllers.BoDashboardWebController BOdashboard = new Vconnect.Controllers.BoDashboardWebController();

                if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && !string.IsNullOrEmpty(objUS.SV_VCLoginID))
                {
                    claimedbusinesslist = BOdashboard.getbusiness(loginid.ToString());
                    if (claimedbusinesslist != null && claimedbusinesslist.Count > 0)
                    {
                        Session["isclaim"] = true;
                    }
                }

                UserDashboardWebModel social = new UserDashboardWebModel();
                UserInfo userInfo = new UserInfo();
                loc = string.IsNullOrEmpty(loc) ? "Lagos" : loc;
                try
                {
                    userInfo.lstFeaturedBusinesses = userInfo.FeaturedBannerResultFunc(loc);
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
                try
                {
                    if (userInfo.lstFeaturedBusinesses.FirstOrDefault().Error == 1)
                    {
                        return Redirect("/");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(loc))
                        {
                            if (loc.Contains('-'))
                            {
                                locState = loc.Split('-')[0].Replace('_', ' ');
                                locSearchLocation = loc.Split('-')[1].Replace('_', ' ');
                                userInfo.SearchLocation = loc.Split('-')[1].Replace('_', ' ');
                            }
                            else
                            {
                                locState = loc.Replace('_', ' ');
                                locSearchLocation = loc.Replace('_', ' ');
                                userInfo.SearchLocation = loc.Replace('_', ' ');
                            }
                            userInfo._SelectedLocation = loc.Replace(' ', '_');
                            Utility.CreateCokkies(userInfo.SelectedStateId.ToString(), userInfo._SelectedLocation, userInfo.SearchText, userInfo.SearchLocation);
                        }
                        userInfo.SearchTextHeader = userInfo.SearchText;
                        userInfo.SearchLocationHeader = userInfo.SearchLocation;
                        int userid = 0;
                        if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && !string.IsNullOrEmpty(objUS.SV_VCLoginID))
                        {
                            userid = Convert.ToInt32(objUS.SV_VCUserContentID);
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
                // return View(model);
                userInfo.userid = loginid;
                string userName = "";
                if (!string.IsNullOrEmpty(objUS.SV_VCUserName))
                    userName = objUS.SV_VCUserName.ToString();
                userInfo = social.socialactivityReturn(loginid);
                userInfo.loginname = userName;
                return View("socialactivity", userInfo);
            }
            else
                return Redirect("/");
        }
        #endregion
        #endregion

        #region Profile Header
        #region editprofile
        [HttpGet]
        public ActionResult EditProfile()
        {
            if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
            {
                if (TempData["UserAlert"] != null)
                    Session["UserAlert"] = TempData["UserAlert"];
                else
                    Session["UserAlert"] = null;
                //profilemodel editfrofile = new profilemodel();
                //Int32 userid = 0;
                //if (!string.IsNullOrEmpty(Request.QueryString["userid"]))
                //{
                //    userid = Convert.ToInt32(Request.QueryString["userid"]);
                //}
                //else
                //{
                //    if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                //    {
                //        userid = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                //        editfrofile.loginid = objUS.SV_VCUserContentID.ToString();
                //    }
                //}
                profilemodel editfrofile = new profilemodel();
                Int32 userid = 0;
                int urlID = 0;
                if (!string.IsNullOrEmpty(Request.QueryString["userid"]))
                {
                    if (int.TryParse(Request.QueryString["userid"], out urlID))
                    {
                        userid = Convert.ToInt32(Request.QueryString["userid"]);
                    }
                    else
                    {
                        editfrofile.userurl = Request.QueryString["userid"];
                        UserDashboardWebModel UserDbWEBModel = new UserDashboardWebModel();
                        userid = Convert.ToInt32((UserDbWEBModel.getUserContentid(Request.QueryString["userid"])).ToString());
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                    {
                        userid = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                        editfrofile.loginid = objUS.SV_VCUserContentID.ToString();
                    }
                }
                if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                    editfrofile.loginid = objUS.SV_VCUserContentID.ToString();
                editfrofile.SuccessMessage = null;
                editfrofile.ErrorMessage = null;
                editfrofile.genderoption = Genderlist();
                if (userid != 0 || !string.IsNullOrEmpty(userid.ToString()))
                {
                    editfrofile.UserId = userid;
                    editfrofile.getWebUserDashboardDetails();
                    editfrofile.password = !string.IsNullOrEmpty(editfrofile.ResultUserDBTab.UserDBDetails.First().pwd) ? editfrofile.ResultUserDBTab.UserDBDetails.First().pwd : "";
                    editfrofile.txtfname = !string.IsNullOrEmpty(editfrofile.ResultUserDBTab.UserDBDetails.First().contactname) ? editfrofile.ResultUserDBTab.UserDBDetails.First().contactname : "";
                    editfrofile.txtsname = !string.IsNullOrEmpty(editfrofile.ResultUserDBTab.UserDBDetails.First().lastname) ? editfrofile.ResultUserDBTab.UserDBDetails.First().lastname : "";
                    //editfrofile.txtdob = !string.IsNullOrEmpty(editfrofile.ResultUserDBTab.UserDBDetails.First().dateofbirth) ? editfrofile.ResultUserDBTab.UserDBDetails.First().dateofbirth : DateTime.Now.ToShortDateString().ToString();
                    editfrofile.txtdob = !string.IsNullOrEmpty(editfrofile.ResultUserDBTab.UserDBDetails.First().dateofbirth) ? editfrofile.ResultUserDBTab.UserDBDetails.First().dateofbirth : "";
                    // string a = (string)(editfrofile.ResultUserDBTab.UserDBDetails.First().dateofbirth);
                    if (editfrofile.ResultUserDBTab.UserDBDetails.First().stateid.HasValue)
                    {
                        editfrofile.SelectedStateId = editfrofile.ResultUserDBTab.UserDBDetails.First().stateid.Value;
                    }
                    else
                    {
                        editfrofile.SelectedStateId = 0;
                    }
                    if (editfrofile.ResultUserDBTab.UserDBDetails.First().cityid.HasValue)
                    {
                        editfrofile.SelectedCityId = editfrofile.ResultUserDBTab.UserDBDetails.First().cityid.Value;
                    }
                    else
                    {
                        editfrofile.SelectedCityId = 0;
                    }
                    editfrofile.StateOptions = GetSelectList();
                    editfrofile.CityOptions = Utility.GetSelectListCity(editfrofile.SelectedStateId);
                    editfrofile.txtphone = !string.IsNullOrEmpty(editfrofile.ResultUserDBTab.UserDBDetails.First().phone) ? editfrofile.ResultUserDBTab.UserDBDetails.First().phone : "";
                    editfrofile.txtdescription = !string.IsNullOrEmpty(editfrofile.ResultUserDBTab.UserDBDetails.First().description) ? editfrofile.ResultUserDBTab.UserDBDetails.First().description : "";
                    SelectList selecttProduct = new SelectList(editfrofile.genderoption, "Id", "Name");
                    if (!string.IsNullOrEmpty(editfrofile.ResultUserDBTab.UserDBDetails.First().sex))
                    {
                        foreach (SelectListItem item in selecttProduct.Items)
                        {
                            if (item.Text == editfrofile.ResultUserDBTab.UserDBDetails.First().sex.ToString())
                            {
                                editfrofile.SelectgenderId = int.Parse(item.Value);
                                break;
                            }
                        }
                    }
                    if (editfrofile.listNotifications == null || editfrofile.listNotifications.Count == 0)
                        editfrofile.listNotifications = editfrofile.DBListNotificationsSettings(userid);
                    ViewBag.userdb = editfrofile;
                    return View(editfrofile);
                }
                else { return Redirect("/"); }
            }
            else { return RedirectToAction("Login", "Account", new { rturl = Request.Url.AbsoluteUri.ToString() }); }
        }
        [HttpPost]
        public ActionResult EditProfile(profilemodel editfrofile)
        {
            string gendername = null;
            editfrofile.UserId = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
            editfrofile.loginid = objUS.SV_VCUserContentID.ToString();
            editfrofile.getWebUserDashboardDetails();

            if (editfrofile.ResultUserDBTab.UserDBDetails.First().stateid.HasValue)
            {
                if (editfrofile.SelectedStateId == editfrofile.ResultUserDBTab.UserDBDetails.First().stateid.Value)
                    editfrofile.SelectedStateId = editfrofile.ResultUserDBTab.UserDBDetails.First().stateid.Value;
                else
                    editfrofile.SelectedStateId = editfrofile.SelectedStateId;
            }
            else
            {
                editfrofile.SelectedStateId = 0;
            }
            if (editfrofile.ResultUserDBTab.UserDBDetails.First().cityid.HasValue)
            {
                if (editfrofile.SelectedCityId == editfrofile.ResultUserDBTab.UserDBDetails.First().cityid.Value)
                    editfrofile.SelectedCityId = editfrofile.ResultUserDBTab.UserDBDetails.First().cityid.Value;
                else
                    editfrofile.SelectedCityId = editfrofile.SelectedCityId;
            }
            else
            {
                editfrofile.SelectedCityId = 0;
            }
            editfrofile.StateOptions = GetSelectList();
            editfrofile.CityOptions = Utility.GetSelectListCity(editfrofile.SelectedStateId);
            editfrofile.genderoption = Genderlist();
            SelectList selecttProduct = new SelectList(editfrofile.genderoption, "Id", "Name");
            foreach (SelectListItem item in selecttProduct.Items)
            {
                if (item.Value == editfrofile.SelectgenderId.ToString())
                {
                    gendername = item.Text;
                    break;
                }
            }
            int result = 1, result1 = 0;
            int useridupdate;
            
            if (objUS.IsSessionAlive())
            {
                useridupdate = Convert.ToInt32(objUS.SV_VCUserContentID);
            }
            else
            {
                useridupdate = 0;
            }
            if (!string.IsNullOrEmpty(editfrofile.txtphone))
            {
                result = Vconnect.Common.Utility.isValidPhone(editfrofile.txtphone);
                result1 = usersphonechk(editfrofile.txtphone, editfrofile.UserId);
                if (result1 == 0)
                {
                    editfrofile.ismobileverified = true;
                }
                if (editfrofile.txtphone == editfrofile.ResultUserDBTab.UserDBDetails.FirstOrDefault().phone)
                {
                    editfrofile.ismobileverified = editfrofile.ResultUserDBTab.UserDBDetails.FirstOrDefault().ismobileverified;
                }

            }
            if (string.IsNullOrEmpty(editfrofile.txtfname))
                editfrofile.ErrorMessage = "Enter your first name";
            //else if (string.IsNullOrEmpty(editfrofile.txtsname))
            //    editfrofile.ErrorMessage = "Enter your surname";
            else if (!string.IsNullOrEmpty(editfrofile.txtdob) && ((DateTime.Now.Year - Convert.ToDateTime(editfrofile.txtdob).Year) < 13))
            {
                editfrofile.ErrorMessage = "Enter valid birthdate";
            }
            //else if (string.IsNullOrEmpty(editfrofile.txtphone))
            //    editfrofile.ErrorMessage = "Enter phone number";
            //else if (result == 0)
            //{
            //    editfrofile.ErrorMessage = "Invalid contact number";
            //}
            //else if (result1 == 1)
            //{
            //    editfrofile.ErrorMessage = "Phone no. already exits";
            //}
            else
            {
                if (result == 0 || result1 == 1)
                {
                    // editfrofile.txtphoneold = editfrofile.txtphone;
                    editfrofile.txtphone = "";
                }
                int result3 = editfrofile.prc_cupdate_userdetails(editfrofile, gendername);
                //TempData["SuccessMessage"] = "Registration successful.";
                editfrofile.SuccessMessage = "Profile updated successfully.";
                //return RedirectToAction("EditProfile", "UserDashboard");
            }
            int idnew = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
            if (editfrofile.listNotifications == null || editfrofile.listNotifications.Count == 0)
                editfrofile.listNotifications = editfrofile.DBListNotificationsSettings(idnew);
            
            Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserName"], editfrofile.txtfname, "VCUserName");
            return View(editfrofile);
            //return View(editfrofile);
        }
        public static SelectList GetSelectList()
        {
            string selectedid = "0";
            var list = new[] 
                { 
                new StatesDropDown  { StateId = 101, StateName = "Abia"  },
                new StatesDropDown  { StateId = 102 ,StateName = "Adamawa"},
                new StatesDropDown  { StateId = 103 ,StateName = "Akwa Ibom" },
                new StatesDropDown  { StateId = 104 , StateName = "Anambra"},
                new StatesDropDown  { StateId = 105,StateName = "Abuja" },
                new StatesDropDown  { StateId = 106,StateName = "Bayelsa" },
                new StatesDropDown  { StateId = 107,StateName = "Benue" },
                new StatesDropDown  { StateId = 108,StateName = "Borno" },
                new StatesDropDown  { StateId = 109,StateName = "Bauchi" },
                new StatesDropDown  { StateId = 110,StateName = "Cross River", },
                new StatesDropDown  { StateId = 111,StateName = "Delta" },
                new StatesDropDown  { StateId = 112, StateName = "Ebonyi",  },
                new StatesDropDown  { StateId = 113 ,StateName = "Edo",},
                new StatesDropDown  { StateId = 114 ,StateName = "Ekiti"},
                new StatesDropDown  { StateId = 115,StateName = "Enugu"},
                new StatesDropDown  { StateId = 116,StateName = "Gombe"  },
                new StatesDropDown  { StateId = 117,StateName = "Imo" },
                new StatesDropDown  { StateId = 118 ,StateName = "Jigawa"},
                new StatesDropDown  { StateId = 119,StateName = "Kaduna" },
                new StatesDropDown  { StateId = 120,StateName = "Kano", },
                new StatesDropDown  { StateId = 121 ,StateName = "Katsina"},
                new StatesDropDown  { StateId = 122 ,StateName = "Kebbi"},
                new StatesDropDown  { StateId = 123 , StateName = "Kogi" },
                new StatesDropDown  { StateId = 124, StateName = "Kwara" },
                new StatesDropDown  { StateId = 125 ,StateName = "Lagos"},
                new StatesDropDown  { StateId = 126 ,StateName = "Nasarawa"},
                new StatesDropDown  { StateId = 127, StateName = "Niger" },
                new StatesDropDown  { StateId = 128,StateName = "Ogun" },
                new StatesDropDown  { StateId = 129,StateName = "Ondo" },
                new StatesDropDown  { StateId = 130 ,StateName = "Osun" },
                new StatesDropDown  { StateId = 131,StateName = "Oyo" },
                new StatesDropDown  { StateId = 132,StateName = "Plateau", },
                new StatesDropDown  { StateId = 133,StateName = "Rivers" },
                new StatesDropDown  { StateId = 134 ,StateName = "Sokoto"},
                new StatesDropDown  { StateId = 135 ,StateName = "Taraba"},
                new StatesDropDown  { StateId = 136, StateName = "Yobe" },
                new StatesDropDown  { StateId = 137,StateName = "Zamfara" }
                };


            if ((System.Web.HttpContext.Current.Response.Cookies.AllKeys.Contains("vcSelectedLocId") || System.Web.HttpContext.Current.Request.Cookies.AllKeys.Contains("vcSelectedLocId")) && System.Web.HttpContext.Current.Request.Cookies["vcSelectedLocId"].Value != "")
            {
                selectedid = System.Web.HttpContext.Current.Request.Cookies["vcSelectedLocId"].Value.ToString();
            }

            return new SelectList(list, "StateId", "StateName", Convert.ToInt32(selectedid));
        }
        public JsonResult sendMobileVerificationCode(string mobilenumber)
        {
            string loginemail = Request.Cookies[ConfigurationManager.AppSettings["VCLoginID"]].Value.ToString();
            string userid = objUS.SV_VCUserContentID.ToString();
            string randCode = WEBLoginDatabaseCall.Generate(4);
            WEBLoginDatabaseCall.UpdateNewVerificationCode(loginemail, randCode);
            SendSMS objSendSMS = new SendSMS();
            string SMSResponse = objSendSMS.Sendmessage("Dear Customer, Your OTP for verifying mobile number is : " + randCode + ".", mobilenumber);
            int usertype = !string.IsNullOrEmpty(objUS.SV_VCUserType) ? Convert.ToInt16(objUS.SV_VCUserType) : 0;
            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && Convert.ToInt32(objUS.SV_VCUserContentID) != 0)
            {
                Utility.SaveSMSEmailLog(Convert.ToInt32(objUS.SV_VCUserContentID), usertype, "", mobilenumber, "", Vconnect.Enums.MessageType.SMS_VerifyCode.GetHashCode(), "Mobile verification Code", "Dear Customer, Your OTP for verifying mobile number is : " + randCode + ".", Convert.ToInt32(userid), "", "", SMSResponse, Utility.GetIpAddressNew(), 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), Vconnect.Enums.ActionType.SMS.GetHashCode(), Utility.GetReversedns(Utility.GetIpAddressNew()), "WEB");
            }
            else
            {
                Utility.SaveSMSEmailLog(0, usertype, "", mobilenumber, "", Vconnect.Enums.MessageType.SMS_VerifyCode.GetHashCode(), "Mobile verification Code", "Dear Customer, Your OTP for verifying mobile number is : " + randCode + ".", Convert.ToInt32(userid), "", "", SMSResponse, Utility.GetIpAddressNew(), 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), Vconnect.Enums.ActionType.SMS.GetHashCode(), Utility.GetReversedns(Utility.GetIpAddressNew()), "WEB");
            }
            return Json(randCode, JsonRequestBehavior.AllowGet);
        }
        public JsonResult verifyCode(string mobilenumber, string verificationcode)
        {
            int userid = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
            profilemodel profilemodel = new profilemodel();
            int result = profilemodel.verifyVerificationCode(userid, verificationcode, mobilenumber);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult updateProfile(string FirstName, string LastName, string birthdate, string gender, string state, string city, string mobilenumber, string mobileverified, string biodata)
        {
            profilemodel editfrofile = new profilemodel();
            string gendername = null;
            editfrofile.txtfname = FirstName;
            editfrofile.txtsname = LastName;
            editfrofile.txtdob = birthdate;

            editfrofile.UserId = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
            editfrofile.loginid = objUS.SV_VCUserContentID.ToString();
            editfrofile.SelectedStateId = string.IsNullOrEmpty(state) ? 0 : Convert.ToInt32(state);
            editfrofile.SelectedCityId = string.IsNullOrEmpty(city) ? 0 : Convert.ToInt32(city);
            editfrofile.txtphone = mobilenumber;
            editfrofile.ismobileverified = string.IsNullOrEmpty(mobileverified) ? false : mobileverified == "1" ? true : false;
            editfrofile.txtdescription = biodata;
            gendername = gender == "1" ? "Male" : gender == "2" ? "Female" : "";
            int result = 1, result1 = 0;
            int useridupdate;
            
            if (objUS.IsSessionAlive())
            {
                useridupdate = Convert.ToInt32(objUS.SV_VCUserContentID);
            }
            else
            {
                useridupdate = 0;
            }
            //if (!string.IsNullOrEmpty(editfrofile.txtphone))
            //{
            //    result = Vconnect.Common.Utility.isValidPhone(mobilenumber);
            //    result1 = usersphonechk(mobilenumber);
            //}
            if (string.IsNullOrEmpty(FirstName))
                ViewBag.Msg = "Enter your first name";
            else if (!string.IsNullOrEmpty(birthdate) && ((DateTime.Now.Year - Convert.ToDateTime(birthdate).Year) < 13))
            {
                ViewBag.Msg = "Enter valid birthdate";
            }
            else
            {
                //if (result == 0 || result1 == 1)
                //{                    
                //    editfrofile.txtphone = "";
                //}
                int result3 = editfrofile.prc_cupdate_userdetails(editfrofile, gendername);
                ViewBag.Msg = "Profile updated successfully.";
            }
            int idnew = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
            if (editfrofile.listNotifications == null || editfrofile.listNotifications.Count == 0)
                editfrofile.listNotifications = editfrofile.DBListNotificationsSettings(idnew);
            
            Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserName"], FirstName, "VCUserName");
            return Json(ViewBag.Msg, JsonRequestBehavior.AllowGet);
        }
        #region phoneNumberCheck
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult phonenumbercheckalt(string phonenumber)
        {
            Int32 loginid = 0;
            if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
            {
                loginid = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
            }
            if ((!Request.AcceptTypes.Contains("text/html")))
            {
                int d = 4;
                if (!string.IsNullOrEmpty(phonenumber))
                {
                    int result = Vconnect.Common.Utility.isValidPhone(phonenumber);
                    int result1 = 0;
                    result1 = usersphonechk(phonenumber, loginid);
                    string sContactNumber = phonenumber;
                    string subSection = sContactNumber.Substring(0, 1);

                    if (subSection != "0")
                    {
                        d = 5;
                    }
                    else if (result == 0)
                    {
                        d = 2;
                    }
                    else if (result1 == 1)
                    {
                        d = 1;
                    }
                    else
                    {
                        d = 0;
                    }
                }
                else
                {
                    d = 4;
                }
                return Json(d, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public static int usersphonechk(string phone, Int32? userid)
        {
            int result = 0;

            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_web_get_usersphonechk]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@phone", phone));
                if (userid.HasValue)
                {
                    cmd.Parameters.Add(new SqlParameter("@userid", userid.Value));
                }
                cmd.Parameters.Add(new SqlParameter("@errorcode", SqlDbType.VarChar, 10));
                cmd.Parameters["@errorcode"].Direction = ParameterDirection.Output;
                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                connection.Open();
                cmd.ExecuteNonQuery();
                db.Database.Connection.Close();
                result = Convert.ToInt32(cmd.Parameters["@errorcode"].Value.ToString());
            }
            return result;
        }
        #endregion
        public static SelectList Genderlist()
        {
            var list = new[] 
                { 
                 new StatesDropDown  { StateId = 1 ,StateName = "Male"},
                   new StatesDropDown  { StateId = 2 ,StateName = "Female"}               
                };
            return new SelectList(list, "StateId", "StateName");
        }
        #endregion
        #region Account
        public ActionResult Account(profilemodel userdashboard)
        {
            return View();
        }
        #endregion
        #region partial
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetWebUserDashboard(profilemodel userdashboard, int num = 1)
        {

            if ((!Request.AcceptTypes.Contains("text/html")))
            {
                if (num == 123)
                {
                    if (Session["UserAlert"] != null)
                    {
                        TempData["UserAlert"] = Session["UserAlert"];
                        Session["UserAlert"] = null;
                    }
                    if (string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                    {
                        return Json(new { Url = "Account/Login", rturl = "UserDashboardWEB/EditProfile" });
                        //return RedirectToAction("Login", "Account", new { rturl = "UserDashboardWEB/EditProfile" });
                    }

                    if (userdashboard == null)
                        userdashboard = new profilemodel();

                    if (TempData["UDM"] != null)
                        userdashboard = (profilemodel)TempData["UDM"];

                    int res = 0;
                    if (!Int32.TryParse(objUS.SV_VCUserContentID.ToString().Trim(), out res))
                    {
                        TempData["UserAlert"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Incorrect UserId.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                        userdashboard.IsErr = 1;
                        return Json(userdashboard);
                        //return View(userdashboard);
                    }
                    else
                    {
                        UserId = Int32.Parse(objUS.SV_VCUserContentID.ToString().Trim());
                    }

                    try
                    {
                        if (userdashboard.IsFBShare == null || userdashboard.IsTWShare == null || userdashboard.IsFBShare == "" || userdashboard.IsTWShare == "")
                        {
                            userdashboard.listUserSharePreferrenceModel = profilemodel.DBGetUserSharePreferences(UserId);
                            if (userdashboard.listUserSharePreferrenceModel != null)
                            {
                                //return checked from the procedure for the checkbox to show checked
                                userdashboard.IsFBShare = userdashboard.listUserSharePreferrenceModel[0].IsFBShare;
                                userdashboard.IsTWShare = userdashboard.listUserSharePreferrenceModel[0].IsTwShare;
                            }
                            else
                            {
                                userdashboard.IsFBShare = "";
                                userdashboard.IsTWShare = "";
                            }

                            if (userdashboard.IsFBShare == "checked")
                            {
                                if (Request.Cookies["VCisfbShare"] != null)
                                {
                                    HttpCookie VCisfbShare = new HttpCookie("VCisfbShare");
                                    if (!Request.Url.AbsoluteUri.Contains("localhost") && !Request.Url.AbsoluteUri.Contains("http://stage.vconnect.com/"))
                                    {
                                        VCisfbShare.Domain = "vconnect.com";
                                        VCisfbShare.Path = "/";
                                    }
                                    VCisfbShare.Expires = DateTime.Now.AddDays(3);
                                    Response.Cookies.Add(VCisfbShare);

                                    VCisfbShare = new HttpCookie("VCisfbShare", "1");
                                    if (!Request.Url.AbsoluteUri.Contains("localhost") && !Request.Url.AbsoluteUri.Contains("http://stage.vconnect.com/"))
                                    {
                                        VCisfbShare.Domain = "vconnect.com";
                                        VCisfbShare.Path = "/";
                                    }
                                    VCisfbShare.Expires = DateTime.Now.AddDays(3);
                                    Response.Cookies.Add(VCisfbShare);
                                }
                                else
                                {
                                    HttpCookie VCisfbShare = new HttpCookie("VCisfbShare", "1");
                                    if (!Request.Url.AbsoluteUri.Contains("localhost") && !Request.Url.AbsoluteUri.Contains("http://stage.vconnect.com/"))
                                    {
                                        VCisfbShare.Domain = "vconnect.com";
                                        VCisfbShare.Path = "/";
                                    }
                                    VCisfbShare.Expires = DateTime.Now.AddDays(3);
                                    Response.Cookies.Add(VCisfbShare);
                                }
                            }

                            if (userdashboard.IsTWShare == "checked")
                            {
                                if (Request.Cookies["VCistwshare"] != null)
                                {
                                    HttpCookie VCistwshare = new HttpCookie("VCistwshare");
                                    if (!Request.Url.AbsoluteUri.Contains("localhost") && !Request.Url.AbsoluteUri.Contains("http://stage.vconnect.com/"))
                                    {
                                        VCistwshare.Domain = "vconnect.com";
                                        VCistwshare.Path = "/";
                                    }
                                    VCistwshare.Expires = DateTime.Now.AddDays(-1);
                                    Response.Cookies.Add(VCistwshare);

                                    VCistwshare = new HttpCookie("VCistwshare", "1");
                                    if (!Request.Url.AbsoluteUri.Contains("localhost") && !Request.Url.AbsoluteUri.Contains("http://stage.vconnect.com/"))
                                    {
                                        VCistwshare.Domain = "vconnect.com";
                                        VCistwshare.Path = "/";
                                    }
                                    VCistwshare.Expires = DateTime.Now.AddDays(3);
                                    Response.Cookies.Add(VCistwshare);
                                }
                                else
                                {
                                    HttpCookie VCistwshare = new HttpCookie("VCistwshare", "1");
                                    if (!Request.Url.AbsoluteUri.Contains("localhost") && !Request.Url.AbsoluteUri.Contains("http://stage.vconnect.com/"))
                                    {
                                        VCistwshare.Domain = "vconnect.com";
                                        VCistwshare.Path = "/";
                                    }
                                    VCistwshare.Expires = DateTime.Now.AddDays(3);
                                    Response.Cookies.Add(VCistwshare);
                                }

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        log.LogMe(ex);
                    }
                }
                //return View(userdashboard);
                /////////////////////////
                Int32 userid = 0;

                if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                {
                    userid = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                    userdashboard.UserId = userid;
                }
                JsonModel jsonModel = new JsonModel();
                string viewName = "";
                switch (num)
                {
                    case 1:
                        viewName = "_settingprofile";
                        break;

                    case 2:
                        viewName = "_accountpwd";
                        userdashboard.getWebUserDashboardDetails();
                        userdashboard.password = userdashboard.ResultUserDBTab.UserDBDetails.First().pwd;
                        break;
                    case 3:
                        if (userdashboard.listNotifications == null || userdashboard.listNotifications.Count == 0)
                            userdashboard.listNotifications = userdashboard.DBListNotificationsSettings(userid);
                        viewName = "_notification";
                        break;
                }
                ViewBag.userdb = userdashboard;
                jsonModel.HTMLString = RenderPartialViewToString(viewName, userdashboard);
                //return Json(jsonModel);
                return Json(jsonModel,
                    JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        protected string RenderPartialViewToString(string viewName, object model)
        {
            // controller.ViewData.Model = model;
            try
            {
                if (string.IsNullOrWhiteSpace(viewName))
                    viewName = ControllerContext.RouteData.GetRequiredString("action");
                ViewData.Model = model;
                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                    ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                    viewResult.View.Render(viewContext, sw);
                    return sw.GetStringBuilder().ToString();
                }
            }
            catch (Exception e)
            {
                ViewData["IsError"] = "y";
                return "";
            }
        }

        #endregion
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult AccountUserDashboard(string pwd, string pwdcurt, string pwdnewcurt)
        {
            profilemodel userdashboard = new profilemodel();
            if ((!Request.AcceptTypes.Contains("text/html")))
            {
                if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                {
                    int userid = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                    //userdashboard.getWebUserDashboardDetails();
                    //userdashboard.password = !string.IsNullOrEmpty(userdashboard.ResultUserDBTab.UserDBDetails.First().pwd) ? userdashboard.ResultUserDBTab.UserDBDetails.First().pwd : "";
                    //userdashboard.txtemailid = !string.IsNullOrEmpty(userdashboard.ResultUserDBTab.UserDBDetails.First().email) ? userdashboard.ResultUserDBTab.UserDBDetails.First().email : "";
                    if (objUS.SV_VCLoginType.ToString().IndexOf("FB") == -1 && objUS.SV_VCLoginType.ToString().IndexOf("twitter") == -1 && objUS.SV_VCLoginType.ToString().IndexOf("Google") == -1)
                    { }
                    else { pwd = pwdcurt; }
                    if (string.IsNullOrEmpty(pwd))
                        ViewBag.Msg = "Enter a valid current password";
                    else if (string.IsNullOrEmpty(pwdcurt) || pwdcurt.Length < 6)
                        ViewBag.Msg = "Enter a valid new password (at least 6 characters)";
                    else if (string.IsNullOrEmpty(pwdnewcurt) || pwdnewcurt.Length < 6)
                        ViewBag.Msg = "Enter a valid new confirm password (at least 6 characters)";
                    else if (pwdcurt != pwdnewcurt)
                        ViewBag.Msg = "Password should be same";
                    else
                    {
                        int result = userdashboard.prc_update_userPassword(pwd, userid, pwdcurt);
                        if (result == 0)
                        {
                            ViewBag.Msg = "Password Updated Successfully..Thankyou";
                        }
                        else if (result == 2)
                        {
                            ViewBag.Msg = "Old Password does not match! try again.";
                        }
                        else
                        {
                            ViewBag.Msg = "An error occurred. Please try again.";
                        }
                    }
                    return Json(ViewBag.Msg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                    //return Json(RedirectToAction("Login", "Account", new { rturl = "UserDashboard/EditProfile" }));
                }
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        #region cropping

        //public ActionResult SaveUploadedFile()
        //{
        //    string userName = "";
        //    int newwidth = 0;
        //    int newheight = 0;
        //    Int64 userid = 0;
        //    string imagepath = string.Empty;
        //    //bool isSavedSuccessfully = true;
        //    bool FileSaved = false;
        //    if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
        //    {
        //        userName = objUS.SV_VCUserName.ToString();
        //        userid = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
        //    }

        //    string contacname = userName.ToString();
        //    string randomno = DateTime.Now.Ticks.ToString();
        //    try
        //    {
        //        foreach (string fileName in Request.Files)
        //        {

        //            HttpPostedFileBase file = Request.Files[fileName];
        //            string filename1 = file.FileName.ToString();
        //            string ext = string.Empty;
        //            if (filename1.LastIndexOf('.') > 0)
        //            {
        //                ext = filename1.Substring(filename1.LastIndexOf('.')).ToLower();
        //            }
        //            if (ext == ".jpg" || ext == ".jpeg" || ext == ".gif" || ext == ".bmp" || ext == ".png" || ext == ".tif" || ext == ".tiff")
        //            {

        //                string filename = contacname.Trim().Replace(" ", "-").Replace("&", "and").Replace("?", "-").Replace("%", "-").Replace(".", "-").Replace("'", "-").Replace("#", "-") + "_" + randomno.ToString();
        //                filename = filename + "_1" + ext;
        //                var fn1 = Server.MapPath("~/Resource/uploads/userphotos/original/" + filename);
        //                (file).SaveAs(fn1);
        //                FileSaved = true;
        //                //Save file content goes here
        //                if (FileSaved)
        //                {
        //                    System.Drawing.Image uploadImage = System.Drawing.Image.FromFile(Server.MapPath("~/Resource/uploads/userphotos/original/" + filename));
        //                    float imageWidth = uploadImage.PhysicalDimension.Width;
        //                    float imageHeight = uploadImage.PhysicalDimension.Height;
        //                    if (imageWidth > imageHeight)
        //                    {
        //                        float ratio = imageWidth / imageHeight;
        //                        newwidth = Convert.ToInt16(imageWidth / ratio);
        //                        newheight = Convert.ToInt16(imageHeight / ratio);
        //                        if (newwidth > 250)
        //                        {
        //                            newwidth = 250;
        //                            ratio = imageWidth / newwidth;
        //                            newheight = Convert.ToInt16(imageHeight / ratio);
        //                        }
        //                        else if (newheight < 100)
        //                        {
        //                            newheight = 100;
        //                            ratio = imageHeight / newheight;
        //                            newwidth = Convert.ToInt16(imageWidth / ratio);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        float ratio = imageHeight / imageWidth;
        //                        newwidth = Convert.ToInt16(imageWidth / ratio);
        //                        newheight = Convert.ToInt16(imageHeight / ratio);
        //                        if (newheight > 250)
        //                        {
        //                            newheight = 250;
        //                            ratio = imageHeight / newheight;
        //                            newwidth = Convert.ToInt16(imageWidth / ratio);
        //                        }
        //                        else if (newwidth < 100)
        //                        {
        //                            newwidth = 100;
        //                            ratio = imageWidth / newwidth;
        //                            newheight = Convert.ToInt16(imageHeight / ratio);
        //                        }
        //                    }
        //                    ResizeImage(newwidth, newheight, Server.MapPath("~/Resource/uploads/userphotos/original/" + filename), Server.MapPath("~/Resource/uploads/userphotos/resize/" + filename));
        //                    try
        //                    {
        //                        // CopyImage.Sync(webconfigSourceImagesRootPath + "userphotos/original/", webconfigDestinationImagesRootPath + "userphotos/original/");
        //                        // CopyImage.Sync(webconfigSourceImagesRootPath + "userphotos/resize/", webconfigDestinationImagesRootPath + "userphotos/resize/");
        //                        CopyImage.Sync(@"C:\Users\Jitu\Desktop\Vconnect_30thMay2014\Vconnect\Resource\uploads\userphotos\original", @"C:\Users\Jitu\Desktop\Vconnect_30thMay2014\Vconnect\Resource\uploads\userphotos\original");
        //                        CopyImage.Sync(@"C:\Users\Jitu\Desktop\Vconnect_30thMay2014\Vconnect\Resource\uploads\userphotos\resize", @"C:\Users\Jitu\Desktop\Vconnect_30thMay2014\Vconnect\Resource\uploads\userphotos\resize");

        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        log.LogMe(ex);
        //                    }
        //                    imagepath = filename;
        //                }
        //            }
        //            else
        //            {
        //                return Json("Photo format is not valid.", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.LogMe(ex);
        //    }
        //    //return Json(imagepath, JsonRequestBehavior.AllowGet);
        //    return Json("~/Resource/uploads/userphotos/original/" + imagepath, JsonRequestBehavior.AllowGet);
        //}


        public ActionResult SaveUploadedFile()
        {
            string userName = "";
            int newwidth = 0;
            int newheight = 0;
            Int64 userid = 0;
            string imagepath = string.Empty;
            //bool isSavedSuccessfully = true;
            bool FileSaved = false;
            //if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
            //{
            //    userName = objUS.SV_VCUserName.ToString();
            //    userid = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
            //}
             if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
             {
                 userName = objUS.SV_VCUserName.ToString();
                 userid = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
             }

            string contacname = userName.ToString();
            string randomno = DateTime.Now.Ticks.ToString();
            try
            {
                foreach (string fileName in Request.Files)
                {

                    HttpPostedFileBase file = Request.Files[fileName];
                    string filename1 = file.FileName.ToString();
                    string ext = string.Empty;
                    if (filename1.LastIndexOf('.') > 0)
                    {
                        ext = filename1.Substring(filename1.LastIndexOf('.')).ToLower();
                    }
                    if (ext == ".jpg" || ext == ".jpeg" || ext == ".gif" || ext == ".bmp" || ext == ".png" || ext == ".tif" || ext == ".tiff")
                    {

                        //string filename = contacname.Trim().Replace(" ", "-").Replace("&", "and").Replace("?", "-").Replace("%", "-").Replace(".", "-").Replace("'", "-").Replace("#", "-") + "_" + randomno.ToString();
                        string filename = userid.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
                        filename = filename + "_1" + ext;
                      //  var fn1 = Server.MapPath("~/Resource/uploads/userphotos/original/" + filename);
                        ImageConverter converter = new ImageConverter();
                        System.Drawing.Image uploadImage = System.Drawing.Image.FromStream(file.InputStream);
                        var temp = (byte[])converter.ConvertTo(uploadImage, typeof(byte[]));
                        Utility.ByteCloudUploadImages(temp, filename, "vcsites/vcimages/resource/uploads/userphotos/original/");
                         
                        //(file).SaveAs(fn1);
                        FileSaved = true;
                        //Save file content goes here
                        if (FileSaved)
                        {
                            #region code commented for makeing croping disabled

                           // System.Drawing.Image uploadImage = System.Drawing.Image.FromFile(Server.MapPath("~/Resource/uploads/userphotos/original/" + filename));
                            float imageWidth = uploadImage.PhysicalDimension.Width;
                            float imageHeight = uploadImage.PhysicalDimension.Height;
                            if (imageWidth > imageHeight)
                            {
                                float ratio = imageWidth / imageHeight;
                                newwidth = Convert.ToInt16(imageWidth / ratio);
                                newheight = Convert.ToInt16(imageHeight / ratio);
                                if (newwidth > 250)
                                {
                                    newwidth = 250;
                                    ratio = imageWidth / newwidth;
                                    newheight = Convert.ToInt16(imageHeight / ratio);
                                }
                                else if (newheight < 100)
                                {
                                    newheight = 100;
                                    ratio = imageHeight / newheight;
                                    newwidth = Convert.ToInt16(imageWidth / ratio);
                                }
                            }
                            else
                            {
                                float ratio = imageHeight / imageWidth;
                                newwidth = Convert.ToInt16(imageWidth / ratio);
                                newheight = Convert.ToInt16(imageHeight / ratio);
                                if (newheight > 250)
                                {
                                    newheight = 250;
                                    ratio = imageHeight / newheight;
                                    newwidth = Convert.ToInt16(imageWidth / ratio);
                                }
                                else if (newwidth < 100)
                                {
                                    newwidth = 100;
                                    ratio = imageWidth / newwidth;
                                    newheight = Convert.ToInt16(imageHeight / ratio);
                                }
                            }
                            ResizeImage(newwidth, newheight, file.InputStream, filename, "vcsites/vcimages/resource/uploads/userphotos/resize/");
                            #endregion
                            UserDashboardWebModel userdashboard = new UserDashboardWebModel();
                            // string profilepic = "uploads/userphotos/original/photomobile/"+filename;
                            string profilepic = "uploads/userphotos/resize/" + filename;
                            int result = 0;
                            result = userdashboard.prc_cupdate_userphoto_opt(userid, profilepic, userid);
                         //   string imagelocation = "vcsites/vcimages/resource/uploads/userphotos/resize/";
                            string imgname = webconfigSourceImagesRootPath + "userphotos/resize/" + filename;
                            try
                            {
                               // ImageConverter converter = new ImageConverter();
                              //  byte[] imagebyte = System.IO.File.ReadAllBytes(imgname);
                               // Utility.cloudimageupload(imagebyte, filename, imagelocation);
                                centralcropping(file.InputStream, filename);
                               // System.IO.File.Delete(webconfigSourceImagesRootPath + "userphotos/original/" + filename);
                                //System.IO.File.Delete(webconfigSourceImagesRootPath + "userphotos/resize/" + filename);

                                //System.IO.File.Copy(webconfigSourceImagesRootPath + "userphotos/original/" + filename, webconfigDestinationImagesRootPath + "userphotos/original/photomobile/" + filename, true);
                                //System.IO.File.Copy(webconfigSourceImagesRootPath + "userphotos/resize/" + filename, webconfigDestinationImagesRootPath + "userphotos/original/thumb/" + filename, true);
                            string zaq="http://image.vconnect.bz/vcsites/vcimages/resource/uploads/userphotos/resize/"+filename;
                                Utility.CreateHttpCookie(ConfigurationManager.AppSettings["VCUserPhoto"], ConfigurationManager.AppSettings["ImagesRootPath"].ToString() + "uploads/userphotos/resize/" + filename, "VCUserPhoto");
                                return Json((zaq).Trim(), JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception ex)
                            {
                                log.LogMe(ex);
                            }
                            imagepath = filename;
                            //return Json((ConfigurationManager.AppSettings["ImagesRootPath"].ToString() + imgname).Trim(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("Photo format is not valid.", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }
            
            return Json(ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + "Resource/uploads/userphotos/original/" + imagepath, JsonRequestBehavior.AllowGet);
            //return Json(null, JsonRequestBehavior.AllowGet);
        }

        protected void centralcropping(Stream stream, string FileName)
        {
            string original = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Resource\\uploads\\userphotos\\original\\";
            string resize = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Resource\\uploads\\userphotos\\resize\\";
            string ImageName = FileName;
            System.Drawing.Image uploadImage = System.Drawing.Image.FromStream(stream);
            float imageWidth = uploadImage.PhysicalDimension.Width;
            float imageHeight = uploadImage.PhysicalDimension.Height;
            int x = 0;
            int y = 0;
            int w = 100; // thumb widht

            int h = 100; // thumb height

            if (imageWidth <= 150 || imageHeight <= 150)
            {
                x = 0;

                y = 0;
            }
            else
            {
                x = Convert.ToInt32(imageWidth / 4);

                y = Convert.ToInt32(imageHeight / 4);
            }



            byte[] CropImage = Crop(resize + ImageName, w, h, x, y, stream);

            using (MemoryStream ms = new MemoryStream(CropImage, 0, CropImage.Length))
            {

                ms.Write(CropImage, 0, CropImage.Length);

                using (SD.Image CroppedImage = SD.Image.FromStream(ms, true))
                {

                    ImageConverter converter = new ImageConverter();
                    string UploadFolder = "vcsites/vcimages/resource/uploads/userphotos/original/thumb/";
                    var t = (byte[])converter.ConvertTo(CroppedImage, typeof(byte[]));
                    Utility.ByteCloudUploadImages(t, FileName, UploadFolder);

                    //  string SaveTo = original + "thumb\\" + ImageName;
                    // CroppedImage.Save(SaveTo, CroppedImage.RawFormat);
                    //  var t = 0;

                    ResizeImage(60, 60, stream, FileName, "vcsites/vcimages/resource/uploads/userphotos/original/photomobile/");
                }

            }

        }
        static byte[] Crop(string Img, int Width, int Height, int X, int Y, Stream stream)
        {

            try
            {

                using (SD.Image OriginalImage = SD.Image.FromStream(stream))
                {

                    using (SD.Bitmap bmp = new SD.Bitmap(Width, Height))
                    {

                        bmp.SetResolution(OriginalImage.HorizontalResolution, OriginalImage.VerticalResolution);

                        using (SD.Graphics Graphic = SD.Graphics.FromImage(bmp))
                        {

                            Graphic.SmoothingMode = SmoothingMode.HighQuality;

                            Graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;

                            Graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;

                            Graphic.DrawImage(OriginalImage, new SD.Rectangle(0, 0, Width, Height), X, Y, Width, Height, SD.GraphicsUnit.Pixel);

                            MemoryStream ms = new MemoryStream();

                            bmp.Save(ms, OriginalImage.RawFormat);

                            return ms.GetBuffer();

                        }

                    }

                }

            }

            catch (Exception Ex)
            {

                throw (Ex);

            }

        }
        public JsonResult SaveDisplayCropImage(string imgurl, int? x, int? y, int? width, int? height, Stream stream)
        {
            UserDashboardWebModel userdashboard = new UserDashboardWebModel();
            string userName = string.Empty;
            Int64 userid = 0;
            //string imgName = imgurl.Substring(imgurl.LastIndexOf("\\"));
            string fileName = Path.GetFileName(imgurl);
            string filePath = Path.Combine(Server.MapPath("~/resource/uploads/userphotos/resize/"), fileName);
            string cropFileName = "";
            string cropFilePath = "";
            string cropThumbFilePath = "";
            string thumb = string.Empty;
            string error = string.Empty;
            int result = 0;
            if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
            {
                userName = objUS.SV_VCUserName.ToString();
                userid = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
            }
            try
            {
                if (System.IO.File.Exists(filePath))
                {
                    System.Drawing.Image orgImg = System.Drawing.Image.FromFile(filePath);
                    Rectangle CropArea = new Rectangle(
                        Convert.ToInt32(x.Value),
                        Convert.ToInt32(y.Value),
                        Convert.ToInt32(width.Value),
                        Convert.ToInt32(height.Value));

                    Bitmap bitMap = new Bitmap(CropArea.Width, CropArea.Height);
                    using (Graphics g = Graphics.FromImage(bitMap))
                    {
                        g.DrawImage(orgImg, new Rectangle(0, 0, bitMap.Width, bitMap.Height), CropArea, GraphicsUnit.Pixel);
                    }
                   // cropFileName = "crop_" + fileName;
                    //cropFilePath = Path.Combine(Server.MapPath("~/Resource/uploads/userphotos/crop/"), cropFileName);

                    //bitMap.Save(cropFilePath);//---------
                    ResizeImage(100, 100, stream, cropFileName,"vcsites/vcimages/resource/uploads/userphotos/original/thumb/");

                    cropThumbFilePath = Path.Combine(Server.MapPath("~/Resource/uploads/userphotos/thumb/"), cropFileName);
                    thumb = "uploads/userphotos/thumb/" + cropFileName;
                    result = userdashboard.prc_cupdate_userphoto_opt(userid, thumb, userid);
                    try
                    {
                        //CopyImage.Sync(webconfigSourceImagesRootPath + "userphotos\\crop", webconfigDestinationImagesRootPath + "userphotos\\crop");
                        //CopyImage.Sync(webconfigSourceImagesRootPath + "userphotos\\thumb", webconfigDestinationImagesRootPath + "userphotos\\thumb");
                        string strVCUserPhoto = MyGlobalVariables.ImagesRootPath + "uploads/userphotos/thumb/" + cropFileName;

                        HttpCookie userphoto = new HttpCookie(ConfigurationManager.AppSettings["VCUserPhoto"]);
                        if (!Request.Url.AbsoluteUri.Contains("localhost"))
                        {
                            userphoto.Domain = "vconnect.com";
                            userphoto.Path = "/";
                        }
                        if (userphoto.Name == "VCUserPhoto")
                        {
                            userphoto.Value = "";
                            userphoto.Expires = DateTime.Now.AddDays(-1d);
                            HttpContext.Response.Cookies.Add(userphoto);
                        }
                        HttpCookie VCUserPhoto = new HttpCookie(ConfigurationManager.AppSettings["VCUserPhoto"], strVCUserPhoto);
                        if (!Request.Url.AbsoluteUri.Contains("localhost"))
                        {
                            VCUserPhoto.Domain = "vconnect.com";
                            VCUserPhoto.Path = "/";
                        }
                        VCUserPhoto.Expires = DateTime.Now.AddDays(3d);
                        HttpContext.Response.Cookies.Add(VCUserPhoto);
                    }
                    catch (Exception ex)
                    {
                        log.LogMe(ex);
                    }
                }
                return Json(cropFileName, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                error = "File could not be uploaded.";
                log.LogMe(ex);
                return Json(error, JsonRequestBehavior.AllowGet);
            }
        }
        #region Image Optization
        public static void ResizeImage(int MaxWidth, int MaxHeight, System.IO.Stream stream, string FileName, string NewFileName)
        {
            Bitmap OriginalBmp = (System.Drawing.Bitmap)System.Drawing.Image.FromStream(stream).Clone();
            Size ResizedDimensions = GetDimensions(MaxWidth, MaxHeight, ref OriginalBmp);
            ImageConverter converter = new ImageConverter();

            //=-====================================
            // System.Drawing.Imaging.ImageFormat imageFormat = OriginalBmp.RawFormat;
            // NewBmp.Save(NewFileName, imageFormat);
            Bitmap NewBmp = new Bitmap(OriginalBmp, ResizedDimensions);
            var t = (byte[])converter.ConvertTo(NewBmp, typeof(byte[]));
            Utility.ByteCloudUploadImages(t, FileName, NewFileName);

            
            //System.Drawing.Imaging.ImageFormat imageFormat = OriginalBmp.RawFormat;
            //NewBmp.Save(NewFileName, imageFormat);
            //OriginalBmp.Dispose();
            //OriginalBmp = null;
            //NewBmp.Dispose();
            //NewBmp = null;
        }

        public static Size GetDimensions(int MaxWidth, int MaxHeight, ref Bitmap Bmp)
        {
            int Width;
            int Height;
            float Multiplier;

            Height = Bmp.Height;
            Width = Bmp.Width;

            // this means you want to shrink an image that is already shrunken!
            if (Height <= MaxHeight && Width <= MaxWidth)
                return new Size(Width, Height);

            // check to see if we can shrink it width first
            Multiplier = (float)((float)MaxWidth / (float)Width);
            if ((Height * Multiplier) <= MaxHeight)
            {
                Height = (int)(Height * Multiplier);
                return new Size(MaxWidth, Height);
            }

            // if we can't get our max width, then use the max height
            Multiplier = (float)MaxHeight / (float)Height;
            Width = (int)(Width * Multiplier);

            return new Size(Width, MaxHeight);
        }
        #endregion

        #endregion
        #region Notification
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult NotificationUserDashboard(string notIdList)
        {
            int flag = 1;
            profilemodel userdashboard = new profilemodel();
            if ((!Request.AcceptTypes.Contains("text/html")))
            {
                if (!string.IsNullOrEmpty(objUS.SV_VCUserContentID))
                {
                    string[] words = notIdList.Split(',');
                    int userid = Convert.ToInt32(objUS.SV_VCUserContentID.ToString());
                    List<UserNotificationsModel> listUserNotificationsModel = new List<UserNotificationsModel>();
                    foreach (string word in words)
                    {
                        if (!string.IsNullOrEmpty(word))
                        {
                            Int32 ntid = Int32.Parse(word);
                            UserNotificationsModel UNM = new UserNotificationsModel
                            {
                                ContentId = userid,
                                NotificationId = ntid,
                                Source = "Web"
                            };
                            listUserNotificationsModel.Add(UNM);
                        }
                        flag = 1;
                    }
                    if (listUserNotificationsModel == null || listUserNotificationsModel.Count == 0)
                    {
                        userdashboard.listNotifications = userdashboard.DBListNotificationsSettings(userid);
                        var distinctContentID = userdashboard.listNotifications
                            .Select(p => p.ContentId)
                            .Distinct();
                        foreach (int q in distinctContentID)
                        {
                            Int32 ntid = q;
                            UserNotificationsModel UNM = new UserNotificationsModel
                            {
                                ContentId = userid,
                                NotificationId = ntid,
                                Source = "Web"
                            };
                            listUserNotificationsModel.Add(UNM);
                        }
                        flag = 3;
                    }
                    int Err = userdashboard.DBSetUserNotifications(listUserNotificationsModel, flag);
                    if (userdashboard.listNotifications == null || userdashboard.listNotifications.Count == 0)
                        userdashboard.listNotifications = userdashboard.DBListNotificationsSettings(userid);
                    return Json(Err, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region sendemail
        protected void SendFavioriteBizCred(string Name, int LoginID, string EmailID, string Password, string verificationcode, int qflag, int usertype)
        {
            string location = string.Empty;
            string vcusername = Name;
            string bizName = Name.ToString().Trim();
            string Message = "";
            if (!string.IsNullOrEmpty(EmailID))
            {
                System.IO.FileStream FsContent;
                FsContent = new System.IO.FileStream(Server.MapPath("~/Resource/email_templates/userregistration.html"), System.IO.FileMode.Open, System.IO.FileAccess.Read);
                System.IO.StreamReader SRcontent = new System.IO.StreamReader(FsContent);
                Message = SRcontent.ReadToEnd();
                FsContent.Close();
                SRcontent.Close();
                FsContent.Dispose();
                SRcontent.Dispose();
                Message = Message.Replace("{10}", Utility.ToTitleCase(Name));
                Message = Message.Replace("{2}", Password);
                Message = Message.Replace("{20}", ConfigurationManager.AppSettings["WebsiteRootPath"]).ToString();
                Message = Message.Replace("{11}", (ConfigurationManager.AppSettings["WebsiteRootPath"]).ToString() + "UserReg/ThankYou?userid=" + LoginID.ToString().Trim() + "&verificationcodenew=" + verificationcode);
                // Message = Message.Replace("{12}", LoginID.ToString().Trim());
                Message = Message.Replace("{13}", EmailID);
                Message = Message.Replace("{14}", qflag.ToString().Trim());
                Message = Message.Replace("{15}", usertype.ToString().Trim());
                //Email.Subject = ConfigurationManager.AppSettings["BizAddFavorite"].ToString();           
                //Email.Subject = bizName.ToString() + " is one of " + vcusername.ToString() + "'s Favorite";
                var recipients = new List<Mandrill.Messages.Recipient>();
                recipients.Add(new Mandrill.Messages.Recipient(EmailID, ""));
                var mandrill = new Mandrill.Messages.Message()
                {
                    To = recipients.ToArray(),
                    FromEmail = "info@VConnect.com",
                    Subject = "You are Successfully Registered on VConnect",

                    Html = Message
                };

                SendEmail send = new SendEmail();
                try
                {
                    send.VCUsermails(mandrill);
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
            }
        }
        #endregion
        #endregion

        #region Facebook

        private Uri RedirectUri
        {
            get
            {
                Session["rturl"] = null;
                if (Request.QueryString["rturl"] != null)
                {
                    Session["rturl"] = Request.QueryString["rturl"].Replace("$", "&");
                    if (Request.QueryString["provider"] != null)
                    {
                        rturl = Request.QueryString["rturl"].ToString().Trim().Replace("$", "&");
                    }
                }

                var uriBuilder = new UriBuilder(Request.Url);
                try
                {
                    var query = System.Web.HttpUtility.ParseQueryString(uriBuilder.Query);
                    query["rturl"] = rturl;
                    uriBuilder.Query = query.ToString();
                    uriBuilder.Fragment = null;

                    uriBuilder.Path = Url.Action("FacebookCallback");

                    return uriBuilder.Uri;
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                    return uriBuilder.Uri;
                }
            }
        }

        [ValidateInput(false)]
        public ActionResult Facebook()
        {
            try
            {
                if (Request.QueryString["rturl"] != null && Request.QueryString["rturl"].ToString().Trim() != "")
                {
                    Session["rturl"] = Request.QueryString["rturl"].ToString().Trim().Replace("$", "&");
                }

                var fb = new FacebookClient();
                var loginUrl = fb.GetLoginUrl(new
                {
                    client_id = ConfigurationManager.AppSettings["facebookAppID"].ToString(),
                    client_secret = ConfigurationManager.AppSettings["facebookAppSecret"].ToString(),
                    redirect_uri = RedirectUri.AbsoluteUri,
                    response_type = "code",
                    scope = "email,publish_actions" // Add other permissions as needed
                });
                if (TempData["UserAlert"] == null || TempData["UserAlert"] == "")
                    TempData["UserAlert"] = "<div></div>";
                return Redirect(loginUrl.AbsoluteUri);

            }
            catch (Exception ex)
            {
                return Redirect("/UserDashboardWeb/EditProfile");
                //log.LogMe(ex);               
            }
        }

        [ValidateInput(false)]
        public ActionResult FacebookCallback(string code)
        {
            if (TempData["UserAlert"] == null || TempData["UserAlert"] == "")
                TempData["UserAlert"] = "<div></div>";
            try
            {

                string rturl = "";
                string FBID = string.Empty;

                profilemodel UDM = new profilemodel();

                int res = 0;
                if (!Int32.TryParse(objUS.SV_VCUserContentID.ToString().Trim(), out res))
                {
                    TempData["UserAlert"] = "Incorrect UserId";
                    UDM.IsErr = 1;
                    return View(UDM);
                }
                else
                {
                    UserId = Int32.Parse(objUS.SV_VCUserContentID.ToString().Trim());
                }

                var fb = new FacebookClient();


                dynamic result = fb.Post("oauth/access_token", new
                {
                    client_id = ConfigurationManager.AppSettings["facebookAppID"].ToString(),
                    client_secret = ConfigurationManager.AppSettings["facebookAppSecret"].ToString(),
                    redirect_uri = RedirectUri.AbsoluteUri,
                    code = code
                });


                var accessToken = result.access_token;



                // // Store the access token in the session
                Session["AccessToken"] = accessToken;

                // // update the facebook client with the access token so 
                // // we can make requests on behalf of the user
                fb.AccessToken = accessToken;

                try
                {
                    // Get the user's information
                    dynamic me = fb.Get("me?fields=name,first_name,last_name,id,email,birthday,gender,link,picture");

                    List<FbUserFriendsList> fbusers = new List<FbUserFriendsList>();
                    try
                    {
                        dynamic frns = fb.Get("me/friends?fields=name,first_name,last_name,id,gender,link"); //string email = me.email;

                        JObject friendListJson = JObject.Parse(frns.ToString());

                        foreach (var friend in friendListJson["data"].Children())
                        {
                            FbUserFriendsList fbUser = new FbUserFriendsList();

                            fbUser.Id = (friend["id"] != null && friend["id"].ToString().Trim() != "" ? friend["id"].ToString().Trim().Replace("\"", "") : "");
                            fbUser.Name = (friend["name"] != null && friend["name"].ToString().Trim() != "" ? friend["name"].ToString().Trim().Replace("\"", "") : "");
                            fbUser.FirstName = (friend["first_name"] != null && friend["first_name"].ToString().Trim() != "" ? friend["first_name"].ToString().Trim().Replace("\"", "") : "");
                            fbUser.LastName = (friend["last_name"] != null && friend["last_name"].ToString().Trim() != "" ? friend["last_name"].ToString().Trim().Replace("\"", "") : "");
                            fbUser.Gender = (friend["gender"] != null && friend["gender"].ToString().Trim() != "" ? friend["gender"].ToString().Trim().Replace("\"", "") : "");
                            fbUser.Link = (friend["link"] != null && friend["link"].ToString().Trim() != "" ? friend["link"].ToString().Trim().Replace("\"", "") : "");
                            fbUser.Source = "Web";
                            fbusers.Add(fbUser);
                        }

                    }
                    catch (Exception ex) { }


                    FBID = me.id;

                    //    //===========================================
                    //    //var args = new Dictionary<string, object>();
                    //    //args["message"] = "Testing3 Testing Testing Testing Testing Testing333333333333333333333333333333333333333333333333333333333 Testing Testing Testing Testing Testing Testing Testing Testing Testing ";
                    //    //args["caption"] = "Testing Testing Testing Testing Testing Testing333333333333333333333333 Testing Testing Testing Testing Testing ";
                    //    //args["description"] = "Testing 333333333333333333333333333333333333333Testing Testing Testing Testing Testing Testing Testing ";
                    //    //args["name"] = "Testing333333333333333333333333333333333333333333333333333333333 ";
                    //    ////args["picture"] = "picture of the 400x210 image";
                    //    //args["link"] = "http://www.rediffmail.com";
                    //    //fb.Post("me/feed/", args);
                    //    //===========================================
                    //    //Session["VCfboauth_token"] = fb.AccessToken;

                    if (Request.Cookies["VCfboauth_token"] != null)
                    {

                        //if exists first delete 
                        HttpCookie VCfboauth_token = new HttpCookie("VCfboauth_token", fb.AccessToken);
                        if (!Request.Url.AbsoluteUri.Contains("localhost"))
                        {
                            VCfboauth_token.Domain = "vconnect.com";
                            VCfboauth_token.Path = "/";
                        }

                        VCfboauth_token.Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies.Add(VCfboauth_token);

                        //then create again
                        VCfboauth_token = new HttpCookie("VCfboauth_token", fb.AccessToken);
                        if (!Request.Url.AbsoluteUri.Contains("localhost"))
                        {
                            VCfboauth_token.Domain = "vconnect.com";
                            VCfboauth_token.Path = "/";
                        }

                        VCfboauth_token.Expires = DateTime.Now.AddDays(3);
                        Response.Cookies.Add(VCfboauth_token);
                    }
                    else
                    {
                        HttpCookie VCfboauth_token = new HttpCookie("VCfboauth_token", fb.AccessToken);
                        if (!Request.Url.AbsoluteUri.Contains("localhost"))
                        {
                            VCfboauth_token.Domain = "vconnect.com";
                            VCfboauth_token.Path = "/";
                        }

                        VCfboauth_token.Expires = DateTime.Now.AddDays(3);
                        Response.Cookies.Add(VCfboauth_token);
                    }
                    int resErr = profilemodel.DBConnectUser(UserId, FBID, "fb", fb.AccessToken, "");

                    if (resErr == 3)
                    {
                        TempData["UserAlert"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">This Facebook id is already associated with other account.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                    }
                    else if (resErr == 0)
                    {
                        //=====================facebook connected cookie=============//
                        if (Request.Cookies["VCisfbconnect"] != null)
                        {
                            HttpCookie VCisfbconnect = new HttpCookie("VCisfbconnect");
                            if (!Request.Url.AbsoluteUri.Contains("localhost") && !Request.Url.AbsoluteUri.Contains("http://stage.vconnect.com/"))
                            {
                                VCisfbconnect.Domain = "vconnect.com";
                                VCisfbconnect.Path = "/";
                            }
                            VCisfbconnect.Expires = DateTime.Now.AddDays(-1);
                            Response.Cookies.Add(VCisfbconnect);

                            VCisfbconnect = new HttpCookie("VCisfbconnect", "1");
                            if (!Request.Url.AbsoluteUri.Contains("localhost") && !Request.Url.AbsoluteUri.Contains("http://stage.vconnect.com/"))
                            {
                                VCisfbconnect.Domain = "vconnect.com";
                                VCisfbconnect.Path = "/";
                            }
                            VCisfbconnect.Expires = DateTime.Now.AddDays(3);
                            Response.Cookies.Add(VCisfbconnect);

                        }
                        else
                        {
                            HttpCookie VCisfbconnect = new HttpCookie("VCisfbconnect", "1");
                            if (!Request.Url.AbsoluteUri.Contains("localhost") && !Request.Url.AbsoluteUri.Contains("http://stage.vconnect.com/"))
                            {
                                VCisfbconnect.Domain = "vconnect.com";
                                VCisfbconnect.Path = "/";
                            }
                            VCisfbconnect.Expires = DateTime.Now.AddDays(3);
                            Response.Cookies.Add(VCisfbconnect);

                        }
                        //=====================facebook connected cookie=============//

                        //facebookfriends list 
                        if (fbusers != null && fbusers.Count > 0)
                        {
                            WEBLoginDatabaseCall.DBSaveFBUserFriendList(fbusers, UserId);
                            TempData["fbusers"] = null;
                        }

                        if (Session["rturl"] != null && !string.IsNullOrEmpty(Session["rturl"].ToString().Trim()))
                        {
                            rturl = "/" + Session["rturl"].ToString().Trim();
                            return Redirect(rturl);
                        }
                        else
                        {
                            return Redirect("/");
                        }
                    }
                    else if (resErr == 1)
                    {
                        TempData["UserAlert"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Some error occured Please try again later.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                    }
                    //===========================================//

                }
                catch (Exception ex)
                {
                    //Response.Write(ex.ToString());
                    //TempData["UserAlert"] = ex.ToString();
                    return Redirect("/UserDashboardWeb/EditProfile");
                }

                if (Session["rturl"] != null && !string.IsNullOrEmpty(Session["rturl"].ToString().Trim()))
                {
                    rturl = "/" + Session["rturl"].ToString();
                    return Redirect(rturl);
                }
                else
                {
                    return Redirect("/");
                }

            }
            catch (Exception ex)
            {
                //TempData["UserAlert"] = ex.ToString();
                return Redirect("/UserDashboardWeb/EditProfile");
            }
        }

        #endregion

        #region Twitter

        [AllowAnonymous]
        [ValidateInput(false)]
        public ActionResult ExternalLoginCallback(string provider, string rturl)
        {
            if (TempData["UserAlert"] == null || TempData["UserAlert"] == "")
                TempData["UserAlert"] = "<div></div>";
            try
            {
                if (Request.QueryString["rturl"] != null)
                {
                    Session["rturl"] = Request.QueryString["rturl"].ToString().Trim().Replace("$", "&");
                    if (Request.QueryString["provider"] != null)
                    {
                        rturl = Request.QueryString["rturl"].ToString().Trim().Replace("$", "&");
                    }
                }
                //VerifyAuthenticationCore
                AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback"));

                if (!result.IsSuccessful)
                {
                    if (Session["rturl"] != null && !string.IsNullOrEmpty(Session["rturl"].ToString().Trim()))
                        return Redirect("/login?rturl=" + Session["rturl"].ToString().Trim().Replace("&", "$"));
                    else
                        return Redirect("/login");
                }

                var userDataFromProvider = result.ExtraData;
                Dictionary<string, string> userData = new Dictionary<string, string>();
                if (result.Provider.ToLower() == "twitter")
                {
                    Session["TWuserid"] = result.ProviderUserId.ToString();
                    Session["TWscreenname"] = result.UserName.ToString();

                    oauth_token = userDataFromProvider["accesstoken"].ToString().Trim();
                    oauth_token_secret = userDataFromProvider["accesssecret"].ToString().Trim();

                    //================twitter followers/followings=================//

                    resource_url = "https://api.twitter.com/1.1/followers/list.json";

                    authHeader = GenerateAuthHeader(resource_url);
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(resource_url);
                    request.Headers.Add("Authorization", authHeader);
                    request.Method = "GET";
                    request.ContentType = "application/x-www-form-urlencoded";
                    //using (Stream stream = request.GetRequestStream())
                    //{
                    //    byte[] content = ASCIIEncoding.ASCII.GetBytes(postBody);
                    //    stream.Write(content, 0, content.Length);
                    //}
                    WebResponse response = request.GetResponse();
                    string resTwt = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    JObject j = JObject.Parse(resTwt);
                    JArray data = (JArray)j["users"];
                    int totalFollowers = (data).Count;
                    //TempData["lblTotalFollowers"] = "Nishant has " + (data).Count + " Followers";

                    List<TwiterFollowers> listFollowers = new List<TwiterFollowers>();

                    if (data != null)
                    {

                        foreach (var item in data)
                        {
                            TwiterFollowers objTwiterFollowers = new TwiterFollowers();
                            objTwiterFollowers.ScreenName = item["screen_name"].ToString().Replace("\"", "");
                            objTwiterFollowers.ProfileImage = item["profile_image_url"].ToString().Replace("\"", "");
                            objTwiterFollowers.TwitterId = item["id"].ToString().Replace("\"", "");
                            objTwiterFollowers.Source = "Web";
                            listFollowers.Add(objTwiterFollowers);
                        }
                    }

                    //==========================twitter friends=========================//
                    resource_url = "https://api.twitter.com/1.1/friends/list.json";
                    authHeader = GenerateAuthHeader(resource_url);

                    request = (HttpWebRequest)WebRequest.Create(resource_url);
                    request.Headers.Add("Authorization", authHeader);
                    request.Method = "GET";
                    request.ContentType = "application/x-www-form-urlencoded";

                    response = request.GetResponse();
                    resTwt = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    j = JObject.Parse(resTwt);
                    data = (JArray)j["users"];
                    int totalFriends = (data).Count;

                    if (data != null)
                    {
                        foreach (var item in data)
                        {
                            TwiterFollowers objTwiterFriends = new TwiterFollowers();
                            objTwiterFriends.ScreenName = item["screen_name"].ToString().Replace("\"", "");
                            objTwiterFriends.ProfileImage = item["profile_image_url"].ToString().Replace("\"", "");
                            objTwiterFriends.TwitterId = item["id"].ToString().Replace("\"", "");
                            objTwiterFriends.Source = "Web";
                            listFollowers.Add(objTwiterFriends);
                        }
                    }

                    //==========================twitter tweet=========================//
                    //_twitter = new TwitterConsumer(oauth_consumer_key, oauth_consumer_secret);
                    //TokenManager.TokenSecrets.Add(oauth_token, oauth_token_secret);


                    if (Request.Cookies["VCtwoauth_token"] != null)
                    {
                        //if exists delete 
                        HttpCookie VCtwoauth_token = new HttpCookie("VCtwoauth_token", oauth_token);
                        if (!Request.Url.AbsoluteUri.Contains("localhost"))
                        {
                            VCtwoauth_token.Domain = "vconnect.com";
                            VCtwoauth_token.Path = "/";
                        }

                        VCtwoauth_token.Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies.Add(VCtwoauth_token);


                        //create again 
                        VCtwoauth_token = new HttpCookie("VCtwoauth_token", oauth_token);

                        if (!Request.Url.AbsoluteUri.Contains("localhost"))
                        {
                            VCtwoauth_token.Domain = "vconnect.com";
                            VCtwoauth_token.Path = "/";
                        }

                        VCtwoauth_token.Expires = DateTime.Now.AddDays(3);
                        Response.Cookies.Add(VCtwoauth_token);

                    }
                    else
                    {
                        HttpCookie VCtwoauth_token = new HttpCookie("VCtwoauth_token", oauth_token);
                        if (!Request.Url.AbsoluteUri.Contains("localhost"))
                        {
                            VCtwoauth_token.Domain = "vconnect.com";
                            VCtwoauth_token.Path = "/";
                        }

                        VCtwoauth_token.Expires = DateTime.Now.AddDays(3);
                        Response.Cookies.Add(VCtwoauth_token);

                    }

                    if (Request.Cookies["VCtwoauth_token_secret"] != null)
                    {
                        //if exists delete
                        HttpCookie VCtwoauth_token_secret = new HttpCookie("VCtwoauth_token_secret", oauth_token_secret);
                        if (!Request.Url.AbsoluteUri.Contains("localhost"))
                        {
                            VCtwoauth_token_secret.Domain = "vconnect.com";
                            VCtwoauth_token_secret.Path = "/";
                        }

                        VCtwoauth_token_secret.Expires = DateTime.Now.AddDays(3);
                        Response.Cookies.Add(VCtwoauth_token_secret);

                        //create again
                        VCtwoauth_token_secret = new HttpCookie("VCtwoauth_token_secret", oauth_token_secret);
                        if (!Request.Url.AbsoluteUri.Contains("localhost"))
                        {
                            VCtwoauth_token_secret.Domain = "vconnect.com";
                            VCtwoauth_token_secret.Path = "/";
                        }

                        VCtwoauth_token_secret.Expires = DateTime.Now.AddDays(3);
                        Response.Cookies.Add(VCtwoauth_token_secret);

                    }
                    else
                    {

                        HttpCookie VCtwoauth_token_secret = new HttpCookie("VCtwoauth_token_secret", oauth_token_secret);
                        if (!Request.Url.AbsoluteUri.Contains("localhost"))
                        {

                            VCtwoauth_token_secret.Domain = "vconnect.com";
                            VCtwoauth_token_secret.Path = "/";
                        }

                        VCtwoauth_token_secret.Expires = DateTime.Now.AddDays(3);
                        Response.Cookies.Add(VCtwoauth_token_secret);
                    }



                    //Session["twoauth_token"] = oauth_token;
                    //Session["twoauth_token_secret"] = oauth_token_secret;

                    //var endpoint = new MessageReceivingEndpoint("https://api.twitter.com/1.1/statuses/update.json", HttpDeliveryMethods.PostRequest | HttpDeliveryMethods.AuthorizationHeaderRequest);

                    //var parts = new[]
                    //{
                    //    MultipartPostPart.CreateFormPart("status", "Done from mbeta once changes")
                    //};

                    //var req = _twitter.PrepareAuthorizedRequest(endpoint, oauth_token, parts);

                    //var respo = req.GetResponse();
                    //==========================twitter tweet=========================//

                    //==========================twitter friends=========================//
                    string TWUID = !string.IsNullOrWhiteSpace(result.ProviderUserId) ? result.ProviderUserId.ToString().Trim() : "";

                    //twfollowers list 
                    if (listFollowers != null && listFollowers.Count > 0)
                    {
                        WEBLoginDatabaseCall.DBSaveTWUserFollowersList(listFollowers, Convert.ToInt32(UserId));
                        TempData["strTwiterFollowers"] = null;
                    }

                    int resErr = profilemodel.DBConnectUser(UserId, TWUID, "tw", oauth_token, oauth_token_secret);

                    if (resErr == 3)
                    {
                        TempData["UserAlert"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">This Twitter id is already associated with other account.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                    }
                    else if (resErr == 0)
                    {
                        //=====================twitter connected cookie=============//
                        if (Request.Cookies["VCistwconnect"] != null)
                        {
                            HttpCookie VCistwconnect = new HttpCookie("VCistwconnect");
                            if (!Request.Url.AbsoluteUri.Contains("localhost") && !Request.Url.AbsoluteUri.Contains("http://stage.vconnect.com/"))
                            {
                                VCistwconnect.Domain = "vconnect.com";
                                VCistwconnect.Path = "/";
                            }
                            VCistwconnect.Expires = DateTime.Now.AddDays(-1);
                            Response.Cookies.Add(VCistwconnect);

                            VCistwconnect = new HttpCookie("VCistwconnect", "1");
                            if (!Request.Url.AbsoluteUri.Contains("localhost") && !Request.Url.AbsoluteUri.Contains("http://stage.vconnect.com/"))
                            {
                                VCistwconnect.Domain = "vconnect.com";
                                VCistwconnect.Path = "/";
                            }
                            VCistwconnect.Expires = DateTime.Now.AddDays(3);
                            Response.Cookies.Add(VCistwconnect);


                        }
                        else
                        {

                            HttpCookie VCistwconnect = new HttpCookie("VCistwconnect", "1");
                            if (!Request.Url.AbsoluteUri.Contains("localhost") && !Request.Url.AbsoluteUri.Contains("http://stage.vconnect.com/"))
                            {
                                VCistwconnect.Domain = "vconnect.com";
                                VCistwconnect.Path = "/";
                            }
                            VCistwconnect.Expires = DateTime.Now.AddDays(3);
                            Response.Cookies.Add(VCistwconnect);

                        }

                        //=====================twitter connected cookie=============//
                    }
                    else if (resErr == 1)
                    {
                        TempData["UserAlert"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Some error occured Please try again later.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                    }
                    //===========================================//

                }


                if (Session["rturl"] != null && !string.IsNullOrEmpty(Session["rturl"].ToString().Trim()))
                {
                    rturl = "/" + Session["rturl"].ToString();
                    return Redirect(rturl);
                }
                else
                {
                    return Redirect("/");
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                if (Session["rturl"] != null && !string.IsNullOrEmpty(Session["rturl"].ToString().Trim()))
                    return Redirect("/login?rturl=" + Session["rturl"].ToString().Trim().Replace("&", "$"));
                else
                    return Redirect("/login");
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateInput(false)]
        public ActionResult ExternalLogin(string provider = "", string rturl = "")
        {
            if (TempData["UserAlert"] == null || TempData["UserAlert"] == "")
                TempData["UserAlert"] = "<div></div>";
            try
            {
                Session["rturl"] = null;
                if (Request.QueryString["rturl"] != null)
                {
                    Session["rturl"] = Request.QueryString["rturl"].ToString().Trim().Replace("$", "&");
                }

                if (provider != null && provider.Trim() != "")
                {
                    return new Vconnect.Controllers.AccountController.ExternalLoginResult(provider, Url.Action("ExternalLoginCallback", new { Provider = provider, rturl = rturl }));
                }
                else
                {
                    if (Session["rturl"] != null && !string.IsNullOrEmpty(Session["rturl"].ToString().Trim()))
                        return Redirect("/login?rturl=" + Session["rturl"].ToString().Trim().Replace("&", "$"));
                    else
                        return Redirect("/login");
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
                if (Session["rturl"] != null && !string.IsNullOrEmpty(Session["rturl"].ToString().Trim()))
                    return Redirect("/login?rturl=" + Session["rturl"].ToString().Trim().Replace("&", "$"));
                else
                    return Redirect("/login");

            }
        }

        protected string GenerateAuthHeader(string resorcUrl)
        {
            // oauth implementation details
            var oauth_version = "1.0";
            var oauth_signature_method = "HMAC-SHA1";

            // unique request details
            var oauth_nonce = Convert.ToBase64String(
                new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
            var timeSpan = DateTime.UtcNow
                - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var oauth_timestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();

            // message api details
            //var status = "Updating status via REST API 1.1";
            resource_url = resorcUrl;

            // create oauth signature
            var baseFormat = "oauth_consumer_key={0}&oauth_nonce={1}&oauth_signature_method={2}" +
                            "&oauth_timestamp={3}&oauth_token={4}&oauth_version={5}";

            var baseString = string.Format(baseFormat,
                                        oauth_consumer_key,
                                        oauth_nonce,
                                        oauth_signature_method,
                                        oauth_timestamp,
                                        oauth_token,
                                        oauth_version
                //,Uri.EscapeDataString(status)
                                        );

            baseString = string.Concat("GET&", Uri.EscapeDataString(resource_url), "&", Uri.EscapeDataString(baseString));

            var compositeKey = string.Concat(Uri.EscapeDataString(oauth_consumer_secret),
                                    "&", Uri.EscapeDataString(oauth_token_secret));

            string oauth_signature;
            using (HMACSHA1 hasher = new HMACSHA1(ASCIIEncoding.ASCII.GetBytes(compositeKey)))
            {
                oauth_signature = Convert.ToBase64String(
                    hasher.ComputeHash(ASCIIEncoding.ASCII.GetBytes(baseString)));
            }

            // create the request header
            var headerFormat = "OAuth oauth_nonce=\"{0}\", oauth_signature_method=\"{1}\", " +
                               "oauth_timestamp=\"{2}\", oauth_consumer_key=\"{3}\", " +
                               "oauth_token=\"{4}\", oauth_signature=\"{5}\", " +
                               "oauth_version=\"{6}\"";

            var authHeader = string.Format(headerFormat,
                                    Uri.EscapeDataString(oauth_nonce),
                                    Uri.EscapeDataString(oauth_signature_method),
                                    Uri.EscapeDataString(oauth_timestamp),
                                    Uri.EscapeDataString(oauth_consumer_key),
                                    Uri.EscapeDataString(oauth_token),
                                    Uri.EscapeDataString(oauth_signature),
                                    Uri.EscapeDataString(oauth_version)
                            );


            // make the request
            // var postBody = "status=" + Uri.EscapeDataString(status);

            ServicePointManager.Expect100Continue = false;

            return authHeader;
        }

        #endregion

        #region Google

        public ActionResult oauth2callback(string state, string code, string error)
        {
            if (TempData["UserAlert"] == null || TempData["UserAlert"] == "")
                TempData["UserAlert"] = "<div></div>";
            List<GoogleOauthParameters> gopList = new List<GoogleOauthParameters>();
            gopList = (List<GoogleOauthParameters>)TempData.Peek("gopList");

            GoogleResponse responseFromServer = new GoogleResponse();
            try
            {
                HttpWebRequest requestToken = (HttpWebRequest)WebRequest.Create("https://accounts.google.com/o/oauth2/token");
                requestToken.Method = "POST";
                string postData = string.Format(
                    "code={0}&client_id={1}&client_secret={2}&redirect_uri={3}&grant_type=authorization_code",
                    gopList[0].Code, gopList[0].ClientId, gopList[0].ClientSecret, gopList[0].RedirectURIs);

                Session["rturl"] = gopList[0].Rturl.ToString().Trim();

                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                requestToken.ContentType = "application/x-www-form-urlencoded";
                requestToken.ContentLength = byteArray.Length;
                using (Stream dataStream = requestToken.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();
                    WebResponse response = requestToken.GetResponse();
                    if (((HttpWebResponse)response).StatusCode == HttpStatusCode.OK)
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            responseFromServer = Deserialise<GoogleResponse>(reader.ReadToEnd());
                            reader.Close();
                            dataStream.Close();
                            response.Close();
                        }
                    }
                    else
                    {
                        dataStream.Close();
                        response.Close();
                        ViewBag.error = ((HttpWebResponse)response).StatusDescription;
                        return View("Index");
                    }
                }
                if (responseFromServer != null && !string.IsNullOrEmpty(responseFromServer.access_token))
                {
                    url = string.Format("https://www.googleapis.com/oauth2/v1/userinfo?access_token={0}", responseFromServer.access_token);

                    HttpWebRequest requestUserInfo = (HttpWebRequest)WebRequest.Create(url);
                    HttpWebResponse responseUserInfo = (HttpWebResponse)requestUserInfo.GetResponse();
                    if (((HttpWebResponse)responseUserInfo).StatusCode == HttpStatusCode.OK)
                    {
                        using (Stream receiveStream = responseUserInfo.GetResponseStream())
                        {
                            Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                            using (StreamReader readStream = new StreamReader(receiveStream, encode))
                            {
                                gId = Deserialise<GoogleId>(readStream.ReadToEnd());
                                responseUserInfo.Close();
                                readStream.Close();

                                var parameters = new OAuth2Parameters
                                {
                                    ClientId = ConfigurationManager.AppSettings["gmailclientid"].ToString().Trim(),
                                    ClientSecret = ConfigurationManager.AppSettings["gmailclientsecret"].ToString().Trim(),
                                    RedirectUri = @ConfigurationManager.AppSettings["gmailredirecturl"].ToString().Trim(),
                                    Scope = @"http://www.google.com/m8/feeds",
                                    AccessCode = code,
                                    AccessToken = responseFromServer.access_token,  /* use the value returned from the old call to GetAccessToken here */
                                    RefreshToken = responseFromServer.access_token,

                                };

                                List<FetchContactsModel> contactDetails = new List<FetchContactsModel>();
                                string App_Name = "MyNetworxk Web Application!";
                                ContactsQuery query = new ContactsQuery(ContactsQuery.CreateContactsUri("default"));
                                RequestSettings rs = new RequestSettings(App_Name, parameters);
                                rs.AutoPaging = true;
                                ContactsRequest cr = new ContactsRequest(rs);
                                Feed<Google.Contacts.Contact> f = cr.GetContacts();

                                if (f.Entries != null)
                                {
                                    Feed<Google.Contacts.Contact> feed = cr.GetContacts();
                                    foreach (Google.Contacts.Contact entry in feed.Entries)
                                    {
                                        FetchContactsModel contact = new FetchContactsModel
                                        {
                                            Name = !string.IsNullOrEmpty(entry.Name.FullName) ? entry.Name.FullName : "",
                                            EmailId = entry.Emails.Count >= 1 ? entry.Emails[0].Address : "",
                                            SecondaryEmail = entry.Emails.Count >= 2 ? entry.Emails[1].Address : "",
                                            Phonenumber = entry.Phonenumbers.Count >= 1 ? entry.Phonenumbers[0].Value : "",
                                            Source = "Web",
                                        };

                                        contactDetails.Add(contact);
                                    }
                                }

                                TempData["gousers"] = contactDetails;

                                string rturl = "";
                                string googleID = string.Empty;


                                googleID = gId.id.ToString().Trim();
                                UserId = Int32.Parse(objUS.SV_VCUserContentID.ToString().Trim());

                                int resErr = profilemodel.DBConnectUser(UserId, googleID, "gm", "", "");

                                if (resErr == 3)
                                {
                                    TempData["UserAlert"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">This Google id is already associated with other account.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                                }
                                else if (resErr == 0)
                                {
                                    //=====================google connected cookie=============//
                                    HttpCookie VCisgmconnect = new HttpCookie("VCisgmconnect", "1");
                                    if (!Request.Url.AbsoluteUri.Contains("localhost") && !Request.Url.AbsoluteUri.Contains("http://stage.vconnect.com/"))
                                    {
                                        VCisgmconnect.Domain = "vconnect.com";
                                        VCisgmconnect.Path = "/";
                                    }
                                    VCisgmconnect.Expires = DateTime.Now.AddDays(3);
                                    Response.Cookies.Add(VCisgmconnect);
                                    //=====================facebook connected cookie=============//

                                    //googleContacts list 
                                    if (contactDetails != null && contactDetails.Count > 0)
                                    {
                                        WEBLoginDatabaseCall.DBSaveGoogleContactsList(contactDetails, Convert.ToInt32(UserId));
                                        TempData["gousers"] = null;
                                    }

                                    if (Session["rturl"] != null && !string.IsNullOrEmpty(Session["rturl"].ToString().Trim()))
                                    {
                                        rturl = "/" + Session["rturl"].ToString().Trim();
                                        return Redirect(rturl);
                                    }
                                    else
                                    {
                                        return Redirect("/");
                                    }
                                }
                                else if (resErr == 1)
                                {
                                    TempData["UserAlert"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Some error occured Please try again later.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                                }
                                //===========================================//

                                if (Session["rturl"] != null && !string.IsNullOrEmpty(Session["rturl"].ToString().Trim()))
                                {
                                    rturl = "/" + Session["rturl"].ToString().Trim();
                                    return Redirect(rturl);
                                }
                                else
                                {
                                    return Redirect("/");

                                }
                            }
                        }
                    }
                }

            }
            catch (System.Net.WebException ex)
            {
                log.LogMe(ex);
                return Redirect("/login");
            }

            return Redirect("/login");
        }

        public static T Deserialise<T>(string json)
        {
            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(json)))
            {
                var serialiser = new DataContractJsonSerializer(typeof(T));
                return (T)serialiser.ReadObject(ms);
            }
        }

        #endregion

        #region Share
        public ActionResult share(string con, FormCollection collection)
        {
            string token = "";
            string tokensecret = "";
            profilemodel UDM = new profilemodel();

            int res = 0;
            if (!Int32.TryParse(objUS.SV_VCUserContentID.ToString().Trim(), out res))
            {
                TempData["UserAlert"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\">Incorrect UserId.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                UDM.IsErr = 1;
                return View(UDM);
            }
            else
            {
                UserId = Int32.Parse(objUS.SV_VCUserContentID.ToString().Trim());
            }

            int isShared = 0;

            switch (con)
            {
                case "fb":
                    isShared = (collection["chkfbshare"] != null) ? 1 : 0;
                    UDM.IsFBShare = isShared == 1 ? "checked" : "";
                    if (Request.Cookies["VCfboauth_token"] != null)
                    {
                        token = Request.Cookies["VCfboauth_token"].Value.ToString().Trim();
                    }
                    break;
                case "tw":
                    isShared = (collection["chktwshare"] != null) ? 1 : 0;
                    UDM.IsTWShare = isShared == 1 ? "checked" : "";
                    if (Request.Cookies["VCtwoauth_token"] != null)
                    {
                        token = Request.Cookies["VCtwoauth_token"].Value.ToString().Trim();
                    }
                    if (Request.Cookies["VCtwoauth_token_secret"] != null)
                    {
                        tokensecret = Request.Cookies["VCtwoauth_token_secret"].Value.ToString().Trim();
                    }
                    break;
            }

            try
            {
                int resErr = profilemodel.DBShareLikesSaveActivities(UserId, isShared, con, token, tokensecret);
                if (resErr == 1)
                {
                    TempData["UserAlert"] = "<div id=\"alertErr\" class=\"alert-box error\" data-alert=\"\" style=\"margin-top:-26px\">Some error occured Please try later.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                }
                else
                {
                    TempData["UserAlert"] = "<div class=\"alert-box success\" id=\"alertErr\" style=\"margin-top:-26px\">Preference saved successfully.<a class=\"close\" href=\"javascript:void(0);\" onclick=\"document.getElementById('alertErr').style.display='none';\">×</a></div>";
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);
            }

            TempData["UDM"] = UDM;
            TempData.Keep("UDM");

            return Redirect("/UserDashboardWEB/EditProfile");
        }

        #endregion
    }
}

