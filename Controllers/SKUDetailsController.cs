﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Vconnect.Controllers
{
    public class SKUDetailsController : Controller
    {
        //
        // GET: /SKUDetails/

        public ActionResult SKUDetails()
        {
            return View();
        }
        public ActionResult KeyFeatures()
        {
            return View();
        }

        public ActionResult SellerListing()
        {
            return View();
        }

        public ActionResult ProductDetails()
        {
            return View();
        }

        public ActionResult SimilarProduct()
        {
            return View();
        }

        public ActionResult ReviewComments()
        {
            return View();
        }

        public ActionResult RecentlyViewed()
        {
            return View();
        }

    }
}
