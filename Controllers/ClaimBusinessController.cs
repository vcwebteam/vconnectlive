﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vconnect.Models;
using Vconnect.Mapping.Listing;
using Vconnect.Common;
using System.IO;
using System.Text;
using Vconnect.Mapping.CommonDb;
using Vconnect.Mapping;
using Vconnect.Enums;
using System.Security.Cryptography;
using System.Configuration;
namespace Vconnect.Controllers
{
    public class ClaimBusinessController : Controller
    {
        //
        // GET: /ClaimBusiness/
        UserSession objUS = new UserSession();
        public ActionResult WebIndex()
        {
            //SendSMS objSendSMS = new SendSMS();
            //string SMSResponse = objSendSMS.Sendmessage("Dear Customer,Test message From ashish: " + "00" + ".", "07051192912");
            ClaimBusinessModel obj = new ClaimBusinessModel();


            Session["rtype"] = "WEB";
            return Redirect("/");
            //if (Request.Cookies["VCLoginID"] != null && Request.Cookies["VCLoginID"].ToString() != "" && Request.Cookies["VCLoginID"].ToString() != "0")
            //{
            //    return View();
            //}
            //else
            //{
            //   // return View("CYBLogin");
            //   return Redirect("/");
            //}
        }
        public ActionResult Index()
        {

            Session["rtype"] = "WAP";
            return Redirect("/");
            //if (Request.Cookies["VCLoginID"] != null && Request.Cookies["VCLoginID"].ToString() != "" && Request.Cookies["VCLoginID"].ToString() != "0")
            //{
            //    Session["Successcontroller"] = null;
            //    Session["Successaction"] = null;
            //    return View();
            //}
            //else
            //{

            //    //Session["Successcontroller"] = "ClaimBusiness";
            //   // Session["Successaction"] = "Index";                
            //    return Redirect("/login?rturl=ClaimBusiness/Index");
            //   // return RedirectToAction("Login", "WapLogin");
            //}


        }
        public ActionResult SearchCYB(ClaimBusinessModel CYBModel, int? Page)
        {
            //  ClaimBusinessModel CYBM = new ClaimBusinessModel();
            // HomeModel homeWapModel = new HomeModel();
            //if (Request.Cookies["VCLoginID"] == null || Request.Cookies["VCLoginID"].ToString() == "" || Request.Cookies["VCLoginID"].ToString() == "0")
            if (string.IsNullOrEmpty(objUS.SV_VCLoginID) || objUS.SV_VCLoginID.ToString() == "" || objUS.SV_VCLoginID.ToString() == "0")
            {
                if (MyGlobalVariables.IsWapWeb != "WAP")
                {
                    // return View("CYBLogin");
                   return Redirect("/");
                }
                else if (MyGlobalVariables.IsWapWeb == "WAP")
                {
                    //Session["Successcontroller"] = "ClaimBusiness";
                    // Session["Successaction"] = "Index";                
                    return Redirect("/login?rturl=ClaimBusiness/Index");
                    // return RedirectToAction("Login", "WapLogin");
                }
            }
            List<SearchListCYB> searchListBuisness = new List<SearchListCYB>();
            string businessname = string.Empty;
            string location = string.Empty;
            string phone = string.Empty;

            if (CYBModel.SearchBuisnesname != null)
            {
                businessname = CYBModel.SearchBuisnesname;
                Session["SearchedKeywordCYB"] = businessname;
            }
            else
            {
                businessname = "0";
            }
            if (CYBModel.SearchBusinessLocation != null)
            {
                location = CYBModel.SearchBusinessLocation;
            }
            else
            {
                location = "0";
            }
            if (CYBModel.SearchBusinessPhone != null)
            {
                phone = CYBModel.SearchBusinessPhone;
                Session["SearchedPhoneCYB"] = phone;
            }
            else
            {
                phone = "0";
            }
            if (Session["SearchedKeywordCYB"] != null)
            {
                businessname = Session["SearchedKeywordCYB"].ToString();
            }
            if (Session["SearchedPhoneCYB"] != null)
            {
                phone = Session["SearchedPhoneCYB"].ToString();
            }

            using (var db = new VconnectDBContext29())
            {

                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_wap_get_claimbiz_opt]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object               
                cmd.Parameters.Add(new SqlParameter("@businessname", businessname));
                cmd.Parameters.Add(new SqlParameter("@location", location));
                cmd.Parameters.Add(new SqlParameter("@Userphone", phone));
                cmd.Parameters.Add(new SqlParameter("@buisnessid", CYBModel.bid));

                try
                {


                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    searchListBuisness = ((IObjectContextAdapter)db).ObjectContext.Translate<SearchListCYB>(reader).ToList();
                    CYBModel.SearchListCYB = searchListBuisness;


                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            return View("SearchCYBWAP", CYBModel);

        }
        [HttpPost]
        public ActionResult SearchCYBWeb(ClaimBusinessModel CYBModel, int? Page)
        {
            //  ClaimBusinessModel CYBM = new ClaimBusinessModel();
            // HomeModel homeWapModel = new HomeModel();
            //if (Request.Cookies["VCLoginID"] == null || Request.Cookies["VCLoginID"].ToString() == "" || Request.Cookies["VCLoginID"].ToString() == "0")
            if (string.IsNullOrEmpty(objUS.SV_VCLoginID) || objUS.SV_VCLoginID.ToString() == "" || objUS.SV_VCLoginID.ToString() == "0")
            {
                if (MyGlobalVariables.IsWapWeb != "WAP")
                {
                    // return View("CYBLogin");
                   return Redirect("/");
                }
                else if (MyGlobalVariables.IsWapWeb == "WAP")
                {
                    //Session["Successcontroller"] = "ClaimBusiness";
                    // Session["Successaction"] = "Index";                
                    return Redirect("/login?rturl=ClaimBusiness/Index");
                    // return RedirectToAction("Login", "WapLogin");
                }
            }
            List<SearchListCYBWeb> searchListBuisness = new List<SearchListCYBWeb>();


            string businessname = string.Empty;
            string location = string.Empty;
            string phone = string.Empty;
            int buisnessidAlreadyClaimed = 0;
            int Userid = 0;
            if (CYBModel.SearchBuisnesname != null)
            {
                businessname = CYBModel.SearchBuisnesname;
                Session["SearchedKeywordCYB"] = businessname;
            }
            else
            {
                businessname = "0";
            }
            if (CYBModel.SearchBusinessLocation != null)
            {
                location = CYBModel.SearchBusinessLocation;
            }
            else
            {
                location = "0";
            }
            if (CYBModel.SearchBusinessPhone != null)
            {
                phone = CYBModel.SearchBusinessPhone;
                Session["SearchedPhoneCYB"] = phone;
            }
            else
            {
                phone = "0";
            }
            if (Session["SearchedKeywordCYB"] != null)
            {
                businessname = Session["SearchedKeywordCYB"].ToString();
            }
            if (Session["SearchedPhoneCYB"] != null)
            {
                phone = Session["SearchedPhoneCYB"].ToString();
            }

            using (var db = new VconnectDBContext29())
            {

                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_Get_ClaimBiz_opt]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object               
                cmd.Parameters.Add(new SqlParameter("@Userid", Userid));
                cmd.Parameters.Add(new SqlParameter("@businessname", businessname));
                cmd.Parameters.Add(new SqlParameter("@location", location));
                cmd.Parameters.Add(new SqlParameter("@Userphone", phone));
                cmd.Parameters.Add(new SqlParameter("@buisnessid", CYBModel.bid));
                cmd.Parameters.Add(new SqlParameter("@buisnessidAlreadyClaimed", buisnessidAlreadyClaimed));

                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    searchListBuisness = ((IObjectContextAdapter)db).ObjectContext.Translate<SearchListCYBWeb>(reader).ToList();
                    CYBModel.SearchListCYBWeb = searchListBuisness;
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            return View("WebIndex", CYBModel);

        }
        //


        // GET: /ClaimBusiness/Details/5
        [HttpGet]
        public ActionResult SearchCYBDetails(ClaimBusinessModel CYBModel, int? bid, string bname)
        {
            try
            {
                MyGlobalVariables.IsWapWeb = "Web";
                if (bid == null && Session["bid"]==null)
                {
                    return Redirect("/"); 
                }
                //if (Request.Cookies["VCLoginID"] == null || Request.Cookies["VCLoginID"].ToString() == "" || Request.Cookies["VCLoginID"].ToString() == "0")
                if (string.IsNullOrEmpty(objUS.SV_VCLoginID) || objUS.SV_VCLoginID.ToString() == "" || objUS.SV_VCLoginID.ToString() == "0")
                {
                    if (MyGlobalVariables.IsWapWeb != "WAP")
                    {
                        // return View("CYBLogin");
                        return Redirect("/");
                    }
                    else if (MyGlobalVariables.IsWapWeb == "WAP")
                    {
                        //Session["Successcontroller"] = "ClaimBusiness";
                        // Session["Successaction"] = "Index";                
                        return Redirect("/login?rturl=ClaimBusiness/Index");
                        // return RedirectToAction("Login", "WapLogin");
                    }
                }
                List<SearchListDetails> SearchListDetails = new List<SearchListDetails>();
                if (Session["CYBError"] == null)
                {
                    Session["CYBError"] = string.Empty;
                }
                string businessname = string.Empty;
                string location = string.Empty;
                string phone = string.Empty;
                int businessid = 0;
                
                if (bid != null)
                {
                    businessid = bid ?? 1;
                    Session["bid"] = bid;
                }
                else
                {
                    businessid = System.Convert.ToInt32(Session["bid"]);
                }
                getBusinessAddress(businessid.ToString(), CYBModel);

                bname = CYBModel.SearchBuisnesname;
                if (CYBModel.isclaim == 1)
                {
                    return RedirectToActionPermanent("CYBReportIncorrectComments", new { bid = businessid });
                }
                if (bname != null)
                {
                    businessname = bname;
                    Session["bname"] = businessname;
                }
                else
                {
                    businessname = Session["bname"].ToString();
                }
                CYBModel.bid = businessid.ToString();
                CYBModel.SearchBuisnesname = businessname;

                using (var db = new VconnectDBContext29())
                {

                    // Create a SQL command to execute the sproc
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[prc_wap_get_bizphonelist_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object               

                    cmd.Parameters.Add(new SqlParameter("@bid", businessid));

                    try
                    {


                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        SearchListDetails = ((IObjectContextAdapter)db).ObjectContext.Translate<SearchListDetails>(reader).ToList();
                        CYBModel.SearchListDetails = SearchListDetails;


                    }
                    finally
                    {
                        db.Database.Connection.Close();
                    }
                }
                if (MyGlobalVariables.IsWapWeb == "WAP")
                {
                    return View("SearchCYBDetails", CYBModel);

                }
                else
                {
                    CYBModel.claimno = "1";
                    return View("SearchCYBDetailsWeb", CYBModel);
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);

                return Redirect("/");
            }
            finally
            {

            }
        }

        public ActionResult GetCYBDetails(ClaimBusinessModel CYBModel, int? bid, string bname)
        {
            //  ClaimBusinessModel CYBM = new ClaimBusinessModel();
            // HomeModel homeWapModel = new HomeModel();
            //if (Request.Cookies["VCLoginID"] == null || Request.Cookies["VCLoginID"].ToString() == "" || Request.Cookies["VCLoginID"].ToString() == "0")
            if (string.IsNullOrEmpty(objUS.SV_VCLoginID) || objUS.SV_VCLoginID.ToString() == "" || objUS.SV_VCLoginID.ToString() == "0")
            {
                if (MyGlobalVariables.IsWapWeb != "WAP")
                {
                    // return View("CYBLogin");
                   return Redirect("/");
                }
                else if (MyGlobalVariables.IsWapWeb == "WAP")
                {
                    //Session["Successcontroller"] = "ClaimBusiness";
                    // Session["Successaction"] = "Index";                
                    return Redirect("/login?rturl=ClaimBusiness/Index");
                    // return RedirectToAction("Login", "WapLogin");
                }
            }
            List<SearchBizDetailsClaim> SearchBizDetailsClaim = new List<SearchBizDetailsClaim>();
            string businessname = string.Empty;
            string location = string.Empty;
            string phone = string.Empty;
            int businessid = 0;
            if (bid != null)
            {
                businessid = bid ?? 1;
                Session["bid"] = bid;
            }
            else
            {
                businessid = System.Convert.ToInt32(Session["bid"]);
            }
            if (bname != null)
            {
                businessname = bname;
                Session["businessname"] = businessname;
            }
            else
            {
                businessname = Session["businessname"].ToString();
            }
            CYBModel.SearchBuisnesname = businessname;
            using (var db = new VconnectDBContext29())
            {

                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_wap_get_claimbizdetail_opt]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //Add parameters to command object               

                cmd.Parameters.Add(new SqlParameter("@bid", businessid));

                try
                {


                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();

                    SearchBizDetailsClaim = ((IObjectContextAdapter)db).ObjectContext.Translate<SearchBizDetailsClaim>(reader).ToList();
                    CYBModel.SearchBizDetailsClaim = SearchBizDetailsClaim;


                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            return View("GetCYBDetails", CYBModel);

        }



        public ActionResult CYBReportIncorrectComments(ClaimBusinessModel CYBModel,int? bid)
        {
            try
            {
                //if (Request.Cookies["VCLoginID"] == null || Request.Cookies["VCLoginID"].ToString() == "" || Request.Cookies["VCLoginID"].ToString() == "0")
                if (string.IsNullOrEmpty(objUS.SV_VCLoginID) || objUS.SV_VCLoginID.ToString() == "" || objUS.SV_VCLoginID.ToString() == "0")
                {
                    if (MyGlobalVariables.IsWapWeb != "WAP")
                    {
                        // return View("CYBLogin");
                        return Redirect("/");
                    }
                    else if (MyGlobalVariables.IsWapWeb == "WAP")
                    {
                        //Session["Successcontroller"] = "ClaimBusiness";
                        // Session["Successaction"] = "Index";                
                        return Redirect("/login?rturl=ClaimBusiness/Index");
                        // return RedirectToAction("Login", "WapLogin");
                    }
                }

                Session["CYBError"] = null;
                if (Session["bid"] == null)
                {
                    Session["bid"] = bid.ToString();
                }
                getBusinessAddress(bid.ToString(), CYBModel);
                // CYBModel.SearchBuisnesname = Session["businessname"].ToString();
                if (MyGlobalVariables.IsWapWeb == "WAP")
                {
                    return View("CYBReportIncorrectComments", CYBModel);
                }
                else
                {
                    return View("CYBReportIncorrectCommentsWeb", CYBModel);
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);

                return Redirect("/");
            }
            finally
            {

            }
        }
        public ActionResult VerifyCYBReportIncorrectCode(ClaimBusinessModel CYBModel, int? bid)
        {
            try
            {
                //if (Request.Cookies["VCLoginID"] == null || Request.Cookies["VCLoginID"].ToString() == "" || Request.Cookies["VCLoginID"].ToString() == "0")
                if (string.IsNullOrEmpty(objUS.SV_VCLoginID) || objUS.SV_VCLoginID.ToString() == "" || objUS.SV_VCLoginID.ToString() == "0")
                {
                   
                       
                        return Redirect("/");
                   
                    
                }
                string uid = objUS.SV_VCUserContentID;
                Session["Userid"] = objUS.SV_VCUserContentID;
                string code = CYBModel.ClaimVerifyCode;
                Session["VCHasVerifiedMobile"] = true;
                CYBModel.RequestType = "Claim";
                CYBModel.typ = 'R';
                string remarks = CYBModel.ReportIncorrectComments;
                //string e = Request.QueryString[0];
                string result = claimyoubusinessupdate(Session["bid"].ToString(), uid, Session["comments"].ToString(), Session["phone"].ToString(), code, "R", (MyGlobalVariables.IsWapWeb));
                if (result == "Success")
                {
                    //string SMSResponse = objSendSMS.Sendmessage("Dear Customer, Your OTP for verifying mobile number is: " +sRandomCode + ".", homeWebModel.Phone.ToString().Trim());
                    string SMSResponse = string.Empty;

                    //Maintain the SMS EMAIL LOG
                    //Utility.SaveSMSEmailLog(Convert.ToInt32(uid), Usertype.OtherUser.GetHashCode(), "", Session["phone"].ToString(), "UserName", MessageType.SMS_UserReg.GetHashCode(), "Claim Business verification code", "Dear Customer, Your OTP for verifying mobile number is:" + code.ToString().Trim() + ".", 0, "", "", SMSResponse, Utility.GetIpAddress(), 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), Utility.GetReversedns(), (MyGlobalVariables.IsWapWeb);
                   /////



                    ///////
                  
                    Utility.SaveSMSEmailLog(Convert.ToInt32(uid), Usertype.OtherUser.GetHashCode(), "", Session["phone"].ToString(), "UserName", MessageType.SMS_UserReg.GetHashCode(), "Claim Business verification code", "Dear Customer, Your OTP for verifying mobile number is:" + code.ToString().Trim() + ".", 0, "", "", SMSResponse, Utility.GetIpAddress(), 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), Utility.GetReversedns(Utility.GetIpAddress()), "Web");
                    if ((Session["VCHasVerifiedMobile"] == null || !Convert.ToBoolean(Session["VCHasVerifiedMobile"].ToString())))//&& typ == 'D'
                    {
                        Session["VCHasVerifiedMobile"] = true;
                        //  claimusersmobileupdate();
                    }

                    return View("thanks", CYBModel);
                }
                else
                {
                    ViewData["alertmsg"] = result;
                    return View("CYBVerifyCodeReportIncorrect", CYBModel);
                    //litMessage.Text = "<p class='errormsg'>Mobile not verified.</p>";
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);

                return Redirect("/");
            }
            finally
            {

            }
        }
        //
        [HttpPost]
        public ActionResult VerifyCYBCode(ClaimBusinessModel CYBModel, int? bid, FormCollection frm)
        {
            try
            {
                //if (Request.Cookies["VCLoginID"] == null || Request.Cookies["VCLoginID"].ToString() == "" || Request.Cookies["VCLoginID"].ToString() == "0")
                if (string.IsNullOrEmpty(objUS.SV_VCLoginID) || objUS.SV_VCLoginID.ToString() == "" || objUS.SV_VCLoginID.ToString() == "0")
                {

                    return Redirect("/");


                }
                string uid = objUS.SV_VCUserContentID;


                Session["Userid"] = objUS.SV_VCUserContentID;
                string code = CYBModel.ClaimVerifyCode;

                if (code == "" || code == null)
                {
                    ViewData["alertmsg"] = "Enter Verifcation code";
                    Session["CYBError"] = "Enter Verifcation code";
                    // getBusinessAddress(Session["bid"].ToString(), CYBModel);              
                    return View("CYBVerifyCodeWeb", CYBModel);

                }
                ///////
                if (frm["ClaimType"] != null)
                {
                    Session["Claimtype"] = frm["ClaimType"];
                }
                if (frm["bid"] != null)
                {
                    Session["bid"] = frm["bid"];
                }

                if (frm["SelectedPhone"] != null)
                {
                    Session["phone"] = frm["SelectedPhone"];
                }
                if (Session["Claimtype"] == null || Session["bid"] == null || Session["phone"] == null)
                {
                  
                   
                       ViewData["alertmsg"] = "Some Error Occured please try to  claim business again";
                       Session["CYBError"] = "Some Error Occured please try to  claim business again";

                       return View("CYBVerifyCodeWeb", CYBModel);
                   

                    //litMessage.Text = "<p class='errormsg'>Mobile not verified.</p>";
                }
                ////
                string d = Session["Claimtype"].ToString();
                Session["VCHasVerifiedMobile"] = true;
                CYBModel.RequestType = "Claim";
                if (string.IsNullOrEmpty(d))
                {
                    CYBModel.typ = 'D';
                    d = "D";
                }
                getBusinessAddress(Session["bid"].ToString(), CYBModel);
                //string e = Request.QueryString[0];
                string result;
                if (CYBModel.typ.ToString() == "R")
                {

                    result = claimyoubusinessupdate(Session["bid"].ToString(), uid, Session["comments"].ToString(), Session["phone"].ToString(), code, "R", (MyGlobalVariables.IsWapWeb));
                }
                else
                {
                    result = claimybusinessupdate(Session["bid"].ToString(), uid, Session["phone"].ToString(), code, d, "Web");
                }
                if (result == "Success")
                {
                    if (CYBModel.typ.ToString() == "D")
                    {
                        if (!getmobileVerication(Session["phone"].ToString(), uid))
                        {

                            claimusersmobileupdate(Session["phone"].ToString());
                        }
                    }
                    if (CYBModel.typ.ToString() == "R" || CYBModel.typ.ToString() == "N")
                    {

                        return View("CYBThankyou", CYBModel);


                    }

                    // if (typ == 'D') sendsmsclaimuser();
                    if (MyGlobalVariables.IsWapWeb == "WAP")
                    {
                        // return View("thanks", CYBModel);
                        ViewBag.alertmsg = "You Have Sucessfully Claimed the Business";
                        //return Redirect("/mybusiness");
                        if (objUS.SV_VCLoginType.ToString() == "WAP")
                        {
                            LoginModel model = new LoginModel();
                            if (Session["model"] != null)
                            {
                                model = (LoginModel)Session["model"];
                            }
                            else
                            {
                                model.IsDuplicate = "0";
                                Session["model"] = model;
                            }
                            //model.Password = Request.Cookies["VCPassword"].Value.ToString();
                            model.Login = objUS.SV_VCLoginID.ToString();
                            ///
                            /// 
                            using (var db = new VconnectDBContext29())
                            {
                                var cmd = db.Database.Connection.CreateCommand();
                                cmd.CommandText = "select top 1 password   from usercredential where customerid =" + uid;
                                cmd.CommandType = CommandType.Text;

                                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                                connection.Open();
                                var reader = cmd.ExecuteReader();
                                reader.Read();
                                model.Password = reader["password"].ToString();


                            }

                            //
                            Session["model"] = model;
                            //Session["rturl"] = "mybusinesss";
                            Session["rturl"] = "mybusiness";
                            objUS.SV_VCUserType = "59";
                            return Redirect("/mybusiness");
                            //return Redirect("/Account/WebLogin");
                        }
                        else
                        {
                            AccountController ac = new AccountController();
                            if (objUS.SV_VCLoginType.Contains( "FB"))
                            {
                                Session["rturl"] = "mybusiness";
                                return ac.RegisterFBUser(new RegisterExternalLoginModel { Email = objUS.SV_VCLoginID.ToString(), Mobile = "", ProviderId = objUS.SV_VCProviderId.ToString(), Provider = "Facebook", rurl = "mybusiness" });

                            }
                            else if (objUS.SV_VCLoginType.Contains("twitter"))
                            {

                                Session["rturl"] = "mybusiness";
                                return ac.RegisterTwitterUser(new RegisterExternalLoginModel { Email = objUS.SV_VCLoginID.ToString(), Mobile = "", ProviderId = objUS.SV_VCProviderId.ToString(), Provider = "twitter", rurl = "mybusiness", UserName = objUS.SV_VCUserName.ToString() });
                            }
                            else if (objUS.SV_VCLoginType.Contains("Google"))
                            {

                                Session["rturl"] = "mybusiness";
                                Dictionary<string, string> userData = new Dictionary<string, string>();
                                userData["email"] = objUS.SV_VCLoginID.ToString();
                                userData["username"] = objUS.SV_VCUserName.ToString();
                                userData["id"] = objUS.SV_VCProviderId.ToString();
                                userData["rurl"] = "mybusiness";
                                return ac.RegisterGoogleUser(userData);
                            }

                        }



                    }

                    else
                    {
                        ViewBag.alertmsg = "You Have Sucessfully Claimed the Business";
                        //return Redirect("/mybusiness");
                        if (objUS.SV_VCLoginType.ToString() == "Web")
                        {
                            LoginModel model = new LoginModel();
                            if (Session["model"] != null)
                            {
                                model = (LoginModel)Session["model"];
                            }
                            else
                            {
                                model.IsDuplicate = "0";
                                Session["model"] = model;
                            }
                            //model.Password = Request.Cookies["VCPassword"].Value.ToString();
                            model.Login = objUS.SV_VCLoginID.ToString();

                            using (var db = new VconnectDBContext29())
                            {
                                var cmd = db.Database.Connection.CreateCommand();
                                cmd.CommandText = "select top 1 password   from usercredential where customerid =" + uid;
                                cmd.CommandType = CommandType.Text;

                                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                                connection.Open();
                                var reader = cmd.ExecuteReader();
                                reader.Read();
                                model.Password = reader["password"].ToString();


                            }
                            Session["model"] = model;
                            Session["rturl"] = "mybusiness";
                           // return Redirect("/Account/WebLogin");
                            objUS.SV_VCUserType = "59";
                            return Redirect("/mybusiness");
                        }
                        else
                        {
                            AccountController ac = new AccountController();
                            if (objUS.SV_VCLoginType.ToString().Contains("FB"))
                            {
                                Session["rturl"] = "mybusiness";
                                return ac.RegisterFBUser(new RegisterExternalLoginModel { Email = objUS.SV_VCLoginID.ToString(), Mobile = "", ProviderId = objUS.SV_VCProviderId.ToString(), Provider = "Facebook", rurl = "mybusiness" });

                            }
                            else if (objUS.SV_VCLoginType.ToString() .Contains("twitter"))
                            {

                                Session["rturl"] = "mybusiness";
                                return ac.RegisterTwitterUser(new RegisterExternalLoginModel { Email = objUS.SV_VCLoginID.ToString(), Mobile = "", ProviderId = objUS.SV_VCProviderId.ToString(), Provider = "twitter", rurl = "mybusiness", UserName = objUS.SV_VCUserName.ToString() });
                            }
                            else if (objUS.SV_VCLoginType.Contains("Google"))
                            {

                                Session["rturl"] = "mybusiness";
                                Dictionary<string, string> userData = new Dictionary<string, string>();
                                userData["email"] = objUS.SV_VCLoginID.ToString();
                                userData["username"] = objUS.SV_VCUserName.ToString();
                                userData["id"] = objUS.SV_VCProviderId.ToString();
                                userData["rurl"] = "mybusiness";
                                return ac.RegisterGoogleUser(userData);
                            }

                        }


                        /////
                        //  return View("CYBThankyou", CYBModel);
                    }
                }
                else
                {
                    ViewData["alertmsg"] = result;
                    Session["CYBError"] = result;
                   
                        return View("CYBVerifyCodeWeb", CYBModel);
                   

                    //litMessage.Text = "<p class='errormsg'>Mobile not verified.</p>";
                }
                return View("CYBThankyou", CYBModel);
            }
            catch (Exception ex)
            {
                log.LogMe(ex);

                return Redirect("/");
            }
            finally
            {

            }
        }

        //
        [HttpPost]
        public ActionResult SendCYBCode(ClaimBusinessModel CYBModel, int? bid, FormCollection frm)
        {
            try
            {
                Session["CYBError"] = string.Empty;
                string message = "";
                //if (Request.Cookies["VCLoginID"] == null || Request.Cookies["VCLoginID"].ToString() == "" || Request.Cookies["VCLoginID"].ToString() == "0")
                if (string.IsNullOrEmpty(objUS.SV_VCLoginID) || objUS.SV_VCLoginID.ToString() == "" || objUS.SV_VCLoginID.ToString() == "0")
                {

                    return Redirect("/");


                }
                if (Session["bid"] == null)
                {
                    Session["bid"] = bid.ToString();
                }
                else
                {
                    bid = Convert.ToInt32(Session["bid"]);
                }
                getBusinessAddress(bid.ToString(), CYBModel);
                if (CYBModel.ReportIncorrectComments != null)
                {
                    Session["comments"] = CYBModel.ReportIncorrectComments;
                }
                else
                {
                    //Session["comments"] = string.Empty;
                    Session["CYBError"] = "Enter Comments";

                    return View("CYBReportIncorrectCommentsWeb", CYBModel);

                }
                if (CYBModel.SearchBusinessPhone == null)
                {
                    Session["CYBError"] = "Enter Mobile No.";

                    return View("CYBReportIncorrectCommentsWeb", CYBModel);

                }
                //CYBModel.SearchBusinessPhone = frm["SelectedPhone"];
                CYBModel.bid = bid.ToString();
                CYBModel.claimType = "R";
                Session["phone"] = CYBModel.SearchBusinessPhone.ToString();
                Session["Claimtype"] = "R";

                string userid = objUS.SV_VCUserContentID;

                string Bid = Session["bid"].ToString();
                int isvalid = Vconnect.Common.Utility.isValidPhone(CYBModel.SearchBusinessPhone.ToString());
                if (isvalid == 1)
                {
                    string mobileCode = Utility.GetAlphaNumericRandomNumber().ToString().Trim();
                    if (Session["Claimtype"] == "R")
                    {
                        message = "Your verification code is " + mobileCode + " to verify you mobile.";
                    }
                    else
                    {
                        message = "Your verification code is " + mobileCode + " to claim your business.";
                    }

                    //Code for sending SMS to be added here 
                    // string response = "Claim";
                    SendSMS objSendSMS = new SendSMS();
                    string SMSResponse = objSendSMS.Sendmessage("Dear Customer, Your OTP for verifying mobile number is: " + mobileCode + ".", Session["phone"].ToString().Trim());
                   // SendEmail(Request.Cookies["VCLoginID"].ToString(), message, "User-Mailer.html", 1);
                    //string SMSResponse = objSendSMS.Sendmessage("Dear Customer, Your OTP for verifying mobile number is: " +sRandomCode + ".", homeWebModel.Phone.ToString().Trim());
                    // string SMSResponse = string.Empty;

                    //Maintain the SMS EMAIL LOG
                    // Utility.SaveSMSEmailLog(Convert.ToInt32(userid), Usertype.OtherUser.GetHashCode(), "", Session["phone"].ToString(), "UserName", MessageType.SMS_UserReg.GetHashCode(), "Claim Business verification code", "Dear Customer, Your OTP for verifying mobile number is:" + mobileCode.ToString().Trim() + ".", 0, "", "", SMSResponse, Utility.GetIpAddress(), 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), Utility.GetReversedns(), (MyGlobalVariables.IsWapWeb);
                    Utility.SaveSMSEmailLog(Convert.ToInt32(userid), Usertype.OtherUser.GetHashCode(), "", Session["phone"].ToString(), "UserName", MessageType.SMS_UserReg.GetHashCode(), "Claim Business verification code", "Dear Customer, Your OTP for verifying mobile number is:" + mobileCode.ToString().Trim() + ".", 0, "", "", SMSResponse, Utility.GetIpAddress(), 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), "", "Web");
                    ///
                    string result = AddClaimBiz(mobileCode, Bid, "AddToBizMaster", userid,"Web");
                    if (result == "Success")
                    {

                        return View("CYBVerifyCodeWeb", CYBModel);

                    }
                    else
                    {
                        Session["CYBError"] = "Some Errror Occured please try again";

                        return View("CYBReportIncorrectCommentsWeb", CYBModel);

                    }
                }
                else
                {
                    Session["CYBError"] = "Invalid Mobile No.";
                    ViewBag.alertmessage = "Invalid Phone";

                    return View("CYBReportIncorrectCommentsWeb", CYBModel);

                }

            }
            catch (Exception ex)
            {
                log.LogMe(ex);

                return Redirect("/");
            }
            finally
            {

            }
        }
       [HttpPost]
        public ActionResult SendCYBCodeNew(ClaimBusinessModel CYBModel, int? bid, FormCollection frm)
        {
            try
            {
                if (bid != null)
                {
                    Session["bid"] = bid;
                }
                else
                {
                   
                        return Redirect("/");
                    
                }

                //if (Request.Cookies["VCLoginID"] == null || Request.Cookies["VCLoginID"].ToString() == "" || Request.Cookies["VCLoginID"].ToString() == "0")
                if (string.IsNullOrEmpty(objUS.SV_VCLoginID) || objUS.SV_VCLoginID.ToString() == "" || objUS.SV_VCLoginID.ToString() == "0")
                {
                   
                        return Redirect("/");
                    
                    
                }

                string d = frm["SelectedPhone"];
                if (d == null || d.Length <= 0)
                {
                    ViewBag.alertmessage = "Invalid Phone";
                    Session["CYBError"] = "Invalid Phone";
                   
                        return RedirectToAction("SearchCYBDetailsWeb");
                   

                }


                string userid = objUS.SV_VCUserContentID;
                Session["Userid"] = objUS.SV_VCUserContentID;

                string typ = "AddToBizMaster";
                string Bid = Session["bid"].ToString();
                Session["bid"] = Bid;
                CYBModel.SearchBusinessPhone = d;
                CYBModel.bid = Bid;
                CYBModel.claimType = frm["ClaimType"];
                getBusinessAddress(Bid.ToString(), CYBModel);
                Session["Claimtype"] = frm["ClaimType"];
                Session["phone"] = CYBModel.SearchBusinessPhone.ToString();
                if (frm["ClaimType"] == "N")
                {
                    int isvalid = Vconnect.Common.Utility.isValidPhone(CYBModel.SearchBusinessPhone.ToString());
                    if (isvalid == 1)
                    {
                        string mobileCode = Utility.GetAlphaNumericRandomNumber().ToString().Trim();
                        string message = "Your verification code is " + mobileCode + " to claim your business.";
                        //Code for sending SMS to be added here 
                        //string response = "Claim";
                        SendSMS objSendSMS = new SendSMS();
                       string SMSResponse = objSendSMS.Sendmessage("Dear Customer, Your OTP for verifying mobile number is: " + mobileCode + ".", Session["phone"].ToString().Trim());
                       // string SMSResponse = "";
                        Utility.SaveSMSEmailLog(Convert.ToInt32(userid), Usertype.OtherUser.GetHashCode(), "", Session["phone"].ToString(), "UserName", MessageType.SMS_UserReg.GetHashCode(), "Claim Business verification code", "Dear Customer, Your OTP for verifying mobile number is:" + mobileCode.ToString().Trim() + ".", 0, "", "", SMSResponse, Utility.GetIpAddress(), 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), "", "Web");
                        ///
                        string result = AddClaimBiz(mobileCode, Bid, typ, userid, "WEB");

                        if (result == "Success")
                        {
                            Session.Remove("CYBError");

                           
                                return View("CYBVerifyCodeWeb", CYBModel);
                           
                        }
                        else
                        {
                            ViewBag.alertmessage = "Invalid Phone";
                            Session["CYBError"] = "Invalid Phone";
                            
                                return RedirectToAction("SearchCYBDetailsWeb");
                            
                        }
                    }
                    else
                    {

                        ViewBag.alertmessage = "Invalid Phone";
                        Session["CYBError"] = "Invalid Phone";
                        
                            return RedirectToAction("SearchCYBDetails");
                       
                    }
                }
                else
                {
                    string mobileCode = Utility.GetAlphaNumericRandomNumber().ToString().Trim();
                    if (frm["ClaimType"] == "R")
                    {
                        string message = "Your verification code is " + mobileCode + " to verify you mobile.";
                    }
                    else
                    {
                        string message = "Your verification code is " + mobileCode + " to claim your business.";
                    }
                    //Code for sending SMS to be added here 
                    // string response = "Claim";
                    ///
                    string result = AddClaimBiz(mobileCode, Bid, typ, userid, "Web");

                    if (result == "Success")
                    {
                        Session.Remove("CYBError");
                        // string SMSResponse = string.Empty;
                        //Code for sending SMS to be added here  
             
                        SendSMS objSendSMS = new SendSMS();
                        string SMSResponse = objSendSMS.Sendmessage("Dear Customer, Your OTP for verifying mobile number is: " + mobileCode + ".", Session["phone"].ToString().Trim());

                       // string SMSResponse = "";


                        //Maintain the SMS EMAIL LOG
                       
                        Utility.SaveSMSEmailLog(Convert.ToInt32(userid), Usertype.OtherUser.GetHashCode(), "", Session["phone"].ToString(), "UserName", MessageType.SMS_UserReg.GetHashCode(), "Claim Business verification code", "Dear Customer, Your OTP for verifying mobile number is:" + mobileCode.ToString().Trim() + ".", 0, "", "", SMSResponse, Utility.GetIpAddress(), 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), "", "Web");
                       // SendEmail(Request.Cookies["VCLoginID"].ToString(), "Your verification code is " + mobileCode + " to claim your business.", "User-Mailer.html", 1);
                       
                            return View("CYBVerifyCodeWeb", CYBModel);
                        
                    }
                    else
                    {
                        ViewBag.alertmessage = "Invalid Phone";
                        Session["CYBError"] = "Invalid Phone";
                        
                            return RedirectToAction("SearchCYBDetailsWeb");
                        
                    }
                }
            }
            catch (Exception ex)
            {
                log.LogMe(ex);

                return Redirect("/");
            }
            finally
            {

            }
        }


        
        public ActionResult ResendCYBCode(ClaimBusinessModel CYBModel, int? bid)
        {
            try
            {
                //if (Request.Cookies["VCLoginID"] == null || Request.Cookies["VCLoginID"].ToString() == "" || Request.Cookies["VCLoginID"].ToString() == "0")
                if (string.IsNullOrEmpty(objUS.SV_VCLoginID) || objUS.SV_VCLoginID.ToString() == "" || objUS.SV_VCLoginID.ToString() == "0")
                {
                   
                        return Redirect("/");
                    
                }
                string userid = objUS.SV_VCUserContentID;
                Session["Userid"] = objUS.SV_VCUserContentID;
                if (Session["bid"] == null)
                {
                    return Redirect("/");
                }
                
                getBusinessAddress(Session["bid"].ToString(), CYBModel);
                CYBModel.SearchBusinessPhone = Session["phone"].ToString();
                string typ = "AddToBizMaster";
                string mobileCode = Utility.GetAlphaNumericRandomNumber().ToString().Trim();
                string message = "Your verification code is " + mobileCode + " to claim your business.";
                //Code for sending SMS to be added here 
                //SendSMS objSendSMS = new SendSMS();
                // string SMSResponse = objSendSMS.Sendmessage("Dear Customer, Your OTP for verifying mobile number is: " + mobileCode + ".", Session["phone"].ToString().Trim());
                //string response = "Claim";
                ///
                string result = AddClaimBiz(mobileCode, Session["bid"].ToString(), typ, userid, "Web");
                if (result == "Success")
                {
                    ViewData["alertmsg"] = "New Verification code has been sent to your mobile";
                    Session["CYBError"] = "New Verification code has been sent to your mobile";

                    //Maintain the SMS EMAIL LOG
                    //Code for sending SMS to be added here               
                    SendSMS objSendSMS = new SendSMS();
                    string SMSResponse = objSendSMS.Sendmessage("Dear Customer, Your OTP for verifying mobile number is: " + mobileCode + ".", Session["phone"].ToString().Trim());
                    Utility.SaveSMSEmailLog(Convert.ToInt32(userid), Usertype.OtherUser.GetHashCode(), "", Session["phone"].ToString(), "UserName", MessageType.SMS_UserReg.GetHashCode(), "Claim Business verification code", "Dear Customer, Your OTP for verifying mobile number is:" + mobileCode.ToString().Trim() + ".", 0, "", "", SMSResponse, Utility.GetIpAddress(), 0, 0, Request.Url.AbsoluteUri.ToString().Trim(), ActionType.SMS.GetHashCode(), "", "Web");
                  //  SendEmail(Request.Cookies["VCLoginID"].ToString(), "Your new verification code is " + mobileCode + " to verify your mobile.", "User-Mailer.html", 1);
                    

                        return View("CYBVerifyCodeWeb", CYBModel);
                    
                }
                else
                {
                    Session["CYBError"] = "Some Error has occured";
                    
                  return View("SearchCYBDetailsWeb", CYBModel);
                    
                }

            }
            catch (Exception ex)
            {
                log.LogMe(ex);

                return Redirect("/");
            }
            finally
            {

            }
        }
        // GET: /ClaimBusiness/Create
        static public string Addclaim(string response, string email, string mobileNumber, string keyword, string location, string message)
        {

            SqlConnection cn = new SqlConnection();
            try
            {

                using (var db = new VconnectDBContext29())
                {

                    // Create a SQL command to execute the sproc
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[prc_wap_isclaimveriffied_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object               

                    cmd.Parameters.Add(new SqlParameter("@Mobile", mobileNumber));
                    cmd.Parameters.Add(new SqlParameter("@email", email));
                    cmd.Parameters.Add(new SqlParameter("@Message", message));
                    cmd.Parameters.Add(new SqlParameter("@response", response));
                    cmd.Parameters.Add(new SqlParameter("@subject", keyword));
                    cmd.Parameters["@stts"].Direction = ParameterDirection.Output;
                    SqlParameter parameter1 = new SqlParameter("@stts", SqlDbType.Int);
                    parameter1.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(parameter1);

                    try
                    {


                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        cmd.ExecuteNonQuery();
                        if (cmd.Parameters["@stts"].Value.ToString() == "12345")
                        {
                            return "Success";

                        }
                        else if (cmd.Parameters["@stts"].Value.ToString() == "0")
                        {
                            return "Mis-Match";
                        }
                        else
                        {
                            return "Fail";
                        }


                    }
                    finally
                    {
                        db.Database.Connection.Close();
                    }
                }

            }
            finally
            {

                cn.Close();
            }



        }

        static public string AddClaimBiz(string Vcode, string BID, string Typ, string Uid, string source)
        {

            SqlConnection cn = new SqlConnection();
            try
            {

                using (var db = new VconnectDBContext29())
                {

                    // Create a SQL command to execute the sproc
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[prc_wap_add_ClaimBiz_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object               

                    cmd.Parameters.Add(new SqlParameter("@VarificationCode", Vcode));
                    cmd.Parameters.Add(new SqlParameter("@BusinessID", BID));
                    cmd.Parameters.Add(new SqlParameter("@Type", Typ));
                    cmd.Parameters.Add(new SqlParameter("@userid", Uid));
                    cmd.Parameters.Add(new SqlParameter("@claimsource", source));
                    SqlParameter parameter1 = new SqlParameter("@Error", SqlDbType.Int);
                    parameter1.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(parameter1);

                    try
                    {


                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        cmd.ExecuteNonQuery();
                        if (cmd.Parameters["@Error"].Value.ToString() == "12345")
                        {
                            return "Success";

                        }
                        else if (cmd.Parameters["@Error"].Value.ToString() == "0")
                        {
                            return "Mis-Match";
                        }
                        else
                        {
                            return "Fail";
                        }


                    }
                    finally
                    {
                        db.Database.Connection.Close();
                    }
                }

            }
            finally
            {

                cn.Close();
            }



        }
        public static string claimyoubusinessupdate(string bid, string uid, string remark, string mobile, string vcode, string typ, string source)
        {

            SqlConnection cn = new SqlConnection();
            try
            {

                using (var db = new VconnectDBContext29())
                {

                    // Create a SQL command to execute the sproc
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[prc_wap_isclaimveriffied_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object               

                    cmd.Parameters.Add(new SqlParameter("@bid", bid));
                    cmd.Parameters.Add(new SqlParameter("@userid", uid));
                    cmd.Parameters.Add(new SqlParameter("@mobile", mobile));
                    cmd.Parameters.Add(new SqlParameter("@typ", typ));
                    cmd.Parameters.Add(new SqlParameter("@remark", remark));
                    cmd.Parameters.Add(new SqlParameter("@verificationcode", vcode));
                    cmd.Parameters.Add(new SqlParameter("@source", @source));
                    SqlParameter parameter1 = new SqlParameter("@stts", SqlDbType.Int);
                    parameter1.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(parameter1);

                    try
                    {


                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        cmd.ExecuteNonQuery();
                        if (cmd.Parameters["@stts"].Value.ToString() == "1")
                        {
                            return "Success";

                        }
                        else if (cmd.Parameters["@stts"].Value.ToString() == "0")
                        {
                            return "Mis-Match";
                        }
                        else
                        {
                            return "Fail";
                        }


                    }
                    finally
                    {
                        db.Database.Connection.Close();
                    }
                }

            }
            finally
            {

                cn.Close();
            }
        }

        public static string claimybusinessupdate(string bid, string uid, string mobile, string vcode, string typ, string source)
        {

            SqlConnection cn = new SqlConnection();
            try
            {

                using (var db = new VconnectDBContext29())
                {

                    // Create a SQL command to execute the sproc
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "[dbo].[prc_wap_isclaimveriffied_opt]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //Add parameters to command object               

                    cmd.Parameters.Add(new SqlParameter("@bid", bid));
                    cmd.Parameters.Add(new SqlParameter("@userid", uid));
                    cmd.Parameters.Add(new SqlParameter("@mobile", mobile));
                    cmd.Parameters.Add(new SqlParameter("@typ", typ));
                    cmd.Parameters.Add(new SqlParameter("@verificationcode", vcode));
                    cmd.Parameters.Add(new SqlParameter("@source", @source));

                    SqlParameter parameter1 = new SqlParameter("@stts", SqlDbType.Int);
                    parameter1.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(parameter1);

                    try
                    {


                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        cmd.ExecuteNonQuery();
                        if (cmd.Parameters["@stts"].Value.ToString() == "1")
                        {
                            return "Success";

                        }
                        else if (cmd.Parameters["@stts"].Value.ToString() == "0")
                        {
                            return "Mis-Match";
                        }
                        else
                        {
                            return "Fail";
                        }


                    }
                    finally
                    {
                        db.Database.Connection.Close();
                    }
                }

            }
            finally
            {

                cn.Close();
            }
        }
        public void claimusersmobileupdate(string mobile)
        {
            string userid = objUS.SV_VCUserContentID;
            SqlConnection cn = new SqlConnection();
            try
            {

                using (var db = new VconnectDBContext29())
                {

                    // Create a SQL command to execute the sproc
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "prc_wap_isclaimmobbileveriffied";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@uid", userid));
                    cmd.Parameters.Add(new SqlParameter("@mobile", mobile));


                    try
                    {


                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        cmd.ExecuteNonQuery();



                    }
                    finally
                    {
                        db.Database.Connection.Close();
                    }
                }

            }
            finally
            {

                cn.Close();
            }


        }

        public ActionResult KeywordAutoComplete(string term)
        {
            var data = new List<SearchKeywordsBname>();
            List<SearchKeywordsBname> keywordCollection = new List<SearchKeywordsBname>();
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "select distinct top 10 contentid,businessname from businessmaster where businessname like '" + term + "%'";
                cmd.CommandType = CommandType.Text;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    keywordCollection = ((IObjectContextAdapter)db).ObjectContext.Translate<SearchKeywordsBname>(reader).ToList();
                    data = ((IObjectContextAdapter)db).ObjectContext.Translate<SearchKeywordsBname>(reader).ToList();
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            var dataJson = keywordCollection.Select(m => new { label = m.businessname, value = string.Concat(m.businessname.ToString()) });
            return Json(dataJson, JsonRequestBehavior.AllowGet);
        }
        public bool getmobileVerication(string phone, string userid)
        {
            using (var db = new VconnectDBContext29())
            {

                // Create a SQL command to execute the sproc

                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "select top 1 isnull(ismobileverified,0) as ismobileverified   from userdetails where contentid=" + userid;
                cmd.CommandType = CommandType.Text;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    reader.Read();
                    if (reader.HasRows)
                    {
                        if (reader["ismobileverified"].ToString().ToLower()=="false")
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }

                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
        }
        //public ActionResult URegistration(UserRegWEBModels homeWapModel, string CaptchaText)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        int q = Vconnect.Common.Utility.isValidPhone(homeWapModel.txtMobile.ToString());
        //        if (q != 0 || (CaptchaText != HttpContext.Session["captchastring"].ToString() || (homeWapModel.ConditionaValue == false)))
        //        {
        //            if (CaptchaText != HttpContext.Session["captchastring"].ToString())
        //            {
        //                homeWapModel.CAPTCHA = "CAPTCHA verification failed!";
        //                ModelState.AddModelError("homeWapModel.CAPTCHA", homeWapModel.CAPTCHA);
        //            }
        //            if (homeWapModel.ConditionaValue == false)
        //            {
        //                homeWapModel.checkbox = "Please select CheckBox";
        //                ModelState.AddModelError("homeWapModel.checkbox", homeWapModel.checkbox);
        //            }
        //            if (q != 0)
        //            {
        //                homeWapModel.PhoneMsg = "Invalid Phone Number";
        //                ModelState.AddModelError("homeWapModel.PhoneMsg", homeWapModel.PhoneMsg);
        //            }
        //        }
        //        else
        //        {
        //            using (var db = new VconnectDBContext29())
        //            {
        //                string sdr = ((System.Guid.NewGuid().ToString().GetHashCode().ToString("x"))).ToString();
        //                var cmd = db.Database.Connection.CreateCommand();
        //                cmd.CommandText = "[dbo].[prc_add_usercredentialsnew]";
        //                cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //                //Add parameters to command object
        //                cmd.Parameters.Add(new SqlParameter("@password", homeWapModel.txtPassword.ToString().Trim()));
        //                cmd.Parameters.Add(new SqlParameter("@usertype", 60));
        //                cmd.Parameters.Add(new SqlParameter("@senderemailid", homeWapModel.txtEmail.ToString().Trim()));
        //                cmd.Parameters.Add(new SqlParameter("@sendercontactnumber", homeWapModel.txtMobile.ToString().Trim()));
        //                cmd.Parameters.Add(new SqlParameter("@sendername", homeWapModel.txtName.ToString()));
        //                cmd.Parameters.Add(new SqlParameter("@VerificationCode", sdr.ToString().Trim()));
        //                cmd.Parameters.Add(new SqlParameter("@encriptpassword", (encryptPassword(homeWapModel.txtPassword.ToString().Trim())).ToString()));

        //                try
        //                {
        //                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
        //                    connection.Open();
        //                    cmd.ExecuteNonQuery();
        //                    //return Content("<script language='javascript' type='text/javascript'>alert('Save Successfully');</script>");
        //                    ViewBag.Message = "Save Successfully for User Registration.";
        //                    TempData["SuccessMessage"] = "Save Successfully for User Registration.";
        //                    return View("CYBLogin");
        //                    //var Error = (cmd.ExecuteScalar());
        //                    //int Err = (int)Error;
        //                    //if (Err == null)
        //                    //{
        //                    //    ViewBag.Message = "Form submitted successfully...";
        //                    //    return View();
        //                    //}
        //                    //else
        //                    //{
        //                    //    if (Err < 0)
        //                    //    {
        //                    //        ViewBag.Message = "An error occurred. Please try again.";
        //                    //    }
        //                    //    else if (Err == 1)
        //                    //    {
        //                    //        ViewBag.Message = "Login id already exists. Try with different ID.";
        //                    //        homeWapModel = null;
        //                    //    }
        //                    //    //Email already exits  and is verified
        //                    //    else if (Err == 3)
        //                    //    {

        //                    //        // ViewBag.Message = "Login ID already in use. Forgot your password? <a target=\"_blank\" href=\"" + webconfigWebsiteRootPath + "forgotpassword/" + homeWapModel.txtEmail.ToString().Trim() + "\" >Click here</a> to retrieve your credentials.";
        //                    //        //homeWapModel = null;
        //                    //    }
        //                    //    //Email already exits  and NOT Verified
        //                    //    else if (Err == 4)
        //                    //    {
        //                    //        ViewBag.Message = "Login ID already registered but email not verified. Please verify email by clicking on the link already been sent to (" + homeWapModel.txtEmail.ToString().Trim() + ").";

        //                    //        //SendNewRegistrationEmail(homeWapModel.txtName.ToString().Trim(), homeWapModel.txtEmail.ToString().Trim(), homeWapModel.txtEmail.ToString().Trim(), homeWapModel.txtPassword.ToString().Trim(), ((System.Guid.NewGuid().ToString().GetHashCode().ToString("x"))).ToString().Trim());
        //                    //        //homeWapModel = null;

        //                    //    }
        //                    //    else if (Err == 5)
        //                    //    {
        //                    //    }
        //                    //    else
        //                    //    {
        //                    //    }
        //                    //}
        //                }
        //                catch (System.Exception e)
        //                {
        //                }
        //                finally
        //                {
        //                    db.Database.Connection.Close();
        //                    homeWapModel.PhoneMsg = "";
        //                }
        //            }
        //        }
        //    }

        //    return View("CYBLogin");
        //}

        public static string encryptPassword(string password)
        {

            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(password.Trim()));
            StringBuilder strEncrptedPassword = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                strEncrptedPassword.Append(data[i].ToString("x2"));
            }
            return strEncrptedPassword.ToString();
        }

        public void getBusinessAddress(string businessid,ClaimBusinessModel cyb )
        {
            using (var db = new VconnectDBContext29())
            {

                // Create a SQL command to execute the sproc
                
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "select businessname,REPLACE( address1 + ', ' + city   + ', ' +  state,', ,',',') as address1,companylogo,isnull(isclaim,0) as isclaim,businessurl from  businessmaster where contentid=" + businessid;
                cmd.CommandType = CommandType.Text;
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();                    
                    reader.Read();
                    cyb.SearchBuisnesname = reader["businessname"].ToString();
                    cyb.SearchBuisnessaddress= reader["address1"].ToString();
                    if (reader["companylogo"].ToString().Length <=0)
                    {
                        cyb.SearchBuisnesslogo = "/img/default-biz-logo.jpg";
                    }
                    else
                    {
                        cyb.SearchBuisnesslogo = "http://image.vconnect.bz/vcsites/vcimages/resource/" + reader["companylogo"];
                    }
                    cyb.isclaim = Convert.ToInt16(reader["isclaim"]);
                    cyb.burl = reader["businessurl"].ToString();
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
        }

        protected void SendEmail(string screenName, string msg, string template, int flagEmail)
        {
            string Message = "";
            System.Net.Mail.SmtpClient SMPT = new System.Net.Mail.SmtpClient();
            System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage();
            System.IO.FileStream FsContent = new System.IO.FileStream(Server.MapPath("~/resource/email_templates/" + template), System.IO.FileMode.Open, System.IO.FileAccess.Read);
            System.IO.StreamReader SRcontent = new System.IO.StreamReader(FsContent);
            Message = SRcontent.ReadToEnd();
            FsContent.Close();
            SRcontent.Close();
            FsContent.Dispose();
            SRcontent.Dispose();
            screenName = objUS.SV_VCUserName;
            if (flagEmail == 1)
            {
                Message = Message.Replace("##ScreenName##", screenName);
                Message = Message.Replace("##Message##", msg);
                Email.Subject = "OTP Code for Mobile Verification.";
            }
            else if (flagEmail == 2)
            {

                Message = Message.Replace("{2}", screenName);
                Message = Message.Replace("{0}", msg);
                Email.Subject = "New Password link for your VConnect account.";
            }


            Email.From = new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["networkUserId"].ToString(), "VConnect.com");
            Email.IsBodyHtml = true;
            Email.Body = Message;
            //if (Request.Cookies["VCUserEmail"].Value != null)
            if (!string.IsNullOrEmpty(objUS.SV_VCUserEmail))
            {
                Email.To.Add(objUS.SV_VCUserEmail);
            }
            else
            {
                Email.To.Add("kelechi@vconnect.com");
            }
            Email.Bcc.Add("ashishagarwalin@gmail.com");
            
            SendEmail send = new SendEmail();
            try
            {
                send.SendMailToUser(ref Email, ConfigurationManager.AppSettings["networkUserId"].ToString(), ConfigurationManager.AppSettings["networkUserIdpassword"].ToString());
            }
            catch (Exception e)
            { }
        }

    }
}
