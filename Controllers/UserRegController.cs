﻿using MailChimp.Types;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Vconnect.Common;
using Vconnect.Enums;
using Vconnect.Models;

namespace Vconnect.Controllers
{
    public class UserRegController : Controller
    {
        UserSession objUS = new UserSession();
        #region userregweb
        public ActionResult UserReg()
        {
            //if (System.Web.HttpContext.Current.Request.Cookies["VCLoginID"] != null && System.Web.HttpContext.Current.Request.Cookies["VCLoginID"].ToString().Trim() != "")
            if (!string.IsNullOrEmpty(objUS.SV_VCLoginID) && objUS.SV_VCLoginID.ToString().Trim() != "")
            {
                if (Request.RawUrl.Contains("$description="))
                {
                    string url = Request.RawUrl.Replace('$', '&');
                    return RedirectPermanent(url);
                }

                if (Request.Url.AbsoluteUri.ToString().IndexOf("rturl") != -1 || Request.QueryString["rturl"] != null)
                {
                    return RedirectPermanent(Request.QueryString["rturl"].ToString().ToLower());
                }
                return RedirectPermanent("/users/socialactivity");
            }

                UserReg.UserRegWeb homeWapModel = new UserReg.UserRegWeb();
                //List<SelectListItem> listStatesNames = new List<SelectListItem>();
                //homeWapModel.StateOptions = Utility.GetSelectList();

                //List<SelectListItem> listCiyNames = new List<SelectListItem>();
                //homeWapModel.CityOptions = Utility.GetSelectListCity(0);

                homeWapModel.SuccessMessage = TempData["SuccessMessage"] as string;
                homeWapModel.MessageError = TempData["MessageError"] as string;
                homeWapModel.ConditionaValue = true;
                return View(homeWapModel);
        }        
        [HttpPost]
        public ActionResult UserReg(UserReg.UserRegWeb homeWapModel,FormCollection frm)
        {

                string hidevalue = frm["hidvaluetext"];
                if (string.IsNullOrEmpty(hidevalue))
                {
                    SendSMS send = new SendSMS();
                    //List<SelectListItem> listStatesNames = new List<SelectListItem>();
                    //homeWapModel.StateOptions = Utility.GetSelectList();
                    //List<SelectListItem> listCiyNames = new List<SelectListItem>();
                    //homeWapModel.CityOptions = Utility.GetSelectListCity(homeWapModel.SelectedStateId);

                    string webconfigWebsiteRootPath = string.Empty;

                    List<UserReg.UserCredentialsNew> listUserCredentialsNew = new List<UserReg.UserCredentialsNew>();
                    bool a = true;
                    if (!string.IsNullOrEmpty(homeWapModel.txtEmail))
                    {
                        Regex regEmail = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
                        a = regEmail.IsMatch(homeWapModel.txtEmail);
                        if (Regex.Matches(homeWapModel.txtEmail, "@").Count > 1 || (homeWapModel.txtEmail.IndexOfAny(new char[] { '/', '%', '!', '#', '$', '^', '&', '*', '(', ')', '+', '=', '|', '{', '}', '[', ']', ':', ';', ',', '>', '<', '?', '~', '`', '"', '-' }) > 0) || Regex.Matches(homeWapModel.txtEmail, "'").Count > 0)
                            a = false;
                    }

                    if (homeWapModel.txtName == null)
                    {
                        ViewBag.Msg = "Enter your first name";
                    }
                    else if (homeWapModel.surname == null)
                    {
                        ViewBag.Msg = "Enter your surname";
                    }
                    else if (homeWapModel.txtName.Trim() == homeWapModel.surname.Trim())
                    {
                        ViewBag.Msg = "First Name and Surname should be different.";
                    }
                    else if (string.IsNullOrEmpty(homeWapModel.txtEmail))
                    {
                        ViewBag.Msg = "Enter a valid email address";
                    }
                    else if (a == false)
                    {
                        ViewBag.Msg = "Invalid Email-Id";
                    }
                    else if (string.IsNullOrEmpty(homeWapModel.txtPassword))
                    {
                        ViewBag.Msg = "Enter your password";
                    }
                    else if (homeWapModel.txtPassword.Length < 6)
                    {
                        ViewBag.Msg = "Enter a valid password (at least 6 characters)";
                    }
                    else if (homeWapModel.txtPassword.Length != homeWapModel.txtConfirmPassword.Length)
                    {
                        ViewBag.Msg = "Password should be same";
                    }
                    else if (homeWapModel.txtConfirmPassword.Length < 6)
                    {
                        ViewBag.Msg = "Enter a valid comfirm password (at least 6 characters)";
                    }
                    else if (homeWapModel.ConditionaValue == false)
                        ViewBag.Msg = "Please accept the terms and conditions.";
                    else
                    {
                        //if (homeWapModel.txtMobile != null)
                        //{
                        //    int q = Vconnect.Common.Utility.isValidPhone(homeWapModel.txtMobile.ToString());
                        //    string sContactNumber = homeWapModel.txtMobile.ToString().Trim();
                        //    string subSection = sContactNumber.Substring(0, 1);
                        //    if (subSection != "0")
                        //        ViewBag.Msg = "Please start phone number with zero.";
                        //    else if (q == 0)
                        //        ViewBag.Msg = "Number not valid! please enter valid number.";
                        //}
                        //else
                        //{
                        //    homeWapModel.txtMobile = "";
                        //}
                        using (var db = new VconnectDBContext29())
                        {
                            //webconfigWebsiteRootPath = System.Web.HttpContext.Current.Items["webconfigWebsiteRootPath"].ToString();
                            //webconfigWebsiteRootPath = ConfigurationManager.AppSettings["WebsiteRootPath"].ToString();
                            webconfigWebsiteRootPath = System.Web.HttpContext.Current.Session["webconfigWebsiteRootPath"] != null ? System.Web.HttpContext.Current.Session["webconfigWebsiteRootPath"].ToString() : ConfigurationManager.AppSettings["WebsiteRootPath"].ToString();
                            string sdr = ((System.Guid.NewGuid().ToString().GetHashCode().ToString("x"))).ToString();
                            var cmd = db.Database.Connection.CreateCommand();
                            cmd.CommandText = "[dbo].[prc_add_usercredentialsMay_opt]";
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            //Add parameters to command object
                            cmd.Parameters.Add(new SqlParameter("@password", homeWapModel.txtPassword.ToString().Trim()));
                            cmd.Parameters.Add(new SqlParameter("@usertype", 60));
                            cmd.Parameters.Add(new SqlParameter("@senderemailid", homeWapModel.txtEmail.ToString().Trim()));
                            // cmd.Parameters.Add(new SqlParameter("@sendercontactnumber", homeWapModel.txtMobile.ToString().Trim()));
                            cmd.Parameters.Add(new SqlParameter("@sendername", homeWapModel.txtName.ToString()));
                            cmd.Parameters.Add(new SqlParameter("@surname", homeWapModel.surname.ToString()));
                            cmd.Parameters.Add(new SqlParameter("@VerificationCode", sdr.ToString().Trim()));
                            cmd.Parameters.Add(new SqlParameter("@source", "WEB"));
                            cmd.Parameters.Add(new SqlParameter("@encriptpassword", (Vconnect.Common.Utility.Encrypt.encryptPassword(homeWapModel.txtPassword.ToString().Trim())).ToString()));
                            //cmd.Parameters.Add(new SqlParameter("@stateid", homeWapModel.SelectedStateId.ToString()));
                            //cmd.Parameters.Add(new SqlParameter("@cityid", homeWapModel.SelectedCityId.ToString()));
                            //cmd.Parameters.Add(new SqlParameter("@DOB", form["dob"].ToString()));
                            //cmd.Parameters.Add(new SqlParameter("@gender", form["Gender"].ToString()));
                            try
                            {
                                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                                connection.Open();
                                var reader = cmd.ExecuteReader();

                                listUserCredentialsNew = ((IObjectContextAdapter)db).ObjectContext.Translate<UserReg.UserCredentialsNew>(reader).ToList();

                                int Err, contentidnew = 0;
                                string OUTUserName = string.Empty;
                                OUTUserName = listUserCredentialsNew.First().ContactName;
                                Err = int.Parse(listUserCredentialsNew.First().Error.ToString());
                                if (Err == 0 || Err == 4)
                                {
                                    contentidnew = int.Parse(listUserCredentialsNew.First().newuserconentid.ToString());
                                }
                                if (Err < 0)
                                {
                                    TempData["MessageError"] = "An error occurred. Please try again.";
                                    return RedirectToAction("UserReg");
                                }
                                else if (Err == 1)
                                {
                                    TempData["MessageError"] = "Login id already exists. Try with different ID.";
                                    return RedirectToAction("UserReg");
                                }
                                //Email already exits  and is verified
                                else if (Err == 3)
                                {
                                    TempData["MessageError"] = "ForgetPassord";
                                    return RedirectToAction("UserReg");
                                }
                                //Email already exits  and NOT Verified
                                else if (Err == 4)
                                {
                                    TempData["MessageError"] = "Login ID already registered but email not verified. Please verify email by clicking on the link already been sent to (" + homeWapModel.txtEmail.ToString().Trim().ToLower() + ").<a style='color: greenyellow;font-size: inherit;' href=/sendverification?userid=" + contentidnew.ToString().Trim() + ">Resend</a>";
                                    //int contentid = contentidselect(homeWapModel.txtEmail.ToString().Trim(), sdr.ToString().Trim());
                                    //SendFavioriteBizCred(OUTUserName.ToString().Trim(), contentidnew, homeWapModel.txtEmail.ToString().Trim(), homeWapModel.txtPassword.ToString().Trim(), sdr.ToString().Trim(), 2, 60);
                                    return RedirectToAction("UserReg");
                                }
                                else if (Err == 5)
                                {
                                    Session["tempMobile"] = homeWapModel.txtMobile.ToString().Trim();
                                    Session["tempNewUserId"] = homeWapModel.txtEmail.ToString().Trim();
                                    int iUserName = OUTUserName.Length;
                                    //OUTUserName = "********" + OUTUserName.Substring(iUserName + 1);
                                    string OutEmailID = string.Empty;
                                    int iEmailID = homeWapModel.txtEmail.ToString().IndexOf("@");
                                    //OutEmailID = "********" + homeWapModel.txtEmail.ToString().Substring(iEmailID + 1);

                                    //ViewBag.Msg = "Contact number already in use with (" + OUTUserName.ToString().Trim() + ": " + OutEmailID.ToString().Trim() + "). Do you want to use it for this account instead? <a href=\"" + webconfigWebsiteRootPath + "thankyou/confirmmobileup\"  onclick=\"javascript:return confirm('Phone number will be moved from previous account to new.');\">Yes</a> /  <a href=\"javascript:void(0);\"  onclick=\"javascript:Hidemessage('ctl00_cphMiddleContainer_divMsg');\">No</a>";
                                    UserSession userid = new UserSession();
                                    //string IPAddress = MyGlobalVariables.GetIpAddress;

                                    Session["IPADDRESS"] = MyGlobalVariables.GetIpAddress;
                                    Session["REVERSEDNS"] = MyGlobalVariables.GetReverseDns;

                                    if (MyGlobalVariables.GetIpAddress == "0")
                                    {
                                        try
                                        {
                                            Session["IPADDRESS"] = Vconnect.MvcApplication.GetVisitorIPAddress();
                                        }
                                        catch
                                        { }
                                    }
                                    if (MyGlobalVariables.GetReverseDns == "0")
                                    {
                                        try
                                        {
                                            Session["REVERSEDNS"] = Vconnect.MvcApplication.GetReverseDns(Session["IPADDRESS"].ToString());
                                        }
                                        catch
                                        { }
                                    }
                                    if (!string.IsNullOrEmpty(homeWapModel.txtEmail))
                                    {
                                        SendFavioriteBizCred(homeWapModel.txtName.ToString().Trim(), contentidnew, homeWapModel.txtEmail.ToString().Trim(), homeWapModel.txtPassword.ToString().Trim(), sdr.ToString().Trim(), 2, 60);

                                        //Maintain the SMS EMAIL LOG
                                        Int32 bid;
                                        if (Session["BID"] != null) bid = int.Parse(Session["BID"].ToString());
                                        else bid = 0;
                                        if (userid.SV_VCUserContentID != null && userid.SV_VCUserContentID != "")
                                        {
                                            Utility.SaveSMSEmailLog(Convert.ToInt32(userid.SV_VCUserContentID), Usertype.OtherUser.GetHashCode(), homeWapModel.txtEmail.ToString().Trim(), "", homeWapModel.txtName.ToString().Trim(), MessageType.Email_Registration.GetHashCode(), "User registration", "Thanks for user registration", bid, "", "", "", MyGlobalVariables.GetIpAddress, 0, 0, MyGlobalVariables._sourceUrl, ActionType.Email.GetHashCode(), MyGlobalVariables.GetReverseDns, "WEB");
                                        }
                                        else
                                        {
                                            Utility.SaveSMSEmailLog(0, Usertype.OtherUser.GetHashCode(), homeWapModel.txtEmail.ToString().Trim(), "", homeWapModel.txtName.ToString().Trim(), MessageType.Email_Registration.GetHashCode(), "User registration", "Thanks for user registration", bid, "", "", "", MyGlobalVariables.GetIpAddress, 0, 0, MyGlobalVariables._sourceUrl, ActionType.Email.GetHashCode(), MyGlobalVariables.GetReverseDns, "WEB");
                                        }
                                    }
                                    else
                                    {
                                    }
                                }
                                else
                                {

                                    string IPAddress = Utility.GetIpAddress();

                                    Session["IPADDRESS"] = MyGlobalVariables.GetIpAddress;
                                    Session["REVERSEDNS"] = MyGlobalVariables.GetReverseDns;

                                    if (MyGlobalVariables.GetIpAddress == "0")
                                    {
                                        try
                                        {
                                            Session["IPADDRESS"] = Vconnect.MvcApplication.GetVisitorIPAddress();
                                        }
                                        catch
                                        { }
                                    }
                                    if (MyGlobalVariables.GetReverseDns == "0")
                                    {
                                        try
                                        {
                                            Session["REVERSEDNS"] = Vconnect.MvcApplication.GetReverseDns(Session["IPADDRESS"].ToString());
                                        }
                                        catch
                                        { }
                                    }
                                    if (!string.IsNullOrEmpty(homeWapModel.txtEmail))
                                    {
                                        SendFavioriteBizCred(homeWapModel.txtName.ToString().Trim(), contentidnew, homeWapModel.txtEmail.ToString().Trim(), homeWapModel.txtPassword.ToString().Trim(), sdr.ToString().Trim(), 2, 60);
                                        UserSession userid = new UserSession();
                                        //Maintain the SMS EMAIL LOG
                                        Int32 bid;
                                        if (Session["BID"] != null) bid = int.Parse(Session["BID"].ToString());
                                        else bid = 0;
                                        if (userid.SV_VCUserContentID != null && userid.SV_VCUserContentID != "")
                                        {
                                            Utility.SaveSMSEmailLog(Convert.ToInt32(userid.SV_VCUserContentID), Usertype.OtherUser.GetHashCode(), homeWapModel.txtEmail.ToString().Trim(), "", homeWapModel.txtName.ToString().Trim(), MessageType.Email_Registration.GetHashCode(), "User registration", "Thanks for user registration", bid, "", "", "", MyGlobalVariables.GetIpAddress, 0, 0, MyGlobalVariables._sourceUrl, ActionType.Email.GetHashCode(), MyGlobalVariables.GetReverseDns, "WEB");
                                        }
                                        else
                                        {
                                            Utility.SaveSMSEmailLog(0, Usertype.OtherUser.GetHashCode(), homeWapModel.txtEmail.ToString().Trim(), "", homeWapModel.txtName.ToString().Trim(), MessageType.Email_Registration.GetHashCode(), "User registration", "Thanks for user registration", bid, "", "", "", MyGlobalVariables.GetIpAddress, 0, 0, MyGlobalVariables._sourceUrl, ActionType.Email.GetHashCode(), MyGlobalVariables.GetReverseDns, "WEB");
                                        }
                                    }
                                    else
                                    {
                                    }
                                    //int isact = isactive(sdr.ToString().Trim());
                                    TempData["SuccessMessage"] = "Registration successful. Verification Link has been sent to your email id (" + homeWapModel.txtEmail.ToString().Trim() + "). Please verify it to access VConnect services.";
                                    if (Session["BID"] != null)
                                    {
                                        Int64 UserType = 60;
                                        businessupdate(Convert.ToInt64(Session["BID"].ToString()), listUserCredentialsNew.First().newuserconentid, UserType);
                                        Session.Remove("BID");
                                        Session["EID"] = homeWapModel.txtEmail.ToString().Trim();
                                        //return RedirectToAction("UserReg");
                                        //Response.Redirect(webconfigWebsiteRootPath.ToString() + "businessregistration/UR", true);
                                        return RedirectToAction("BizReg", "BizReg", new { urdiv = homeWapModel.txtEmail.ToString().Trim() });
                                    }
                                    //return RedirectToAction("ThankYou", "UserReg", new { verificationcode = sdr.ToString().Trim(), userid = contentidnew, loginid = homeWapModel.txtEmail.ToString().Trim(), password = homeWapModel.txtPassword.ToString().Trim(), name = homeWapModel.txtName.ToString().Trim(), resend ="Yes"});                               
                                    return RedirectToAction("ThankYou", "UserReg", new { userid = contentidnew, resend = sdr.ToString().Trim() });
                                }
                                connection.Close();
                                connection.Dispose();
                            }
                            catch (Exception ex)
                            {
                                log.LogMe(ex);
                            }
                            finally
                            {
                                db.Database.Connection.Close();
                                homeWapModel.PhoneMsg = "";
                            }
                        }
                    }
                }
                return View(homeWapModel);
        }
        #endregion
        #region businessid update
        public static void businessupdate(Int64 bid, Int64 uid, Int64 utype)
        {   
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_wap_update_businessusernew_opt]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@bid", bid));
                cmd.Parameters.Add(new SqlParameter("@uid", uid));
                cmd.Parameters.Add(new SqlParameter("@Utype", utype));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
        }
        #endregion
        //#region LGA
        //[AcceptVerbs(HttpVerbs.Get)]
        //public JsonResult GetProductByCategory(int catId)
        //{
        //    return Json(Utility.GetSelectListCity(catId), JsonRequestBehavior.AllowGet);
        //}
        //#endregion
        #region ThanksYou
        public ActionResult ThankYou(string userid, string clickmail, string resend, string verificationcodenew)
        {
            if (verificationcodenew == null || verificationcodenew == "")
                verificationcodenew = "qaz";
            string name=null, loginid=null, password=null, verificationcode=null;
            List<UserReg.UserDetailEmail> showdetails = new List<UserReg.UserDetailEmail>();
            int checkid;
            if (!string.IsNullOrEmpty(userid))
            {
                int intuserid;
                if (int.TryParse(userid, out intuserid))
                {
                    checkid = intuserid;
                }
                else
                {
                    checkid = 0;
                }
            }
            else
            {
                checkid = 0;
            }
            if (checkid > 0)
            {
                using (var db = new VconnectDBContext29())
                {
                    var cmd = db.Database.Connection.CreateCommand();
                        cmd.CommandText = "[dbo].[prc_web_select_userdetail_opt]";
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandTimeout = 300;
                        cmd.Parameters.Add(new SqlParameter("@contentid", checkid));
                    try
                    {
                        var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        showdetails = ((IObjectContextAdapter)db).ObjectContext.Translate<UserReg.UserDetailEmail>(reader).ToList();
                        if (showdetails.Count > 0)
                        {
                            name = !string.IsNullOrEmpty(showdetails.First().contactname) ? showdetails.First().contactname.ToString() : "";
                            loginid = showdetails.First().email.ToString();
                            password = showdetails.First().password.ToString();
                            verificationcode = showdetails.First().verificationcode.ToString();
                        }
                        else
                        {
                            name = ""; loginid = ""; password = ""; verificationcode = "";
                        }
                    }
                    catch (Exception ex)
                    {
                        log.LogMe(ex);
                    }
                    finally
                    {
                        db.Database.Connection.Close();
                    }
                }
                if (clickmail == "email")
                {
                    if (!string.IsNullOrEmpty(userid))
                    {
                        int useridnew = int.Parse(userid.ToString());
                        if (!string.IsNullOrEmpty(loginid))
                        {
                            SendFavioriteBizCred(name, useridnew, loginid, password, verificationcode, 2, 60);
                            ViewBag.Msg = "Activation link sent to your email(" + loginid + ")  successfully";
                        }
                    }
                    else
                        ViewBag.Message = "1";
                }
                if (string.IsNullOrEmpty(resend))
                {
                    int result = 0;
                    if (!string.IsNullOrEmpty(verificationcode) && !string.IsNullOrEmpty(loginid))
                    {
                        using (var db = new VconnectDBContext29())
                        {
                            var cmd = db.Database.Connection.CreateCommand();
                            cmd.CommandText = "[dbo].[prc_VerifyUserBeta_opt]";
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@contentid", checkid));
                            cmd.Parameters.Add(new SqlParameter("@verificationcode", verificationcodenew.ToString().Trim()));
                            cmd.Parameters.Add(new SqlParameter("@isemailverified", 1));
                            //cmd.Parameters.Add(new SqlParameter("@ismobileverified", 1));
                            cmd.Parameters.Add(new SqlParameter("@Err", SqlDbType.VarChar, 10));
                            cmd.Parameters["@Err"].Direction = ParameterDirection.Output;
                            try
                            {
                                var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                                connection.Open();
                                cmd.ExecuteNonQuery();
                                result = Convert.ToInt32(cmd.Parameters["@Err"].Value.ToString());
                            }
                            catch (Exception ex)
                            {
                                log.LogMe(ex);
                            }
                            finally
                            {
                                db.Database.Connection.Close();
                            }
                        }
                        if (result == 0)
                        {
                            if (!string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(loginid))
                            {
                                sendmail(userid.Trim(), loginid.Trim(), "WelcomeAboard.html");
                                LoginModel login = new LoginModel();
                                login.Password = password;
                                login.Login = loginid.ToString().Trim();
                                ViewBag.Message = "0";
                                Session["model"] = login;
                                return RedirectToRoute("wblogin");
                            }
                            else
                            {
                                ViewBag.Message = "0";
                            }
                        }
                        else if (result == 1)
                        {
                            ViewBag.Message = "1";
                        }
                        else if (result == 2)
                        {
                            ViewBag.Message = "2";
                        }
                        else
                        {
                            ViewBag.Message = "3";
                        }
                    }
                    else
                    {
                        ViewBag.Message = "1";
                    }
                }
                return View();
            }
            else
            {
                ViewBag.Message = "1";
                return View();
            }
        }
        #endregion
        #region termscondition
        public ActionResult TermsCondition()
        {
            return View();
        }
        #endregion
        #region sendemail
        protected void sendmail(string userid, string useremail, string templateName)
        {
            bool isEmail = Regex.IsMatch(useremail, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z");
            if (isEmail)
            {
                string Message = "";
                string userurl = Utility.userdetail(Int32.Parse(userid), "userurl");
                //System.Net.Mail.SmtpClient SMPT = new System.Net.Mail.SmtpClient();
                //System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage();

                System.IO.FileStream FsContent = new System.IO.FileStream(Server.MapPath("~/resource/email_templates/" + templateName), System.IO.FileMode.Open, System.IO.FileAccess.Read);
                System.IO.StreamReader SRcontent = new System.IO.StreamReader(FsContent);
                Message = SRcontent.ReadToEnd();
                FsContent.Close();
                SRcontent.Close();
                FsContent.Dispose();
                SRcontent.Dispose();
                Message = Message.Replace("{1}", ConfigurationManager.AppSettings["WebsiteRootPath"].ToString() + "user/" + userurl);
                Message = Message.Replace("{3}", ConfigurationManager.AppSettings["WebsiteRootPath"]).ToString();

                //Email.Subject = "Welcome to Vconnect!!";
                //Email.From = new System.Net.Mail.MailAddress((ConfigurationManager.AppSettings["networkUserIdvcCustomer"]).ToString(), "VConnect.com");
                //Email.IsBodyHtml = true;
                //Email.Body = Message;
                //Email.To.Add(useremail.ToString().Trim());
                //===================================
                var recipients = new List<Mandrill.Messages.Recipient>();
                recipients.Add(new Mandrill.Messages.Recipient(useremail, ""));
                var mandrill = new Mandrill.Messages.Message()
                {
                    To = recipients.ToArray(),
                    FromEmail = "info@VConnect.com",
                    Subject = "Welcome to Vconnect!!",

                    Html = Message
                };
                //===================================
                SendEmail send = new SendEmail();
                try
                {
                    send.VCUsermails(mandrill);
                    //send.SendMailToUser(ref Email, (ConfigurationManager.AppSettings["networkUserIdvcCustomer"]).ToString(), (ConfigurationManager.AppSettings["networkUserIdPasswordvcCustomer"]).ToString());
                    //send.SendMailToUser(ref Email, ConfigurationManager.AppSettings["DGmailid"].ToString(), ConfigurationManager.AppSettings["DGpwd"].ToString());

                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
                //Email.Dispose();
            }
        }
        protected void SendFavioriteBizCred(string Name, int LoginID, string EmailID, string Password, string verificationcode, int qflag, int usertype)
        {
            string location = string.Empty;
            string vcusername = Name;
            string bizName = Name.ToString().Trim();
            string Message = "";
            if (!string.IsNullOrEmpty(EmailID))
            {
                System.IO.FileStream FsContent;
                FsContent = new System.IO.FileStream(Server.MapPath("~/Resource/email_templates/userregistration.html"), System.IO.FileMode.Open, System.IO.FileAccess.Read);
                System.IO.StreamReader SRcontent = new System.IO.StreamReader(FsContent);
                Message = SRcontent.ReadToEnd();
                FsContent.Close();
                SRcontent.Close();
                FsContent.Dispose();
                SRcontent.Dispose();
                Message = Message.Replace("{10}", Utility.ToTitleCase(Name));
                Message = Message.Replace("{2}", Password);
                Message = Message.Replace("{20}", ConfigurationManager.AppSettings["WebsiteRootPath"]).ToString();
                Message = Message.Replace("{11}", (ConfigurationManager.AppSettings["WebsiteRootPath"]).ToString() + "UserReg/ThankYou?userid=" + LoginID.ToString().Trim() + "&verificationcodenew=" + verificationcode);
                // Message = Message.Replace("{12}", LoginID.ToString().Trim());
                Message = Message.Replace("{13}", EmailID);
                Message = Message.Replace("{14}", qflag.ToString().Trim());
                Message = Message.Replace("{15}", usertype.ToString().Trim());
                //Email.Subject = ConfigurationManager.AppSettings["BizAddFavorite"].ToString();           
                //Email.Subject = bizName.ToString() + " is one of " + vcusername.ToString() + "'s Favorite";
                var recipients = new List<Mandrill.Messages.Recipient>();
                recipients.Add(new Mandrill.Messages.Recipient(EmailID, ""));
                var mandrill = new Mandrill.Messages.Message()
                {
                    To = recipients.ToArray(),
                    FromEmail = "info@VConnect.com",
                    Subject = "You are Successfully Registered on VConnect",

                    Html = Message
                };

                SendEmail send = new SendEmail();
                try
                {
                    send.VCUsermails(mandrill);
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
            }
        }
        #endregion
        #region selectid
        public int contentidselect( string emailid,string sdr)
        {
            string name = null, password = null;
            List<UserReg.UserDetailEmail> showdetails = new List<UserReg.UserDetailEmail>();
            int contentid = 0;
            using (var db = new VconnectDBContext29())
            {
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[prc_web_select_contentidNew_opt]";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandTimeout = 300;
                cmd.Parameters.Add(new SqlParameter("@emailid", emailid.ToString().Trim()));
                cmd.Parameters.Add(new SqlParameter("@password", sdr.ToString().Trim()));
                try
                {
                    var connection = ((IObjectContextAdapter)db).ObjectContext.Connection;
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    showdetails = ((IObjectContextAdapter)db).ObjectContext.Translate<UserReg.UserDetailEmail>(reader).ToList();
                    contentid = int.Parse(showdetails.First().customerid.ToString());
                    name = !string.IsNullOrEmpty(showdetails.First().contactname) ? showdetails.First().contactname.ToString() : "";
                    password = showdetails.First().password.ToString();
                    SendFavioriteBizCred(name.ToString(), contentid, emailid.ToString().Trim(), password.ToString().Trim(), sdr.ToString().Trim(), 2, 60);                              
                }
                catch (Exception ex)
                {
                    log.LogMe(ex);
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
            return contentid;
        }
        #endregion
    }
}
